Инструкция по установке на Windows:

1. Скачать актуальную версию Python (3.11.5 на 25.08.23)
Ссылка на установочный файл - https://www.python.org/ftp/python/3.11.5/python-3.11.5-amd64.exe

2. Установить данный .exe (желательно с правами администратора, но не обязательно)

3. После установки нужно 1 (один) раз запустить скрипт pip.cmd, находящийся в папке с основными скриптами
Данный скрипт установит актуальную версию менеджера пакетов Python - pip - и все библиотеки, необходимые для работы скриптов, которые обрабатывают данные

4. После этого можно пользоваться основными скриптами обработки данных.
Общая инструкция для запуска:
- Открыть командную строку Windows
- Вписать команду: python "путь_к_файлу" флаг аргумент_флага
    Пример: python "C:/Users/MyUser/Documents/opendata_parser.py" -u https://rfgf.ru/data.csv

    4.1. opendata_parser.py

    Предусмотрен выбор режима загрузки CSV. Без указания опций запустится с флагом -u с дефолтным значением. При указании обоих флагов -u и -f приоритет имеет флаг -u
    Опции запуска:
        -h, --help - Показывает справку о параметрах запуска и закрывает скрипт, не выполняя парсинг
        -u [URL], --url [URL] - Загрузка CSV-файла по прямой ссылке. Если не указать адрес вместо [URL], то по умолчанию используется https://rfgf.ru/catalog/temp_files/opendata/license/opendata.csv
        -f [File], --file [File] - Загрузка CSV-файла из файловой системы. Если не указать путь к файлу вместо [File], то по умолчанию используется файл opendata.csv из директории, в которой лежит этот скрипт

    Примеры запусков:
        python "C:/Users/MyUser/Documents/opendata_parser.py" - загрузит CSV-файл по дефолтной ссылке https://rfgf.ru/catalog/temp_files/opendata/license/opendata.csv
        python "C:/Users/MyUser/Documents/opendata_parser.py" -u https://rfgf.ru/data.csv - загрузит CSV-файл по указанной ссылке https://rfgf.ru/data.csv
        python "C:/Users/MyUser/Documents/opendata_parser.py" -f - загрузит CSV-файл opendata.csv из файловой системы, а именно C:/Users/MyUser/Documents/opendata.csv (по умолчанию папка с CSV совпадает с папкой расположения скрипта)
        python "C:/Users/MyUser/Documents/opendata_parser.py" -f C:/Users/MyUser/Documents/111.csv - загрузит указанный CSV-файл из файловой системы, а именно C:/Users/MyUser/Documents/111.csv

    4.2. unmz_parser.py

    Предусмотрен выбор папки с Excel-файлами / Excel-файла для обработки. Без указания опций запустится с флагом -d без аргумента, т.е. все Excel-файлы из текущей директории, в которой лежит этот скрипт

    Опции запуска:
        -h, --help - Показывает справку о параметрах запуска и закрывает скрипт, не выполняя парсинг
        -d [Directory], --dir [Directory], --directory [Directory] - Обработка всех Excel-файлов в выбранной папке. Если не указать путь к папке вместо [Directory], то используется папка, в которой лежит этот скрипт
        -f [File], --file [File] - Обработка Excel-файла из файловой системы. Если не указать путь к файлу вместо [File], то по умолчанию используется файл Книга1.xslx из папки, в которой лежит этот скрипт

    Примеры запусков:
        python "C:/Users/MyUser/Documents/unmz_parser.py" - загрузит все Excel-файлы (файлы с расширениями .xslx и .xsl) из текущей папки, а именно C:/Users/MyUser/Documents (по умолчанию папка с Excel совпадает с папкой расположения скрипта)
        python "C:/Users/MyUser/Documents/unmz_parser.py" -d C:/Users/MyUser/Documents/UNMZ - загрузит все Excel-файлы из указанной папки C:/Users/MyUser/Documents/UNMZ
        python "C:/Users/MyUser/Documents/unmz_parser.py" -f - загрузит один дефолтный Excel-файл C:/Users/MyUser/Documents/Книга1.xslx (по умолчанию папка с Excel совпадает с папкой расположения скрипта)
        python "C:/Users/MyUser/Documents/unmz_parser.py" -f C:/Users/MyUser/Documents/Книга2.xsl - загрузит один Excel-файл по указанному пути, а именно C:/Users/MyUser/Documents/Книга2.xsl


Для удобства также сделал скрипты opendata_parser.cmd и unmz_parser.cmd - они автоматически запускают соответствующие с настройками по умолчанию

5. Результат работы скриптов:

    5.1. opendata_parser.py
        В схеме public будут созданы два представления: public.license_points и public.license_polygons, содержащие актуальные данные по всем загруженным точкам и полигонам.
        Также в схеме public будут содержаться таблицы public.license_points_current и public.license_polygons_current, на основе которых построены вышеупомянутые представления.
        При загрузке новых данных таблица с суффиксом _current будет переименована в таблицу с суффиксом _backup, затем будет создана новая таблица с суффиксом _current и новые данные загрузятся в неё, не удаляя предыдущую версию (она будет храниться в _backup-таблице)
        Представления автоматически переключатся на _current-таблицу. Таким образом, данные в представлениях будут всегда актуальны.

    5.2. unmz_parser.py
        Алгоритм действий полностью аналогичный, результаты хранятся в представленияъ public.unmz_points и public.unmz_polygons, и соответствующих _current и _backup-таблицах

6. Все параметры парсера указаны в файле config.py
Например, там можно поменять схему или имена таблиц с результатами, пароль от БД и т.д.