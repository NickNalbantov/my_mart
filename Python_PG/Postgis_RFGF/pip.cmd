cd %~dp0
python -m pip install --no-cache-dir pip==23.2.1
python -m pip install --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org -r requirements.txt
pause