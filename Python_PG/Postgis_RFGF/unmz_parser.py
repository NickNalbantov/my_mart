from config import *

lg = logger()


################################################################

# Подготовка параметров запуска
parser = argparse.ArgumentParser(description = """Выбор папки с Excel-файлами. По умолчанию - запуск c флагом -d без аргумента, т.е. все Excel-файлы из текущей директории, в которой лежит этот скрипт""")

parser.add_argument('-d',
                    '--dir',
                    '--directory',
                    metavar = 'Directory',
                    type = str,
                    const = Path(__file__).parent,
                    nargs = '?',
                    help = f'Обработка всех Excel-файлов в выбранной директории. Если не указать путь к папке, то используется директория, в которой лежит этот скрипт',
                    action = 'store'
                   )

parser.add_argument('-f',
                    '--file',
                    metavar = 'File',
                    type = str,
                    const = (Path(__file__).parent).joinpath(SOURCE.EXCEL_DEFAULT_FILENAME),
                    nargs = '?',
                    help = f'Обработка Excel-файла из файловой системы. Если не указать путь к файлу, то по умолчанию используется файл {SOURCE.EXCEL_DEFAULT_FILENAME} из директории, в которой лежит этот скрипт',
                    action = 'store'
                   )

args = vars(parser.parse_args())

folder_path, file_path = '', ''

if args['dir'] is None and args['file'] is None: # если аргументы не заданы, обрабатываем все Excel из текущей папки
    folder_path = Path(__file__).parent

elif args['dir'] is not None: # Если вызван флаг -d, обрабатываем все Excel из выбранной папки (= дефолтной, если аргумент -d пустой)
    folder_path = args['dir']

else: # если не передавался флаг -d и передавался флаг -f, используем путь к Excel-файлу (по умолчанию при пустом аргументе -f = *папка с этим скриптом*/Книга1.xslx)
    file_path = args['file']


################################################################

# Обработка файлов
df = pd.DataFrame()
obj_num = 1

    # Обработка папки
if folder_path != '':
    for file_name in Path.iterdir(folder_path):
        if str(file_name).endswith(".xlsx") or str(file_name).endswith(".xls"):

            file_path = folder_path.joinpath(file_name)

            try:
                xsl_df = pd.read_excel(file_path, usecols='A:G', header = None, keep_default_na = False, na_values = "")

                xsl_df['object_num'] = obj_num # считаем, что в каждом файле хранятся точки одного участка, выдаём им последовательную нумерацию
                xsl_df.astype({'object_num': 'float64'})
                obj_num += 1

                df = pd.concat([df, xsl_df], ignore_index=True)

            except Exception as e:
                lg.error(f"Ошибка при обработке маппига: {file_name}. Причина: {str(e)}")
                raise Exception(f"Ошибка при обработке маппига: {file_name}. Причина: {str(e)}")

    # Обработка отдельного файла
elif folder_path == '' and file_path != '':
    try:
        df = pd.read_excel(file_path, usecols='B:G', keep_default_na = False, na_values = "")
        df = df.drop(df.index[0]).reset_index(drop = True)

    except Exception as e:
        lg.error(f"Ошибка при обработке маппига: {file_path}. Причина: {str(e)}")
        raise Exception(f"Ошибка при обработке маппига: {file_path}. Причина: {str(e)}")

else:
    raise ValueError('Требуется указать валидный путь к нужному Excel-файлу или к папке, содержащей его')

df.rename(columns = {0: "point_num", 1: "lat_deg", 2: "lat_min", 3: "lat_sec", 4: "lon_deg", 5: "lon_min", 6: "lon_sec"}, inplace = True)

df['lat'] = df["lat_deg"].astype(str) + '|' + df["lat_min"].astype(str) + '|' + df["lat_sec"].astype(str)
df['lat'] = df['lat'].astype(str).apply(lambda x: dms2dd_unmz(x))

df['lon'] = df["lon_deg"].astype(str) + '|' + df["lon_min"].astype(str) + '|' + df["lon_sec"].astype(str)
df['lon'] = df['lon'].astype(str).apply(lambda x: dms2dd_unmz(x))

df = df[
        (abs(df['lat'].astype('float64')) < 180)
        & (abs(df['lon'].astype('float64')) < 180)
        & (df['lat'].astype('float64') != 0)
        & (df['lon'].astype('float64') != 0)
       ]

df['points'] = "SRID=4326;POINT(" + df['lon'] + " " + df['lat']  + ")"
df.drop(columns = ["lat_deg", "lat_min", "lat_sec", "lon_deg", "lon_min", "lon_sec", 'lon', 'lat'], inplace = True)

df = df[(~df['points'].isna()) & (df['points'] != '')]

df = df[['object_num', 'point_num', 'points']]

lg.info(f"Обработка файлов завершена, будет загружено {len(df.index)} строк")


################################################################

# Выгружаем в PG
engine = create_engine(url = DB.DB_CONNECT_URI, echo = False)

tab_cols = {
            'object_num': sa_tp.INTEGER(),
            'point_num': sa_tp.INTEGER(),
            'points': sa_tp.TEXT()
           }

with engine.connect() as con_alc:
    with con_alc.begin():
        lg.info("Соединение с БД открыто")

        tbl_cur = sql.Identifier(f"{DB.UNMZ_POINTS_TABLE}_current")
        tbl_bkp = sql.Identifier(f"{DB.UNMZ_POINTS_TABLE}_backup")
        sch = sql.Identifier(DB.TARGET_SCHEMA)

        con_alc.execute(text(sql.SQL("""drop table if exists {schemaname}.{tablename_backup} cascade""").format(
                schemaname = sch,
                tablename_backup = tbl_bkp
            ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            alter table if exists {schemaname}.{tablename_current}
                rename to {tablename_backup}
            """
            ).format(
                        schemaname = sch,
                        tablename_current = tbl_cur,
                        tablename_backup = tbl_bkp
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            create table if not exists {schemaname}.{tablename_current}
            (
                id int generated always as identity primary key,
                object_num int not null,
                point_num int not null,
                points geography(point, 4326) not null
            )
            """
            ).format(
                        schemaname = sch,
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        df.to_sql(name = f"{DB.UNMZ_POINTS_TABLE}_current", schema = DB.TARGET_SCHEMA, con = con_alc, dtype = tab_cols, index = False, if_exists = 'append', method = psql_insert_copy)

        con_alc.execute(text(sql.SQL("""create index on {schemaname}.{tablename_current} using gist (points) with (fillfactor='100')""").format(
                        schemaname = sch,
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""analyze {schemaname}.{tablename_current}""").format(
                        schemaname = sch,
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            create or replace view {schemaname}.{viewname} as
                select *
                from {schemaname}.{tablename_current}
            """
            ).format(
                        schemaname = sch,
                        viewname = sql.Identifier(DB.UNMZ_POINTS_TABLE),
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        lg.info(f"Таблица с точками из Excel-файлов обновлена")


        # создаём таблицу с полигонами и рассчитываем их из точек
        tbl_cur = sql.Identifier(f"{DB.UNMZ_POLYGONS_TABLE}_current")
        tbl_bkp = sql.Identifier(f"{DB.UNMZ_POLYGONS_TABLE}_backup")

        con_alc.execute(text(sql.SQL("""drop table if exists {schemaname}.{tablename_backup} cascade""").format(
                        schemaname = sch,
                        tablename_backup = tbl_bkp
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            alter table if exists {schemaname}.{tablename_current}
                rename to {tablename_backup}
            """
            ).format(
                        schemaname = sch,
                        tablename_current = tbl_cur,
                        tablename_backup = tbl_bkp
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            create table if not exists {schemaname}.{tablename_current}
            (
                id int generated always as identity primary key,
                object_num integer not null,
                polygons geography(multipolygon, 4326) not null
            )
            """
            ).format(
                        schemaname = sch,
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            insert into {schemaname}.{poly_table} (object_num, polygons)
                with lines as
                (
                    select
                        object_num,
                        ST_MakeLine(array_agg(points order by point_num)) as lin
                    from
                    (
                        select
                            object_num,
                            point_num,
                            points::geometry(point, 4326)
                        from {schemaname}.{points_table}
                    ) p
                    group by
                        object_num
                    having max(point_num) > 2 -- берём только объекты с количеством точек больше 2
                ),

                clsd as
                (
                    select
                        object_num,
                        case
                            when ST_IsClosed(lin) is false -- незамкнутые линии
                                then ST_MakeValid(ST_AddPoint(lin, ST_StartPoint(lin)))
                            when ST_IsClosed(lin) is true -- замкнутые линии
                                then ST_MakeValid(lin)
                        end as lin
                    from lines
                ),

                smpl as
                (
                    select
                        object_num,
                        case
                            when ST_IsSimple(lin) is false -- самопересекающиеся линии
                                then ST_MakeValid(ST_UnaryUnion(lin))
                            when ST_IsSimple(lin) is true -- простые линии
                                then lin
                        end as lin
                    from clsd
                )

                select *
                from
                (
                    select
                        object_num,
                        ST_Multi(ST_BuildArea(lin)) as polygons
                    from smpl
                ) l
                where polygons is not null
            """
            ).format(
                        schemaname = sch,
                        poly_table = tbl_cur,
                        points_table = sql.Identifier(DB.UNMZ_POINTS_TABLE)
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""create index on {schemaname}.{tablename_current} using gist (polygons) with (fillfactor='100')""").format(
                    schemaname = sch,
                    tablename_current = tbl_cur
                ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""create index on {schemaname}.{tablename_current} using btree (object_num)""").format(
                    schemaname = sch,
                    tablename_current = tbl_cur
                ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""analyze {schemaname}.{tablename_current}""").format(
                    schemaname = sch,
                    tablename_current = tbl_cur
                ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            create or replace view {schemaname}.{viewname} as
                select *
                from {schemaname}.{tablename_current}
            """
            ).format(
                        schemaname = sch,
                        viewname = sql.Identifier(DB.UNMZ_POLYGONS_TABLE),
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        lg.info("Таблица с полигонами обновлена")

lg.info("Соединение с БД закрыто")