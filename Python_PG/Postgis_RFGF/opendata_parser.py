from config import *

lg = logger()


################################################################

# Подготовка параметров запуска
parser = argparse.ArgumentParser(description = """Выбор режима загрузки CSV. Без указания опций запустится с флагом -u с дефолтным значением. При указании обоих флагов -u и -f приоритет имеет флаг -u""")

parser.add_argument('-u',
                    '--url',
                    metavar = 'URL',
                    type = str,
                    const = SOURCE.CSV_URL,
                    nargs = '?',
                    help = f'Загрузка CSV-файла по прямой ссылке. Если не указать адрес, то по умолчанию используется {SOURCE.CSV_URL}',
                    action = 'store'
                   )

parser.add_argument('-f',
                    '--file',
                    metavar = 'File',
                    type = str,
                    const = (Path(__file__).parent).joinpath(SOURCE.CSV_FILENAME),
                    nargs = '?',
                    help = f'Загрузка CSV-файла из файловой системы. Если не указать путь к файлу, то по умолчанию используется файл {SOURCE.CSV_FILENAME} из директории, в которой лежит этот скрипт',
                    action = 'store'
                   )

args = vars(parser.parse_args())

if args['url'] is None and args['file'] is None: # если аргументы не заданы, скачиваем CSV по дефолтной ссылке
    lg.info(f"Загрузка файла стартовала, ссылка: {SOURCE.CSV_URL}")
    response = requests.get(SOURCE.CSV_URL, allow_redirects = True)
    enc = response.apparent_encoding # автоматическое определение кодировки
    csv_file = StringIO(response.content.decode(enc))
    lg.info("Загрузка файла завершена")

elif args['url'] is not None: # Если вызван флаг -u, используем переданную ссылку (= дефолтной, если аргумент -u пустой)
    lg.info(f"Загрузка файла стартовала, ссылка: {args['url']}")
    response = requests.get(args['url'], allow_redirects = True)
    enc = response.apparent_encoding # автоматическое определение кодировки
    csv_file = StringIO(response.content.decode(enc))
    lg.info("Загрузка файла завершена")

else: # если не передавался флаг -u и передавался флаг -f, используем путь к локальному CSV-файлу (по умолчанию при пустом аргументе -f = *папка с этим скриптом*/opendata.csv)
    csv_file = args['file']
    with open(csv_file, 'rb') as f:
        enc = chardet.detect(f.readline())['encoding'] # автоматическое определение кодировки


################################################################

# Начинаем парсить
lg.info("Начало процедуры парсинга")

# Выгружаем в dataframe
df = pd.read_csv(
                    filepath_or_buffer = csv_file,
                    sep = ';',
                    header = 0,
                    usecols = [1, 8],
                    names = ['reg_num', 'raw'],
                    dtype = {'reg_num': str, 'raw': str},
                    encoding = enc
                )

# Фильтруем пустые строки
df = df[(~df['raw'].isna()) & (df['raw'] != '')]
df = df[(~df['reg_num'].isna()) & (df['reg_num'] != '')]

# Выделяем систему координат
df['srid'] = df['raw'].str.replace(r'(^.+(Система координат - ))', '', regex = True)
df['srid'] = df['srid'].str.replace(r' .+$', '', regex = True)
srid_table = {'WGS-84': '4326', 'ГСК-2011': '7683', 'Пулково-42': '4284'}
df['srid'] = df['srid'].astype(str).apply(lambda x: regex_translate(x, srid_table))

# Обрезаем текст в начале
df['raw'] = df['raw'].str.replace(r'(^.+(Д\(гр,мин,сек\)  ))', '', regex = True)

# Разделяем на объекты
df['raw'] = df['raw'].astype(str).apply(lambda x: x.split(sep = '----------------Объект № '))
df = df.explode(column = 'raw', ignore_index = True)
df = df[df['raw'] != ''] # убираем лишние пустые строки после explode
df['raw'] = df['raw'].apply(lambda x: add_delimiter_for_split(x, '---------------  ', 1, to_end = False)) # вставляем заглушку в начало, т.к. есть строки, которые начинаются сразу с координат точек без маркера объекта
df[['object_num', 'points_dms']] = df['raw'].str.split('---------------  ', n = 1, expand = True)
df.drop(columns = 'raw', inplace = True)
df.replace({'~null~':1}, inplace = True) # Если объект единственный, то проставляем ему № 1
df.astype({'object_num': 'float64'})

# Разделяем на точки
df['points_dms'] = df['points_dms'].astype(str).apply(lambda x: re.split(r'\d{1,4}\s{4,8}', x))
df = df.explode(column = 'points_dms', ignore_index = True)
df = df[df['points_dms'] != ''] # убираем лишние пустые строки после explode

df['points_dms'] = df['points_dms'].str.replace(r'^\D.+', '', regex = True) # убираем лишние символы в начале
repl = lambda m: f'"{m.group(1)}, '
# Долгота сейчас обозначена русской буквой Е, заложил и правильный вариант с английской E
df['points_dms'] = df['points_dms'].str.replace(r'\"(N|S|W|E|Е)\s+', repl, regex = True) # разделяем широту и долготу запятой
repl = lambda m: f'"{m.group(1)}'
df['points_dms'] = df['points_dms'].str.replace(r'\"(N|S|W|E|Е), (^[0-9-]).+$', repl, regex = True) # убираем текст в конце (границы)
df['points_dms'] = df['points_dms'].str.replace(', ', ' ') # меняем запятую на пробел для формата точек PostGIS
df['points_dms'] = df['points_dms'].str.replace(' )', ')') # подчищаем лишние пробелы
df['points_dms'] = df['points_dms'].str.replace('Е', 'E') # меняем русские Е на английские

# Преобразуем в десятичный формат и переставляем координаты местами
df[['lat', 'lon']] = df['points_dms'].str.split(' ', n = 1, expand = True)
df['lat'] = df['lat'].astype(str).apply(lambda x: dms2dd_opendata(x))
df['lon'] = df['lon'].astype(str).apply(lambda x: dms2dd_opendata(x))
# Форматируем координаты как точку PostGIS
df['points'] = "SRID=" + df['srid'] + ";POINT(" + df['lon'] + " " + df['lat']  + ")"

# Фильтруем строки с некорректными координатами
df = df[
        (~df['points'].isna())
        & (df['points'] != '')
        & (abs(df['lat'].astype('float64')) < 180)
        & (abs(df['lon'].astype('float64')) < 180)
        & (df['lat'].astype('float64') != 0)
        & (df['lon'].astype('float64') != 0)
       ]
df.drop(columns = ['points_dms', 'lon', 'lat'], inplace = True)

# Нумеруем точки
df['point_num'] = df.sort_index().groupby(['reg_num', 'object_num']).cumcount() + 1
df.astype({'point_num': 'float64'})

df = df[['reg_num', 'object_num', 'point_num', 'srid', 'points']]

lg.info(f"Обработка файла завершена, будет загружено {len(df.index)} строк")


################################################################

# Выгружаем в PG
engine = create_engine(url = DB.DB_CONNECT_URI, echo = False)

tab_cols = {
            'reg_num': sa_tp.TEXT(),
            'object_num': sa_tp.INTEGER(),
            'point_num': sa_tp.INTEGER(),
            'points': sa_tp.TEXT()
           }

with engine.connect() as con_alc:
    with con_alc.begin():
        lg.info("Соединение с БД открыто")

        tbl_cur = sql.Identifier(f"{DB.POINTS_TABLE}_current")
        tbl_bkp = sql.Identifier(f"{DB.POINTS_TABLE}_backup")
        sch = sql.Identifier(DB.TARGET_SCHEMA)

        # Создаём и заполняем из датафрейма таблицу с точками
            # WGS-84
        con_alc.execute(text(sql.SQL("""drop table if exists {schemaname}.{tablename_backup} cascade""").format(
                    schemaname = sch,
                    tablename_backup = tbl_bkp
                ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            alter table if exists {schemaname}.{tablename_current}
                rename to {tablename_backup}
            """
            ).format(
                        schemaname = sch,
                        tablename_current = tbl_cur,
                        tablename_backup = tbl_bkp
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            create table if not exists {schemaname}.{tablename_current}
            (
                id int generated always as identity primary key,
                reg_num text not null,
                object_num integer not null,
                point_num integer not null,
                points geography(point, 4326) not null
            )
            """
            ).format(
                        schemaname = sch,
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        df_wgs = df[df['srid'] == '4326']
        df_wgs = df_wgs[['reg_num', 'object_num', 'point_num', 'points']]
        df_wgs.to_sql(name = f"{DB.POINTS_TABLE}_current", schema = DB.TARGET_SCHEMA, con = con_alc, dtype = tab_cols, index = False, if_exists = 'append', method = psql_insert_copy)

        lg.info(f"Обработка {len(df_wgs.index)} точек в системе координат WGS-84 выполнена")

        # Выгрузка других SRID, кроме 4326 (WGS-84)
            # ГСК-2011

        con_alc.execute(text(sql.SQL("""drop table if exists {schemaname}.{tablename_gsk} cascade""").format(
                    schemaname = sch,
                    tablename_gsk = sql.Identifier(f"{DB.POINTS_TABLE}_7683")
                ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            create table if not exists {schemaname}.{tablename_gsk}
            (
                id int generated always as identity primary key,
                reg_num text not null,
                object_num integer not null,
                point_num integer not null,
                points geography(point, 7683) not null -- ГСК-2011
            )
            """
            ).format(
                        schemaname = sch,
                        tablename_gsk = sql.Identifier(f"{DB.POINTS_TABLE}_7683")
                    ).as_string(con_alc))
        )

        df_gsk = df[df['srid'] == '7683']
        df_gsk = df_gsk[['reg_num', 'object_num', 'point_num', 'points']]
        df_gsk.to_sql(name = f"{DB.POINTS_TABLE}_7683", schema = DB.TARGET_SCHEMA, con = con_alc, dtype = tab_cols, index = False, if_exists = 'append', method = psql_insert_copy)

        con_alc.execute(text(sql.SQL("""
            insert into {schemaname}.{tablename_current} (reg_num, object_num, point_num, points)
                select
                    reg_num,
                    object_num,
                    point_num,
                    ST_Transform(points::geometry(point, 7683), 4326)::geography(point, 4326) as points
                from {schemaname}.{tablename_gsk}
            """
            ).format(
                        schemaname = sch,
                        tablename_current = tbl_cur,
                        tablename_gsk = sql.Identifier(f"{DB.POINTS_TABLE}_7683")
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""drop table if exists {schemaname}.{tablename_gsk} cascade""").format(
                    schemaname = sch,
                    tablename_gsk = sql.Identifier(f"{DB.POINTS_TABLE}_7683")
                ).as_string(con_alc))
        )

        lg.info(f"Обработка {len(df_gsk.index)} точек в системе координат ГСК-2011 выполнена")

            # Пулково-42

        con_alc.execute(text(sql.SQL("""drop table if exists {schemaname}.{tablename_sk42} cascade""").format(
                    schemaname = sch,
                    tablename_sk42 = sql.Identifier(f"{DB.POINTS_TABLE}_4284")
                ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            create table if not exists {schemaname}.{tablename_sk42}
            (
                id int generated always as identity primary key,
                reg_num text not null,
                object_num integer not null,
                point_num integer not null,
                points geography(point, 4284) not null -- Пулково-42
            )
            """
            ).format(
                        schemaname = sch,
                        tablename_sk42 = sql.Identifier(f"{DB.POINTS_TABLE}_4284")
                    ).as_string(con_alc))
        )

        df_sk42 = df[df['srid'] == '4284']
        df_sk42 = df_sk42[['reg_num', 'object_num', 'point_num', 'points']]
        df_sk42.to_sql(name = f"{DB.POINTS_TABLE}_4284", schema = DB.TARGET_SCHEMA, con = con_alc, dtype = tab_cols, index = False, if_exists = 'append', method = psql_insert_copy)

        con_alc.execute(text(sql.SQL("""
            insert into {schemaname}.{tablename_current} (reg_num, object_num, point_num, points)
                select
                    reg_num,
                    object_num,
                    point_num,
                    ST_Transform(points::geometry(point, 4284), 4326)::geography(point, 4326) as points
                from {schemaname}.{tablename_sk42}
            """
            ).format(
                        schemaname = sch,
                        tablename_current = tbl_cur,
                        tablename_sk42 = sql.Identifier(f"{DB.POINTS_TABLE}_4284")
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""drop table if exists {schemaname}.{tablename_sk42} cascade""").format(
                    schemaname = sch,
                    tablename_sk42 = sql.Identifier(f"{DB.POINTS_TABLE}_4284")
                ).as_string(con_alc))
        )

        lg.info(f"Обработка {len(df_sk42.index)} точек в системе координат Пулково-42 выполнена")

        # Обслуживание таблицы точек
        con_alc.execute(text(sql.SQL("""create index on {schemaname}.{tablename_current} using gist (points) with (fillfactor='100')""").format(
                        schemaname = sch,
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""create index on {schemaname}.{tablename_current} using btree (reg_num)""").format(
                        schemaname = sch,
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""analyze {schemaname}.{tablename_current}""").format(
                        schemaname = sch,
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            create or replace view {schemaname}.{viewname} as
                select *
                from {schemaname}.{tablename_current}
            """
            ).format(
                        schemaname = sch,
                        viewname = sql.Identifier(DB.POINTS_TABLE),
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        lg.info(f"Таблица с точками обновлена")


        # создаём таблицу с полигонами и рассчитываем их из точек
        tbl_cur = sql.Identifier(f"{DB.POLYGONS_TABLE}_current")
        tbl_bkp = sql.Identifier(f"{DB.POLYGONS_TABLE}_backup")

        con_alc.execute(text(sql.SQL("""drop table if exists {schemaname}.{tablename_backup} cascade""").format(
                        schemaname = sch,
                        tablename_backup = tbl_bkp
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            alter table if exists {schemaname}.{tablename_current}
                rename to {tablename_backup}
            """
            ).format(
                        schemaname = sch,
                        tablename_current = tbl_cur,
                        tablename_backup = tbl_bkp
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            create table if not exists {schemaname}.{tablename_current}
            (
                id int generated always as identity primary key,
                reg_num text not null,
                object_num integer not null,
                polygons geography(multipolygon, 4326) not null
            )
            """
            ).format(
                        schemaname = sch,
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            insert into {schemaname}.{poly_table} (reg_num, object_num, polygons)
                with lines as
                (
                    select
                        reg_num,
                        object_num,
                        ST_MakeLine(array_agg(points order by point_num)) as lin
                    from
                    (
                        select
                            reg_num,
                            object_num,
                            point_num,
                            points::geometry(point, 4326)
                        from {schemaname}.{points_table}
                    ) p
                    group by
                        reg_num,
                        object_num
                    having max(point_num) > 2 -- берём только объекты с количеством точек больше 2
                ),

                clsd as
                (
                    select
                        reg_num,
                        object_num,
                        case
                            when ST_IsClosed(lin) is false -- незамкнутые линии
                                then ST_MakeValid(ST_AddPoint(lin, ST_StartPoint(lin)))
                            when ST_IsClosed(lin) is true -- замкнутые линии
                                then ST_MakeValid(lin)
                        end as lin
                    from lines
                ),

                smpl as
                (
                    select
                        reg_num,
                        object_num,
                        case
                            when ST_IsSimple(lin) is false -- самопересекающиеся линии
                                then ST_MakeValid(ST_UnaryUnion(lin))
                            when ST_IsSimple(lin) is true -- простые линии
                                then lin
                        end as lin
                    from clsd
                )

                select *
                from
                (
                    select
                        reg_num,
                        object_num,
                        ST_Multi(ST_BuildArea(lin)) as polygons
                    from smpl
                ) l
                where polygons is not null
            """
            ).format(
                        schemaname = sch,
                        poly_table = tbl_cur,
                        points_table = sql.Identifier(DB.POINTS_TABLE)
                    ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""create index on {schemaname}.{tablename_current} using gist (polygons) with (fillfactor='100')""").format(
                    schemaname = sch,
                    tablename_current = tbl_cur
                ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""create index on {schemaname}.{tablename_current} using btree (reg_num)""").format(
                    schemaname = sch,
                    tablename_current = tbl_cur
                ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""analyze {schemaname}.{tablename_current}""").format(
                    schemaname = sch,
                    tablename_current = tbl_cur
                ).as_string(con_alc))
        )

        con_alc.execute(text(sql.SQL("""
            create or replace view {schemaname}.{viewname} as
                select *
                from {schemaname}.{tablename_current}
            """
            ).format(
                        schemaname = sch,
                        viewname = sql.Identifier(DB.POLYGONS_TABLE),
                        tablename_current = tbl_cur
                    ).as_string(con_alc))
        )

        lg.info("Таблица с полигонами обновлена")

lg.info("Соединение с БД закрыто")