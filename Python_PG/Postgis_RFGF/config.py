import pandas as pd
import numpy as np
import chardet
import re
import requests
import argparse
import csv
import logging
import sys
import openpyxl
import configparser

from sqlalchemy import create_engine, text, types as sa_tp
from psycopg import sql
from io import StringIO
from pathlib import Path

CONFIG_FILE = f'{Path(__file__).resolve().parent}/config.ini'

config = configparser.ConfigParser(allow_no_value = True, empty_lines_in_values = True)
config.read(CONFIG_FILE, 'utf8')

db_connect = config['DB connection']


class DB:
    # Сервер
    HOST = db_connect['host']
    # Порт
    PORT = db_connect['port']
    # База данных
    DATABASE = db_connect['database']
    # Пользователь
    DB_LOGIN = db_connect['user']
    # Пароль
    DB_PASSWORD = db_connect['password']
    # Строка подключения, вариант синтаксиса (SQL Alchemy + psycopg3)
    DB_CONNECT_URI = f"postgresql+psycopg://{DB_LOGIN}:{DB_PASSWORD}@{HOST}:{PORT}/{DATABASE}"
    # Целевая схема
    TARGET_SCHEMA = 'public'
    # Целевые таблицы
    POINTS_TABLE = 'license_points'
    POLYGONS_TABLE = 'license_polygons'
    UNMZ_POINTS_TABLE = 'unmz_points'
    UNMZ_POLYGONS_TABLE = 'unmz_polygons'


class SOURCE:
    CSV_URL = 'https://rfgf.ru/catalog/temp_files/opendata/license/opendata.csv'
    CSV_FILENAME = 'opendata.csv'
    EXCEL_DEFAULT_FILENAME = 'Книга1.xslx'


# Логгер
def logger(log_level: str = 'DEBUG'):
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    frmt = logging.Formatter(fmt = '%(asctime)s - %(levelname)s - строка %(lineno)d - %(message)s', datefmt = '%d.%m.%Y %H:%M:%S')
    handler.setFormatter(frmt)

    log = logging.getLogger('console_logger')
    log.addHandler(handler)
    log.setLevel(log_level)
    log.propagate = False

    return log


# Вспомогательные функции

def add_delimiter_for_split(input, delimiter: str, split_num: int, to_end: bool = True, placeholder: str = '~null~'):
    """
    Функция добавляет заглушки в строковые столбцы, которые подлежат разделению на несколько столбцов методом split
    Этот метод падает с ValueError("Columns must be same length as key"), если на всей выборке одна из новых колонок оказалась пустой
    Использование этой функции предотвращает падение метода split
    """
    cnt_delim = input.count(delimiter)

    while cnt_delim < split_num:
        if to_end is True:
            input = f"{input}{delimiter}{placeholder}"
        else:
            input = f"{placeholder}{delimiter}{input}"

        cnt_delim = input.count(delimiter)

    return input


def dms2dd_opendata(input: str):
    """Функция для конвертации координат из формата DMS в десятичную форму"""
    dms = re.sub("""°|'|\"""", '|', input) # 56°07'40.9968"N -> 56|07|40.9968|N
    dms_lst = dms.split('|')
    try:
        hemisph = dms_lst[3][0]
    except IndexError:
        return np.nan

    # В CSV координаты Западного полушария (Чукотка) представленны некорректно в виде -178°07'40.9968"E, поэтому приходится вводить корректировку
    correct_sign = 1

    if hemisph in ['N', 'E']:
        if float(dms_lst[0]) < 0:
            hemi_sign = -1
            correct_sign = -1
        else:
            hemi_sign = 1
    elif hemisph in ['W', 'S']:
        if float(dms_lst[0]) < 0:
            hemi_sign = 1
            correct_sign = -1
        else:
            hemi_sign = -1
    else:
        return np.nan

    dd = hemi_sign * (correct_sign * float(dms_lst[0]) + (float(dms_lst[1])/60) + (float(dms_lst[2])/3600))

    return f"{dd}"


def dms2dd_unmz(input: str):
    dms_lst = input.split('|')
    dd = float(dms_lst[0]) + float(dms_lst[1])/60 + float(dms_lst[2])/3600
    return f"{dd}"


def regex_translate(input, trans_table):
    """Функция для подмены нескольких сложны подстрок одновременно"""
    regex = re.compile('|'.join(map(re.escape, trans_table)))
    return regex.sub(lambda match: trans_table[match.group(0)], input)


def psql_insert_copy(table, conn, keys, data_iter):
    """
    https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html#insertion-method
    Execute SQL statement inserting data

    Parameters
    ----------
    table : pandas.io.sql.SQLTable
    conn : sqlalchemy.engine.Engine or sqlalchemy.engine.Connection
    keys : list of str
        Column names
    data_iter : Iterable that iterates the values to be inserted
    """
    # gets a DBAPI connection that can provide a cursor
    dbapi_conn = conn.connection
    with dbapi_conn.cursor() as cur:
        s_buf = StringIO()
        writer = csv.writer(s_buf)
        writer.writerows(data_iter)
        s_buf.seek(0)

        columns = ', '.join(['"{}"'.format(k) for k in keys])
        if table.schema:
            table_name = '{}.{}'.format(table.schema, table.name)
        else:
            table_name = table.name

        sql = 'COPY {} ({}) FROM STDIN WITH CSV'.format(table_name, columns)

        with cur.copy(sql) as copy:
            while data := s_buf.read(8192):
                copy.write(data)