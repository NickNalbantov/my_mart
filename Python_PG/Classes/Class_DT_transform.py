import datetime
from datetime import datetime as dtime

class DT_transform():
    """Разнообразные функции для трансформации дат и времени"""

    def __init__(self) -> None:
        pass

# текущие timestamp:

    def dmy_us_now(self):
        """Текущее время --> datetime(год, месяц, день, час, минута, секунда)"""
        now = dtime.now()
        return dtime(now.year, now.month, now.day, now.hour, now.minute, now.second, now.microsecond)

    def dmy_hms_now(self):
        """Текущее время --> datetime(год, месяц, день, час, минута, секунда)"""
        now = dtime.now()
        return dtime(now.year, now.month, now.day, now.hour, now.minute, now.second)

    def dmy_now(self):
        """Текущее время --> datetime(год, месяц, день)"""
        now = dtime.now()
        return dtime(now.year, now.month, now.day)

    def curr_date_dmy(self):
        """Текущая дата в формате 'dd.mm.yy'"""
        dmy = self.dmy_now()
        return dmy.strftime('%d.%m.%y')

    def curr_date_dmy_hms(self):
        """Текущая дата в формате 'dd.mm.yy HH:MM:SS'"""
        dmy = self.dmy_hms_now()
        return dmy.strftime('%d.%m.%y %H:%M:%S')

    def curr_date_us(self):
        """Текущая дата в формате 'dd.mm.yy HH:MM:SS.ffffff'"""
        dmy = self.dmy_us_now()
        return dmy.strftime('%d.%m.%y %H:%M:%S.%f')

    def curr_weekday_number(self):
        """Текущий день недели"""
        dmy = self.dmy_now()
        return dmy.isoweekday()

    def curr_week_number(self):
        """Номер текущей недели в году (isoyear)"""
        dmy = self.dmy_now()
        return dmy.isocalendar().week

# разбор определённой даты:

    def get_weekday_number(self, date_input: str | datetime.datetime | datetime.date = None, format: str = '%d.%m.%y'):
        """Определение дня недели для конкретной даты в формате 'dd.mm.yy'"""
        if date_input is None:
            return self.curr_weekday_number()
        else:
            if isinstance(date_input, str) is True:
                dt = dtime.strptime(date_input, format)
            elif isinstance(date_input, datetime.datetime | datetime.date):
                dt = date_input
            else:
                raise TypeError("Некорректный тип даты")
            return dt.isoweekday()

    def get_week_number(self, date_input: str | datetime.datetime | datetime.date = None, format: str = '%d.%m.%y'):
        """Определение номера недели для конкретной даты в формате 'DD.MM.YY'"""
        if date_input is None:
            return self.curr_week_number()
        else:
            if isinstance(date_input, str) is True:
                dt = dtime.strptime(date_input, format)
            elif isinstance(date_input, datetime.datetime | datetime.date):
                dt = date_input
            else:
                raise TypeError("Некорректный тип даты")
            return dt.isocalendar().week

# дата следующего/предыдущего дня недели:

    def next_weekday(self, weekday: int, base_date: str | datetime.datetime | datetime.date = None, format: str = '%d.%m.%y'):
        """Дата ближайшего следующего дня недели, указанного как номер дня от 1 до 7, в формате 'dd.mm.yy'. Если искомый день недели совпадает с текущим, то вернётся сегодняшняя дата.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'dd.mm.yy'
        """
        if base_date is None:
            now = self.dmy_now()
            dmy = dtime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead < 0:
                days_ahead += 7

            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')
        else:
            if isinstance(base_date, str) is True:
                date = dtime.strptime(base_date, format)
            elif isinstance(base_date, datetime.datetime | datetime.date):
                date = base_date
            else:
                raise TypeError("Некорректный тип даты")

            dmy = dtime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead < 0:
                days_ahead += 7

            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')

    def last_weekday(self, weekday: int, base_date: str | datetime.datetime | datetime.date = None, format: str = '%d.%m.%y'):
        """Дата ближайшего предыдущего дня недели, указанного как номер дня от 1 до 7, в формате 'dd.mm.yy'. Если искомый день недели совпадает с текущим, то вернётся сегодняшняя дата.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'dd.mm.yy'
        """
        if base_date is None:
            now = self.dmy_now()
            dmy = dtime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead > 0:
                days_ahead -= 7

            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')
        else:
            if isinstance(base_date, str) is True:
                date = dtime.strptime(base_date, format)
            elif isinstance(base_date, datetime.datetime | datetime.date):
                date = base_date
            else:
                raise TypeError("Некорректный тип даты")

            dmy = dtime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead > 0:
                days_ahead -= 7

            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')

    def last_weekday_noninclusive(self, weekday: int, base_date: str | datetime.datetime | datetime.date = None, format: str = '%d.%m.%y'):
        """Дата ближайшего предыдущего дня недели, указанного как номер дня от 1 до 7, в формате 'dd.mm.yy'. Если искомый день недели совпадает с текущим, всё равно вернётся дата из следующей недели.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'dd.mm.yy'
        """
        if base_date is None:
            now = self.dmy_now()
            dmy = dtime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead >= 0:
                days_ahead -= 7

            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')
        else:
            if isinstance(base_date, str) is True:
                date = dtime.strptime(base_date, format)
            elif isinstance(base_date, datetime.datetime | datetime.date):
                date = base_date
            else:
                raise TypeError("Некорректный тип даты")

            dmy = dtime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead >= 0:
                days_ahead -= 7

            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')

    def next_weekday_noninclusive(self, weekday: int, base_date: str | datetime.datetime | datetime.date = None, format: str = '%d.%m.%y'):
        """Дата ближайшего следующего дня недели, указанного как номер дня от 1 до 7, в формате 'dd.mm.yy'. Если искомый день недели совпадает с текущим, всё равно вернётся дата из следующей недели.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'dd.mm.yy'
        """
        if base_date is None:
            now = self.dmy_now()
            dmy = dtime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead <= 0:
                days_ahead += 7

            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')
        else:
            if isinstance(base_date, str) is True:
                date = dtime.strptime(base_date, format)
            elif isinstance(base_date, datetime.datetime | datetime.date):
                date = base_date
            else:
                raise TypeError("Некорректный тип даты")

            dmy = dtime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead <= 0:
                days_ahead += 7

            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')