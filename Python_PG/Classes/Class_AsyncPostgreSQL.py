import psycopg
from psycopg import Error, sql, rows

import contextlib
import re
import platform

import asyncio
import signal

from collections import namedtuple

class AsyncPostgreSQL():
    """
    Class for asynchronous connects to PostgreSQL databases and executing SQL scripts.
    Mandatory arguments:
        host (host address - str),
        port (port number - int),
        db (database name at PostgreSQL cluster - str),
        user (username at PostgreSQL cluster - str),
        password (user password at PostgreSQL cluster - str)
    Optional arguments:
        row_type (type of returned data; options - dict/namedtuple/tuple, str, default = 'tuple')
        retry_connect (will there be retries if first connection attempt would fail, bool, default = True),
        autocommit (bool, default = True)
    Supports plain string queries (not secure), and psycopg.sql.Composed type queries (made by psycopg.sql.SQL method, protected from SQL injection)
    """

    def __init__(self, host: str, port: int, db: str, user: str, password: str, row_type: str = 'tuple', retry_connect: bool = True, autocommit: bool = True) -> None:
        self.host = host
        self.port = str(port)
        self.db = db
        self.user = user
        self.password = password
        self.row_type = row_type
        self.retry_connect = retry_connect
        self.autocommit = autocommit



    async def db_connect(self):
        """Establishing connections with PostgreSQL server
           If retry_connect = True - there'll be two more attempts to connect after first failed attempt"""

        row_fact = None
        if self.row_type == 'dict':
            row_fact = rows.dict_row
        elif self.row_type == 'namedtuple':
            row_fact = rows.namedtuple_row
        else:
            row_fact = rows.tuple_row

        success = False
        attempts = 1

        while (self.retry_connect is True and success is False and attempts < 4) or (self.retry_connect is False and attempts == 1):
            if not success:
                try:
                    connection = await psycopg.AsyncConnection.connect (
                                                        host = self.host,
                                                        port = self.port,
                                                        dbname = self.db,
                                                        user = self.user,
                                                        password = self.password,
                                                        row_factory = row_fact,
                                                        autocommit = self.autocommit
                                                       )
                    msg = (
                           f"""{f"Attempt #{attempts}" if self.retry_connect is True else ''}\n"""
                           f"""Succesfully connected to PostgreSQL server at {self.host}:{self.port}/{self.db}@{self.user}"""
                          )
                    print(msg)
                    attempts += 1
                    success = True

                except (Exception, Error) as error:
                    msg = (
                           f"""{f"Attempt #{attempts}" if self.retry_connect is True else ''}\n"""
                           f"""Connection to PostgreSQL server at {self.host}:{self.port}/{self.db}@{self.user} was unsuccessful.\n"""
                           f"""Error {error}"""
                          )
                    print(msg)
                    attempts += 1
                    await asyncio.sleep(1)

        if success is True:
            loop = asyncio.get_event_loop()

            if platform.system() == 'Linux':
                loop.add_signal_handler(signal.SIGINT, connection.cancel) # ручное прерывание скрипта

            return connection
        else:
            return contextlib.nullcontext()



    #  SELECT

    async def fetch_headers(self, query):
        """Returns columns' headers"""

        async with await self.db_connect() as connection:
            if connection is not None:

                try:
                    async with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        await cursor.execute(query_final)
                        col_str = str(cursor.description)
                        return re.findall("""'(.*?)'""", col_str)

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except KeyError as error:
                    return f"Dictionary row factory can't be applied to the result.\nChange row_type in connection settings to None/'tuple'"

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)



    async def fetch_one(self, query):
        """Returns first cell from SELECT query result"""

        async with await self.db_connect() as connection:
            if connection is not None:

                try:
                    async with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        await cursor.execute(query_final)
                        result = await cursor.fetchone()

                        if result is None:
                            return None
                        else:
                            if self.row_type == 'dict':
                                result_nt = namedtuple('result_nt', result.keys())(*result.values())
                                return result_nt[0]
                            else:
                                return result[0]

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except KeyError as error:
                    return f"Dictionary row factory can't be applied to the result.\nChange row_type in connection settings to None/'tuple'"

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)



    async def fetch_first_row(self, query):
        """Returns first row from SELECT query result"""

        async with await self.db_connect() as connection:
            if connection is not None:

                try:
                    async with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        await cursor.execute(query_final)
                        return await cursor.fetchone()

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except KeyError as error:
                    return f"Dictionary row factory can't be applied to the result.\nChange row_type in connection settings to None/'tuple'"

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)



    async def fetch_many(self, query, n: int):
        """Returns first n rows from SELECT query result"""

        async with await self.db_connect() as connection:
            if connection is not None:

                try:
                    async with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        await cursor.execute(query_final)
                        return await cursor.fetchmany(n)

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except KeyError as error:
                    return f"Dictionary row factory can't be applied to the result.\nChange row_type in connection settings to None/'tuple'"

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)



    async def fetch_all(self, query):
        """Returns all rows from SELECT query result"""

        async with await self.db_connect() as connection:
            if connection is not None:

                try:
                    async with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        await cursor.execute(query_final)
                        return await cursor.fetchall()

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except KeyError as error:
                    return f"Dictionary row factory can't be applied to the result.\nChange row_type in connection settings to None/'tuple'"

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)



    # Команды без методов fetch

    async def execute(self, query):
        """Executes one query"""

        async with await self.db_connect() as connection:
            if connection is not None:
                try:

                    async with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        await cursor.execute(query_final)
                        return cursor.statusmessage

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)



    async def execute_multiple(self, queries: list):
        """Executes multiple queries (from the input list) in single transaction"""

        async with await self.db_connect() as connection:
            if connection is not None:
                query_number = 0

                async with connection.cursor() as cursor:

                    # Looping through query list
                    for query in queries:
                        try:

                            if isinstance(query, sql.Composed) is True:
                                query_final = query.as_string(cursor)
                            elif isinstance(query, str) is True:
                                query_final = query
                            else:
                                raise ValueError(
                                                 f"""Invalid query #{queries.index(query) + 1}.\n"""
                                                 f"""Query text:\n{query}"""
                                                )

                            await cursor.execute(query_final)
                            msg = (
                                   f"""Query #{queries.index(query) + 1} succesfully executed.\n"""
                                   f"""Query text:\n{query_final}"""
                                  )
                            print(msg)
                            query_number += 1
                            continue

                        except psycopg.errors.QueryCanceled as error:
                            await connection.rollback()
                            msg = (
                                    f"""Transaction #{queries.index(query)+1} have been cancelled due to timeout or user request.\n"""
                                    f"""{error.diag.sqlstate}: {error}\n"""
                                    f"""Query text:\n{query_final}"""
                                  )
                            return msg

                        except (Exception, Error) as error:
                            await connection.rollback()
                            msg = (
                                    f"""Query #{queries.index(query)+1} - error {error.diag.sqlstate}: {error}\n"""
                                    f"""Query text:\n{query_final}"""
                                  )
                            return msg
                    else:
                        query_number

                    if query_number == len(queries):
                        msg = "All queries has been executed.\nConnection has been closed"
                        return msg


    async def insert_batch(self, table: str, values_names: list = [], data_list: list = [], schema: str = '', drop_temp_table: bool = True):
        """Method for batch insert via COPY command
           Like a COPY, works only with SQL literals, not SQL functions
           For simple small inserts use execute() function instead

           Arguments:
           - table - target table name (without schema)
           - values_names - list of columns in target table
           - data_list - list of m tuples, each with n elements (values for insertion), where m - number of rows, n - number of columns
           - schema - schema name for target table (optional)
           - drop_temp_table - boolean; if False then intermediate unlogged table won't be dropped after insert to target table
        """

        # Validating inputs
        if values_names is None or data_list is None:
            raise ValueError("""No values specified or no data to insert""")

        column_cnt = len(values_names)

        for i in range(len(data_list)):
            if column_cnt != len(data_list[i]):
                raise ValueError("""Amount of columns in data list is not equal to amount of updated columns""")
            else:
                continue
        else:
            pass

        async with await self.db_connect() as connection:
            if connection is not None:
                try:
                    async with connection.cursor() as cursor:

                        # Preparing queries
                        temp_drop_query = sql.SQL("""drop table if exists {}""").format(sql.Identifier(schema, f'{table}_temp_insert') if schema != '' else sql.Identifier(f'{table}_temp_insert'))

                        temp_query = sql.SQL("""create unlogged table {temp} as select {col} from {t} limit 0"""
                                            ).format(temp = sql.Identifier(schema, f'{table}_temp_insert') if schema != '' else sql.Identifier(f'{table}_temp_insert'),
                                                     col = sql.SQL(', ').join(sql.Identifier(n) for n in values_names),
                                                     t = sql.Identifier(schema, table) if schema != '' else sql.Identifier(table)
                                                     )

                        insert_query = sql.SQL("""insert into {t} ({col}) select * from {temp}"""
                                              ).format(
                                                       t = sql.Identifier(schema, table) if schema != '' else sql.Identifier(table),
                                                       col = sql.SQL(', ').join(sql.Identifier(n) for n in values_names),
                                                       temp = sql.Identifier(schema, f'{table}_temp_insert') if schema != '' else sql.Identifier(f'{table}_temp_insert')
                                                      )

                        copy_query = sql.SQL("""copy {} from stdin""").format(sql.Identifier(schema, f'{table}_temp_insert') if schema != '' else sql.Identifier(f'{table}_temp_insert'))

                        # Creating intermediate unlogged table
                        await cursor.execute(temp_drop_query.as_string(cursor))
                        await cursor.execute(temp_query.as_string(cursor))

                        # Unloading data into intermediate table
                        async with cursor.copy(copy_query.as_string(cursor)) as copy:
                            for row in data_list:
                                await copy.write_row(row)

                        # Inserting into target table
                        await cursor.execute(insert_query.as_string(cursor))

                        # Dropping intermediate table
                        await cursor.execute(temp_drop_query.as_string(cursor))
                        return True

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)