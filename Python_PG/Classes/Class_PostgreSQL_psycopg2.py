import psycopg2
import psycopg2.extras as extr
from psycopg2 import Error, errorcodes, extensions, sql

import time
import pendulum

from .Class_CSV_logger import pg_csv_logger

import re
import platform
from pathlib import Path
from collections import namedtuple



# CSV-logger presets
if platform.system() == 'Linux':
    csv_path = f'{Path(__file__).resolve().parent}/logs/PG_connect_log.csv'
else:
    csv_path = f'{Path(__file__).resolve().parent}\\logs\\PG_connect_log.csv'

csv_logger = pg_csv_logger(path = csv_path)
driver = 'psycopg2'

# Flattening strings for CSV-log
def str_flatten(string: str):
    trans = {'\n': ' ', '\t': '', '    ': ''}
    res = re.sub('({})'.format('|'.join(map(re.escape, trans.keys()))), lambda m: trans[m.group()], string)
    return res

# Current timestamp string
def pdm_now():
    return pendulum.now(tz = "UTC").format('YYYY-MM-DD HH:mm:ss.SSSSSS')



class PostgreSQL():
    """
    Class for synchronous connects to PostgreSQL databases and executing SQL scripts.
    Mandatory arguments:
        host (host address - str),
        port (port number - int),
        db (database name at PostgreSQL cluster - str),
        user (username at PostgreSQL cluster - str),
        password (user password at PostgreSQL cluster - str)
    Optional arguments:
        row_type (type of returned data; options - dict/namedtuple/tuple, str, default = 'tuple')
        retry_connect (will there be retries if first connection attempt would fail, bool, default = True),
        autocommit (bool, default = True)
    Supports plain string queries (not secure), and psycopg.sql.Composed type queries (made by psycopg.sql.SQL method, protected from SQL injection)
    """

    def __init__(self, host: str, port: int, db: str, user: str, password: str, row_type: str = 'tuple', retry_connect: bool = True, autocommit: bool = True) -> None:
        self.host = host
        self.port = str(port)
        self.db = db
        self.user = user
        self.password = password
        self.row_type = row_type
        self.retry_connect = retry_connect
        self.autocommit = autocommit

    def db_connect(self):
        """Establishing connections with PostgreSQL server
           If retry_connect = True - there'll be two more attempts to connect after first failed attempt"""

        row_fact = None
        if self.row_type == 'dict':
            row_fact = extr.RealDictCursor
        elif self.row_type == 'namedtuple':
            row_fact = extr.NamedTupleCursor

        success = False
        attempts = 1

        while (self.retry_connect is True and success is False and attempts < 4) or (self.retry_connect is False and attempts == 1):
            try:
                conn_start_str = pdm_now()
                connection = psycopg2.connect(
                                                host = self.host,
                                                port = self.port,
                                                dbname = self.db,
                                                user = self.user,
                                                password = self.password,
                                                cursor_factory = row_fact,
                                                )
                connection.set_session(autocommit = self.autocommit)
                msg = f"""{f"Attempt #{attempts}. " if self.retry_connect is True else ''}Succesfully connected to PostgreSQL"""
                log_data = [[conn_start_str, driver, self.host, self.port, self.db, self.user, None, None, msg, None]]
                csv_logger.info(log_data)
                attempts += 1
                success = True

            except (Exception, Error) as error:
                msg = f"""{f"Attempt #{attempts}. " if self.retry_connect is True else ''}Connection to PostgreSQL server was unsuccessful"""
                err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, None, None, msg, err_code, str_flatten(str(error))]]
                csv_logger.error(log_data)
                attempts += 1
                if self.retry_connect is True and success is False and attempts < 4:
                    time.sleep(1)
                    continue
                else:
                    raise error

        return connection



    #  SELECT

    def fetch_headers(self, query):
        """Returns columns' headers"""

        with self.db_connect() as connection:
            if connection is not None:

                try:
                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            query_final = str(query)
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = pendulum.now(tz = "UTC")
                        cursor.execute(query_final)
                        query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()

                        msg = f"Query executed in {query_runtime}"
                        log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), str(query_runtime), msg, None]]
                        csv_logger.info(log_data)

                        col_str = str(cursor.description)
                        return re.findall("""'(.*?)'""", col_str)

                except extensions.QueryCanceledError as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                except KeyError as error:
                    msg = f"Dictionary row factory can't be applied to the result. Change row_type in connection settings to None/'tuple'"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, None]]
                    csv_logger.error(log_data)
                    return None

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                finally:
                    msg = "Connection has been closed"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, None, None, msg, None]]
                    csv_logger.info(log_data)



    def fetch_one(self, query):
        """Returns first cell from SELECT query result"""

        with self.db_connect() as connection:
            if connection is not None:

                try:
                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            query_final = str(query)
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = pendulum.now(tz = "UTC")
                        cursor.execute(query_final)
                        query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()

                        msg = f"Query executed in {query_runtime}"
                        log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), str(query_runtime), msg, None]]
                        csv_logger.info(log_data)

                        result = cursor.fetchone()

                        if result is None:
                            return None
                        else:
                            if self.row_type == 'dict':
                                result_nt = namedtuple('result_nt', result.keys())(*result.values())
                                return result_nt[0]
                            else:
                                return result[0]

                except extensions.QueryCanceledError as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                except KeyError as error:
                    msg = f"Dictionary row factory can't be applied to the result. Change row_type in connection settings to None/'tuple'"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, None]]
                    csv_logger.error(log_data)
                    return None

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                finally:
                    msg = "Connection has been closed"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, None, None, msg, None]]
                    csv_logger.info(log_data)



    def fetch_first_row(self, query):
        """Returns first row from SELECT query result"""

        with self.db_connect() as connection:
            if connection is not None:

                try:
                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            query_final = str(query)
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = pendulum.now(tz = "UTC")
                        cursor.execute(query_final)
                        query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()

                        msg = f"Query executed in {query_runtime}"
                        log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), str(query_runtime), msg, None]]
                        csv_logger.info(log_data)

                        return cursor.fetchone()

                except extensions.QueryCanceledError as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                except KeyError as error:
                    msg = f"Dictionary row factory can't be applied to the result. Change row_type in connection settings to None/'tuple'"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, None]]
                    csv_logger.error(log_data)
                    return None

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                finally:
                    msg = "Connection has been closed"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, None, None, msg, None]]
                    csv_logger.info(log_data)



    def fetch_many(self, query, n: int):
        """Returns first n rows from SELECT query result"""

        with self.db_connect() as connection:
            if connection is not None:

                try:
                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            query_final = str(query)
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = pendulum.now(tz = "UTC")
                        cursor.execute(query_final)
                        query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()

                        msg = f"Query executed in {query_runtime}"
                        log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), str(query_runtime), msg, None]]
                        csv_logger.info(log_data)

                        return cursor.fetchmany(n)

                except extensions.QueryCanceledError as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                except KeyError as error:
                    msg = f"Dictionary row factory can't be applied to the result. Change row_type in connection settings to None/'tuple'"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, None]]
                    csv_logger.error(log_data)
                    return None

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                finally:
                    msg = "Connection has been closed"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, None, None, msg, None]]
                    csv_logger.info(log_data)



    def fetch_all(self, query):
        """Returns all rows from SELECT query result"""

        with self.db_connect() as connection:
            if connection is not None:

                try:
                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            query_final = str(query)
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = pendulum.now(tz = "UTC")
                        cursor.execute(query_final)
                        query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()

                        msg = f"Query executed in {query_runtime}"
                        log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), str(query_runtime), msg, None]]
                        csv_logger.info(log_data)

                        return cursor.fetchall()

                except extensions.QueryCanceledError as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                except KeyError as error:
                    msg = f"Dictionary row factory can't be applied to the result. Change row_type in connection settings to None/'tuple'"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, None]]
                    csv_logger.error(log_data)
                    return None

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                finally:
                    msg = "Connection has been closed"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, None, None, msg, None]]
                    csv_logger.info(log_data)



    # Commands without fetch methods

    def copy_expert(self, query, file, buffer_size: int = 8192):
        """Method for executing COPY command

           Arguments:
           - query - string or psycopg.sql.Composed object with a SQL COPY command to be executed
           - file - path to a source / target file, must be eligible for open() function
           - buffer_size - size of a data batch in bytes
        """

        with self.db_connect() as connection:
            if connection is not None:
                try:

                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            query_final = str(query)
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = pendulum.now(tz = "UTC")
                        cursor.copy_expert(query_final, file, buffer_size)
                        query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()

                        msg = f"Query executed in {query_runtime}"
                        log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), str(query_runtime), msg, None]]
                        csv_logger.info(log_data)

                        return 'OK'

                except extensions.QueryCanceledError as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return False

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "PG Error"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return False

                finally:
                    msg = "Connection has been closed"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, None, None, msg, None]]
                    csv_logger.info(log_data)



    def execute(self, query):
        """Executes one query"""

        with self.db_connect() as connection:
            if connection is not None:
                try:

                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            query_final = str(query)
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = pendulum.now(tz = "UTC")
                        cursor.execute(query_final)
                        query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()

                        msg = f"Query executed in {query_runtime}"
                        log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), str(query_runtime), msg, None]]
                        csv_logger.info(log_data)

                        return cursor.statusmessage

                except extensions.QueryCanceledError as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return False

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return False

                finally:
                    msg = "Connection has been closed"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, None, None, msg, None]]
                    csv_logger.info(log_data)



    def execute_multiple(self, queries: list):
        """Executes multiple queries (from the input list) in single transaction"""

        with self.db_connect() as connection:
            if connection is not None:
                query_number = 0

                with connection.cursor() as cursor:

                    # Looping through query list
                    for query in queries:
                        try:

                            if isinstance(query, sql.Composed) is True:
                                query_final = query.as_string(cursor)
                            elif isinstance(query, str) is True:
                                query_final = query
                            else:
                                raise ValueError(f"""Invalid query #{queries.index(query) + 1}.\nQuery text:\n{query_final}""")

                            query_start = pendulum.now(tz = "UTC")
                            cursor.execute(query_final)
                            query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()

                            msg = f"""Query #{queries.index(query) + 1} succesfully executed in {query_runtime}"""
                            log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), str(query_runtime), msg, None]]
                            csv_logger.info(log_data)
                            query_number += 1
                            continue

                        except extensions.QueryCanceledError as error:
                            connection.rollback()
                            msg = f"Transaction #{queries.index(query) + 1} have been cancelled due to timeout or user request"
                            err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                            log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                            csv_logger.error(log_data)
                            return False

                        except (Exception, Error) as error:
                            connection.rollback()
                            msg = f"Query #{queries.index(query) + 1} - other PG Error"
                            err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                            log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                            csv_logger.error(log_data)
                            return False

                    else:
                        query_number

                    if query_number == len(queries):
                        msg = "All queries has been executed"
                        log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, None, None, msg, None]]
                        csv_logger.info(log_data)

                    msg = "Connection has been closed"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, None, None, msg, None]]
                    csv_logger.info(log_data)
                    return True



    def execute_special(self, query):
        """Executes special commands such as VACUUM, ANALYZE, CREATE и др.
           Works only with autocommit = true, only without context managers, only in separate transaction"""

        connection = self.db_connect()
        if connection is not None:
            try:

                with connection.cursor() as cursor:

                    if isinstance(query, sql.Composed) is True:
                        query_final = query.as_string(cursor)
                    elif isinstance(query, str) is True:
                        query_final = query
                    else:
                        query_final = str(query)
                        raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                    query_start = pendulum.now(tz = "UTC")
                    cursor.execute(query_final)
                    query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()

                    msg = f"Query executed in {query_runtime}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), str(query_runtime), msg, None]]
                    csv_logger.info(log_data)

                    return cursor.statusmessage

            except extensions.QueryCanceledError as error:
                connection.rollback()
                msg = "Transaction have been cancelled due to timeout or user request"
                err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                csv_logger.error(log_data)
                return False

            except (Exception, Error) as error:
                connection.rollback()
                msg = "Other PG Error"
                err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), None, msg, err_code, str_flatten(str(error))]]
                csv_logger.error(log_data)
                return False

            finally:
                connection.close()
                msg = "Connection has been closed"
                log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, None, None, msg, None]]
                csv_logger.info(log_data)



    def insert_batch(self, table: str, values_names: list = [], data_list: list = [], schema: str = '', drop_temp_table: bool = True):
        """Method for batch insert via COPY command
           Like a COPY, works only with SQL literals, not SQL functions
           For simple small inserts use execute() function instead

           Arguments:
           - table - target table name (without schema)
           - values_names - list of columns in target table
           - data_list - list of m tuples, each with n elements (values for insertion), where m - number of rows, n - number of columns
           - schema - schema name for target table (optional)
           - drop_temp_table - boolean; if False then intermediate unlogged table won't be dropped after insert to target table
        """

        # Validating inputs
        if values_names is None or data_list is None:
            raise ValueError("""No values specified or no data to insert""")

        column_cnt = len(values_names)

        for i in range(len(data_list)):
            if column_cnt != len(data_list[i]):
                raise ValueError("""Amount of columns in data list is not equal to amount of updated columns""")
            else:
                continue
        else:
            pass

        with self.db_connect() as connection:
            if connection is not None:
                try:
                    with connection.cursor() as cursor:

                        # Preparing queries
                        temp_drop_query = sql.SQL("""drop table if exists {}""").format(sql.Identifier(schema, f'{table}_temp_insert') if schema != '' else sql.Identifier(f'{table}_temp_insert'))

                        temp_query = sql.SQL("""create unlogged table {temp} as select {col} from {t} limit 0"""
                                            ).format(temp = sql.Identifier(schema, f'{table}_temp_insert') if schema != '' else sql.Identifier(f'{table}_temp_insert'),
                                                     col = sql.SQL(', ').join(sql.Identifier(n) for n in values_names),
                                                     t = sql.Identifier(schema, table) if schema != '' else sql.Identifier(table)
                                                     )

                        insert_query = sql.SQL("""insert into {t} ({col}) select * from {temp}"""
                                              ).format(
                                                       t = sql.Identifier(schema, table) if schema != '' else sql.Identifier(table),
                                                       col = sql.SQL(', ').join(sql.Identifier(n) for n in values_names),
                                                       temp = sql.Identifier(schema, f'{table}_temp_insert') if schema != '' else sql.Identifier(f'{table}_temp_insert')
                                                      )
                        insert_query_str = str_flatten(insert_query.as_string(cursor))

                        copy_query = sql.SQL("""copy {} from stdin""").format(sql.Identifier(schema, f'{table}_temp_insert') if schema != '' else sql.Identifier(f'{table}_temp_insert'))

                        # Creating intermediate unlogged table
                        query_start = pendulum.now(tz = "UTC")
                        cursor.execute(temp_drop_query.as_string(cursor))
                        cursor.execute(temp_query.as_string(cursor))

                        # Unloading data into intermediate table
                        with cursor.copy(copy_query.as_string(cursor)) as copy:
                            for row in data_list:
                                copy.write_row(row)

                        # Inserting into target table
                        cursor.execute(insert_query_str)

                        # Dropping intermediate table
                        if drop_temp_table is True:
                            cursor.execute(temp_drop_query.as_string(cursor))

                        query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()
                        msg = f"Query executed in {query_runtime}"
                        log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, insert_query_str, str(query_runtime), msg, None]]
                        csv_logger.info(log_data)

                        return True

                except extensions.QueryCanceledError as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, insert_query_str, None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return False

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    err_code = f"{error.pgcode} {errorcodes.lookup(error.pgcode)}"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, insert_query_str, None, msg, err_code, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return False

                finally:
                    msg = "Connection has been closed"
                    log_data = [[pdm_now(), driver, self.host, self.port, self.db, self.user, None, None, msg, None]]
                    csv_logger.info(log_data)