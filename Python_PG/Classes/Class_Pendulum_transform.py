import pendulum

class Pendulum_transform():
    """Разнообразные функции для трансформации дат и времени"""

    def __init__(self) -> None:
        pass

# текущие timestamp:

    def dmy_us_now(self):
        """Текущее время --> datetime(год, месяц, день, час, минута, секунда)"""
        now = pendulum.now()
        return pendulum.datetime(now.year, now.month, now.day, now.hour, now.minute, now.second, now.microsecond).replace(tzinfo = None)

    def dmy_hms_now(self):
        """Текущее время --> datetime(год, месяц, день, час, минута, секунда)"""
        now = pendulum.now()
        return pendulum.datetime(now.year, now.month, now.day, now.hour, now.minute, now.second).replace(tzinfo = None)

    def dmy_now(self):
        """Текущее время --> datetime(год, месяц, день)"""
        now = pendulum.now()
        return pendulum.datetime(now.year, now.month, now.day).replace(tzinfo = None)

    def curr_date_dmy(self):
        """Текущая дата в формате 'DD.MM.YY'"""
        dmy = self.dmy_now()
        return dmy.format('DD.MM.YY')

    def curr_date_dmy_hms(self):
        """Текущая дата в формате 'DD.MM.YY HH:mm:ss'"""
        dmy = self.dmy_hms_now()
        return dmy.format('DD.MM.YY HH:mm:ss')

    def curr_date_us(self):
        """Текущая дата в формате 'DD.MM.YY HH:mm:ss.SSSSSS'"""
        dmy = self.dmy_us_now()
        return dmy.format('DD.MM.YY HH:mm:ss.SSSSSS')

    def curr_weekday_number(self):
        """Текущий день недели"""
        dmy = self.dmy_now()
        return dmy.isoweekday()

    def curr_week_number(self):
        """Номер текущей недели в году (isoyear)"""
        dmy = self.dmy_now()
        return dmy.isocalendar().week

# разбор определённой даты:

    def get_weekday_number(self, date_input: str | pendulum.DateTime | pendulum.Date = None, format: str = 'DD.MM.YY'):
        """Определение дня недели для конкретной даты в формате 'DD.MM.YY'"""
        if date_input is None:
            return self.curr_weekday_number()
        else:
            if isinstance(date_input, str) is True:
                dt = pendulum.from_format(date_input, format)
            elif isinstance(date_input, pendulum.DateTime | pendulum.Date):
                dt = date_input
            else:
                raise TypeError("Некорректный тип даты")
            return dt.isoweekday()

    def get_week_number(self, date_input: str | pendulum.DateTime | pendulum.Date = None, format: str = 'DD.MM.YY'):
        """Определение номера недели для конкретной даты в формате 'DD.MM.YY'"""
        if date_input is None:
            return self.curr_week_number()
        else:
            if isinstance(date_input, str) is True:
                dt = pendulum.from_format(date_input, format)
            elif isinstance(date_input, pendulum.DateTime | pendulum.Date):
                dt = date_input
            else:
                raise TypeError("Некорректный тип даты")
            return dt.isocalendar().week

# дата следующего/предыдущего дня недели:

    def next_weekday(self, weekday: int, base_date: str | pendulum.DateTime | pendulum.Date = None, format: str = 'DD.MM.YY'):
        """Дата ближайшего следующего дня недели, указанного как номер дня от 1 до 7, в формате 'DD.MM.YY'. Если искомый день недели совпадает с текущим, то вернётся сегодняшняя дата.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'DD.MM.YY'
        """
        if base_date is None:
            now = self.dmy_now()
            dmy = pendulum.datetime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead < 0:
                days_ahead += 7
            result = dmy + pendulum.duration(days = days_ahead)

            return result.format('DD.MM.YY')
        else:
            if isinstance(base_date, str) is True:
                date = pendulum.from_format(base_date, format)
            elif isinstance(base_date, pendulum.DateTime | pendulum.Date):
                date = base_date
            else:
                raise TypeError("Некорректный тип даты")

            dmy = pendulum.datetime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead < 0:
                days_ahead += 7

            result = dmy + pendulum.duration(days = days_ahead)
            return result.format('DD.MM.YY')

    def last_weekday(self, weekday: int, base_date: str | pendulum.DateTime | pendulum.Date = None, format: str = 'DD.MM.YY'):
        """Дата ближайшего предыдущего дня недели, указанного как номер дня от 1 до 7, в формате 'DD.MM.YY'. Если искомый день недели совпадает с текущим, то вернётся сегодняшняя дата.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'DD.MM.YY'
        """
        if base_date is None:
            now = self.dmy_now()
            dmy = pendulum.datetime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead > 0:
                days_ahead -= 7

            result = dmy + pendulum.duration(days = days_ahead)
            return result.format('DD.MM.YY')
        else:
            if isinstance(base_date, str) is True:
                date = pendulum.from_format(base_date, format)
            elif isinstance(base_date, pendulum.DateTime | pendulum.Date):
                date = base_date
            else:
                raise TypeError("Некорректный тип даты")

            dmy = pendulum.datetime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead > 0:
                days_ahead -= 7

            result = dmy + pendulum.duration(days = days_ahead)
            return result.format('DD.MM.YY')

    def last_weekday_noninclusive(self, weekday: int, base_date: str | pendulum.DateTime | pendulum.Date = None, format: str = 'DD.MM.YY'):
        """Дата ближайшего предыдущего дня недели, указанного как номер дня от 1 до 7, в формате 'DD.MM.YY'. Если искомый день недели совпадает с текущим, всё равно вернётся дата из следующей недели.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'DD.MM.YY'
        """
        if base_date is None:
            now = self.dmy_now()
            dmy = pendulum.datetime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead >= 0:
                days_ahead -= 7

            result = dmy + pendulum.duration(days = days_ahead)
            return result.format('DD.MM.YY')
        else:
            if isinstance(base_date, str) is True:
                date = pendulum.from_format(base_date, format)
            elif isinstance(base_date, pendulum.DateTime | pendulum.Date):
                date = base_date
            else:
                raise TypeError("Некорректный тип даты")

            dmy = pendulum.datetime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead >= 0:
                days_ahead -= 7

            result = dmy + pendulum.duration(days = days_ahead)
            return result.format('DD.MM.YY')

    def next_weekday_noninclusive(self, weekday: int, base_date: str | pendulum.DateTime | pendulum.Date = None, format: str = 'DD.MM.YY'):
        """Дата ближайшего следующего дня недели, указанного как номер дня от 1 до 7, в формате 'DD.MM.YY'. Если искомый день недели совпадает с текущим, всё равно вернётся дата из следующей недели.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'DD.MM.YY'
        """
        if base_date is None:
            now = self.dmy_now()
            dmy = pendulum.datetime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead <= 0:
                days_ahead += 7

            result = dmy + pendulum.duration(days = days_ahead)
            return result.format('DD.MM.YY')
        else:
            if isinstance(base_date, str) is True:
                date = pendulum.from_format(base_date, format)
            elif isinstance(base_date, pendulum.DateTime | pendulum.Date):
                date = base_date
            else:
                raise TypeError("Некорректный тип даты")

            dmy = pendulum.datetime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()

            if days_ahead <= 0:
                days_ahead += 7

            result = dmy + pendulum.duration(days = days_ahead)
            return result.format('DD.MM.YY')