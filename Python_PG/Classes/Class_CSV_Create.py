import csv

class CSV_Create():
    """
    Создание и запись CSV-файла.
    Входные аргументы:
        - csv_path - полный системный путь к файлу;
        - mode, encoding, newline - аргументы функции open();
        - все аргументы от csv.writer(), кроме dialect.
    Для записи CSV в одну папку со скриптом
        import platform
        from pathlib import Path
        if platform.system() == 'Linux':
            path = f'{Path(__file__).resolve().parent}/test_csv.csv'
        else:
            path = f'{Path(__file__).resolve().parent}\\test_csv.csv'
    """

    def __init__(self, csv_path: str, mode: str = 'a', encoding: str = 'UTF8', newline: str = '', delimiter: str = ',', doublequote: bool = True,
                 escapechar: str = None, lineterminator: str = '\r\n', quotechar: str = '"', quoting = csv.QUOTE_MINIMAL, skipinitialspace: bool = True, strict: bool = False):

        self.csv_file = open(file = csv_path, mode = mode, encoding = encoding, newline = newline)
        self.writer = csv.writer(self.csv_file, delimiter = delimiter, doublequote = doublequote, escapechar = escapechar, lineterminator = lineterminator,
                                 quotechar = quotechar, quoting = quoting, skipinitialspace = skipinitialspace, strict = strict)

    def truncate(self):
        """Очистка файла"""
        self.csv_file.truncate()

    def write_row(self, row: list):
        """Запись одной строки. Входной аргумент для записи данных - список [ , , ...]."""
        self.writer.writerow(row)

    def write_rows(self, rows: list):
        """Запись нескольких строк. Входной аргумент для записи данных - массив из построчных списков значений [ [ , , ...], [ , , ...], [ , , ...], ...]"""
        for row in rows:
            self.writer.writerow(row)

    def close(self):
        """Закрытие файла"""
        self.csv_file.close()

    def write_close(self, rows: list):
        """Запись и закрытие файла"""
        self.write_rows(rows)
        self.close()