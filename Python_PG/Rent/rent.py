"Визуализатор для ЖКХ-платежей"

# импорты
import sys
import tkinter
import configparser
import warnings
from pathlib import Path
from PyQt6.QtWidgets import QApplication

import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.cm as cm
import matplotlib

import pandas as pd
import numpy as np

from Classes.Class_PostgreSQL import PostgreSQL

# убиваем раздражающие алармы от pandas
warnings.simplefilter(action = 'ignore', category = FutureWarning)
warnings.simplefilter(action = 'ignore', category = UserWarning)

# подключение к PostgreSQL
CONFIG_FILE = f'{Path(__file__).resolve().parent}/Classes/config.ini'

config = configparser.ConfigParser(allow_no_value = True, empty_lines_in_values = True)
config.read(CONFIG_FILE, 'utf8')

db_pg = config['DB connection']

PG = PostgreSQL(db_pg['host'], db_pg['port'], db_pg['database'], db_pg['user'], db_pg['password'])
connect = PG.db_connect()

# ещё один раздражающий аларм
pd.set_option('mode.chained_assignment', None)

# формируем датафреймы
    # Датафрейм для линейных графиков по основным услугам
df_line = pd.read_sql("""select pay_month, pay_year, service, payment
                         from rent.v_rent
                         where is_common is false and service_group != 'УК'
                      """, connect
                     )

    # Датафрейм для столбчатой диаграммы по месяцам
df_bar = pd.read_sql("""select lpad(pay_month::text, 2, '0')||'.'||pay_year as pay_date, total
                        from rent.v_rent_by_month
                      """, connect
                     )

    # Датафрейм для столбчатых диаграмм со средними значениями
df_avg = pd.read_sql("""select service, pay_year, avg_payment, avg_volume
                        from rent.v_rent_avg_by_year
                        where is_common is false and service_group != 'УК'
                      """, connect
                     )


# оформление графиков
plt.style.use('seaborn-v0_8-whitegrid')
# col_list = [*mcolors.TABLEAU_COLORS.keys(), *mcolors.BASE_COLORS.keys(), *mcolors.CSS4_COLORS.keys()]
col_list = [*mcolors.BASE_COLORS.keys()]

# параметры монитора
def screen_params():
    """DPI монитора, ширина и высота монитора в пикселях"""
    # получаем DPI монитора
    app = QApplication(sys.argv)
    screen = app.screens()[0]
    dpi = screen.physicalDotsPerInch()
    app.quit()
    # получаем разрешение монитора
    root = tkinter.Tk()
    root.update_idletasks()
    root.attributes('-fullscreen', True)
    root.state('iconic')
    height = root.winfo_screenheight()
    width = root.winfo_screenwidth()
    root.destroy()
    return (dpi, width, height)

scr_dpi, scr_w, scr_h = screen_params()


# линейные графики для основных услуг
df_line = df_line.pivot(index = ['pay_month', 'pay_year'], columns = 'service', values = 'payment') # разворачиваем услуги в столбцы
df_line = df_line.reset_index().rename_axis(None, axis = 1)
df_line = df_line.replace({np.NaN: 0}) # заполняем NaN нулями
pay_date = []
for i in range(0, len(df_line)):
    pay_date.append(str(df_line.iloc[i]['pay_month']).replace('.0', '').zfill(2) + '.' + str(df_line.iloc[i]['pay_year'])[2:4])
df_line.insert(0, "pay_date", pay_date) # комбинированный столбец из месяца и даты
df_line = df_line.sort_values(['pay_year', 'pay_month']) # сортировка
df_line = df_line.drop(['pay_month', 'pay_year'], axis = 1) # удаление лишних столбцов
df_line.insert(0, "idx", range(1, len(df_line) + 1)) # новый отсортированный столбец
df_line = df_line.set_index('idx') # индексирование по новому индексу

plotted = df_line.columns.to_list()[1:] # список столбцов, по которым будут строиться графики
xt = df_line['pay_date'].tolist() # список подписей для значений на оси X (MM.YY)
idx_arr = df_line.index.to_numpy() # индекс в numpy-массив
xmin = idx_arr[0] # нижняя граница оси X
xmax = max(idx_arr) # верхняя граница оси X

fig, gr_line = plt.subplots(ncols = 2, nrows = 3, sharex = True, figsize = (scr_w/scr_dpi, 0.9 * scr_h/scr_dpi), dpi = scr_dpi) # рисунок 2х3
plt.subplots_adjust(bottom = 0.15) # приподнимает график со дна рисунка
gr_line = gr_line.flatten() # преобразование массив графика в одномерный массив
gr_cols = col_list[:len(plotted)] # список цветов для графиков
for m, _ in enumerate(gr_line):
    ymax = df_line[plotted[m]].max() # верхняя граница оси y
    df_line.plot(kind = 'line', y = plotted[m], color = gr_cols[m], ax = gr_line[m], xlim = (xmin, xmax), ylim = (0, ymax * 1.1), label = plotted[m], fontsize = 9) # график
    gr_line[m].locator_params(axis = 'x', nbins = len(xt)) # количество меток на оси X
    gr_line[m].locator_params(axis = 'y', nbins = 5) # количество меток на оси Y
    gr_line[m].set_xticks(ticks = idx_arr, labels = xt, rotation = 90, fontweight = 'medium') # подписи по оси X
    gr_line[m].set_yticklabels(labels = [int(i) for i in gr_line[m].get_yticks()], weight = 'medium') # подписи по оси Y
    gr_line[m].set_xlabel('Month', fontsize = 10, fontweight = 'demibold', labelpad = 8) # название оси X
    gr_line[m].set_ylabel('Payment', fontsize = 10, fontweight = 'demibold', labelpad = 8) # название оси Y
    gr_line[m].legend(loc = 'lower right', prop = {'weight': 'semibold'}) # легенда
    rows, cols = df_line[[plotted[m]]].shape
    for j in range(cols): # подпись для каждой точки на каждом графике значением из соответствующего столбца
        for i in range(rows):
            x_off, y_off = 0, 0
            # смещение аннотации по X и Y
            if i != 0:
                x_off, y_off = -10, 25
            else:
                x_off, y_off = 5, 10
            # аннотации
            gr_line[m].annotate(f"{df_line[[plotted[m]]].iloc[i, j]}", xycoords = ('data', 'data'), xy = (idx_arr[i], df_line[[plotted[m]]].iloc[i, j]),
                                color = 'k', fontweight = 'demibold', arrowprops = {"arrowstyle": "-|>"}, textcoords = ('offset points', 'offset points'),
                                xytext = (x_off, y_off), fontsize = 6
                               )


# столбчатая диаграмма по месяцам
xt_bar = df_bar['pay_date'].tolist() # список подписей для значений на оси X (MM.YY)
idx_arr_bar = df_bar.index.to_numpy()
ylim = max(df_bar['total'])
cbar_step = 500
cb_xt = range(0, int(round(ylim, -3)) + cbar_step, cbar_step) # подписи к цветовой шкале
colormap = matplotlib.colormaps['rainbow'] # цветовая карта для шкалы и графика
color_arr = []
for i in range(len(df_bar['total'])):
    color_arr.append(colormap(df_bar['total'].iloc[i] / ylim)) # выбор цвета в зависимости от значения относительно максимального

fig, gr_bar = plt.subplots(figsize = (scr_w/scr_dpi, 0.9 * scr_h/scr_dpi), dpi = scr_dpi)
plt.subplots_adjust(bottom = 0.15) # приподнимает график со дна рисунка
df_bar.plot(kind = 'bar', y = 'total', ax = gr_bar, xlabel = 'Month', ylabel = 'Payment', label = 'Sum by month', color = color_arr, legend = False, fontsize = 9) # график
gr_bar.locator_params(nbins = len(xt_bar)) # количество меток на оси X
gr_bar.set_xticks(ticks = idx_arr_bar, labels = xt_bar, rotation = 90, fontweight = 'medium') # подписи по оси X
gr_bar.set_yticklabels(labels = gr_bar.get_yticks(), weight = 'medium') # подписи по оси Y
gr_bar.set_xlabel('Month', fontsize = 14, fontweight = 'demibold', labelpad = 8) # название оси X
gr_bar.set_ylabel('Payment', fontsize = 14, fontweight = 'demibold', labelpad = 8) # название оси Y
rows, cols = df_bar[['total']].shape
for j in range(cols): # подпись для каждой точки на каждом графике значением из соответствующего столбца
    for i in range(rows):
        # аннотации
        gr_bar.annotate(f"{df_bar[['total']].iloc[i, j]}", xycoords = ('data', 'data'), xy = (idx_arr_bar[i], df_bar[['total']].iloc[i, j]),
                        color = 'k', fontweight = 'demibold', textcoords = ('offset points', 'offset points'), xytext = (-15, 5), fontsize = 7)
sm = cm.ScalarMappable(cmap = colormap, norm = plt.Normalize(0, ylim)) # построение цветовой шкалы
sm.set_array([])
cbar = fig.colorbar(sm, ax = gr_bar, orientation = "vertical", ticks = cb_xt)
cbar.set_ticklabels(ticklabels = cbar.get_ticks(), size = 9, weight = 'medium')

# столбчатые диаграммы со средними значениями
    # сабфрейм со средними платежами
df_avg_pay = df_avg[['service', 'pay_year', 'avg_payment']]
df_avg_pay = df_avg_pay.pivot(index = ['pay_year'], columns = 'service', values = 'avg_payment')
df_avg_pay = df_avg_pay.reset_index().rename_axis(None, axis = 1)
df_avg_pay = df_avg_pay.replace({np.NaN: 0})
plotted_avg_pay = df_avg_pay.columns.to_list()[2:]
    # сабфрейм со средними объёмами
df_avg_vol = df_avg[['service', 'pay_year', 'avg_volume']]
df_avg_vol = df_avg_vol.pivot(index = ['pay_year'], columns = 'service', values = 'avg_volume')
df_avg_vol = df_avg_vol.reset_index().rename_axis(None, axis = 1)
df_avg_vol = df_avg_vol.replace({np.NaN: 0})
plotted_avg_vol = df_avg_vol.columns.to_list()[2:]

fig, gr_avg = plt.subplots(ncols = 1, nrows = 2, sharex = True, figsize = (scr_w/scr_dpi, 0.9 * scr_h/scr_dpi), dpi = scr_dpi) # рисунок 1х2
gr_avg = gr_avg.flatten() # преобразование массив графика в одномерный массив

df_avg_pay.plot(kind = 'bar', x = 'pay_year', y = plotted_avg_pay, logy = True, ax = gr_avg[0], colormap = 'tab10') # график средних платежей
gr_avg[0].tick_params(rotation = 0, labelsize = 12) # подписи по осям
gr_avg[0].set_xlabel('Years', fontsize = 14, fontweight = 'demibold', labelpad = 8) # название оси X
gr_avg[0].set_ylabel('Avg payment', fontsize = 14, fontweight = 'demibold', labelpad = 8) # название оси Y
gr_avg[0].legend(loc = 'upper left', prop = {'weight': 'medium', 'size': 8}) # легенда
for p in gr_avg[0].containers: # аннотации
    gr_avg[0].bar_label(p, fmt = '%.2f', label_type = 'edge', color = 'k', fontweight = 'demibold', fontsize = 7)

df_avg_vol.plot(kind = 'bar', x = 'pay_year', y = plotted_avg_vol, logy = True, ax = gr_avg[1], colormap = 'tab10') # график средних объёмов
gr_avg[1].tick_params(rotation = 0, labelsize = 12) # подписи по осям
gr_avg[1].set_xlabel('Years', fontsize = 14, fontweight = 'demibold', labelpad = 8) # название оси X
gr_avg[1].set_ylabel('Avg volume', fontsize = 14, fontweight = 'demibold', labelpad = 8) # название оси Y
gr_avg[1].legend(loc = 'upper left', prop = {'weight': 'medium', 'size': 8}) # легенда
for p in gr_avg[1].containers: # аннотации
    gr_avg[1].bar_label(p, fmt = '%.2f', label_type = 'edge', color = 'k', fontweight = 'demibold', fontsize = 7)


# Отображение графиков
plt.tight_layout()
plt.show()
