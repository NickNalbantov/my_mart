drop schema if exists rent cascade;

create schema rent;

comment on schema rent is 'ЖКХ';


-- Тарифы

    -- Квартира
create table rent.fares_flat
(
    service_id smallint generated always as identity primary key,
    service text,
    is_common bool,
    service_group text,
    fare numeric,
    unit text,
    upd_month smallint check (upd_month between 1 and 12),
    upd_year smallint check (upd_year between 2020 and 2100)
);

copy rent.fares_flat (service, is_common, service_group, fare, unit, upd_month, upd_year)
from '/tmp/fares_flat.csv'
(format csv, header);

create index fare_flat_group_idx on rent.fares_flat using btree (service_group);
create index fare_flat_upd_idx on rent.fares_flat using btree (upd_month, upd_year);

cluster rent.fares_flat using fares_flat_pkey;
analyze rent.fares_flat;

    -- Парковка
create table rent.fares_parking
(
    service_id smallint generated always as identity primary key,
    service text,
    is_common bool,
    service_group text,
    fare numeric,
    unit text,
    upd_month smallint check (upd_month between 1 and 12),
    upd_year smallint check (upd_year between 2023 and 2100)
);

copy rent.fares_parking (service, is_common, service_group, fare, unit, upd_month, upd_year)
from '/tmp/fares_parking.csv'
(format csv, header);

create index fare_parking_group_idx on rent.fares_parking using btree (service_group);
create index fare_parking_upd_idx on rent.fares_parking using btree (upd_month, upd_year);

cluster rent.fares_parking using fares_parking_pkey;
analyze rent.fares_parking;


-- Платежи;

    -- Квартира
create table rent.payments_flat
(
    payment_id smallint generated always as identity primary key,
    service_id smallint references rent.fares_flat (service_id),
    volume numeric,
    payment numeric,
    pay_month smallint check (pay_month between 1 and 12),
    pay_year smallint check (pay_year between 2020 and 2100)
);

copy rent.payments_flat (service_id, volume, payment, pay_month, pay_year)
from '/tmp/payments_flat.csv'
(format csv, header);

create index payment_flat_sid_idx on rent.payments_flat using btree (service_id);
create index payment_flat_date_idx on rent.payments_flat using btree (pay_month, pay_year);

cluster rent.payments_flat using payments_flat_pkey;
analyze rent.payments_flat;

    -- Парковка
create table rent.payments_parking
(
    payment_id smallint generated always as identity primary key,
    service_id smallint references rent.fares_parking (service_id),
    volume numeric,
    payment numeric,
    pay_month smallint check (pay_month between 1 and 12),
    pay_year smallint check (pay_year between 2023 and 2100)
);

copy rent.payments_parking (service_id, volume, payment, pay_month, pay_year)
from '/tmp/payments_parking.csv'
(format csv, header);

create index payment_parking_sid_idx on rent.payments_parking using btree (service_id);
create index payment_parking_date_idx on rent.payments_parking using btree (pay_month, pay_year);

cluster rent.payments_parking using payments_parking_pkey;
analyze rent.payments_parking;


-- Представления

    -- общий список платежей

create or replace view rent.v_rent_flat as
(
    select
        p.pay_month,
        p.pay_year,
        f.service,
        f.service_group,
        f.is_common,
        f.unit,
        p.volume,
        f.fare,
        p.payment,
        f.upd_month as fare_upd_month,
        f.upd_year as fare_upd_year
    from rent.payments_flat p
    join rent.fares_flat f using (service_id)
    order by
        p.pay_year,
        p.pay_month,
        f.is_common desc,
        f.service_group,
        f.service
);

create or replace view rent.v_rent_parking as
(
    select
        p.pay_month,
        p.pay_year,
        f.service,
        f.service_group,
        f.is_common,
        f.unit,
        p.volume,
        f.fare,
        p.payment,
        f.upd_month as fare_upd_month,
        f.upd_year as fare_upd_year
    from rent.payments_parking p
    join rent.fares_parking f using (service_id)
    order by
        p.pay_year,
        p.pay_month,
        f.is_common desc,
        f.service_group,
        f.service
);

create or replace view rent.v_rent as
(
    select *
    from
    (
        select
            'Квартира' as obj,
            *
        from rent.v_rent_flat

        union all

        select
            'Парковка' as obj,
            *
        from rent.v_rent_parking
    ) a
    order by
        pay_year,
        pay_month,
        obj,
        is_common desc,
        service_group,
        service
);

    -- общая сумма платежей по месяцам

create or replace view rent.v_rent_by_month_flat as
(
    select
        pay_month,
        pay_year,
        sum(payment) as total
    from rent.payments_flat
    group by
        pay_month,
        pay_year
    order by
        pay_year,
        pay_month
);

create or replace view rent.v_rent_by_month_parking as
(
    select
        pay_month,
        pay_year,
        sum(payment) as total
    from rent.payments_parking
    group by
        pay_month,
        pay_year
    order by
        pay_year,
        pay_month
);

create or replace view rent.v_rent_by_month as
(
    select *
    from
    (
        select
            'Квартира' as obj,
            *
        from rent.v_rent_by_month_flat

        union all

        select
            'Парковка' as obj,
            *
        from rent.v_rent_by_month_parking
    ) a
    order by
        pay_year,
        pay_month,
        obj
);


    -- сумма платежей по категориям и месяцам

create or replace view rent.v_rent_by_group_flat as
(
    select
        p.pay_month,
        p.pay_year,
        f.service_group,
        f.is_common,
        sum(payment) as total
    from rent.payments_flat p
    join rent.fares_flat f using (service_id)
    group by
        p.pay_month,
        p.pay_year,
        f.service_group,
        f.is_common
    order by
        p.pay_year,
        p.pay_month,
        f.is_common desc,
        f.service_group
);

create or replace view rent.v_rent_by_group_parking as
(
    select
        p.pay_month,
        p.pay_year,
        f.service_group,
        f.is_common,
        sum(payment) as total
    from rent.payments_parking p
    join rent.fares_parking f using (service_id)
    group by
        p.pay_month,
        p.pay_year,
        f.service_group,
        f.is_common
    order by
        p.pay_year,
        p.pay_month,
        f.is_common desc,
        f.service_group
);

create or replace view rent.v_rent_by_group as
(
    select *
    from
    (
        select
            'Квартира' as obj,
            *
        from rent.v_rent_by_group_flat

        union all

        select
            'Парковка' as obj,
            *
        from rent.v_rent_by_group_parking
    ) a
    order by
        pay_year,
        pay_month,
        obj,
        is_common desc,
        service_group
);


    -- средние платежи за услуги в пределах сроков действия тарифа;

create or replace view rent.v_rent_avg_by_fare_flat as
(
    select *
    from
    (
        select distinct
            f.service,
            f.service_group,
            f.is_common,
            round(avg(p.payment) over w1, 2)::numeric as avg_payment,
            (avg(p.volume) over w1)::numeric as avg_volume,
            f.fare,
            lpad(f.upd_month::text, 2, '0') || '.'|| f.upd_year as fare_start,
            lpad((case when f.fe_month > 1 then (f.fe_month - 1)::text else '12'::text end), 2, '0') || '.' || (case when f.fe_month > 1 then f.fe_year else f.fe_year -1 end) as fare_end
        from
        (
                select
                    service_id,
                    service,
                    service_group,
                    is_common,
                    fare,
                    upd_month,
                    upd_year,
                    coalesce(lead(upd_month) over w2, to_char(current_date, 'MM')::int2) as fe_month,
                    coalesce(lead(upd_year) over w2, to_char(current_date, 'YYYY')::int2) as fe_year
                from rent.fares_flat
                window w2 as (partition by service, is_common order by is_common desc, service, upd_year, upd_month)
        ) f
        join rent.payments_flat p using (service_id)
        window w1 as (partition by f.service_id)
    ) a
    order by
        is_common desc,
        service_group,
        service,
        substring(fare_start, '\d{4}$')::int2,
        substring(fare_start, '^\d{2}')::int2
);

create or replace view rent.v_rent_avg_by_fare_parking as
(
    select *
    from
    (
        select distinct
            f.service,
            f.service_group,
            f.is_common,
            round(avg(p.payment) over w1, 2)::numeric as avg_payment,
            (avg(p.volume) over w1)::numeric as avg_volume,
            f.fare,
            lpad(f.upd_month::text, 2, '0') || '.'|| f.upd_year as fare_start,
            lpad((case when f.fe_month > 1 then (f.fe_month - 1)::text else '12'::text end), 2, '0') || '.' || (case when f.fe_month > 1 then f.fe_year else f.fe_year -1 end) as fare_end
        from
        (
                select
                    service_id,
                    service,
                    service_group,
                    is_common,
                    fare,
                    upd_month,
                    upd_year,
                    coalesce(lead(upd_month) over w2, to_char(current_date, 'MM')::int2) as fe_month,
                    coalesce(lead(upd_year) over w2, to_char(current_date, 'YYYY')::int2) as fe_year
                from rent.fares_parking
                window w2 as (partition by service, is_common order by is_common desc, service, upd_year, upd_month)
        ) f
        join rent.payments_parking p using (service_id)
        window w1 as (partition by f.service_id)
    ) a
    order by
        is_common desc,
        service_group,
        service,
        substring(fare_start, '\d{4}$')::int2,
        substring(fare_start, '^\d{2}')::int2
);

create or replace view rent.v_rent_avg_by_fare as
(
    select *
    from
    (
        select
            'Квартира' as obj,
            *
        from rent.v_rent_avg_by_fare_flat

        union all

        select
            'Парковка' as obj,
            *
        from rent.v_rent_avg_by_fare_parking
    ) a
    order by
        obj,
        is_common desc,
        service_group,
        service,
        substring(fare_start, '\d{4}$')::int,
        substring(fare_start, '^\d{2}')::int
);


    -- средние платежи за услуги за календарный год

create or replace view rent.v_rent_avg_by_year_flat as
(
    select *
    from
    (
        select distinct
            f.service,
            f.service_group,
            f.is_common,
            p.pay_year,
            round(avg(p.payment) over w, 2)::numeric as avg_payment,
            (avg(p.volume) over w)::numeric as avg_volume
        from rent.payments_flat p
        join rent.fares_flat f using (service_id)
        window w as (partition by f.service, f.is_common, p.pay_year)

        union all

        select distinct
            'Отопление / 12 месяцев' as service,
            f.service_group,
            f.is_common,
            p.pay_year,
            round(((sum(p.payment) over w)::numeric / 12.0), 2)::numeric as avg_payment,
            ((avg(p.volume) over w)::numeric / 12.0)::numeric as avg_volume
        from rent.payments_flat p
        join rent.fares_flat f using (service_id)
        where f.service = 'Отопление'
        window w as (partition by f.service, f.is_common, p.pay_year)
    ) a
    order by
        pay_year,
        is_common desc,
        service_group,
        service
);

create or replace view rent.v_rent_avg_by_year_parking as
(
    select *
    from
    (
        select distinct
            f.service,
            f.service_group,
            f.is_common,
            p.pay_year,
            round(avg(p.payment) over w, 2)::numeric as avg_payment,
            (avg(p.volume) over w)::numeric as avg_volume
        from rent.payments_parking p
        join rent.fares_parking f using (service_id)
        window w as (partition by f.service, f.is_common, p.pay_year)

        union all

        select distinct
            'Отопление / 12 месяцев' as service,
            f.service_group,
            f.is_common,
            p.pay_year,
            round(((sum(p.payment) over w)::numeric / 12.0), 2)::numeric as avg_payment,
            ((avg(p.volume) over w)::numeric / 12.0)::numeric as avg_volume
        from rent.payments_parking p
        join rent.fares_parking f using (service_id)
        where f.service = 'Отопление'
        window w as (partition by f.service, f.is_common, p.pay_year)
    ) a
    order by
        pay_year,
        is_common desc,
        service_group,
        service
);

create or replace view rent.v_rent_avg_by_year as
(
    select *
    from
    (
        select
            'Квартира' as obj,
            *
        from rent.v_rent_avg_by_year_flat

        union all

        select
            'Парковка' as obj,
            *
        from rent.v_rent_avg_by_year_parking
    ) a
    order by
        obj,
        pay_year,
        is_common desc,
        service_group,
        service
);