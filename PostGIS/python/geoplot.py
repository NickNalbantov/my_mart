##### импорт основных библиотек
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm

##### убиваем раздражающие алармы от pandas и tensorflow
import warnings
warnings.simplefilter(action = 'ignore', category = FutureWarning)
warnings.simplefilter(action = 'ignore', category = UserWarning)
pd.set_option('mode.chained_assignment', None)

##### работа с геоданными
import geopandas as gpd

##### параметры монитора
import sys
import tkinter
from PyQt6.QtWidgets import QApplication

##### подключение к PostgreSQL
from Classes.Class_DB_config import DB_config
from Classes.Class_PostgreSQL import PostgreSQL
db_pg = DB_config.PG_connections()
PG = PostgreSQL(db_pg['host'], db_pg['port'], db_pg['db'], db_pg['user'], db_pg['password'])
connect = PG.db_connect()

# параметры монитора
def ScreenParams():
    """
    DPI монитора, ширина и высота монитора в пикселях
    """
    # получаем DPI монитора
    app = QApplication(sys.argv)
    screen = app.screens()[0]
    scr_dpi = screen.physicalDotsPerInch()
    app.quit()
    # получаем разрешение монитора
    root = tkinter.Tk()
    root.update_idletasks()
    root.attributes('-fullscreen', True)
    root.state('iconic')
    scr_h = root.winfo_screenheight()
    scr_w = root.winfo_screenwidth()
    root.destroy()
    return (scr_dpi, scr_w, scr_h)
scr_dpi, scr_w, scr_h = ScreenParams()

wgs84_sql = """
               select b.reg_id, b.reg_name, r.population, b.x_min, b.y_min, b.x_max, b.y_max, b.delta_lon, b.delta_lat, b.reg_geom
               from pgis_ru.reg_boundaries_wgs84 b
               join pgis_ru.regions r using (reg_id)
            """

gdf_wgs84 = gpd.GeoDataFrame.from_postgis(sql = wgs84_sql, con = connect, geom_col = 'reg_geom', crs = 'epsg:4326')

wgs84_east = gdf_wgs84[0:86].query('x_min != -180')
wgs84_west = gdf_wgs84[0:86].query('x_min == -180')

pop_lim = max(gdf_wgs84['population'])
cbar_step = 1000000
cb_xt = range(0, int(round(pop_lim, -6)), cbar_step) # подписи к цветовой шкале
colormap = plt.cm.get_cmap('hot') # цветовая карта для шкалы и графика
color_arr = []
for i in range(len(gdf_wgs84['population'])):
    color_arr.append(colormap(gdf_wgs84['population'].iloc[i] / pop_lim)) # выбор цвета в зависимости от значения относительно максимального

fig, ax = plt.subplots(figsize = (scr_w/scr_dpi, 0.9 * scr_h/scr_dpi), dpi = scr_dpi) # график разворачивается на 100% экрана по ширине и 90% по высоте
reg_east = wgs84_east.plot(kind = 'geo', column = 'reg', ax = ax, color = color_arr)
reg_west = wgs84_west.plot(kind = 'geo', column = 'population', ax = ax, color = color_arr)

sm = cm.ScalarMappable(cmap = colormap, norm = plt.Normalize(0, pop_lim)) # построение цветовой шкалы
sm.set_array([])
cax = fig.add_axes([ax.get_position().x1 + 0.01, ax.get_position().y0, 0.02, ax.get_position().height])
cbar = fig.colorbar(sm, ax = ax, cax = cax, orientation = "vertical", ticks = cb_xt)
cbar.set_ticklabels(ticklabels = cbar.get_ticks(), size = 9, weight = 'medium')

plt.show()
