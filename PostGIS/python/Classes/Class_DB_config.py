class DB_config():
    def PG_connections(db_name: str = 'postgres'):
        pg_con = {
                    "postgres": {
                                 "host": "localhost",
                                 "port": 5432,
                                 "db": "postgres",
                                 "user": "postgres",
                                 "password": "123",
                                },
                    # "omdb": {
                    #         "host": "localhost",
                    #         "port": 5432,
                    #         "db": "omdb",
                    #         "user": "postgres",
                    #         "password": "1q2w3e4R",
                    #         },
                    "is_schedule": {
                                    "host": "localhost",
                                    "port": 5432,
                                    "db": "is_schedule",
                                    "user": "postgres",
                                    "password": "123",
                                   },
                    "demo": {
                             "host": "localhost",
                             "port": 5432,
                             "db": "demo",
                             "user": "postgres",
                             "password": "123",
                            }
                }
        if db_name in pg_con.keys():
            return [pg_con[i] for i in [db_name]][0]
        else:
            raise ValueError('Unknown database alias')
