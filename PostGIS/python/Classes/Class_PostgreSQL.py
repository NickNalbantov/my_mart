import psycopg
from psycopg import Error, sql, rows

import time
from datetime import datetime as dtime
from .Class_DT_transform import DT_transform

from .Class_CSV_logger import pg_csv_logger

import contextlib
import re
import platform
from pathlib import Path


DT = DT_transform()

# настройки логгера
if platform.system() == 'Linux':
    csv_path = f'{Path(__file__).resolve().parent}/logs/PG_connect_log.csv'
else:
    csv_path = f'{Path(__file__).resolve().parent}\\logs\\PG_connect_log.csv'

csv_logger = pg_csv_logger(path = csv_path)
driver = 'psycopg3'

# Преобразование строк для CSV-лога
def str_flatten(string: str):
    trans = {'\n': ' ', '\t': '', '    ': ''}
    res = re.sub('({})'.format('|'.join(map(re.escape, trans.keys()))), lambda m: trans[m.group()], string)
    return res



class PostgreSQL():
    """
    Класс для синхронного подключения к БД PostgreSQL, выполнения SQL-команд и логирования операций в CSV-файл.
    Обязательные параметры:
        host (адрес хоста - str),
        port (номер порта - int),
        db (имя базы данных на сервере - str),
        user (имя пользователя - str), password (пароль пользователя - str)
    Необязательные параметры:
        row_type (выбор типа возвращаемого массива; возможные варианты - dict/namedtuple/tuple (дефолтное значение, если не указано иное), str, default = 'tuple')
        persistent_connect (повторные попытки подключения при попытке соединения, bool, default = True),
        autocommit (bool, default = True)
    Можно использовать как запросы типа string (имена таблиц, колонок и т.д. явно указаны в тексте запроса), так и параметрические запросы типа psycopg.sql.Composed (создаются методом psycopg.sql.SQL, обеспечивают защиту от SQL injection)
    Вставлять строковые переменные в запросы типа string НЕЛЬЗЯ - угроза SQL injection
    """

    def __init__(self, host: str, port: int, db: str, user: str, password: str, row_type: str = 'tuple', persistent_connect: bool = True, autocommit: bool = True) -> None:
        self.host = host
        self.port = str(port)
        self.db = db
        self.user = user
        self.password = password
        self.row_type = row_type
        self.persistent_connect = persistent_connect
        self.autocommit = autocommit

    def db_connect(self):
        """Установка соединения с сервером PostgreSQL
           Если persistent_connect = True - повторять попытки подключения ещё 2 раза в случае сбоя"""

        row_fact = None
        if self.row_type == 'dict':
            row_fact = rows.dict_row
        elif self.row_type == 'namedtuple':
            row_fact = rows.namedtuple_row
        else:
            row_fact = rows.tuple_row

        success = False
        attempts = 1

        while (self.persistent_connect is True and success is False and attempts < 4) or (self.persistent_connect is False and attempts == 1):
            if not success:
                try:
                    conn_start_str = DT.curr_date_us()
                    connection = psycopg.connect(
                                                    host = self.host,
                                                    port = self.port,
                                                    dbname = self.db,
                                                    user = self.user,
                                                    password = self.password,
                                                    row_factory = row_fact,
                                                    autocommit = self.autocommit
                                                )
                    msg = f"""{f"Attempt #{attempts}. " if self.persistent_connect is True else ''}Succesfully connected to PostgreSQL"""
                    log_data = [[conn_start_str, driver, self.host, self.port, self.db, self.user, None, msg, None]]
                    csv_logger.info(log_data)
                    attempts += 1
                    success = True

                except (Exception, Error) as error:
                    msg = f"""{f"Attempt #{attempts}. " if self.persistent_connect is True else ''}Connection to PostgreSQL server was unsuccessful"""

                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, None, msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    attempts += 1
                    time.sleep(1)

        if success is True:
            return connection
        else:
            return contextlib.nullcontext()



    #  SELECT

    def fetch_headers(self, query):
        """Возвращает заголовки столбцов"""

        with self.db_connect() as connection:
            if connection is not None:

                try:
                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = dtime.now().timestamp()
                        cursor.execute(query_final)
                        query_runtime = dtime.now().timestamp() - query_start

                        msg = f"Query executed in {query_runtime} seconds"
                        log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, None]]
                        csv_logger.info(log_data)

                        col_str = str(cursor.description)
                        return re.findall("""'(.*?)'""", col_str)

                except psycopg.errors.QueryCanceled as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                except KeyError as error:
                    msg = f"Dictionary row factory can't be applied to the result. Change row_type in connection settings to None/'tuple'"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, None]]
                    csv_logger.error(log_data)
                    return None

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                finally:
                    msg = "Connection has been closed"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, None, msg, None]]
                    csv_logger.info(log_data)



    def fetch_one(self, query):
        """Для выполнения запросов SELECT.
           Возвращает первую ячейку"""

        with self.db_connect() as connection:
            if connection is not None:

                try:
                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = dtime.now().timestamp()
                        cursor.execute(query_final)
                        query_runtime = dtime.now().timestamp() - query_start

                        msg = f"Query executed in {query_runtime} seconds"
                        log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, None]]
                        csv_logger.info(log_data)

                        return cursor.fetchone()[0]

                except psycopg.errors.QueryCanceled as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                except KeyError as error:
                    msg = f"Dictionary row factory can't be applied to the result. Change row_type in connection settings to None/'tuple'"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, None]]
                    csv_logger.error(log_data)
                    return None

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                finally:
                    msg = "Connection has been closed"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, None, msg, None]]
                    csv_logger.info(log_data)



    def fetch_first_row(self, query):
        """Для выполнения запросов SELECT.
           Возвращает первую строчку"""

        with self.db_connect() as connection:
            if connection is not None:

                try:
                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = dtime.now().timestamp()
                        cursor.execute(query_final)
                        query_runtime = dtime.now().timestamp() - query_start

                        msg = f"Query executed in {query_runtime} seconds"
                        log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, None]]
                        csv_logger.info(log_data)

                        return cursor.fetchone()

                except psycopg.errors.QueryCanceled as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                except KeyError as error:
                    msg = f"Dictionary row factory can't be applied to the result. Change row_type in connection settings to None/'tuple'"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, None]]
                    csv_logger.error(log_data)
                    return None

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                finally:
                    msg = "Connection has been closed"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, None, msg, None]]
                    csv_logger.info(log_data)



    def fetch_many(self, query, n: int):
        """Для выполнения запросов SELECT.
           Возвращает n строк"""

        with self.db_connect() as connection:
            if connection is not None:

                try:
                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = dtime.now().timestamp()
                        cursor.execute(query_final)
                        query_runtime = dtime.now().timestamp() - query_start

                        msg = f"Query executed in {query_runtime} seconds"
                        log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, None]]
                        csv_logger.info(log_data)

                        return cursor.fetchmany(n)

                except psycopg.errors.QueryCanceled as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                except KeyError as error:
                    msg = f"Dictionary row factory can't be applied to the result. Change row_type in connection settings to None/'tuple'"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, None]]
                    csv_logger.error(log_data)
                    return None

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                finally:
                    msg = "Connection has been closed"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, None, msg, None]]
                    csv_logger.info(log_data)



    def fetch_all(self, query):
        """Для выполнения запросов SELECT.
           Возвращает все строки"""

        with self.db_connect() as connection:
            if connection is not None:

                try:
                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = dtime.now().timestamp()
                        cursor.execute(query_final)
                        query_runtime = dtime.now().timestamp() - query_start

                        msg = f"Query executed in {query_runtime} seconds"
                        log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, None]]
                        csv_logger.info(log_data)

                        return cursor.fetchall()

                except psycopg.errors.QueryCanceled as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                except KeyError as error:
                    msg = f"Dictionary row factory can't be applied to the result. Change row_type in connection settings to None/'tuple'"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, None]]
                    csv_logger.error(log_data)
                    return None

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return None

                finally:
                    msg = "Connection has been closed"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, None, msg, None]]
                    csv_logger.info(log_data)



    # Команды без методов fetch

    def execute(self, query):
        """Выполняет один запрос"""

        with self.db_connect() as connection:
            if connection is not None:
                try:

                    with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = dtime.now().timestamp()
                        cursor.execute(query_final)
                        query_runtime = dtime.now().timestamp() - query_start

                        msg = f"Query executed in {query_runtime} seconds"
                        log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, None]]
                        csv_logger.info(log_data)

                        return cursor.statusmessage

                except psycopg.errors.QueryCanceled as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return False

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return False

                finally:
                    msg = "Connection has been closed"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, None, msg, None]]
                    csv_logger.info(log_data)



    def execute_multiple(self, queries: list):
        """Выполняет последовательно несколько запросов в одной транзакции"""

        with self.db_connect() as connection:
            if connection is not None:
                query_number = 0

                with connection.cursor() as cursor:

                    # последовательное выполнение запросов из списка
                    for query in queries:
                        try:

                            if isinstance(query, sql.Composed) is True:
                                query_final = query.as_string(cursor)
                            elif isinstance(query, str) is True:
                                query_final = query
                            else:
                                raise ValueError(f"""Invalid query #{queries.index(query) + 1}.\nQuery text:\n{query_final}""")

                            query_start = dtime.now().timestamp()
                            cursor.execute(query_final)
                            query_runtime = dtime.now().timestamp() - query_start

                            msg = f"""Query #{queries.index(query) + 1} succesfully executed in {query_runtime} seconds"""
                            log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, None]]
                            csv_logger.info(log_data)
                            query_number += 1
                            continue

                        except psycopg.errors.QueryCanceled as error:
                            connection.rollback()
                            msg = f"Transaction #{queries.index(query)+1} have been cancelled due to timeout or user request"
                            log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                            csv_logger.error(log_data)
                            return False

                        except (Exception, Error) as error:
                            connection.rollback()
                            msg = f"Query #{queries.index(query)+1} - other PG Error"
                            log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, str_flatten(query_final), msg, error.diag.sqlstate, str_flatten(str(error))]]
                            csv_logger.error(log_data)
                            return False

                    else:
                        query_number

                    if query_number == len(queries):
                        msg = "All queries has been executed"
                        log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, None, msg, None]]
                        csv_logger.info(log_data)

                    msg = "Connection has been closed"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, None, msg, None]]
                    csv_logger.info(log_data)
                    return True



    def insert_batch(self, table: str, values_names: list = [], data_list: list = [], schema: str = ''):
        """Для вставки большого количества строк.
           Работает только со вставкой литералов. Если требуется использовать функции в VALUES - надо использовать execute
           table - имя таблицы без имени схемы
           schema - имя схемы (опционально, если требуется)
           values_names - список с названиями колонок
           data_list - список из m кортежей длиной n со значениями для вставки, где m - число строк, n - число столбцов"""

        # предварительные проверки корректности входных данных
        if values_names is None or data_list is None:
            raise ValueError("""No values specified or no data to insert""")

        column_cnt = len(values_names)

        for i in range(len(data_list)):
            if column_cnt != len(data_list[i]):
                raise ValueError("""Amount of columns in data list is not equal to amount of updated columns""")
            else:
                continue
        else:
            pass

        with self.db_connect() as connection:
            if connection is not None:
                try:
                    with connection.cursor() as cursor:

                        # подготовка запросов
                        temp_drop_query = sql.SQL("""drop table if exists {}""").format(sql.Identifier(schema, 'temp_table_insert') if schema != '' else sql.Identifier('temp_table_insert'))

                        temp_query = sql.SQL("""create unlogged table {temp} as select {col} from {t} limit 0"""
                                            ).format(temp = sql.Identifier(schema, 'temp_table_insert') if schema != '' else sql.Identifier('temp_table_insert'),
                                                     col = sql.SQL(', ').join(sql.Identifier(n) for n in values_names),
                                                     t = sql.Identifier(schema, table) if schema != '' else sql.Identifier(table)
                                                     )

                        insert_query = sql.SQL("""insert into {t} ({col}) select * from {temp}"""
                                              ).format(
                                                       t = sql.Identifier(schema, table) if schema != '' else sql.Identifier(table),
                                                       col = sql.SQL(', ').join(sql.Identifier(n) for n in values_names),
                                                       temp = sql.Identifier(schema, 'temp_table_insert') if schema != '' else sql.Identifier('temp_table_insert')
                                                      )
                        insert_query_str = str_flatten(insert_query.as_string(cursor))

                        copy_query = sql.SQL("""copy {} from stdin""").format(sql.Identifier(schema, 'temp_table_insert') if schema != '' else sql.Identifier('temp_table_insert'))

                        # создаём пустую временную таблицу по формату вставки данных
                        query_start = dtime.now().timestamp()
                        cursor.execute(temp_drop_query.as_string(cursor))
                        cursor.execute(temp_query.as_string(cursor))

                        # вставляем данные из буфера во временную таблицу
                        with cursor.copy(copy_query.as_string(cursor)) as copy:
                            for row in data_list:
                                copy.write_row(row)

                        # делаем INSERT в целевую таблицу
                        cursor.execute(insert_query_str)

                        # удаляем временную таблицу
                        cursor.execute(temp_drop_query.as_string(cursor))

                        query_runtime = dtime.now().timestamp() - query_start
                        msg = f"Query executed in {query_runtime} seconds"
                        log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, insert_query_str, msg, None]]
                        csv_logger.info(log_data)

                        return True

                except psycopg.errors.QueryCanceled as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, insert_query_str, msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return False

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other PG Error"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, insert_query_str, msg, error.diag.sqlstate, str_flatten(str(error))]]
                    csv_logger.error(log_data)
                    return False

                finally:
                    msg = "Connection has been closed"
                    log_data = [[DT.curr_date_us(), driver, self.host, self.port, self.db, self.user, None, msg, None]]
                    csv_logger.info(log_data)