import psycopg
from psycopg import Error, sql, rows

import contextlib
import re
import platform

import asyncio
import signal

class AsyncPostgreSQL():
    """
    Класс для асинхронного подключения к БД PostgreSQL и выполнения SQL-команд.
    Обязательные параметры:
        host (адрес хоста - str),
        port (номер порта - int),
        db (имя базы данных на сервере - str),
        user (имя пользователя - str), password (пароль пользователя - str)
    Необязательные параметры:
        row_type (выбор типа возвращаемого массива; возможные варианты - dict/namedtuple/tuple (дефолтное значение, если не указано иное), str, default = 'tuple')
        persistent_connect (повторные попытки подключения при попытке соединения, bool, default = True),
        autocommit (bool, default = True)
    Можно использовать как запросы типа string (имена таблиц, колонок и т.д. явно указаны в тексте запроса), так и параметрические запросы типа psycopg.sql.Composed (создаются методом psycopg.sql.SQL, обеспечивают защиту от SQL injection)
    Вставлять строковые переменные в запросы типа string НЕЛЬЗЯ - угроза SQL injection
    """

    def __init__(self, host: str, port: int, db: str, user: str, password: str, row_type: str = 'tuple', persistent_connect: bool = True, autocommit: bool = True) -> None:
        self.host = host
        self.port = str(port)
        self.db = db
        self.user = user
        self.password = password
        self.row_type = row_type
        self.persistent_connect = persistent_connect
        self.autocommit = autocommit



    async def db_connect(self):
        """Установка соединения с сервером PostgreSQL
           Если persistent_connect = True - повторять попытки подключения ещё 2 раза в случае сбоя"""

        row_fact = None
        if self.row_type == 'dict':
            row_fact = rows.dict_row
        elif self.row_type == 'namedtuple':
            row_fact = rows.namedtuple_row
        else:
            row_fact = rows.tuple_row

        success = False
        attempts = 1

        while (self.persistent_connect is True and success is False and attempts < 4) or (self.persistent_connect is False and attempts == 1):
            if not success:
                try:
                    connection = await psycopg.AsyncConnection.connect (
                                                        host = self.host,
                                                        port = self.port,
                                                        dbname = self.db,
                                                        user = self.user,
                                                        password = self.password,
                                                        row_factory = row_fact,
                                                        autocommit = self.autocommit
                                                       )
                    msg = (
                           f"""{f"Attempt #{attempts}" if self.persistent_connect is True else ''}\n"""
                           f"""Succesfully connected to PostgreSQL server at {self.host}:{self.port}/{self.db}@{self.user}"""
                          )
                    print(msg)
                    attempts += 1
                    success = True

                except (Exception, Error) as error:
                    msg = (
                           f"""{f"Attempt #{attempts}" if self.persistent_connect is True else ''}\n"""
                           f"""Connection to PostgreSQL server at {self.host}:{self.port}/{self.db}@{self.user} was unsuccessful.\n"""
                           f"""Error {error}"""
                          )
                    print(msg)
                    attempts += 1
                    await asyncio.sleep(1)

        if success is True:
            loop = asyncio.get_event_loop()

            if platform.system() == 'Linux':
                loop.add_signal_handler(signal.SIGINT, connection.cancel) # ручное прерывание скрипта

            return connection
        else:
            return contextlib.nullcontext()



    #  SELECT

    async def fetch_headers(self, query):
        """Возвращает заголовки столбцов"""

        async with await self.db_connect() as connection:
            if connection is not None:

                try:
                    async with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        await cursor.execute(query_final)
                        col_str = str(cursor.description)
                        return re.findall("""'(.*?)'""", col_str)

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except KeyError as error:
                    return f"Dictionary row factory can't be applied to the result.\nChange row_type in connection settings to None/'tuple'"

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)



    async def fetch_one(self, query):
        """Для выполнения запросов SELECT.
           Возвращает первую ячейку"""

        async with await self.db_connect() as connection:
            if connection is not None:

                try:
                    async with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        await cursor.execute(query_final)
                        res = await cursor.fetchone()
                        return res[0]

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except KeyError as error:
                    return f"Dictionary row factory can't be applied to the result.\nChange row_type in connection settings to None/'tuple'"

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)



    async def fetch_first_row(self, query):
        """Для выполнения запросов SELECT.
           Возвращает первую строчку"""

        async with await self.db_connect() as connection:
            if connection is not None:

                try:
                    async with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        await cursor.execute(query_final)
                        return await cursor.fetchone()

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except KeyError as error:
                    return f"Dictionary row factory can't be applied to the result.\nChange row_type in connection settings to None/'tuple'"

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)



    async def fetch_many(self, query, n: int):
        """Для выполнения запросов SELECT.
           Возвращает n строк"""

        async with await self.db_connect() as connection:
            if connection is not None:

                try:
                    async with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        await cursor.execute(query_final)
                        return await cursor.fetchmany(n)

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except KeyError as error:
                    return f"Dictionary row factory can't be applied to the result.\nChange row_type in connection settings to None/'tuple'"

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)



    async def fetch_all(self, query):
        """Для выполнения запросов SELECT.
           Возвращает все строки"""

        async with await self.db_connect() as connection:
            if connection is not None:

                try:
                    async with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        await cursor.execute(query_final)
                        return await cursor.fetchall()

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except KeyError as error:
                    return f"Dictionary row factory can't be applied to the result.\nChange row_type in connection settings to None/'tuple'"

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)



    # Команды без методов fetch

    async def execute(self, query):
        """Выполняет один запрос"""

        async with await self.db_connect() as connection:
            if connection is not None:
                try:

                    async with connection.cursor() as cursor:

                        if isinstance(query, sql.Composed) is True:
                            query_final = query.as_string(cursor)
                        elif isinstance(query, str) is True:
                            query_final = query
                        else:
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        await cursor.execute(query_final)
                        return cursor.statusmessage

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)



    async def execute_multiple(self, queries: list):
        """Выполняет последовательно несколько запросов в одной транзакции"""

        async with await self.db_connect() as connection:
            if connection is not None:
                query_number = 0

                async with connection.cursor() as cursor:

                    # последовательное выполнение запросов из списка
                    for query in queries:
                        try:

                            if isinstance(query, sql.Composed) is True:
                                query_final = query.as_string(cursor)
                            elif isinstance(query, str) is True:
                                query_final = query
                            else:
                                raise ValueError(
                                                 f"""Invalid query #{queries.index(query) + 1}.\n"""
                                                 f"""Query text:\n{query}"""
                                                )

                            await cursor.execute(query_final)
                            msg = (
                                   f"""Query #{queries.index(query) + 1} succesfully executed.\n"""
                                   f"""Query text:\n{query_final}"""
                                  )
                            print(msg)
                            query_number += 1
                            continue

                        except psycopg.errors.QueryCanceled as error:
                            await connection.rollback()
                            msg = (
                                    f"""Transaction #{queries.index(query)+1} have been cancelled due to timeout or user request.\n"""
                                    f"""{error.diag.sqlstate}: {error}\n"""
                                    f"""Query text:\n{query_final}"""
                                  )
                            return msg

                        except (Exception, Error) as error:
                            await connection.rollback()
                            msg = (
                                    f"""Query #{queries.index(query)+1} - error {error.diag.sqlstate}: {error}\n"""
                                    f"""Query text:\n{query_final}"""
                                  )
                            return msg
                    else:
                        query_number

                    if query_number == len(queries):
                        msg = "All queries has been executed.\nConnection has been closed"
                        return msg


    async def insert_batch(self, table: str, values_names: list = [], data_list: list = [], schema: str = ''):
        """Для вставки большого количества строк.
           Работает только со вставкой литералов. Если требуется использовать функции в VALUES - надо использовать execute
           table - имя таблицы без имени схемы
           schema - имя схемы (опционально, если требуется)
           values_names - список с названиями колонок
           data_list - список из m кортежей длиной n со значениями для вставки, где m - число строк, n - число столбцов"""

        # предварительные проверки корректности входных данных
        if values_names is None or data_list is None:
            raise ValueError("""No values specified or no data to insert""")

        column_cnt = len(values_names)

        for i in range(len(data_list)):
            if column_cnt != len(data_list[i]):
                raise ValueError("""Amount of columns in data list is not equal to amount of updated columns""")
            else:
                continue
        else:
            pass

        async with await self.db_connect() as connection:
            if connection is not None:
                try:
                    async with connection.cursor() as cursor:

                        # подготовка запросов
                        temp_drop_query = sql.SQL("""drop table if exists {}""").format(sql.Identifier(schema, 'temp_table_insert') if schema != '' else sql.Identifier('temp_table_insert'))

                        temp_query = sql.SQL("""create unlogged table {temp} as select {col} from {t} limit 0"""
                                            ).format(temp = sql.Identifier(schema, 'temp_table_insert') if schema != '' else sql.Identifier('temp_table_insert'),
                                                     col = sql.SQL(', ').join(sql.Identifier(n) for n in values_names),
                                                     t = sql.Identifier(schema, table) if schema != '' else sql.Identifier(table)
                                                     )

                        insert_query = sql.SQL("""insert into {t} ({col}) select * from {temp}"""
                                              ).format(
                                                       t = sql.Identifier(schema, table) if schema != '' else sql.Identifier(table),
                                                       col = sql.SQL(', ').join(sql.Identifier(n) for n in values_names),
                                                       temp = sql.Identifier(schema, 'temp_table_insert') if schema != '' else sql.Identifier('temp_table_insert')
                                                      )

                        copy_query = sql.SQL("""copy {} from stdin""").format(sql.Identifier(schema, 'temp_table_insert') if schema != '' else sql.Identifier('temp_table_insert'))

                        # создаём пустую временную таблицу по формату вставки данных
                        await cursor.execute(temp_drop_query.as_string(cursor))
                        await cursor.execute(temp_query.as_string(cursor))

                        # вставляем данные из буфера во временную таблицу
                        async with cursor.copy(copy_query.as_string(cursor)) as copy:
                            for row in data_list:
                                await copy.write_row(row)

                        # делаем INSERT в целевую таблицу
                        await cursor.execute(insert_query.as_string(cursor))

                        # удаляем временную таблицу
                        await cursor.execute(temp_drop_query.as_string(cursor))
                        return True

                except psycopg.errors.QueryCanceled as error:
                    await connection.rollback()
                    msg = f"Transaction have been cancelled due to timeout or user request.\n{error.diag.sqlstate}: {error}"
                    return msg

                except (Exception, Error) as error:
                    await connection.rollback()
                    return f"{error.diag.sqlstate}: {error}"

                finally:
                    msg = "Connection closed"
                    print(msg)