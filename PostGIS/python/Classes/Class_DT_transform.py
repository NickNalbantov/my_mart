import datetime
from datetime import datetime as dtime

class DT_transform():
    """Разнообразные функции для трансформации дат и времени"""

    def __init__(self) -> None:
        pass

# текущие timestamp:

    def dmy_us_now(self):
        """Текущее время --> datetime(год, месяц, день, час, минута, секунда)"""
        now = dtime.now()
        return dtime(now.year, now.month, now.day, now.hour, now.minute, now.second, now.microsecond)

    def dmy_hms_now(self):
        """Текущее время --> datetime(год, месяц, день, час, минута, секунда)"""
        now = dtime.now()
        return dtime(now.year, now.month, now.day, now.hour, now.minute, now.second)

    def dmy_now(self):
        """Текущее время --> datetime(год, месяц, день)"""
        now = dtime.now()
        return dtime(now.year, now.month, now.day)

    def dm_now(self):
        """Текущее время --> datetime(год, месяц)"""
        now = dtime.now()
        return dtime(now.year, now.month, now.day)

    def curr_date_dmy(self):
        """Текущая дата в формате 'dd.mm.yy'"""
        dm = self.dmy_now()
        return dm.strftime('%d.%m.%y')

    def curr_date_dmy_hms(self):
        """Текущая дата в формате 'dd.mm.yy HH:MM:SS'"""
        dm = self.dmy_hms_now()
        return dm.strftime('%d.%m.%Y %H:%M:%S')

    def curr_date_us(self):
        """Текущая дата в формате 'dd.mm.yy HH:MM:SS.ffffff'"""
        dm = self.dmy_us_now()
        return dm.strftime('%d.%m.%Y %H:%M:%S.%f')

    def curr_weekday_number(self):
        """Текущий день недели"""
        dm = self.dmy_now()
        dmy = dtime(dm.year, dm.month, dm.day)
        return dm.isoweekday()

    def curr_week_number(self):
        """Текущий день недели"""
        dm = self.dmy_now()
        dmy = dtime(dm.year, dm.month, dm.day)
        return dm.isocalendar().week

# разбор определённой даты:

    def get_weekday_number(self, date_input: str):
        """Определение дня недели для конкретной даты в формате 'dd.mm.yy'"""
        dt = dtime.strptime(date_input, '%d.%m.%y')
        # dmy = dtime(dm.year, dm.month, dm.day)
        return dt.isoweekday()

    def get_week_number(self, date_input: str):
        """Определение номера недели для конкретной даты в формате 'dd.mm.yy'"""
        dt = dtime.strptime(date_input, '%d.%m.%y')
        return dt.isocalendar().week

# дата следующего/предыдущего дня недели:

    def next_weekday(self, weekday: int, base_date: str = ''):
        """Дата ближайшего следующего дня недели, указанного как номер дня от 1 до 7, в формате 'dd.mm.yy'. Если искомый день недели совпадает с текущим, то вернётся сегодняшняя дата.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'dd.mm.yy'
        """
        if base_date == '':
            now = self.dmy_now()
            dmy = dtime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()
            if days_ahead < 0:
                days_ahead += 7
            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')
        else:
            date = dtime.strptime(base_date, '%d.%m.%y')
            dmy = dtime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()
            if days_ahead < 0:
                days_ahead += 7
            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')

    def last_weekday(self, weekday: int, base_date: str = ''):
        """Дата ближайшего предыдущего дня недели, указанного как номер дня от 1 до 7, в формате 'dd.mm.yy'. Если искомый день недели совпадает с текущим, то вернётся сегодняшняя дата.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'dd.mm.yy'
        """
        if base_date == '':
            now = self.dmy_now()
            dmy = dtime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()
            if days_ahead > 0:
                days_ahead -= 7
            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')
        else:
            date = dtime.strptime(base_date, '%d.%m.%y')
            dmy = dtime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()
            if days_ahead > 0:
                days_ahead -= 7
            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')

    def next_weekday(self, weekday: int, base_date: str = ''):
        """Дата ближайшего следующего дня недели, указанного как номер дня от 1 до 7, в формате 'dd.mm.yy'. Если искомый день недели совпадает с текущим, то вернётся сегодняшняя дата.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'dd.mm.yy'
        """
        if base_date == '':
            now = self.dmy_now()
            dmy = dtime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()
            if days_ahead < 0:
                days_ahead += 7
            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')
        else:
            date = dtime.strptime(base_date, '%d.%m.%y')
            dmy = dtime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()
            if days_ahead < 0:
                days_ahead += 7
            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')

    def last_weekday_noninclusive(self, weekday: int, base_date: str = ''):
        """Дата ближайшего предыдущего дня недели, указанного как номер дня от 1 до 7, в формате 'dd.mm.yy'. Если искомый день недели совпадает с текущим, всё равно вернётся дата из следующей недели.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'dd.mm.yy'
        """
        if base_date == '':
            now = self.dmy_now()
            dmy = dtime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()
            if days_ahead >= 0:
                days_ahead -= 7
            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')
        else:
            date = dtime.strptime(base_date, '%d.%m.%y')
            dmy = dtime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()
            if days_ahead >= 0:
                days_ahead -= 7
            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')

    def next_weekday_noninclusive(self, weekday: int, base_date: str = ''):
        """Дата ближайшего следующего дня недели, указанного как номер дня от 1 до 7, в формате 'dd.mm.yy'. Если искомый день недели совпадает с текущим, всё равно вернётся дата из следующей недели.
        По умолчанию опорной датой является сегодняшний день. Если необходимо выполнить поиск от другой даты, надо указать опорную дату во втором опциональном аргумента в формате 'dd.mm.yy'
        """
        if base_date == '':
            now = self.dmy_now()
            dmy = dtime(now.year, now.month, now.day)
            days_ahead = weekday - dmy.isoweekday()
            if days_ahead <= 0:
                days_ahead += 7
            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')
        else:
            date = dtime.strptime(base_date, '%d.%m.%y')
            dmy = dtime(date.year, date.month, date.day)
            days_ahead = weekday - dmy.isoweekday()
            if days_ahead <= 0:
                days_ahead += 7
            result = dmy + datetime.timedelta(days_ahead)
            return result.strftime('%d.%m.%y')