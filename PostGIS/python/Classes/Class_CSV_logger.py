import logging
import platform
from pathlib import Path
from .Class_CSV_Create import CSV_Create


class LogCSVHandler(logging.Handler):
    """Класс для логирования в CSV-файл
       Данные для логирования передаются в log_data - список списков (строк CSV-файла)
    """
    def __init__(self, path: str):
        logging.Handler.__init__(self)
        self.path = path

    def emit(self, record):
        """Принимает сообщения из логгера и раскладывает их в строки CSV-файла"""
        self.format(record)

        # на вторую позицию вставляем уровень сообщения
        log_list = record.msg
        if isinstance(log_list[0], list):
            for i in range(len(record.msg)):
                log_list[i].insert(1, record.levelname)
        else:
            log_list.insert(1, record.levelname)

        # вставляем данные в CSV-лог
        csv = CSV_Create(self.path) # также поддерживает основные параметры open() и csv.writer(), см. Class_CSV_Create.py
        csv.write_close(log_list)

# сюда можно накидывать функции под конкретные реализации

if platform.system() == 'Linux':
    csv_path = f'{Path(__file__).resolve().parent}/logs/PG_connect_log.csv'
else:
    csv_path = f'{Path(__file__).resolve().parent}\\logs\\PG_connect_log.csv'

def pg_csv_logger(path: str = csv_path, log_level: str = 'DEBUG'):

    LogCSV = LogCSVHandler(path = path)

    log = logging.getLogger('PG_CSV_logger')
    log.addHandler(LogCSV)
    log.setLevel(log_level)
    log.propagate = False

    return log

# log_level = 'DEBUG'
# db_logger = pg_csv_logger(csv_path, log_level)

# log_data = [[11, False],[12, "тест", True]]
# db_logger.error(msg = log_data)
# log_data = [[13, False],[14, "тест", True]]
# db_logger.warning(msg = log_data)
# log_data = [[15, False],[16, "тест", True]]
# db_logger.critical(msg = log_data)
# log_data = [[17, False],[18, "тест", True]]
# db_logger.debug(msg = log_data)
# log_data = [[19, False],[20, "тест", True]]
# db_logger.info(msg = log_data)