import logging

from Class_PostgreSQL import PostgreSQL
from Class_DB_config import DB_config


class LogDBHandler(logging.Handler):
    """Класс для логирования в таблицы PG
       Необходимо указать псевдоним БД из класса Class_DB_config, имя схемы, целевой таблицы и столбцов в ней (необходимо, т.к. могут быть столбцы с constraint и дефолтным заполнением)
       Данные для логирования передаются в log_data - список кортежей (строк таблицы)
    """
    def __init__(self, db_alias: str, db_schema: str, db_log_table: str, db_log_table_columns: list, formatter, splitter: str = ' ~ '):
        logging.Handler.__init__(self)
        self.db_alias = db_alias
        self.db_schema = db_schema
        self.db_log_table = db_log_table
        self.db_log_table_columns = db_log_table_columns
        self.splitter = splitter
        self.formatter = formatter

    def emit(self, record):
        """Принимает сообщения из логгера (дефолтный разделитель ' ~ ', можно менять в аргументе splitter) и парсит их в список"""
        self.formatter.format(record)
        log_data_list = record.msg.split(self.splitter)
        log_data_list.append(record.levelname) # в конец таблицы всегда добавляем уровень сообщения и время
        log_data_list.append(record.asctime)
        log_data = [tuple(log_data_list)]
        # подключаемся к PG
        DB_PG = DB_config.PG_connections(self.db_alias)
        PG = PostgreSQL(DB_PG['host'], DB_PG['port'], DB_PG['db'], DB_PG['user'], DB_PG['password'])

        # вставляем данные в лог
        PG.insert_batch(table = self.db_log_table, values_names = self.db_log_table_columns, data_list = log_data, schema = self.db_schema)


# сюда можно накидывать функции под конкретные реализации

def is_bot_handler_logger(db_log_table: str, db_log_table_columns: list, db_alias: str = 'is_schedule', db_schema: str = 'is_bot_logs', splitter: str = ' ~ ', log_level: str = 'DEBUG'):
    """Логгер для handlers.py"""

    frmtr = logging.Formatter(fmt = '%(msg)s - %(levelname)s - %(asctime)s', datefmt = '%d.%m.%Y %H:%M:%S')
    LogDB = LogDBHandler(db_alias = db_alias, db_schema = db_schema, db_log_table = db_log_table, db_log_table_columns = db_log_table_columns, formatter = frmtr, splitter = splitter)

    log = logging.getLogger('ISS_Bot_handlers_PG_logger')
    log.addHandler(LogDB)
    log.setLevel(log_level)
    log.propagate = False
    return log


def is_bot_root_logger(db_log_table: str, db_log_table_columns: list, db_alias: str = 'is_schedule', db_schema: str = 'is_bot_logs', splitter: str = ' ~ ', log_level: str = 'DEBUG'):
    """Логгер для bot.py"""

    frmtr = logging.Formatter(fmt = '%(msg)s - %(levelname)s - %(asctime)s', datefmt = '%d.%m.%Y %H:%M:%S')
    LogDB = LogDBHandler(db_alias = db_alias, db_schema = db_schema, db_log_table = db_log_table, db_log_table_columns = db_log_table_columns, formatter = frmtr, splitter = splitter)

    log = logging.getLogger('ISS_Bot_root_PG_logger')
    log.addHandler(LogDB)
    log.setLevel(log_level)
    log.propagate = False
    return log

# db_alias = 'is_schedule'
# db_schema = 'is_bot_logs'
# db_log_table = 'bot_events_logs'
# db_log_table_columns = ['chat_id', 'bot_event', 'log_level_name', 'log_time']
# splitter = ' ~ '
# log_level = 'DEBUG'

# chat_id = 273537341
# bot_event = """PDF-файл отправлен"""

# log_data = f"{chat_id}{splitter}{bot_event}"

# db_logger = is_bot_handler_logger(db_log_table, db_log_table_columns, db_alias, db_schema, splitter, log_level)

# db_logger.error(msg = log_data)
# db_logger.warning(msg = log_data)
# db_logger.critical(msg = log_data)
# db_logger.debug(msg = log_data)
# db_logger.info(msg = log_data)