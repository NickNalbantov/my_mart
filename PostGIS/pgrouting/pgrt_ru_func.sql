-- функция для обработки координат начала и конца пути в id узлов;
    -- широта/долгота старта, широта/долгота конца, srid, таблица с рёбрами;
    -- подходит для алгоритма Дейкстры и других алгоритмов, когда надо найти путь из точки А в точку Б;

create or replace function pgrt_ru.pgrt_source_target_geo
(
    lat1 numeric,
    lon1 numeric,
    lat2 numeric,
    lon2 numeric,
    srid smallint default 4326,
    schema_name text default 'pgrt_ru',
    table_name text default 'routes'
)
returns table
(
    "source" bigint,
    target bigint
)
language plpgsql
as $func$
    declare
        st_place text;
        fin_place text;
        st_geom geometry;
        fin_geom geometry;
        st_geo geography;
        fin_geo geography;
        source_cte text := '';
        target_cte text := '';
        query_end text;
        final_query text;
    begin
        st_geom := st_pointfromtext('POINT(' || $2 || ' ' || $1 || ')', $5);
        fin_geom := st_pointfromtext('POINT(' || $4 || ' ' || $3||')', $5);
        st_geo := st_geom::geography;
        fin_geo := fin_geom::geography;
        query_end := $qe$
                         select s.source, t.target
                         from s
                         cross join t
                     $qe$;

        -- проверяем попадания в города и регионы;
        execute format($st_ch$
                              select 's' || max(settlement_id)::text
                              from (select settlement_id, geometry from pgis_ru.settlements) s
                              join (select %L as st_geo) st on st_contains(s.geometry, st.st_geo)
                       $st_ch$, st_geom)
        into st_place;

        if st_place is null then
            execute format($st_ch$
                                  select 'r' || max(reg_id)::text
                                  from (select reg_id, geometry from pgis_ru.regions) r
                                  join (select %L as st_geo) st on st_contains(r.geometry, st.st_geo)
                           $st_ch$, st_geom)
            into st_place;
        end if;

        execute format($f_ch$
                             select 's' || max(settlement_id)::text
                             from (select settlement_id, geometry from pgis_ru.settlements) s
                             join (select %L as fin_geo) f on st_contains(s.geometry, f.fin_geo)
                       $f_ch$, fin_geom)
        into fin_place;

        if fin_place is null then
            execute format($st_ch$
                                  select 'r' || max(reg_id)::text
                                  from (select reg_id, geometry from pgis_ru.regions) r
                                  join (select %L as fin_geo) f on st_contains(r.geometry, f.fin_geo)
                           $st_ch$, fin_geom)
            into fin_place;
        end if;

        -- конструируем CTE;
            -- выбираем узлы с минимальным расстоянием от указанной точки;
        if st_place like 's%' then
            source_cte := format($src$
                                      with s as
                                      (select max(source) as source
                                       from (
                                             select distinct source, dist, min(dist) over () as min_dist
                                             from (
                                                   select source::bigint as source, (st_distance(st_pointfromtext('POINT(' || rt.x1 || ' ' || rt.y1 || ')', %L)::geography, %L)/1000)::decimal as dist
                                                   from %I.%I rt
                                                   where rt.settlement_id = ltrim(%L, 's')::int
                                                  ) a
                                            ) b
                                       where dist = min_dist
                                      ),
                                 $src$, srid, st_geo, schema_name, table_name, st_place);
        else
            source_cte := format($src$
                                      with s as
                                      (select max(source) as source
                                       from (
                                             select distinct source, dist, min(dist) over () as min_dist
                                             from (
                                                   select source::bigint as source, (st_distance(st_pointfromtext('POINT(' || rt.x1 || ' ' || rt.y1 || ')', %L)::geography, %L)/1000)::decimal as dist
                                                   from %I.%I rt
                                                   where rt.settlement_id = ltrim(%L, 'r')::int
                                                  ) a
                                            ) b
                                       where dist = min_dist
                                      ),
                                 $src$, srid, st_geo, schema_name, table_name, st_place);
        end if;

        if fin_place like 's%' then
            target_cte := format($tgt$
                                      t as
                                      (select max(target) as target
                                       from (
                                             select distinct target, dist, min(dist) over () as min_dist
                                             from (
                                                   select target::bigint as target, (st_distance(st_pointfromtext('POINT(' || rt.x2 || ' ' || rt.y2 || ')', %L)::geography, %L)/1000)::decimal as dist
                                                   from %I.%I rt
                                                   where rt.settlement_id = ltrim(%L, 's')::int
                                                  ) a
                                            ) b
                                       where dist = min_dist
                                      )
                                 $tgt$, srid, fin_geo, schema_name, table_name, fin_place);
        else
            target_cte := format($tgt$
                                      t as
                                      (select max(target) as target
                                       from (
                                             select distinct target, dist, min(dist) over () as min_dist
                                             from (
                                                   select target::bigint as target, (st_distance(st_pointfromtext('POINT(' || rt.x2 || ' ' || rt.y2 || ')', %L)::geography, %L)/1000)::decimal as dist
                                                   from %I.%I rt
                                                   where rt.settlement_id = ltrim(%L, 'r')::int
                                                  ) a
                                            ) b
                                       where dist = min_dist
                                      )
                                 $tgt$, srid, fin_geo, schema_name, table_name, fin_place);
        end if;

        -- собираем итоговый запрос;
        final_query := source_cte || ' ' || target_cte || ' ' || query_end;

        return query execute format('%s', final_query);
    end;
$func$;

comment on function pgrt_ru.pgrt_source_target_geo is 'start/end nodes id from (lon, lat) coordinates';


-- функция для обработки координат точки старта в id узла;
    -- широта/долгота старта, srid, таблица с рёбрами;
    -- подходит для генерации массивов точек;

create or replace function pgrt_ru.pgrt_source_geo
(
    lat numeric,
    lon numeric,
    srid smallint default 4326,
    schema_name text default 'pgrt_ru',
    table_name text default 'routes'
)
returns table
(
    "source" bigint
)
language plpgsql
as $func$
    declare
        n_place text;
        n_geom geometry;
        n_geo geography;
        node_cte text := '';
        query_end text;
        final_query text;
    begin
        n_geom := st_pointfromtext('POINT(' || $2 || ' ' || $1 || ')', $3);
        n_geo := n_geom::geography;
        query_end := $qe$
                         select n.source
                         from n
                     $qe$;

        -- проверяем попадания в города и регионы;
        execute format($st_ch$
                              select 's' || max(settlement_id)::text
                              from (select settlement_id, geometry from pgis_ru.settlements) s
                              join (select %L as n_geo) n on st_contains(s.geometry, n.n_geo)
                       $st_ch$, n_geom)
        into n_place;

        if n_place is null then
            execute format($st_ch$
                                  select 'r' || max(reg_id)::text
                                  from (select reg_id, geometry from pgis_ru.regions) r
                                  join (select %L as n_geo) n on st_contains(r.geometry, n.n_geo)
                           $st_ch$, n_geom)
            into n_place;
        end if;

        -- конструируем CTE;
            -- выбираем узел с минимальным расстоянием от указанной точки;
        if n_place like 's%' then
            node_cte := format($src$
                                      with n as
                                      (select max(source) as source
                                       from (
                                             select distinct source, dist, min(dist) over () as min_dist
                                             from (
                                                   select source::bigint as source, (st_distance(st_pointfromtext('POINT(' || rt.x1 || ' ' || rt.y1 || ')', %L)::geography, %L)/1000)::decimal as dist
                                                   from %I.%I rt
                                                   where rt.settlement_id = ltrim(%L, 's')::int
                                                  ) a
                                            ) b
                                       where dist = min_dist
                                      )
                               $src$, srid, n_geo, schema_name, table_name, n_place);
        else
            node_cte := format($src$
                                      with n as
                                      (select max(source) as source
                                       from (
                                             select distinct source, dist, min(dist) over () as min_dist
                                             from (
                                                   select source::bigint as source, (st_distance(st_pointfromtext('POINT(' || rt.x1 || ' ' || rt.y1 || ')', %L)::geography, %L)/1000)::decimal as dist
                                                   from %I.%I rt
                                                   where rt.settlement_id = ltrim(%L, 'r')::int
                                                  ) a
                                            ) b
                                       where dist = min_dist
                                      )
                               $src$, srid, n_geo, schema_name, table_name, n_place);
        end if;

        -- собираем итоговый запрос;
        final_query := node_cte || ' ' || query_end;

        return query execute format('%s', final_query);
    end;
$func$;

comment on function pgrt_ru.pgrt_source_geo is 'node source id from (lon, lat) coordinates';


-- функция для обработки координат точки конца в id узла;
    -- широта/долгота конца, srid, таблица с рёбрами;
    -- подходит для генерации массивов точек;

create or replace function pgrt_ru.pgrt_target_geo
(
    lat numeric,
    lon numeric,
    srid smallint default 4326,
    schema_name text default 'pgrt_ru',
    table_name text default 'routes'
)
returns table
(
    target bigint
)
language plpgsql
as $func$
    declare
        n_place text;
        n_geom geometry;
        n_geo geography;
        node_cte text := '';
        query_end text;
        final_query text;
    begin
        n_geom := st_pointfromtext('POINT(' || $2 || ' ' || $1 || ')', $3);
        n_geo := n_geom::geography;
        query_end := $qe$
                         select n.target
                         from n
                     $qe$;

        -- проверяем попадания в города и регионы;
        execute format($st_ch$
                              select 's' || max(settlement_id)::text
                              from (select settlement_id, geometry from pgis_ru.settlements) s
                              join (select %L as n_geo) n on st_contains(s.geometry, n.n_geo)
                       $st_ch$, n_geom)
        into n_place;

        if n_place is null then
            execute format($st_ch$
                                  select 'r' || max(reg_id)::text
                                  from (select reg_id, geometry from pgis_ru.regions) r
                                  join (select %L as n_geo) n on st_contains(r.geometry, n.st_geo)
                           $st_ch$, n_geom)
            into n_place;
        end if;

        -- конструируем CTE;
            -- выбираем узел с минимальным расстоянием от указанной точки;
        if n_place like 's%' then
            node_cte := format($src$
                                      with n as
                                      (select max(target) as target
                                       from (
                                             select distinct target, dist, min(dist) over () as min_dist
                                             from (
                                                   select target::bigint as target, (st_distance(st_pointfromtext('POINT(' || rt.x2 || ' ' || rt.y2 || ')', %L)::geography, %L)/1000)::decimal as dist
                                                   from %I.%I rt
                                                   where rt.settlement_id = ltrim(%L, 's')::int
                                                  ) a
                                            ) b
                                       where dist = min_dist
                                      )
                               $src$, srid, n_geo, schema_name, table_name, n_place);
        else
            node_cte := format($src$
                                      with n as
                                      (select max(target) as target
                                       from (
                                             select distinct target, dist, min(dist) over () as min_dist
                                             from (
                                                   select target::bigint as target, (st_distance(st_pointfromtext('POINT(' || rt.x2 || ' ' || rt.y2 || ')', %L)::geography, %L)/1000)::decimal as dist
                                                   from %I.%I rt
                                                   where rt.settlement_id = ltrim(%L, 'r')::int
                                                  ) a
                                            ) b
                                       where dist = min_dist
                                      )
                               $src$, srid, n_geo, schema_name, table_name, n_place);
        end if;

        -- собираем итоговый запрос;
        final_query := node_cte || ' ' || query_end;

        return query execute format('%s', final_query);
    end;
$func$;

comment on function pgrt_ru.pgrt_target_geo is 'node target id from (lon, lat) coordinates';


-- процедура для циклической вставки данных для сложного графа регионов;

create or replace procedure pgrt_ru.reg_routes_insert()
language plpgsql
as $func$
    declare
     row_ record;
     src_reg smallint;
     tgt_reg smallint;
     src_id bigint;
     tgt_id bigint;
    begin
        for row_ in (select "source", target, source_id, target_id from pgrt_ru.reg_capital_arr order by "source", target) loop

            src_reg := row_."source";
            tgt_reg := row_.target;
            src_id := row_.source_id;
            tgt_id := row_.target_id;

            execute format('drop table if exists pgrt_ru.route_%1$s_%2$s', src_reg, tgt_reg);
            commit;
            raise notice 'Initial drop committed - pgrt_ru.route_%_%', src_reg, tgt_reg;

            execute format('create unlogged table pgrt_ru.route_%1$s_%2$s as
                                select *
                                from pgrt_ru.routes
                                where reg_id in (%1$L, %2$L)'
                           , src_reg, tgt_reg);
            commit;
            raise notice 'Temp table created - pgrt_ru.route_%_%', src_reg, tgt_reg;

            execute format($qry$
                                insert into pgrt_ru.reg_capital_routes
                                    select "source", target, sum("cost") as "cost", sum(reverse_cost) as reverse_cost, st_union(the_geom) as the_geom
                                    from
                                    (
                                        select %1$L::bigint as "source", %2$L::bigint as target, r."cost", r.reverse_cost, r.the_geom
                                        from pgrt_ru.route_%1$s_%2$s r
                                        join public.pgr_bddijkstra(
                                                                   'select r.id, r.source, r.target, r.cost, r.reverse_cost
                                                                    from pgrt_ru.route_%1$s_%2$s r'::text
                                                                   , %3$L::bigint, %4$L::bigint
                                                                  ) d on d.edge = r.id
                                    ) a
                                    group by "source", target;
                           $qry$, src_reg, tgt_reg, src_id, tgt_id)
            commit;
            raise notice 'Route between regions % and % inserted', src_reg, tgt_reg;

            execute format('drop table if exists pgrt_ru.route_%1$s_%2$s', src_reg, tgt_reg);
            commit;
            raise notice 'Final drop committed - pgrt_ru.route_%_%', src_reg, tgt_reg;

        end loop;
        raise notice 'All links inserted';
    end;
$func$;