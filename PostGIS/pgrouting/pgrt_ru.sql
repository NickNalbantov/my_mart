-- Импорт данных;

-- psql -U postgres -d postgres -q -f pgrt_ru_2po_4pgr.sql

-- переносим сырые данные (рёбра графа) в схему raw_geo;

create table if not exists raw_geo.russia_map_routes
(like public.pgrt_ru_2po_4pgr including all);

insert into raw_geo.russia_map_routes
    select *
    from public.pgrt_ru_2po_4pgr
    order by id;

analyze raw_geo.russia_map_routes;

drop table if exists public.pgrt_ru_2po_4pgr cascade;

alter table if exists raw_geo.russia_map_routes rename column geom_way to the_geom; -- используется в функции pgr_analyzegraph;

-- восстанавливаем последовательность;

create sequence raw_geo.russia_map_routes_id_seq
    owned by raw_geo.russia_map_routes.id;

select setval('raw_geo.russia_map_routes_id_seq', (select max(id) from raw_geo.russia_map_routes));

alter table if exists raw_geo.russia_map_routes
    alter column id set default nextval('raw_geo.russia_map_routes_id_seq');

-- анализ графа;
select public.pgr_createVerticesTable('raw_geo.russia_map_routes');

select public.pgr_analyzegraph('raw_geo.russia_map_routes', 0.000001);

select public.pgr_nodeNetwork('raw_geo.russia_map_routes', 0.001);

-- Результаты анализа записываем в таблицу с рёбрами;
    -- см. https://docs.pgrouting.org/latest/en/pgr_nodeNetwork.html

alter table if exists raw_geo.russia_map_routes drop column if exists old_id;

alter table if exists raw_geo.russia_map_routes add column old_id int;

insert into raw_geo.russia_map_routes (old_id, cost, reverse_cost, the_geom)
   (
        with segmented as
        (
            select
                old_id,
                count(*) as i
            from raw_geo.russia_map_routes_noded
            group by old_id
        )

        select
            segments.old_id,
            edges.cost,
            edges.reverse_cost,
            segments.the_geom
        from raw_geo.russia_map_routes as edges
        join raw_geo.russia_map_routes_noded as segments on (edges.id = segments.old_id)
        where edges.id in (
                            select old_id
                            from segmented
                            where i > 1
                          )
    );

drop table if exists raw_geo.russia_map_routes_noded cascade;

-- заполняем пустые поля (source, target, x1, y1, x2, y2 и т.д.) для новых отрезков;

drop table if exists raw_geo.russia_map_routes_vertices_pgr cascade;

select public.pgr_createtopology('raw_geo.russia_map_routes', 0.000001);

-- повторный анализ;

select public.pgr_analyzegraph('raw_geo.russia_map_routes', 0.000001);

analyze raw_geo.russia_map_routes;
analyze raw_geo.russia_map_routes_vertices_pgr;

--создаём схему для аналитики маршрутов;

create schema if not exists pgrt_ru;
comment on schema pgrt_ru is 'pg_routing analyzed data';

-- Загрузка итоговой таблицы с рёбрами;

create unlogged table if not exists pgrt_ru.routes_copy as
    select *
    from raw_geo.russia_map_routes;

alter table if exists pgrt_ru.routes_copy
    add column reg_id smallint,
    add column settlement_id integer;

-- временные таблицы с id;

create unlogged table if not exists pgrt_ru.rt_regions as
with r as
(
    select reg_id, geometry
    from pgis_ru.regions
)

select distinct
    rt.id,
    max(r.reg_id) over(partition by rt.the_geom) as reg_id
from raw_geo.russia_map_routes rt
join r on st_intersects(rt.the_geom, r.geometry);

alter table if exists pgrt_ru.rt_regions
    add primary key (id);

create index rt_reg_idx on pgrt_ru.rt_regions using btree (reg_id);

analyze pgrt_ru.rt_regions;


create unlogged table if not exists pgrt_ru.rt_settlements as

with s as
(
    select settlement_id, geometry
    from pgis_ru.settlements s
)

select a.id, s.settlement_id
from
(
    select distinct
        rt.id,
        max(s.settlement_id) over(partition by rt.the_geom) as settlement_id
    from raw_geo.russia_map_routes rt
    join s on st_intersects(rt.the_geom, s.geometry)
) a
join s using (settlement_id)
order by a.id;

alter table if exists pgrt_ru.rt_settlements
    add primary key (id);

analyze pgrt_ru.rt_settlements;


create unlogged table if not exists pgrt_ru.rt_ids as
    select
        id,
        reg_id,
        settlement_id
    from pgrt_ru.rt_regions
    left join pgrt_ru.rt_settlements using (id)
    order by id;

alter table if exists pgrt_ru.rt_ids
    add primary key (id);

analyze pgrt_ru.rt_ids;

drop table if exists pgrt_ru.rt_regions cascade;
drop table if exists pgrt_ru.rt_settlements cascade;

-- добавляем id;
update pgrt_ru.routes_copy a
set
    reg_id = res.reg_id,
    settlement_id = res.settlement_id
from pgrt_ru.rt_ids res
where a.id = res.id;

alter table if exists pgrt_ru.routes_copy
    add foreign key (reg_id) references pgis_ru.regions(reg_id),
    add foreign key (settlement_id) references pgis_ru.settlements(settlement_id);

analyze pgrt_ru.routes_copy;

drop table if exists raw_geo.russia_map_routes cascade;
drop table if exists raw_geo.russia_map_routes_vertices_pgr cascade;

-- собираем итоговую таблицу;

create table if not exists pgrt_ru.routes
    (like pgrt_ru.routes_copy excluding all);

alter table if exists pgrt_ru.routes
    drop column if exists old_id,
    drop column if exists osm_id,
    drop column if exists osm_source_id,
    drop column if exists osm_target_id;

create sequence pgrt_ru.routes_id_seq
    increment by 1 minvalue 1 maxvalue 9223372036854775807 cache 1 no cycle start with 1 owned by pgrt_ru.routes.id;

alter table if exists pgrt_ru.routes
    alter column id set default nextval('pgrt_ru.routes_id_seq');

insert into pgrt_ru.routes
(
    osm_name, osm_meta, clazz, flags, "source", target, km, kmh,
    "cost", reverse_cost, x1, y1, x2, y2, the_geom, reg_id, settlement_id
)
    select
        max(osm_name) as osm_name,
        max(osm_meta) as osm_meta,
        clazz,
        flags,
        "source",
        target,
        km,
        kmh,
        "cost",
        reverse_cost,
        x1,
        y1,
        x2,
        y2,
        the_geom,
        reg_id,
        settlement_id
    from pgrt_ru.routes_copy
    where reg_id is not null and x1 is not null
    group by
        clazz,
        flags,
        "source",
        target,
        km,
        kmh,
        "cost",
        reverse_cost,
        x1,
        y1,
        x2,
        y2,
        the_geom,
        reg_id,
        settlement_id
    order by
        reg_id,
        settlement_id,
        source,
        target,
        the_geom;

alter table if exists pgrt_ru.routes
    add foreign key (reg_id) references pgis_ru.regions(reg_id),
    add foreign key (settlement_id) references pgis_ru.settlements(settlement_id);

alter table if exists pgrt_ru.routes
    add primary key (id);

drop table if exists pgrt_ru.routes_copy cascade;

create index pgrt_source_idx on pgrt_ru.routes using btree ("source");
create index pgrt_target_idx on pgrt_ru.routes using btree (target);
create index pgrt_reg_id_idx on pgrt_ru.routes using btree (reg_id);
create index pgrt_sm_id_idx on pgrt_ru.routes using btree (settlement_id);
create index pgrt_geom_idx on pgrt_ru.routes using gist (the_geom) with (fillfactor='100');

cluster pgrt_ru.routes using routes_pkey;
analyze pgrt_ru.routes;


-- Граф из регионов России;

    -- Простая модель линков между соседними регионами:
        -- Учитываются любые соприкосновения;
        -- В качестве узлов - географические центры регионов;
        -- Все рёбра имеют вес 1 в обоих направлениях;

create table if not exists pgrt_ru.reg_simple_adjacency
(
    id smallint generated always as identity,
    "source" smallint,
    target smallint,
    "cost" numeric,
    reverse_cost numeric,
    x1 numeric,
    y1 numeric,
    x2 numeric,
    y2 numeric,
    the_geom geometry(geometry, 4326)
);

insert into pgrt_ru.reg_simple_adjacency ("source", target, "cost", reverse_cost, x1, y1, x2, y2, the_geom)
    select
        reg_id_a as "source",
        reg_id_b as target,
        1 as "cost",
        1 as reverse_cost,
        case when reg_id_a = 87 then 173.49609375000003::numeric
            else st_x(st_centroid(geom_a))::numeric end as x1,
        case when reg_id_a = 87 then 66.93006025862448::numeric
            else st_y(st_centroid(geom_a))::numeric end as y1,
        case when reg_id_b = 87 then 173.49609375000003::numeric
            else st_x(st_centroid(geom_b))::numeric end as x2,
        case when reg_id_b = 87 then 66.93006025862448::numeric
            else st_y(st_centroid(geom_b))::numeric end as y2,
        case when reg_id_a = 87 then st_makeline(st_setsrid(st_makepoint(173.49609375000003, 66.93006025862448), 4326), st_centroid(geom_b))
             when reg_id_b = 87 then st_makeline(st_centroid(geom_a), st_setsrid(st_makepoint(173.49609375000003, 66.93006025862448), 4326))
            else st_makeline(st_centroid(geom_a), st_centroid(geom_b)) end as the_geom
    from
    (
        select
            a.reg_id as reg_id_a,
            a.geometry as geom_a,
            b.reg_id as reg_id_b,
            b.geometry as geom_b
        from pgis_ru.regions a
        join pgis_ru.regions b on st_touches(a.geometry, b.geometry)
        where
            a.reg_id != b. reg_id
            and not (a.reg_id = 87 and b.reg_id in (14, 41, 49) or a.reg_id in (14, 41, 49) and b.reg_id = 87)

        union all

        select
            a.reg_id as reg_id_a,
            a.geometry as geom_a,
            b.reg_id as reg_id_b,
            b.geometry as geom_b
        from pgis_ru.regions a
        join pgis_ru.regions b on st_intersects(a.geometry, b.geometry)
        where
            a.reg_id != b. reg_id
            and (a.reg_id = 87 and b.reg_id in (14, 41, 49) or a.reg_id in (14, 41, 49) and b.reg_id = 87)
    ) p
    order by source, target;

alter table if exists pgrt_ru.reg_simple_adjacency
    add primary key (id);

alter table if exists pgrt_ru.reg_simple_adjacency
    add foreign key ("source") references pgis_ru.regions(reg_id),
    add foreign key (target) references pgis_ru.regions(reg_id);

create index rsa_source_idx on pgrt_ru.reg_simple_adjacency using btree ("source");
create index rsa_target_idx on pgrt_ru.reg_simple_adjacency using btree (target);
create index rsa_geom_idx on pgrt_ru.reg_simple_adjacency using gist (the_geom) with (fillfactor='100');

cluster pgrt_ru.reg_simple_adjacency using reg_simple_adjacency_pkey;
analyze pgrt_ru.reg_simple_adjacency;


    -- Усложнённая модель линков между соседними регионами:
        -- В качестве узлов - столицы регионов;
        -- Регионы соединены рёбрами только тогда, когда существует сухопутная дорога между столицами регионов, не выходящая из границ этих двух регионов;
        -- Все рёбра имеют вес, равный весу маршрута между столицами регионов;

create table if not exists pgrt_ru.reg_capital_adjacency
(
    id smallint generated always as identity,
    "source" smallint,
    target smallint,
    "cost" numeric,
    reverse_cost numeric,
    x1 numeric,
    y1 numeric,
    x2 numeric,
    y2 numeric,
    the_geom geometry(geometry, 4326)
);

create unlogged table if not exists pgrt_ru.reg_capital_arr
(
    "source" smallint references pgis_ru.regions(reg_id),
    target smallint references pgis_ru.regions(reg_id),
    x1 numeric,
    y1 numeric,
    x2 numeric,
    y2 numeric,
    shared_boundary geometry(geometry, 4326),
    source_id bigint,
    target_id bigint
);

insert into pgrt_ru.reg_capital_arr ("source", target, x1, y1, x2, y2, shared_boundary, source_id, target_id)
    with r as
    (
        select
            reg_id,
            geometry
        from pgis_ru.regions
    ),

    rc as
    (
        select
            reg_id,
            settlement_id
        from pgis_ru.reg_capitals
    ),

    s as
    (
        select
            settlement_id,
            geometry
        from pgis_ru.settlements
    ),

    g as
    (
        select
            reg_id_a as "source", reg_id_b as target,
            st_x(st_centroid(cap_a))::numeric as x1,
            st_y(st_centroid(cap_a))::numeric as y1,
            st_x(st_centroid(cap_b))::numeric as x2,
            st_y(st_centroid(cap_b))::numeric as y2,
            shared_boundary
        from
        (
            select
                a.reg_id as reg_id_a,
                a.geometry as geom_a,
                s_a.geometry as cap_a,
                b.reg_id as reg_id_b,
                b.geometry as geom_b,
                s_b.geometry as cap_b,
                st_intersection(a.geometry, b.geometry) as shared_boundary
            from r a
            join rc rc_a on a.reg_id = rc_a.reg_id
            join s s_a on s_a.settlement_id = rc_a.settlement_id
            join r b on st_touches(a.geometry, b.geometry)
            join rc rc_b on b.reg_id = rc_b.reg_id
            join s s_b on s_b.settlement_id = rc_b.settlement_id
            where
                a.reg_id != b. reg_id
                and not (a.reg_id = 87 and b.reg_id in (14, 41, 49) or a.reg_id in (14, 41, 49) and b.reg_id = 87)

            union all

            select
                a.reg_id as reg_id_a,
                a.geometry as geom_a,
                s_a.geometry as cap_a,
                b.reg_id as reg_id_b,
                b.geometry as geom_b,
                s_b.geometry as cap_b,
                ST_ApproximateMedialAxis(st_intersection(a.geometry, b.geometry)) as shared_boundary
            from r a
            join rc rc_a on a.reg_id = rc_a.reg_id
            join s s_a on s_a.settlement_id = rc_a.settlement_id
            join r b on st_intersects(a.geometry, b.geometry)
            join rc rc_b on b.reg_id = rc_b.reg_id
            join s s_b on s_b.settlement_id = rc_b.settlement_id
            where
                a.reg_id != b. reg_id
                and (a.reg_id = 87 and b.reg_id in (14, 41, 49) or a.reg_id in (14, 41, 49) and b.reg_id = 87)
        ) p
    )

    select distinct
        g.*,
        n.source as source_id,
        n.target as target_id
    from g
    cross join pgrt_ru.pgrt_source_target_geo(g.y1, g.x1, g.y2, g.x2) n;

create index rca_arr_source_idx on pgrt_ru.reg_capital_arr using btree ("source");
create index rca_arr_target_idx on pgrt_ru.reg_capital_arr using btree (target);
create index rca_arr_source_id_idx on pgrt_ru.reg_capital_arr using btree (source_id);
create index rca_arr_target_id_idx on pgrt_ru.reg_capital_arr using btree (target_id);
create index rca_arr_geom_idx on pgrt_ru.reg_capital_arr using gist (shared_boundary) with (fillfactor='100');

analyze pgrt_ru.reg_capital_arr;

create unlogged table if not exists pgrt_ru.reg_capital_routes
(
    "source" smallint references pgis_ru.regions(reg_id),
    target smallint references pgis_ru.regions(reg_id),
    "cost" numeric,
    reverse_cost numeric,
    the_geom geometry(geometry, 4326)
);

call pgrt_ru.reg_routes_insert();

drop procedure pgrt_ru.reg_routes_insert();

create index rca_rt_source_idx on pgrt_ru.reg_capital_routes using btree ("source");
create index rca_rt_target_idx on pgrt_ru.reg_capital_routes using btree (target);
create index rca_rt_geom_idx on pgrt_ru.reg_capital_routes using gist (the_geom) with (fillfactor='100');

analyze pgrt_ru.reg_capital_routes;

    -- финальная вставка;

insert into pgrt_ru.reg_capital_adjacency ("source", target, "cost", reverse_cost, x1, y1, x2, y2, the_geom)
    select
        a."source",
        a.target,
        r."cost",
        r.reverse_cost,
        a.x1,
        a.y1,
        a.x2,
        a.y2,
        r.the_geom
    from pgrt_ru.reg_capital_arr a
    join pgrt_ru.reg_capital_routes r on a."source" = r."source" and a.target = r.target
    order by
        source,
        target;

alter table if exists pgrt_ru.reg_capital_adjacency
    add primary key (id);

alter table if exists pgrt_ru.reg_capital_adjacency
    add foreign key ("source") references pgis_ru.regions(reg_id),
    add foreign key (target) references pgis_ru.regions(reg_id);

create index rca_source_idx on pgrt_ru.reg_capital_adjacency using btree ("source");
create index rca_target_idx on pgrt_ru.reg_capital_adjacency using btree (target);
create index rca_geom_idx on pgrt_ru.reg_capital_adjacency using gist (the_geom) with (fillfactor='100');

drop table if exists pgrt_ru.reg_capital_arr cascade;
drop table if exists pgrt_ru.reg_capital_routes cascade;

cluster pgrt_ru.reg_capital_adjacency using reg_capital_adjacency_pkey;
analyze pgrt_ru.reg_capital_adjacency;