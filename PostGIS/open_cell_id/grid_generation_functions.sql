-- Генерация прямоугольной сетки, размеры в метрах;

create or replace function public.makegrid_2d (
  bound_polygon public.geometry,
  width_step double precision,
  height_step double precision
)
returns public.geometry as
$body$
declare
  x_min double precision;
  x_max double precision;
  y_max double precision;
  x double precision;
  y double precision;
  nextx double precision;
  nexty double precision;
  cpoint public.geometry;
  sectors public.geometry[];
  i integer;
  srid integer;
begin
  x_min := st_xmin(bound_polygon);
  x_max := st_xmax(bound_polygon);
  y_max := st_ymax(bound_polygon);
  srid := st_srid(bound_polygon);

  y := st_ymin(bound_polygon); --current sector's corner coordinate
  i := -1;
  <<yloop>>
  loop
    if (y > y_max) then
        exit;
    end if;

    x := x_min;
    <<xloop>>
    loop
      if (x > x_max) then
          exit;
      end if;

      cpoint := st_setsrid(st_makepoint(x, y), srid);
      nextx := st_x(st_project(cpoint, $2, radians(90))::geometry);
      nexty := st_y(st_project(cpoint, $3, radians(0))::geometry);

      i := i + 1;
      sectors[i] := st_makeenvelope(x, y, nextx, nexty, srid);

      x := nextx;
    end loop xloop;
    cpoint := st_setsrid(st_makepoint(x, y), srid);
    nexty := st_y(st_project(cpoint, $3, radians(0))::geometry);
    y := nexty;
  end loop yloop;

  return st_collect(sectors);
end;
$body$
language 'plpgsql';


-- Генерация шестиугольной сетки;
    -- width - 2 * радиус вписанной окружности (r) в метрах;
    -- Пример использования:
        -- SELECT settlement_id, ST_Transform(grid, 4326) AS geom
        --     FROM (
        --             select settlement_id,
        --                 public.generate_hexgrid
        --                 (
        --                     10000.0/sqrt(3), -- width = 2 * r = R * sqrt(3)
        --                     ST_XMin(ST_Extent(ST_Transform(geometry, 3857))),
        --                     ST_YMin(ST_Extent(ST_Transform(geometry, 3857))),
        --                     ST_XMax(ST_Extent(ST_Transform(geometry, 3857))),
        --                     ST_YMax(ST_Extent(ST_Transform(geometry, 3857)))
        --                 ) as grid
        --             from s_sq
        --             group by settlement_id
        --         ) a

create or replace function public.generate_hexgrid(width float, x_min float, y_min float, x_max float, y_max float)
returns table (geom_res geometry) as $$
declare
  b float := width / 2;
  a float := tan(radians(30)) * b;
  c float := 2 * a;
  height float := 2 * a + c;
  ncol float := ceil(abs(x_max - x_min) / width);
  nrow float := ceil(abs(y_max - y_min) / height);
  polygon_string text := 'polygon((' ||
    0 || ' ' || 0 || ' , ' || b || ' ' || a || ' , ' || b || ' ' || a + c || ' , ' || 0 || ' ' || 2 * a + c || ' , ' ||
   -1 * b || ' ' || a + c || ' , ' || -1 * b || ' ' || a || ' , ' || 0 || ' ' || 0 || '))';
begin

  return query
    select st_setsrid(st_translate(geom, x_series*(2 * a + c) + x_min, y_series*(2 * (c + a)) + y_min), 3857) as geom_res
    from generate_series(0, ncol::int, 1) as x_series,
         generate_series(0, nrow::int, 1) as y_series,
         (
           select polygon_string::geometry as geom
           union
           select st_translate(polygon_string::geometry, b, a + c) as geom
         ) as two_hex;

end;
$$ language plpgsql;