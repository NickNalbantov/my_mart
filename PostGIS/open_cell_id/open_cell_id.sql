-- заготовки таблиц;

create table if not exists raw_geo.cells_ru
(
    id int generated always as identity primary key,
    radio text,
    mcc smallint,
    mnc int,
    lac_tac_nid bigint,
    cid bigint,
    psc_pci smallint,
    longitude decimal,
    latitude decimal,
    range int,
    samples bigint,
    changeable smallint,
    created bigint,
    updated bigint,
    averagesignal decimal
);


-- импорт из CSV;

copy raw_geo.cells_ru (radio, mcc, mnc, lac_tac_nid, cid, psc_pci, longitude, latitude, "range", samples, changeable, created, updated, averagesignal)
from '/tmp/250.csv'
(format csv, header);

analyze raw_geo.cells_ru;


-- создаём схему;

create schema if not exists open_cell_id;
comment on schema open_cell_id is 'Base Stations by OpenCellID';


-- Перечисляемый тип для радиотехнологий;

create type open_cell_id.radio_type as enum ('GSM', 'CDMA', 'UMTS', 'LTE', 'NR');


-- Список операторов по MNC;

create table if not exists open_cell_id.mnc_ru
(
    mnc smallint primary key,
    provider text,
    active boolean,
    mvno boolean,
    mvno_provider text[]
);

copy open_cell_id.mnc_ru
from '/tmp/mnc_ru.csv'
(format csv, header);

cluster open_cell_id.mnc_ru using mnc_ru_pkey;
analyze open_cell_id.mnc_ru;


-- Заливаем предобработанные данные;

create table if not exists open_cell_id.cells_ru as
    select
        id,
        radio::open_cell_id.radio_type as radio,
        mcc,
        mnc,
        lac_tac_nid,
        cid,
        psc_pci,
        st_pointfromtext('POINT(' || longitude::text || ' ' || latitude::text || ')', 4326) as geometry,
        (case
            when radio = 'LTE' and "range" >= 100000 then ("range"/1000)::numeric
            when radio = 'LTE' and "range" between 25000 and 100000 then ("range"/100)::numeric
            when radio = 'LTE' and "range" between 2500 and 25000 then ("range"/10)::numeric
            when radio in ('CDMA', 'UMTS') and "range" >= 500000 then ("range"/1000)::numeric
            when radio in ('CDMA', 'UMTS') and "range" between 50000 and 500000 then ("range"/100)::numeric
            when radio in ('CDMA', 'UMTS') and "range" between 7500 and 50000 then ("range"/10)::numeric
            when radio = 'GSM' and "range" >= 1000000 then ("range"/1000)::numeric
            when radio = 'GSM' and "range" between 100000 and 1000000 then ("range"/100)::numeric
            when radio = 'GSM' and "range" between 35000 and 100000 then ("range"/10)::numeric
            else "range"::numeric end
        ) as coverage_radius
    from raw_geo.cells_ru;

create index cells_ru_geometry_idx on open_cell_id.cells_ru using gist (geometry) with (fillfactor='100');

alter table if exists open_cell_id.cells_ru
    add primary key (id);

alter table if exists open_cell_id.cells_ru
    add constraint cells_ru_cid_uidx unique (radio, mcc, mnc, lac_tac_nid, cid);

alter table if exists open_cell_id.cells_ru
    add foreign key (mnc) references open_cell_id.mnc_ru (mnc);

cluster open_cell_id.cells_ru using cells_ru_pkey;
analyze open_cell_id.cells_ru;

drop table if exists raw_geo.cells_ru cascade;


-- BTS по населённым пунктам;

create table if not exists open_cell_id.cells_settlements_ru
(
    cell_id int,
    settlement_id int
);

insert into open_cell_id.cells_settlements_ru
    with c as
    (
        select id, geometry
        from open_cell_id.cells_ru
    ),

    s as
    (
        select settlement_id, geometry, square
        from pgis_ru.settlements
    )

    select cell_id, settlement_id
    from (
        select distinct c.id as cell_id, s.settlement_id, s.square, max(s.square) over(partition by c.id) as max_square
        from c
        join s on st_contains(s.geometry, c.geometry)
    ) a
    where square = max_square;

alter table if exists open_cell_id.cells_settlements_ru add primary key (cell_id);

alter table if exists open_cell_id.cells_settlements_ru
    add foreign key (cell_id) references open_cell_id.cells_ru (id),
    add foreign key (settlement_id) references pgis_ru.settlements (settlement_id);

create index cells_ru_settlements_id_idx on open_cell_id.cells_settlements_ru using btree (settlement_id);
cluster open_cell_id.cells_settlements_ru using cells_settlements_ru_pkey;
analyze open_cell_id.cells_settlements_ru;


-- BTS по регионам;

create table if not exists open_cell_id.cells_regions_ru
(
    cell_id int,
    reg_id int
);

insert into open_cell_id.cells_regions_ru
    with c as
    (
        select
            id,
            geometry
        from open_cell_id.cells_ru
    ),

    r as
    (
        select
            reg_id,
            geometry
        from pgis_ru.regions
    )

    select distinct
        c.id as cell_id,
        r.reg_id
    from c
    join r on st_contains(r.geometry, c.geometry);

alter table if exists open_cell_id.cells_regions_ru add primary key (cell_id);

alter table if exists open_cell_id.cells_regions_ru
    add foreign key (cell_id) references open_cell_id.cells_ru (id),
    add foreign key (reg_id) references pgis_ru.regions (reg_id);

create index cells_ru_reg_id_idx on open_cell_id.cells_regions_ru using btree (reg_id);
cluster open_cell_id.cells_regions_ru using cells_regions_ru_pkey;
analyze open_cell_id.cells_regions_ru;


-- Радиопокрытие по населённым пунктам для каждого оператора и технологии;

create table if not exists open_cell_id.settlements_coverage_ru
(
    settlement_id int,
    mnc smallint,
    radio open_cell_id.radio_type,
    bts_count int,
    bts_locations geometry(geometry, 4326),
    radio_coverage geometry(geometry, 4326)
);

insert into open_cell_id.settlements_coverage_ru
    with cell as
    (
        select
            cs.cell_id,
            cs.settlement_id,
            c.radio,
            c.mnc,
            c.geometry,
            c.coverage_radius
        from open_cell_id.cells_settlements_ru cs
        join
            (
              select
                id,
                radio,
                mnc,
                geometry,
                coverage_radius
              from open_cell_id.cells_ru
            ) c on c.id = cs.cell_id
    )

    select
        settlement_id,
        mnc,
        radio,
        count(*) as bts_count,
        st_union(geometry) as bts_locations,
        st_union((st_buffer("geometry"::geography,
        coverage_radius)::geometry)) as radio_coverage
    from cell
    group by
        settlement_id,
        mnc,
        radio;

alter table if exists open_cell_id.settlements_coverage_ru
    add foreign key (settlement_id) references pgis_ru.settlements (settlement_id),
    add foreign key (mnc) references open_cell_id.mnc_ru (mnc);

alter table if exists open_cell_id.settlements_coverage_ru
    add constraint settle_mnc_radio_uniq unique (settlement_id, mnc, radio);

create index sm_bts_locations_geometry_idx on open_cell_id.settlements_coverage_ru using gist (bts_locations) with (fillfactor='100');
create index sm_radio_coverage_geometry_idx on open_cell_id.settlements_coverage_ru using gist (radio_coverage) with (fillfactor='100');
create index settle_settlement_idx on open_cell_id.settlements_coverage_ru using btree (settlement_id);

cluster open_cell_id.settlements_coverage_ru using settle_mnc_radio_uniq;
analyze open_cell_id.settlements_coverage_ru;


-- Радиопокрытие по регионам для каждого оператора и технологии;

create table if not exists open_cell_id.region_coverage_ru
(
    reg_id smallint,
    mnc smallint,
    radio open_cell_id.radio_type,
    bts_count int,
    bts_locations geometry(geometry, 4326),
    radio_coverage geometry(geometry, 4326)
);

insert into open_cell_id.region_coverage_ru
    with cell as
    (
        select
            cr.cell_id,
            cr.reg_id,
            c.radio,
            c.mnc,
            c.geometry,
            c.coverage_radius
        from open_cell_id.cells_regions_ru cr
        join
            (
              select
                id,
                radio,
                mnc,
                geometry,
                coverage_radius
              from open_cell_id.cells_ru
            ) c on c.id = cr.cell_id
    )

    select
        reg_id,
        mnc,
        radio,
        count(*) as bts_count,
        st_union(geometry) as bts_locations,
        st_union((st_buffer("geometry"::geography, coverage_radius)::geometry)) as radio_coverage
    from cell
    group by
        reg_id,
        mnc,
        radio;

alter table if exists open_cell_id.region_coverage_ru
    add foreign key (reg_id) references pgis_ru.regions (reg_id),
    add foreign key (mnc) references open_cell_id.mnc_ru (mnc);

alter table if exists open_cell_id.region_coverage_ru
    add constraint reg_mnc_radio_uniq unique (reg_id, mnc, radio);

create index reg_bts_locations_geometry_idx on open_cell_id.region_coverage_ru using gist (bts_locations) with (fillfactor='100');
create index reg_radio_coverage_geometry_idx on open_cell_id.region_coverage_ru using gist (radio_coverage) with (fillfactor='100');
create index reg_region_idx on open_cell_id.region_coverage_ru using btree (reg_id);

cluster open_cell_id.region_coverage_ru using reg_mnc_radio_uniq;
analyze open_cell_id.region_coverage_ru;


-- Общее радиопокрытие для каждого оператора и технологии;

create table if not exists open_cell_id.coverage_ru
(
    mnc smallint,
    radio open_cell_id.radio_type,
    bts_count int,
    radio_coverage geometry(geometry, 4326)
);

insert into open_cell_id.coverage_ru
    with cell as
    (
        select
            id,
            radio,
            mnc,
            geometry,
            coverage_radius
        from open_cell_id.cells_ru
    )

    select
        mnc,
        radio,
        count(*) as bts_count,
        st_union((st_buffer("geometry"::geography, coverage_radius)::geometry)) as radio_coverage
    from cell
    group by
        mnc,
        radio;

alter table if exists open_cell_id.coverage_ru
    add foreign key (mnc) references open_cell_id.mnc_ru (mnc);

alter table if exists open_cell_id.coverage_ru
    add constraint mnc_radio_uniq unique (mnc, radio);

create index radio_coverage_geometry_idx on open_cell_id.coverage_ru using gist (radio_coverage) with (fillfactor='100');

cluster open_cell_id.coverage_ru using mnc_radio_uniq;
analyze open_cell_id.coverage_ru;


-- Сравнение покрытий LTE, NR Stage 1, NR Stage 2;

    -- Теоретическое радиопокрытие сетью 5G по населённым пунктам, если каждый оператор разместит оборудование 5G на всех своих существующих 4G-сайтах внутри населённых пунктов;
        -- радиус соты 5G принят = 300 метров;

    create unlogged table if not exists open_cell_id.nr_stage1_settlements_coverage_ru
    (
        settlement_id int,
        mnc smallint,
        radio open_cell_id.radio_type default 'NR',
        bts_count int,
        bts_locations geometry(geometry, 4326),
        radio_coverage geometry(geometry, 4326)
    );

    insert into open_cell_id.nr_stage1_settlements_coverage_ru
        with cell as
        (
            select
                cs.cell_id,
                cs.settlement_id,
                c.mnc,
                c.geometry,
                c.radio,
                c.coverage_radius
            from open_cell_id.cells_settlements_ru cs
            join
                (
                    select
                        id,
                        mnc,
                        geometry,
                        radio,
                        coverage_radius
                    from open_cell_id.cells_ru
                    where radio in ('LTE', 'NR')
                ) c on c.id = cs.cell_id
        )

        select
            settlement_id,
            mnc,
            'NR'::open_cell_id.radio_type as radio,
            count(*) as bts_count,
            st_union(geometry) as bts_locations,
            st_union(
                        case when radio = 'LTE' then (st_buffer("geometry"::geography, 300)::geometry)
                             when radio = 'NR' then (st_buffer("geometry"::geography, coverage_radius)::geometry) end
                    ) as radio_coverage
        from cell
        group by
            settlement_id,
            mnc;

    alter table if exists open_cell_id.nr_stage1_settlements_coverage_ru
        add foreign key (settlement_id) references pgis_ru.settlements (settlement_id),
        add foreign key (mnc) references open_cell_id.mnc_ru (mnc);

    alter table if exists open_cell_id.nr_stage1_settlements_coverage_ru
        add constraint nr_st1_settle_mnc_uniq unique (settlement_id, mnc);

    cluster open_cell_id.nr_stage1_settlements_coverage_ru using nr_st1_settle_mnc_uniq;
    analyze open_cell_id.nr_stage1_settlements_coverage_ru;

    comment on table open_cell_id.nr_stage1_settlements_coverage_ru is 'LTE-NR colocation';

    -- Теоретическое радиопокрытие сетью 5G по населённым пунктам, если каждый оператор разместит оборудование 5G на всех своих существующих 2G/3G/4G-сайтах внутри населённых пунктов;
        -- радиус соты 5G принят = 300 метров;

    create unlogged table if not exists open_cell_id.nr_stage2_settlements_coverage_ru
    (
        settlement_id int,
        mnc smallint,
        radio open_cell_id.radio_type default 'NR',
        bts_count int,
        bts_locations geometry(geometry, 4326),
        radio_coverage geometry(geometry, 4326)
    );

    insert into open_cell_id.nr_stage2_settlements_coverage_ru
        with cell as
        (
            select
                cs.cell_id,
                cs.settlement_id,
                c.mnc,
                c.geometry,
                c.radio,
                c.coverage_radius
            from open_cell_id.cells_settlements_ru cs
            join
                (
                  select
                    id,
                    mnc,
                    geometry,
                    radio,
                    coverage_radius
                  from open_cell_id.cells_ru
                ) c on c.id = cs.cell_id
        )

        select
            settlement_id,
            mnc,
            'NR'::open_cell_id.radio_type as radio,
            count(*) as bts_count,
            st_union(geometry) as bts_locations,
            st_union(
                        case when radio = 'LTE' then (st_buffer("geometry"::geography, 300)::geometry)
                             when radio = 'NR' then (st_buffer("geometry"::geography, coverage_radius)::geometry) end
                    ) as radio_coverage
        from cell
        group by
            settlement_id,
            mnc;

    alter table if exists open_cell_id.nr_stage2_settlements_coverage_ru
        add foreign key (settlement_id) references pgis_ru.settlements (settlement_id),
        add foreign key (mnc) references open_cell_id.mnc_ru (mnc);

    alter table if exists open_cell_id.nr_stage2_settlements_coverage_ru
        add constraint nr_st2_settle_mnc_uniq unique (settlement_id, mnc);

    cluster open_cell_id.nr_stage2_settlements_coverage_ru using nr_st2_settle_mnc_uniq;
    analyze open_cell_id.nr_stage2_settlements_coverage_ru;

    comment on table open_cell_id.nr_stage2_settlements_coverage_ru is 'All sites-NR colocation';

    -- временная таблица;
    create unlogged table if not exists open_cell_id.lte_nr_comparison_ru_copy
    (
        reg_id smallint,
        settlement_id int,
        settlement_name text,
        provider text,
        mnc_arr smallint[],
        radio open_cell_id.radio_type,
        stage text,
        active_bts_count int,
        active_bts_locations geometry(geometry, 4326),
        active_coverage_percentage numeric,
        active_radio_coverage geometry(geometry, 4326),
        city_coverage_diff geometry(geometry, 4326),
        new_bts_count int,
        new_bts_locations geometry(geometry, 4326),
        diff_coverage geometry(geometry, 4326),
        new_coverage_percentage numeric,
        new_radio_coverage geometry(geometry, 4326),
        city_square numeric
    );

    -- вставляем первую часть;
    insert into open_cell_id.lte_nr_comparison_ru_copy
    (
        reg_id, settlement_id, settlement_name, provider, mnc_arr, radio, stage, active_bts_count,
        active_bts_locations, active_coverage_percentage, active_radio_coverage, city_coverage_diff, city_square
    )
        with s as
        (
            select
                reg_id,
                settlement_id,
                settlement_name,
                square,
                geometry
            from pgis_ru.settlements
        ),

        p as
        (
            select
                provider,
                array_agg(distinct mnc order by mnc) as mnc_arr
            from open_cell_id.mnc_ru
            where mvno is false
            group by provider
        ),

        lte as
        ( -- разница между покрытием и площадью города;
            select
                cov.reg_id,
                cov.settlement_id,
                cov.settlement_name,
                cov.provider,
                cov.mnc_arr,
                cov.radio,
                cov.stage,
                cov.active_bts_count,
                cov.active_bts_locations,
                (((st_area(cov.radio_coverage::geography)/1000000)::decimal)/cov.city_square)*100::decimal as active_coverage_percentage,
                cov.radio_coverage as active_radio_coverage,
                st_difference(cov.city, cov.radio_coverage) as city_coverage_diff,
                cov.city_square
            from
            ( -- количество BTS и покрытие на основе действующих сайтов;
                select distinct
                    s.reg_id,
                    s.settlement_id,
                    s.settlement_name,
                    p.provider,
                    p.mnc_arr,
                    c.radio,
                    'Current' as stage,
                    sum(c.bts_count) over w as active_bts_count,
                    st_union(c.bts_locations) over w as active_bts_locations,
                    st_union(c.radio_coverage) over w as radio_coverage,
                    s.square as city_square,
                    s.geometry as city
                from open_cell_id.settlements_coverage_ru c
                join s on s.settlement_id = c.settlement_id
                join p on c.mnc = any(p.mnc_arr)
                where c.radio = 'LTE'
                window w as (partition by s.reg_id, s.settlement_id, p.provider)
            ) cov
        ),

        nr as
        (-- разница между покрытием и площадью города;
            select
                cov.reg_id,
                cov.settlement_id,
                cov.settlement_name,
                cov.provider,
                cov.mnc_arr,
                cov.radio,
                cov.stage,
                cov.active_bts_count,
                cov.active_bts_locations,
                (((st_area(cov.radio_coverage::geography)/1000000)::decimal)/cov.city_square)*100::decimal as active_coverage_percentage,
                cov.radio_coverage as active_radio_coverage,
                st_difference(cov.city, cov.radio_coverage) as city_coverage_diff,
                cov.city_square
            from
            ( -- количество BTS и покрытие на основе действующих сайтов;
                select distinct
                    s.reg_id,
                    s.settlement_id,
                    s.settlement_name,
                    p.provider,
                    p.mnc_arr,
                    c.radio,
                    'Stage 1' as stage,
                    sum(c.bts_count) over w as active_bts_count,
                    st_union(c.bts_locations) over w as active_bts_locations,
                    st_union(c.radio_coverage) over w as radio_coverage,
                    s.square as city_square,
                    s.geometry as city
                from open_cell_id.nr_stage1_settlements_coverage_ru c
                join s on s.settlement_id = c.settlement_id
                join p on c.mnc = any(p.mnc_arr)
                window w as (partition by s.reg_id, s.settlement_id, p.provider)

                union all

                select distinct
                    s.reg_id,
                    s.settlement_id,
                    s.settlement_name,
                    p.provider,
                    p.mnc_arr,
                    c.radio,
                    'Stage 2' as stage,
                    sum(c.bts_count) over w as active_bts_count,
                    st_union(c.bts_locations) over w as active_bts_locations,
                    st_union(c.radio_coverage) over w as radio_coverage,
                    s.square as city_square,
                    s.geometry as city
                from open_cell_id.nr_stage2_settlements_coverage_ru c
                join s on s.settlement_id = c.settlement_id
                join p on c.mnc = any(p.mnc_arr)
                window w as (partition by s.reg_id, s.settlement_id, p.provider)
            ) cov
        )

        select *
        from
        (
            select *
            from lte

            union all

            select *
            from nr
        ) fin;

    alter table if exists open_cell_id.lte_nr_comparison_ru_copy
        add constraint lte_nr_uniq_1 unique (settlement_id, provider, radio, stage);

    analyze open_cell_id.lte_nr_comparison_ru_copy;

    -- вставляем вторую часть;
        --LTE;

    create unlogged table if not exists open_cell_id.lte_nr_comparison_ru_copy_2 as
        select *
        from open_cell_id.lte_nr_comparison_ru_copy;

    with s as
    (
        select settlement_id, geometry
        from pgis_ru.settlements
    ),

    grid_lte as
    (
        select
            settlement_id,
            ST_Transform(grid, 4326) AS geom
        from
        (
            select
                settlement_id,
                public.generate_hexgrid
                (
                    2500.0 * 3 * 0.7, -- width = 2 * r = R * sqrt(3); сверху умножаем ещё на sqrt(3) для полного перекрытия окружностями; снимаем 30% на погрешности преобразования расстояний.
                    ST_XMin(ST_Extent(ST_Transform(geometry, 3857))),
                    ST_YMin(ST_Extent(ST_Transform(geometry, 3857))),
                    ST_XMax(ST_Extent(ST_Transform(geometry, 3857))),
                    ST_YMax(ST_Extent(ST_Transform(geometry, 3857)))
                ) as grid
            from s
            group by settlement_id
        ) a
    ),

    lte as
    ( -- строим шестиугольную сетку, в центр каждого шестиугольника ставим новый сайт, вычисляем покрытие белых пятен;
        select
            dif.settlement_id,
            dif.provider,
            dif.radio,
            dif.stage,
            st_union(st_centroid(grid_lte.geom)) as new_bts_locations,
            st_union((st_buffer(st_centroid(grid_lte.geom)::geography, 2500)::geometry)) as diff_coverage
        from open_cell_id.lte_nr_comparison_ru_copy dif
        join grid_lte
            on dif.settlement_id = grid_lte.settlement_id
            and st_contains(dif.city_coverage_diff, st_centroid(grid_lte.geom))
        where radio = 'LTE'
        group by
            dif.settlement_id,
            dif.provider,
            dif.radio,
            dif.stage
    )

    update open_cell_id.lte_nr_comparison_ru_copy_2 a
    set
        new_bts_locations = lte.new_bts_locations,
        diff_coverage = lte.diff_coverage
    from lte
    where
        a.settlement_id = lte.settlement_id
        and a.provider = lte.provider
        and a.radio = lte.radio
        and a.stage = lte.stage;

        -- NR;
    with s as
    (
        select
            settlement_id,
            geometry
        from pgis_ru.settlements
    ),

    grid_nr as
    (
        select settlement_id, ST_Transform(grid, 4326) AS geom
        from (
                select settlement_id,
                    public.generate_hexgrid
                    (
                        300.0 * 3 * 0.7, -- width = 2 * r = R * sqrt(3); сверху умножаем ещё на sqrt(3) для полного перекрытия окружностями; снимаем 30% на погрешности преобразования расстояний.
                        ST_XMin(ST_Extent(ST_Transform(geometry, 3857))),
                        ST_YMin(ST_Extent(ST_Transform(geometry, 3857))),
                        ST_XMax(ST_Extent(ST_Transform(geometry, 3857))),
                        ST_YMax(ST_Extent(ST_Transform(geometry, 3857)))
                    ) as grid
                from s
                group by settlement_id
            ) a
    ),

    nr as
    ( -- строим шестиугольную сетку, в центр каждого шестиугольника ставим новый сайт, вычисляем покрытие белых пятен;
        select
            dif.settlement_id,
            dif.provider,
            dif.radio,
            dif.stage,
            st_union(st_centroid(grid_nr.geom)) as new_bts_locations,
            st_union((st_buffer(st_centroid(grid_nr.geom)::geography, 300)::geometry)) as diff_coverage
        from open_cell_id.lte_nr_comparison_ru_copy dif
        join grid_nr
            on dif.settlement_id = grid_nr.settlement_id
            and st_contains(dif.city_coverage_diff, st_centroid(grid_nr.geom))
        where radio = 'NR'
        group by
            dif.settlement_id,
            dif.provider,
            dif.radio,
            dif.stage
    )

    update open_cell_id.lte_nr_comparison_ru_copy_2 a
    set
        new_bts_locations = nr.new_bts_locations,
        diff_coverage = nr.diff_coverage
    from nr
    where
        a.settlement_id = nr.settlement_id
        and a.provider = nr.provider
        and a.radio = nr.radio
        and a.stage = nr.stage;

    -- удаляем первую копию;
    drop table if exists open_cell_id.lte_nr_comparison_ru_copy cascade;

    -- индексация и сбор статистики;
    alter table if exists open_cell_id.lte_nr_comparison_ru_copy_2
        add constraint lte_nr_uniq_2 unique (settlement_id, provider, radio, stage);

    analyze open_cell_id.lte_nr_comparison_ru_copy_2;

    -- Вставляем третью часть;

    create unlogged table if not exists open_cell_id.lte_nr_comparison_ru_copy_3 as
        select *
        from open_cell_id.lte_nr_comparison_ru_copy_2
        where new_bts_locations is not null;

    with res as
    (
    -- вычисляем новый процент радиопокрытия;
        select
            settlement_id,
            provider,
            radio,
            stage,
            new_bts_count,
            new_bts_locations,
            (((st_area(new_radio_coverage::geography)/1000000)::decimal)/city_square)*100::decimal as new_coverage_percentage,
            new_radio_coverage
        from
        ( -- считаем количество новых сайтов и суммарное новое покрытие;
            select
                settlement_id,
                provider,
                radio,
                stage,
                array_upper(string_to_array(st_astext(new_bts_locations), ','), 1) as new_bts_count,
                new_bts_locations, st_union(active_radio_coverage, diff_coverage) as new_radio_coverage,
                city_square
            from open_cell_id.lte_nr_comparison_ru_copy_2 dif
            where new_bts_locations is not null
        ) new_
    )

    update open_cell_id.lte_nr_comparison_ru_copy_3 a
    set
        new_bts_count = res.new_bts_count,
        new_coverage_percentage = res.new_coverage_percentage,
        new_radio_coverage = res.new_radio_coverage
    from res
    where
        a.settlement_id = res.settlement_id
        and a.provider = res.provider
        and a.radio = res.radio
        and a.stage = res.stage;

    -- удаляем лишние поля;
    alter table if exists open_cell_id.lte_nr_comparison_ru_copy_3
        drop column city_square,
        drop column diff_coverage;

    -- удаляем вторую копию;
    drop table if exists open_cell_id.lte_nr_comparison_ru_copy_2 cascade;

    -- создаём финальную таблицу;

    create table if not exists open_cell_id.lte_nr_comparison_ru as
        select *
        from open_cell_id.lte_nr_comparison_ru_copy_3;

    -- удаляем третью копию и опорные таблицы;
    drop table if exists open_cell_id.lte_nr_comparison_ru_copy_3 cascade;
    drop table if exists open_cell_id.nr_stage1_settlements_coverage_ru cascade;
    drop table if exists open_cell_id.nr_stage2_settlements_coverage_ru cascade;

    -- индексация и сбор статистики;

    alter table if exists open_cell_id.lte_nr_comparison_ru
        add constraint lte_nr_uniq unique (settlement_id, provider, radio, stage);

    alter table if exists open_cell_id.lte_nr_comparison_ru
        add foreign key (reg_id) references pgis_ru.regions (reg_id),
        add foreign key (settlement_id) references pgis_ru.settlements (settlement_id);

    create index lte_nr_active_bts_idx on open_cell_id.lte_nr_comparison_ru using gist (active_bts_locations) with (fillfactor='100');
    create index lte_nr_active_cover_idx on open_cell_id.lte_nr_comparison_ru using gist (active_radio_coverage) with (fillfactor='100');
    create index lte_nr_city_diff_coverage_idx on open_cell_id.lte_nr_comparison_ru using gist (city_coverage_diff) with (fillfactor='100');
    create index lte_nr_new_bts_idx on open_cell_id.lte_nr_comparison_ru using gist (new_bts_locations) with (fillfactor='100');
    create index lte_nr_new_cover_idx on open_cell_id.lte_nr_comparison_ru using gist (new_radio_coverage) with (fillfactor='100');
    create index lte_nr_settlement_name_idx on open_cell_id.lte_nr_comparison_ru using hash (settlement_name);
    create index lte_nr_region_idx on open_cell_id.lte_nr_comparison_ru using btree (reg_id);
    create index lte_nr_settlement_idx on open_cell_id.lte_nr_comparison_ru using btree (settlement_id);

    cluster open_cell_id.lte_nr_comparison_ru using lte_nr_uniq;
    analyze open_cell_id.lte_nr_comparison_ru;

    comment on table open_cell_id.lte_nr_comparison_ru is 'Current LTE - NR stages comparison';


-- Представления;

    -- Покрытие по населённым пунктам;
    create materialized view if not exists open_cell_id.mv_settlements_coverage_ru as
    (

        with p as
        (
            select
                provider,
                array_agg(distinct mnc order by mnc) as mnc_arr
            from open_cell_id.mnc_ru
            where mvno is false
            group by provider
        )

        select distinct
            s.reg_id,
            r.reg_name as region,
            s.settlement_id,
            s.settlement_name,
            p.provider,
            p.mnc_arr,
            c.radio,
            sum(c.bts_count) over w as bts_count,
            st_union(c.bts_locations) over w as bts_locations,
            st_union(c.radio_coverage) over w as radio_coverage
        from open_cell_id.settlements_coverage_ru c
        join p on c.mnc = any(mnc_arr)
        join pgis_ru.settlements s on s.settlement_id = c.settlement_id
        join pgis_ru.regions r on r.reg_id = s.reg_id
        window w as (partition by s.settlement_id, p.provider, c.radio)
        order by
            settlement_id,
            provider,
            radio
    )
    with data;

    create unique index mv_settlement_cover_uidx on open_cell_id.mv_settlements_coverage_ru using btree (settlement_id, provider, radio);
    create index mv_settlement_cover_bts_idx on open_cell_id.mv_settlements_coverage_ru using gist (bts_locations) with (fillfactor='100');
    create index mv_settlement_cover_cover_idx on open_cell_id.mv_settlements_coverage_ru using gist (radio_coverage) with (fillfactor='100');

    analyze open_cell_id.mv_settlements_coverage_ru;


    -- Покрытие по регионам;
    create materialized view if not exists open_cell_id.mv_region_coverage_ru as
    (

        with p as
        (
            select
                provider,
                array_agg(distinct mnc order by mnc) as mnc_arr
            from open_cell_id.mnc_ru
            where mvno is false
            group by provider
        )

        select distinct
            r.reg_id,
            r.reg_name as region,
            p.provider,
            p.mnc_arr,
            c.radio,
            sum(c.bts_count) over w as bts_count,
            st_union(c.bts_locations) over w as bts_locations,
            st_union(c.radio_coverage) over w as radio_coverage
        from open_cell_id.region_coverage_ru c
        join p on c.mnc = any(mnc_arr)
        join pgis_ru.regions r on r.reg_id = c.reg_id
        window w as (partition by r.reg_id, p.provider, c.radio)
        order by
            reg_id,
            provider,
            radio
    )
    with data;

    create unique index mv_reg_cover_uidx on open_cell_id.mv_region_coverage_ru using btree (reg_id, provider, radio);
    create index mv_reg_cover_bts_idx on open_cell_id.mv_region_coverage_ru using gist (bts_locations) with (fillfactor='100');
    create index mv_reg_cover_cover_idx on open_cell_id.mv_region_coverage_ru using gist (radio_coverage) with (fillfactor='100');

    analyze open_cell_id.mv_region_coverage_ru;


    -- Покрытие по всей стране;
    create materialized view if not exists open_cell_id.mv_coverage_ru as
    (

        with p as
        (
            select
                provider,
                array_agg(distinct mnc order by mnc) as mnc_arr
            from open_cell_id.mnc_ru
            where mvno is false
            group by provider
        )

        select distinct
            p.provider,
            p.mnc_arr,
            c.radio,
            sum(c.bts_count) over w as bts_count,
            st_union(c.radio_coverage) over w as radio_coverage
        from open_cell_id.coverage_ru c
        join p on c.mnc = any(mnc_arr)
        window w as (partition by p.provider, c.radio)
        order by
            provider,
            radio
    )
    with data;

    create unique index mv_cover_uidx on open_cell_id.mv_coverage_ru using btree (provider, radio);
    create index mv_cover_cover_idx on open_cell_id.mv_coverage_ru using gist (radio_coverage) with (fillfactor='100');

    analyze open_cell_id.mv_coverage_ru;