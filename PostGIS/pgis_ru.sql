-- Делаем схему для аналитики

    create schema if not exists pgis_ru;
    comment on schema pgis_ru is 'PostGIS-based analytics - Russia';


-- Список часовых поясов;

    create table if not exists pgis_ru.timezones
    (
        tz_id smallint generated always as identity,
        full_name text,
        short_name text,
        utc_offset text,
        msk_offset text,
        square decimal,
        perimeter decimal,
        geometry geometry(geometry, 4326)
    );

    insert into pgis_ru.timezones (short_name, utc_offset, msk_offset) values
        ('KALT', '+02:00', '-01:00'),
        ('MSK', '+03:00', '00:00'),
        ('SAMT', '+04:00', '+01:00'),
        ('YEKT', '+05:00', '+02:00'),
        ('OMST', '+06:00', '+03:00'),
        ('KRAT', '+07:00', '+04:00'),
        ('IRKT', '+08:00', '+05:00'),
        ('YAKT', '+09:00', '+06:00'),
        ('VLAT', '+10:00', '+07:00'),
        ('MAGT', '+11:00', '+08:00'),
        ('PETT', '+12:00', '+09:00');

    update pgis_ru.timezones
        set (full_name, square, perimeter, geometry) =
            (select "name" as full_name, (st_area(way::geography)/1000000)::decimal as square, (st_perimeter(way::geography)/1000)::decimal as perimeter, way as geometry
             from raw_geo.russia_map_polygon
             where boundary = 'timezone' and "name" = 'Калининградское время')
        where tz_id = 1;

    update pgis_ru.timezones
        set (full_name, square, perimeter, geometry) =
            (select "name" as full_name, (st_area(way::geography)/1000000)::decimal as square, (st_perimeter(way::geography)/1000)::decimal as perimeter, way as geometry
             from raw_geo.russia_map_polygon
             where boundary = 'timezone' and "name" = 'Московское время')
        where tz_id = 2;

    update pgis_ru.timezones
        set (full_name, square, perimeter, geometry) =
            (select "name" as full_name, (st_area(way::geography)/1000000)::decimal as square, (st_perimeter(way::geography)/1000)::decimal as perimeter, way as geometry
             from raw_geo.russia_map_polygon
             where boundary = 'timezone' and "name" = 'Самарское время')
        where tz_id = 3;

    update pgis_ru.timezones
        set (full_name, square, perimeter, geometry) =
            (select "name" as full_name, (st_area(way::geography)/1000000)::decimal as square, (st_perimeter(way::geography)/1000)::decimal as perimeter, way as geometry
             from raw_geo.russia_map_polygon
             where boundary = 'timezone' and "name" = 'Екатеринбургское время')
        where tz_id = 4;

    update pgis_ru.timezones
        set (full_name, square, perimeter, geometry) =
            (select "name" as full_name, (st_area(way::geography)/1000000)::decimal as square, (st_perimeter(way::geography)/1000)::decimal as perimeter, way as geometry
             from raw_geo.russia_map_polygon
             where boundary = 'timezone' and "name" = 'Омское время')
        where tz_id = 5;

    update pgis_ru.timezones
        set (full_name, square, perimeter, geometry) =
            (select "name" as full_name, (st_area(way::geography)/1000000)::decimal as square, (st_perimeter(way::geography)/1000)::decimal as perimeter, way as geometry
             from raw_geo.russia_map_polygon
             where boundary = 'timezone' and "name" = 'Красноярское время')
        where tz_id = 6;

    update pgis_ru.timezones
        set (full_name, square, perimeter, geometry) =
            (select "name" as full_name, (st_area(way::geography)/1000000)::decimal as square, (st_perimeter(way::geography)/1000)::decimal as perimeter, way as geometry
             from raw_geo.russia_map_polygon
             where boundary = 'timezone' and "name" = 'Иркутское время')
        where tz_id = 7;

    update pgis_ru.timezones
        set (full_name, square, perimeter, geometry) =
            (select "name" as full_name, (st_area(way::geography)/1000000)::decimal as square, (st_perimeter(way::geography)/1000)::decimal as perimeter, way as geometry
             from raw_geo.russia_map_polygon
             where boundary = 'timezone' and "name" = 'Якутское время')
        where tz_id = 8;

    update pgis_ru.timezones
        set (full_name, square, perimeter, geometry) =
            (select "name" as full_name, (st_area(way::geography)/1000000)::decimal as square, (st_perimeter(way::geography)/1000)::decimal as perimeter, way as geometry
             from raw_geo.russia_map_polygon
             where boundary = 'timezone' and "name" = 'Владивостокское время')
        where tz_id = 9;

    update pgis_ru.timezones
        set (full_name, square, perimeter, geometry) =
            (select "name" as full_name, (st_area(way::geography)/1000000)::decimal as square, (st_perimeter(way::geography)/1000)::decimal as perimeter, way as geometry
             from raw_geo.russia_map_polygon
             where boundary = 'timezone' and "name" = 'Магаданское время')
        where tz_id = 10;

    update pgis_ru.timezones
        set (full_name, square, perimeter, geometry) =
            (select "name" as full_name, (st_area(way::geography)/1000000)::decimal as square, (st_perimeter(way::geography)/1000)::decimal as perimeter, way as geometry
             from raw_geo.russia_map_polygon
             where boundary = 'timezone' and "name" = 'Камчатское время')
        where tz_id = 11;

    alter table if exists pgis_ru.timezones add primary key (tz_id);
    create index tz_geometry_idx on pgis_ru.timezones using gist (geometry) with (fillfactor='100');

    cluster pgis_ru.timezones using timezones_pkey;
    analyze pgis_ru.timezones;


-- Список федеральных округов;

    create table if not exists pgis_ru.federal_districts
    (
        fd_id smallint generated always as identity,
        full_name text,
        short_name text,
        square decimal,
        perimeter decimal,
        geometry geometry(geometry, 4326)
    );

    insert into pgis_ru.federal_districts(short_name) values ('ЦФО'), ('СЗФО'), ('ЮФО'), ('СКФО'), ('ПФО'), ('УрФО'), ('СФО'), ('ДВФО');

    update pgis_ru.federal_districts
            set (full_name, square, perimeter, geometry) =
                (select "name" as federal_district, (st_area(way::geography) / 1000000)::decimal as square,
                        (st_perimeter(way::geography) / 1000)::decimal as perimeter, way as geometry
                from raw_geo.russia_map_polygon
                where boundary = 'administrative'and "name" = 'Центральный федеральный округ')
            where fd_id = 1;

        update pgis_ru.federal_districts
            set (full_name, square, perimeter, geometry) =
                (select "name" as federal_district, (st_area(way::geography) / 1000000)::decimal as square,
                        (st_perimeter(way::geography) / 1000)::decimal as perimeter, way as geometry
                from raw_geo.russia_map_polygon
                where boundary = 'administrative'and "name" = 'Северо-Западный федеральный округ')
            where fd_id = 2;

        update pgis_ru.federal_districts
            set (full_name, square, perimeter, geometry) =
                (select "name" as federal_district, (st_area(way::geography) / 1000000)::decimal as square,
                        (st_perimeter(way::geography) / 1000)::decimal as perimeter, way as geometry
                from raw_geo.russia_map_polygon
                where boundary = 'administrative'and "name" = 'Южный федеральный округ')
            where fd_id = 3;

        update pgis_ru.federal_districts
            set (full_name, square, perimeter, geometry) =
                (select "name" as federal_district, (st_area(way::geography) / 1000000)::decimal as square,
                        (st_perimeter(way::geography) / 1000)::decimal as perimeter, way as geometry
                from raw_geo.russia_map_polygon
                where boundary = 'administrative'and "name" = 'Северо-Кавказский федеральный округ')
            where fd_id = 4;

        update pgis_ru.federal_districts
            set (full_name, square, perimeter, geometry) =
                (select "name" as federal_district, (st_area(way::geography) / 1000000)::decimal as square,
                        (st_perimeter(way::geography) / 1000)::decimal as perimeter, way as geometry
                from raw_geo.russia_map_polygon
                where boundary = 'administrative'and "name" = 'Приволжский федеральный округ')
            where fd_id = 5;

        update pgis_ru.federal_districts
            set (full_name, square, perimeter, geometry) =
                (select "name" as federal_district, (st_area(way::geography) / 1000000)::decimal as square,
                        (st_perimeter(way::geography) / 1000)::decimal as perimeter, way as geometry
                from raw_geo.russia_map_polygon
                where boundary = 'administrative'and "name" = 'Уральский федеральный округ')
            where fd_id = 6;

        update pgis_ru.federal_districts
            set (full_name, square, perimeter, geometry) =
                (select "name" as federal_district, (st_area(way::geography) / 1000000)::decimal as square,
                        (st_perimeter(way::geography) / 1000)::decimal as perimeter, way as geometry
                from raw_geo.russia_map_polygon
                where boundary = 'administrative'and "name" = 'Сибирский федеральный округ')
            where fd_id = 7;

        update pgis_ru.federal_districts
            set (full_name, square, perimeter, geometry) =
                (select "name" as federal_district, (st_area(way::geography) / 1000000)::decimal as square,
                        (st_perimeter(way::geography) / 1000)::decimal as perimeter, way as geometry
                from raw_geo.russia_map_polygon
                where boundary = 'administrative'and "name" = 'Дальневосточный федеральный округ')
            where fd_id = 8;

    alter table if exists pgis_ru.federal_districts add primary key (fd_id);
    create index fd_geometry_idx on pgis_ru.federal_districts using gist (geometry) with (fillfactor='100');

    cluster pgis_ru.federal_districts using federal_districts_pkey;
    analyze pgis_ru.federal_districts;


-- Список регионов;
    -- значения st_relate подобраны эмпирически, много разных граничных условий;

    create table if not exists pgis_ru.regions
    (
        reg_id smallint,
        fd_id smallint,
        tz_ids smallint[],
        reg_name text,
        gibdd text[],
        okato int,
        okmto smallint,
        iso_3166_2 text,
        gost_7_67_2003 text[],
        square decimal,
        perimeter decimal,
        geometry geometry(geometry, 4326)
    );

    create unlogged table if not exists pgis_ru.regs_temp as
    select * from pgis_ru.regions;

    -- импорт из codes.csv;
    copy pgis_ru.regs_temp (reg_name, gibdd, okato, okmto, iso_3166_2, gost_7_67_2003)
    from '/tmp/codes.csv'
    (format csv, header, delimiter ';');

    update pgis_ru.regs_temp
    set reg_id = gibdd[1]::smallint;

    create materialized view if not exists pgis_ru.regs_temp_view as
        select *
        from pgis_ru.regs_temp;

    truncate table pgis_ru.regs_temp;

    -- insert
    with poly as
    (
        select distinct "name", "ref", st_area(way::geography)/1000000 as square, st_perimeter(way::geography)/1000 as perimeter, way as geometry
        from raw_geo.russia_map_polygon
        where ("name" in ('Севастополь', 'Республика Крым') or substring("ref", 'RU-\w{2,3}$') is not null)
              and boundary = 'administrative'
    ),

    res as
    (
        select r.reg_id, fd.fd_id, array_agg(tz.tz_id order by tz.tz_id) as tz_ids,
                r.reg_name, r.gibdd, r.okato, r.okmto, r.iso_3166_2, r.gost_7_67_2003,
                p.square, p.perimeter, p.geometry
        from poly p
        join pgis_ru.regs_temp_view r on (r.iso_3166_2 like 'RU%' and p."ref" = r.iso_3166_2) or (r.reg_name in ('Севастополь', 'Республика Крым') and p."name" = r.reg_name)
        join pgis_ru.federal_districts fd on st_relate(fd.geometry, p.geometry) in ('2F2F11FF2', '212F11FF2', '212FF1FF2', '212F01FF2', '212111212')
        join pgis_ru.timezones tz on st_relate(tz.geometry, p.geometry) in ('2FFF1FFF2', '212FF1FF2', '212F11FF2', '212111212', '2F2111212', '212F11212', '2F2F11FF2')
        group by r.reg_id, fd.fd_id, r.reg_name,r.gibdd, r.okato, r.okmto, r.iso_3166_2, r.gost_7_67_2003, p.square, p.perimeter, p.geometry
    )

    insert into pgis_ru.regs_temp
        select *
        from res;

    -- Исправляем Магадан - есть проблемы с пересечением границ с Чукоткой и Камчаткой, из-за этого приписывается лишний часовой пояс;
    update pgis_ru.regs_temp
    set tz_ids = array[10::smallint]
    where iso_3166_2 = 'RU-MAG';

    insert into pgis_ru.regions
        select *
        from pgis_ru.regs_temp;

    drop materialized view pgis_ru.regs_temp_view cascade;
    drop table if exists pgis_ru.regs_temp cascade;

    -- Добавляем население;
    alter table if exists pgis_ru.regions add column population int;

    create unlogged table if not exists pgis_ru.regions_population_names
    (
        reg_name text,
        population int
    );

    copy pgis_ru.regions_population_names
    from '/tmp/Population.csv'
    (format csv);

    with pop as
    (
        select reg_name, population
        from pgis_ru.regions_population_names
    )

    update pgis_ru.regions r
    set population = p.population
    from pop p
    where r.reg_name = p.reg_name;

    drop table if exists pgis_ru.regions_population_names cascade;

    alter table if exists pgis_ru.regions add primary key (reg_id);

    alter table if exists pgis_ru.regions
        add foreign key (fd_id) references pgis_ru.federal_districts (fd_id);

    create index reg_geometry_idx on pgis_ru.regions using gist (geometry) WITH (fillfactor='100');
    create index region_name_idx on pgis_ru.regions using hash (reg_name);

    cluster pgis_ru.regions using regions_pkey;
    analyze pgis_ru.regions;


-- Таблица отношений между ФО и часовыми поясами;

    create table if not exists pgis_ru.tz_fd_relations
    (
        tz_id smallint,
        fd_ids smallint[],
        fd_id smallint,
        tz_ids smallint[]
    );


    with reg_tz as
    (
        select fd_id, unnest(tz_ids) as tz_id
        from pgis_ru.regions
    ),

    fd_tz as
    (
        select fd_id, array_agg(distinct tz_id order by tz_id) as tz_arr
        from reg_tz
        group by fd_id
    ),

    tz_fd as
    (
        select tz_id, array_agg(distinct fd_id order by fd_id) as fd_arr
        from reg_tz
        group by tz_id
    )

    insert into pgis_ru.tz_fd_relations
        select t.*, f.*
        from tz_fd t
        left join fd_tz f on f.fd_id = t.tz_id
        order by t.tz_id, f.fd_id;

    alter table if exists pgis_ru.tz_fd_relations
        add foreign key (fd_id) references pgis_ru.federal_districts (fd_id),
        add foreign key (tz_id) references pgis_ru.timezones (tz_id);

    analyze pgis_ru.tz_fd_relations;


-- Список населённых пунктов;
    -- Первая точка из геополигона нужна для генерации уникального идентификатора населённого пункта (т.к. внутри одного региона могут быть одинаковые названия населённых пунктов;

    create table if not exists pgis_ru.settlements
    (
        settlement_id int generated always as identity,
        reg_id smallint,
        fd_id smallint,
        tz_id smallint,
        settlement_name text,
        square decimal,
        perimeter decimal,
        geometry geometry(geometry, 4326)
    );

       create unlogged table if not exists pgis_ru.settlements_temp
       as
    with settl as
    (
        select "name" as settlement_name, way,
                st_pointfromtext('POINT('||substring(ltrim(translate(st_astext(way), 'MULTIPOYGN()', '')), '^(\d+\.\d+ \d+\.\d+),')||')', 4326) as settlement_point
        from raw_geo.russia_map_polygon
        where place in ('city', 'town', 'hamlet', 'alloments', 'allotments_set', 'allotments', 'village', 'cottage settlement')
              and "name" is not null
              -- вычищаем одно корявое СНТ под Питером;
              and not ("name" in ('Василеостровский район', 'Невский район', 'Выборгский район', 'Кировский район', 'Петроградский район', 'Смольнинский район', 'Водный транспорт'))
    ),

    rg as
    (
        select reg_id, geometry
        from pgis_ru.regions
    ),

    fd as
    (
        select fd_id, geometry
        from pgis_ru.federal_districts
    ),

    tz as
    (
        select tz_id, geometry
        from pgis_ru.timezones
    )

    select reg_id, fd_id, tz_id, settlement_name, square, st_perimeter(geometry::geography)/1000 as perimeter, geometry, settlement_point,
           row_number() over(partition by reg_id, fd_id, tz_id, geometry order by length(settlement_name)) as rn -- для вычистки дубликатов типа город/городской округ
    from
    (
        select distinct reg_id, fd_id, tz_id, settlement_name, settlement_point, geometry, sq,
                        max(sq) over(partition by reg_id, fd_id, tz_id, settlement_name, settlement_point) as square
        from (
                select rg.reg_id, fd.fd_id, tz.tz_id, c.settlement_name, c.settlement_point, st_area(c.way::geography)/1000000 as sq, c.way as geometry
                from settl c
                join rg on st_contains(rg.geometry, c.way)
                join fd on st_contains(fd.geometry, c.way)
                join tz on st_contains(tz.geometry, c.way)
            ) s
    ) sm
    where sq = square
          and not (settlement_name = 'Амбарчик' and st_astext(settlement_point) = 'POINT(162.296412 69.6243362)' and (tz_id = 11 or reg_id = 87)); -- пересечение Чукотки с Якутией

    insert into pgis_ru.settlements (reg_id, fd_id, tz_id, settlement_name, square, perimeter, geometry)
        select reg_id, fd_id, tz_id, settlement_name, square, perimeter, geometry
        from pgis_ru.settlements_temp
        where rn = 1
        order by reg_id, settlement_name, st_astext(settlement_point);

    drop table if exists pgis_ru.settlements_temp cascade;

    alter table if exists pgis_ru.settlements add primary key (settlement_id);

    alter table if exists pgis_ru.settlements
        add foreign key (reg_id) references pgis_ru.regions (reg_id),
        add foreign key (fd_id) references pgis_ru.federal_districts (fd_id),
        add foreign key (tz_id) references pgis_ru.timezones (tz_id);

    create index settlement_geometry_idx on pgis_ru.settlements using gist (geometry) WITH (fillfactor='100');
    create index settlement_name_idx on pgis_ru.settlements using hash (settlement_name);
    create index settlement_region_idx on pgis_ru.settlements using btree (reg_id);

    cluster pgis_ru.settlements using settlements_pkey;
    analyze pgis_ru.settlements;


-- Список столиц федеральных округов;

    create table if not exists pgis_ru.fd_capitals
    (
        fd_id smallint references pgis_ru.federal_districts(fd_id),
        tz_id smallint references pgis_ru.timezones(tz_id),
        reg_id smallint references pgis_ru.regions(reg_id),
        settlement_id int references pgis_ru.settlements(settlement_id)
    );

    insert into pgis_ru.fd_capitals (fd_id, tz_id, reg_id, settlement_id)
        select fd_id, tz_id, reg_id, settlement_id
        from
        (
            select distinct fd_id, tz_id, reg_id, settlement_id, sq,
                            max(sq) over(partition by settlement_name) as square
            from
            (
                select s.fd_id, s.tz_id, s.reg_id, s.settlement_id, s.settlement_name, st_area(s.geometry::geography)/1000000 as sq
                from pgis_ru.settlements s
                where s.settlement_name in ('Москва', 'Санкт-Петербург', 'Пятигорск', 'Ростов-на-Дону', 'Нижний Новгород', 'Екатеринбург', 'Новосибирск', 'Владивосток')
            ) a
        ) sm
        where sq = square;

    alter table if exists pgis_ru.fd_capitals add primary key (fd_id);
    cluster pgis_ru.fd_capitals using fd_capitals_pkey;
    analyze pgis_ru.fd_capitals;


-- Список столиц регионов;

    create table if not exists pgis_ru.reg_capitals
    (
        reg_id smallint,
        tz_id smallint,
        fd_id smallint,
        settlement_id int
    );

    create unlogged table if not exists pgis_ru.reg_capitals_temp as
        select * from pgis_ru.reg_capitals;

    insert into pgis_ru.reg_capitals_temp (reg_id, tz_id, fd_id)
        select reg_id, tz_ids[1] as tz_id, fd_id
        from pgis_ru.regions;

    -- импорт списка городов из cap.csv
    create unlogged table if not exists pgis_ru.caps (reg_id smallint, cap_name text);

    copy pgis_ru.caps (reg_id, cap_name)
    from '/tmp/cap.csv'
    (format csv);

    -- update settlement_id во временную таблицу
    with s as
    (
        select reg_id, fd_id, tz_id, settlement_id
        from
        (
            select distinct fd_id, tz_id, reg_id, settlement_id, sq,
                            max(sq) over(partition by settlement_name) as square
            from
            (
                select s.fd_id, s.tz_id, s.reg_id, s.settlement_id, s.settlement_name, st_area(s.geometry::geography)/1000000 as sq
                from pgis_ru.settlements s
                join pgis_ru.caps c on c.reg_id = s.reg_id and c.cap_name = s.settlement_name
            ) a
        ) sm
        where sq = square
    )

    update pgis_ru.reg_capitals_temp rc
    set settlement_id = s.settlement_id
    from s
    where rc.fd_id = s.fd_id and rc.reg_id = s.reg_id and rc.tz_id = s.tz_id;

    drop table if exists pgis_ru.caps cascade;

    insert into pgis_ru.reg_capitals
        select distinct *
        from pgis_ru.reg_capitals_temp;

    drop table if exists pgis_ru.reg_capitals_temp cascade;

    alter table if exists pgis_ru.reg_capitals add primary key (reg_id);

    alter table if exists pgis_ru.reg_capitals
        add foreign key (reg_id) references pgis_ru.regions (reg_id),
        add foreign key (fd_id) references pgis_ru.federal_districts (fd_id),
        add foreign key (tz_id) references pgis_ru.timezones (tz_id),
        add foreign key (settlement_id) references pgis_ru.settlements (settlement_id);

    cluster pgis_ru.reg_capitals using reg_capitals_pkey;
    analyze pgis_ru.reg_capitals;


-- Список улиц;
    -- Уникальные идентификаторы населённого пункта из pgis_ru.settlements, имя улицы, длина улицы в км, геолиния;
    -- Длину улиц не делим на 2, т.к. большинство имеют всего одну линию на оба направления;

    create table if not exists pgis_ru.streets
        (
            street_id int generated always as identity,
            settlement_id int,
            street_name text,
            "length" decimal,
            geometry geometry(geometry, 4326)
        );

    create unlogged table if not exists pgis_ru.streets_temp_raw as
        select *
        from
        (
            select "name" as street, way as str_way, ST_PointN(way, 1) as start_point, ST_PointN(way, ST_NumPoints(way)) as end_point
            from raw_geo.russia_map_roads
            where substring(lower("name"), '(улица|проезд|бульвар|проспект|переулок|объезд|обход|спуск|подъём|шоссе|тупик|дорога|набережная|площадь|мост|аллея|кольцо)') is not null
                    and substring(lower("name"), '(автобус|трамвай|троллейбус|маршрут)') is null
                    and railway is null and (route is null or route in ('road', 'junction'))
            union
            select "name" as street, way as str_way, ST_PointN(way, 1) as start_point, ST_PointN(way, ST_NumPoints(way)) as end_point
            from raw_geo.russia_map_line
            where substring(lower("name"), '(улица|проезд|бульвар|проспект|переулок|объезд|обход|спуск|подъём|шоссе|тупик|дорога|набережная|площадь|мост|аллея|кольцо)') is not null
                    and substring(lower("name"), '(автобус|трамвай|троллейбус|маршрут)') is null
                    and railway is null and (route is null or route in ('road', 'junction'))
        ) a;


    create unlogged table if not exists pgis_ru.streets_temp as
        with settle as
        (
            select settlement_id, geometry
            from pgis_ru.settlements
        ),

        str_loc as
        (
            select c.settlement_id, s.street, s.str_way, s.start_point, s.end_point
            from pgis_ru.streets_temp_raw s
            join settle c on st_contains(c.geometry, s.start_point) or st_contains(c.geometry, s.end_point)
        )

        select distinct settlement_id, street_name, st_union(str_way) over(partition by settlement_id, lower(street_name)) as geometry
        from
        (
            select a.settlement_id, a.street as street_name, coalesce(b.str_way, a.str_way) as str_way
            from str_loc a
            left join str_loc b on a.settlement_id = b.settlement_id and a.street = b.street
                                and st_dwithin(a.end_point, b.start_point, 0.01)
        ) res;


    insert into pgis_ru.streets (settlement_id, street_name, "length", geometry)
        select settlement_id, max(street_name) as street_name, st_length(geometry::geography)/1000 as "length", geometry
        from
        (
            select s.settlement_id, s.street_name, st_intersection(s.geometry, c.geometry) as geometry
            from pgis_ru.streets_temp s
            join (select settlement_id, geometry from pgis_ru.settlements) c on s.settlement_id = c.settlement_id
        ) a
        where geometry is not null
        group by settlement_id, geometry
        order by settlement_id, street_name;

    drop table if exists pgis_ru.streets_temp_raw cascade;
    drop table if exists pgis_ru.streets_temp cascade;

    alter table if exists pgis_ru.streets add primary key (street_id);

    alter table if exists pgis_ru.streets
        add foreign key (settlement_id) references pgis_ru.settlements (settlement_id);

    create index street_geometry_idx on pgis_ru.streets using gist (geometry) WITH (fillfactor='100');
    create index street_name_idx on pgis_ru.streets using hash (street_name);
    create index street_settlement_idx on pgis_ru.streets using btree (settlement_id);

    cluster pgis_ru.streets using streets_pkey;
    analyze pgis_ru.streets;


-- Список маршрутов общественного транспорта;
    -- Всё по аналогии с таблицей streets. Длина маршрута также берётся полная, в обе стороны.

    create table if not exists pgis_ru.public_transport
        (
            route_id int generated always as identity,
            settlement_id int,
            pt_type text,
            route_name text,
            "length" decimal,
            geometry geometry(geometry, 4326)
        );

    create unlogged table if not exists pgis_ru.pub_trans_temp_raw as
        select *
        from
        (
            select "name" as pub_trans,
                    (case when lower(route) = 'tram' then 'Трамвай'
                            when lower(route) = 'trolleybus' then 'Троллейбус'
                            when lower(route) = 'subway' then 'Метро'
                            when lower(route) = 'train' and "name" ilike '%автобус%'  then 'Рельсовый автобус'
                            when substring(lower("name"), '(маршрутка|маршрутное)') is not null then 'Маршрутное такси'
                            else 'Автобус' end
                    ) as pt_type,
                    way as str_way, ST_PointN(way, 1) as start_point, ST_PointN(way, ST_NumPoints(way)) as end_point
            from raw_geo.russia_map_roads
            where substring(lower("name"), '(автобус|трамвай|троллейбус|маршрут|метро)') is not null
                    and substring(lower(route), '(closed|disused|was|fitness|ski|ferry|road|hiking|foot|piste|bicycle)') is null and route is not null
            union
            select "name" as pub_trans,
                    (case when lower(route) = 'tram' then 'Трамвай'
                            when lower(route) = 'trolleybus' then 'Троллейбус'
                            when lower(route) = 'subway' then 'Метро'
                            when lower(route) = 'train' and "name" ilike '%автобус%'  then 'Рельсовый автобус'
                            when substring(lower("name"), '(маршрутка|маршрутное)') is not null then 'Маршрутное такси'
                            else 'Автобус' end
                    ) as pt_type,
                    way as str_way, ST_PointN(way, 1) as start_point, ST_PointN(way, ST_NumPoints(way)) as end_point
            from raw_geo.russia_map_line
            where substring(lower("name"), '(автобус|трамвай|троллейбус|маршрут|метро)') is not null
                    and substring(lower(route), '(closed|disused|was|fitness|ski|ferry|road|hiking|foot|piste|bicycle)') is null and route is not null
        ) a;


    create unlogged table if not exists pgis_ru.pub_trans_temp as
        with settle as
        (
            select settlement_id, geometry
            from pgis_ru.settlements
        ),

        str_loc as
        (
            select c.settlement_id, s.pub_trans, s.pt_type, s.str_way, s.start_point, s.end_point
            from pgis_ru.pub_trans_temp_raw s
            join settle c on st_contains(c.geometry, s.start_point) or st_contains(c.geometry, s.end_point)
        )

        select distinct settlement_id, pt_type, route_name, st_union(str_way) over(partition by settlement_id, pt_type, lower(route_name)) as geometry
        from
        (
            select a.settlement_id, a.pt_type, a.pub_trans as route_name, coalesce(b.str_way, a.str_way) as str_way
            from str_loc a
            left join str_loc b on a.settlement_id = b.settlement_id and a.pub_trans = b.pub_trans
                                and st_dwithin(a.end_point, b.start_point, 0.01)
        ) res;


    insert into pgis_ru.public_transport (settlement_id, pt_type, route_name, "length", geometry)
        select settlement_id, pt_type, max(route_name) as route_name, st_length(geometry::geography)/1000 as "length", geometry
        from
        (
            select s.settlement_id, s.pt_type, s.route_name, st_intersection(s.geometry, c.geometry) as geometry
            from pgis_ru.pub_trans_temp s
            join (select settlement_id, geometry from pgis_ru.settlements) c on s.settlement_id = c.settlement_id
        ) a
        where geometry is not null
        group by settlement_id, pt_type, geometry
        order by settlement_id, pt_type, route_name;

    drop table if exists pgis_ru.pub_trans_temp_raw cascade;
    drop table if exists pgis_ru.pub_trans_temp cascade;

    alter table if exists pgis_ru.public_transport add primary key (route_id);

    alter table if exists pgis_ru.public_transport
        add foreign key (settlement_id) references pgis_ru.settlements (settlement_id);

    create index pub_trans_geometry_idx on pgis_ru.public_transport using gist (geometry) WITH (fillfactor='100');
    create index public_transport_name_idx on pgis_ru.public_transport using hash (route_name);
    create index public_transport_settlement_idx on pgis_ru.public_transport using btree (settlement_id);

    cluster pgis_ru.public_transport using public_transport_pkey;
    analyze pgis_ru.public_transport;


-- Представления;

    -- Столицы федеральных округов;

        create or replace view pgis_ru.v_fd_capitals as
            select
                fd.full_name as fd_name,
                fd.short_name as fd_abbr,
                s.settlement_name as capital_city,
                c.settlement_id as city_id,
                r.reg_name as capital_region,
                c.reg_id as capital_reg_code,
                t.full_name as tz_name,
                t.short_name as tz_abbr,
                t.msk_offset,
                t.utc_offset
            from pgis_ru.fd_capitals c
            join pgis_ru.regions r on c.reg_id = r.reg_id
            join pgis_ru.federal_districts fd on c.fd_id = fd.fd_id
            join pgis_ru.timezones t on c.tz_id = t.tz_id
            join pgis_ru.settlements s on c.settlement_id = s.settlement_id
            order by c.fd_id;


    -- Столицы регионов;

        create or replace view pgis_ru.v_reg_capitals as
            select
                c.reg_id as region_code,
                r.reg_name as region,
                s.settlement_name as capital_city,
                c.settlement_id as city_id,
                fd.full_name as fd_name,
                fd.short_name as fd_abbr,
                t.full_name as tz_name,
                t.short_name as tz_abbr,
                t.msk_offset,
                t.utc_offset
            from pgis_ru.reg_capitals c
            join pgis_ru.regions r on c.reg_id = r.reg_id
            join pgis_ru.federal_districts fd on c.fd_id = fd.fd_id
            join pgis_ru.timezones t on c.tz_id = t.tz_id
            join pgis_ru.settlements s on c.settlement_id = s.settlement_id
            order by c.reg_id;


    -- Отношения ФО и часовых поясов

    create or replace view pgis_ru.v_tz_fd_relations as
        select
            t.full_name as tz_name,
            t.short_name as tz_abbr,
            t.utc_offset,
            t.msk_offset,
            array_agg(f.full_name order by f.fd_id) as fd_names,
            array_agg(f.short_name order by f.fd_id) as fd_abbrs
        from pgis_ru.tz_fd_relations r
        join pgis_ru.timezones t on r.tz_id = t.tz_id
        join pgis_ru.federal_districts f on f.fd_id = any(r.fd_ids)
        group by r.tz_id, t.full_name, t.short_name, t.utc_offset, t.msk_offset
        order by r.tz_id;

    create or replace view pgis_ru.v_fd_tz_relations as
        select
            f.full_name as fd_name,
            f.short_name as fd_abbr,
            array_agg(t.full_name order by t.tz_id) as tz_names, array_agg(t.short_name order by t.tz_id) as tz_abbrs,
            array_agg(t.utc_offset order by t.tz_id) as utc_offsets, array_agg(t.msk_offset order by t.tz_id) as msk_offsets
        from pgis_ru.tz_fd_relations r
        join pgis_ru.timezones t on t.tz_id = any(r.tz_ids)
        join pgis_ru.federal_districts f on r.fd_id = f.fd_id
        group by r.fd_id, f.full_name, f.short_name
        order by r.fd_id;


    -- Список границ регионов в проекции WGS84 (EPSG 4326);
    create materialized view if not exists pgis_ru.reg_boundaries_wgs84 as
    (
        with regs as
        (
            select *
            from
            (
                select
                    reg_id,
                    reg_name,
                    x_min,
                    y_min,
                    x_max,
                    y_max,
                    st_distance(st_makepoint(x_min, y_min), st_makepoint(x_max, y_min)) as delta_lon,
                    st_distance(st_makepoint(x_min, y_min), st_makepoint(x_min, y_max)) as delta_lat,
                    envelope,
                    reg_geom
                from
                (
                    select
                        reg_id,
                        reg_name,
                        st_xmin(st_envelope(geometry)) as x_min,
                        st_ymin(st_envelope(geometry)) as y_min,
                        st_xmax(st_envelope(geometry)) as x_max,
                        st_ymax(st_envelope(geometry)) as y_max,
                        st_envelope(geometry) as envelope,
                        geometry as reg_geom
                    from pgis_ru.regions
                    where reg_id != 87
                ) reg

                union all

                select
                    reg_id,
                    reg_name,
                    x_min,
                    y_min,
                    x_max,
                    y_max,
                    st_distance(st_makepoint(x_min, y_min), st_makepoint(x_max, y_min)) as delta_lon,
                    st_distance(st_makepoint(x_min, y_min), st_makepoint(x_min, y_max)) as delta_lat,
                    envelope,
                    reg_geom
                from
                (
                    select distinct
                        reg_id,
                        reg_name,
                        x_min,
                        x_max,
                        min(y_min) over w as y_min,
                        max(y_max) over w as y_max,
                        envelope,
                        reg_geom
                    from
                    (
                        select
                            reg_id,
                            reg_name,
                            st_xmin(st_envelope(dump)) as x_min,
                            st_ymin(st_envelope(dump)) as y_min,
                            st_xmax(st_envelope(dump)) as x_max,
                            st_ymax(st_envelope(dump)) as y_max,
                            st_envelope(dump) as envelope,
                            dump as reg_geom
                        from
                        (
                            select
                                reg_id,
                                reg_name,
                                st_transform((st_dump(r.geometry)).geom, 4326) AS dump
                            from pgis_ru.regions r
                            where reg_id = 87
                        ) d
                    ) agg
                    window w as (partition by reg_id)
                ) ch
            ) a
        ),

        rus as
        (
            select
                100 as reg_id,
                'Восточное полушарие' as reg_name,
                x_min,
                y_min,
                x_max,
                y_max,
                st_distance(st_makepoint(x_min, y_min), st_makepoint(x_max, y_min)) as delta_lon,
                st_distance(st_makepoint(x_min, y_min), st_makepoint(x_min, y_max)) as delta_lat,
                envelope,
                reg_geom
            from
            (
                select
                    st_xmin(st_envelope(reg_geom)) as x_min,
                    st_ymin(st_envelope(reg_geom)) as y_min,
                    st_xmax(st_envelope(reg_geom)) as x_max,
                    st_ymax(st_envelope(reg_geom)) as y_max,
                    st_envelope(reg_geom) as envelope,
                    reg_geom
                from
                (
                    select st_union(reg_geom) as reg_geom
                    from regs
                    where x_min != -180
                ) a
            ) b

            union all

            select
                101 as reg_id,
                'Западное полушарие' as reg_name,
                x_min,
                y_min,
                x_max,
                y_max,
                st_distance(st_makepoint(x_min, y_min), st_makepoint(x_max, y_min)) as delta_lon,
                st_distance(st_makepoint(x_min, y_min), st_makepoint(x_min, y_max)) as delta_lat,
                envelope,
                reg_geom
            from
            (
                select
                    st_xmin(st_envelope(reg_geom)) as x_min,
                    st_ymin(st_envelope(reg_geom)) as y_min,
                    st_xmax(st_envelope(reg_geom)) as x_max,
                    st_ymax(st_envelope(reg_geom)) as y_max,
                    st_envelope(reg_geom) as envelope,
                    reg_geom
                from
                (
                    select st_union(reg_geom) as reg_geom
                    from regs
                    where x_min = -180
                ) a
            ) b
        )

        select *
        from
        (
            select *
            from regs

            union all

            select *
            from rus
        ) res
        order by
            reg_id,
            x_min desc
    )
    with data;

    create index wgs84_boundaries_id_idx on pgis_ru.reg_boundaries_wgs84 using btree (reg_id);
    create index wgs84_bound_envelope_geometry_idx on pgis_ru.reg_boundaries_wgs84 using gist (envelope) WITH (fillfactor='100');
    create index wgs84_bound_geometry_idx on pgis_ru.reg_boundaries_wgs84 using gist (reg_geom) WITH (fillfactor='100');

    analyze pgis_ru.reg_boundaries_wgs84;


    -- Список границ регионов в проекции Web Mercator (EPSG 3857);
    create materialized view if not exists pgis_ru.reg_boundaries_mercator as
    (
        with regs as
        (
            select *
            from
            (
                select
                    reg_id,
                    reg_name,
                    x_min,
                    y_min,
                    x_max,
                    y_max,
                    st_distance(st_makepoint(x_min, y_min), st_makepoint(x_max, y_min)) as delta_lon,
                    st_distance(st_makepoint(x_min, y_min), st_makepoint(x_min, y_max)) as delta_lat,
                    envelope,
                    reg_geom
                from
                (
                    select
                        reg_id,
                        reg_name,
                        st_xmin(st_envelope(geometry)) as x_min,
                        st_ymin(st_envelope(geometry)) as y_min,
                        st_xmax(st_envelope(geometry)) as x_max,
                        st_ymax(st_envelope(geometry)) as y_max,
                        st_envelope(geometry) as envelope,
                        geometry as reg_geom
                    from
                    (
                        select
                            reg_id,
                            reg_name,
                            st_transform(geometry, 3857) as geometry
                        from pgis_ru.regions
                        where reg_id != 87
                    ) r
                ) reg

                union all

                select
                    reg_id,
                    reg_name,
                    x_min,
                    y_min,
                    x_max,
                    y_max,
                    st_distance(st_makepoint(x_min, y_min), st_makepoint(x_max, y_min)) as delta_lon,
                    st_distance(st_makepoint(x_min, y_min), st_makepoint(x_min, y_max)) as delta_lat,
                    envelope,
                    reg_geom
                from
                (
                    select distinct
                        reg_id,
                        reg_name,
                        x_min,
                        x_max,
                        min(y_min) over w as y_min,
                        max(y_max) over w as y_max,
                        envelope,
                        reg_geom
                    from
                    (
                        select
                            reg_id,
                            reg_name,
                            st_xmin(st_envelope(dump)) as x_min,
                            st_ymin(st_envelope(dump)) as y_min,
                            st_xmax(st_envelope(dump)) as x_max,
                            st_ymax(st_envelope(dump)) as y_max,
                            st_envelope(dump) as envelope,
                            dump as reg_geom
                        from
                        (
                            select
                                reg_id,
                                reg_name,
                                st_transform((st_dump(r.geometry)).geom, 3857) AS dump
                            from
                            (
                                select
                                    reg_id,
                                    reg_name,
                                    st_transform(geometry, 3857) as geometry
                                from pgis_ru.regions
                                where reg_id = 87
                            ) r
                        ) d
                    ) agg
                    window w as (partition by reg_id)
                ) ch
            ) a
        ),

        rus as
        (
            select
                100 as reg_id,
                'Восточное полушарие' as reg_name,
                x_min,
                y_min,
                x_max,
                y_max,
                st_distance(st_makepoint(x_min, y_min), st_makepoint(x_max, y_min)) as delta_lon,
                st_distance(st_makepoint(x_min, y_min), st_makepoint(x_min, y_max)) as delta_lat,
                envelope,
                reg_geom
            from
            (
                select
                    st_xmin(st_envelope(reg_geom)) as x_min,
                    st_ymin(st_envelope(reg_geom)) as y_min,
                    st_xmax(st_envelope(reg_geom)) as x_max,
                    st_ymax(st_envelope(reg_geom)) as y_max,
                    st_envelope(reg_geom) as envelope,
                    reg_geom
                from
                (
                    select st_union(reg_geom) as reg_geom
                    from regs
                    where x_min > 0
                ) a
            ) b

            union all

            select
                101 as reg_id,
                'Западное полушарие' as reg_name,
                x_min,
                y_min,
                x_max,
                y_max,
                st_distance(st_makepoint(x_min, y_min), st_makepoint(x_max, y_min)) as delta_lon,
                st_distance(st_makepoint(x_min, y_min), st_makepoint(x_min, y_max)) as delta_lat,
                envelope,
                reg_geom
            from
            (
                select
                    st_xmin(st_envelope(reg_geom)) as x_min,
                    st_ymin(st_envelope(reg_geom)) as y_min,
                    st_xmax(st_envelope(reg_geom)) as x_max,
                    st_ymax(st_envelope(reg_geom)) as y_max,
                    st_envelope(reg_geom) as envelope,
                    reg_geom
                from
                (
                    select st_union(reg_geom) as reg_geom
                    from regs
                    where x_min < 0
                ) a
            ) b
        )

        select *
        from
        (
            select *
            from regs

            union all

            select *
            from rus
        ) res
        order by
            reg_id,
            x_min desc
        )
    with data;

    create index mercator_boundaries_id_idx on pgis_ru.reg_boundaries_mercator using btree (reg_id);
    create index mercator_bound_envelope_geometry_idx on pgis_ru.reg_boundaries_mercator using gist (envelope) WITH (fillfactor='100');
    create index mercator_bound_geometry_idx on pgis_ru.reg_boundaries_mercator using gist (reg_geom) WITH (fillfactor='100');

    analyze pgis_ru.reg_boundaries_mercator;