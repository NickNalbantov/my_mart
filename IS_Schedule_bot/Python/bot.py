"""Главный модуль, содержащий все хэндлеры и запускающий бота"""

import os
import platform
import asyncio
import configparser
from pathlib import Path

# Импорт классов для SQL
from imports.classes.Class_PostgreSQL import PostgreSQL # методы для работы с SQL-командами
from psycopg import sql # метод борьбы с SQL injection из драйвера БД

# Импорт классов для TG
from aiogram import Bot, types # базовые методы
from aiogram import Dispatcher # диспетчер сообщений
from aiogram import F # magic filters
from aiogram.filters.command import Command # фильтр команд
from aiogram.types import FSInputFile # обёртка для отправки файлов

# прочие локальные импорты
import imports.pdf_generator as pdf # генерация расписаний
from imports import keyboard # описание кнопок
from my_mart.IS_Schedule_bot.Python.imports.classes.pendulum_transform import PendulumTransform
from imports.logger import is_bot_logger


CONFIG_FILE = f'{Path(__file__).resolve().parent}/imports/classes/config.ini'

config = configparser.ConfigParser(allow_no_value = True, empty_lines_in_values = True)
config.read(CONFIG_FILE, 'utf8')

db_connect = config['DB']
tg_connect= config['TG']

# Коннект к PostgreSQL
PG = PostgreSQL(db_connect['host'], db_connect['port'], db_connect['database'], db_connect['user'], db_connect['password'])

# трансформация дат
PND = PendulumTransform()

# базовые настройки бота
bot = Bot(token = tg_connect['token'])
dp = Dispatcher()

# настройки логгера
db_schema = 'is_bot_logs'
db_log_table = 'bot_events_logs'
db_log_table_columns = ['log_time', 'chat_id', 'bot_event', 'log_level_name']
splitter = ' ~ '
log_level = 'DEBUG'

db_logger = is_bot_logger(db_log_table, db_log_table_columns, db_schema, splitter, log_level)

# если у пользователя нет ника - используем имя/фамилию/id чата
def username_check(username: str, first_name: str, last_name: str, chat_id: str) -> str:
    "Обработка имени пользователя"
    if username != 'None':
        return username
    if username == 'None' and first_name != 'None' and last_name != 'None':
        return f"{first_name} {last_name}"
    if username == 'None' and first_name != 'None' and last_name == 'None':
        return first_name
    if username == 'None' and first_name == 'None' and last_name != 'None':
        return last_name
    return chat_id


def chat_params(obj: types.Message | types.CallbackQuery):
    "Получение параметров чата с пользователем"
    if isinstance(obj, types.Message) is True:
        chat_id = str(obj.chat.id)
        first_name = str(obj.from_user.first_name)
        last_name = str(obj.from_user.last_name)
        username = str(obj.from_user.username)
    elif isinstance(obj, types.CallbackQuery) is True:
        chat_id = str(obj.message.chat.id)
        first_name = str(obj.message.chat.first_name)
        last_name = str(obj.message.chat.last_name)
        username = str(obj.message.chat.username)
    else:
        return None, None, None, None, None

    username_fin = username_check(username, first_name, last_name, chat_id)

    return chat_id, first_name, last_name, username, username_fin


# проверяем наличие PDF; генерируем, если его нет
def pdf_handler(
        subgroup: str = '',
        user_input: str | int = None,
        yr: str = PND.dmy_now().format('YYYY'),
        today: bool = False,
        curr_week: bool = False,
        next_week: bool = False,
        date_input: bool = False,
        week_input_by_date: bool = False,
        week_input_by_num: bool = False,
        two_weeks: bool = False,
        numerator: bool = False,
        denominator: bool = False
    ):
    """Запуск обработчика PDF.
    На вход получает имя подгруппы, пользовательский ввод (если есть) и флаг сценария.
    Возвращает полный путь к PDF-файлу и его имя"""

    # выбираем имя PDF
    storage_type = ''
    if today is True:
        pdf_name = f"IS_{subgroup}_{PND.dmy_now().format('DD.MM')}_schedule"
        storage_type = 'temporary'
    if curr_week is True:
        pdf_name = f'IS_{subgroup}_week_{PND.curr_week_number()}_schedule'
        storage_type = 'temporary'
    if next_week is True:
        pdf_name = f'IS_{subgroup}_week_{PND.curr_week_number() + 1}_schedule'
        storage_type = 'temporary'
    if date_input is True and user_input is not None:
        pdf_name = f'IS_{subgroup}_{user_input}.{yr[-2:]}_schedule'
        storage_type = 'temporary'
    if week_input_by_date is True and user_input is not None:
        pdf_name = f"""IS_{subgroup}_week_{PND.get_week_number(f'{user_input}.{yr[-2:]}')}_schedule"""
        storage_type = 'temporary'
    if week_input_by_num is True and user_input is not None:
        pdf_name =  f'IS_{subgroup}_week_{user_input}_year_{yr}_schedule'
        storage_type = 'temporary'
    if two_weeks is True:
        pdf_name = f'IS_{subgroup}_two_weeks_schedule'
        storage_type = 'persistent'
    if numerator is True:
        pdf_name = f'IS_{subgroup}_numerator_schedule'
        storage_type = 'persistent'
    if denominator is True:
        pdf_name = f'IS_{subgroup}_denominator_schedule'
        storage_type = 'persistent'

    if user_input is None and (date_input is True or week_input_by_date is True or week_input_by_num is True):
        return None, None

    # проверка готовности файла
    if platform.system() == 'Linux':
        full_path = f'{os.path.dirname(os.path.abspath(__file__))}/imports/classes/pdf_stash/{storage_type}/{pdf_name}.pdf'
    else:
        full_path = f'{os.path.dirname(os.path.abspath(__file__))}\\imports\\classes\\pdf_stash\\{storage_type}\\{pdf_name}.pdf'

    file_check = os.path.isfile(full_path)
    if file_check:
        return full_path, pdf_name
    else:
        try:
            # выбираем функцию генерации PDF
            if today is True:
                return pdf.pdf_today_schedule(subgroup)
            if curr_week is True:
                return pdf.pdf_curr_week_schedule(subgroup)
            if next_week is True:
                return pdf.pdf_next_week_schedule(subgroup)
            if date_input is True:
                return pdf.pdf_date_input_schedule(subgroup, user_input)
            if week_input_by_date is True:
                return pdf.pdf_week_input_by_date_schedule(subgroup, user_input)
            if week_input_by_num is True:
                return pdf.pdf_week_input_by_week_number_schedule(subgroup, user_input)
            if two_weeks is True:
                pdf.pdf_two_weeks_schedule()
                return full_path, pdf_name
            if numerator is True:
                pdf.pdf_numerator_schedule()
                return full_path, pdf_name
            if denominator is True:
                pdf.pdf_denominator_schedule()
                return full_path, pdf_name
        except Exception:
            return None, None



# handlers

    # help
@dp.message(Command(commands = ['help', 'помощь'], ignore_case = True))
async def help_command(message: types.Message):
    "Сценарий /help"
    # параметры чата и логирование старта сценария
    chat_id, *_, username_fin = chat_params(message)
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} запустил сценарий /help"
    db_logger.info(log_data)
    # ответ
    await message.answer(
        text = "Даже не знаю, чем я могу тебе помочь 🤔\nПотыкайся в кнопки меню сам, там всё понятно))",
        parse_mode = 'HTML',
        reply_markup = keyboard.start.as_markup()
        )

    # start
@dp.message(Command(commands = ['start', 'старт'], ignore_case = True))
async def start_command(message: types.Message):
    "Сценарий /start"
        # параметры чата
    chat_id, first_name, last_name, username, username_fin = chat_params(message)
        # сканирование PG
    query = sql.SQL("""
        select count(*) as cnt
        from is_bot_logs.bot_users
        where chat_id = {}""").format(sql.Literal(chat_id))
    result = PG.fetch_one(query)
        # регистрация нового пользователя в БД
    if result == 0:
        reg_query = sql.SQL("""
            insert into is_bot_logs.bot_users
            (
                chat_id,
                first_name,
                last_name,
                username
            )
            values
            (
                {chat_id},
                {first_name},
                {last_name},
                {username}
            )""").format(
                        chat_id = sql.Literal(chat_id),
                        first_name = sql.Literal(first_name),
                        last_name = sql.Literal(last_name),
                        username = sql.Literal(username)
                        )
        PG.execute(reg_query) # выполняем отдельно, т.к. надо закоммитить значение вторичного ключа chat_id
        state_query = sql.SQL("""
            insert into is_bot_fsm.user_scenarios_states
            values ({}, 0, 0, 0, 0, 0, 0, 0, 0, 0)""").format(sql.Literal(chat_id))
        input_query = sql.SQL("""
            insert into is_bot_fsm.user_inputs_states
            values ({}, NULL, NULL, NULL)""").format(sql.Literal(chat_id))
        queries = [state_query, input_query]
        PG.execute_multiple(queries)
        log_data = f"{chat_id}{splitter}Зарегистрирован новый юзер - {username_fin}"
        db_logger.info(log_data)
    else:
        state_query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set
                today = 0,
                curr_week = 0,
                next_week = 0,
                two_weeks = 0,
                numerator = 0,
                denominator = 0,
                date_input = 0,
                week_input_by_date = 0,
                week_input_by_num = 0
            where chat_id = {}""").format(sql.Literal(chat_id))
        input_query = sql.SQL("""
            update is_bot_fsm.user_inputs_states
            set
                date_input = NULL,
                week_input_by_date = NULL,
                week_input_by_num = NULL
            where chat_id = {}""").format(sql.Literal(chat_id))
        queries = [state_query, input_query]
        PG.execute_multiple(queries)
    # ответ
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} открыл чат с ботом"
    db_logger.info(log_data)
    await message.answer(
        text = f"Привет, {first_name}!\nКакое расписание тебя интересует?",
        parse_mode = 'HTML',
        reply_markup = keyboard.start.as_markup()
        )

    # today
        # запуск сценария
@dp.callback_query(F.data == 'today')
async def today_callback(callback_query: types.CallbackQuery):
    "Запуск сценария для получения расписания на сегодня"
    # параметры чата и логирование старта сценария
    chat_id, *_, username_fin = chat_params(callback_query)
    # ответ
    if callback_query.data and callback_query.data == "today":
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set {} = 1
            where chat_id = {}""").format(sql.Identifier(callback_query.data), sql.Literal(chat_id))
        PG.execute(query)
        log_data = f"{chat_id}{splitter}Пользователь {username_fin} запустил сценарий {callback_query.data}"
        db_logger.info(log_data)
    await callback_query.message.answer('Выбери группу:', reply_markup = keyboard.today_group.as_markup())

        # выбор группы
@dp.callback_query(F.data.startswith('today_group_'))
async def today_group_callback(callback_query: types.CallbackQuery):
    "Запуск сценария выбора группы для получения расписания на сегодняшний день"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # проверка состояния
    query = sql.SQL("""
        select today
        from is_bot_fsm.user_scenarios_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    state = PG.fetch_one(query)
    # генерация результата
    if callback_query.data and callback_query.data.startswith("today_group_") and state == 1:
        subgroup = callback_query.data[-3:]
        schedule_file, pdf_name = pdf_handler(subgroup = subgroup, today = True)
        # сброс состояния
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set today = 0
            where chat_id = {}""").format(sql.Literal(chat_id))
        PG.execute(query)
        # ответ
        if schedule_file is not None:
            await callback_query.message.answer_document(FSInputFile(schedule_file, pdf_name))
            await callback_query.message.answer(text = 'PDF-файл с расписанием во вложении 👆', reply_markup = keyboard.back_today.as_markup())
            log_data = f"{chat_id}{splitter}Успешно отработан сценарий {callback_query.data} и сформирован файл {pdf_name} для {username_fin}"
            db_logger.info(log_data)
        else:
            await callback_query.message.answer(text = 'Не удалось сформировать расписание по этому запросу.', reply_markup = keyboard.back_today.as_markup())
            log_data = f"{chat_id}{splitter}Отработан сценарий {callback_query.data} для {username_fin}, но файл не был сформирован"
            db_logger.warning(log_data)


    # curr_week
@dp.callback_query(F.data == 'curr_week')
async def curr_week_callback(callback_query: types.CallbackQuery):
    "Запуск сценария для получения расписания на текущую неделю"
    # параметры чата и логирование старта сценария
    chat_id, *_, username_fin = chat_params(callback_query)
    # ответ
    if callback_query.data and callback_query.data == "curr_week":
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set {} = 1
            where chat_id = {}""").format(sql.Identifier(callback_query.data), sql.Literal(chat_id))
        PG.execute(query)
        log_data = f"{chat_id}{splitter}Пользователь {username_fin} запустил сценарий {callback_query.data}"
        db_logger.info(log_data)
    await callback_query.message.answer('Выбери группу:', reply_markup = keyboard.curr_week_group.as_markup())

@dp.callback_query(F.data.startswith('curr_week_group_'))
async def curr_week_group_callback(callback_query: types.CallbackQuery):
    "Запуск сценария выбора группы для получения расписания на текущую неделю"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # проверка состояния
    query = sql.SQL("""
        select curr_week
        from is_bot_fsm.user_scenarios_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    state = PG.fetch_one(query)
    # генерация результата
    if callback_query.data and callback_query.data.startswith("curr_week_group_") and state == 1:
        subgroup = callback_query.data[-3:]
        schedule_file, pdf_name = pdf_handler(subgroup = subgroup, curr_week = True)
        # сброс состояния
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set curr_week = 0
            where chat_id = {}""").format(sql.Literal(chat_id))
        PG.execute(query)
        # ответ
        if schedule_file is not None:
            await callback_query.message.answer_document(FSInputFile(schedule_file, pdf_name))
            await callback_query.message.answer(text = 'PDF-файл с расписанием во вложении 👆', reply_markup = keyboard.back_curr_week.as_markup())
            log_data = f"{chat_id}{splitter}Успешно отработан сценарий {callback_query.data} и сформирован файл {pdf_name} для {username_fin}"
            db_logger.info(log_data)
        else:
            await callback_query.message.answer(text = 'Не удалось сформировать расписание по этому запросу.', reply_markup = keyboard.back_curr_week.as_markup())
            log_data = f"{chat_id}{splitter}Отработан сценарий {callback_query.data} для {username_fin}, но файл не был сформирован"
            db_logger.warning(log_data)

    # next_week
@dp.callback_query(F.data == 'next_week')
async def next_week_callback(callback_query: types.CallbackQuery):
    "Запуск сценария для получения расписания на следующую неделю"
    # параметры чата и логирование старта сценария
    chat_id, *_, username_fin = chat_params(callback_query)
    # ответ
    if callback_query.data and callback_query.data == "next_week":
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set {} = 1
            where chat_id = {}""").format(sql.Identifier(callback_query.data), sql.Literal(chat_id))
        PG.execute(query)
        log_data = f"{chat_id}{splitter}Пользователь {username_fin} запустил сценарий {callback_query.data}"
        db_logger.info(log_data)
    await callback_query.message.answer('Выбери группу:', reply_markup = keyboard.next_week_group.as_markup())

@dp.callback_query(F.data.startswith('next_week_group_'))
async def next_week_group_callback(callback_query: types.CallbackQuery):
    "Запуск сценария выбора группы для получения расписания на следующую неделю"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # проверка состояния
    query = sql.SQL("""
        select next_week
        from is_bot_fsm.user_scenarios_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    state = PG.fetch_one(query)
    # генерация результата
    if callback_query.data and callback_query.data.startswith("next_week_group_") and state == 1:
        subgroup = callback_query.data[-3:]
        schedule_file, pdf_name = pdf_handler(subgroup = subgroup, next_week = True)
        # сброс состояния
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set next_week = 0
            where chat_id = {}""").format(sql.Literal(chat_id))
        PG.execute(query)
        # ответ
        if schedule_file is not None:
            await callback_query.message.answer_document(FSInputFile(schedule_file, pdf_name))
            await callback_query.message.answer(text = 'PDF-файл с расписанием во вложении 👆', reply_markup = keyboard.back_next_week.as_markup())
            log_data = f"{chat_id}{splitter}Успешно отработан сценарий {callback_query.data} и сформирован файл {pdf_name} для {username_fin}"
            db_logger.info(log_data)
        else:
            await callback_query.message.answer(text = 'Не удалось сформировать расписание по этому запросу.', reply_markup = keyboard.back_next_week.as_markup())
            log_data = f"{chat_id}{splitter}Отработан сценарий {callback_query.data} для {username_fin}, но файл не был сформирован"
            db_logger.warning(log_data)

    # two_weeks
@dp.callback_query(F.data == 'two_weeks')
async def two_weeks_callback(callback_query: types.CallbackQuery):
    "Запуск сценария для получения расписания на две недели"
    # параметры чата и логирование старта сценария
    chat_id, *_, username_fin = chat_params(callback_query)
    # ответ
    if callback_query.data and callback_query.data == "two_weeks":
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set {} = 1
            where chat_id = {}""").format(sql.Identifier(callback_query.data), sql.Literal(chat_id))
        PG.execute(query)
        log_data = f"{chat_id}{splitter}Пользователь {username_fin} запустил сценарий {callback_query.data}"
        db_logger.info(log_data)
    await callback_query.message.answer('Выбери группу:', reply_markup = keyboard.two_weeks_group.as_markup())

@dp.callback_query(F.data.startswith('two_weeks_group_'))
async def two_weeks_group_callback(callback_query: types.CallbackQuery):
    "Запуск сценария выбора группы для получения расписания на две недели"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # проверка состояния
    query = sql.SQL("""
        select two_weeks
        from is_bot_fsm.user_scenarios_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    state = PG.fetch_one(query)
    # генерация результата
    if callback_query.data and callback_query.data.startswith("two_weeks_group_") and state == 1:
        subgroup = callback_query.data[-3:]
        schedule_file, pdf_name = pdf_handler(subgroup = subgroup, two_weeks = True)
        # сброс состояния
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set two_weeks = 0
            where chat_id = {}""").format(sql.Literal(chat_id))
        PG.execute(query)
        # ответ
        if schedule_file is not None:
            await callback_query.message.answer_document(FSInputFile(schedule_file, pdf_name))
            await callback_query.message.answer(text = 'PDF-файл с расписанием во вложении 👆', reply_markup = keyboard.back_two_weeks.as_markup())
            log_data = f"{chat_id}{splitter}Успешно отработан сценарий {callback_query.data} и сформирован файл {pdf_name} для {username_fin}"
            db_logger.info(log_data)
        else:
            await callback_query.message.answer(text = 'Не удалось сформировать расписание по этому запросу.', reply_markup = keyboard.back_two_weeks.as_markup())
            log_data = f"{chat_id}{splitter}Отработан сценарий {callback_query.data} для {username_fin}, но файл не был сформирован"
            db_logger.warning(log_data)

    # numerator
@dp.callback_query(F.data == 'numerator')
async def numerator_callback(callback_query: types.CallbackQuery):
    "Запуск сценария для получения расписания на неделю по числителю"
    # параметры чата и логирование старта сценария
    chat_id, *_, username_fin = chat_params(callback_query)
    # обработка
    if callback_query.data and callback_query.data == "numerator":
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set {} = 1
            where chat_id = {}""").format(sql.Identifier(callback_query.data), sql.Literal(chat_id))
        PG.execute(query)
        log_data = f"{chat_id}{splitter}Пользователь {username_fin} запустил сценарий {callback_query.data}"
        db_logger.info(log_data)
    # ответ
    await callback_query.message.answer('Выбери группу:', reply_markup = keyboard.numerator_group.as_markup())

@dp.callback_query(F.data.startswith('numerator_group_'))
async def numerator_group_callback(callback_query: types.CallbackQuery):
    "Запуск сценария выбора группы для получения расписания на неделю по числителю"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # проверка состояния
    query = sql.SQL("""
        select numerator
        from is_bot_fsm.user_scenarios_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    state = PG.fetch_one(query)
    # генерация результата
    if callback_query.data and callback_query.data.startswith("numerator_group_") and state == 1:
        subgroup = callback_query.data[-3:]
        schedule_file, pdf_name = pdf_handler(subgroup = subgroup, numerator = True)
        # сброс состояния
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set numerator = 0
            where chat_id = {}""").format(sql.Literal(chat_id))
        PG.execute(query)
        # ответ
        if schedule_file is not None:
            await callback_query.message.answer_document(FSInputFile(schedule_file, pdf_name))
            await callback_query.message.answer(text = 'PDF-файл с расписанием во вложении 👆', reply_markup = keyboard.back_numerator.as_markup())
            log_data = f"{chat_id}{splitter}Успешно отработан сценарий {callback_query.data} и сформирован файл {pdf_name} для {username_fin}"
            db_logger.info(log_data)
        else:
            await callback_query.message.answer(text = 'Не удалось сформировать расписание по этому запросу.', reply_markup = keyboard.back_numerator.as_markup())
            log_data = f"{chat_id}{splitter}Отработан сценарий {callback_query.data} для {username_fin}, но файл не был сформирован"
            db_logger.warning(log_data)

    # denominator
@dp.callback_query(F.data == 'denominator')
async def denominator_callback(callback_query: types.CallbackQuery):
    "Запуск сценария выбора группы для получения расписания на неделю по знаменателю"
    # параметры чата и логирование старта сценария
    chat_id, *_, username_fin = chat_params(callback_query)
    # обработка
    if callback_query.data and callback_query.data == "denominator":
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set {} = 1
            where chat_id = {}""").format(sql.Identifier(callback_query.data), sql.Literal(chat_id))
        PG.execute(query)
        log_data = f"{chat_id}{splitter}Пользователь {username_fin} запустил сценарий {callback_query.data}"
        db_logger.info(log_data)
    # ответ
    await callback_query.message.answer('Выбери группу:', reply_markup = keyboard.denominator_group.as_markup())

@dp.callback_query(F.data.startswith('denominator_group_'))
async def denominator_group_callback(callback_query: types.CallbackQuery):
    "Запуск сценария выбора группы для получения расписания на неделю по знаменателю"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # проверка состояния
    query = sql.SQL("""
        select denominator
        from is_bot_fsm.user_scenarios_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    state = PG.fetch_one(query)
    # генерация результата
    if callback_query.data and callback_query.data.startswith("denominator_group_") and state == 1:
        subgroup = callback_query.data[-3:]
        schedule_file, pdf_name = pdf_handler(subgroup = subgroup, denominator = True)
        # сброс состояния
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set denominator = 0
            where chat_id = {}""").format(sql.Literal(chat_id))
        PG.execute(query)
        # ответ
        if schedule_file is not None:
            await callback_query.message.answer_document(FSInputFile(schedule_file, pdf_name))
            await callback_query.message.answer(text = 'PDF-файл с расписанием во вложении 👆', reply_markup = keyboard.back_denominator.as_markup())
            log_data = f"{chat_id}{splitter}Успешно отработан сценарий {callback_query.data} и сформирован файл {pdf_name} для {username_fin}"
            db_logger.info(log_data)
        else:
            await callback_query.message.answer(text = 'Не удалось сформировать расписание по этому запросу.', reply_markup = keyboard.back_denominator.as_markup())
            log_data = f"{chat_id}{splitter}Отработан сценарий {callback_query.data} для {username_fin}, но файл не был сформирован"
            db_logger.warning(log_data)

# хэндлеры с вводом

    # date_input
@dp.callback_query(F.data == 'date_input')
async def date_input_callback(callback_query: types.CallbackQuery):
    "Запуск сценария для получения расписания на выбранную дату"
    # параметры чата и логирование старта сценария
    chat_id, *_, username_fin = chat_params(callback_query)
    # обработка
    if callback_query.data and callback_query.data == "date_input":
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set {} = 1
            where chat_id = {}""").format(sql.Identifier(callback_query.data), sql.Literal(chat_id))
        PG.execute(query)
        log_data = f"{chat_id}{splitter}Пользователь {username_fin} запустил сценарий {callback_query.data}"
        db_logger.info(log_data)
    # ответ
    await callback_query.message.answer("""📅 Введи 'день_ДД.ММ'\n\n📅 Пример команды день_17.03""", parse_mode = 'HTML')

        # дополнительная ступень - запись ввода
@dp.message(F.text.regexp(r'день_\d{1,2}\.\d{1,2}'))
async def date_input_input_message(message: types.Message):
    "Обработка пользовательского ввода даты в сценарии для получения расписания на выбранную дату"
    # параметры чата
    chat_id, *_, username_fin = chat_params(message)
    # логируем как бот принял ввод
    log_data = f"{chat_id}{splitter}От пользователя {username_fin} принята команда '{message.text}' и обработана в переменную '{message.text[-5:]}'"
    db_logger.debug(log_data)
    # проверка состояния
    state_query = sql.SQL("""
        select date_input
        from is_bot_fsm.user_scenarios_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    state = PG.fetch_one(state_query)
    # запись ввода
    if state == 1:
        query = sql.SQL("""
            update is_bot_fsm.user_inputs_states
            set date_input = {input}
            where chat_id = {chat_id}""").format(input = sql.Literal(message.text[-5:]), chat_id = sql.Literal(chat_id))
        PG.execute(query)
    # ответ
        await message.answer("""Выбери группу: """, reply_markup = keyboard.date_input_group.as_markup())
    else:
        await message.answer("""Рано вводишь дату))\nСначала выбери пункт меню:""", parse_mode = 'HTML', reply_markup = keyboard.start.as_markup())

@dp.callback_query(F.data.startswith('date_input_group_'))
async def date_input_group_callback(callback_query: types.CallbackQuery):
    "Запуск сценария выбора группы для получения расписания на выбранную дату"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # проверка состояния и получение ввода
    state_query = sql.SQL("""
        select date_input
        from is_bot_fsm.user_scenarios_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    input_query = sql.SQL("""
        select date_input
        from is_bot_fsm.user_inputs_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    state = PG.fetch_one(state_query)
    user_input = PG.fetch_one(input_query)
    # генерация результата
    if callback_query.data and callback_query.data.startswith("date_input_group_") and state == 1:
        subgroup = callback_query.data[-3:]
        schedule_file, pdf_name = pdf_handler(subgroup = subgroup, user_input = user_input, date_input = True)
        # сброс состояния; ввод не затираем на случай возврата
        state_query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set date_input = 0
            where chat_id = {}""").format(sql.Literal(chat_id))
        PG.execute(state_query)
        # ответ
        if schedule_file is not None:
            await callback_query.message.answer_document(FSInputFile(schedule_file, pdf_name))
            await callback_query.message.answer(text = 'PDF-файл с расписанием во вложении 👆', reply_markup = keyboard.back_to_input_by_date.as_markup())
            log_data = f"{chat_id}{splitter}Успешно отработан сценарий {callback_query.data} и сформирован файл {pdf_name} для {username_fin}"
            db_logger.info(log_data)
        else:
            await callback_query.message.answer(text = 'Не удалось сформировать расписание по этому запросу.', reply_markup = keyboard.back_to_input_by_date.as_markup())
            log_data = f"{chat_id}{splitter}Отработан сценарий {callback_query.data} для {username_fin}, но файл не был сформирован"
            db_logger.warning(log_data)

    # week_input_by_date
@dp.callback_query(F.data == 'week_input_by_date')
async def week_input_by_date_callback(callback_query: types.CallbackQuery):
    "Запуск сценария для получения расписания на выбранную неделю (выбор по дате)"
    # параметры чата и логирование старта сценария
    chat_id, *_, username_fin = chat_params(callback_query)
    # обработка
    if callback_query.data and callback_query.data == "week_input_by_date":
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set {} = 1
            where chat_id = {}""").format(sql.Identifier(callback_query.data), sql.Literal(chat_id))
        PG.execute(query)
        log_data = f"{chat_id}{splitter}Пользователь {username_fin} запустил сценарий {callback_query.data}"
        db_logger.info(log_data)
    # ответ
    await callback_query.message.answer("""📅 Введи 'неделя_ДД.ММ'\n\n📅 Пример команды неделя_17.03""", parse_mode = 'HTML')

@dp.message(F.text.regexp(r'неделя_\d{1,2}\.\d{1,2}'))
async def week_input_by_date_input_message(message: types.Message):
    "Обработка пользовательского ввода даты в сценарии для получения расписания на выбранную неделю (выбор по дате)"
    # параметры чата
    chat_id, *_, username_fin = chat_params(message)
    # логируем как бот принял ввод
    log_data = f"{chat_id}{splitter}От пользователя {username_fin} принята команда '{message.text}' и обработана в переменную '{message.text[-5:]}'"
    db_logger.debug(log_data)
    # проверка состояния
    state_query = sql.SQL("""
        select week_input_by_date
        from is_bot_fsm.user_scenarios_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    state = PG.fetch_one(state_query)
    # запись ввода
    if state == 1:
        query = sql.SQL("""
            update is_bot_fsm.user_inputs_states
            set week_input_by_date = {input}
            where chat_id = {chat_id}""").format(input = sql.Literal(message.text[-5:]), chat_id = sql.Literal(chat_id))
        PG.execute(query)
    # ответ
        await message.answer("""Выбери группу: """, reply_markup = keyboard.week_input_by_date_group.as_markup())
    else:
        await message.answer("""Рано вводишь дату))\nСначала выбери пункт меню:""", parse_mode = 'HTML', reply_markup = keyboard.start.as_markup())

@dp.callback_query(F.data.startswith('week_input_by_date_group_'))
async def week_input_by_date_group_callback(callback_query: types.CallbackQuery):
    "Запуск сценария выбора группы для получения расписания на выбранную неделю (выбор по дате)"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # проверка состояния и получение ввода
    state_query = sql.SQL("""
        select week_input_by_date
        from is_bot_fsm.user_scenarios_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    input_query = sql.SQL("""
        select week_input_by_date
        from is_bot_fsm.user_inputs_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    state = PG.fetch_one(state_query)
    user_input = PG.fetch_one(input_query)
    # генерация результата
    if callback_query.data and callback_query.data.startswith("week_input_by_date_group_") and state == 1:
        subgroup = callback_query.data[-3:]
        schedule_file, pdf_name = pdf_handler(subgroup = subgroup, user_input = user_input, week_input_by_date = True)
        # сброс состояния; ввод не затираем на случай возврата
        state_query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set week_input_by_date = 0
            where chat_id = {}""").format(sql.Literal(chat_id))
        PG.execute(state_query)
        # ответ
        if schedule_file is not None:
            await callback_query.message.answer_document(FSInputFile(schedule_file, pdf_name))
            await callback_query.message.answer(text = 'PDF-файл с расписанием во вложении 👆', reply_markup = keyboard.back_to_week_input_by_date.as_markup())
            log_data = f"{chat_id}{splitter}Успешно отработан сценарий {callback_query.data} и сформирован файл {pdf_name} для {username_fin}"
            db_logger.info(log_data)
        else:
            await callback_query.message.answer(text = 'Не удалось сформировать расписание по этому запросу.', reply_markup = keyboard.back_to_week_input_by_date.as_markup())
            log_data = f"{chat_id}{splitter}Отработан сценарий {callback_query.data} для {username_fin}, но файл не был сформирован"
            db_logger.warning(log_data)

    # week_input_by_week_num
@dp.callback_query(F.data == 'week_input_by_num')
async def week_input_by_week_num_callback(callback_query: types.CallbackQuery):
    "Запуск сценария для получения расписания на выбранную неделю (выбор по номеру недели)"
    # параметры чата и логирование старта сценария
    chat_id, *_, username_fin = chat_params(callback_query)
    # обработка
    if callback_query.data and callback_query.data == "week_input_by_num":
        query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set {} = 1
            where chat_id = {}""").format(sql.Identifier(callback_query.data), sql.Literal(chat_id))
        PG.execute(query)
        log_data = f"{chat_id}{splitter}Пользователь {username_fin} запустил сценарий {callback_query.data}"
        db_logger.info(log_data)
    # ответ
    await callback_query.message.answer("""🔢 Введи 'номер недели':\n\n🔢 Пример команды: 11""", parse_mode = 'HTML')

@dp.message(F.text.regexp(r'\d{1,2}'))
async def week_input_by_week_num_input_message(message: types.Message):
    "Обработка пользовательского ввода даты в сценарии для получения расписания на выбранную неделю (выбор по номеру недели)"
    # параметры чата
    chat_id, *_, username_fin = chat_params(message)
    # логируем как бот принял ввод
    log_data = f"{chat_id}{splitter}От пользователя {username_fin} принята команда '{message.text}' и обработана в переменную '{int(message.text[1:])}'"
    db_logger.debug(log_data)
    # проверка состояния
    state_query = sql.SQL("""
        select week_input_by_num
        from is_bot_fsm.user_scenarios_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    state = PG.fetch_one(state_query)
    # запись ввода
    if state == 1:
        query = sql.SQL("""
            update is_bot_fsm.user_inputs_states
            set week_input_by_num = {input}
            where chat_id = {chat_id}""").format(input = sql.Literal(int(message.text[1:])), chat_id = sql.Literal(chat_id))
        PG.execute(query)
    # ответ
        await message.answer("""Выбери группу: """, reply_markup = keyboard.week_input_by_num_group.as_markup())
    else:
        await message.answer("""Что это? Номер недели?)\nСначала выбери пункт меню:""", parse_mode = 'HTML', reply_markup = keyboard.start.as_markup())


@dp.callback_query(F.data.startswith('week_input_by_num_group_'))
async def week_input_by_week_num_group_callback(callback_query: types.CallbackQuery):
    "Запуск сценария выбора группы для получения расписания на выбранную неделю (выбор по номеру недели)"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # проверка состояния и получение ввода
    state_query = sql.SQL("""
        select week_input_by_num
        from is_bot_fsm.user_scenarios_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    input_query = sql.SQL("""
        select week_input_by_num
        from is_bot_fsm.user_inputs_states
        where chat_id = {}""").format(sql.Literal(chat_id))
    state = PG.fetch_one(state_query)
    user_input = PG.fetch_one(input_query)
    # генерация результата
    if callback_query.data and callback_query.data.startswith("week_input_by_num_group_") and state == 1:
        subgroup = callback_query.data[-3:]
        schedule_file, pdf_name = pdf_handler(subgroup = subgroup, user_input = user_input, week_input_by_num = True)
        # сброс состояния; ввод не затираем на случай возврата
        state_query = sql.SQL("""
            update is_bot_fsm.user_scenarios_states
            set week_input_by_date = 0
            where chat_id = {}""").format(sql.Literal(chat_id))
        PG.execute(state_query)
        # ответ
        if schedule_file is not None:
            await callback_query.message.answer_document(FSInputFile(schedule_file, pdf_name))
            await callback_query.message.answer(text = 'PDF-файл с расписанием во вложении 👆', reply_markup = keyboard.back_to_week_input_by_num.as_markup())
            log_data = f"{chat_id}{splitter}Успешно отработан сценарий {callback_query.data} и сформирован файл {pdf_name} для {username_fin}"
            db_logger.info(log_data)
        else:
            await callback_query.message.answer(text = 'Не удалось сформировать расписание по этому запросу.', reply_markup = keyboard.back_to_week_input_by_num.as_markup())
            log_data = f"{chat_id}{splitter}Отработан сценарий {callback_query.data} для {username_fin}, но файл не был сформирован"
            db_logger.warning(log_data)


# возвраты

    # возвраты к группам
@dp.callback_query(F.data == 'today_step_back_to_group')
async def today_step_back_to_group_callback(callback_query: types.CallbackQuery):
    "Возврат в меню выбора группы из сценария для получения расписания на сегодняшний день"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # возврат состояния состояний
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set today = 1
        where chat_id = {}""").format(sql.Literal(chat_id))
    PG.execute(state_query)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в меню выбора подгрупп через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = """Вернулся в меню выбора подгруппы.\nПовтори выбор:""",
        parse_mode = 'HTML',
        reply_markup = keyboard.today_group.as_markup()
    )

@dp.callback_query(F.data == 'curr_week_step_back_to_group')
async def curr_week_step_back_to_group_callback(callback_query: types.CallbackQuery):
    "Возврат в меню выбора группы из сценария для получения расписания на текущую неделю"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # возврат состояния состояний
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set curr_week = 1
        where chat_id = {}""").format(sql.Literal(chat_id))
    PG.execute(state_query)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в меню выбора подгрупп через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = """Вернулся в меню выбора подгруппы.\nПовтори выбор:""",
        parse_mode = 'HTML',
        reply_markup = keyboard.curr_week_group.as_markup()
    )

@dp.callback_query(F.data == 'next_week_step_back_to_group')
async def next_week_step_back_to_group_callback(callback_query: types.CallbackQuery):
    "Возврат в меню выбора группы из сценария для получения расписания на следующую неделю"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # возврат состояния состояний
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set next_week = 1
        where chat_id = {}""").format(sql.Literal(chat_id))
    PG.execute(state_query)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в меню выбора подгрупп через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = """Вернулся в меню выбора подгруппы.\nПовтори выбор:""",
        parse_mode = 'HTML',
        reply_markup = keyboard.next_week_group.as_markup()
    )

@dp.callback_query(F.data == 'two_weeks_step_back_to_group')
async def two_weeks_step_back_to_group_callback(callback_query: types.CallbackQuery):
    "Возврат в меню выбора группы из сценария для получения расписания на две недели"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # возврат состояния состояний
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set two_weeks = 1
        where chat_id = {}""").format(sql.Literal(chat_id))
    PG.execute(state_query)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в меню выбора подгрупп через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = """Вернулся в меню выбора подгруппы.\nПовтори выбор:""",
        parse_mode = 'HTML',
        reply_markup = keyboard.two_weeks_group.as_markup()
    )

@dp.callback_query(F.data == 'numerator_step_back_to_group')
async def numerator_step_back_to_group_callback(callback_query: types.CallbackQuery):
    "Возврат в меню выбора группы из сценария для получения расписания на неделю по числителю"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # возврат состояния состояний
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set numerator = 1
        where chat_id = {}""").format(sql.Literal(chat_id))
    PG.execute(state_query)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в меню выбора подгрупп через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = """Вернулся в меню выбора подгруппы.\nПовтори выбор:""",
        parse_mode = 'HTML',
        reply_markup = keyboard.numerator_group.as_markup()
    )

@dp.callback_query(F.data == 'denominator_step_back_to_group')
async def denominator_step_back_to_group_callback(callback_query: types.CallbackQuery):
    "Возврат в меню выбора группы из сценария для получения расписания на неделю по знаменателю"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # возврат состояния состояний
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set denominator = 1
        where chat_id = {}""").format(sql.Literal(chat_id))
    PG.execute(state_query)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в меню выбора подгрупп через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = """Вернулся в меню выбора подгруппы.\nПовтори выбор:""",
        parse_mode = 'HTML',
        reply_markup = keyboard.denominator_group.as_markup()
    )

@dp.callback_query(F.data == 'date_input_step_back_to_group')
async def date_input_step_back_to_group_callback(callback_query: types.CallbackQuery):
    "Возврат в меню выбора группы из сценария для получения расписания на выбранную дату"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # возврат состояния состояний
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set date_input = 1
        where chat_id = {}""").format(sql.Literal(chat_id))
    PG.execute(state_query)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в меню выбора подгрупп через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = """Вернулся в меню выбора подгруппы.\nПовтори выбор:""",
        parse_mode = 'HTML',
        reply_markup = keyboard.date_input_group.as_markup()
    )

@dp.callback_query(F.data == 'week_input_by_date_step_back_to_group')
async def week_input_by_date_step_back_to_group_callback(callback_query: types.CallbackQuery):
    "Возврат в меню выбора группы из сценария для получения расписания на выбранную неделю (выбор по дате)"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # возврат состояния состояний
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set week_input_by_date = 1
        where chat_id = {}""").format(sql.Literal(chat_id))
    PG.execute(state_query)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в меню выбора подгрупп через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = """Вернулся в меню выбора подгруппы.\nПовтори выбор:""",
        parse_mode = 'HTML',
        reply_markup = keyboard.week_input_by_date_group.as_markup()
    )

@dp.callback_query(F.data == 'week_input_by_num_step_back_to_group')
async def week_input_by_num_step_back_to_group_callback(callback_query: types.CallbackQuery):
    "Возврат в меню выбора группы из сценария для получения расписания на выбранную неделю (выбор по номеру недели)"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # возврат состояния состояний
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set week_input_by_num = 1
        where chat_id = {}""").format(sql.Literal(chat_id))
    PG.execute(state_query)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в меню выбора подгрупп через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = """Вернулся в меню выбора подгруппы.\nПовтори выбор:""",
        parse_mode = 'HTML',
        reply_markup = keyboard.week_input_by_num_group.as_markup()
    )


    # возвраты к вводу
@dp.callback_query(F.data == 'date_input_step_back_to_date_input')
async def step_back_to_date_input_callback(callback_query: types.CallbackQuery):
    "Возврат в меню ввода даты из сценария для получения расписания на выбранную дату"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # возврат состояния
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set date_input = 1
        where chat_id = {}""").format(sql.Literal(chat_id))
    PG.execute(state_query)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в меню ввода через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = """Вернулся в меню ввода даты.\nВведи новую команду:""",
        parse_mode = 'HTML',
        reply_markup = keyboard.date_input_kb.as_markup()
    )

@dp.callback_query(F.data == 'week_input_by_date_step_back_to_date_input')
async def step_back_to_week_by_date_input_callback(callback_query: types.CallbackQuery):
    "Возврат в меню ввода даты из сценария для получения расписания на выбранную неделю (выбор по дате)"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # возврат состояния
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set week_input_by_date = 1
        where chat_id = {}""").format(sql.Literal(chat_id))
    PG.execute(state_query)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в меню ввода через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = """Вернулся в меню ввода даты.\nВведи новую команду:""",
        parse_mode = 'HTML',
        reply_markup = keyboard.week_input_by_date_kb.as_markup()
    )

@dp.callback_query(F.data == 'week_input_by_num_step_back_to_week_input')
async def step_back_to_week_by_num_input_callback(callback_query: types.CallbackQuery):
    "Возврат в меню ввода номера недели из сценария для получения расписания на выбранную неделю (выбор по номеру недели)"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # возврат состояния
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set week_input_by_num = 1
        where chat_id = {}""").format(sql.Literal(chat_id))
    PG.execute(state_query)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в меню ввода через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = """Вернулся в меню ввода номера недели.\nВведи новую команду:""",
        parse_mode = 'HTML',
        reply_markup = keyboard.week_input_by_num_kb.as_markup()
    )

    # возврат на старт
@dp.callback_query(F.data == 'to_start')
async def to_start_callback(callback_query: types.CallbackQuery):
    "Возврат на старт"
    # параметры чата
    chat_id, *_, username_fin = chat_params(callback_query)
    # сброс состояний
    state_query = sql.SQL("""
        update is_bot_fsm.user_scenarios_states
        set
            today = 0,
            curr_week = 0,
            next_week = 0,
            two_weeks = 0,
            numerator = 0,
            denominator = 0,
            date_input = 0,
            week_input_by_date = 0,
            week_input_by_num = 0
        where chat_id = {}""").format(sql.Literal(chat_id))
    input_query = sql.SQL("""
        update is_bot_fsm.user_inputs_states
        set
            date_input = NULL,
            week_input_by_date = NULL,
            week_input_by_num = NULL
        where chat_id = {}""").format(sql.Literal(chat_id))
    queries = [state_query, input_query]
    PG.execute_multiple(queries)
    # логирование
    log_data = f"{chat_id}{splitter}Пользователь {username_fin} вернулся в главное меню через команду {callback_query.data}"
    db_logger.info(log_data)
    # ответ
    await bot.send_message(
        callback_query.from_user.id,
        text = f"Ещё раз приветствую, {callback_query.from_user.first_name}!\nКакое расписание тебя интересует?",
        parse_mode = 'HTML',
        reply_markup = keyboard.start.as_markup()
    )





# запуск и отлов exceptions
async def main():
    "Основная корутина"
    try:
        log_data = """0 ~ IS_Schedule_bot запущен"""
        db_logger.info(log_data)
        await dp.start_polling(bot)
    except (KeyboardInterrupt, SystemExit, Exception, ValueError) as err:
        await dp.stop_polling()
        log_data = f"0 ~ IS_Schedule_bot остановлен из-за ошибки или исключения: {err}"
        state_query = """
            update is_bot_fsm.user_scenarios_states
            set
                today = 0,
                curr_week = 0,
                next_week = 0,
                two_weeks = 0,
                numerator = 0,
                denominator = 0,
                date_input = 0,
                week_input_by_date = 0,
                week_input_by_num = 0
            """
        input_query = """
            update is_bot_fsm.user_inputs_states
                set
                    date_input = NULL,
                    week_by_date_input = NULL,
                    week_by_num_input = NULL
            """
        queries = [state_query, input_query]
        PG.execute_multiple(queries)
        db_logger.error(log_data)

if __name__ == "__main__":
    asyncio.run(main())
