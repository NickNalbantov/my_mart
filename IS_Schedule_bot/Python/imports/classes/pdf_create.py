"Модуль для генерации PDF файлов"

from pathlib import Path
import platform
from fpdf import FPDF

class PDFTable():
    "Класс для генерации табличных PDF"

    def pdf_table_gen(
            self,
            pdf_path: str,
            pdf_name: str,
            table_header: list,
            data: list,
            font: str = 'DejaVuLGCSansCondensed',
            page_header: str = '',
            page_header_align: str = 'C',
            page_header_font_color: list = [0, 0, 0],
            page_header_font_size: int = 20,
            table_header_font_color: list = [0, 0, 0],
            table_header_fill_color: list = [255, 255, 255],
            table_header_font_size: int = 16,
            table_font_color: list = [0, 0, 0],
            table_fill_color: list = [255, 255, 255],
            table_font_size: int = 14,
            table_align: str = 'J',
            orientation: str = 'P',
            page_format: str = 'A4'
        ):
        """
        Создание PDF-файла и запись таблицы из массива данных.
        Для записи PDF в одну папку со скриптом - pdf_path = '{}'

        Параметры:
        pdf_path - путь к целевой папке
        pdf_name - имя целевого файла
        table_header - список названий колонок
        data - содержимое таблицы
        font - шрифт, default - "DejaVuLGCSansCondensed" - не менять, если в документе есть любые не-латинские символы
        page_header - заголовок страницы, default = ''
        page_header_align - выравнивание заголовка страницы ('L' - слева, 'C' - по центру, 'R' - справа) default = 'C'
        page_header_font_color - цвет шрифта заголока страницы [R, G ,B], default [0, 0, 0]
        page_header_font_size - размер шрифта заголовка страницы, default = 20
        table_header_font_color - цвет шрифта заголока таблицы [R, G ,B], default [0, 0, 0]
        table_header_fill_color - цвет заливки заголока таблицы [R, G ,B], default [255, 255, 255]
        table_header_font_size - размер шрифта заголовка таблицы, default = 16
        table_font_color - цвет шрифта таблицы [R, G ,B], default [0, 0, 0]
        table_fill_color - цвет заливки таблицы [R, G ,B], default [255, 255, 255]
        table_font_size - размер шрифта в таблице, default = 14
        table_align - выравнивание текста в таблице ('J' - по ширине, 'L' - слева, 'C' - по центру, 'R' - справа), default = 'J'
        orientation - положение страницы ('P' - книжное, 'L' - альбомное), default = 'P'
        page_format - формат страницы ('A3', 'A4', 'A5'), default = 'A4'
        """
        # запуск и добавление шрифтов
        pdf = FPDF(orientation, 'mm', page_format)

        subpath = "/fonts/dejavu-lgc-fonts-ttf-2.37/ttf"
        if platform.system() != 'Linux':
            subpath = subpath.replace('/', '\\')

        pdf.add_font('DejaVuLGCSansCondensed', style = '', fname = f'{Path(__file__).resolve().parent}{subpath}DejaVuLGCSansCondensed.ttf')
        pdf.add_font('DejaVuLGCSansCondensed', style = 'B', fname = f'{Path(__file__).resolve().parent}{subpath}DejaVuLGCSansCondensed-Bold.ttf')
        pdf.add_font('DejaVuLGCSansCondensed', style = 'I', fname = f'{Path(__file__).resolve().parent}{subpath}DejaVuLGCSansCondensed-Oblique.ttf')
        pdf.add_font('DejaVuLGCSansCondensed', style = 'BI', fname = f'{Path(__file__).resolve().parent}{subpath}DejaVuLGCSansCondensed-BoldOblique.ttf')

        # создём пустую страницу
        pdf.add_page()

        # Заголовок таблицы
        pdf.set_font(font, style = 'B', size = page_header_font_size)
        pdf.set_text_color(page_header_font_color[0], page_header_font_color[1], page_header_font_color[2])
        pdf.cell(w = pdf.epw, text = page_header, ln = 2, align = page_header_align)
        pdf.cell(w = pdf.epw, text = ' ', ln = 2, align = page_header_align)

        # первичный расчёт ширины таблицы по ширине заголовков
        shrink_factor = 3.5
        col_width_list = []
        for cell in table_header:
            col_width = len(cell) * table_header_font_size / shrink_factor  # ширина ячейки
            col_width_list.append(col_width) # записываем ширину ячейки заголовка таблицы; все ячейки в столбце ниже будут иметь ту же ширину

        # располагаем таблицу в центре листа, изменяя отступы
        pdf.set_right_margin(0)
        pdf.set_left_margin(0)
        table_width_init = sum(int(i) for i in col_width_list)

            # если таблица получилась шире страницы - уменьшаем шрифт
        while pdf.epw - table_width_init < 0:
            table_header_font_size -= 1
            table_font_size -= 1
            col_width_list = []
            for cell in table_header:
                col_width = len(cell) * table_header_font_size / shrink_factor
                col_width_list.append(col_width)
            table_width_init = sum(int(i) for i in col_width_list)

        table_width_final = sum(int(i) for i in col_width_list)
        pdf.set_left_margin((pdf.epw - table_width_final) / 2)
        pdf.set_right_margin((pdf.epw - table_width_final) / 2)

        # Имена колонок
        pdf.set_font(font, style = 'B', size = table_header_font_size)
        pdf.set_text_color(table_header_font_color[0], table_header_font_color[1], table_header_font_color[2])
        pdf.set_fill_color(table_header_fill_color[0], table_header_fill_color[1], table_header_fill_color[2])
        line_height = pdf.font_size * 3
        for cell in table_header:
            col_width = len(cell) * table_header_font_size / shrink_factor
            pdf.multi_cell( # расчёт разбивки строк внутри ячеек с помощью split_only
                w = col_width,
                h = line_height,
                text = str(cell),
                border = 1,
                align = 'C',
                fill = True,
                split_only = True,
                ln = 3,
                max_line_height = pdf.font_size
            )
            pdf.multi_cell( # запись в ячейку
                w = col_width,
                h = line_height,
                text = str(cell),
                border = 1,
                align = 'C',
                fill = True,
                ln = 3,
                max_line_height = pdf.font_size
            )
        pdf.ln(line_height)

        # основная таблица
        pdf.set_font(font, style = '', size = table_font_size)
        pdf.set_text_color(table_font_color[0], table_font_color[1], table_font_color[2])
        pdf.set_fill_color(table_fill_color[0], table_fill_color[1], table_fill_color[2])
        fill_status = False
        data_to_header_ratio = []
            # рассчитываем максимальную высоту ячейки в таблице
        for row in data:
            for i, col in enumerate(row):
                data_to_header_ratio.append((len(str(col)) * table_header_font_size) / (shrink_factor * col_width_list[i]))
                pdf.multi_cell(
                    w = col_width_list[i],
                    h = 100 * pdf.font_size,
                    text = str(col),
                    border = 1,
                    fill = fill_status,
                    split_only = True,
                    ln = 3,
                    max_line_height = pdf.font_size
                )
        line_height = pdf.font_size * max(data_to_header_ratio)
            # записываем данные
        for row in data:
            for i, col in range(len(row)):
                pdf.multi_cell(
                    w = col_width_list[i],
                    h = line_height,
                    text = str(col),
                    border = 1,
                    align = table_align,
                    fill = fill_status,
                    split_only = True,
                    ln = 3,
                    max_line_height = pdf.font_size
                )
                pdf.multi_cell(
                    w = col_width_list[i],
                    h = line_height,
                    text = str(col),
                    border = 1,
                    align = table_align,
                    fill = fill_status,
                    ln = 3,
                    max_line_height = pdf.font_size
                )
            fill_status = not fill_status # чередуем прозрачные строки с закрашенными
            pdf.ln(line_height)

        # выгрузка файла
        if platform.system() == 'Linux':
            filepath = f'{pdf_path}/{pdf_name}.pdf'
        else:
            filepath = f'{pdf_path}\\{pdf_name}.pdf'
        pdf.output(filepath)
        print('PDF file has been created at ' + filepath)

        return filepath, pdf_name
