"""Модуль с клавиатурами для разных сценариев в чате"""

from aiogram.types import InlineKeyboardButton
from aiogram.utils.keyboard import InlineKeyboardBuilder

# стартовое меню
start = InlineKeyboardBuilder()
    # кнопки
today = InlineKeyboardButton(text = 'На сегодня', callback_data = 'today')
curr_week = InlineKeyboardButton(text = 'На текущую неделю', callback_data = 'curr_week')
next_week = InlineKeyboardButton(text = 'На следующую неделю', callback_data = 'next_week')
date_input = InlineKeyboardButton(text = 'На другую дату', callback_data = 'date_input')
week_input_by_date = InlineKeyboardButton(text = 'Выбрать неделю по дате', callback_data = 'week_input_by_date')
week_input_by_num = InlineKeyboardButton(text = 'Выбрать неделю по номеру', callback_data = 'week_input_by_num')
two_weeks = InlineKeyboardButton(text = 'На две недели', callback_data = 'two_weeks')
numerator = InlineKeyboardButton(text = 'По числителю', callback_data = 'numerator')
denominator = InlineKeyboardButton(text = 'По знаменателю', callback_data = 'denominator')
    # добавляем в ряды
start.row(today, curr_week)
start.row(next_week, two_weeks)
start.row(numerator, denominator)
start.add(date_input)
start.add(week_input_by_date)
start.add(week_input_by_num)
start.adjust(2)



# навигация назад
# на старт
to_start = InlineKeyboardButton(text = '⏮ В начало', callback_data = 'to_start')
# к строке ввода
date_input_step_back_to_date_input = InlineKeyboardButton(text = '⏪ К строке ввода', callback_data = 'date_input_step_back_to_date_input')
week_input_by_date_step_back_to_date_input = InlineKeyboardButton(text = '⏪ К строке ввода', callback_data = 'week_input_by_date_step_back_to_date_input')
week_input_by_num_step_back_to_week_input = InlineKeyboardButton(text = '⏪ К строке ввода', callback_data = 'week_input_by_num_step_back_to_week_input')
# к группам
today_step_back_to_group = InlineKeyboardButton(text = '◀️ К выбору группы', callback_data = 'today_step_back_to_group')
curr_week_step_back_to_group = InlineKeyboardButton(text = '◀️ К выбору группы', callback_data = 'curr_week_step_back_to_group')
next_week_step_back_to_group = InlineKeyboardButton(text = '◀️ К выбору группы', callback_data = 'next_week_step_back_to_group')
two_weeks_step_back_to_group = InlineKeyboardButton(text = '◀️ К выбору группы', callback_data = 'two_weeks_step_back_to_group')
numerator_step_back_to_group = InlineKeyboardButton(text = '◀️ К выбору группы', callback_data = 'numerator_step_back_to_group')
denominator_step_back_to_group = InlineKeyboardButton(text = '◀️ К выбору группы', callback_data = 'denominator_step_back_to_group')
date_input_step_back_to_group = InlineKeyboardButton(text = '◀️ К выбору группы', callback_data = 'date_input_step_back_to_group')
week_input_by_date_step_back_to_group = InlineKeyboardButton(text = '◀️ К выбору группы', callback_data = 'week_input_by_date_step_back_to_group')
week_input_by_num_step_back_to_group = InlineKeyboardButton(text = '◀️ К выбору группы', callback_data = 'week_input_by_num_step_back_to_group')

# меню отката с конечной страницы
    # возврат - today
back_today = InlineKeyboardBuilder()
        # добавление
back_today.add(today_step_back_to_group)
back_today.add(to_start)
back_today.adjust(1)

    # возврат - curr_week
back_curr_week = InlineKeyboardBuilder()
        # добавление
back_curr_week.add(curr_week_step_back_to_group)
back_curr_week.add(to_start)
back_curr_week.adjust(1)

    # возврат - next_week
back_next_week = InlineKeyboardBuilder()
        # добавление
back_next_week.add(next_week_step_back_to_group)
back_next_week.add(to_start)
back_next_week.adjust(1)

    # возврат - two_weeks
back_two_weeks = InlineKeyboardBuilder()
        # добавление
back_two_weeks.add(two_weeks_step_back_to_group)
back_two_weeks.add(to_start)
back_two_weeks.adjust(1)

    # возврат - numerator
back_numerator = InlineKeyboardBuilder()
        # добавление
back_numerator.add(numerator_step_back_to_group)
back_numerator.add(to_start)
back_numerator.adjust(1)

    # возврат - denominator
back_denominator = InlineKeyboardBuilder()
        # добавление
back_denominator.add(denominator_step_back_to_group)
back_denominator.add(to_start)
back_denominator.adjust(1)

    # возврат - ввод даты
back_to_input_by_date = InlineKeyboardBuilder()
        # добавление
back_to_input_by_date.add(date_input_step_back_to_group)
back_to_input_by_date.add(date_input_step_back_to_date_input)
back_to_input_by_date.add(to_start)
back_to_input_by_date.adjust(1)

    # возврат - ввод недели по дате
back_to_week_input_by_date = InlineKeyboardBuilder()
        # добавление
back_to_week_input_by_date.add(week_input_by_date_step_back_to_group)
back_to_week_input_by_date.add(week_input_by_date_step_back_to_date_input)
back_to_week_input_by_date.add(to_start)
back_to_week_input_by_date.adjust(1)

    # возврат - ввод недели по номеру
back_to_week_input_by_num = InlineKeyboardBuilder()
        # добавление
back_to_week_input_by_num.add(week_input_by_num_step_back_to_group)
back_to_week_input_by_num.add(week_input_by_num_step_back_to_week_input)
back_to_week_input_by_num.add(to_start)
back_to_week_input_by_num.adjust(1)





# выбор по дате
date_input_kb = InlineKeyboardBuilder()
    # кнопки
date_input_button = InlineKeyboardButton(text = "📅 Введи дату: ", callback_data = 'date_input_button')
    # добавление
date_input_kb.add(to_start)
date_input_kb.adjust(1)

# выбор недели по дате
week_input_by_date_kb = InlineKeyboardBuilder()
    # кнопки
week_input_by_date_button = InlineKeyboardButton(text = "📅 Введи дату: ", callback_data = 'week_input_by_date_button')
    # добавление
week_input_by_date_kb.add(to_start)
week_input_by_date_kb.adjust(1)

# выбор недели по номеру недели
week_input_by_num_kb = InlineKeyboardBuilder()
    # кнопки
week_input_by_num_button = InlineKeyboardButton(text = "🔢 Введи номер недели: ", callback_data = 'week_input_by_num_button')
    # добавление
week_input_by_num_kb.add(to_start)
week_input_by_num_kb.adjust(1)




# Группы:

# today - группы
today_group = InlineKeyboardBuilder()
    # кнопки
today_group_is_1_1 = InlineKeyboardButton(text = "1️⃣➖1️⃣ ИС-1, подгруппа 1", callback_data = 'today_group_is_1_1')
today_group_is_1_2 = InlineKeyboardButton(text = "1️⃣➖2️⃣ ИС-1, подгруппа 2", callback_data = 'today_group_is_1_2')
today_group_is_2_1 = InlineKeyboardButton(text = "2️⃣➖1️⃣ ИС-2, подгруппа 1", callback_data = 'today_group_is_2_1')
today_group_is_2_2 = InlineKeyboardButton(text = "2️⃣➖2️⃣ ИС-2, подгруппа 2", callback_data = 'today_group_is_2_2')
    # добавление
today_group.row(today_group_is_1_1, today_group_is_1_2)
today_group.row(today_group_is_2_1, today_group_is_2_2)
today_group.add(to_start)
today_group.adjust(2)

# curr_week - группы
curr_week_group = InlineKeyboardBuilder()
    # кнопки
curr_week_group_is_1_1 = InlineKeyboardButton(text = "1️⃣➖1️⃣ ИС-1, подгруппа 1", callback_data = 'curr_week_group_is_1_1')
curr_week_group_is_1_2 = InlineKeyboardButton(text = "1️⃣➖2️⃣ ИС-1, подгруппа 2", callback_data = 'curr_week_group_is_1_2')
curr_week_group_is_2_1 = InlineKeyboardButton(text = "2️⃣➖1️⃣ ИС-2, подгруппа 1", callback_data = 'curr_week_group_is_2_1')
curr_week_group_is_2_2 = InlineKeyboardButton(text = "2️⃣➖2️⃣ ИС-2, подгруппа 2", callback_data = 'curr_week_group_is_2_2')
    # добавление
curr_week_group.row(curr_week_group_is_1_1, curr_week_group_is_1_2)
curr_week_group.row(curr_week_group_is_2_1, curr_week_group_is_2_2)
curr_week_group.add(to_start)
curr_week_group.adjust(2)

# next_week - группы
next_week_group = InlineKeyboardBuilder()
    # кнопки
next_week_group_is_1_1 = InlineKeyboardButton(text = "1️⃣➖1️⃣ ИС-1, подгруппа 1", callback_data = 'next_week_group_is_1_1')
next_week_group_is_1_2 = InlineKeyboardButton(text = "1️⃣➖2️⃣ ИС-1, подгруппа 2", callback_data = 'next_week_group_is_1_2')
next_week_group_is_2_1 = InlineKeyboardButton(text = "2️⃣➖1️⃣ ИС-2, подгруппа 1", callback_data = 'next_week_group_is_2_1')
next_week_group_is_2_2 = InlineKeyboardButton(text = "2️⃣➖2️⃣ ИС-2, подгруппа 2", callback_data = 'next_week_group_is_2_2')
    # добавление
next_week_group.row(next_week_group_is_1_1, next_week_group_is_1_2)
next_week_group.row(next_week_group_is_2_1, next_week_group_is_2_2)
next_week_group.add(to_start)
next_week_group.adjust(2)

# two_weeks - группы
two_weeks_group = InlineKeyboardBuilder()
    # кнопки
two_weeks_group_is_1_1 = InlineKeyboardButton(text = "1️⃣➖1️⃣ ИС-1, подгруппа 1", callback_data = 'two_weeks_group_is_1_1')
two_weeks_group_is_1_2 = InlineKeyboardButton(text = "1️⃣➖2️⃣ ИС-1, подгруппа 2", callback_data = 'two_weeks_group_is_1_2')
two_weeks_group_is_2_1 = InlineKeyboardButton(text = "2️⃣➖1️⃣ ИС-2, подгруппа 1", callback_data = 'two_weeks_group_is_2_1')
two_weeks_group_is_2_2 = InlineKeyboardButton(text = "2️⃣➖2️⃣ ИС-2, подгруппа 2", callback_data = 'two_weeks_group_is_2_2')
    # добавление
two_weeks_group.row(two_weeks_group_is_1_1, two_weeks_group_is_1_2)
two_weeks_group.row(two_weeks_group_is_2_1, two_weeks_group_is_2_2)
two_weeks_group.add(to_start)
two_weeks_group.adjust(2)

# numerator - группы
numerator_group = InlineKeyboardBuilder()
    # кнопки
numerator_group_is_1_1 = InlineKeyboardButton(text = "1️⃣➖1️⃣ ИС-1, подгруппа 1", callback_data = 'numerator_group_is_1_1')
numerator_group_is_1_2 = InlineKeyboardButton(text = "1️⃣➖2️⃣ ИС-1, подгруппа 2", callback_data = 'numerator_group_is_1_2')
numerator_group_is_2_1 = InlineKeyboardButton(text = "2️⃣➖1️⃣ ИС-2, подгруппа 1", callback_data = 'numerator_group_is_2_1')
numerator_group_is_2_2 = InlineKeyboardButton(text = "2️⃣➖2️⃣ ИС-2, подгруппа 2", callback_data = 'numerator_group_is_2_2')
    # добавление
numerator_group.row(numerator_group_is_1_1, numerator_group_is_1_2)
numerator_group.row(numerator_group_is_2_1, numerator_group_is_2_2)
numerator_group.add(to_start)
numerator_group.adjust(2)

# denominator - группы
denominator_group = InlineKeyboardBuilder()
    # кнопки
denominator_group_is_1_1 = InlineKeyboardButton(text = "1️⃣➖1️⃣ ИС-1, подгруппа 1", callback_data = 'denominator_group_is_1_1')
denominator_group_is_1_2 = InlineKeyboardButton(text = "1️⃣➖2️⃣ ИС-1, подгруппа 2", callback_data = 'denominator_group_is_1_2')
denominator_group_is_2_1 = InlineKeyboardButton(text = "2️⃣➖1️⃣ ИС-2, подгруппа 1", callback_data = 'denominator_group_is_2_1')
denominator_group_is_2_2 = InlineKeyboardButton(text = "2️⃣➖2️⃣ ИС-2, подгруппа 2", callback_data = 'denominator_group_is_2_2')
    # добавление
denominator_group.row(denominator_group_is_1_1, denominator_group_is_1_2)
denominator_group.row(denominator_group_is_2_1, denominator_group_is_2_2)
denominator_group.add(to_start)
denominator_group.adjust(2)

# date_input - группы
date_input_group = InlineKeyboardBuilder()
    # кнопки
date_input_group_is_1_1 = InlineKeyboardButton(text = "1️⃣➖1️⃣ ИС-1, подгруппа 1", callback_data = 'date_input_group_is_1_1')
date_input_group_is_1_2 = InlineKeyboardButton(text = "1️⃣➖2️⃣ ИС-1, подгруппа 2", callback_data = 'date_input_group_is_1_2')
date_input_group_is_2_1 = InlineKeyboardButton(text = "2️⃣➖1️⃣ ИС-2, подгруппа 1", callback_data = 'date_input_group_is_2_1')
date_input_group_is_2_2 = InlineKeyboardButton(text = "2️⃣➖2️⃣ ИС-2, подгруппа 2", callback_data = 'date_input_group_is_2_2')
    # добавление
date_input_group.row(date_input_group_is_1_1, date_input_group_is_1_2)
date_input_group.row(date_input_group_is_2_1, date_input_group_is_2_2)
date_input_group.row(date_input_step_back_to_date_input, to_start)
date_input_group.adjust(2)

# week_input_by_date - группы
week_input_by_date_group = InlineKeyboardBuilder()
    # кнопки
week_input_by_date_group_is_1_1 = InlineKeyboardButton(text = "1️⃣➖1️⃣ ИС-1, подгруппа 1", callback_data = 'week_input_by_date_group_is_1_1')
week_input_by_date_group_is_1_2 = InlineKeyboardButton(text = "1️⃣➖2️⃣ ИС-1, подгруппа 2", callback_data = 'week_input_by_date_group_is_1_2')
week_input_by_date_group_is_2_1 = InlineKeyboardButton(text = "2️⃣➖1️⃣ ИС-2, подгруппа 1", callback_data = 'week_input_by_date_group_is_2_1')
week_input_by_date_group_is_2_2 = InlineKeyboardButton(text = "2️⃣➖2️⃣ ИС-2, подгруппа 2", callback_data = 'week_input_by_date_group_is_2_2')
    # добавление
week_input_by_date_group.row(week_input_by_date_group_is_1_1, week_input_by_date_group_is_1_2)
week_input_by_date_group.row(week_input_by_date_group_is_2_1, week_input_by_date_group_is_2_2)
week_input_by_date_group.row(week_input_by_date_step_back_to_date_input, to_start)
week_input_by_date_group.adjust(2)

# week_input_by_num - группы
week_input_by_num_group = InlineKeyboardBuilder()
    # кнопки
week_input_by_num_group_is_1_1 = InlineKeyboardButton(text = "1️⃣➖1️⃣ ИС-1, подгруппа 1", callback_data = 'week_input_by_num_group_is_1_1')
week_input_by_num_group_is_1_2 = InlineKeyboardButton(text = "1️⃣➖2️⃣ ИС-1, подгруппа 2", callback_data = 'week_input_by_num_group_is_1_2')
week_input_by_num_group_is_2_1 = InlineKeyboardButton(text = "2️⃣➖1️⃣ ИС-2, подгруппа 1", callback_data = 'week_input_by_num_group_is_2_1')
week_input_by_num_group_is_2_2 = InlineKeyboardButton(text = "2️⃣➖2️⃣ ИС-2, подгруппа 2", callback_data = 'week_input_by_num_group_is_2_2')
    # добавление
week_input_by_num_group.row(week_input_by_num_group_is_1_1, week_input_by_num_group_is_1_2)
week_input_by_num_group.row(week_input_by_num_group_is_2_1, week_input_by_num_group_is_2_2)
week_input_by_num_group.row(week_input_by_num_step_back_to_week_input, to_start)
week_input_by_num_group.adjust(2)
