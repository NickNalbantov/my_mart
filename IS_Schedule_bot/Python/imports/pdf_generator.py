"""Модуль с функциями для генерации PDF-файлов для различных сценариев.
Обращается к соответствующим представлениям в БД, затем передаёт данные в функцию .classes.pdf_create.PDFTable.pdf_table_gen()
"""

from pathlib import Path
import platform
import configparser

import pendulum
from psycopg import sql

from .classes.pdf_create import PDFTable
from .classes.pendulum_transform import PendulumTransform
from .classes.Class_PostgreSQL import PostgreSQL


CONFIG_FILE = f'{Path(__file__).resolve().parent}/classes/config.ini'

config = configparser.ConfigParser(allow_no_value = True, empty_lines_in_values = True)
config.read(CONFIG_FILE, 'utf8')

db_connect = config['DB']

# подключение к БД
PG = PostgreSQL(db_connect['host'], db_connect['port'], db_connect['database'], db_connect['user'], db_connect['password'])

# трансформация дат
PND = PendulumTransform()

# перебор подгрупп
subgroups = ['1_1', '1_2', '2_1', '2_2']

# переменные для генерации PDF
table_header_font_color = [255, 255, 255]
table_header_fill_color = [81, 99, 212]
table_header_font_size = 14
table_fill_color = [193, 196, 198]
table_font_size = 12
table_align = 'C'
orientation = 'L'
page_format = 'A3'
PDF = PDFTable()

# генерация пути к файлу:
def pdf_filepath(storage_type: str):
    "Функция для генерации папок, в которые будут сохраняться PDF-файлы"
    if platform.system() == 'Linux':
        path = f'{Path(__file__).resolve().parent}/classes/pdf_stash/{storage_type}'
    else:
        path = f'{Path(__file__).resolve().parent}\\classes\\pdf_stash\\{storage_type}'

    Path(path).mkdir(mode = 755, parents = True, exist_ok = True)

    return path

# функции генерации PDF

    # расписание на сегодня
def pdf_today_schedule(subgroup: str):
    "Генерация расписания на сегодняшний день"
    pdf_name = f"IS_{subgroup}_{PND.dmy_now().format('DD.MM')}_schedule"
    storage_type = 'temporary'
    pdf_path = pdf_filepath(storage_type)
    page_header = f"Расписание на сегодня ({PND.curr_date_dmy()}) для подгруппы ИС-{subgroup}"
    query = sql.SQL("""select * from is_schedule.today_schedule({})""").format(sql.Literal(subgroup))
    table_header = PG.fetch_headers(query)
    data = PG.fetch_all(query)
    if len(data) > 0:
        return PDF.pdf_table_gen(
            pdf_path = pdf_path,
            pdf_name = pdf_name,
            table_header = table_header,
            data = data,
            page_header = page_header,
            table_header_font_color = table_header_font_color,
            table_header_fill_color = table_header_fill_color,
            table_header_font_size = table_header_font_size,
            table_fill_color = table_fill_color,
            table_font_size = table_font_size,
            table_align = table_align,
            orientation = orientation,
            page_format = page_format
        )
    else:
        return None, None

    # расписание на текущую неделю
def pdf_curr_week_schedule(subgroup: str):
    "Генерация расписания на текущую неделю"
    pdf_name = f'IS_{subgroup}_week_{PND.curr_week_number()}_schedule'
    storage_type = 'temporary'
    pdf_path = pdf_filepath(storage_type)
    this_monday = PND.last_weekday(1)
    this_sunday = PND.next_weekday(7)
    page_header = f"Расписание на текущую неделю ({this_monday} - {this_sunday}) для подгруппы ИС-{subgroup}"
    query = sql.SQL("""select * from is_schedule.curr_week_schedule({})""").format(sql.Literal(subgroup))
    table_header = PG.fetch_headers(query)
    data = PG.fetch_all(query)
    if len(data) > 0:
        return PDF.pdf_table_gen(
            pdf_path = pdf_path,
            pdf_name = pdf_name,
            table_header = table_header,
            data = data,
            page_header = page_header,
            table_header_font_color = table_header_font_color,
            table_header_fill_color = table_header_fill_color,
            table_header_font_size = table_header_font_size,
            table_fill_color = table_fill_color,
            table_font_size = table_font_size,
            table_align = table_align,
            orientation = orientation,
            page_format = page_format
        )
    return None, None

    # расписание на следующую неделю
def pdf_next_week_schedule(subgroup: str):
    "Генерация расписания на следующую неделю"
    pdf_name = f'IS_{subgroup}_week_{PND.curr_week_number() + 1}_schedule'
    storage_type = 'temporary'
    pdf_path = pdf_filepath(storage_type)
    next_monday = PND.last_weekday(1, PND.dmy_now() + pendulum.duration(days = 7))
    next_sunday = PND.next_weekday(7, PND.dmy_now() + pendulum.duration(days = 7))
    page_header = f"Расписание на следующую неделю ({next_monday} - {next_sunday}) для подгруппы ИС-{subgroup}"
    query = sql.SQL("""select * from is_schedule.next_week_schedule({})""").format(sql.Literal(subgroup))
    table_header = PG.fetch_headers(query)
    data = PG.fetch_all(query)
    if len(data) > 0:
        return PDF.pdf_table_gen(
            pdf_path = pdf_path,
            pdf_name = pdf_name,
            table_header = table_header,
            data = data,
            page_header = page_header,
            table_header_font_color = table_header_font_color,
            table_header_fill_color = table_header_fill_color,
            table_header_font_size = table_header_font_size,
            table_fill_color = table_fill_color,
            table_font_size = table_font_size,
            table_align = table_align,
            orientation = orientation,
            page_format = page_format
        )
    return None, None

    # расписание на указанную дату
def pdf_date_input_schedule(subgroup: str, date_input: str, yr: str = PND.dmy_now().format('YYYY')):
    "Генерация расписания на указанную дату"
    pdf_name = f'IS_{subgroup}_{date_input}.{yr[-2:]}_schedule'
    storage_type = 'temporary'
    pdf_path = pdf_filepath(storage_type)
    page_header = f"Расписание на {date_input}.{yr[-2:]} для подгруппы ИС-{subgroup}"
    query = sql.SQL("""select * from is_schedule.date_input_schedule({}, {}, {})""").format(
        sql.Literal(subgroup),
        sql.Literal(date_input),
        sql.Literal(int(yr))
        )
    table_header = PG.fetch_headers(query)
    data = PG.fetch_all(query)
    if len(data) > 0:
        return PDF.pdf_table_gen(
            pdf_path = pdf_path,
            pdf_name = pdf_name,
            table_header = table_header,
            data = data,
            page_header = page_header,
            table_header_font_color = table_header_font_color,
            table_header_fill_color = table_header_fill_color,
            table_header_font_size = table_header_font_size,
            table_fill_color = table_fill_color,
            table_font_size = table_font_size,
            table_align = table_align,
            orientation = orientation,
            page_format = page_format
        )
    return None, None

    # расписание на указанную неделю (по дате)
def pdf_week_input_by_date_schedule(subgroup: str, date_input: str, yr: str = PND.dmy_now().format('YYYY')):
    "Генерация расписания на указанную неделю (по дате)"
    full_date = f'{date_input}.{yr[-2:]}'
    pdf_name = f"IS_{subgroup}_week_{PND.get_week_number(full_date)}_schedule"
    storage_type = 'temporary'
    pdf_path = pdf_filepath(storage_type)
    selected_monday = PND.last_weekday(1, full_date)
    selected_sunday = PND.next_weekday(7, full_date)
    page_header = f"Расписание на неделю {selected_monday} - {selected_sunday} для подгруппы ИС-{subgroup}"
    query = sql.SQL("""select * from is_schedule.week_input_by_date_schedule({}, {}, {})""").format(
        sql.Literal(subgroup),
        sql.Literal(date_input),
        sql.Literal(int(yr))
        )
    table_header = PG.fetch_headers(query)
    data = PG.fetch_all(query)
    if len(data) > 0:
        return PDF.pdf_table_gen(
            pdf_path = pdf_path,
            pdf_name = pdf_name,
            table_header = table_header,
            data = data,
            page_header = page_header,
            table_header_font_color = table_header_font_color,
            table_header_fill_color = table_header_fill_color,
            table_header_font_size = table_header_font_size,
            table_fill_color = table_fill_color,
            table_font_size = table_font_size,
            table_align = table_align,
            orientation = orientation,
            page_format = page_format
        )
    return None, None

    # расписание на указанную неделю (по номеру недели)
def pdf_week_input_by_week_number_schedule(subgroup: str, week_num_input: int, yr: str = PND.dmy_now().format('YYYY')):
    "Генерация расписания на указанную неделю (по номеру недели)"
    pdf_name = f'IS_{subgroup}_week_{week_num_input}_year_{yr}_schedule'
    storage_type = 'temporary'
    pdf_path = pdf_filepath(storage_type)
    selected_monday = PND.last_weekday(1, pendulum.parse(f'{yr}-W{week_num_input}-1'))
    selected_sunday = PND.next_weekday(7, pendulum.parse(f'{yr}-W{week_num_input}-1'))
    page_header = f"Расписание на неделю № {week_num_input} ({selected_monday} - {selected_sunday}) для подгруппы ИС-{subgroup}"
    query = sql.SQL("""select * from is_schedule.week_input_by_week_number_schedule({}, {}, {})""").format(
        sql.Literal(subgroup),
        sql.Literal(week_num_input),
        sql.Literal(int(yr))
        )
    table_header = PG.fetch_headers(query)
    data = PG.fetch_all(query)
    return PDF.pdf_table_gen(
            pdf_path = pdf_path,
            pdf_name = pdf_name,
            table_header = table_header,
            data = data,
            page_header = page_header,
            table_header_font_color = table_header_font_color,
            table_header_fill_color = table_header_fill_color,
            table_header_font_size = table_header_font_size,
            table_fill_color = table_fill_color,
            table_font_size = table_font_size,
            table_align = table_align,
            orientation = orientation,
            page_format = page_format
        )

    # расписание на две недели
def pdf_two_weeks_schedule(): # статичные таблицы, генерируем один раз
    "Генерация расписания на две недели (статичные таблицы)"
    storage_type = 'persistent'
    pdf_path = pdf_filepath(storage_type)
    for subgroup in subgroups:
        pdf_name = f'IS_{subgroup}_two_weeks_schedule'
        page_header = f'Расписание на две недели для подгруппы ИС-{subgroup}'
        query = sql.SQL("""select * from is_schedule.two_weeks_schedule({})""").format(sql.Literal(subgroup))
        table_header = PG.fetch_headers(query)
        data = PG.fetch_all(query)
        if len(data) > 0:
            PDF.pdf_table_gen(
                pdf_path = pdf_path,
                pdf_name = pdf_name,
                table_header = table_header,
                data = data,
                page_header = page_header,
                table_header_font_color = table_header_font_color,
                table_header_fill_color = table_header_fill_color,
                table_header_font_size = table_header_font_size,
                table_fill_color = table_fill_color,
                table_font_size = table_font_size,
                table_align = table_align,
                orientation = orientation,
                page_format = page_format
            )
        else:
            return None, None
    return 'All PDF files have been created'

    # расписание на недели по числителю
def pdf_numerator_schedule(): # статичные таблицы, генерируем один раз
    "Генерация расписания по числителю (статичные таблицы)"
    storage_type = 'persistent'
    pdf_path = pdf_filepath(storage_type)
    for subgroup in subgroups:
        pdf_name = f'IS_{subgroup}_numerator_schedule'
        page_header = f'Расписание недели по числителю для подгруппы ИС-{subgroup}'
        query = sql.SQL("""select * from is_schedule.numerator_schedule({})""").format(sql.Literal(subgroup))
        table_header = PG.fetch_headers(query)
        data = PG.fetch_all(query)
        if len(data) > 0:
            PDF.pdf_table_gen(
                pdf_path = pdf_path,
                pdf_name = pdf_name,
                table_header = table_header,
                data = data,
                page_header = page_header,
                table_header_font_color = table_header_font_color,
                table_header_fill_color = table_header_fill_color,
                table_header_font_size = table_header_font_size,
                table_fill_color = table_fill_color,
                table_font_size = table_font_size,
                table_align = table_align,
                orientation = orientation,
                page_format = page_format
            )
        else:
            return None, None
    return 'All PDF files have been created'

    # расписание на недели по знаменателю
def pdf_denominator_schedule(): # статичные таблицы, генерируем один раз
    "Генерация расписания по знаменателю (статичные таблицы)"
    storage_type = 'persistent'
    pdf_path = pdf_filepath(storage_type)
    for subgroup in subgroups:
        pdf_name = f'IS_{subgroup}_denominator_schedule'
        page_header = f'Расписание на недели по знаменателю для подгруппы ИС-{subgroup}'
        query = sql.SQL("""select * from is_schedule.denominator_schedule({})""").format(sql.Literal(subgroup))
        table_header = PG.fetch_headers(query)
        data = PG.fetch_all(query)
        if len(data) > 0:
            PDF.pdf_table_gen(
                pdf_path = pdf_path,
                pdf_name = pdf_name,
                table_header = table_header,
                data = data,
                page_header = page_header,
                table_header_font_color = table_header_font_color,
                table_header_fill_color = table_header_fill_color,
                table_header_font_size = table_header_font_size,
                table_fill_color = table_fill_color,
                table_font_size = table_font_size,
                table_align = table_align,
                orientation = orientation,
                page_format = page_format
            )
        else:
            return None, None
    return 'All PDF files have been created'

# pdf_today_schedule('2_2')
# pdf_curr_week_schedule('2_1')
# pdf_next_week_schedule('2_1')

# pdf_week_input_by_date_schedule('2_1', '15.03')
# pdf_date_input_schedule('2_1', '16.03')
# pdf_week_input_by_week_number_schedule('1_1', 11)

# pdf_two_weeks_schedule()
# pdf_denominator_schedule()
# pdf_numerator_schedule()
