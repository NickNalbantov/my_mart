"""Модуль с логгером, который пишет логи бота в PostgreSQL"""

import logging
import configparser
from pathlib import Path

from .classes.Class_PostgreSQL import PostgreSQL


CONFIG_FILE = f'{Path(__file__).resolve().parent}/classes/config.ini'

config = configparser.ConfigParser(allow_no_value = True, empty_lines_in_values = True)
config.read(CONFIG_FILE, 'utf8')

db_connect = config['DB']

class LogDBHandler(logging.Handler):
    """Класс для логирования в таблицы PG
       Необходимо указать имя схемы, целевой таблицы и столбцов в ней (необходимо, т.к. могут быть столбцы с constraint и дефолтным заполнением)
       Данные для логирования передаются в log_data - список кортежей (строк таблицы)
    """
    def __init__(
        self,
        db_schema: str,
        db_log_table: str,
        db_log_table_columns: list,
        formatter,
        splitter: str = ' ~ '
        ):

        logging.Handler.__init__(self)
        self.db_schema = db_schema
        self.db_log_table = db_log_table
        self.db_log_table_columns = db_log_table_columns
        self.splitter = splitter
        self.formatter = formatter

    def emit(self, record):
        """Принимает сообщения из логгера (дефолтный разделитель ' ~ ', можно менять в аргументе splitter) и парсит их в список"""
        self.formatter.format(record)
        log_data_list = record.msg.split(self.splitter)
        log_data = [tuple((record.asctime, log_data_list[0], log_data_list[1], record.levelname))]
        # подключаемся к PG
        PG = PostgreSQL(db_connect['host'], db_connect['port'], db_connect['database'], db_connect['user'], db_connect['password'])

        # вставляем данные в лог
        PG.insert_batch(table = self.db_log_table, values_names = self.db_log_table_columns, data_list = log_data, schema = self.db_schema)


# сюда можно накидывать функции под конкретные реализации

def is_bot_logger(
        db_log_table: str,
        db_log_table_columns: list,
        db_schema: str = 'is_bot_logs',
        splitter: str = ' ~ ',
        log_level: str = 'DEBUG'
    ):
    """Логгер для bot.py"""

    frmtr = logging.Formatter(fmt = '%(msg)s - %(levelname)s - %(asctime)s', datefmt = '%d.%m.%Y %H:%M:%S')
    log_db = LogDBHandler(
        db_schema = db_schema,
        db_log_table = db_log_table,
        db_log_table_columns = db_log_table_columns,
        formatter = frmtr,
        splitter = splitter
        )

    log = logging.getLogger('ISS_Bot_PG_logger')
    log.addHandler(log_db)
    log.setLevel(log_level)
    log.propagate = False
    return log
