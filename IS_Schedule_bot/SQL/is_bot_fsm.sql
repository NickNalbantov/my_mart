-- создаём схему
drop schema if exists is_bot_fsm cascade;
create schema if not exists is_bot_fsm;

-- добавляем описание схемы
comment on schema is_bot_fsm
    is 'Хранилище Finite-State Machine';

-- матрица состояний сценариев для каждого пользователя
create table if not exists is_bot_fsm.user_scenarios_states
(
    chat_id bigint references is_bot_logs.bot_users,
    today smallint,
    curr_week smallint,
    next_week smallint,
    two_weeks smallint,
    numerator smallint,
    denominator smallint,
    date_input smallint,
    week_input_by_date smallint,
    week_input_by_num smallint
);

-- матрица состояний вводов для каждого пользователя
create table if not exists is_bot_fsm.user_inputs_states
(
    chat_id bigint references is_bot_logs.bot_users,
    date_input text,
    week_input_by_date text,
    week_input_by_num text
);