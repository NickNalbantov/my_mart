-- создаём схему
drop schema if exists is_bot_logs cascade;
create schema if not exists is_bot_logs;

-- добавляем описание схемы
comment on schema is_bot_logs
    is 'OLTP-хранилище логов';

-- логирование выполнения триггеров на изменение данных в схеме is_schedule
create table if not exists is_bot_logs.data_update_procedures_log
(
    exec_dttm timestamptz,
    id int generated always as identity,
    command text,
    schema_name text,
    obj_name text
);

-- Таблица юзеров
create table if not exists is_bot_logs.bot_users
(
    first_appearance timestamptz not null default clock_timestamp(),
    chat_id bigint primary key,
    user_id int not null generated always as identity,
    first_name text,
    last_name text,
    username text not null
);

-- регистрируем самого бота с chat_id = 0
insert into is_bot_logs.bot_users (chat_id, username, first_appearance) values (0, 'IS_Schedule_bot', now());

-- события из чата
create table if not exists is_bot_logs.bot_events_logs
(
    log_time timestamptz not null,
    chat_id bigint not null references is_bot_logs.bot_users,
    event_id int not null generated always as identity,
    bot_event text,
    log_level_name text not null
);


-- процедура для быстрой зачистки логов
create or replace procedure is_bot_logs.delete_bot_stats(schema_ text default 'is_bot_logs')
language plpgsql
as $proc$
    declare
        tab text;
        seq text;
    begin
        -- Зачистка таблиц
        for tab in (
                    select tablename::text as obj_name
                    from pg_catalog.pg_tables
                    where schemaname = schema_
                   ) loop
            execute 'truncate table ' || quote_ident(schema_) || '.' || quote_ident(tab) || ' cascade';
            commit;
        end loop;

        -- Восстанавливаем строку с id самого бота
        execute $$insert into is_bot_logs.bot_users (chat_id, username, first_appearance) values (0, 'IS_Schedule_bot', now())$$;
        commit;

        -- Перезапуск последовательностей
        for seq in (
                    select sequencename::text as obj_name
                    from pg_catalog.pg_sequences
                    where schemaname = schema_
                   ) loop
            execute 'alter sequence ' || quote_ident(schema_) || '.' || quote_ident(seq) || ' restart with 1';
            commit;
        end loop;
    end;
$proc$;