-- создаём схему
drop schema if exists is_schedule cascade;
create schema if not exists is_schedule;

-- добавляем описание схемы
comment on schema is_schedule
    is 'OLAP-хранилище расписания';


-- Таблица с днями недели
create table if not exists is_schedule.days
(
    weekday_number smallint generated always as identity primary key,
    day_name text
);

insert into is_schedule.days (day_name) values
    ('Понедельник'),
    ('Вторник'),
    ('Среда'),
    ('Четверг'),
    ('Пятница'),
    ('Суббота'),
    ('Воскресенье');


-- Таблица с датами, днями недели, номерами недель и типами недель - 'Ч' (numerator, числитель), 'З' (denominator, делитель)
create table if not exists is_schedule.dates
(
    dt date,
    date_number smallint generated always as identity primary key,
    weekday_number smallint references is_schedule.days,
    week_number smallint,
    isoyear smallint,
    academ_year smallint,
    academ_week_number smallint,
    week_type text
);

insert into is_schedule.dates
(
    dt,
    weekday_number,
    week_number,
    isoyear,
    academ_year,
    academ_week_number,
    week_type
)
    with dts as
    (
        select
            d::date as dt,
            extract(isodow from d::date) as weekday_number,
            extract(week from d::date)::smallint as week_number,
            extract(isoyear from d::date)::smallint as isoyear,
            case
                when extract(month from d::date) >= 9 then extract(year from d::date)
                else extract(year from d::date) - 1
            end as academ_year
        from generate_series('01.09.2021', '30.06.2034', '1 day'::interval) d -- любой интервал
    )

    select
        dt::date as dt,
        weekday_number::smallint as weekday_number,
        week_number::smallint as week_number,
        isoyear::smallint as isoyear,
        academ_year::smallint as academ_year,
        academ_week_number::smallint as academ_week_number,
        case when mod(academ_week_number, 2) = 1 then 'Ч' else 'З' end as week_type
    from
        (
        select
            dt,
            weekday_number,
            week_number,
            isoyear,
            academ_year,
            sum(academ_week_gr) over(partition by academ_year order by dt) as academ_week_number
        from
        (
            select
                *,
                case
                    when extract(day from dt) in (1, 2) and extract(month from dt) = 9 and extract(isodow from dt) >= 6 then 0
                    when lag(week_number) over(partition by academ_year order by dt) = week_number then 0
                    else 1
                end as academ_week_gr
            from dts
        ) gr
    ) res
    order by dt;


-- Таблица с временем пар
create table if not exists is_schedule.timeslots
(
    lesson_number smallint generated always as identity primary key,
    start_time time(0),
    end_time time(0)
);

insert into is_schedule.timeslots
(
    start_time,
    end_time
)
values
    ('08:00'::time, '09:30'::time),
    ('09:40'::time, '11:10'::time),
    ('11:30'::time, '13:00'::time),
    ('13:10'::time, '14:40'::time),
    ('15:00'::time, '16:30'::time),
    ('16:40'::time, '18:10'::time),
    ('18:20'::time, '19:50'::time);


-- Таблица с типами пар
create table if not exists is_schedule.discipline_types
(
    discipline_type_id smallint generated always as identity primary key,
    discipline_type_name text
);

insert into is_schedule.discipline_types (discipline_type_name) values ('Лекция'), ('Семинар'), ('Лабораторная работа');


-- Таблица с преподавателями
create table if not exists is_schedule.teachers(
    teacher_id smallint generated always as identity primary key,
    teacher_name text
);

insert into is_schedule.teachers (teacher_name) values
    ('Благодырь М. А.'),
    ('Пурунова А. М.'),
    ('Куликова Н. Н.'),
    ('Фоменко М. А.'),
    ('Ляпин В. М.'),
    ('Нечипоренко О. А.'),
    ('Корж Т. В.'),
    ('Лежнёв В. В.'),
    ('Беженар М. В.'),
    ('Никитин Ю. Г.'),
    ('Налбантов Н. Н.'),
    ('Рудоман Н. Р.'),
    ('Аванесов В. М.'),
    ('Савченко О. П.');


-- Таблица с названиями предметов и преподавателями
create table if not exists is_schedule.disciplines
(
    discipline_id smallint generated always as identity primary key,
    discipline_type_id smallint references is_schedule.discipline_types,
    teacher_id smallint references is_schedule.teachers,
    discipline_name text
);

insert into is_schedule.disciplines
(
    discipline_name,
    discipline_type_id,
    teacher_id
) values
    ('Основы теории кодирования', 1, 1),
    ('Основы теории кодирования', 3, 2),
    ('Технологии разработки веб-приложений', 1, 3),
    ('Технологии разработки веб-приложений', 3, 4),
    ('Элективные дисциплины по физической культуре и спорту', 2, 5),
    ('Инфокоммуникационные системы и сети', 1, 3),
    ('Инфокоммуникационные системы и сети', 2, 3),
    ('Инфокоммуникационные системы и сети', 3, 3),
    ('Моделирование процессов и систем', 1, 6),
    ('Моделирование процессов и систем', 3, 6),
    ('Проектирование информационных систем', 3, 7),
    ('Проектирование информационных систем', 1, 8),
    ('Интерфейсы информационных систем', 3, 6),
    ('Интерфейсы информационных систем', 3, 9),
    ('Интерфейсы информационных систем', 1, 10),
    ('Математическая логика и теория алгоритмов', 1, 10),
    ('Математическая логика и теория алгоритмов', 3, 10),
    ('Системы управления базами данных Oracle SQL и PostgreSQL', 1, 11),
    ('Системы управления базами данных Oracle SQL и PostgreSQL', 3, 11),
    ('Разработка и эксплуатация САПР', 1, 12),
    ('Теоретические основы информационно-измерительной техники', 1, 13),
    ('Теоретические основы информационно-измерительной техники', 3, 13),
    ('Проектирование и администрирование БД', 1, 14);


-- Таблица для ИС-1_1-числитель
create table if not exists is_schedule.is_1_1_schedule_n (
    lesson_id smallint generated always as identity primary key,
    weekday_number smallint references is_schedule.days,
    lesson_number smallint references is_schedule.timeslots,
    discipline_id smallint references is_schedule.disciplines,
    room text
);

insert into is_schedule.is_1_1_schedule_n (weekday_number, lesson_number, discipline_id, room)
values
(1, 4, 1, '114c'),
(1, 5, 3, '212c'),
(1, 6, 4, '142c'),
(1, 7, 4, '142c'),

(3, 1, 5, 'Стадион'),
(3, 2, 6, '209c'),

(4, 4, 7, '133c'),

(5, 4, 9, '315c'),
(5, 5, 12, '315c'),
(5, 6, 17, '230c'),
(5, 7, 17, '230c'),

(6, 1, 19, '142c'),
(6, 2, 19, '142c'),
(6, 3, 16, '230c'),
(6, 4, 18, '114c');


-- Таблица для ИС-1_1-знаменатель
create table if not exists is_schedule.is_1_1_schedule_d (
    lesson_id smallint generated always as identity primary key,
    weekday_number smallint references is_schedule.days,
    lesson_number smallint references is_schedule.timeslots,
    discipline_id smallint references is_schedule.disciplines,
    room text
);

insert into is_schedule.is_1_1_schedule_d (weekday_number, lesson_number, discipline_id, room)
values
(1, 4, 1, '114c'),
(1, 5, 3, '212c'),

(3, 1, 5, 'Стадион'),
(3, 2, 6, '209c'),
(3, 3, 10, '213c'),
(3, 4, 10, '213c'),

(4, 3, 8, '213c'),
(4, 4, 11, '133c'),
(4, 5, 11, '133c'),

(5, 5, 12, '315c'),
(5, 6, 14, '207c'),
(5, 7, 19, '207c'),

(6, 1, 2, '212c'),
(6, 2, 2, '212c'),
(6, 3, 15, '230c'),
(6, 4, 18, '114c');


-- Таблица для ИС-1_2-числитель
create table if not exists is_schedule.is_1_2_schedule_n (
    lesson_id smallint generated always as identity primary key,
    weekday_number smallint references is_schedule.days,
    lesson_number smallint references is_schedule.timeslots,
    discipline_id smallint references is_schedule.disciplines,
    room text
);

insert into is_schedule.is_1_2_schedule_n (weekday_number, lesson_number, discipline_id, room)
values
(1, 4, 1, '114c'),
(1, 5, 3, '212c'),

(3, 1, 5, 'Стадион'),
(3, 2, 6, '209c'),
(3, 3, 10, '213c'),
(3, 4, 10, '213c'),

(4, 2, 11, '207c'),
(4, 3, 11, '207c'),
(4, 4, 7, '133c'),

(5, 3, 13, '207c'),
(5, 4, 9, '315c'),
(5, 5, 12, '315c'),
(5, 6, 8, '207c'),
(5, 7, 19, '207c'),

(6, 1, 2, '212c'),
(6, 2, 2, '212c'),
(6, 3, 16, '230c'),
(6, 4, 18, '114c');


-- Таблица для ИС-1_2-знаменатель
create table if not exists is_schedule.is_1_2_schedule_d (
    lesson_id smallint generated always as identity primary key,
    weekday_number smallint references is_schedule.days,
    lesson_number smallint references is_schedule.timeslots,
    discipline_id smallint references is_schedule.disciplines,
    room text
);

insert into is_schedule.is_1_2_schedule_d (weekday_number, lesson_number, discipline_id, room)
values
(1, 4, 1, '114c'),
(1, 5, 3, '212c'),
(1, 6, 4, '142c'),
(1, 7, 4, '142c'),

(3, 1, 5, 'Стадион'),
(3, 2, 6, '209c'),

(5, 5, 12, '315c'),
(5, 6, 17, '230c'),
(5, 7, 17, '230c'),

(6, 1, 19, '142c'),
(6, 2, 19, '142c'),
(6, 3, 15, '230c'),
(6, 4, 18, '114c');


-- Таблица для ИС-2_1-числитель
create table if not exists is_schedule.is_2_1_schedule_n (
    lesson_id smallint generated always as identity primary key,
    weekday_number smallint references is_schedule.days,
    lesson_number smallint references is_schedule.timeslots,
    discipline_id smallint references is_schedule.disciplines,
    room text
);

insert into is_schedule.is_2_1_schedule_n (weekday_number, lesson_number, discipline_id, room)
values
(1, 4, 1, '114c'),
(1, 5, 20, '213c'),
(1, 6, 21, '211c'),
(1, 7, 8, '207c'),

(2, 1, 11, '212c'),
(2, 2, 11, '212c'),

(3, 1, 5, 'Стадион'),
(3, 2, 6, '209c'),

(5, 4, 9, '315c'),
(5, 5, 12, '315c'),
(5, 6, 22, '327c'),
(5, 7, 22, '327c'),

(6, 4, 18, '114c'),
(6, 5, 19, '213c'),
(6, 6, 19, '213c'),
(6, 7, 19, '213c');


-- Таблица для ИС-2_1-знаменатель
create table if not exists is_schedule.is_2_1_schedule_d (
    lesson_id smallint generated always as identity primary key,
    weekday_number smallint references is_schedule.days,
    lesson_number smallint references is_schedule.timeslots,
    discipline_id smallint references is_schedule.disciplines,
    room text
);

insert into is_schedule.is_2_1_schedule_d (weekday_number, lesson_number, discipline_id, room)
values
(1, 4, 1, '114c'),
(1, 5, 23, '213c'),
(1, 6, 21, '211c'),
(1, 7, 23, '212c'),

(3, 1, 5, 'Стадион'),
(3, 2, 6, '209c'),
(3, 3, 7, '207с'),

(5, 5, 12, '315c'),
(5, 6, 10, '133c'),
(5, 7, 10, '133c'),

(6, 4, 18, '114c'),
(6, 5, 2, '212c'),
(6, 6, 2, '212c');


-- Таблица для ИС-2_2-числитель
create table if not exists is_schedule.is_2_2_schedule_n (
    lesson_id smallint generated always as identity primary key,
    weekday_number smallint references is_schedule.days,
    lesson_number smallint references is_schedule.timeslots,
    discipline_id smallint references is_schedule.disciplines,
    room text
);

insert into is_schedule.is_2_2_schedule_n (weekday_number, lesson_number, discipline_id, room)
values
(1, 4, 1, '114c'),
(1, 5, 20, '213c'),
(1, 6, 21, '211c'),
(1, 7, 23, '212c'),

(3, 1, 5, 'Стадион'),
(3, 2, 6, '209c'),

(5, 4, 9, '315c'),
(5, 5, 12, '315c'),
(5, 6, 10, '208c'),
(5, 7, 10, '208c'),

(6, 4, 18, '114c'),
(6, 5, 2, '212c'),
(6, 6, 2, '212c');


-- Таблица для ИС-2_2-знаменатель
create table if not exists is_schedule.is_2_2_schedule_d (
    lesson_id smallint generated always as identity primary key,
    weekday_number smallint references is_schedule.days,
    lesson_number smallint references is_schedule.timeslots,
    discipline_id smallint references is_schedule.disciplines,
    room text
);

insert into is_schedule.is_2_2_schedule_d (weekday_number, lesson_number, discipline_id, room)
values
(1, 4, 1, '114c'),
(1, 5, 23, '213c'),
(1, 6, 21, '211c'),
(1, 7, 8, '207c'),

(2, 1, 11, '212c'),
(2, 2, 11, '212c'),

(3, 1, 5, 'Стадион'),
(3, 2, 6, '209c'),
(3, 3, 7, '207c'),

(5, 5, 12, '315c'),
(5, 6, 22, '327c'),
(5, 7, 22, '327c'),

(6, 4, 18, '114c'),
(6, 5, 19, '213c'),
(6, 6, 19, '213c'),
(6, 7, 19, '213c');

-- Создание мат. представлений
    --ИС 1_1
create materialized view if not exists is_schedule.mv_is_1_1_schedule as
(
    select
        row_number() over(order by week_type desc, weekday_number asc, lesson_number asc) as subgroup_2w_lesson_id,
        week_type,
        weekday_number,
        day_name,
        lesson_number,
        start_time,
        end_time,
        discipline_name,
        discipline_type_name,
        teacher_name,
        room
    from
    (
        select
            'Ч' as week_type,
            d.weekday_number,
            d.day_name,
            s.lesson_number,
            t.start_time,
            t.end_time,
            di.discipline_name,
            dt.discipline_type_name,
            te.teacher_name,
            s.room
        from is_schedule.is_1_1_schedule_n s
            join is_schedule.days d on d.weekday_number = s.weekday_number
            join is_schedule.timeslots t on t.lesson_number = s.lesson_number
            join is_schedule.disciplines di on di.discipline_id = s.discipline_id
                join is_schedule.discipline_types dt on di.discipline_type_id = dt.discipline_type_id
                join is_schedule.teachers te on di.teacher_id = te.teacher_id

        union all

        select
            'З' as week_type,
            d.weekday_number,
            d.day_name,
            s.lesson_number,
            t.start_time,
            t.end_time,
            di.discipline_name,
            dt.discipline_type_name,
            te.teacher_name,
            s.room
        from is_schedule.is_1_1_schedule_d s
            join is_schedule.days d on d.weekday_number = s.weekday_number
            join is_schedule.timeslots t on t.lesson_number = s.lesson_number
            join is_schedule.disciplines di on di.discipline_id = s.discipline_id
                join is_schedule.discipline_types dt on di.discipline_type_id = dt.discipline_type_id
                join is_schedule.teachers te on di.teacher_id = te.teacher_id
    ) sch
)
with data;

create unique index if not exists is_1_1_schedule_uidx on is_schedule.mv_is_1_1_schedule (subgroup_2w_lesson_id);

    --ИС 1_2
create materialized view if not exists is_schedule.mv_is_1_2_schedule as
(
    select
        row_number() over(order by week_type desc, weekday_number asc, lesson_number asc) as subgroup_2w_lesson_id,
        week_type,
        weekday_number,
        day_name,
        lesson_number,
        start_time,
        end_time,
        discipline_name,
        discipline_type_name,
        teacher_name,
        room
    from
    (
        select
            'Ч' as week_type,
            d.weekday_number,
            d.day_name,
            s.lesson_number,
            t.start_time,
            t.end_time,
            di.discipline_name,
            dt.discipline_type_name,
            te.teacher_name,
            s.room
        from is_schedule.is_1_2_schedule_n s
            join is_schedule.days d on d.weekday_number = s.weekday_number
            join is_schedule.timeslots t on t.lesson_number = s.lesson_number
            join is_schedule.disciplines di on di.discipline_id = s.discipline_id
                join is_schedule.discipline_types dt on di.discipline_type_id = dt.discipline_type_id
                join is_schedule.teachers te on di.teacher_id = te.teacher_id

        union all

        select
            'З' as week_type,
            d.weekday_number,
            d.day_name,
            s.lesson_number,
            t.start_time,
            t.end_time,
            di.discipline_name,
            dt.discipline_type_name,
            te.teacher_name,
            s.room
        from is_schedule.is_1_2_schedule_d s
            join is_schedule.days d on d.weekday_number = s.weekday_number
            join is_schedule.timeslots t on t.lesson_number = s.lesson_number
            join is_schedule.disciplines di on di.discipline_id = s.discipline_id
                join is_schedule.discipline_types dt on di.discipline_type_id = dt.discipline_type_id
                join is_schedule.teachers te on di.teacher_id = te.teacher_id
    ) sch
)
with data;

create unique index if not exists is_1_2_schedule_uidx on is_schedule.mv_is_1_2_schedule (subgroup_2w_lesson_id);

    --ИС 2_1
create materialized view if not exists is_schedule.mv_is_2_1_schedule as
(
    select
        row_number() over(order by week_type desc, weekday_number asc, lesson_number asc) as subgroup_2w_lesson_id,
        week_type,
        weekday_number,
        day_name,
        lesson_number,
        start_time,
        end_time,
        discipline_name,
        discipline_type_name,
        teacher_name,
        room
    from
    (
        select
            'Ч' as week_type,
            d.weekday_number,
            d.day_name,
            s.lesson_number,
            t.start_time,
            t.end_time,
            di.discipline_name,
            dt.discipline_type_name,
            te.teacher_name,
            s.room
        from is_schedule.is_2_1_schedule_n s
            join is_schedule.days d on d.weekday_number = s.weekday_number
            join is_schedule.timeslots t on t.lesson_number = s.lesson_number
            join is_schedule.disciplines di on di.discipline_id = s.discipline_id
                join is_schedule.discipline_types dt on di.discipline_type_id = dt.discipline_type_id
                join is_schedule.teachers te on di.teacher_id = te.teacher_id

        union all

        select
            'З' as week_type,
            d.weekday_number,
            d.day_name,
            s.lesson_number,
            t.start_time,
            t.end_time,
            di.discipline_name,
            dt.discipline_type_name,
            te.teacher_name,
            s.room
        from is_schedule.is_2_1_schedule_d s
            join is_schedule.days d on d.weekday_number = s.weekday_number
            join is_schedule.timeslots t on t.lesson_number = s.lesson_number
            join is_schedule.disciplines di on di.discipline_id = s.discipline_id
                join is_schedule.discipline_types dt on di.discipline_type_id = dt.discipline_type_id
                join is_schedule.teachers te on di.teacher_id = te.teacher_id
    ) sch
)
with data;

create unique index if not exists is_2_1_schedule_uidx on is_schedule.mv_is_2_1_schedule (subgroup_2w_lesson_id);

    --ИС 2_2
create materialized view if not exists is_schedule.mv_is_2_2_schedule as
(
    select
        row_number() over(order by week_type desc, weekday_number asc, lesson_number asc) as subgroup_2w_lesson_id,
        week_type,
        weekday_number,
        day_name,
        lesson_number,
        start_time,
        end_time,
        discipline_name,
        discipline_type_name,
        teacher_name,
        room
    from
    (
        select
            'Ч' as week_type,
            d.weekday_number,
            d.day_name,
            s.lesson_number,
            t.start_time,
            t.end_time,
            di.discipline_name,
            dt.discipline_type_name,
            te.teacher_name,
            s.room
        from is_schedule.is_2_2_schedule_n s
            join is_schedule.days d on d.weekday_number = s.weekday_number
            join is_schedule.timeslots t on t.lesson_number = s.lesson_number
            join is_schedule.disciplines di on di.discipline_id = s.discipline_id
                join is_schedule.discipline_types dt on di.discipline_type_id = dt.discipline_type_id
                join is_schedule.teachers te on di.teacher_id = te.teacher_id

        union all

        select
            'З' as week_type,
            d.weekday_number,
            d.day_name,
            s.lesson_number,
            t.start_time,
            t.end_time,
            di.discipline_name,
            dt.discipline_type_name,
            te.teacher_name,
            s.room
        from is_schedule.is_2_2_schedule_d s
            join is_schedule.days d on d.weekday_number = s.weekday_number
            join is_schedule.timeslots t on t.lesson_number = s.lesson_number
            join is_schedule.disciplines di on di.discipline_id = s.discipline_id
                join is_schedule.discipline_types dt on di.discipline_type_id = dt.discipline_type_id
                join is_schedule.teachers te on di.teacher_id = te.teacher_id
    ) sch
)
with data;

create unique index if not exists is_2_2_schedule_uidx on is_schedule.mv_is_2_2_schedule (subgroup_2w_lesson_id);

-- Создание функций для динамической выборки данных для любой подгруппы
    -- Формат запросов для выборки - select * from function_name()
    -- Формат аргумента group_ - '1_1', '1_2', '2_1', '2_2'
    -- Пример формата аргумента input_date - '02.03'
    -- Пример формата аргумента input_week_num - '9

    -- today_schedule
create or replace function is_schedule.today_schedule
(
    group_ text,
    yr int default extract(isoyear from current_date)::int
)
returns table
(
    "Дата" text,
    "Тип недели" text,
    "День недели" text,
    "№ пары" smallint,
    "Начало пары" time(0),
    "Конец пары" time(0),
    "Название предмета" text,
    "Тип занятия" text,
    "Преподаватель" text,
    "Кабинет" text
)
language plpgsql
as $$
    begin
        return query execute format('
            select
                to_char(dt.dt, ''dd.mm'') as dt,
                s.week_type,
                s.day_name,
                s.lesson_number,
                s.start_time,
                s.end_time,
                s.discipline_name,
                s.discipline_type_name,
                s.teacher_name,
                s.room
            from is_schedule.dates dt
            join is_schedule.mv_is_%s_schedule s
                on s.weekday_number = dt.weekday_number
                and s.week_type = dt.week_type
            where dt.dt = current_date
            order by s.subgroup_2w_lesson_id',
            group_,
            yr
        );
    end;
$$;


    -- curr_week
create or replace function is_schedule.curr_week_schedule
(
    group_ text,
    yr int default extract(isoyear from current_date)::int
)
returns table
(
    "№ недели" smallint,
    "Тип недели" text,
    "День недели" text,
    "Дата" text,
    "№ пары" smallint,
    "Начало пары" time(0),
    "Конец пары" time(0),
    "Название предмета" text,
    "Тип занятия" text,
    "Преподаватель" text,
    "Кабинет" text
)
language plpgsql
as $$
    begin
        return query execute format('
            select
                dt.week_number,
                s.week_type,
                s.day_name,
                to_char(wd.dt, ''dd.mm'') as dt,
                s.lesson_number,
                s.start_time,
                s.end_time,
                s.discipline_name,
                s.discipline_type_name,
                s.teacher_name,
                s.room
            from is_schedule.dates dt
            join is_schedule.mv_is_%s_schedule s on s.week_type = dt.week_type
            join is_schedule.dates wd
                on wd.week_number = dt.week_number
                and s.weekday_number = wd.weekday_number
                and dt.isoyear = wd.isoyear
            where dt.dt = current_date
            order by s.subgroup_2w_lesson_id',
            group_,
            yr
        );
    end;
$$;


    -- next_week
create or replace function is_schedule.next_week_schedule
(
    group_ text,
    yr int default extract(isoyear from current_date)::int
)
returns table
(
    "№ недели" smallint,
    "Тип недели" text,
    "День недели" text,
    "Дата" text,
    "№ пары" smallint,
    "Начало пары" time(0),
    "Конец пары" time(0),
    "Название предмета" text,
    "Тип занятия" text,
    "Преподаватель" text,
    "Кабинет" text
)
language plpgsql
as $$
    begin
        return query execute format('
            select
                dt.week_number,
                s.week_type,
                s.day_name,
                to_char(wd.dt, ''dd.mm'') as dt,
                s.lesson_number,
                s.start_time,
                s.end_time,
                s.discipline_name,
                s.discipline_type_name,
                s.teacher_name,
                s.room
            from is_schedule.dates dt
            join is_schedule.mv_is_%s_schedule s on s.week_type = dt.week_type
            join is_schedule.dates wd
                on wd.week_number = dt.week_number
                and s.weekday_number = wd.weekday_number
                and dt.isoyear = wd.isoyear
            where dt.dt = current_date + interval ''7'' day
            order by s.subgroup_2w_lesson_id',
            group_,
            yr
        );
    end;
$$;


    -- two_weeks
create or replace function is_schedule.two_weeks_schedule
(
    group_ text
)
returns table
(
    "Тип недели" text,
    "День недели" text,
    "№ пары" smallint,
    "Начало пары" time(0),
    "Конец пары" time(0),
    "Название предмета" text,
    "Тип занятия" text,
    "Преподаватель" text,
    "Кабинет" text
)
language plpgsql
as $$
    begin
        return query execute format('
            select
                week_type,
                day_name,
                lesson_number,
                start_time,
                end_time,
                discipline_name,
                discipline_type_name,
                teacher_name,
                room
            from is_schedule.mv_is_%s_schedule
            order by subgroup_2w_lesson_id',
            group_
        );
    end;
$$;


    -- date_input
create or replace function is_schedule.date_input_schedule
(
    group_ text,
    input_date text,
    yr int default extract(isoyear from current_date)::int
)
returns table
(
    "Дата" text,
    "Тип недели" text,
    "День недели" text,
     "№ пары" smallint,
     "Начало пары" time(0),
     "Конец пары" time(0),
    "Название предмета" text,
    "Тип занятия" text,
    "Преподаватель" text,
    "Кабинет" text
)
language plpgsql
as $$
    begin
        return query execute format('
            select
                to_char(dt.dt, ''dd.mm'') as dt,
                s.week_type,
                s.day_name,
                s.lesson_number,
                s.start_time,
                s.end_time,
                s.discipline_name,
                s.discipline_type_name,
                s.teacher_name,
                s.room
            from is_schedule.dates dt
            join is_schedule.mv_is_%s_schedule s
                on s.weekday_number = dt.weekday_number
                and s.week_type = dt.week_type
            where
                to_char(dt.dt, ''dd.mm'') = %L
                and dt.isoyear = %s
            order by s.subgroup_2w_lesson_id',
            group_,
            input_date,
            yr
        );
    end;
$$;


    -- week_input_by_date
create or replace function is_schedule.week_input_by_date_schedule
(
    group_ text,
    input_date text,
    yr int default extract(isoyear from current_date)::int
)
returns table
(
    "№ недели" smallint,
    "Тип недели" text,
    "День недели" text,
    "Дата" text,
    "№ пары" smallint,
    "Начало пары" time(0),
    "Конец пары" time(0),
    "Название предмета" text,
    "Тип занятия" text,
    "Преподаватель" text,
    "Кабинет" text
)
language plpgsql
as $$
    begin
        return query execute format('
            select
                dt.week_number,
                s.week_type,
                s.day_name,
                to_char(wd.dt, ''dd.mm'') as dt,
                s.lesson_number,
                s.start_time,
                s.end_time,
                s.discipline_name,
                s.discipline_type_name,
                s.teacher_name,
                s.room
            from is_schedule.dates dt
            join is_schedule.mv_is_%s_schedule s on s.week_type = dt.week_type
            join is_schedule.dates wd
                on wd.week_number = dt.week_number
                and s.weekday_number = wd.weekday_number
                and dt.isoyear = wd.isoyear
            where
                to_char(dt.dt, ''dd.mm'') = %L
                and dt.isoyear = %s
            order by s.subgroup_2w_lesson_id',
            group_,
            input_date,
            yr
        );
    end;
$$;


    -- week_input_by_week_number
create or replace function is_schedule.week_input_by_week_number_schedule
(
    group_ text,
    input_week_num int,
    yr int default extract(isoyear from current_date)::int
)
returns table
(
    "№ недели" smallint,
    "Тип недели" text,
    "День недели" text,
    "Дата" text,
    "№ пары" smallint,
    "Начало пары" time(0),
    "Конец пары" time(0),
    "Название предмета" text,
    "Тип занятия" text,
    "Преподаватель" text,
    "Кабинет" text
)
language plpgsql
as $$
    begin
        return query execute format('
            select
                dt.week_number,
                s.week_type,
                s.day_name,
                to_char(dt.dt, ''dd.mm'') as dt,
                s.lesson_number,
                s.start_time,
                s.end_time,
                s.discipline_name,
                s.discipline_type_name,
                s.teacher_name,
                s.room
            from is_schedule.dates dt
            join is_schedule.mv_is_%s_schedule s
                on s.week_type = dt.week_type
                and s.weekday_number = dt.weekday_number
            where
                dt.week_number = %L
                and dt.isoyear = %s
            order by s.subgroup_2w_lesson_id',
            group_,
            input_week_num,
            yr
        );
    end;
$$;


    -- numerator
create or replace function is_schedule.numerator_schedule
(
    group_ text
)
returns table
(
    "Тип недели" text,
    "День недели" text,
    "№ пары" smallint,
    "Начало пары" time(0),
    "Конец пары" time(0),
    "Название предмета" text,
    "Тип занятия" text,
    "Преподаватель" text,
    "Кабинет" text
)
language plpgsql
as $$
    begin
        return query execute format('
            select
                week_type,
                day_name,
                lesson_number,
                start_time,
                end_time,
                discipline_name,
                discipline_type_name,
                teacher_name,
                room
            from is_schedule.mv_is_%s_schedule
            where week_type = ''Ч''
            order by subgroup_2w_lesson_id',
            group_
        );
    end;
$$;


    -- denominator
create or replace function is_schedule.denominator_schedule
(
    group_ text
)
returns table
(
    "Тип недели" text,
    "День недели" text,
    "№ пары" smallint,
    "Начало пары" time(0),
    "Конец пары" time(0),
    "Название предмета" text,
    "Тип занятия" text,
    "Преподаватель" text,
    "Кабинет" text)
language plpgsql
as $$
    begin
        return query execute format('
            select
                week_type,
                day_name,
                lesson_number,
                start_time,
                end_time,
                discipline_name,
                discipline_type_name,
                teacher_name,
                room
            from is_schedule.mv_is_%s_schedule
            where week_type = ''З''
            order by subgroup_2w_lesson_id',
            group_
        );
    end;
$$;

-- На случай перезаписи:
-- drop function is_schedule.curr_week_schedule(text);
-- drop function is_schedule.denominator_schedule(text);
-- drop function is_schedule.next_week_schedule(text);
-- drop function is_schedule.numerator_schedule(text);
-- drop function is_schedule.today_schedule(text);
-- drop function is_schedule.two_weeks_schedule(text);
-- drop function is_schedule.date_input_schedule(text, text);
-- drop function is_schedule.week_input_by_date_schedule(text, text);
-- drop function is_schedule.week_input_by_week_number_schedule(text, int);


-- Создание служебных процедур

    -- анализ для всех таблиц и мат. представлений в выбранной схеме
create or replace procedure is_schedule.schema_analyze(schema_ text default 'is_schedule')
language plpgsql
as $$
    declare obj text;
    begin
        for obj in
        (
            select tablename::text as obj_name
            from pg_catalog.pg_tables
            where schemaname = schema_
            union all
            select matviewname::text as obj_name
            from pg_catalog.pg_matviews
            where schemaname = schema_
        ) loop
            execute 'analyze ' || quote_ident(schema_) || '.' || quote_ident(obj);
            commit;
            execute format('
                insert into is_bot_logs.data_update_procedures_log (command, schema_name, obj_name, exec_dttm)
                    values (%L, %L, %L, %L);',
                'analyze',
                schema_,
                obj,
                now()
            );
            commit;
        end loop;
    end;
$$;


    -- обновление всех мат. представлений в выбранной схеме
create or replace procedure is_schedule.schema_refresh_mv(schema_ text default 'is_schedule')
language plpgsql
as $$
    declare obj text;
    begin
        for obj in
        (
            select matviewname::text as obj_name
            from pg_catalog.pg_matviews
            where schemaname = schema_
        )
        loop
            execute 'refresh materialized view ' || quote_ident(schema_) || '.' || quote_ident(obj);
            commit;
            execute format('
                insert into is_bot_logs.data_update_procedures_log (command, schema_name, obj_name, exec_dttm)
                    values (%L, %L, %L, %L);',
                'refresh materialized view',
                schema_,
                obj,
                now()
            );
            commit;
        end loop;
    end;
$$;

call is_schedule.schema_analyze();