import os

# Параметры конфигурации проекта
class CONF_PRJ:
    # Директория для размещения журналов работы
    LOG_DIR = r'/app/deployed_projects/logs/codegen_processor'

    # Уровень журналирования
    LOG_LEVEL = 'INFO'  # 'ERROR'
    # Технический разделитель принятый в проекте
    TECH_DELIMITER = '_'

    # Ссылка на API редактора метаданных
    EDITOR_API_URL = f"http://{os.environ['WEB_HOST']}:{os.environ['EDITOR_PORT']}"
    # Дополнение для расчёта hash token
    HASH_SALT = f"DatAx_2023_{os.environ['ENV_NAME']}"

# Параметры конфигурации GIT
class CONF_GIT:
    # Директория для временного размещения веток кода
    TEMP = r"/app/deployed_projects/tmp/codegen_processor/git"

    # Репозиторий проекта с ветками результатов генерации на основе метаданных
    # Могут быть задействованы переменные %(token) %(user)
    # GITLAB    https://%(user)s:%(token)s@gitlab.com/username/myrepo.git
    # BITBUCKET https://%(user)s:%(token)s@gitlab.com/username/myrepo.git
    # GTIHUB    https://%(token)s@github.com/username/repo.git
    if os.environ['REPO_TYPE'] in ['Gitlab', 'Bitbucket']:
        REPO = rf"https://%(user)s:%(token)s@{os.environ['GIT_REPO']}"
    elif os.environ['REPO_TYPE'] == 'Github':
        REPO = rf"https://%(token)s@@{os.environ['GIT_REPO']}"
    # Базовая ветвь. на основе которой следует создавать результаты
    BASE_BRANCH = os.environ['META_PROCESSOR_BRANCH']
    # Префикс, добавляемый к названию итоговой ветки кода
    GEN_BRANCH_SUFFIX = f"_gen_{os.environ['ENV_NAME']}"
