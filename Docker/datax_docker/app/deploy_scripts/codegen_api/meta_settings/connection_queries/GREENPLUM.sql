with sat_name as (
	select 
		sat.table_schema, 
		sat.table_name,
		'test table'::text as master_table_name,
		'test schema'::text as master_table_schema,
		'test style distr'::text as distr_style_cd
	from information_schema.tables sat
	where 1=1
	and sat.table_schema = ''
	and sat.table_name = ''
),
myconstraint as (
	select 
	ccu.constraint_name,
	ccu.table_name,
	ccu.table_schema,
	ccu.column_name,
	case when tc.constraint_type = 'PRIMARY KEY' then 'PK'
		when tc.constraint_type = 'FOREIGN KEY' then 'FK'
	end constraint_type
	from information_schema.constraint_column_usage ccu
	inner join information_schema.table_constraints tc
		on ccu.constraint_schema = tc.constraint_schema 
		and ccu.constraint_name  = tc.constraint_name 
		and ccu.table_name       = tc.table_name 
		and ccu.table_schema     = tc.table_schema
	where tc.constraint_type in ('PRIMARY KEY','FOREIGN KEY')
),
indexs as (
	select ai.schemaname,
		   ai.tablename,
		   string_agg(ai.indexdef,';') as index_list 
	from pg_indexes ai  
	left join sat_name sat 
	on ai.schemaname = sat.table_schema and ai.tablename = sat.table_name
	where 1=1
	and ai.schemaname = sat.table_schema and ai.tablename = sat.table_name
	group by  ai.schemaname,ai.tablename
),
distribution_all as (
 	select d.localoid,
	   n.nspname as table_schema, 
	   c.relname as table_name, 
	   d.policytype,
	   d.distkey,
	   array_agg(a.attname order by dk.key_num) as distkey_names
from pg_catalog.gp_distribution_policy d
join pg_catalog.pg_class c on c."oid" = d.localoid
left join unnest(d.distkey::int2[]) with ordinality dk (distkey, key_num) on 1=1
left join pg_catalog.pg_attribute a on c."oid" = a.attrelid and a.attnum = dk.distkey
left join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
group by d.localoid,
	   	 n.nspname, 
	     c.relname, 
	     d.policytype,
	     d.distkey
)
select
-- Идентификатор строки
	row_number () over (order by col.ordinal_position) as id,
-- ID коннектора к БД
	'GREENPLUM'::text as connector_id,
-- Схема
	tb.table_schema as schema_name,
-- Таблица
	tb.table_name,
-- Тип таблицы
--	tb.table_type as type,
-- Табличное пространство
--	pgtab.tablespace as tablespace_name,
-- Описание таблицы
	obj_description(format('%s.%s', tb.table_schema, tb.table_name)::regclass) as table_description,
-- PK
	case when (constr.constraint_type = 'PRIMARY KEY') then true else false end as PK,
-- Описание поля
	pg_catalog.col_description(format('%s.%s',col.table_schema,col.table_name)::regclass::oid,col.ordinal_position) as column_description,
-- Имя столбца
	col.column_name,
-- Позиция
	col.ordinal_position as column_index,
-- NULL or NOT NULL
	col.is_nullable,
-- Тип данных
	case when col.data_type ='ARRAY' then  translate(col.udt_name::regtype::text, '[]','') else col.data_type end as data_type_cd,
-- Макс. длина строкового поля или числового поля
	case when col.data_type in ('character','bit','bit varying','character varying') then  col.character_maximum_length when col.data_type in ('numeric','decimal') then col.numeric_precision else null end as "length",
	case when col.data_type ='ARRAY' then true else false end as is_array,
-- Точность числового поля
	case when col.data_type in ('numeric','decimal') then col.numeric_scale else null end as "scale",
-- (FK) Код типа поля. 
	case when (split_part(tb.table_name,'_', 1) not in ('hub','lnk','ref','sat','hsat')) then constr.constraint_type
	else null
	end as tab_col_type_cd,
-- (FK->hub_column_types) Код типа поля, которое может входить в hub. 
	case when (split_part(tb.table_name,'_', 1) = 'hub' ) then constr.constraint_type
	else null
	end as hub_col_type_cd,
-- (FK) Код типа поля, которое может входить в link. 
	case when (split_part(tb.table_name,'_', 1) = 'lnk') then constr.constraint_type
	else null
	end as lnk_col_type_cd,
-- (FK) Код типа поля. См. справочник ref_column_types. 
	case when (split_part(tb.table_name,'_', 1) = 'ref') then constr.constraint_type
	else null
	end as ref_col_type_cd,
-- (FK->sat_column_types). Код типа поля, которое может входить в satellite. 
	case when (split_part(tb.table_name,'_', 1) in ('sat','hsat')) then constr.constraint_type
	else null
	end as sat_col_type_cd,
-- Имя таблицы, на которую ссылается поле
	case when constr.constraint_type = 'FK' then sat.master_table_name else null end as master_table_name,
-- Имя схемы таблицы master_table_name
	case when sat.master_table_name is null then null else tb.table_schema end as master_table_schema,
-- Стиль дистрибуции для таблицы
--	$distr_style_cd,
-- Поле дистрибьюции
	substring(pgview.definition from '(?:.*\sSELECT\s)?([^,]*' || col.column_name || '[^,\n]*)(?:,\n|,\s|\sFROM|\nFROM|FROM)?') as original_col_name,
	case when (col.column_name =  all_dist_indx.distkey_names) then all_dist_indx.key_num else null end as distr_order,
	case when tb.table_type = 'VIEW' then true else false end as is_view,
	substring(pgview.definition from '(?:.*)(?:\n\s\sWHERE)(.*)(?:;$)') as where_clause,
	case when (all_dist.policytype ='p' and all_dist.distkey_names != array [null::name]) then 'KEY'::varchar(20) when (all_dist.policytype ='p' and all_dist.distkey_names = array [null::name]) then 'RND'::varchar(20) when all_dist.policytype ='r' then 'RPL'::varchar(20) else null::varchar(20) end  as distr_style_cd
-- Индексы
--	indx.index_list
-- Партиционирование
--	partition_expr
from information_schema.tables tb
 left join indexs indx
	on tb.table_name = indx.tablename and tb.table_schema = indx.schemaname
 left join sat_name sat
	on tb.table_schema = sat.table_schema and tb.table_name = sat.table_name
 inner join information_schema.columns col
	on tb.table_name = col.table_name and tb.table_schema = col.table_schema and tb.table_catalog = col.table_catalog
 left join myconstraint  constr 
 	on tb.table_name = constr.table_name and tb.table_schema = constr.table_schema and col.column_name = constr.column_name
 left join distribution_all all_dist
 	on tb.table_name = all_dist.table_name and tb.table_schema = all_dist.table_schema 
 left join unnest(distkey_names) with ordinality all_dist_indx (distkey_names, key_num) on col.column_name = all_dist_indx.distkey_names
 left join pg_tables pgtab
 	on tb.table_name = pgtab.tablename and tb.table_schema = pgtab.schemaname
 left join pg_views pgview
 	on tb.table_name = pgview.viewname and tb.table_schema = pgview.schemaname
where 1=1
and tb.table_schema = sat.table_schema and tb.table_name = sat.table_name