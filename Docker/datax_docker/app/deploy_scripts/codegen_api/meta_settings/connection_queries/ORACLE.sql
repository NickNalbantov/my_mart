WITH indexs AS (
	select 
		ai.TABLE_OWNER,
		ai.TABLE_name, 
		LISTAGG(DBMS_METADATA.GET_DDL('INDEX',ai.index_name, ai.OWNER),';') WITHIN GROUP (ORDER BY ai.TABLE_OWNER,ai.TABLE_name) AS index_list
	from all_indexes ai  
		WHERE ai.table_owner ='' 
			  AND ai.table_name ='' 
		GROUP BY  ai.TABLE_OWNER,
				  ai.TABLE_name 
),
tables_and_views AS ( 
	SELECT 
		OWNER AS OWNER,
		TABLE_NAME AS TABLE_NAME,
		'TABLE' as TABLE_TYPE,
		NULL AS view_definition
	FROM ALL_TABLES 
	UNION 
	SELECT 
		OWNER AS OWNER, 
		VIEW_NAME AS TABLE_NAME, 
		'VIEW' as TABLE_TYPE,
		TEXT_VC AS view_definition
	FROM ALL_VIEWS 
	WHERE OWNER not in ('SYS', 'SYSTEM')
), 
myconstraint as ( 
	SELECT 
		ccu.TABLE_NAME, ccu.OWNER, 
		ccu.column_name,
		tc.constraint_type 
	FROM ALL_CONS_COLUMNS ccu
	INNER JOIN ALL_CONSTRAINTS tc 
		ON ccu.CONSTRAINT_NAME  = tc.CONSTRAINT_NAME 
		and ccu.TABLE_NAME = tc.TABLE_NAME 
		and ccu.OWNER = tc.OWNER WHERE tc.constraint_type = 'P' 
		AND tc.STATUS = 'ENABLED' 
		and tc.OWNER not in ('SYS', 'SYSTEM')
)
SELECT distinct 
	tb.OWNER as schema_name,
	tb.TABLE_NAME as table_name,
	CASE WHEN tb.table_type = 'VIEW' THEN 1 ELSE 0 END AS is_view,
	ccomm.COMMENTS as column_description,
	col.COLUMN_NAME as column_name,
	col.COLUMN_ID as column_num,
	col.NULLABLE as is_null,
	tcomm.COMMENTS as table_description,
	CASE WHEN col.DATA_TYPE LIKE 'TIMESTAMP%' THEN lower(replace(col.DATA_TYPE, substr(col.DATA_TYPE, 10,3),'')) ELSE lower(col.DATA_TYPE) END  as data_type_cd,
	case when col.DATA_TYPE in ('NVARCHAR2','VARCHAR2','RAW','UROWID','CHAR','NCHAR') THEN col.DATA_LENGTH  when col.DATA_TYPE in ('FLOAT','NUMBER') THEN col.DATA_PRECISION ELSE NULL END as length,
	0 as is_array,
    null as where_clause,
	REGEXP_SUBSTR(tb.view_definition, '(^select|^SELECT)?([^,]*' || col.COLUMN_NAME || '[^,\n]*)[\n\s]*(,|from|FROM)') AS original_col_name,
	case when col.DATA_TYPE = 'NUMBER' THEN col.DATA_SCALE ELSE NULL END as scale,
	case 
		when lower(substr(tb.table_name,1,3)) = 'sat' then constr.constraint_type 
		else null 
	end as sat_col_type_cd,
	null as distr_order 
FROM TABLES_AND_VIEWS tb 
INNER JOIN ALL_TAB_COLUMNS col 
	ON tb.TABLE_NAME = col.TABLE_NAME 
	AND tb.OWNER = col.OWNER  
LEFT JOIN indexs indx 
	ON  tb.TABLE_NAME = indx.TABLE_NAME 
	AND tb.OWNER = indx.TABLE_OWNER  
LEFT JOIN ALL_TAB_COMMENTS tcomm 
	ON tcomm.OWNER = tb.OWNER 
	AND tcomm.TABLE_NAME = tb.TABLE_NAME 
	AND tcomm.TABLE_TYPE = tb.TABLE_TYPE
LEFT JOIN ALL_COL_COMMENTS ccomm 
	ON ccomm.OWNER = tb.OWNER 
	AND ccomm.TABLE_NAME = tb.TABLE_NAME 
	AND ccomm.COLUMN_NAME = col.COLUMN_NAME 
LEFT JOIN myconstraint constr 
	on tb.TABLE_NAME = constr.TABLE_NAME 
	and tb.OWNER = constr.OWNER 
	and col.COLUMN_NAME = constr.COLUMN_NAME 
WHERE 
	tb.owner = '' 
	and tb.table_name = '';