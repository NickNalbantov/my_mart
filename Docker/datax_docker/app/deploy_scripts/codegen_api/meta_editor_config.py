import os

# Параметры конфигурации GIT
class CONF_GIT:
    # Директория для временного размещения веток кода
    TEMP = r"/app/deployed_projects/tmp/codegen_api/git"

    # Репозиторий проекта с ветками метаданных
    # Могут быть задействованы переменные %(token) %(user)
    # GITLAB    https://%(user)s:%(token)s@gitlab.com/username/myrepo.git
    # BITBUCKET https://%(user)s:%(token)s@gitlab.com/username/myrepo.git
    # GTIHUB    https://%(token)s@github.com/username/repo.git
    if os.environ['REPO_TYPE'] in ['Gitlab', 'Bitbucket']:
        REPO = rf"https://%(user)s:%(token)s@{os.environ['GIT_REPO']}"
    elif os.environ['REPO_TYPE'] == 'Github':
        REPO = rf"https://%(token)s@{os.environ['GIT_REPO']}"

    # Расположение версии метаданных относительно корня репозитория
    META_PATH = "postgre_scripts"
    SKYNET_PATH = "skynet_scripts"

    # Следует ли проверять подлинность токена перед выполнением операций
    TOKEN_CHECK_ENABLED = False
    # Заголовок запроса для провеки правильности токена доступа
    # GITLAB   'PRIVATE-TOKEN'
    # BITBUCKET ?
    # GTIHUB    'Authorization'
    if os.environ['REPO_TYPE'] == 'Gitlab':
        TOKEN_CHECK_HEADER = 'PRIVATE-TOKEN'
    elif os.environ['REPO_TYPE'] == 'Github':
        TOKEN_CHECK_HEADER = 'Authorization'
    elif os.environ['REPO_TYPE'] == 'Bitbucket':
        TOKEN_CHECK_HEADER = None # под замену

    # Заголовок запроса для провеки правильности токена доступа
    # GITLAB   '%(token)s'
    # BITBUCKET ?
    # GTIHUB    'token %(token)s'
    if os.environ['REPO_TYPE'] == 'Gitlab':
        TOKEN_CHECK_HEADER_VALUE = '%(token)s'
    elif os.environ['REPO_TYPE'] == 'Github':
        TOKEN_CHECK_HEADER_VALUE = 'token %(token)s'
    elif os.environ['REPO_TYPE'] == 'Bitbucket':
        TOKEN_CHECK_HEADER_VALUE = None # под замену

    # URL запроса для провеки правильности токена доступа
    # GITLAB   'https://gitlab.com/api/v4/projects'
    # BITBUCKET ?
    # GTIHUB    'https://api.github.com/user/repos'
    TOKEN_CHECK_URL = f"https://{os.environ['GIT_TOKEN_CHECK']}"


# Параметры конфигурации утилиты PostgREST
class CONF_PGREST:
    # URL для доступа к утилите
    URL = os.environ['PGRST_OPENAPI_SERVER_PROXY_URI']
    # Канал LISTEN/NOTIFY
    POSTGREST_DB_CHANNEL = os.environ['POSTGREST_DB_CHANNEL']
    # Схема с инфраструктурой PostgREST
    POSTGREST_ADMIN_SCHEMA = os.environ['POSTGREST_ADMIN_SCHEMA']
    # Роль-authenticator
    POSTGREST_AUTHENTICATOR_ROLE = os.environ['POSTGREST_AUTHENTICATOR_ROLE']
    # Время жизни JWT-токена
    JWT_TOKEN_EXPIRATION = float(os.environ['JWT_TOKEN_EXPIRATION'])
    # Секретный ключ для JWT-токена
    JWT_SECRET = os.environ['JWT_SECRET']

# Параметры конфигурации проекта
class CONF_PRJ:
    # Директория для размещения журналов работы
    LOG_DIR = r'/app/deployed_projects/logs/codegen_api'
    # Уровень журналирования
    LOG_LEVEL = 'INFO'  # 'ERROR'
    # Технический разделитель принятый в проекте
    TECH_DELIMITER = '_'
    # Дополнение для расчёта hash token
    HASH_SALT = f"DatAx_2023_{os.environ['ENV_NAME']}"



class CONF_PRJ_PROCESSOR:
    # Сервер
    HOST = os.environ['WEB_HOST']
    # Порт
    PORT = os.environ['PROCESSOR_PORT']
    # Строка подключения
    URL = f'http://{HOST}:{PORT}'


# Параметры конфигурации PostgreSQL
class CONF_PG:
    # Сервер
    HOST = os.environ['DB_HOST']
    # Порт
    PORT = '5432'
    # База данных
    DATABASE = os.environ['DB_NAME']
    # Пользователь
    DBA_LOGIN = os.environ['DBA_LOGIN']
    # Пароль
    DBA_PASSWORD = os.environ['DBA_PASSWORD']
    # Строка подключения, вариант синтаксиса
    DBA_CONNECT_STRING = f'host={HOST} port={PORT} dbname={DATABASE} user={DBA_LOGIN} password={DBA_PASSWORD}'
    # Строка подключения, вариант синтаксиса
    DBA_CONNECT_URI = f"postgres://{DBA_LOGIN}:{DBA_PASSWORD}@{HOST}:{PORT}/{DATABASE}"


# Параметры валидатора коннектора
class CONF_CONN_MANAGER:
    META_CONN_SETTINGS_FOLDER = r'/app/deploy_scripts/codegen_api/meta_settings'
    META_CSV_FOLDER = r'/app/deployed_projects/tmp/codegen_api/git/'

    SETTINGS_NAME = os.path.join(META_CONN_SETTINGS_FOLDER,'connection_settings/connection_settings.json')
    URI_TEMPLATES_PATH = os.path.join(META_CONN_SETTINGS_FOLDER,'connection_settings/connections_uris.json')

    CRYPT_KEY = os.environ['CRYPT_KEY']
    LOG_DIR =  os.path.join(CONF_PRJ.LOG_DIR,'db_connector_logs')
    LOG_FILENAME = 'db_connector'

    RECODING_DICTS_FOLDER = os.path.join(META_CONN_SETTINGS_FOLDER,'recoding_dicts')