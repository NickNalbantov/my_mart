import os
from cryptography.fernet import Fernet

class FernetEncoder():
    
    def __init__(self,  in_filepath: str, out_filepath: str = None, crypt_key: str = None):
        self.in_filepath = in_filepath
        self.out_filepath = out_filepath if out_filepath is not None else self.in_filepath # Если не указать путь для нового зашифрованного файла - перезапишет входной файл
        self.crypt_key = crypt_key if crypt_key is not None else os.environ['CRYPT_KEY'] # Если не указать явно - возьмёт ключ из переменной Docker в /.env


    def encrypt_file(self):

        with open(self.in_filepath,'rb') as in_file:
            data = in_file.read()

        fernet = Fernet(self.crypt_key)
        encrypted = fernet.encrypt(data)

        with open(self.out_filepath,'wb') as out_file:
            out_file.write(encrypted)


    def decrypt_file(self):

        with open(self.in_filepath,'rb') as in_file:
            data = in_file.read()

        fernet = Fernet(self.crypt_key)
        decrypted = fernet.decrypt(data)

        with open(self.out_filepath,'wb') as out_file:
            out_file.write(decrypted)