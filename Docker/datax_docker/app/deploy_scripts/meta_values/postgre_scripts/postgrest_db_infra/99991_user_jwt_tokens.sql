-- Таблица для хранения JWT-токенов

create table user_jwt_tokens
  (
    role_name text primary key,
    jwt_token text not null,
    token_expiration_epoch bigint not null,
    token_expiration_dttm timestamptz not null,
    token_creation_dttm timestamptz not null
  );

create table user_jwt_tokens_archive
  (
    role_name text not null,
    jwt_token text primary key,
    token_deletion_dttm timestamptz not null
  );

-- Очистка и перемещение токенов в архив

create or replace function user_jwt_tokens_cleanup_function ()
returns trigger
language plpgsql
as
$$
    begin
        with del as
        (
          -- Вычищаем просроченные токены и токены удалённых юзеров
          delete 
          from user_jwt_tokens t
          where t.token_expiration_epoch <= extract(epoch from now())
                or not exists
                (
                  select 
                  from pg_catalog.pg_authid a
                  where t.role_name = a.rolname
                )
          returning role_name, jwt_token
        )

        -- Перемещаем удалённые токены в архив
        insert into user_jwt_tokens_archive 
          select role_name,
                 jwt_token, 
                 now()
          from del;

        return NEW;
    end;
$$;

create or replace trigger user_jwt_tokens_cleanup_trigger before insert or update
on user_jwt_tokens
for each statement
execute function user_jwt_tokens_cleanup_function();