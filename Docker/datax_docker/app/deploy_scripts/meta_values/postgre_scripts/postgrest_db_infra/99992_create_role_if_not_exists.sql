create or replace function create_role_if_not_exists
(
    role_name text,
    password_ text default null
)
returns void
language plpgsql
as
$$
    begin
        if exists (
            select from pg_catalog.pg_authid
            where rolname = role_name
                  ) then

            raise notice 'Role % already exists, skipping', role_name;
        else
            begin   -- nested block
                if password_ is not null then
                    execute format($fmt$create role %1$I noinherit nocreatedb nosuperuser login password %2$L$fmt$, role_name, password_);
                else
                    execute format($fmt$create role %1$I noinherit nocreatedb nosuperuser nologin$fmt$, role_name);
                end if;
            exception
                when duplicate_object then
                    raise notice 'Role % was just created by a concurrent transaction, skipping', role_name;
            end;
        end if;
    end;
$$;