#!/bin/bash

# Выполнение SQL-скриптов через psql

psql -d ${POSTGRES_DB} -U ${POSTGRES_USER} << EOT > dev/null

-- Dropping default objects

drop database postgres with (force);
drop schema if exists public cascade;

-- Creating PostgREST infrastructure

create schema if not exists ${POSTGREST_ADMIN_SCHEMA};
set search_path = ${POSTGREST_ADMIN_SCHEMA};

    -- JWT generation

create extension if not exists pgcrypto with schema ${POSTGREST_ADMIN_SCHEMA};

\i /init_db_structure/postgrest_db_infra/99990_pgjwt.sql

\i /init_db_structure/postgrest_db_infra/99991_user_jwt_tokens.sql

\i /init_db_structure/postgrest_db_infra/99992_create_role_if_not_exists.sql


  -- Функция генерации JWT-токенов

create or replace function ${POSTGREST_ADMIN_SCHEMA}.jwt_token_generation
(
    role_name text default current_role, 
    expiration numeric default ${JWT_TOKEN_EXPIRATION},
    jwt_secret text default '${JWT_SECRET}'
) 
returns ${POSTGREST_ADMIN_SCHEMA}.user_jwt_tokens 
as 
\$\$
    select r.role as role_name,
           ${POSTGREST_ADMIN_SCHEMA}.sign(row_to_json(r), \$3) as jwt_token,
           r.exp as token_expiration_epoch,
           to_timestamp(r.exp) as token_expiration_dttm,
           now() as token_creation_dttm
    from 
    (
      select \$1::text as role,
             round((extract(epoch from now())::numeric) + coalesce(case when \$2 > 0 then \$2 else 0 end, 0))::bigint AS exp
    ) r
\$\$
language sql;


    -- PostgREST roles

create role ${POSTGREST_ANON_ROLE} noinherit nocreatedb nosuperuser nologin;
create role ${POSTGREST_AUTHENTICATOR_ROLE} noinherit nocreatedb nosuperuser login password '${POSTGREST_AUTHENTICATOR_PASSWORD}';

revoke all on schema ${POSTGREST_ADMIN_SCHEMA} from public;
revoke all on all tables in schema ${POSTGREST_ADMIN_SCHEMA} from public;
revoke all on all sequences in schema ${POSTGREST_ADMIN_SCHEMA} from public;
revoke all on all routines in schema ${POSTGREST_ADMIN_SCHEMA} from public;
alter default privileges revoke all on schemas from public;
alter default privileges revoke all on tables from public;
alter default privileges revoke all on routines from public;
alter default privileges revoke all on sequences from public;
alter default privileges revoke all on types from public;

grant ${POSTGREST_ANON_ROLE} to ${POSTGREST_AUTHENTICATOR_ROLE};
alter default privileges for role ${POSTGREST_ANON_ROLE} grant all on schemas to ${POSTGREST_AUTHENTICATOR_ROLE};

  -- Создание токена для authenticator

insert into ${POSTGREST_ADMIN_SCHEMA}.user_jwt_tokens
	select *
  from ${POSTGREST_ADMIN_SCHEMA}.jwt_token_generation('${POSTGREST_AUTHENTICATOR_ROLE}', 999999999);

EOT

echo "All SQL queries completed"


# Ограничения доступа к БД по IP

if [[ "${POSTGRES_ALLOWED_SUBNET}" != 'all' ]]; then

  # Формируем список IP для параметра listen_addresses в postgresql.conf

  docker_subnet_ips=$(/bin/bash /init_db_structure/unnest_network.sh ${DOCKER_SUBNET})

  readarray -td ' ' postgres_allowed_subnets_arr <<< "${POSTGRES_ALLOWED_SUBNET} "
  unset postgres_allowed_subnets_arr[-1]

  postgres_allowed_subnet_ips=""
  shopt -s lastpipe

  for allowed_subnet in "${postgres_allowed_subnets_arr[@]}"
  do
    next_subnet=$(/bin/bash /init_db_structure/unnest_network.sh ${allowed_subnet})
    postgres_allowed_subnet_ips+=", ${next_subnet}"
  done

  postgres_allowed_subnet_ips=$(echo ${postgres_allowed_subnet_ips} | cut -c 3-)

  outside_ips="${postgres_allowed_subnet_ips}, ${POSTGRES_HOST}, ${POSTGREST_HOST}, ${WEB_HOST}"
  outside_ips_uniq=`echo $outside_ips | sed -e "s/\s\\+//g" | sed -e "s/,/\\n/g" | sort | uniq | sed -z "s/\n/, /g;s/, $/\n/"`

  ip_list="${docker_subnet_ips}, ${outside_ips_uniq}"

  # Подмена listen_addresses в postgresql.conf

  sed -i "s/listen_addresses = '\*'/listen_addresses = '${ip_list}'/" /var/lib/postgresql/data/postgresql.conf


  # Редактирование pg_hba.conf

  sed -i "s|host all all all scram-sha-256|host all all ${DOCKER_SUBNET} scram-sha-256\n|" /var/lib/postgresql/data/pg_hba.conf

  outside_ips_with_mask="$(echo ${POSTGRES_ALLOWED_SUBNET} | sed -e "s/\s\\+/, /g"), ${POSTGRES_HOST}/32, ${POSTGREST_HOST}/32, ${WEB_HOST}/32"
  outside_ips_with_mask_uniq=`echo $outside_ips_with_mask | sed -e "s/\s\\+//g" | sed -e "s/,/\\n/g" | sort | uniq | sed -z "s/\n/,/g;s/,$/\n/"`

  readarray -td ',' outside_ips_with_mask_uniq_arr <<< "${outside_ips_with_mask_uniq},"
  unset outside_ips_with_mask_uniq_arr[-1]

  for subnet in "${outside_ips_with_mask_uniq_arr[@]}"
  do
    echo "host all all $subnet scram-sha-256" >> /var/lib/postgresql/data/pg_hba.conf
  done

fi