// Класс для измерения элементов интерфейса
class UiMeasure {

    static getHeightSum(elt, includeElt, marginTop, paddingTop, paddingBottom, marginBottom) {
        let styles = window.getComputedStyle(elt);
        let res = 0
        if (includeElt) { res += elt.offsetHeight }
        if (marginTop) { res += parseFloat(styles['marginTop']) }
        if (paddingTop) { res += parseFloat(styles['paddingTop']) }
        if (paddingBottom) { res += parseFloat(styles['paddingBottom']) }
        if (marginBottom) { res += parseFloat(styles['marginBottom']) }

        return Math.ceil(res)
    }
}

//  Класс для управления повторяющимися уведомления
class InfLogger {

    static previous_msgs = []
    static repeat_delay_ms = 5000

    // Совпадает ли уведомление с предыдущим
    static isRepeatingMessage(msg, msg_type) {
        InfLogger.previous_msgs = InfLogger.previous_msgs.filter(elt => (Date.now() - elt.dttm) < InfLogger.repeat_delay_ms)
        let is_repeat = InfLogger.previous_msgs.find(elt => elt.msg == msg && elt.msg_type == msg_type)
        InfLogger.previous_msgs.push({ 'msg': msg, 'msg_type': msg_type, dttm: Date.now() })
        return is_repeat
    }

    // Добавление информационного сообщения 
    static AddNotification(msg) {
        InfLogger.AddInfMsg(msg.message);

        if (msg.status == 'danger') {
            msg.message = "Возникла ошибка, пожалуйста изучите детали во вкладке [Сообщения] и свяжитесь с администратором кодогенератора если потребуется помощь в устранении причины"
        }

        if (!InfLogger.isRepeatingMessage(msg.message, 'notification')) {
            UIkit.notification(msg)
        }
    }

    // Добавление информационного сообщения 
    static AddInfMsg(msg) {
        if (!InfLogger.isRepeatingMessage(msg, 'inf')) {
            document.getElementById('elid_txt_inf').value += Date().toString().slice(0, 25) + ': ' + msg.replace(/<span.*<\/span>/, '') + '\n'
        }
    }

}

// Класс для подключения к серверной части кодогенератора 
class ApiConnect {

    //Создание объекта связи интерфейса и API сервера
    constructor(api_url, elid_user, elid_token, elid_branch, elid_txt_commit, lcl_event_hub) {

        // Ссылка для подключения к APi редактора
        this.api_url = api_url

        // Элемент ввода логина пользователя Bitbucket
        this.el_user = document.getElementById(elid_user)
        this.el_user.value = localStorage['meta_interface_user'] || ''

        // Элемент ввода маркера доступа пользователя Bitbucket
        this.el_token = document.getElementById(elid_token)
        this.el_token.value = localStorage['meta_interface_token'] || ''

        // Элемент ввода ветви (релиза, тега) Bitbucket
        this.el_branch = document.getElementById(elid_branch)
        //this.el_branch.value = localStorage['meta_interface_branch'] || ''

        // Элемент ввода сообщения, передаваемого в момент фиксации изменений в системе контроля версий
        this.elid_txt_commit = document.getElementById(elid_txt_commit)

        // Объект обмена соощениями о состоянии элементов управления, между элементами текущей вкладки 
        this.lcl_event_hub = lcl_event_hub


        // Проверка связи с API
        fetch(`${this.api_url}/check_status`, { mode: 'cors', method: "post" }).catch(e => {
            UIkit.modal.alert('Соединение с сервером кодогенератора блокируется браузером. Пожалуйста проверьте что браузер запущен с параметром --ignore-certificate-errors, согласно руководству пользователя')
        })

    }


    // Выполнения нескольких попыток получения данных
    static fetchWithRetry = (base_url, path, opt) => {
/*
        
        let url_check = [base_url, 'check_token'].join('/')

        // Проверка маркера доступа
        fetch(url_check, {
            mode: 'cors',
            method: "POST",
            body: JSON.stringify({ 'token': JSON.parse(opt.body).token })
        }).then(res => {
            res.json().then(json_data => {
                if (!json_data.token_valid) {
                    InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Токен недействителен` })
                }
            })
        },
            err => {
                InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Не удаётся проверить токен` })
            }
        );
*/
        // Обращение к API, несколько попыток подключения
        return new Promise((resolve, reject) => {
            let attempts = 1;
            const fetch_retry = (url, opt, n) => {
                return fetch(url, opt).then(res => {
                    // Статусы успешного завершения операции
                    // 200 ок
                    // 201 ресурс создан
                    if ([200, 201].includes(res.status)) {
                        resolve(res.json())
                    }
                    else {
                        // Неизвестная ошибка
                        res.json().then(json_data => {
                            let inf_msg = JSON.stringify(json_data)

                            // Демонстрация уведомления
                            InfLogger.AddNotification({ status: 'danger', timeout: 15000, pos: 'top-right', message: `<span uk-icon=\'icon: ban\'></span> Ошибка операции, получено сообщение с описанием причины` })

                            // Добавлеине сообщения в общее текстовое поле
                            InfLogger.AddInfMsg(inf_msg)

                            reject(json_data)
                        })

                    }
                }

                ).catch(function (error) {
                    if (n === 1) { reject(error) }
                    else {
                        console.error(error);
                        InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Ошибка подключения, будет выполнено ещё ${n - 1} попыт${n - 1 == 1 ? 'ка' : 'ки'} ...` })
                        setTimeout(() => {
                            attempts++
                            fetch_retry(url, opt, n - 1);
                        }, attempts * 3000)
                    }
                });
            }
            return fetch_retry([base_url, path].join('/'), opt, 3);
        });
    }


    // Зпрос данных таблицы
    async getDataForTableOrView(table_name, table_sort) {

        let data = {
            'user': this.el_user.value,
            'token': this.el_token.value,
            'branch': this.el_branch.value,
            'table_name': table_name,
            'table_sort': table_sort
        }
        // Обращение к API
        return ApiConnect.fetchWithRetry(this.api_url, 'get_data', {
            mode: 'cors',
            method: "POST",
            body: JSON.stringify(data)
        })
    }
    // Удаление строки таблицы с требуемым ID
    async deleteTableRow(table_name, row_id) {

        let data = {
            'user': this.el_user.value,
            'token': this.el_token.value,
            'branch': this.el_branch.value,
            'table_name': table_name,
            'row_id': row_id
        }
        // Обращение к API
        return ApiConnect.fetchWithRetry(this.api_url, 'delete_row', {
            mode: 'cors',
            method: "POST",
            body: JSON.stringify(data)
        })
    }
    // Добавление строки таблицы
    async addTableRow(table_name) {

        let data = {
            'user': this.el_user.value,
            'token': this.el_token.value,
            'branch': this.el_branch.value,
            'table_name': table_name
        }
        // Обращение к API
        return ApiConnect.fetchWithRetry(this.api_url, 'add_row', {
            mode: 'cors',
            method: "POST",
            body: JSON.stringify(data)
        })
    }
    // Сохранение данных таблицы
    async saveTableData(table_name, table_data) {

        let data = {
            'user': this.el_user.value,
            'token': this.el_token.value,
            'branch': this.el_branch.value,
            'table_name': table_name,
            'table_data': table_data
        }
        // Информирование о начале операции
        InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: twitter\'></span> Сохранение данных начато' })

        // Обращение к API
        return ApiConnect.fetchWithRetry(this.api_url, 'save_data', {
            mode: 'cors',
            method: "POST",
            body: JSON.stringify(data)
        }).then(res => {

            // Информирование о завершении операции
            InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: twitter\'></span> Сохранение данных завершено успешно' })

            return res
        })
    }

    // Обновление содержимого выпадающего списка с перечнем веток
    async refreshBranchList() {

        let data = {
            'user': this.el_user.value,
            'token': this.el_token.value,
        }
        // Локальная копия переменной
        let lcl_event_hub = this.lcl_event_hub
        let lcl_this = this

        // Базовая проверка правильности введённых папаметров
        if (data.user.length > 0 && data.token.length > 10) {

            // Информирование о начале операции
            InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: twitter\'></span> Обновление списка доступных веток начато' })
            // Обращение к API
            ApiConnect.fetchWithRetry(this.api_url, 'get_branch_list', {
                mode: 'cors',
                method: "POST",
                body: JSON.stringify(data)
            }).then(res => {

                let opt_list = lcl_this.el_branch.options
                //Сброс всех значений кроме значения по умолчанию
                opt_list.length = 1

                for (let val of res) {
                    if (! /^meta_.*/.test(val)) {
                        opt_list.add(new Option(val, val))
                    }
                }

                // Информирование о завершения выполнения операции
                InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: git-branch\'></span> Перечень доступных веток метаданных обновлён' })
            }).catch(res => {
                console.error(res);
                InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Ошибка при выполнении операции обновления перечня доступных веток метаданных. Проверьте пожалуйста правильность значений "Логин пользователя", "Токен доступа" на вкладке "Подключение"` })


            });

        }
        else {
            // Информирование о сбое выполнения операции
            UIkit.modal.alert('Значения параметров подключения к репозиторию метаданных введены не полностью либо неверно. Проверьте пожалуйста, правильность значений "логин пользователя", "Токен доступа" на вкладке "Подключения". Затем повторите операцию.')
        }
    }


    // Загрузка версии метаданных из репозитория Bitbucket
    async loadBranch() {

        let data = {
            'user': this.el_user.value,
            'token': this.el_token.value,
            'branch': this.el_branch.value
        }

        // Базовая проверка правильности введённых папаметров
        if (data.user.length > 0 && data.token.length > 10 && data.branch.length > 0) {


            // Запрос подтвержения дейсвия по загрузке версии метаданных в случае если уже загружена другая версия
            if (window.lcl_is_branch_loaded) {
                await UIkit.modal.confirm("Загрузить другую версию метаданных? Если в текущей версии есть изменения, пожалуйста предварительно зафиксируйте. В противном случае они будут удалены.")
            }

            // Событие для очистки содержимого таблиц
            lcl_event_hub.invoke('finish_dump_branch')

            // Информирование о начале операции
            InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: twitter\'></span> Загрузка версии метаданных начата' })

            // Обращение к API
            ApiConnect.fetchWithRetry(this.api_url, 'load_branch', {
                mode: 'cors',
                method: "POST",
                body: JSON.stringify(data)
            }).then(res => {
                // Событие завершения загрузки метаданных
                this.lcl_event_hub.invoke('finish_load_branch')
                // Информирование о завершении операции
                InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: check\'></span> Загрузка версии метаданных выполнена успешно' })

            }).catch(res => {
                console.error(res);
                InfLogger.AddInfMsg(res)
                InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Ошибка при выполнении операции загрузки. Проверьте пожалуйста правильность значений "Логин пользователя", "Токен доступа", "Наименование ветви кода" на вкладке "Подключение", затем выполните повторную загрузку версии кода (lb)` })

            });
            // Сохранение в браузере введённых данных, позволяет использовать интерфейс в смежных вкладках
            localStorage['meta_interface_user'] = data.user
            localStorage['meta_interface_token'] = data.token
            localStorage['meta_interface_branch'] = data.branch



        }

        else {
            // Информирование о сбое выполнения операции
            UIkit.modal.alert('Значения параметров подключения к репозиторию метаданных введены не полностью, проверьте пожалуйста, что заполнены все три обязательные поля: "Логин пользователя", "Токен доступа", "Наименование ветви кода" на вкладке "Подключение". Затем повторите операцию.')
        }


    }
    // Выгрузка версии метаданных в репозиторий Bitbucket
    async dumpBranch() {

        let data = {
            'user': this.el_user.value,
            'token': this.el_token.value,
            'branch': this.el_branch.value,
            'commit_message': this.elid_txt_commit.value
        }
        // Локальная копия переменной
        let lcl_event_hub = this.lcl_event_hub

        // Базовая проверка правильности введённых папаметров
        if (data.user.length > 0 && data.token.length > 10 && data.branch.length > 0 && data.commit_message.length > 0) {

            // Информирование о начале операции
            InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: twitter\'></span> Фиксация изменений метаданных начата' })
            // Обращение к API
            ApiConnect.fetchWithRetry(this.api_url, 'dump_branch', {
                mode: 'cors',
                method: "POST",
                body: JSON.stringify(data)
            }).then(res => {
                // Событие завершения выгрузки метаданных
                lcl_event_hub.invoke('finish_dump_branch')
                // Очистка сообщения commit
                this.elid_txt_commit.value = ''

                // Информирование о завершения выполнения операции
                InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: check\'></span> Фиксация изменений метаданных завершена успешно' })
            }).catch(res => {
                console.error(res);
                InfLogger.AddInfMsg(res)
                InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Ошибка при выполнении операции выгрузки. Проверьте пожалуйста, что данные были предварительно загружены и внесено минимум одно изменение. Затем проверьте правильность значений "Логин пользователя", "Токен доступа", "Наименование ветви кода" на вкладке "Подключение" и "Сообщение commit" на вкладке "Операции"` })

            });

        }
        else {
            // Информирование о сбое выполнения операции
            UIkit.modal.alert('Значения параметров подключения к репозиторию метаданных введены не полностью, проверьте пожалуйста, что заполнены все четыре обязательные поля: "Логин пользователя", "Токен доступа", "Наименование ветви кода" на вкладке "Подключение" и "Сообщение commit" на вкладке "Операции". Затем повторите операцию.')
        }
    }

    // Публикация релиза на основе версии метаданных в репозиторий Bitbucket проекта CI/CD
    async processBranch() {

        let data = {
            'user': this.el_user.value,
            'token': this.el_token.value,
            'branch': this.el_branch.value,
            'commit_message': this.elid_txt_commit.value
        }
        // Локальная копия переменной
        let lcl_event_hub = this.lcl_event_hub
        let lcl_branch_name = this.el_branch.value

        // Базовая проверка правильности введённых папаметров
        if (data.user.length > 0 && data.token.length > 10 && data.branch.length > 0 && data.commit_message.length > 0) {

            // Информирование о начале операции
            InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: twitter\'></span> Генерация артефактов согласно установленным значениям CI/CD фильтра начата' })
            // Обращение к API
            ApiConnect.fetchWithRetry(this.api_url, 'generate_release', {
                keepalive: true,
                mode: 'cors',
                method: "POST",
                body: JSON.stringify(data)
            }).then(async res => {

                // Очистка сообщения commit
                this.elid_txt_commit.value = ''

                console.log(res, 'Результат генерации релиза')

                if (res.includes('[nochanged]')) {
                    // Информирование о завершения выполнения операции
                    InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: check\'></span> Генерация артефактов успешно завершена. Отличий от существующих веток в репозитории "Артефакты КХД" системы Bitbucket не выявлено.' })

                } else {
                    // Информирование о завершения выполнения операции
                    InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: check\'></span> Генерация артефактов успешно завершена. Результаты переданы в ветку репозитория "Артефакты КХД" системы Bitbucket.' })

                }

            }).catch(res => {
                console.error(res);
                InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Ошибка при выполнении операции публикации релиза. Проверьте пожалуйста отсутствие ошибок на вкладке "META Контроль". Затем проверьте правильность значений "логин пользователя", "токен доступа", "наименование ветви кода", "сообщение commit". Если выполняется генерация релизной ветки, проверьте что целевая ветка релиза c наименованием "${lcl_branch_name}" предварительно создана в проекте "Артефакты КХД"` })

            });

        }
        else {
            // Информирование о сбое выполнения операции
            UIkit.modal.alert('Значения параметров подключения к репозиторию метаданных введены не полностью, проверьте пожалуйста, что заполнены все четыре обязательные поля: "Логин пользователя", "Токен доступа", "Наименование ветви кода" на вкладке "Подключение" и "Сообщение commit" на вкладке "Операции". Затем повторите операцию.')
        }
    }
}

// Класс для управления палитрами оформления интерфейса
class ThemeSelector {

    constructor() {

        // Варианты оформления элементов интерфейса
        this.theme_params = {
            background_color_tab: ['rgb(56 56 55)', '#303030', '#404040', '#303030', '#303030',],
            table_theme: ['ag-theme-alpine-dark', 'ag-theme-alpine', 'ag-theme-material', 'ag-theme-alpine', 'ag-theme-balham'],
            uikit_theme: [
                'linear-gradient(125deg, rgb(15 18 18 / 82%), rgb(44 44 44))',
                'linear-gradient(359deg, #2b9e4e, rgb(18 68 45 / 71%))',
                'linear-gradient(318deg, rgb(169 169 169 / 82%), rgba(62, 7, 171, 0.71))',
                'linear-gradient(25deg, rgb(0 0 0 / 97%), rgb(126 195 197 / 71%))',
                'linear-gradient(25deg, rgb(7 143 117 / 82%), rgba(181, 52, 153, 0.71))'],
        }

        // Праоверка корректности текущего индекса 
        this.theme_index = localStorage['meta_interface_theme_index']
        if (!(this.theme_index >= 0 && this.theme_index < this.theme_params.table_theme.length)) {
            localStorage['meta_interface_theme_index'] = 0
            this.theme_index = 0
        }

    }
    // Переключение текущей темы оформления
    switchTheme(e) {
        let is_init = e?.detail?.is_init

        // Переключение темы не требуется, достаточно восстановить последнюю использованную
        if (!is_init) {
            this.theme_index = ++this.theme_index % this.theme_params.table_theme.length
            localStorage['meta_interface_theme_index'] = this.theme_index
        }

        // Страница браузера в целом
        document.getElementById('browser_tab').style.backgroundColor = this.theme_params.background_color_tab[this.theme_index];

        // Область интерфейса редактора    
        document.getElementById('mt_editor').style.background = this.theme_params.uikit_theme[this.theme_index];

        // Темы оформления интерактивных таблиц
        document.querySelectorAll('.aggrid').forEach(e => {

            e.classList.remove(... this.theme_params.table_theme);
            e.classList.add(this.theme_params.table_theme[this.theme_index]);
        })

    }

}

// Класс перечня таблиц для редактирования
class AgGridTblList {

    constructor(elid_tbl_list, lcl_api_connect, lcl_event_hub) {
        this.elid_tbl_list = elid_tbl_list
        this.lcl_api_connect = lcl_api_connect
        this.lcl_event_hub = lcl_event_hub
        this.ag_grid = null

        this.list_reorder_mark = '⤴'

        // Обработка события завершения загрузки ветви кода
        this.lcl_event_hub.register('finish_load_branch', this.refreshView.bind(this))
        // Обработка события завершения выгрузки ветви кода
        this.lcl_event_hub.register('finish_dump_branch', this.clearView.bind(this))
        // Обработка события о возможных изменения в перечне таблиц
        this.lcl_event_hub.register('refresh_table_list', this.refreshView.bind(this))

    }
    // Обновление данных
    refreshView(e) {

        // Информирование о начале операции
        InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: grid\'></span> Выполняется подготовка интерфейса' })

        // Базовое описание колонок
        let ag_col_def = [{
            "headerName": "Перечень объектов",
            "field": "short_comment",
            "hide": false,
            "editable": false,
            "sortable": true,
            "filter": 'agTextColumnFilter',
            "floatingFilter": true,
            "pinned": false,
            "headerTooltip": "Перечень доступных объектов метаданных",
            "cellEditor": "agTextCellEditor",
            rowDrag: true,
            "maxWidth": 500,
            "width": 196
        }]

        // Типовые операции по расширению функциональности
        let columns_defs = ag_col_def.map((element, i) => {

            // Дополнение общими полезными свойствами
            let column_def = Object.assign({
                resizable: true,
                sortable: true,
                editable: true,
                rowDrag: false,
                getQuickFilterText: params => {
                    return params.colDef.hide ? "" : params.value
                }
            }, element);

            // Возврат итогового описания колонки
            return column_def
        });

        // Запрос полного перечня таблиц
        this.lcl_api_connect.getDataForTableOrView('v_editor_tables', '').then(tbl_row_data => {

            // Упорядочиване полученного списка согласно атрибуту "группа"
            let map_tbl_row_data = tbl_row_data.sort(function (a, b) {
                return a.mt_json.group.localeCompare(b.mt_json.group)
            }).map(elt => {
                // Отбор требуемых атрибутов
                return {
                    "mt_table_name": elt.mt_table_name,
                    "short_comment": elt.mt_json.short_comment,
                    "table_sort": elt.mt_json.table_sort,
                    'group': elt.mt_json.group,
                    "freq_sort": 0
                }
            })
            // Параметры интерактивной таблицы
            this.ap_opt = {
                columnDefs: columns_defs,
                rowData: map_tbl_row_data,
                rowSelection: 'single',
                headerHeight: 55,
                animateRows: true,
                rowDragManaged: true,
                localeText: ag_grid_locale_ru,
                defaultColDef: { flex: 1 },
                // Событие выбора строки таблицы
                onRowSelected: (event) => {

                    // Определение функции выбранного элемента
                    if (event.node.isSelected() && event.node.data.short_comment != '') {
                        //Переупорядочивание перечня имён согласно частоте обращения
                        if (event.node.isSelected() && event.node.data.short_comment.startsWith(this.list_reorder_mark)) {
                            let target_tag = event.node.data.short_comment.split(' ')[1]

                            let tag_names = []
                            let tbl_names = []

                            this.ap_opt.rowData.forEach(elt => {
                                if (elt.short_comment.startsWith(this.list_reorder_mark) ||
                                    elt.short_comment === '') { tag_names.push(elt) }
                                else {
                                    tbl_names.push(elt)
                                }

                            });

                            function arrayMin(arr) {
                                return arr.reduce(function (p, v) {
                                    return (p < v ? p : v);
                                });
                            }

                            function arrayMax(arr) {
                                return arr.reduce(function (p, v) {
                                    return (p > v ? p : v);
                                });
                            }

                            let max_freq = arrayMax(tbl_names.map(elt => elt.freq_sort)) + 1

                            tbl_names = tbl_names.map(elt => {
                                if (elt.short_comment.startsWith(target_tag)) { elt.freq_sort = max_freq }
                                return elt
                            })

                            // Упорядочивание сначала по частоте использования, затем по группе
                            tbl_names = tbl_names.sort(function (a, b) {
                                return b.freq_sort - a.freq_sort ||
                                    a.group.localeCompare(b.group)

                            })

                            let reordered_row_data = tag_names.concat(tbl_names)
                            this.ag_grid?.gridOptions?.api.setRowData(reordered_row_data)
                        }
                        else {
                            // Выбрана обычная таблицы, необходимо вывести её данные
                            this.lcl_event_hub.invoke('change_table_selection', event.node.data)
                        }
                    }
                }
            };

            // Дополнение перечня таблиц элементами для переупорядочивания согласно частоте обращения
            // -------------------------------------
            let tags = []

            Array.from(map_tbl_row_data).reverse().forEach(elt => {

                let tag = elt.short_comment.split(' ')[0]
                if (!tags.includes(tag)) { tags.unshift(tag) }
            })

            // Если групп слишком мало, нет необходимости создавать дополнительные элементы, они лишь затруднят навигацию
            if (tags.length < 3) {
                tags = []
            }
            else {
                // Разделитель группы переупорядочивания и основного списка
                tags.push('')
            }

            tags = tags.map(elt => {
                return {
                    "mt_table_name": 'tn',
                    "short_comment": elt === '' ? '' : this.list_reorder_mark + ' ' + elt,
                    "table_sort": 'ts',
                    "freq_sort": 0
                }

            })

            map_tbl_row_data = tags.concat(map_tbl_row_data)
            this.ap_opt.rowData = map_tbl_row_data
            // -------------------------------------


            // Пересоздание таблицы
            this.ag_grid?.gridOptions?.api?.destroy()
            this.mnt = document.getElementById(this.elid_tbl_list)
            this.ag_grid = new agGrid.Grid(this.mnt, this.ap_opt)

            //Автоматическая загрузка данных переданной таблицы либо первой в перечне            
            let trg_idx = 0
            if (e?.detail?.current_table_name) {
                // Поиск таблицы по имени
                let idx = map_tbl_row_data.findIndex(elt => elt.mt_table_name == e.detail.current_table_name)

                if (idx !== -1) {
                    trg_idx = idx
                }
            }

            if (trg_idx == 0) {
                //Поиск первой подходящей таблицы
                let idx = map_tbl_row_data.findIndex(elt => !(elt.short_comment == '' || elt.short_comment.startsWith(this.list_reorder_mark)))

                if (idx !== -1) {
                    trg_idx = idx
                }

            }



            //Выбор первого элемента списка
            this.ag_grid.gridOptions.api.forEachNode((rowNode, index) => {
                index == trg_idx && rowNode.setSelected(true)
            });

        }).catch(res => {
            console.error(res);
            InfLogger.AddInfMsg(res)
            InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Ошибка при выполнении операции. Проверьте пожалуйста правильность значений "Логин пользователя", "Токен доступа", "Наименование ветви кода" на вкладке "Подключения", а также что версия данных была загружена посредством "Загрузить" на вкладке "Операции"` })

        });
    }
    // Очистка содержимого таблицы
    clearView() {
        this.ag_grid?.gridOptions?.api.setRowData([])
    }
}

// Класс для демонстрации содержимого таблицы
class AgGridTblData {

    constructor(elid_tbl_data, elid_file_content, lcl_api_connect, lcl_event_hub) {
        this.elid_tbl_data = elid_tbl_data
        this.elid_file_content = elid_file_content

        this.lcl_api_connect = lcl_api_connect
        this.lcl_event_hub = lcl_event_hub

        this.ag_grid = null
        this.table_name = null

        // Обработка изменения фильтра
        this.lcl_event_hub.register('change_filter_table_data', (e) => {
            this.ag_grid.gridOptions.api.setQuickFilter(e.detail.value);
            // Автоматический подбор ширины колонок после применения фильтра
            this.autoSizeGrid();
        })

        let lcl_this = this
        lcl_this.is_edit_in_progress = false;

        // Загрузка и выгрузка версий метаданных
        this.lcl_event_hub.register('change_table_selection', this.refreshView.bind(this));
        this.lcl_event_hub.register('finish_dump_branch', this.clearView.bind(this))
        // Инструменты редактирования
        this.lcl_event_hub.register('click_save_table_data', this.saveTableData.bind(this))
        this.lcl_event_hub.register('click_add_table_row', this.addTableRow.bind(this))
        this.lcl_event_hub.register('click_duplicate_table_row', this.duplicateTableRow.bind(this))
        this.lcl_event_hub.register('click_delete_table_row', this.deleteTableRow.bind(this))

        this.lcl_event_hub.register('show_save_table_data', () => lcl_this.is_edit_in_progress = true)
        this.lcl_event_hub.register('hide_save_table_data', () => lcl_this.is_edit_in_progress = false)


        //  Обработака события выгрузки файла
        this.lcl_event_hub.register('click_download_file', this.downlowdFile.bind(this))
        // Обработака события загрузки файла
        document.getElementById(this.elid_file_content).addEventListener('change', this.uploadFile.bind(this), false);
    }

    // Получение файла
    downlowdFile() {

        // Информирование о начале операции
        InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: grid\'></span> Выгрузка текущей таблицы в виде XLSX файла начата' })

        try {
            // Формирование набора данных для передачи                    
            let table_name = this.table_name
            let table_data = []
            this.ag_grid.gridOptions.api.forEachNode(node => table_data.push(node.data));

            let table_header = this.ag_grid.gridOptions.api.getColumnDefs().map(elt => elt.field)
            let column_headers = this.ag_grid.gridOptions.api.getColumnDefs().map(elt => elt.headerName)

            // Формирование даты
            let d = new Date();
            let dttm = d.getFullYear() + "" + (d.getMonth() + 1).toString().padStart(2, '0') + d.getDate().toString().padStart(2, '0') +
                '_' + d.getHours().toString().padStart(2, '0') + "_" + d.getMinutes().toString().padStart(2, '0');

            // Формирование файла
            // Лист с данными
            let ws = XLSX.utils.json_to_sheet(table_data, { header: table_header });
            let wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, table_name);

            // Лист с заголовками колонок
            ws = XLSX.utils.json_to_sheet([], { header: column_headers });
            XLSX.utils.book_append_sheet(wb, ws, 'column_headers');
            // Выгрузка файла
            XLSX.writeFile(wb, `${table_name}_${dttm}.xlsx`);

            // Информирование о завершении операции
            InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: check\'></span> Выгрузка текущей таблицы в виде XLSX файла завершена' })

        }
        catch (e) {
            console.error(e);
            InfLogger.AddInfMsg(e.toString())
            InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: close\'></span> Ошибка при выполнении операции. Пожалуйста проверьте сообщения либо уведомления браузера полученные при попытке выгрузки файла` })
        }
    }

    // Загрузка файла
    uploadFile(e) {

        // Информирование о начале операции
        InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: grid\'></span> Загрузка текущей таблицы из XLSX файла начата' })

        try {
            let this_cl = this

            let elt_input = e.target
            let files = elt_input.files, f = files[0];
            let reader = new FileReader();
            reader.onload = function (e) {
                try {
                    // Получение данных с листа с данными
                    let wb = XLSX.read(e.target.result);

                    let ws = wb.Sheets[this_cl.table_name]

                    // Данные не найдены в загружаемом документе
                    if (!ws) { throw 'Неверная структура файла' }


                    let ws_json = XLSX.utils.sheet_to_json(ws, { defval: null });

                    let id_unique = [... new Set(ws_json.map(e => e.id))]
                    let id_null = id_unique.filter(e => e == null)

                    if (id_null.length != 0) { throw 'Неверное содержимое файла, есть null значения в колонке ID. Значения ID в рамках листа Excel должны быть уникальными положительными числами.' }
                    if (id_unique.length != ws_json.length || id_null.length != 0) { throw 'Неверное содержимое файла, есть дубликаты в колонке ID. Значения ID в рамках листа Excel должны быть уникальными положительными числами' }


                    // Запись загруженных значений
                    this_cl.ag_grid.gridOptions.api.setRowData(ws_json)

                    //Информирование о наличии изменений, требующих сохранения
                    this_cl.lcl_event_hub.invoke('show_save_table_data')
                    // Информирование о завершении операции
                    InfLogger.AddNotification({ pos: 'top-right', message: '<span uk-icon=\'icon: check\'></span> Загрузка текущей таблицы из XLSX файла завершена' })

                }
                catch (e) {
                    console.error(e);
                    InfLogger.AddInfMsg(e.toString())
                    InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: close\'></span> Ошибка при выполнении операции. Пожалуйста проверьте соответствие формата выбранного для загрузки файла (должен быть лист с названием совпадающим с техническим названием загружаемого объекта метаданных) и структуры целевой таблицы (колонки в листе должны совпадать с названиями колонок объекта метаданных)` })

                }
                finally {
                    // Очистка загруженного файла                    
                    elt_input.value = '';
                }

            };
            reader.readAsArrayBuffer(f);

        }
        catch (e) {
            console.error(e);
            InfLogger.AddInfMsg(e.toString())
            InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: close\'></span> Ошибка при выполнении операции. Пожалуйста проверьте соответствие формата выбранного файла и структуры целевой таблицы` })

        }

    }

    // Удаление строки таблицы
    deleteTableRow() {

        let top_this = this
        let ag_api = this.ag_grid.gridOptions.api
        let selectedData = ag_api.getSelectedRows()

        let all_rows_count = 0
        ag_api.forEachNode(() => all_rows_count++)


        // Проверка выполнимости операции
        if (selectedData.length == 0) {
            // Информирование о сбое операции
            UIkit.modal.alert('Операция может быть выполнена только при условии предварительного выделения одной или нескольких строк таблицы');
            return;
        }

        //Пример sleep
        //await new Promise((res, rej)=> setTimeout(()=> res('x'),2000 ))

        // Запрос подтвержения дейсвия
        let msg;
        let is_full_delete;
        is_full_delete = selectedData.length == all_rows_count
        msg = is_full_delete ? "Вы выделили все строки. Вы уверены что необходимо удалить все данные на данной вкладке?" : "Удалить выделенные строки?"

        UIkit.modal.confirm(msg).then(async res => {

            if (is_full_delete) {
                await top_this.lcl_api_connect.deleteTableRow(top_this.table_name, -1)
            }
            else {
                for (let e of selectedData) {
                    await top_this.lcl_api_connect.deleteTableRow(top_this.table_name, e.id)
                }
            }
        }).then(res => {
            console.log('DeleteCompleted')
            // Удаление данных из интерфейса
            ag_api.applyTransaction({ remove: selectedData });

            // Изменения в данной таблице потенциально ведут к изменению перечня
            if (top_this.table_name == 'meta_tables') {
                this.lcl_event_hub.invoke('refresh_table_list', { 'current_table_name': top_this.table_name })
            }
        }).catch(res => {
            if (res) {
                console.info(res);
                InfLogger.AddInfMsg(res)
                InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Ошибка при выполнении операции. Пожалуйста проверьте содержимое вкладки "Сообщения"` })
            }
        });

    }


    // Дублирование строки таблицы
    duplicateTableRow() {

        let ag_api = this.ag_grid.gridOptions.api

        var selectedData = ag_api.getSelectedRows();
        // Проверка выполнимости операции
        if (!(selectedData.length == 1 && selectedData[0].id)) {
            // Информирование о сбое операции
            UIkit.modal.alert('Операция может быть выполнена только при условии выделения ровно одной строки таблицы');
            return;
        }

        // Создание новой строки
        this.lcl_api_connect.addTableRow(this.table_name).then(res => {
            // Дублирование данных
            let new_duplicated_row = Object.assign({}, selectedData[0])
            new_duplicated_row.id = res[0].id
            // Добавление строки в интерфейс
            var res = ag_api.applyTransaction({
                add: [new_duplicated_row],
                addIndex: (ag_api.getSelectedNodes().length > 0 ? ag_api.getSelectedNodes()[0].rowIndex + 1 : 0),
            });

            //Информирование о наличии изменений, требующих сохранения
            this.lcl_event_hub.invoke('show_save_table_data')
        }).catch(res => {
            console.error(res);
            InfLogger.AddInfMsg(res)
            InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Ошибка при выполнении операции. Пожалуйста проверьте результат проверок метаданных в таблице "Контроль META"` })

        });


    }
    // Создание стркои таблицы
    addTableRow() {
        this.lcl_api_connect.addTableRow(this.table_name).then(res => {
            let ag_api = this.ag_grid.gridOptions.api
            // Добавление строки в интерфейс
            var res = ag_api.applyTransaction({
                add: res,
                addIndex: (ag_api.getSelectedNodes().length > 0 ? ag_api.getSelectedNodes()[0].rowIndex + 1 : 0),
            });

            //Информирование о наличии изменений, требующих сохранения
            this.lcl_event_hub.invoke('show_save_table_data')
        }).catch(res => {
            console.error(res);
            InfLogger.AddInfMsg(res)
            InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Ошибка при выполнении операции. Пожалуйста проверьте результат проверок метаданных в таблице "Контроль META"` })

        });


    }
    // Сохраниение данных
    saveTableData() {

        let rowData = [];
        let table_name = this.table_name

        // Подготовка вспомогательного объекта для накопления данных
        let empty_obj = {}
        this.ag_grid.gridOptions.api.getColumnDefs().forEach(elt => empty_obj[elt.field] = null)

        // Формирование набора данных для передачи
        this.ag_grid.gridOptions.api.forEachNode(node => {

            let new_row = {}
            //Формирование однотипных объектов перечень свойств которых совпадает с перечнем колонок
            Object.keys(empty_obj).forEach(key => {
                new_row[key] = node.data[key] === undefined ? null : node.data[key];
            });

            //Накопление объектов для передачи в хранилище
            rowData.push(new_row)

        })

        this.lcl_api_connect.saveTableData(table_name, rowData).then(res => {

            // Изменения в данной таблице потенциально ведут к изменению перечня
            if (table_name == 'meta_tables') {
                this.lcl_event_hub.invoke('refresh_table_list', { 'current_table_name': table_name })
            }

            //Информирование о наличии изменений, требующих сохранения
            this.lcl_event_hub.invoke('hide_save_table_data')
        }).catch(res => {
            console.error(res);
            InfLogger.AddInfMsg(res)
            InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Ошибка при выполнении операции. Пожалуйста проверьте результат проверок метаданных в таблице "Контроль META"` })

        });

    }


    // Обнолвение данных в интерфейсе
    async refreshView(e) {

        if (this.ag_grid?.gridOptions?.api) {

            // Завершить операции редактирования
            if (this.ag_grid.gridOptions.api.getEditingCells().length > 0) {
                this.is_edit_in_progress = true
                this.ag_grid.gridOptions.api.stopEditing()
            }

            this.ag_grid.gridOptions.api.deselectAll()
        }

        if (this.is_edit_in_progress) {
            await UIkit.modal.confirm("Отменить изменения в данной вкладке?")
            this.is_edit_in_progress = false
            document.getElementById(`elid_save_table_data`).style.display = 'none'
        }

        this.table_name = e.detail.mt_table_name
        let table_name = this.table_name

        // Запрос данных о структуре таблицы и её содержимом

        let col_def = this.lcl_api_connect.getDataForTableOrView('v_editor_columns', '')
        let row_data = this.lcl_api_connect.getDataForTableOrView(table_name, e.detail.table_sort)
        let tbl_def = this.lcl_api_connect.getDataForTableOrView('v_editor_tables', '')

        Promise.all([col_def, row_data, tbl_def]).then((res_values) => {

            // Фильтрация с целью выделить только текущую таблицу
            let ag_col_def = res_values[0].filter(elt => elt.mc_table_name == table_name).map(elt => elt.mc_json)[0]
            let ag_tbl_def = res_values[2].filter(elt => elt.mt_table_name == table_name).map(elt => elt.mt_json)[0]

            // Обработка перечня колонок
            let columns_defs = ag_col_def.map((element, i) => {

                // Получение параметра с перечнем значений для выпадающего списка при редактировании, при его наличии
                let sel_list_values = element.sel_list_values ?? ''
                delete element.sel_list_values

                // Дополнение общими полезными свойствами
                let column_def = Object.assign({
                    //filter: 'agTextColumnFilter',
                    filter: true,
                    floatingFilter: true,
                    resizable: true,
                    sortable: true,
                    editable: true,
                    rowDrag: true,
                    minWidth: 50,
                    maxWidth: 450,
                    filterParams: { buttons: ['apply', 'reset'], },
                    getQuickFilterText: params => {
                        return params.colDef.hide ? "" : params.value
                    }
                }, element);


                //Оптимизация максимальной допустимой ширины колонки для таблиц с небольшим числом колонок
                if ( ag_col_def.length < 3) { column_def.maxWidth = column_def.maxWidth * 4 }
                if ( ag_col_def.length < 6) { column_def.maxWidth = column_def.maxWidth * 3 }

                // Колонки с знаком вопроса в конце отображать в виде галочек
                if (column_def.headerName.endsWith('?')) {

                    column_def['cellRenderer'] = params => {
                        let is_checked = ['ИСТИНА', 'ДА', '1', 'YES', 'TRUE', 'ON'].indexOf(params.value?.toString().toUpperCase()) != -1
                        // Возврат значения false необходим для предотвращения срабатывания стандартного обработчика значений checkbox
                        return "<input type='checkbox' " + (is_checked ? 'checked' : '') + " onclick='return false;' />";
                    }

                    // Разные варианты списков редактирования значений в зависимости от текущего значения
                    column_def['cellEditorSelector'] = params => {

                        return {
                            component: 'agSelectCellEditor',
                            params: params.value ? { values: ['Нет', 'Да'] } : { values: ['Да', 'Нет'] }
                        }

                    }

                    column_def['valueSetter'] = params => {
                        params.data[column_def['field']] = params.newValue?.toString().toUpperCase() === 'ДА' ? 'true' : 'false';
                        //console.log(params.data)
                        return true;
                    }
                }

                // Колонки с назначенным списком возможных значений отображать с выпадающим списком
                if (sel_list_values != '') {

                    let new_data_struct = []
                    //Формирование однотипных объектов перечень свойств которых совпадает с перечнем колонок
                    sel_list_values.split(/\r?\n|,/).forEach(element=> {
                        new_data_struct.push({"value": sel_list_values.indexOf(element), "label": element})
                    });
                    column_def['cellEditor'] = AutocompleteSelectCellEditor
                    column_def['cellEditorParams'] = {
                        required: true,
                        placeholder: 'choose data type',
                        selectData: new_data_struct,
                        autocomplete: { showOnFocus: true, minLength: 0 }
                    }
                    column_def['valueFormatter'] = params => {
                        if (params.value) {
                        return params.value.label || params.value.value || params.value;
                        }
                        return "";
                }
                }


                // Если название колонки содержит только emojii и знак вопроса то знак вопроса не отображать
                if (column_def.headerName.endsWith('?') && column_def.headerName.length == 3) {
                    column_def.headerName = column_def.headerName.substring(0, 2)
                    column_def.headerClass = 'header-emojii'
                }

                // Перемещение возможно только за закреплённые колонки
                if (!column_def.pinned) { column_def.rowDrag = false }

                // Возврат итогового описания колонки
                return column_def
            });
            // Параметры интерактивной таблицы

            this.ap_opt = {
                columnDefs: columns_defs,
                rowData: Array.isArray(res_values[1]) ? res_values[1] : [],
                rowSelection: 'multiple',
                headerHeight: 55,
                animateRows: true,
                rowDragManaged: true,
                undoRedoCellEditing: true,
                undoRedoCellEditingLimit: 77,
                enableRangeSelection: true,
                copyHeadersToClipboard: true,
                // cacheQuickFilter: true,
                localeText: ag_grid_locale_ru,
                defaultColDef: { flex: 1 }
            };

            // Создание новой таблицы либо подмена данных в текущей
            if (this.ag_grid?.gridOptions?.api) {

                // Завершить операции редактирования
                this.ag_grid.gridOptions.api.stopEditing()
                this.ag_grid.gridOptions.api.deselectAll()

                this.ag_grid.gridOptions.api.setColumnDefs(this.ap_opt.columnDefs)
                this.ag_grid.gridOptions.api.setRowData(this.ap_opt.rowData)
            } else {

                this.mnt = document.getElementById(this.elid_tbl_data)
                this.ag_grid = new agGrid.Grid(this.mnt, this.ap_opt);
            }

            //Автоматический подбор оптимальной ширины колонок
            let lcl_this = this
            setTimeout(() => { lcl_this.autoSizeGrid() }, 0)


            this.ag_grid.gridOptions.onCellValueChanged = () => {
                //Информирование о наличии изменений, требующих сохранения
                ag_tbl_def.readonly == false && this.lcl_event_hub.invoke('show_save_table_data')
            }

            // Определение доступности элементов редактирования для данной таблицы
            let tbl_edit_prop = {
                "is_edit_add": ag_tbl_def.is_edit_add ?? true,
                "is_edit_copy": ag_tbl_def.is_edit_copy ?? true,
                "is_edit_delete": ag_tbl_def.is_edit_delete ?? true
            }
            ag_tbl_def.readonly == true && this.lcl_event_hub.invoke('hide_edit_table_data')
            ag_tbl_def.readonly == false && this.lcl_event_hub.invoke('show_edit_table_data', tbl_edit_prop)

            // Изменение заголовка страницы
            this.lcl_event_hub.invoke('change_tab_title', e.detail.short_comment)

        }).catch(res => {
            console.error(res);
            //Очистка поля в случае отсутствия данных
            this.clearView()
            InfLogger.AddInfMsg(res)
            InfLogger.AddNotification({ status: 'danger', timeout: 10000, pos: 'top-right', message: `<span uk-icon=\'icon: clock\'></span> Ошибка при выполнении операции. Пожалуйста проверьте результат проверок метаданных в таблице "Контроль META"` })

        });

    }

    // Автоматический подбор ширины клонок
    autoSizeGrid() {
        // Формирование перечня колонок
        var allColumnIds = [];
        this.ag_grid.gridOptions.columnApi.getAllColumns().forEach(function (column) {
            allColumnIds.push(column.colId);
        });

        // Автоматический подбор ширины столбцов согласно содержимому
        let skipHeader = false;
        this.ag_grid.gridOptions.columnApi.autoSizeColumns(allColumnIds, skipHeader);
        //console.log('Grid autosized')
    }

    // Очистка содержимого таблцы
    clearView() {
        this.ag_grid?.gridOptions?.api.setRowData([])
        // Сокрытие элемента сохранения
        this.lcl_event_hub.invoke('hide_save_table_data')
        // Изменение заголовка страницы
        this.lcl_event_hub.invoke('change_tab_title')
    }
}

// Класс для регистрации и обработки событий интерфейса
class EventHub {

    constructor(elid_event_hub) {
        this.elid_event_hub = document.getElementById(elid_event_hub)
    }
    // Регистрация события и его обработчика
    register(event, func) {
        this.elid_event_hub.addEventListener(event, func, false)
    }
    // Обработка события путём вызова всех зарегистрированных обработчиков
    invoke(event, data) {
        let evt = new CustomEvent(event, { detail: data });
        console.log(`Evt invoke ${Date.now() / 1000}`, evt)
        this.elid_event_hub.dispatchEvent(evt);
    }

}
