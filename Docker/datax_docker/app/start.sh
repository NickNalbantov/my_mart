#!/bin/bash

export META_EDITOR_BRANCH=$1
export META_PROCESSOR_BRANCH=$2

###############################
function indent_echo (){
    echo `date "+%d.%m %H:%M:%S"` ': - ' $1
}      
  

###############################
function datax_backup {

    indent_echo 'Start backup, please wait (20 sec ETA)'
    mkdir -p /app/backups
    mkdir -p /app/deployed_projects
    tar -zcf /app/backups/all_codegen_`date +%d%m_%H%M` /app/deployed_projects  /app/deploy_scripts 
    indent_echo 'List of backups'
    ls -laht /app/backups

    indent_echo  'Operation completed'
}


###############################
function datax_pull_codegen () {

    branch_editor=$1
    branch_processor=$2

    rm -rf /app/git_projects/{meta_editor,meta_processor}

    mkdir -p /app/git_projects/meta_editor
    mkdir -p /app/git_projects/meta_processor
    
    indent_echo "meta_editor branch - ${branch_editor}, meta_processor branch - ${branch_processor}"

    cd /app/git_projects/meta_editor 
    #git init
    git clone --depth 1 --single-branch -b ${branch_editor} https://${GIT_ADMIN_USER}:${GIT_ADMIN_TOKEN}@${GIT_REPO}

    cd /app/git_projects/meta_processor 
    #git init
    git clone --depth 1 --single-branch -b ${branch_processor} https://${GIT_ADMIN_USER}:${GIT_ADMIN_TOKEN}@${GIT_REPO}
    cd /app/git_projects/meta_processor

    indent_echo 'Operation completed'
}


###############################
function datax_deploy_codegen_web {

    indent_echo "Cleaning /app/deployed_projects/codegen_web folder"
    rm -rf /app/deployed_projects/codegen_web
    mkdir -p /app/deployed_projects/codegen_web

    indent_echo "Copy files for 'meta_editor Web UI' from local pulled git"
    cp -r /app/git_projects/meta_editor/metagen/web_scripts/meta_editor/* /app/deployed_projects/codegen_web

    indent_echo "Fix API link "
    sed -i "s/new ApiConnect('http:\/\/127.0.0.1:5100/new ApiConnect('https:\/\/${WEB_HOST}:${APACHE_LISTEN_SSL_PORT}/" /app/deployed_projects/codegen_web/meta_editor.html

    indent_echo "Fix version text"
    date_lbl=`date "+%Y.%m.%d %H:%M"`
    sed -i "s/Версия vXXXX.YYYY/Версия v${date_lbl}/" /app/deployed_projects/codegen_web/meta_editor.html

    indent_echo "Fix CVS name"    
    sed -i "s/NameOfCVS/${REPO_TYPE}/g" /app/deployed_projects/codegen_web/meta_editor.html
    sed -i "s/NameOfCVS/${REPO_TYPE}/g" /app/deployed_projects/codegen_web/js/utils.js

    indent_echo 'Operation completed'
}


###############################
function datax_deploy_codegen_api {

    indent_echo  "Cleaning target folder for meta_editor"
    rm -rf /app/deployed_projects/codegen_api/python_scripts
    indent_echo  "Copy base version of files"
    mkdir -p /app/deployed_projects/codegen_api/python_scripts
    cp -r /app/git_projects/meta_editor/metagen/python_scripts/meta_editor/* /app/deployed_projects/codegen_api/python_scripts
    indent_echo  "Replace config to local version"
    cp /app/deploy_scripts/codegen_api/meta_editor_config.py /app/deployed_projects/codegen_api/python_scripts/utils/config.py

    mkdir -p /app/deployed_projects/codegen_api/vtb_mappings

    indent_echo  'Operation completed'
}


###############################
function datax_restart_apache2 {

a2enmod proxy proxy_connect proxy_http socache_shmcb ssl headers env

mkdir -p /etc/apache2/ssl
cd /etc/apache2/ssl
openssl req -new -x509 -days 1461 -nodes -out cert.pem -keyout cert.key -subj "/C=RU/ST=SPb/L=SPb/O=Global Security/OU=IT Department/CN=test.ai-stand.local/CN=test"

a2ensite 000-default
a2ensite default-ssl

# Passing enviroment variables to Apache2 variables

grep -xqF "# Custom variables" /etc/apache2/envvars
is_config_not_done=$?

if [[ "${is_config_not_done}" == "1" ]]; then

    echo "# Custom variables start" >> /etc/apache2/envvars
    echo "export APACHE_STATUSURL=http://${WEB_HOST}:${APACHE_LISTEN_PORT}/server-status" >> /etc/apache2/envvars
    echo "export WEB_HOST=${WEB_HOST}" >> /etc/apache2/envvars
    echo "export EDITOR_PORT=${EDITOR_PORT}" >> /etc/apache2/envvars
    echo "export PROCESSOR_PORT=${PROCESSOR_PORT}" >> /etc/apache2/envvars
    echo "export APACHE_LISTEN_PORT=${APACHE_LISTEN_PORT}" >> /etc/apache2/envvars
    echo "export APACHE_LISTEN_SSL_PORT=${APACHE_LISTEN_SSL_PORT}" >> /etc/apache2/envvars
    echo "export APACHE_LOG_LEVEL=${APACHE_LOG_LEVEL}" >> /etc/apache2/envvars
    echo "export DOCKER_SUBNET=${DOCKER_SUBNET}" >> /etc/apache2/envvars

    if [[ "${APACHE_ALLOWED_SUBNET}" != 'all' ]]; then
        echo "export APACHE_ALLOWED_SUBNET='${APACHE_ALLOWED_SUBNET}'" >> /etc/apache2/envvars
    else
        echo "export APACHE_ALLOWED_SUBNET=''" >> /etc/apache2/envvars
    fi

fi

# remove duplicate lines
gawk -i inplace '!(NF && seen[$0]++)' /etc/apache2/envvars

indent_echo  'Start Apache service'
# do not use restart, because it acts weird about rechecking custom variables
apache2ctl stop
apache2ctl start

indent_echo  'Operation completed'
}


###############################
function datax_restart_codegen_api {

    indent_echo 'Go to project dir'
    cd /app/deployed_projects/codegen_api/python_scripts

    export FLASK_APP=meta_editor_api.py
    export FLASK_RUN_PORT=${EDITOR_PORT}

    echo "Web app API port ${FLASK_RUN_PORT}"

    indent_echo  'Terminate any existing runs'
    lsof -t -i :${EDITOR_PORT} | xargs -I '{}' kill -9 {}
    indent_echo  'Start codegen_api'
    nohup python -m flask run --host=0.0.0.0 &
        
    indent_echo  'Operation completed'
}


###############################
function datax_deploy_codegen_processor {

    indent_echo  "Cleaning target folder for meta_processor"
    rm -rf /app/deployed_projects/codegen_processor/python_scripts
    indent_echo  "Copy base version of files"
    mkdir -p /app/deployed_projects/codegen_processor/python_scripts
    cp -r /app/git_projects/meta_processor/metagen/python_scripts/meta_processor/* /app/deployed_projects/codegen_processor/python_scripts
    indent_echo  "Replace config to local version"
    cp /app/deploy_scripts/codegen_processor/meta_processor_config.py /app/deployed_projects/codegen_processor/python_scripts/utils/config.py

    indent_echo  'Operation completed'
}


###############################
function datax_restart_codegen_processor {

    indent_echo 'Go to project dir'
    cd /app/deployed_projects/codegen_processor/python_scripts

        
    export FLASK_APP=meta_processor_api.py
    export FLASK_RUN_PORT=${PROCESSOR_PORT}

    echo "Web app port ${FLASK_RUN_PORT}"

    indent_echo  'Terminate any existing runs'
    lsof -t -i :${PROCESSOR_PORT} | xargs -I '{}' kill -9 {}
    indent_echo  'Start codegen_processor'
    nohup python -m flask run --host=0.0.0.0 &
        
    indent_echo  'Operation completed'
}


###############################
function datax_edit_codegen_api_config {

    indent_echo 'Editing config'
    vi "/app/deploy_scripts/codegen_api/meta_editor_config.py"
    indent_echo 'Please redeploy and restart codegen_api in case of need'
}


###############################
function datax_edit_codegen_processor_config {

    indent_echo 'Editing config'
    vi "/app/deploy_scripts/codegen_processor/meta_processor_config.py"
    indent_echo 'Please redeploy and restart codegen_processor in case of need'
}


###############################
function datax_status () {

    indent_echo  '---------------------------------'
    indent_echo  'List of Flask processes:'
    indent_echo  '---------------------------------'
    ps -ef | grep flask | grep -v grep
    indent_echo  '---------------------------------'

    indent_echo  'Apache2 status:'
    apache2ctl fullstatus
    indent_echo  '---------------------------------'

    flask_stat=`ps -ef | grep flask | grep -v grep | wc -l`
    apache_stat=`grep -o "Server uptime" <<< $(apache2ctl status)`


    while [[ "$flask_stat" != '2' || "$apache_stat" != 'Server uptime' ]]; do

        if [[ "$flask_stat" != '2' ]]; then
            indent_echo  "Restarting Flask"
            lsof -t -i :${PROCESSOR_PORT} | xargs -I '{}' kill -9 {}
            lsof -t -i :${EDITOR_PORT} | xargs -I '{}' kill -9 {}
            sleep 10
            datax_restart_codegen_api
            datax_restart_codegen_processor
            flask_stat=`ps -ef | grep flask | grep -v grep | wc -l`
            indent_echo  '---------------------------------'
            indent_echo  'List of Flask processes:'
            indent_echo  '---------------------------------'
            ps -ef | grep flask | grep -v grep
            indent_echo  '---------------------------------'

        elif [[ "$apache_stat" != 'Server uptime' ]]; then
            indent_echo  "Restarting Apache2"
            sleep 10
            datax_restart_apache2
            apache_stat=`grep -o "Server uptime" <<< $(apache2ctl status)`
            indent_echo  'Apache2 status:'
            apache2ctl fullstatus
            indent_echo  '---------------------------------'

        fi

    done

    indent_echo  'DatAx status:  OK'
}


###############################
alias daxh="clear && datax_help"
alias daxs="clear && datax_status"


###############################
function datax_help {

echo """
================================================================ [GREET] ===
Greet you, DatAx Administrator
========================================================== [CURRENT ENV] ===
Current target_env                                   = [${ENV_NAME}]
Selected branch for browser interface and meta_editor = [${META_EDITOR_BRANCH}]
Selected branch for meta processor                    = [${META_PROCESSOR_BRANCH}]

===================================================== [LIST OF COMMANDS ]===
a.1) Show this message  $> datax_help OR $> daxh                          

b.1) For change environment use:         $> datax_set_env ${META_EDITOR_BRANCH} ${META_PROCESSOR_BRANCH} 

c.1) Backup all environment :            $> datax_backup                        
c.2) Current stat of all env:            $> datax_status OR $> daxs

d.1) Git pull codegen project branches:  $> datax_pull_codegen ${META_EDITOR_BRANCH} ${META_PROCESSOR_BRANCH}
                 
e.1) Edit codegen api config file:       $> datax_edit_codegen_api_config       
e.2) Edit codegen processor config file: $> datax_edit_codegen_processor_config 

d.1) Deploy codegen browser interface:   $> datax_deploy_codegen_web
d.2) Deploy codegen api:                 $> datax_deploy_codegen_api
d.3) Deploy codegen processor:           $> datax_deploy_codegen_processor

e.1) Restart codegen api:                $> datax_restart_codegen_api
e.2) Restart codegen processor:          $> datax_restart_codegen_processor

================================================ [SAMPLE OF COMMAND USE] ===
datax_pull_codegen ${META_EDITOR_BRANCH} ${META_PROCESSOR_BRANCH}
datax_deploy_codegen_web
datax_deploy_codegen_api
datax_deploy_codegen_processor
datax_restart_codegen_api
datax_restart_codegen_processor                                 

============================================================================
"""
}


###############################
#datax_backup

datax_pull_codegen ${META_EDITOR_BRANCH} ${META_PROCESSOR_BRANCH}

datax_deploy_codegen_web
datax_deploy_codegen_api
datax_deploy_codegen_processor

datax_restart_apache2
datax_restart_codegen_api
datax_restart_codegen_processor

datax_status

sleep infinity