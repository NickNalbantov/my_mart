# Центральная страница с описанием проекта
https://gl-yc.ax-team.ru/analytical_stand/ai-stand/-/wikis/%D0%9F%D1%80%D0%BE%D0%B4%D1%83%D0%BA%D1%82%D1%8B%20%D0%A1%D0%BA%D0%BE%D0%BB%D0%BA%D0%BE%D0%B2%D0%BE/DatAx

# issue


# code
https://gl-yc.ax-team.ru/analytical_stand/datax/metagen/-/tree/datax_docker
https://gl-yc.ax-team.ru/analytical_stand/datax/metagen/-/tree/meta_editor
https://gl-yc.ax-team.ru/analytical_stand/datax/metagen/-/tree/meta_processor

# docs
https://www.postgresql.org/docs/15/index.html
https://postgrest.org/en/v11.1/
https://httpd.apache.org/docs/2.4/
https://flask.palletsprojects.com/en/2.3.x/
https://www.ag-grid.com/documentation/