# Запуск образа через Makefile
make run

# Остановка образа через Makefile
make stop

# Перезапуск образа через Makefile
make restart

# Запуск образа через Docker
docker compose up

# Остановка образа через Makefile
docker compose down

# Перезапуск образа через Makefile
docker compose down
docker compose up

# Перезапуск сервиса внутри контейнера
pkill -f "bash /app/start.sh"
/bin/bash /app/start.sh $META_EDITOR_BRANCH $META_PROCESSOR_BRANCH >> /proc/1/fd/1        