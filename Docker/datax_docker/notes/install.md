# ОС
Debian 11.7 Bullseye

# APT-пакеты
<ol>
  <li>apache2 2.4.56-1~deb11u2</li>
  <li>build-essential 12.9</li>
  <li>curl 7.74.0-1.3+deb11u7</li>
  <li>expat 2.2.10-2+deb11u5</li>
  <li>gawk 1:5.1.0-1</li>
  <li>git 1:2.30.2-1+deb11u2</li>
  <li>libbz2-dev 1.0.8-4</li>
  <li>libcurl4-openssl-dev 7.74.0-1.3+deb11u7</li>
  <li>libffi-dev 3.3-6</li>
  <li>libpq5 13.11-0+deb11u1</li>
  <li>libssl-dev 1.1.1n-0+deb11u5</li>
  <li>libtool 2.4.6-15</li>
  <li>lsof 4.93.2+dfsg-1.1</li>
  <li>lynx 2.9.0dev.6-3~deb11u1</li>
  <li>openssh-client 1:8.4p1-5+deb11u1</li>
  <li>screen 4.8.0-6</li>
  <li>wget 1.21-1+deb11u1</li>
</ol>

# Основные элементы контейнеров
<ol>
  <li>Python 3.11.4</li>
  <li>PostgreSQL 15.3</li>
  <li>PostgREST 11.1.0</li>
</ol>

# PyPI-пакеты
<ol>
  <li>pip 23.2</li>
  <li>certifi 2023.5.7</li>
  <li>charset-normalizer 3.1.0</li>
  <li>click 8.1.3</li>
  <li>colorama 0.4.6</li>
  <li>cryptography 41.0.1</li>
  <li>et-xmlfile 1.1.0</li>
  <li>Flask 2.3.2</li>
  <li>Flask-Cors 3.0.10</li>
  <li>idna 3.4</li>
  <li>importlib-resources 5.12.0</li>
  <li>importlib-metadata 6.6.0</li>
  <li>itsdangerous 2.1.2</li>
  <li>Jinja2 3.1.2</li>
  <li>MarkupSafe 2.1.2</li>
  <li>openpyxl 3.1.2</li>
  <li>oracledb 1.3.2</li>
  <li>pandas 2.0.3</li>
  <li>psycopg 3.1.9</li>
  <li>psycopg[binary] 3.1.9</li>
  <li>psycopg-pool 3.1.7</li>
  <li>requests 2.31.0</li>
  <li>SQLAlchemy 2.0.18</li>
  <li>typing-extensions 4.6.2</li>
  <li>urllib3 2.0.2</li>
  <li>Werkzeug 2.3.4</li>
  <li>XlsxWriter 3.1.1</li>
  <li>zipp 3.15.0</li>
</ol>

# NPM-пакеты
<ol>
  <li>ag-charts-community.5.3.0</li>
  <li>ag-grid-autocomplete-editor 3.0.1</li>
  <li>ag-grid-community 27.3.0</li>
  <li>ag-grid-locale-ru</li>
  <li>core-js 2.5.7</li>
  <li>cose-base</li>
  <li>cytoscape</li>
  <li>cytoscape-cose-bilkent</li>
  <li>UIkit 3.7.6</li>
  <li>xlsx</li>
</ol>

# Параметры запуска контейнера
/.env - файл содержит все переменные контейнера с дефолтными значениями и описаниями