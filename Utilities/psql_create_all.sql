-- /bins
\ir ./bins/datetime_bins_set.sql
\ir ./bins/datetime_bins.sql
\ir ./bins/numeric_bins_set.sql
\ir ./bins/numeric_bins.sql


-- /randomizers
\ir ./randomizers/bigint.sql
\ir ./randomizers/boolean.sql
\ir ./randomizers/date.sql
\ir ./randomizers/float4.sql
\ir ./randomizers/float8.sql
\ir ./randomizers/int.sql
\ir ./randomizers/numeric.sql
\ir ./randomizers/smallint.sql
\ir ./randomizers/text.sql
\ir ./randomizers/time.sql
\ir ./randomizers/timestamp.sql
\ir ./randomizers/timestamptz.sql
\ir ./randomizers/timetz.sql
\ir ./randomizers/varbit.sql


-- /Others
\ir ./array_sort_distinct.sql
\ir ./bits_to_nums.sql
\ir ./calendar.sql
\ir ./jsonb_value_agg.sql
\ir ./system.sql