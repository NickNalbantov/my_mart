-- Based upon https://wiki.postgresql.org/wiki/Pg_depend_display

-- List of dependencies between objects in DB
       -- Example - getting all objects dependant on pg_catalog.pg_class:
              -- select *
              -- from pg_dependency
              -- where 'pg_catalog.pg_class'::regclass = any(dependency_oid_chain)
       -- Other options to get parents and childs - functions dependency_childs and dependency_parents below

drop view if exists pg_dependency cascade;

drop function if exists dependency_childs(in pg_catalog.oid, in pg_catalog.bool) cascade;
drop function if exists dependency_parents(in pg_catalog.oid, in pg_catalog.bool) cascade;


create or replace view pg_dependency
as
    with recursive
        dependency_pair as
        (
            select
                dep.objid,
                obj.type as object_type,
                coalesce(obj.schema, "substring"(obj.identity, '(\w+?)\.'::text), ''::text) as object_schema,
                obj.name as object_name,
                obj.identity as object_identity,
                dep.refobjid,
                refobj.type as refobj_type,
                coalesce(
                            case when refobj.type = 'schema'::text then refobj.identity
                            else refobj.schema end,
                            "substring"(refobj.identity, '(\w+?)\.'::text), ''::text
                        ) as refobj_schema,
                refobj.name as refobj_name,
                refobj.identity as refobj_identity,
                case dep.deptype
                    when 'n'::"char" then 'normal'::text
                    when 'a'::"char" then 'auto'::text
                    when 'i'::"char" then 'internal'::text
                    when 'e'::"char" then 'extension'::text
                    when 'p'::"char" then 'pinned'::text
                    when 'x'::char then 'auto extension'::text
                    when 'P'::char then 'partition primary'::text -- deprecated since PG15
                    when 'S'::char then 'partition secondary'::text
                    else null::text
                end as dependency_type
            from
                (
                    select
                        classid,
                        objid,
                        refclassid,
                        refobjid,
                        deptype
                    from pg_catalog.pg_depend

                    union all

                    select
                        classid,
                        objid,
                        refclassid,
                        refobjid,
                        deptype
                    from pg_catalog.pg_shdepend
                ) dep
            cross join lateral pg_catalog.pg_identify_object(dep.classid, dep.objid, 0) obj (type, schema, name, identity)
            cross join lateral pg_catalog.pg_identify_object(dep.refclassid, dep.refobjid, 0) refobj (type, schema, name, identity)

            union

            select
                dep.refobjid as objid,
                obj.type as object_type,
                coalesce(obj.schema, "substring"(obj.identity, '(\w+?)\.'::text), ''::text) as object_schema,
                obj.name as object_name,
                obj.identity as object_identity,
                dep.objid as refobjid,
                refobj.type as refobj_type,
                coalesce(
                        case when refobj.type = 'schema'::text then refobj.identity
                        else refobj.schema end,
                        "substring"(refobj.identity, '(\w+?)\.'::text), ''::text
                        ) as refobj_schema,
                refobj.name as refobj_name,
                refobj.identity as refobj_identity,
                case dep.deptype
                    when 'n'::"char" then 'normal'::text
                    when 'a'::"char" then 'auto'::text
                    when 'i'::"char" then 'internal'::text
                    when 'e'::"char" then 'extension'::text
                    when 'p'::"char" then 'pinned'::text
                    when 'x'::char then 'auto extension'::text
                    when 'P'::char then 'partition primary'::text -- deprecated since PG15
                    when 'S'::char then 'partition secondary'::text
                    else null::text
                end as dependency_type
            from pg_catalog.pg_depend dep
            join pg_catalog.pg_rewrite r on r.oid = dep.objid and dep.refobjid = r.ev_class
            cross join lateral pg_catalog.pg_identify_object(dep.classid, dep.objid, 0) refobj (type, schema, name, identity)
            cross join lateral pg_catalog.pg_identify_object(dep.refclassid, dep.refobjid, 0) obj (type, schema, name, identity)
        ),

        dependency_hierarchy as
        (
            select
                0 as level,
                root.refobjid as objid,
                root.refobj_type as obj_type,
                root.refobj_identity as obj_name,
                null::text as dependency_type,
                array[root.refobjid] as dependency_oid_chain,
                array[concat(upper(root.refobj_type), ': ', root.refobj_identity)] as dependency_obj_chain
            from dependency_pair root
            where not exists
                (
                    select
                    from dependency_pair branch
                    where branch.objid = root.refobjid
                )

            union

            select
                parent.level + 1 as level,
                child.objid as objid,
                child.object_type as obj_type,
                child.object_identity as obj_name,
                child.dependency_type,
                parent.dependency_oid_chain || child.objid,
                parent.dependency_obj_chain || concat(upper(child.object_type), ': ', child.object_identity)
            from dependency_pair child
            join dependency_hierarchy parent on parent.objid = child.refobjid
            where
                child.objid != all(parent.dependency_oid_chain)
        )

    select distinct
        h.level,
        h.objid,
        h.obj_type,
        h.obj_name,
        h.dependency_type,
        h.dependency_oid_chain,
        h.dependency_obj_chain,

        case
            when h.obj_type in ('table', 'toast table') then get_table_def(h.objid::oid)
            when h.obj_type in ('function', 'procedure') then get_function_def(h.objid::oid)
            when h.obj_type in ('view', 'materialized view') then get_view_def(h.objid::oid)
            when h.obj_type = 'index' then get_index_def(h.objid::oid)
            when h.obj_type = 'rule' then get_rule_def(h.objid::oid)
            when h.obj_type = 'trigger' then get_trigger_def(h.objid::oid)
            when h.obj_type = 'event trigger' then get_event_trigger_def(h.objid::oid)
            when h.obj_type in ('constraint', 'table constraint', 'domain constraint') then get_constraint_def(h.objid::oid, true)
            when h.obj_type = 'sequence' then get_sequence_def(h.objid::oid)
            when h.obj_type = 'schema' then get_schema_def(h.objid::oid)
            when h.obj_type = 'type' then get_type_def(h.objid::oid)
            when h.obj_type = 'composite type' then get_type_def((h.objid::bigint + 2)::oid) -- each composite type generates 3 objects: object in pg_class with relkind = 'c' (e.g. oid = 10000), corresponding array-type in pg_type (oid = 10001), and main composite_type in pg_type (oid = 10002)
            when h.obj_type = 'default value' then get_column_defaults_def(ad.adnum, ad.adrelid)
            when h.obj_type = 'extension' then get_extension_def(h.objid::oid)
            when h.obj_type = 'language' then get_language_def(h.objid::oid)
            when h.obj_type = 'cast' then get_cast_def(h.objid::oid)
            when h.obj_type = 'operator' then get_operator_def(h.objid::oid)
            when h.obj_type = 'access method' then get_access_method_def(h.objid::oid)
            when h.obj_type = 'operator family' then get_operator_family_def(h.objid::oid)
            when h.obj_type in ('operator of access method', 'function of access method') and opd.dep_check = 'operator family'
                then get_operator_family_def(opd.opd_oid::oid)
            when h.obj_type = 'operator class' then get_operator_class_def(h.objid::oid)
            when h.obj_type in ('operator of access method', 'function of access method') and opd.dep_check = 'operator class'
                then get_operator_class_def(opd.opd_oid::oid)
            when h.obj_type = 'text search dictionary' then get_ts_dictionary_def(h.objid::oid)
            when h.obj_type = 'text search template' then get_ts_template_def(h.objid::oid)
            when h.obj_type = 'text search parser' then get_ts_parser_def(h.objid::oid)
            when h.obj_type = 'text search configuration' then get_ts_config_def(h.objid::oid)
            when h.obj_type = 'foreign-data wrapper' then get_fdw_def(h.objid::oid)
            when h.obj_type = 'user mapping' then get_user_mapping_def(h.objid::oid)
            when h.obj_type = 'server' then get_server_def(h.objid::oid)
            when h.obj_type = 'foreign table' then get_foreign_table_def(h.objid::oid)
            when h.obj_type = 'aggregate' then get_aggregate_def(h.objid::oid)
            when h.obj_type = 'transform' then get_transform_def(h.objid::oid)
            when h.obj_type = 'tablespace' then get_tablespace_def(h.objid::oid)
            when h.obj_type = 'statistics object' then get_statistics_def(h.objid::oid)
            when h.obj_type = 'collation' then get_collation_def(h.objid::oid)
            when h.obj_type = 'conversion' then get_conversion_def(h.objid::oid)
            when h.obj_type = 'database' then get_database_def(h.objid::oid)
            when h.obj_type = 'role' then get_role_def(h.objid::oid)
            when h.obj_type = 'policy' then get_policy_def(h.objid::oid)
            when h.obj_type = 'publication' then get_publication_def(h.objid::oid)
            when h.obj_type = 'publication namespace' then get_publication_def(pn.pnpubid::oid)
            when h.obj_type = 'publication relation' then get_publication_def(pr.prpubid::oid)
            when h.obj_type = 'subscription' then get_subscription_def(h.objid::oid)
            when h.obj_type = 'default acl' then get_default_acl_def(h.objid::oid)
            when h.obj_type = 'parameter ACL' then get_parameter_acl_def(h.objid::oid)
            when h.obj_type = 'large object' then E'-- A command to create an empty large object with specified OID\n\nSELECT lo_create(' || h.objid::text || ');'
        else null::text end as ddl_create,

        case
            when h.obj_type in ('table', 'toast table') then get_table_drop(h.objid::oid)
            when h.obj_type in ('function', 'procedure') then get_function_drop(h.objid::oid)
            when h.obj_type in ('view', 'materialized view') then get_view_drop(h.objid::oid)
            when h.obj_type = 'index' then get_index_drop(h.objid::oid)
            when h.obj_type = 'rule' then get_rule_drop(h.objid::oid, true)
            when h.obj_type = 'trigger' then get_trigger_drop(h.objid::oid, true)
            when h.obj_type = 'event trigger' then get_event_trigger_drop(h.objid::oid)
            when h.obj_type in ('constraint', 'table constraint', 'domain constraint') then get_constraint_drop(h.objid::oid, true)
            when h.obj_type = 'sequence' then get_sequence_drop(h.objid::oid)
            when h.obj_type = 'schema' then get_schema_drop(h.objid::oid)
            when h.obj_type = 'type' then get_type_drop(h.objid::oid)
            when h.obj_type = 'composite type' then get_type_drop((h.objid::bigint + 2)::oid) -- each composite type generates 3 objects: object in pg_class with relkind = 'c' (e.g. oid = 10000), corresponding array-type in pg_type (oid = 10001), and main composite_type in pg_type (oid = 10002)
            when h.obj_type = 'default value' then get_column_defaults_drop(ad.adnum, ad.adrelid)
            when h.obj_type = 'extension' then get_extension_drop(h.objid::oid)
            when h.obj_type = 'language' then get_language_drop(h.objid::oid)
            when h.obj_type = 'cast' then get_cast_drop(h.objid::oid)
            when h.obj_type = 'operator' then get_operator_drop(h.objid::oid)
            when h.obj_type = 'access method' then get_access_method_drop(h.objid::oid)
            when h.obj_type = 'operator family' then get_operator_family_drop(h.objid::oid)
            when h.obj_type in ('operator of access method', 'function of access method') and opd.dep_check = 'operator family'
                then get_operator_family_drop(opd.opd_oid::oid)
            when h.obj_type = 'operator class' then get_operator_class_drop(h.objid::oid)
            when h.obj_type in ('operator of access method', 'function of access method') and opd.dep_check = 'operator class'
                then get_operator_class_drop(opd.opd_oid::oid)
            when h.obj_type = 'text search dictionary' then get_ts_dictionary_drop(h.objid::oid)
            when h.obj_type = 'text search template' then get_ts_template_drop(h.objid::oid)
            when h.obj_type = 'text search parser' then get_ts_parser_drop(h.objid::oid)
            when h.obj_type = 'text search configuration' then get_ts_config_drop(h.objid::oid)
            when h.obj_type = 'foreign-data wrapper' then get_fdw_drop(h.objid::oid)
            when h.obj_type = 'user mapping' then get_user_mapping_drop(h.objid::oid)
            when h.obj_type = 'server' then get_server_drop(h.objid::oid)
            when h.obj_type = 'foreign table' then get_foreign_table_drop(h.objid::oid)
            when h.obj_type = 'aggregate' then get_aggregate_drop(h.objid::oid)
            when h.obj_type = 'transform' then get_transform_drop(h.objid::oid)
            when h.obj_type = 'tablespace' then get_tablespace_drop(h.objid::oid)
            when h.obj_type = 'statistics object' then get_statistics_drop(h.objid::oid)
            when h.obj_type = 'collation' then get_collation_drop(h.objid::oid)
            when h.obj_type = 'conversion' then get_conversion_drop(h.objid::oid)
            when h.obj_type = 'database' then get_database_drop(h.objid::oid)
            when h.obj_type = 'role' then get_role_drop(h.objid::oid)
            when h.obj_type = 'policy' then get_policy_drop(h.objid::oid)
            when h.obj_type = 'publication' then get_publication_drop(h.objid::oid)
            when h.obj_type = 'publication namespace' then get_publication_drop(pn.pnpubid::oid)
            when h.obj_type = 'publication relation' then get_publication_drop(pr.prpubid::oid)
            when h.obj_type = 'subscription' then get_subscription_drop(h.objid::oid)
            when h.obj_type = 'default acl' then get_default_acl_drop(h.objid::oid)
            when h.obj_type = 'parameter ACL' then get_parameter_acl_drop(h.objid::oid)
            when h.obj_type = 'large object' then get_large_object_drop(h.objid::oid)
        else null::text end as ddl_drop,

        case
            when h.obj_type = 'table' then get_table_owner(h.objid::oid)
            when h.obj_type in ('function', 'procedure') then get_function_owner(h.objid::oid)
            when h.obj_type in ('view', 'materialized view') then get_view_owner(h.objid::oid)
            when h.obj_type = 'event trigger' then get_event_trigger_owner(h.objid::oid)
            when h.obj_type = 'sequence' then get_sequence_owner(h.objid::oid)
            when h.obj_type = 'schema' then get_schema_owner(h.objid::oid)
            when h.obj_type = 'type' then get_type_owner(h.objid::oid)
            when h.obj_type = 'composite type' then get_type_owner((h.objid::bigint + 2)::oid) -- each composite type generates 3 objects: object in pg_class with relkind = 'c' (e.g. oid = 10000), corresponding array-type in pg_type (oid = 10001), and main composite_type in pg_type (oid = 10002)
            when h.obj_type = 'language' then get_language_owner(h.objid::oid)
            when h.obj_type = 'operator' then get_operator_owner(h.objid::oid)
            when h.obj_type = 'operator family' then get_operator_family_owner(h.objid::oid)
            when h.obj_type = 'operator class' then get_operator_class_owner(h.objid::oid)
            when h.obj_type = 'text search dictionary' then get_ts_dictionary_owner(h.objid::oid)
            when h.obj_type = 'text search configuration' then get_ts_config_owner(h.objid::oid)
            when h.obj_type = 'foreign-data wrapper' then get_fdw_owner(h.objid::oid)
            when h.obj_type = 'server' then get_server_owner(h.objid::oid)
            when h.obj_type = 'foreign table' then get_foreign_table_owner(h.objid::oid)
            when h.obj_type = 'aggregate' then get_aggregate_owner(h.objid::oid)
            when h.obj_type = 'tablespace' then get_tablespace_owner(h.objid::oid)
            when h.obj_type = 'statistics object' then get_statistics_owner(h.objid::oid)
            when h.obj_type = 'collation' then get_collation_owner(h.objid::oid)
            when h.obj_type = 'conversion' then get_conversion_owner(h.objid::oid)
            when h.obj_type = 'database' then get_database_owner(h.objid::oid)
            when h.obj_type = 'publication' then get_publication_owner(h.objid::oid)
            when h.obj_type = 'subscription' then get_subscription_owner(h.objid::oid)
            when h.obj_type = 'large object' then get_large_object_owner(h.objid::oid)
        else null::text end as ownr,

        case
            when h.obj_type in ('table', 'toast table', 'view', 'materialized view', 'foreign table') then get_table_acl_def(h.objid::oid)
            when h.obj_type in ('function', 'procedure') then get_function_acl_def(h.objid::oid)
            when h.obj_type = 'sequence' then get_sequence_acl_def(h.objid::oid)
            when h.obj_type = 'schema' then get_schema_acl_def(h.objid::oid)
            when h.obj_type = 'type' then get_type_acl_def(h.objid::oid)
            when h.obj_type = 'composite type' then get_type_acl_def((h.objid::bigint + 2)::oid) -- each composite type generates 3 objects: object in pg_class with relkind = 'c' (e.g. oid = 10000), corresponding array-type in pg_type (oid = 10001), and main composite_type in pg_type (oid = 10002)
            when h.obj_type = 'language' then get_language_acl_def(h.objid::oid)
            when h.obj_type = 'foreign-data wrapper' then get_fdw_acl_def(h.objid::oid)
            when h.obj_type = 'server' then get_server_acl_def(h.objid::oid)
            when h.obj_type = 'aggregate' then get_aggregate_acl_def(h.objid::oid)
            when h.obj_type = 'tablespace' then get_tablespace_acl_def(h.objid::oid)
            when h.obj_type = 'database' then get_database_acl_def(h.objid::oid)
            when h.obj_type = 'default acl' then get_default_acl_def(h.objid::oid)
            when h.obj_type = 'parameter ACL' then get_parameter_acl_def(h.objid::oid)
            when h.obj_type = 'large object' then get_large_object_acl_def(h.objid::oid)
        else null::text end as acl_def,

        case
            when h.obj_type in ('table', 'toast table', 'view', 'materialized view', 'foreign table') then get_table_acl_drop(h.objid::oid)
            when h.obj_type in ('function', 'procedure') then get_function_acl_drop(h.objid::oid)
            when h.obj_type = 'sequence' then get_sequence_acl_drop(h.objid::oid)
            when h.obj_type = 'schema' then get_schema_acl_drop(h.objid::oid)
            when h.obj_type = 'type' then get_type_acl_drop(h.objid::oid)
            when h.obj_type = 'composite type' then get_type_acl_drop((h.objid::bigint + 2)::oid) -- each composite type generates 3 objects: object in pg_class with relkind = 'c' (e.g. oid = 10000), corresponding array-type in pg_type (oid = 10001), and main composite_type in pg_type (oid = 10002)
            when h.obj_type = 'language' then get_language_acl_drop(h.objid::oid)
            when h.obj_type = 'foreign-data wrapper' then get_fdw_acl_drop(h.objid::oid)
            when h.obj_type = 'server' then get_server_acl_drop(h.objid::oid)
            when h.obj_type = 'aggregate' then get_aggregate_acl_drop(h.objid::oid)
            when h.obj_type = 'tablespace' then get_tablespace_acl_drop(h.objid::oid)
            when h.obj_type = 'database' then get_database_acl_drop(h.objid::oid)
            when h.obj_type = 'default acl' then get_default_acl_drop(h.objid::oid)
            when h.obj_type = 'parameter ACL' then get_parameter_acl_drop(h.objid::oid)
            when h.obj_type = 'large object' then get_large_object_acl_drop(h.objid::oid)
        else null::text end as acl_drop,

        case
            when h.obj_type in ('table', 'toast table') then get_table_comment(h.objid::oid)
            when h.obj_type in ('function', 'procedure') then get_function_comment(h.objid::oid)
            when h.obj_type in ('view', 'materialized view') then get_view_comment(h.objid::oid)
            when h.obj_type = 'index' then get_index_comment(h.objid::oid)
            when h.obj_type = 'rule' then get_rule_comment(h.objid::oid)
            when h.obj_type = 'trigger' then get_trigger_comment(h.objid::oid)
            when h.obj_type = 'event trigger' then get_event_trigger_comment(h.objid::oid)
            when h.obj_type in ('constraint', 'table constraint', 'domain constraint') then get_constraint_comment(h.objid::oid)
            when h.obj_type = 'sequence' then get_sequence_comment(h.objid::oid)
            when h.obj_type = 'schema' then get_schema_comment(h.objid::oid)
            when h.obj_type = 'type' then get_type_comment(h.objid::oid)
            when h.obj_type = 'composite type' then get_type_comment((h.objid::bigint + 2)::oid) -- each composite type generates 3 objects: object in pg_class with relkind = 'c' (e.g. oid = 10000), corresponding array-type in pg_type (oid = 10001), and main composite_type in pg_type (oid = 10002)
            when h.obj_type = 'extension' then get_extension_comment(h.objid::oid)
            when h.obj_type = 'language' then get_language_comment(h.objid::oid)
            when h.obj_type = 'cast' then get_cast_comment(h.objid::oid)
            when h.obj_type = 'operator' then get_operator_comment(h.objid::oid)
            when h.obj_type = 'access method' then get_access_method_comment(h.objid::oid)
            when h.obj_type = 'operator family' then get_operator_family_comment(h.objid::oid)
            when h.obj_type = 'operator class' then get_operator_class_comment(h.objid::oid)
            when h.obj_type = 'text search dictionary' then get_ts_dictionary_comment(h.objid::oid)
            when h.obj_type = 'text search template' then get_ts_template_comment(h.objid::oid)
            when h.obj_type = 'text search parser' then get_ts_parser_comment(h.objid::oid)
            when h.obj_type = 'text search configuration' then get_ts_config_comment(h.objid::oid)
            when h.obj_type = 'foreign-data wrapper' then get_fdw_comment(h.objid::oid)
            when h.obj_type = 'server' then get_server_comment(h.objid::oid)
            when h.obj_type = 'foreign table' then get_foreign_table_comment(h.objid::oid)
            when h.obj_type = 'aggregate' then get_aggregate_comment(h.objid::oid)
            when h.obj_type = 'transform' then get_transform_comment(h.objid::oid)
            when h.obj_type = 'tablespace' then get_tablespace_comment(h.objid::oid)
            when h.obj_type = 'statistics object' then get_statistics_comment(h.objid::oid)
            when h.obj_type = 'collation' then get_collation_comment(h.objid::oid)
            when h.obj_type = 'conversion' then get_conversion_comment(h.objid::oid)
            when h.obj_type = 'database' then get_database_comment(h.objid::oid)
            when h.obj_type = 'role' then get_role_comment(h.objid::oid)
            when h.obj_type = 'policy' then get_policy_comment(h.objid::oid)
            when h.obj_type = 'publication' then get_publication_comment(h.objid::oid)
            when h.obj_type = 'subscription' then get_subscription_comment(h.objid::oid)
            when h.obj_type = 'large object' then get_large_object_comment(h.objid::oid)
        else null::text end as comments

    from dependency_hierarchy h
    left join pg_catalog.pg_attrdef ad on h.objid = ad."oid"
    left join (
                select coalesce(o."oid" , p."oid") as opd_subid, d.refobjid as opd_oid,
                       case when c."oid" is not null then 'operator class'
                            when f."oid" is not null then 'operator family'
                       else null end as dep_check
                from pg_catalog.pg_depend d
                left join pg_catalog.pg_opclass c on d.refobjid = c."oid"
                left join pg_catalog.pg_opfamily f on d.refobjid = f."oid"
                left join pg_catalog.pg_amop o on d.objid = o."oid"
                left join pg_catalog.pg_amproc p on d.objid = p."oid"
                where coalesce(o."oid" , p."oid") is not null and coalesce(c."oid", f."oid") is not null
              ) opd on opd.opd_subid = h.objid
    left join pg_catalog.pg_publication_namespace pn on pn."oid" = h.objid
    left join pg_catalog.pg_publication_rel pr on pr."oid" = h.objid
    order by h.dependency_oid_chain
;

comment on view pg_dependency
    is 'Hierarchical list of dependencies in DB';


-- Function for getting objects's childs by object's OID

create or replace function dependency_childs
    (
        in_obj oid,
        major_obj_only bool default true
    )
    returns table
       (
           objid oid,
           obj_type text,
           obj_name text,
           level int,
           ddl_create text,
           ddl_drop text,
           ownr text,
           acl_def text,
           acl_drop text,
           comments text
       )
    language plpgsql
    security definer
    stable
    as $func$
        declare

            types_ text := '';
            qry text;
        begin

            -- Filter for types of objects
            if major_obj_only is true then
                types_ := $tp$and obj_type in ('database', 'schema', 'table', 'view', 'materialized view', 'index', 'trigger', 'sequence', 'policy', 'function', 'procedure', 'aggregate')$tp$;
            end if;

            -- Final query
            qry := format(
                          $query$
                                 with lvl as
                                 (
                                   select max(level) as lvl
                                   from  pg_dependency
                                   where objid = %1$L
                                 )

                                 select
                                    objid,
                                    obj_type,
                                    obj_name,
                                    max(level) as level,
                                    ddl_create,
                                    ddl_drop,
                                    ownr,
                                    acl_def,
                                    acl_drop,
                                    comments
                                 from pg_dependency d
                                 cross join lvl l
                                 where %1$s::oid = any(dependency_oid_chain) and d.level >= l.lvl
                                 %2$s
                                 group by
                                    objid,
                                    obj_type,
                                    obj_name,
                                    ddl_create,
                                    ddl_drop,
                                    ownr,
                                    acl_def,
                                    acl_drop,
                                    comments
                                 order by level
                          $query$,
                          in_obj,
                          types_
                         );

            return query execute qry;
            end;
    $func$
;

comment on function dependency_childs(in_obj oid, major_obj_only bool)
    is 'Generates a list of child objects for a chosen object by its OID; if major_obj_only is true (by default) then it omits system objects';


-- Function for getting objects's parents by object's OID

create or replace function dependency_parents
    (
        obj oid,
        major_obj_only bool default true
    )
    returns table
       (
           child_objid oid,
           child_obj_type text,
           child_obj_name text,
           parent_objid oid,
           parent_obj_type text,
           parent_obj_name text,
           level int,
           ddl_create text,
           ddl_drop text,
           ownr text,
           acl_def text,
           acl_drop text,
           comments text
       )
    language plpgsql
    security definer
    stable
    as $func$
        declare

            types_ text := '';
            qry text;
        begin

            -- Filter for types of objects
            if major_obj_only is true then
                types_ := $tp$and obj_type in ('database', 'schema', 'table', 'view', 'materialized view', 'index', 'trigger', 'sequence', 'policy', 'function', 'procedure', 'aggregate')$tp$;
            end if;

            -- Final query
            qry := format(
                          $query$
                                 select *
                                 from
                                 (
                                   select c.*,
                                          p.obj_type as parent_obj_type,
                                          p.obj_name as parent_obj_name,
                                          (dense_rank() over(order by max(p.level)) - 1)::int as level,
                                          p.ddl_create,
                                          p.ddl_drop,
                                          p.ownr,
                                          p.acl_def,
                                          p.acl_drop,
                                          p.comments
                                   from pg_dependency p
                                   join (
                                          select distinct
                                                 objid as child_objid,
                                                 obj_type as child_obj_type,
                                                 obj_name as child_obj_name,
                                                 unnest(dependency_oid_chain) as parent_objid
                                          from pg_dependency
                                          where %1$s::oid = objid
                                         ) c on c.parent_objid = p.objid and c.child_objid != p.objid
                                   %2$s
                                   group by
                                        c.child_objid,
                                        c.child_obj_type,
                                        c.child_obj_name,
                                        c.parent_objid,
                                        p.obj_type,
                                        p.obj_name,
                                        p.ddl_create,
                                        p.ddl_drop,
                                        p.ownr,
                                        p.acl_def,
                                        p.acl_drop,
                                        p.comments

                                   union all

                                   select objid as child_objid,
                                          obj_type as child_obj_type,
                                          obj_name as child_obj_name,
                                          objid as parent_objid,
                                          obj_type as parent_obj_type,
                                          obj_name as parent_obj_name,
                                          max(level) as level,
                                          ddl_create,
                                          ddl_drop,
                                          ownr,
                                          acl_def,
                                          acl_drop,
                                          comments
                                   from pg_dependency
                                   where %1$s::oid = objid
                                   group by
                                        objid,
                                        obj_type,
                                        obj_name,
                                        ddl_create,
                                        ddl_drop,
                                        ownr,
                                        acl_def,
                                        acl_drop,
                                        comments
                                 ) res
                                 order by level
                          $query$,
                          obj,
                          types_
                         );

            return query execute qry;
            end;
    $func$
;

comment on function dependency_parents(in_obj oid, major_obj_only bool)
    is 'Generates a list of parent objects for a chosen object by its OID; if major_obj_only is true (by default) then it omits system objects';