-- Examples:

-- select get_function_identity_arguments('version'),
--        get_function_identity_arguments('substring', array['text', 'int']),
--        get_function_identity_arguments('get_function_identity_arguments', array['text', 'text[]', 'text', 'text[]', 'bool', 'bool']),
--        get_function_identity_arguments('get_function_identity_arguments', array['oid', 'bool', 'bool']);

-- select get_function_identity_arguments(proname, typnames, nspname, typschemas),
--        get_function_identity_arguments("oid", false, false)
-- from
-- (
--     select p."oid",
--            p.proname,
--            n.nspname,
--            case when array_agg(t.typname) = array[null]::name[] then null::text[] else array_agg(t.typname order by pat.rn) end as typnames,
--            case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else array_agg(nt.nspname order by pat.rn) end as typschemas
--     from pg_catalog.pg_proc p
--     join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
--     cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
--     left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
--     left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
--     where prokind != 'a'
--     group by p."oid",
--              p.proname,
--              n.nspname
-- ) a;


-- Name + schema + signature arguments

create or replace function get_function_identity_arguments
  (
    in_proc text,
    in_arg_types text[] default null,
    in_schema text default null,
    in_arg_type_schemas text[] default null,
    with_argmodes bool default true,
    with_argnames bool default true
  )
returns text
language plpgsql
as
$$

-- Generates list of identity arguments (IN, INOUT, VARIADIC) for all functions and procedures, but not for aggregates - use get_aggregate_def instead
-- For a full list of arguments (and full DDL) use get_function_def

-- There are two different scenarios of using this function:

  -- Generating ALTER FUNCTION / PROCEDURE command. This usecase requires arguments' modes and accepts arguments' names but does not requires them
      -- Call example: get_function_identity_arguments("oid", true, true/false)
  -- Links to functions when generating commands like CREATE AGGREGATE, CREATE TYPE, CREATE OPERATOR etc. This usecase does not accept neither arguments' modes nor names, only arguments' types
      -- Call example: get_function_identity_arguments("oid", false, false)

-- Example:

-- select get_function_identity_arguments('version'),
--        get_function_identity_arguments('substring', array['text', 'int']);

  declare

    qry text;
    v_proc_params record;

    arg_list text := ''::text;
    in_arg_types_fixed text[];
    in_arg_types_oids oid[];
    v_typoids_rec record;

    input_args_excptn text;

    int_syn text[] := array['integer', 'int'];
    smallint_syn text := 'smallint';
    bigint_syn text := 'bigint';
    num_syn text := 'decimal';
    real_syn text := 'real';
    dp_syn text[] := array['double precision', 'float'];
    time_syn text := 'time without time zone';
    timetz_syn text := 'time with time zone';
    timestamp_syn text := 'timestamp without time zone';
    timestamptz_syn text := 'timestamp with time zone';
    bpchar_syn text := 'character';
    varchar_syn text[] := array['character varying', 'char varying'];
    varbit_syn text := 'bit varying';
    bool_syn text := 'boolean';

  begin

    -- Comparing in_arg_types and in_arg_type_schemas input arguments
    if coalesce(cardinality(in_arg_type_schemas), 0) < coalesce(cardinality(in_arg_types), 0) then
      for in_card in 1..coalesce(cardinality(in_arg_types), 0) - coalesce(cardinality(in_arg_type_schemas), 0)
      loop
        in_arg_type_schemas := array_append(in_arg_type_schemas, null);
      end loop;
    elsif coalesce(cardinality(in_arg_type_schemas), 0) > coalesce(cardinality(in_arg_types), 0) then
      raise warning 'There are more types'' schemas specified in in_arg_type_schemas argument than types'' names specified in in_arg_types argument';

      return null;
    end if;

    -- Catching synonyms for arguments' types
    select array_agg(case when right(tps_sw, 2) = '[]' then rtrim('_' || tps_sw, '[]')
                          else tps_sw end
                     order by rn
                    )
    into in_arg_types_fixed
    from
    (
      select rn,
        case when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = any(int_syn) then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1int4\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = smallint_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1int2\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = bigint_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1int8\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = num_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1numeric\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = real_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1float4\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = any(dp_syn) then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1float8\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = time_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1time\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = timetz_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1timetz\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = timestamp_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1timestamp\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = timestamptz_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1timestamptz\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = bpchar_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1bpchar\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = any(varchar_syn) then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1varchar\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = varbit_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1varbit\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = bool_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1bool\2')
        else tps end as tps_sw
      from
        (
          select tps, rn
          from unnest(coalesce(in_arg_types, array['']::text[])) with ordinality u (tps, rn)
        ) a
    ) b;

    -- Getting OIDs for signature arguments
    if in_arg_types is null then
      in_arg_types_oids := array[0]::oid[];

    else
      for v_typoids_rec in
          select t."oid",
                 tf.rn
          from pg_catalog.pg_type t
          join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
          join unnest(in_arg_types_fixed) with ordinality tf (tps, rn) on tf.tps = t.typname
          join unnest(in_arg_type_schemas) with ordinality ts (arg_sch, rn) on tf.rn = ts.rn
          left join lateral unnest(current_schemas(true)) with ordinality s (sch, rn) on 1 = 1
          where coalesce(ts.arg_sch, s.sch) = n.nspname
          group by t."oid",
                   tf.rn
          order by tf.rn
      loop
        in_arg_types_oids[v_typoids_rec.rn] := v_typoids_rec."oid";
      end loop;

      in_arg_types_oids := array_replace(in_arg_types_oids, null, 0::oid);

    end if;


    -- Checking existence of function
    select b."oid",
           n.nspname,
           jsonb_object_agg(b.rn, jsonb_build_object('argtypes', b.argtypes, 'argmodes', b.argmodes, 'argnames', b.argnames)) as arg_params,
           max(b.rn) as arg_quantity
    into v_proc_params
    from
    (
        select a."oid",
               a.pronamespace,
               quote_ident(nt.nspname) || '.' || quote_ident(t.typname) as argtypes,
               (case a.argmodes when 'o' then 'OUT'
                                when 'b' then 'INOUT'
                                when 'v' then 'VARIADIC'
                                when 't' then 'TABLE'
                else 'IN' end
               )::text as argmodes,
               a.argnames,
               row_number() over(partition by a."oid") as rn
        from
        (
            select p."oid",
                   p.proname,
                   p.pronamespace,
                   unnest(string_to_array(case when p.proargtypes = ''::oidvector then null::text else p.proargtypes::text end, ' ')::oid[]) as in_argtypes,
                   unnest(coalesce(p.proallargtypes, array[0]::oid[])) as argtypes,
                   unnest(p.proargmodes) as argmodes,
                   unnest(p.proargnames) as argnames
            from pg_catalog.pg_proc p
            where p.prokind != 'a'
                  and p.proname = in_proc
                  and string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[] = in_arg_types_oids
        ) a
        left join pg_catalog.pg_type t on coalesce(a.in_argtypes, a.argtypes) = t."oid"
        left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    ) b
    left join pg_catalog.pg_namespace n on n."oid" = b.pronamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where n.nspname = coalesce(in_schema, s.sch)
    group by b."oid",
             n.nspname,
             s.rn
    order by s.rn
    limit 1;

    if v_proc_params."oid" is null then

      select string_agg(coalesce(quote_ident(ts.arg_sch), '[' || array_to_string(current_schemas(true), ', ') || ']') || '.' || quote_ident(tf.tps), ', ' order by ts.rn)
      into input_args_excptn
      from unnest(in_arg_type_schemas) with ordinality ts (arg_sch, rn)
      join unnest(in_arg_types_fixed) with ordinality tf (tps, rn) on tf.rn = ts.rn;

      raise warning 'Function / procedure %.% (%) does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_proc),
        input_args_excptn;

      return null;
    end if;

    -- Getting final list of arguments
    if v_proc_params.arg_params -> 1::text ->> 'argmodes' in ('IN', 'INOUT', 'VARIADIC') and v_proc_params.arg_params -> 1::text ->> 'argtypes' is not null then

      select string_agg(
                        case when with_argmodes is true then r.argmodes || ' ' else '' end
                        || case when with_argnames is true and coalesce(r.argnames, '') != ''
                                then r.argnames || ' '
                           else '' end
                        || r.argtypes,
                        ', ' order by r.arg_num
                       )
      into arg_list
      from
      (
        select f.arg_num,
               a.argmodes,
               a.argnames,
               a.argtypes
        from
        (
            select n."key"::int as arg_num,
                   jsonb_agg(n."value") as vals
            from
            (
              select "key",
                     "value"
              from jsonb_each(v_proc_params.arg_params)
            ) n
            group by n."key"
        ) f
        cross join lateral jsonb_to_recordset(f.vals) a (argmodes text, argnames text, argtypes text)
      ) r
      where r.argmodes in ('IN', 'INOUT', 'VARIADIC');

    end if;

    -- Result

    qry := quote_ident(v_proc_params.nspname) || '.' || quote_ident(in_proc) || '(' || trim(arg_list, ', ') || ')';

    return qry;

  end;

$$;

comment on function get_function_identity_arguments(in_proc text, in_arg_types text[], in_schema text, in_arg_type_schemas text[], with_argmodes bool, with_argnames bool)
    is 'Generates the signature of a function by its name, schema (default - search_path) and signature arguments (as text arrays of types'' names and schemas (default - search_path)); by default - with argument modes and argument names, but in can be disabled via with_argmodes and with_argnames boolean parameters';


-- OID

create or replace function get_function_identity_arguments
  (
    proc_oid oid,
    with_argmodes bool default true,
    with_argnames bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Generates list of identity arguments (IN, INOUT, VARIADIC) for all functions and procedures, but not for aggregates - use get_aggregate_def instead
-- For a full list of arguments (and full DDL) use get_function_def

-- There are two different scenarios of using this function:

  -- Generating ALTER FUNCTION / PROCEDURE command. This usecase requires arguments' modes and accepts arguments' names but does not requires them
      -- Call example: get_function_identity_arguments("oid", true, true/false)
  -- Links to functions when generating commands like CREATE AGGREGATE, CREATE TYPE, CREATE OPERATOR etc. This usecase does not accept neither arguments' modes nor names, only arguments' types
      -- Call example: get_function_identity_arguments("oid", false, false)

-- Examples:

-- select get_function_identity_arguments('version'::regproc),
--        get_function_identity_arguments('substring(text, int)'::regprocedure);

-- select get_function_identity_arguments("oid")
-- from pg_catalog.pg_proc
-- where prokind != 'a'
-- limit 10;

  declare

    qry text;
    v_proc_params record;
    arg_list text := ''::text;

  begin

    -- Checking existence of function
    select proname,
           nspname,
           jsonb_object_agg(rn, jsonb_build_object('argtypes', argtypes, 'argmodes', argmodes, 'argnames', argnames)) as arg_params,
           max(rn) as arg_quantity
    into v_proc_params
    from
    (
        select a.proname,
               a.nspname,
               quote_ident(nt.nspname) || '.' || quote_ident(t.typname) as argtypes,
               (case a.argmodes when 'o' then 'OUT'
                                when 'b' then 'INOUT'
                                when 'v' then 'VARIADIC'
                                when 't' then 'TABLE'
                else 'IN' end
               )::text as argmodes,
               a.argnames,
               row_number() over(partition by a."oid") as rn
        from
        (
            select p."oid",
                   p.proname,
                   n.nspname,
                   unnest(string_to_array(case when p.proargtypes = ''::oidvector then null::text else p.proargtypes::text end, ' ')::oid[]) as in_argtypes,
                   unnest(coalesce(p.proallargtypes, array[0]::oid[])) as argtypes,
                   unnest(p.proargmodes) as argmodes,
                   unnest(p.proargnames) as argnames
            from pg_catalog.pg_proc p
            join pg_catalog.pg_namespace n on n."oid" = p.pronamespace
            where p.prokind != 'a'
                  and p."oid" = proc_oid
        ) a
        left join pg_catalog.pg_type t on coalesce(a.in_argtypes, a.argtypes) = t."oid"
        left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    ) b
    group by proname,
             nspname;

    if v_proc_params.proname is null then
      raise warning 'Function / procedure with OID % does not exist', proc_oid;

      return null;
    end if;

    -- Getting final list of arguments
    if v_proc_params.arg_params -> 1::text ->> 'argmodes' in ('IN', 'INOUT', 'VARIADIC') and v_proc_params.arg_params -> 1::text ->> 'argtypes' is not null then

      select string_agg(
                        case when with_argmodes is true then r.argmodes || ' ' else '' end
                        || case when with_argnames is true and coalesce(r.argnames, '') != ''
                                then r.argnames || ' '
                           else '' end
                        || r.argtypes,
                        ', ' order by r.arg_num
                       )
      into arg_list
      from
      (
        select f.arg_num,
               a.argmodes,
               a.argnames,
               a.argtypes
        from
        (
            select n."key"::int as arg_num,
                   jsonb_agg(n."value") as vals
            from
            (
              select "key",
                     "value"
              from jsonb_each(v_proc_params.arg_params)
            ) n
            group by n."key"
        ) f
        cross join lateral jsonb_to_recordset(f.vals) a (argmodes text, argnames text, argtypes text)
      ) r
      where r.argmodes in ('IN', 'INOUT', 'VARIADIC');

    end if;

    -- Result

    qry := quote_ident(v_proc_params.nspname) || '.' || quote_ident(v_proc_params.proname) || '(' || trim(arg_list, ', ') || ')';

    return qry;

  end;

$$;

comment on function get_function_identity_arguments(proc_oid oid, with_argmodes bool, with_argnames bool)
    is 'Generates the signature of a function by its OID; by default - with argument modes and argument names, but in can be disabled via with_argmodes and with_argnames boolean parameters';