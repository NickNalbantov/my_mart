-- Examples:

-- select get_role_comment(s.rolname),
--        get_role_comment(s."oid")
-- from pg_catalog.pg_shdescription d
-- join pg_catalog.pg_authid s on s.tableoid = d.classoid and s."oid" = d.objoid;


-- Name

create or replace function get_role_comment
  (
    in_rol text
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_role_comment(s.rolname)
-- from pg_catalog.pg_shdescription d
-- join pg_catalog.pg_authid s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_oid oid;
    v_new_owner_oid oid;

  begin

    -- Checking existence of roles
    select "oid"
    into v_oid
    from pg_catalog.pg_authid
    where rolname = in_rol;

    if v_oid is null then
      raise warning 'Role % does not exist', quote_ident(in_rol);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_shdescription d
    where
      d.classoid = 'pg_catalog.pg_authid'::regclass
      and d.objoid = v_oid;

    -- Result
    qry := 'COMMENT ON ROLE ' || quote_ident(in_rol) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_role_comment(in_rol text)
    is 'Generates COMMENT command for a role by its name';


-- OID

create or replace function get_role_comment
  (
    rol_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_role_comment(s."oid")
-- from pg_catalog.pg_shdescription d
-- join pg_catalog.pg_authid s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_rolname text;
    v_new_owner text;

  begin

    -- Checking existence of roles
    select rolname
    into v_rolname
    from pg_catalog.pg_authid
    where "oid" = rol_oid;

    if v_rolname is null then
      raise warning 'Role with OID % does not exist', rol_oid;

      return null;

    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_shdescription d
    where
      d.classoid = 'pg_catalog.pg_authid'::regclass
      and d.objoid = rol_oid;

    -- Result
    qry := 'COMMENT ON ROLE ' || quote_ident(v_rolname) || E'\n    IS ' || quote_literal(v_comment) || ';';
    return qry;

  end;

$$;

comment on function get_role_comment(rol_oid oid)
    is 'Generates COMMENT command for a role by its OID';