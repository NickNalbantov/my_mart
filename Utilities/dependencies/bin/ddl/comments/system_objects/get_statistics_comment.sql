-- Examples:

-- select get_statistics_comment(s.stxname, n.nspname),
--        get_statistics_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_statistic_ext s on s.tableoid = d.classoid and s."oid" = d.objoid
-- join pg_catalog.pg_namespace n on s.stxnamespace = n."oid";


-- Name

create or replace function get_statistics_comment
  (
    in_stat text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_statistics_comment(s.stxname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_statistic_ext s s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.stxnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_stat_params record;

  begin

    -- Checking existence of statistics object

    select
      st."oid",
      n.nspname
    into v_stat_params
    from pg_catalog.pg_statistic_ext st
    left join pg_catalog.pg_namespace n on n."oid" = st.stxnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      in_stat = st.stxname
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_stat_params."oid" is null then
      raise warning 'Statistics object %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_stat);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_statistic_ext'::regclass
      and d.objoid = v_stat_params."oid";

    -- Result
    qry := 'COMMENT ON STATISTICS ' || quote_ident(v_stat_params.nspname) || '.' || quote_ident(in_stat)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_statistics_comment(in_stat text, in_schema text)
    is 'Generates COMMENT command for a statistics object by its name and schema (default - search_path)';


-- OID

create or replace function get_statistics_comment
  (
    stat_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_statistics_comment("oid")
-- from pg_catalog.pg_statistic_ext s
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_stat_params record;

  begin

    -- Checking existence of statistics object

    select
      st.stxname,
      n.nspname
    into v_stat_params
    from pg_catalog.pg_statistic_ext st
    join pg_catalog.pg_namespace n on n."oid" = st.stxnamespace
    where stat_oid = st."oid";

    if v_stat_params.stxname is null then
      raise warning 'Statistics object with OID % does not exist', stat_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_statistic_ext'::regclass
      and d.objoid = stat_oid;

    -- Result

    qry := 'COMMENT ON STATISTICS ' || quote_ident(v_stat_params.nspname) || '.' || quote_ident(v_stat_params.stxname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_statistics_comment(stat_oid oid)
    is 'Generates COMMENT command for a statistics object by its OID';