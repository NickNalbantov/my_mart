-- Examples:

-- select get_tablespace_comment(s.spcname),
--        get_tablespace_comment(s."oid")
-- from pg_catalog.pg_shdescription d
-- join pg_catalog.pg_tablespace s on s.tableoid = d.classoid and s."oid" = d.objoid;


-- Name

create or replace function get_tablespace_comment
  (
    in_tblspc text
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_tablespace_comment(s.spcname)
-- from pg_catalog.pg_shdescription d
-- join pg_catalog.pg_tablespace s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_oid oid;

  begin

    -- Checking existence of tablespace
    select "oid"
    into v_oid
    from pg_catalog.pg_tablespace
    where spcname = in_tblspc;

    if v_oid is null then
      raise warning 'Tablespace % does not exist', quote_ident(in_tblspc);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_shdescription d
    where
      d.classoid = 'pg_catalog.pg_tablespace'::regclass
      and d.objoid = v_oid;

    -- Result
    qry := 'COMMENT ON TABLESPACE ' || quote_ident(in_tblspc) || E'\n    IS ' || quote_literal(v_comment) || ';';
    return qry;

  end;

$$;

comment on function get_tablespace_comment(in_tblspc text)
    is 'Generates COMMENT command for a tablespace by its name';


-- OID

create or replace function get_tablespace_comment
  (
    tblspc_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_tablespace_comment(s."oid")
-- from pg_catalog.pg_shdescription d
-- join pg_catalog.pg_tablespace s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_spcname text;

  begin

    -- Checking existence of tablespace
    select spcname
    into v_spcname
    from pg_catalog.pg_tablespace
    where "oid" = tblspc_oid;

    if v_spcname is null then
      raise warning 'Tablespace with OID % does not exist', tblspc_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_shdescription d
    where
      d.classoid = 'pg_catalog.pg_tablespace'::regclass
      and d.objoid = tblspc_oid;

    -- Result
    qry := 'COMMENT ON TABLESPACE ' || quote_ident(v_spcname) || E'\n    IS ' || quote_literal(v_comment) || ';';
    return qry;

  end;

$$;

comment on function get_tablespace_comment(tblspc_oid oid)
    is 'Generates COMMENT command for a tablespace by its OID';