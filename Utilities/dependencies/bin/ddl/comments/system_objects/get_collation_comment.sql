-- Examples:

-- select get_collation_comment(s.collname, n.nspname),
--        get_collation_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_collation s on s.tableoid = d.classoid and s."oid" = d.objoid
-- join pg_catalog.pg_namespace n on s.collnamespace = n."oid";

-- select get_collation_comment('ru-RU-x-icu');


-- Name + schema

create or replace function get_collation_comment
  (
    in_coll text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Examples:

-- select get_collation_comment('ru-RU-x-icu');

-- select get_collation_comment(s.collname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_collation s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.collnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_coll_params record;

  begin

    -- Checking existence of collation
    select
      c."oid",
      n.nspname
    into v_coll_params
    from pg_catalog.pg_collation c
    left join pg_catalog.pg_namespace n on c.collnamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      c.collname = in_coll
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_coll_params."oid" is null then
      raise warning 'Collation %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_coll);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_collation'::regclass
      and d.objoid = v_coll_params."oid";

    -- Result
    qry := 'COMMENT ON COLLATION ' || quote_ident(v_coll_params.nspname) || '.' || quote_ident(in_coll)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_collation_comment(in_coll text, in_schema text)
    is 'Generates COMMENT command for a collation by its name and schema (default - search_path)';


-- OID

create or replace function get_collation_comment
  (
    coll_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Examples:

-- select get_collation_comment('"ru-RU-x-icu"'::regcollation);

-- select get_collation_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_collation'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_coll_params record;

  begin

    -- Checking existence of collation
    select
      c.collname,
      n.nspname
    into v_coll_params
    from pg_catalog.pg_collation c
    join pg_catalog.pg_namespace n on c.collnamespace = n."oid"
    where c."oid" = coll_oid;

    if v_coll_params.collname is null then
      raise warning 'Collation with OID % does not exist', coll_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_collation'::regclass
      and d.objoid = coll_oid;

    -- Result
    qry := 'COMMENT ON COLLATION ' || quote_ident(v_coll_params.nspname) || '.' || quote_ident(v_coll_params.collname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_collation_comment(coll_oid oid)
    is 'Generates COMMENT command for a collation by its OID';