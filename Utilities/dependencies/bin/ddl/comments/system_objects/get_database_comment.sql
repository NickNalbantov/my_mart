-- Examples:

-- select get_database_comment(s.datname),
--        get_database_comment(s."oid")
-- from pg_catalog.pg_shdescription d
-- join pg_catalog.pg_database s on s.tableoid = d.classoid and s."oid" = d.objoid;


-- Name

create or replace function get_database_comment
  (
    in_db text
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_database_comment(s.datname),
-- from pg_catalog.pg_shdescription d
-- join pg_catalog.pg_database s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_oid oid;

  begin

    -- Checking existence of database
    select d."oid"
    into v_oid
    from pg_catalog.pg_database d
    where d.datname = in_db;

    if v_oid is null then
      raise warning 'Database % does not exist', quote_ident(in_db);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_shdescription d
    where
      d.classoid = 'pg_catalog.pg_database'::regclass
      and d.objoid = v_oid;

    -- Result
    qry := 'COMMENT ON DATABASE ' || quote_ident(in_db) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_database_comment(in_db text)
    is 'Generates COMMENT command for a database by its name';


-- OID

create or replace function get_database_comment
  (
    db_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_database_comment("oid"),
-- from pg_catalog.pg_shdescription d
-- join pg_catalog.pg_database s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_db_name text;

  begin

    -- Checking existence of database
    select d.datname
    into v_db_name
    from pg_catalog.pg_database d
    where d."oid" = db_oid;

    if v_db_name is null then
      raise warning 'Database with OID % does not exist', db_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_shdescription d
    where
      d.classoid = 'pg_catalog.pg_database'::regclass
      and d.objoid = db_oid;

    -- Result
    qry := 'COMMENT ON DATABASE ' || quote_ident(v_db_name) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;
  end;
$$;

comment on function get_database_comment(db_oid oid)
    is 'Generates COMMENT command for a database by its OID';