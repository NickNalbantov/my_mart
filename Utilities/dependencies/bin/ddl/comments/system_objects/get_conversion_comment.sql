-- Examples:

-- select get_conversion_comment(s.conname, n.nspname),
--        get_conversion_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_conversion s on s.tableoid = d.classoid and s."oid" = d.objoid
-- join pg_catalog.pg_namespace n on s.connamespace = n."oid";

-- select get_conversion_comment('utf8_to_windows_1251');


-- Name + schema

create or replace function get_conversion_comment
  (
    in_con text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Examples:

-- select get_conversion_comment('utf8_to_windows_1251');

-- select get_conversion_comment(s.conname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_conversion s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.connamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_con_params record;

  begin

    -- Checking existence of conversion
    select
      c."oid",
      n.nspname
    into v_con_params
    from pg_catalog.pg_conversion c
    left join pg_catalog.pg_namespace n on c.connamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      c.conname = in_con
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_con_params."oid" is null then
      raise warning 'Conversion %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_con);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_conversion'::regclass
      and d.objoid = v_con_params."oid";

    -- Result
    qry := 'COMMENT ON CONVERSION ' || quote_ident(v_con_params.nspname) || '.' || quote_ident(in_con)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_conversion_comment(in_con text, in_schema text)
    is 'Generates COMMENT command for a conversion by its name and schema (default - search_path)';


-- OID

create or replace function get_conversion_comment
  (
    con_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_conversion_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_conversion'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_con_params record;

  begin

    -- Checking existence of conversion
    select
      c.conname,
      n.nspname
    into v_con_params
    from pg_catalog.pg_conversion c
    join pg_catalog.pg_namespace n on c.connamespace = n."oid"
    where c."oid" = con_oid;

    if v_con_params.conname is null then
      raise warning 'Conversion with OID % does not exist', con_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_conversion'::regclass
      and d.objoid = con_oid;

    -- Result
    qry := 'COMMENT ON CONVERSION ' || quote_ident(v_con_params.nspname) || '.' || quote_ident(v_con_params.conname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_conversion_comment(con_oid oid)
    is 'Generates COMMENT command for a conversion by its OID';