-- Examples:

-- select "oid" as obj_oid,
--        amname as obj_name,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_am

-- union all

--  select c."oid",
--         c.relnamespace::regnamespace || '.' || c.relname || '.' || a.attname,
--         get_object_comment(c.tableoid, c."oid", a.attnum)
--  from pg_catalog.pg_class c
--  join pg_catalog.pg_attribute a on a.attrelid = c."oid"
--  where a.attnum > 0
--        and a.attisdropped is false

-- union all

-- select "oid",
--        rolname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_authid

-- union all

-- select "oid",
--        "oid"::text,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_cast

-- union all

-- select "oid",
--        relnamespace::regnamespace || '.' || relname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_class
-- where relkind != 't'

-- union all

-- select "oid",
--        collnamespace::regnamespace || '.' || collname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_collation

-- union all

-- select "oid",
--        connamespace::regnamespace || '.' || conname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_constraint

-- union all

-- select "oid",
--        connamespace::regnamespace || '.' || conname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_conversion

-- union all

-- select "oid",
--        datname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_database

-- union all

-- select "oid",
--        evtname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_event_trigger

-- union all

-- select "oid",
--        extname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_extension

-- union all

-- select "oid",
--        fdwname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_foreign_data_wrapper

-- union all

-- select "oid",
--        srvname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_foreign_server

-- union all

-- select "oid",
--        lanname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_language

-- union all

-- select "oid",
--        "oid"::text as lomname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_largeobject_metadata

-- union all

-- select "oid",
--        nspname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_namespace

-- union all

-- select "oid",
--        opcnamespace::regnamespace || '.' || opcname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_opclass

-- union all

-- select "oid",
--        oprnamespace::regnamespace || '.' || oprname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_operator

-- union all

-- select "oid",
--        opfnamespace::regnamespace || '.' || opfname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_opfamily

-- union all

-- select "oid",
--        polname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_policy

-- union all

-- select "oid",
--        pronamespace::regnamespace || '.' || proname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_proc

-- union all

-- select "oid",
--        pubname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_publication

-- union all

-- select "oid",
--        rulename,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_rewrite

-- union all

-- select "oid",
--        stxnamespace::regnamespace || '.' || stxname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_statistic_ext

-- union all

-- select "oid",
--        subname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_subscription

-- union all

-- select "oid",
--        spcname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_tablespace

-- union all

-- select "oid",
--        "oid"::text,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_transform

-- union all

-- select "oid",
--        tgname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_trigger

-- union all

-- select "oid",
--        cfgnamespace::regnamespace || '.' || cfgname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_ts_config

-- union all

-- select "oid",
--        dictnamespace::regnamespace || '.' || dictname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_ts_dict

-- union all

-- select "oid",
--        tmplnamespace::regnamespace || '.' || tmplname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_ts_template

-- union all

-- select "oid",
--        prsnamespace::regnamespace || '.' || prsname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_ts_parser

-- union all

-- select "oid",
--        typnamespace::regnamespace || '.' || typname,
--        get_object_comment(tableoid, "oid")
-- from pg_catalog.pg_type;


-- OIDs

create or replace function get_object_comment
  (
    classid oid,
    objid oid,
    objsubid anycompatiblenonarray default 0
  )
returns text
language plpgsql
strict
as
$$

-- This is a general-use version of get_xxx_comment function, works only with OIDs/sub-ids. For text-based inputs and faster OID recognition use get_xxx_comment functions

-- 'classid' argument is necessary because objects' OID are unique only within given catalog

-- Example:

-- select s."oid",
--        s.relnamespace::regnamespace || '.' || s.relname,
--        get_object_comment(s.tableoid, s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_identity_rec record;
    v_object_clause text;
    v_comment text;
    v_domain_check boolean default false;
    v_objsubid int;
    v_shared bool;

  begin

    v_objsubid := objsubid::int; -- needed to avoid ambiguity with a column in pg_description

    -- Identifying object
    select
      case when i."type" like '%column' then quote_ident('pg_attribute')
          else quote_ident(c.relname)
      end as catalog_name,
      i."type" as obj_type,
      i."identity" as obj_identity
    into v_identity_rec
    from pg_catalog.pg_class c
    cross join lateral pg_identify_object(c."oid", objid, case when classid = 'pg_catalog.pg_class'::regclass then objsubid else 0 end) i
    where
        c."oid" = classid
        and c.relnamespace = 'pg_catalog'::regnamespace
        and c.relkind = 'r';


    if v_identity_rec.catalog_name is null then
      raise warning E'Can''t identify object with classid (system catalog table''s OID) % and object''s OID %',
        classid,
        objid;

      return null;
    end if;

    -- Check if object is a domain
    if classid = 'pg_catalog.pg_type'::regclass then
      select typtype = 'd'
      into v_domain_check
      from pg_catalog.pg_type
      where "oid" = objid;
    elsif classid = 'pg_catalog.pg_constraint'::regclass then
      select contypid != 0
      into v_domain_check
      from pg_catalog.pg_constraint
      where "oid" = objid;
    end if;

    -- Building object type clause
    select
      case
        when v_identity_rec.obj_type = 'type' and v_domain_check is true then 'DOMAIN'
        when v_identity_rec.obj_type = 'type' and v_domain_check is not true then 'TYPE'
        when v_identity_rec.obj_type in ('table constraint', 'domain constraint') then 'CONSTRAINT'
        when v_identity_rec.obj_type = 'composite type' then 'TYPE'
        when v_identity_rec.obj_type = 'foreign-data wrapper' then 'FOREIGN DATA WRAPPER'
        when v_identity_rec.obj_type = 'statistics object' then 'STATISTICS'
        when v_identity_rec.obj_type like '%column' then 'COLUMN'
        when v_identity_rec.obj_type in (
                                          'table', 'foreign table', 'view', 'materialized view', 'sequence', 'index',
                                          'function', 'procedure', 'aggregate', 'event trigger', 'trigger', 'constraint', 'policy', 'rule',
                                          'collation', 'conversion', 'language', 'transform', 'extension', 'large object',
                                          'server', 'publication', 'subscription', 'role',
                                          'schema', 'database', 'tablespace',
                                          'text search configuration', 'text search dictionary', 'text search parser', 'text search template',
                                          'access method', 'cast', 'operator', 'operator class', 'operator family'
                                        )
              then upper(v_identity_rec.obj_type)
        else null::text
      end,
      case when lower(v_identity_rec.obj_type) in ('database', 'tablespace', 'role') then true else false end
    into
      v_object_clause,
      v_shared;

    -- Getting the comment
    if v_shared is true then
      select "description"
      into v_comment
      from pg_catalog.pg_shdescription d
      where
        classid = d.classoid
        and objid = d.objoid;

    else
      select "description"
      into v_comment
      from pg_catalog.pg_description d
      where
        classid = d.classoid
        and objid = d.objoid
        and v_objsubid = d.objsubid;

    end if;

    -- Result

    qry := 'COMMENT ON ' || v_object_clause || ' ' || v_identity_rec.obj_identity || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_object_comment(classid oid, objid oid, objsubid anycompatiblenonarray)
    is 'Generates COMMENT command for an object by OID of parent catalog, object''s OID and object''s sub-id (e.g. column number)';