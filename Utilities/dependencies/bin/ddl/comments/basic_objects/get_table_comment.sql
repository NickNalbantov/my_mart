-- Examples:

-- select get_table_comment(c.relname, n.nspname),
--        get_table_comment(c."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class c on c.tableoid = d.classoid and c."oid" = d.objoid
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where c.relkind in ('r', 'p');


-- Name + schema

create or replace function get_table_comment
  (
    in_table text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Generates COMMENT command for regular tables, typed tables, partitioned tables, partitions and subpartitions

-- For foreign tables use get_foreign_table_comment functions

-- Examples:

-- select get_table_comment(s.relname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.relkind in ('r', 'p')
--       and s.relnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_table_params record;

  begin

    -- Checking existence of table
    select
      c."oid",
      n.nspname
    into v_table_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      c.relkind in ('r', 'p')
      and c.relname = in_table
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_table_params."oid" is null then
      raise warning 'Table %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_table);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_class'::regclass
      and d.objoid = v_table_params."oid";

    -- Result
    qry := 'COMMENT ON TABLE ' || quote_ident(v_table_params.nspname) || '.' || quote_ident(in_table)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_table_comment(in_table text, in_schema text)
    is 'Generates COMMENT command for a table by its name and schema (default - search_path)';


-- OID

create or replace function get_table_comment
  (
    table_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Generates COMMENT command for regular tables, typed tables, partitioned tables, partitions and subpartitions

-- For foreign tables use get_foreign_table_comment functions

-- Examples:

-- select get_table_comment('pg_trigger'::regclass);

-- select get_table_comment(d.objoid)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.relkind in ('r', 'p')
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_table_params record;

  begin

    -- Checking existence of table
    select n.nspname,
           c.relname
    into v_table_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where c.relkind in ('r','p')
          and c."oid" = table_oid;

    if v_table_params.relname is null then
      raise warning 'Table with OID % does not exist', table_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_class'::regclass
      and d.objoid = table_oid;

    -- Result
    qry := 'COMMENT ON TABLE ' || quote_ident(v_table_params.nspname) || '.' || quote_ident(v_table_params.relname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_table_comment(table_oid oid)
    is 'Generates COMMENT command for a table by its OID';