-- Examples:

-- select get_index_comment(c.relname, n.nspname),
--        get_index_comment(c."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class c on c.tableoid = d.classoid and c."oid" = d.objoid
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where c.relkind in ('i', 'I');


-- Name + schema

create or replace function get_index_comment
  (
    in_idx text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_index_comment(s.relname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.relkind in ('i', 'I')
--       and s.relnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_index_params record;

  begin

    -- Checking existence of index
    select
      c."oid",
      n.nspname
    into v_index_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      c.relkind in ('i', 'I')
      and c.relname = in_idx
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_index_params."oid" is null then
      raise warning 'Index % in schema % not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_idx);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_class'::regclass
      and d.objoid = v_index_params."oid";

    -- Result
    qry := 'COMMENT ON INDEX ' || quote_ident(v_index_params.nspname) || '.' || quote_ident(in_idx)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_index_comment(in_idx text, in_schema text)
    is 'Generates COMMENT command for an index by its name and schema (default - search_path)';


-- OID

create or replace function get_index_comment
  (
    idx_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Examples:

-- select get_index_comment('pg_attrdef_adrelid_adnum_index'::regclass);

-- select get_index_comment(d.objoid)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.relkind in ('i', 'I')
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_index_params record;

  begin

    -- Checking existence of index
    select
      c.relname,
      n.nspname
    into v_index_params
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
    where c."oid" = idx_oid;

    if v_index_params.relname is null then
      raise warning 'Index with OID % does not exist', idx_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_class'::regclass
      and d.objoid = idx_oid;

    -- Result
    qry := 'COMMENT ON INDEX ' || quote_ident(v_index_params.nspname) || '.' || quote_ident(v_index_params.relname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_index_comment(idx_oid oid)
    is 'Generates COMMENT command for an index by its OID';