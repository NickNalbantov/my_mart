-- Examples:

-- select get_trigger_comment(t.tgname, c.relname, n.nspname),
--        get_trigger_comment(t."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_trigger t on t.tableoid = d.classoid and t."oid" = d.objoid
-- join pg_catalog.pg_class c on t.tgrelid = c."oid"
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace;

-- Name

create or replace function get_trigger_comment
  (
    in_trig text,
    in_parent text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

  declare

    qry text;
    v_comment text;
    v_trig_params record;

  begin

    -- Checking existence of trigger
    select
      t."oid",
      n.nspname
    into v_trig_params
    from pg_catalog.pg_trigger t
    join pg_catalog.pg_class c on t.tgrelid = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      t.tgname = in_trig
      and c.relname = in_parent
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_trig_params."oid" is null then
      raise warning 'Trigger % on %.% does not exist',
        quote_ident(in_trig),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_parent);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_trigger'::regclass
      and d.objoid = v_trig_params."oid";

    -- Result
    qry := 'COMMENT ON TRIGGER ' || quote_ident(in_trig) || ' ON ' || quote_ident(v_trig_params.nspname) || '.' || quote_ident(in_parent)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_trigger_comment(in_trig text, in_parent text, in_schema text)
    is 'Generates COMMENT command for a trigger by its name, name of the parent object and its schema (default - search_path)';


-- OID

create or replace function get_trigger_comment
  (
    trig_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_trigger_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_trigger'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_trig_params record;

  begin

    -- Checking existence of trigger
    select t.tgname,
           c.relname,
           n.nspname
    into v_trig_params
    from pg_catalog.pg_trigger t
    join pg_catalog.pg_class c on t.tgrelid = c."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where t."oid" = trig_oid;

    if v_trig_params.tgname is null then
      raise warning 'Trigger with OID % does not exist', trig_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_trigger'::regclass
      and d.objoid = trig_oid;

    -- Result
    qry := 'COMMENT ON TRIGGER ' || quote_ident(v_trig_params.tgname) || ' ON ' || quote_ident(v_trig_params.nspname) || '.' || quote_ident(v_trig_params.relname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_trigger_comment(trig_oid oid)
    is 'Generates COMMENT command for a trigger by its OID';