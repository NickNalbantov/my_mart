-- Examples:

-- select get_rule_comment(r.rulename, c.relname, n.nspname),
--        get_rule_comment(r."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_rewrite r on r.tableoid = d.classoid and r."oid" = d.objoid
-- join pg_catalog.pg_class c on r.ev_class = c."oid"
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace;


-- Rule name + parent name + parent schema

create or replace function get_rule_comment
  (
    in_rule text,
    in_parent text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

  declare

    qry text;
    v_comment text;
    v_rule_params record;

  begin

    -- Checking existence of rule
    select
      r."oid",
      n.nspname
    into v_rule_params
    from pg_catalog.pg_rewrite r
    join pg_catalog.pg_class c on r.ev_class = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      r.rulename = in_rule
      and c.relname = in_parent
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_rule_params."oid" is null then
      raise warning 'Rule % for table / view %.% does not exist',
        quote_ident(in_rule),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_parent);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_rewrite'::regclass
      and d.objoid = v_rule_params."oid";

    -- Result
    qry := 'COMMENT ON RULE ' || quote_ident(in_rule) || ' ON ' || quote_ident(v_rule_params.nspname) || '.' || quote_ident(in_parent)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_rule_comment(in_rule text, in_parent text, in_schema text)
    is 'Generates COMMENT command for a rule by its name and parent''s name and schema (default - search_path)';


-- OID

create or replace function get_rule_comment
  (
    rule_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_rule_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_rewrite'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_rule_params record;
    v_rule_parent text;

  begin

    -- Checking existence of rule
    select
      rulename,
      ev_class
    into v_rule_params
    from pg_catalog.pg_rewrite
    where "oid" = rule_oid;

    if v_rule_params.rulename is null then
      raise warning 'Rule with OID % does not exist', rule_oid;

      return null;
    end if;

    -- Parent
    select quote_ident(n.nspname) || '.' || quote_ident(c.relname)
    into v_rule_parent
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where c."oid" = v_rule_params.ev_class;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_rewrite'::regclass
      and d.objoid = rule_oid;

    -- Result
    qry := 'COMMENT ON RULE ' || quote_ident(v_rule_params.rulename) || ' ON ' || v_rule_parent
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_rule_comment(rule_oid oid)
    is 'Generates COMMENT command for a rule by its OID';