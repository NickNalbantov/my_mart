-- Examples:

-- select get_constraint_comment(c.conname, coalesce(cl.relname, t.typname), n.nspname),
--        get_constraint_comment(c."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_constraint c on c.tableoid = d.classoid and c."oid" = d.objoid
-- left join pg_catalog.pg_class cl on c.conrelid = cl."oid" and cl.relkind in ('r', 'p', 'f')
-- left join pg_catalog.pg_type t on c.contypid = t."oid" and t.typtype = 'd'
-- join pg_catalog.pg_namespace n on n."oid" = c.connamespace
-- where contype != 't';

-- select get_constraint_comment('pg_attrdef_adrelid_adnum_index', 'pg_attrdef');


-- Name

create or replace function get_constraint_comment
  (
    in_constr text,
    in_parent text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_constraint_comment('pg_attrdef_adrelid_adnum_index', 'pg_attrdef');

  declare

    qry text;
    v_comment text;
    v_constr_params record;

  begin

    -- Checking existence of constraint
    select c."oid",
           n.nspname,
           c.contypid
    into v_constr_params
    from pg_catalog.pg_constraint c
    left join pg_catalog.pg_class cl on c.conrelid = cl."oid" and cl.relkind in ('r', 'p', 'f')
    left join pg_catalog.pg_type t on c.contypid = t."oid" and t.typtype = 'd'
    left join pg_catalog.pg_namespace n on n."oid" = c.connamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.conname = in_constr
          and n.nspname = coalesce(in_schema, s.sch)
          and coalesce(cl.relname, t.typname) = in_parent
          and c.contype != 't'
    order by s.rn
    limit 1;

    if v_constr_params."oid" is null then
      raise warning 'Constraint % for %.% does not exist',
        quote_ident(in_constr),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_parent);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_constraint'::regclass
      and d.objoid = v_constr_params."oid";

    -- Result

    qry := 'COMMENT ON CONSTRAINT ' || quote_ident(in_constr) || ' ON '
           || case when v_constr_params.contypid != 0::oid then 'DOMAIN ' else '' end
           || quote_ident(v_constr_params.nspname) || '.' || quote_ident(in_parent)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_constraint_comment(in_constr text, in_parent text, in_schema text)
    is 'Generates COMMENT command by constraint''s name, name of the parent object and its schema (default - search_path)';


-- OID

create or replace function get_constraint_comment
  (
    constr_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_constraint_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_constraint'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_constr_params record;

  begin

    -- Checking existence of constraint
    select n.nspname as parent_schema,
           coalesce(cl.relname, t.typname) as parent_obj,
           c.conname,
           c.contypid
    into v_constr_params
    from pg_catalog.pg_constraint c
    left join pg_catalog.pg_class cl on c.conrelid = cl."oid" and cl.relkind in ('r', 'p', 'f')
    left join pg_catalog.pg_type t on c.contypid = t."oid" and t.typtype = 'd'
    join pg_catalog.pg_namespace n on n."oid" = c.connamespace
    where c."oid" = constr_oid
          and c.contype != 't';

    if v_constr_params.conname is null then
      raise warning 'Constraint with OID % does not exist', constr_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_constraint'::regclass
      and d.objoid = constr_oid;

    -- Result

    qry := 'COMMENT ON CONSTRAINT ' || quote_ident(v_constr_params.conname) || ' ON '
           || case when v_constr_params.contypid != 0::oid then 'DOMAIN ' else '' end
           || quote_ident(v_constr_params.parent_schema) || '.' || quote_ident(v_constr_params.parent_obj)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_constraint_comment(constr_oid oid)
    is 'Generates COMMENT command by constraint''s OID';