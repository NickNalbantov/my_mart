-- Examples:

-- select get_column_comment(a.attname, c.relname, n.nspname),
--        get_column_comment(a.attname, c."oid"),
--        get_column_comment(a.attnum, c."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class c on c.tableoid = d.classoid and c."oid" = d.objoid
-- join pg_catalog.pg_attribute a on c."oid" = a.attrelid and a.attnum = d.objsubid
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where a.attisdropped is false;


-- Column name + table name + schema

create or replace function get_column_comment
  (
    in_column text,
    in_table text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

  declare

    v_col_def_params record;
    qry text;
    v_comment text;

  begin

    -- Checking existence of the column
    select
      c."oid" as parent_oid,
      n.nspname,
      a.attnum
    into v_col_def_params
    from pg_catalog.pg_attribute a
    join pg_catalog.pg_class c on a.attrelid = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      a.attisdropped is false
      and a.attname = in_column
      and c.relname = in_table
      and coalesce(in_schema, s.sch) = n.nspname
      and c.relkind in ('r', 'p', 'v', 'm', 'c')
    order by s.rn
    limit 1;

    if v_col_def_params.parent_oid is null then
      raise warning 'Column % of table %.% does not exist',
        quote_ident(in_column),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_table);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_class'::regclass
      and d.objoid = v_col_def_params.parent_oid
      and d.objsubid = v_col_def_params.attnum;

    -- Result
    qry := 'COMMENT ON COLUMN '
           || quote_ident(v_col_def_params.nspname) || '.' || quote_ident(in_table) || '.' || quote_ident(in_column)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_column_comment(in_column text, in_table text, in_schema text)
    is 'Generates COMMENT command for a table''s column by name of the column and table''s name and schema (default - search_path)';


-- Column name + OID

create or replace function get_column_comment
  (
    in_column text,
    table_oid oid
  )
  returns text
  language plpgsql
  strict
  as
  $$

-- Same command can be obtained with get_object_comment function as well

  declare

    v_col_def_params record;
    qry text;
    v_comment text;

  begin

    -- Checking existence of default value
    select c.relname,
           n.nspname,
           a.attnum
    into v_col_def_params
    from pg_catalog.pg_attribute a
    join pg_catalog.pg_class c on a.attrelid = c."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where
      a.attisdropped is false
      and a.attname = in_column
      and c.relkind in ('r', 'p', 'v', 'm', 'c')
      and c."oid" = table_oid;

    if v_col_def_params.relname is null then
      raise warning 'Default value for column % of table with OID % does not exist', quote_ident(in_column), table_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_class'::regclass
      and d.objoid = table_oid
      and d.objsubid = v_col_def_params.attnum;

    -- Result
    qry := 'COMMENT ON COLUMN '
           || quote_ident(v_col_def_params.nspname) || '.' || quote_ident(v_col_def_params.relname) || '.' || quote_ident(in_column)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;
  end;
$$;

comment on function get_column_comment(in_column text, table_oid oid)
    is 'Generates COMMENT command for a table''s by name of the column and table''s OID';


-- Column position + OID

create or replace function get_column_comment
  (
    column_num anycompatiblenonarray,
    table_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

  declare

    v_col_def_params record;
    qry text;
    v_comment text;

  begin

    -- Checking existence of default value
    select c.relname,
           n.nspname,
           a.attname
    into v_col_def_params
    from pg_catalog.pg_attribute a
    join pg_catalog.pg_class c on a.attrelid = c."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where a.attisdropped is false
          and a.attnum = column_num
          and c.relkind in ('r', 'p', 'v', 'm', 'c')
          and c."oid" = table_oid;

    if v_col_def_params.relname is null then
      raise warning 'Default value for column at position % of table with OID % does not exist', column_num, table_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_class'::regclass
      and d.objoid = table_oid
      and d.objsubid = column_num;

    -- Result
    qry := 'COMMENT ON COLUMN '
           || quote_ident(v_col_def_params.nspname) || '.' || quote_ident(v_col_def_params.relname) || '.' || quote_ident(v_col_def_params.attname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_column_comment(column_num anycompatiblenonarray, table_oid oid)
    is 'Generates COMMENT command for a table''s column by column''s position number and table''s OID';