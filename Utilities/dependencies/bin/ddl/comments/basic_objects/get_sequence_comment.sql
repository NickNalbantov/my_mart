-- Examples:

-- select get_sequence_comment(c.relname, n.nspname),
--        get_sequence_comment(c."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class c on c.tableoid = d.classoid and c."oid" = d.objoid
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
-- where c.relkind = 'S';


-- Name + schema

create or replace function get_sequence_comment
  (
    in_seq text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

  declare

    qry text;
    v_comment text;
    v_seq_params record;

  begin

    -- Checking existence of sequence
    select
      c."oid",
      n.nspname
    into v_seq_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      c.relkind = 'S'
      and c.relname = in_seq
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_seq_params."oid" is null then
      raise warning 'Sequence %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_seq);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_class'::regclass
      and d.objoid = v_seq_params."oid";

    -- Result
    qry := 'COMMENT ON SEQUENCE ' || quote_ident(v_seq_params.nspname) || '.' || quote_ident(in_seq)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_sequence_comment(in_seq text, in_schema text)
    is 'Generates COMMENT command for a sequence by its name and schema (default - search_path)';


-- OID

create or replace function get_sequence_comment
  (
    seq_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_sequence_comment(d.objoid)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.relkind = 'S'
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_seq_params record;

  begin

    -- Checking existence of sequence
    select
      c.relname,
      n.nspname
    into v_seq_params
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where
      c.relkind = 'S'
      and c."oid" = seq_oid;

    if v_seq_params.relname is null then
      raise warning 'Sequence with OID % does not exist', seq_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_class'::regclass
      and d.objoid = seq_oid;

    -- Result
    qry := 'COMMENT ON SEQUENCE ' || quote_ident(v_seq_params.nspname) || '.' || quote_ident(v_seq_params.relname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_sequence_comment(seq_oid oid)
    is 'Generates COMMENT command for a sequence by its OID';