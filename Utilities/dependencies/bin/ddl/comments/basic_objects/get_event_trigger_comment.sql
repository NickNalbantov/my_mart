-- Examples:

-- select get_event_trigger_comment(s.evtname),
--        get_event_trigger_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_event_trigger s on s.tableoid = d.classoid and s."oid" = d.objoid;


-- Name

create or replace function get_event_trigger_comment
  (
    in_et text
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_event_trigger_comment(s.evtname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_event_trigger s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_oid oid;

  begin

    -- Checking existence of event trigger
    select "oid"
    into v_oid
    from pg_catalog.pg_event_trigger
    where evtname = in_et;

    if v_oid is null then
      raise warning 'Event trigger % does not exist', quote_ident(in_et);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_event_trigger'::regclass
      and d.objoid = v_oid;

    -- Result
    qry := 'COMMENT ON EVENT TRIGGER ' || quote_ident(in_et) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_event_trigger_comment(in_et text)
    is 'Generates COMMENT command for an event trigger by its name';


-- OID

create or replace function get_event_trigger_comment
  (
    et_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_event_trigger_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_event_trigger'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    in_et text;

  begin

    -- Checking existence of event trigger
    select evtname
    into in_et
    from pg_catalog.pg_event_trigger
    where "oid" = et_oid;

    if in_et is null then
      raise warning 'Event trigger with OID % does not exist', et_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_event_trigger'::regclass
      and d.objoid = et_oid;

    -- Result
    qry := 'COMMENT ON EVENT TRIGGER ' || quote_ident(in_et) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_event_trigger_comment(et_oid oid)
    is 'Generates COMMENT command for an event trigger by its OID';