-- Examples:

-- select get_view_comment(c.relname, n.nspname),
--        get_view_comment(c."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class c on c.tableoid = d.classoid and c."oid" = d.objoid
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where c.relkind in ('v', 'm');

-- select get_view_comment('pg_settings');


-- Name + schema

create or replace function get_view_comment
  (
    in_view text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Generates COMMENT command for views and materialized views

-- Examples:

-- select get_view_comment(s.relname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.relkind in ('v', 'm')
--       and s.relnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_view_params record;

  begin

    -- Checking existence of view
    select
      c."oid",
      c.relkind,
      n.nspname
    into v_view_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      c.relkind in ('v', 'm')
      and c.relname = in_view
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_view_params."oid" is null then
      raise warning 'View / materialized view %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_view);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_class'::regclass
      and d.objoid = v_view_params."oid";

    -- Result
    qry := 'COMMENT ON' || case when v_view_params.relkind = 'm' then ' MATERIALIZED ' else ' ' end || 'VIEW '
           || quote_ident(v_view_params.nspname) || '.' || quote_ident(in_view)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_view_comment(in_view text, in_schema text)
    is 'Generates COMMENT command for a view by its name and schema (default - search_path)';


-- OID

create or replace function get_view_comment
  (
    view_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Generates COMMENT command for views and materialized views

-- Examples:

-- select get_view_comment(d.objoid)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.relkind in ('v', 'm')
--       and s.relnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_view_params record;

  begin

    -- Checking existence of view
    select
      c.relname,
      c.relkind,
      n.nspname
    into v_view_params
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
    where c."oid" = view_oid;

    if v_view_params.relname is null then
      raise warning 'View / materialized view with OID % does not exist', view_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_class'::regclass
      and d.objoid = view_oid;

    -- Result
    qry := 'COMMENT ON' || case when v_view_params.relkind = 'm' then ' MATERIALIZED ' else ' ' end || 'VIEW '
          || quote_ident(v_view_params.nspname) || '.' || quote_ident(v_view_params.relname)
          || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_view_comment(view_oid oid)
    is 'Generates COMMENT command for a view by its OID';