-- Example:

-- select s."oid",
--       get_large_object_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_largeobject_metadata s on s."oid" = d.objoid
-- where d.classoid = 'pg_catalog.pg_largeobject'::regclass;


-- OID

create or replace function get_large_object_comment
  (
    lo_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select "oid",
--        get_large_object_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_largeobject_metadata s on s."oid" = d.objoid
-- where d.classoid = 'pg_catalog.pg_largeobject'::regclass;
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_oid oid;

  begin

    -- Checking existence of large object ACL
    select "oid"
    into v_oid
    from pg_catalog.pg_largeobject_metadata
    where "oid" = lo_oid;

    if v_oid is null then
      raise warning 'Large object with OID % does not exist', lo_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_largeobject'::regclass
      and d.objoid = lo_oid;

    -- Result
    qry := 'COMMENT ON LARGE OBJECT ' || lo_oid || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_large_object_comment(lo_oid oid)
    is 'Generates COMMENT command for a large object by its OID';