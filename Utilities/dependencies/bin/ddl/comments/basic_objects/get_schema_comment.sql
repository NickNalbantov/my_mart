-- Examples:

-- select get_schema_comment(s.nspname),
--        get_schema_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_namespace s on s.tableoid = d.classoid and s."oid" = d.objoid;


-- Name

create or replace function get_schema_comment
  (
    in_schema text
  )
returns text
language plpgsql
strict
as
$$

--Example:

-- select get_schema_comment(s.nspname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_namespace s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_oid oid;

  begin

    -- Checking existence of schema
    select n."oid"
    into v_oid
    from pg_catalog.pg_namespace n
    where n.nspname = in_schema;

    if v_oid is null then
      raise warning 'Schema % does not exist', quote_ident(in_schema);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_namespace'::regclass
      and d.objoid = v_oid;

    -- Result
    qry := 'COMMENT ON SCHEMA ' || quote_ident(in_schema) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_schema_comment(in_schema text)
    is 'Generates COMMENT command for a schema by its name';


-- OID

create or replace function get_schema_comment
  (
    schema_oid oid
  )
returns text
language plpgsql
strict
as
$$

--Examples:

-- select get_schema_comment('pg_catalog'::regnamespace);

-- select get_schema_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_namespace'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_name text;

  begin

    -- Checking existence of schema
    select n.nspname
    into v_name
    from pg_catalog.pg_namespace n
    where n."oid" = schema_oid;

    if v_name is null then
      raise warning 'Schema with OID % does not exist', schema_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_namespace'::regclass
      and d.objoid = schema_oid;

    -- Result
    qry := 'COMMENT ON SCHEMA ' || quote_ident(v_name) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_schema_comment(schema_oid oid)
    is 'Generates COMMENT command for a schema by its OID';