-- Examples:

-- select get_type_comment(s.typname, n.nspname),
--        get_type_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_type s on s.tableoid = d.classoid and s."oid" = d.objoid
-- join pg_catalog.pg_namespace n on s.typnamespace = n."oid";

-- select get_type_comment('integer');


-- Name + schema

create or replace function get_type_comment
  (
    in_type text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Generates COMMENT command for all kind of types (DROP TYPE...) and domains (DROP DOMAIN...)

-- Examples:

-- select get_type_comment('integer');

-- select get_type_comment(s.typname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_type s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.typnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_type_params record;
    in_type_fixed text;

    int_syn text[] := array['integer', 'int'];
    smallint_syn text := 'smallint';
    bigint_syn text := 'bigint';
    num_syn text := 'decimal';
    real_syn text := 'real';
    dp_syn text[] := array['double precision', 'float'];
    time_syn text := 'time without time zone';
    timetz_syn text := 'time with time zone';
    timestamp_syn text := 'timestamp without time zone';
    timestamptz_syn text := 'timestamp with time zone';
    bpchar_syn text := 'character';
    varchar_syn text[] := array['character varying', 'char varying'];
    varbit_syn text := 'bit varying';
    bool_syn text := 'boolean';

  begin

    -- Catching synonyms for type
    select case when right(tp, 2) = '[]' then rtrim('_' || tp, '[]')
           else tp end
    into in_type_fixed
    from
    (
      select
        case when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = any(int_syn) then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1int4\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = smallint_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1int2\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = bigint_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1int8\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = num_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1numeric\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = real_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1float4\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = any(dp_syn) then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1float8\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = time_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1time\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = timetz_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1timetz\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = timestamp_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1timestamp\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = timestamptz_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1timestamptz\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = bpchar_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1bpchar\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = any(varchar_syn) then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1varchar\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = varbit_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1varbit\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = bool_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1bool\2')
        else in_type end as tp
    ) a;

    -- Checking existence of type
    select
      t."oid",
      t.typtype, -- type class: normal, composite, enum, range, multirange etc.
      n.nspname
    into v_type_params
    from pg_catalog.pg_type t
    left join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      in_type_fixed = t.typname
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_type_params."oid" is null then
      raise warning 'Type %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_type);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_type'::regclass
      and d.objoid = v_type_params."oid";

    -- Result
    qry := 'COMMENT ON '
           || case when v_type_params.typtype = 'd' then 'DOMAIN ' else 'TYPE ' end
           || quote_ident(v_type_params.nspname) || '.' || quote_ident(in_type_fixed)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_type_comment(in_type text, in_schema text)
    is 'Generates COMMENT command for a type by its name and schema (default - search_path)';


-- OID

create or replace function get_type_comment
  (
    type_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Generates COMMENT command for all kind of types (DROP TYPE...) and domains (DROP DOMAIN...)

-- Examples:

-- select get_type_comment('integer'::regtype);

-- select get_type_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_type'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_type_params record;

  begin

    -- Checking existence of type
    select
      t."oid",
      t.typname,
      n.nspname,
      t.typtype -- type class: normal, composite, enum, range, multirange etc.
    into v_type_params
    from pg_catalog.pg_type t
    join pg_catalog.pg_namespace n on n.oid = t.typnamespace
    where type_oid = t."oid";

    if v_type_params.typname is null then
      raise warning 'Type with OID % does not exist', type_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_type'::regclass
      and d.objoid = type_oid;

    -- Result
    qry := 'COMMENT ON '
           || case when v_type_params.typtype = 'd' then 'DOMAIN ' else 'TYPE ' end
           || quote_ident(v_type_params.nspname) || '.' || quote_ident(v_type_params.typname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_type_comment(type_oid oid)
    is 'Generates COMMENT command for a type by its OID';