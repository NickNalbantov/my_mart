-- Examples:

-- select get_operator_class_comment(o.opcname, a.amname, n.nspname),
--        get_operator_class_comment(o."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_opclass o on o.tableoid = d.classoid and o."oid" = d.objoid
-- join pg_catalog.pg_am a on o.opcmethod = a."oid"
-- join pg_catalog.pg_namespace n on n."oid" = o.opcnamespace;

-- select get_operator_class_comment('text_ops')
-- union all
-- select get_operator_class_comment('text_ops', 'hash');


-- Name + schema + access method

create or replace function get_operator_class_comment
  (
    in_opc text,
    in_method text default 'btree',
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_operator_class_comment('text_ops')
-- union all
-- select get_operator_class_comment('text_ops', 'hash');

  declare

    qry text;
    v_comment text;
    v_opc_params record;

  begin

    -- Checking existence of operator class
    select
      o."oid",
      n.nspname
    into v_opc_params
    from pg_catalog.pg_opclass o
    join pg_catalog.pg_am a on o.opcmethod = a."oid"
    left join pg_catalog.pg_namespace n on n."oid" = o.opcnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      o.opcname = in_opc
      and a.amname = in_method
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_opc_params."oid" is null then
      raise warning 'Operator class %.% with access method % does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_opc),
        quote_ident(in_method);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_opclass'::regclass
      and d.objoid = v_opc_params."oid";

    -- Result
    qry := 'COMMENT ON OPERATOR CLASS ' || quote_ident(v_opc_params.nspname) || '.' || quote_ident(in_opc)
           || ' USING ' || quote_ident(in_method) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_operator_class_comment(in_opc text, in_schema text, in_method text)
    is 'Generates COMMENT command for an operator class by its name and schema (default - search_path) + used access method (def ''btree'')';


-- OID

create or replace function get_operator_class_comment
  (
    opc_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_operator_class_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_opclass'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_opc_params record;
    v_access_method text;

  begin

    -- Checking existence of operator class
    select
      o.opcname,
      n.nspname,
      o.opcmethod
    into v_opc_params
    from pg_catalog.pg_opclass o
    join pg_catalog.pg_namespace n on n."oid" = o.opcnamespace
    where o."oid" = opc_oid;

    if v_opc_params.opcname is null then
      raise warning 'Operator class with OID % does not exist', opc_oid;

      return null;
    end if;

    -- Access method
    select amname::text
    into v_access_method
    from pg_catalog.pg_am
    where v_opc_params.opcmethod = "oid";

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_opclass'::regclass
      and d.objoid = opc_oid;

    -- Result
    qry := 'COMMENT ON OPERATOR CLASS ' || quote_ident(v_opc_params.nspname) || '.' || quote_ident(v_opc_params.opcname)
           || ' USING ' || quote_ident(v_access_method) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_operator_class_comment(opc_oid oid)
    is 'Generates COMMENT command for an operator class by its OID';