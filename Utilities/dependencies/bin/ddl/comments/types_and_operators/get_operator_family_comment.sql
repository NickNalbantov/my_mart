-- Examples:

-- select get_operator_family_comment(o.opfname, a.amname, n.nspname),
--        get_operator_family_comment(o."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_opfamily o on o.tableoid = d.classoid and o."oid" = d.objoid
-- join pg_catalog.pg_am a on o.opfmethod = a."oid"
-- join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace;

-- select get_operator_family_comment('text_ops')
-- union all
-- select get_operator_family_comment('text_ops', 'hash');


-- Name + schema + access method

create or replace function get_operator_family_comment
  (
    in_opf text,
    in_method text default 'btree',
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_operator_family_comment('text_ops')
-- union all
-- select get_operator_family_comment('text_ops', 'hash');

  declare

    qry text;
    v_comment text;
    v_opf_params record;

  begin

    -- Checking existence of operator family
    select
      o."oid",
      n.nspname
    into v_opf_params
    from pg_catalog.pg_opfamily o
    join pg_catalog.pg_am a on a."oid" = o.opfmethod
    left join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      o.opfname = in_opf
      and a.amname = in_method
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_opf_params."oid" is null then
      raise warning 'Operator family %.% with access method % does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_opf),
        quote_ident(in_method);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_opfamily'::regclass
      and d.objoid = v_opf_params."oid";

    -- Result
    qry := 'COMMENT ON OPERATOR FAMILY ' || quote_ident(v_opf_params.nspname) || '.' || quote_ident(in_opf) ||
           ' USING ' || quote_ident(in_method) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_operator_family_comment(in_opf text, in_method text, in_schema text)
    is 'Generates COMMENT command for an operator family by its name and schema (default - search_path) + used access method (def ''btree'')';


-- OID

create or replace function get_operator_family_comment
  (
    opf_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_operator_family_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_opfamily'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_opf_params record;
    v_access_method text;

  begin

    -- Checking existence of operator family
    select
      o.opfname,
      n.nspname,
      o.opfmethod
    into v_opf_params
    from pg_catalog.pg_opfamily o
    join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace
    where o."oid" = opf_oid;

    if v_opf_params.opfname is null then
      raise warning 'Operator family with OID % does not exist', opf_oid;

      return null;
    end if;

    -- Access method
    select amname::text
    into v_access_method
    from pg_catalog.pg_am
    where v_opf_params.opfmethod = "oid";

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_opfamily'::regclass
      and d.objoid = opf_oid;

    -- Result
    qry := 'COMMENT ON OPERATOR FAMILY ' || quote_ident(v_opf_params.nspname) || '.' || quote_ident(v_opf_params.opfname) ||
           ' USING ' || quote_ident(v_access_method) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_operator_family_comment(opf_oid oid)
    is 'Generates COMMENT command for an operator family by its OID';