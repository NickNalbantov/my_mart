-- Examples:

-- select get_access_method_comment(s.amname),
--        get_access_method_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_am s on s.tableoid = d.classoid and s."oid" = d.objoid;


-- Name

create or replace function get_access_method_comment
  (
    in_am text
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_access_method_comment(s.amname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_am s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    v_oid oid;
    qry text;
    v_comment text;

  begin

    -- Checking existence of access method
    select "oid"
    into v_oid
    from pg_catalog.pg_am
    where amname = in_am;

    if v_oid is null then
      raise warning 'Access method % does not exist', quote_ident(in_am);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_am'::regclass
      and d.objoid = v_oid;

    -- Result
    qry := 'COMMENT ON ACCESS METHOD ' || quote_ident(in_am) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_access_method_comment(in_am text)
    is 'Generates COMMENT command for an access method by its name';


-- OID

create or replace function get_access_method_comment
  (
    am_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_access_method_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_am s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    v_name text;
    qry text;
    v_comment text;

  begin

    -- Checking existence of access method
    select amname
    into v_name
    from pg_catalog.pg_am
    where "oid" = am_oid;

    if v_name is null then
      raise warning 'Access method with OID % does not exist', am_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_am'::regclass
      and d.objoid = am_oid;

    -- Result
    qry := 'COMMENT ON ACCESS METHOD ' || quote_ident(v_name) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_access_method_comment(am_oid oid)
    is 'Generates COMMENT command for an access method by its OID';