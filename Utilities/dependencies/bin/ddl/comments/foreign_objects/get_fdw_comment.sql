-- Examples:

-- select get_fdw_comment(s.fdwname),
--        get_fdw_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_foreign_data_wrapper s on s.tableoid = d.classoid and s."oid" = d.objoid;


-- Name

create or replace function get_fdw_comment
  (
    in_fdw text
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_fdw_comment(s.fdwname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_foreign_data_wrapper s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_oid oid;

  begin

    -- Checking existence of FDW
    select "oid"
    into v_oid
    from pg_catalog.pg_foreign_data_wrapper
    where fdwname = in_fdw;

    if v_oid is null then
      raise warning 'Foreign data wrapper % does not exist', quote_ident(in_fdw);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_foreign_data_wrapper'::regclass
      and d.objoid = v_oid;

    -- Result
    qry := 'COMMENT ON FOREIGN DATA WRAPPER ' || quote_ident(in_fdw) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_fdw_comment(in_fdw text)
    is 'Generates COMMENT command for a foreign data wrapper by its name';


-- OID

create or replace function get_fdw_comment
  (
    fdw_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_fdw_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_foreign_data_wrapper'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    in_fdw text;

  begin

    -- Checking existence of FDW
    select fdwname
    into in_fdw
    from pg_catalog.pg_foreign_data_wrapper
    where "oid" = fdw_oid;

    if in_fdw is null then
      raise warning 'Foreign data wrapper with OID % does not exist', fdw_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_foreign_data_wrapper'::regclass
      and d.objoid = fdw_oid;

    -- Result
    qry := 'COMMENT ON FOREIGN DATA WRAPPER ' || quote_ident(in_fdw) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_fdw_comment(fdw_oid oid)
    is 'Generates COMMENT command for a foreign data wrapper by its OID';