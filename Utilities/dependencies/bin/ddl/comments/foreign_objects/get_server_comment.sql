-- Examples:

-- select get_server_comment(s.srvname),
--        get_server_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_foreign_server s on s.tableoid = d.classoid and s."oid" = d.objoid;


-- Name

create or replace function get_server_comment
  (
    in_srv text
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_server_comment(s.srvname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_foreign_server s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_oid oid;

  begin

    -- Checking existence of foreign server
    select "oid"
    into v_oid
    from pg_catalog.pg_foreign_server
    where srvname = in_srv;

    if v_oid is null then
      raise warning 'Foreign server % does not exist', quote_ident(in_srv);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_foreign_server'::regclass
      and d.objoid = v_oid;

    -- Result
    qry := 'COMMENT ON SERVER ' || quote_ident(in_srv) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_server_comment(in_srv text)
    is 'Generates COMMENT command for a foreign server by its name';


-- OID

create or replace function get_server_comment
  (
    srv_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_server_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_foreign_server'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    in_srv text;

  begin

    -- Checking existence of foreign server
    select srvname
    into in_srv
    from pg_catalog.pg_foreign_server
    where "oid" = srv_oid;

    if in_srv is null then
      raise warning 'Foreign server with OID % does not exist', srv_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_foreign_server'::regclass
      and d.objoid = srv_oid;

    -- Result
    qry := 'COMMENT ON SERVER ' || quote_ident(in_srv) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_server_comment(srv_oid oid)
    is 'Generates COMMENT command for a foreign server by its OID';