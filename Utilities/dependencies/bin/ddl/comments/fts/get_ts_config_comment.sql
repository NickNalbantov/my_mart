-- Examples:

-- select get_ts_config_comment(s.cfgname, n.nspname),
--        get_ts_config_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_ts_config s on s.tableoid = d.classoid and s."oid" = d.objoid
-- join pg_catalog.pg_namespace n on n."oid" = s.cfgnamespace;

-- select get_ts_config_comment('german');


-- Name + schema

create or replace function get_ts_config_comment
  (
    in_tscfg text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Examples:

-- select get_ts_config_comment('german');

-- select get_ts_config_comment(s.cfgname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_ts_config s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.cfgnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_tscfg_params record;

  begin

    -- Checking existence of FTS configuration
    select
      c."oid",
      n.nspname
    into v_tscfg_params
    from pg_catalog.pg_ts_config c
    left join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      c.cfgname = in_tscfg
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tscfg_params."oid" is null then
      raise warning 'Text search configuration %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tscfg);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_ts_config'::regclass
      and d.objoid = v_tscfg_params."oid";

    -- Result
    qry := 'COMMENT ON TEXT SEARCH CONFIGURATION ' || quote_ident(v_tscfg_params.nspname) || '.' || quote_ident(in_tscfg)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_ts_config_comment(in_tscfg text, in_schema text)
    is 'Generates COMMENT command for a FTS configuration by its name and schema (default - search_path)';


-- OID

create or replace function get_ts_config_comment
  (
    tscfg_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Examples:

-- select get_ts_config_comment('german'::regconfig);

-- select get_ts_config_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_ts_config'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_tscfg_params record;

  begin

    -- Checking existence of FTS configuration
    select n.nspname,
           c.cfgname
    into v_tscfg_params
    from pg_catalog.pg_ts_config c
    join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace
    where c."oid" = tscfg_oid;

    if v_tscfg_params.cfgname is null then
      raise warning 'Text search configuration with OID % does not exist', tscfg_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_ts_config'::regclass
      and d.objoid = tscfg_oid;

    -- Result
    qry := 'COMMENT ON TEXT SEARCH CONFIGURATION ' || quote_ident(v_tscfg_params.nspname) || '.' || quote_ident(v_tscfg_params.cfgname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_ts_config_comment(tscfg_oid oid)
    is 'Generates COMMENT command for a FTS configuration by its OID';