-- Examples:

-- select get_ts_parser_comment(s.prsname, n.nspname),
--        get_ts_parser_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_ts_parser s on s.tableoid = d.classoid and s."oid" = d.objoid
-- join pg_catalog.pg_namespace n on n."oid" = s.prsnamespace;

-- select get_ts_parser_comment('default');


-- Name + schema

create or replace function get_ts_parser_comment
  (
    in_tsprs text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Examples:

-- select get_ts_parser_comment('default');

-- select get_ts_parser_comment(s.prsname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_ts_parser s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.prsnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_tsprs_params record;

  begin

    -- Checking existence of FTS parser
    select
      t."oid",
      n.nspname
    into v_tsprs_params
    from pg_catalog.pg_ts_parser t
    left join pg_catalog.pg_namespace n on n."oid" = t.prsnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      t.prsname = in_tsprs
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tsprs_params."oid" is null then
      raise warning 'Text search parser %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tsprs);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_ts_parser'::regclass
      and d.objoid = v_tsprs_params."oid";

    -- Result
    qry := 'COMMENT ON TEXT SEARCH PARSER ' || quote_ident(v_tsprs_params.nspname) || '.' || quote_ident(in_tsprs)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_ts_parser_comment(in_tsprs text, in_schema text)
    is 'Generates COMMENT command for a FTS parser by its name and schema (default - search_path)';


-- OID

create or replace function get_ts_parser_comment
  (
    tsprs_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_ts_parser_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_ts_parser'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_tsprs_params record;

  begin

    -- Checking existence of FTS parser
    select
      n.nspname,
      t.prsname
    into v_tsprs_params
    from pg_catalog.pg_ts_parser t
    join pg_catalog.pg_namespace n on n."oid" = t.prsnamespace
    where t."oid" = tsprs_oid;

    if v_tsprs_params.prsname is null then
      raise warning 'Text search parser with OID % does not exist', tsprs_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_ts_parser'::regclass
      and d.objoid = tsprs_oid;

    -- Result
    qry := 'COMMENT ON TEXT SEARCH PARSER ' || quote_ident(v_tsprs_params.nspname) || '.' || quote_ident(v_tsprs_params.prsname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_ts_parser_comment(tsprs_oid oid)
    is 'Generates COMMENT command for a FTS parser by its OID';