-- Examples:

-- select get_ts_template_comment(s.tmplname, n.nspname),
--        get_ts_template_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_ts_template s on s.tableoid = d.classoid and s."oid" = d.objoid
-- join pg_catalog.pg_namespace n on n."oid" = s.tmplnamespace;

-- select get_ts_template_comment('synonym');


-- Name + schema

create or replace function get_ts_template_comment
  (
    in_tstmpl text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Examples:

-- select get_ts_template_comment('synonym');

-- select get_ts_template_comment(s.tmplname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_ts_template s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.tmplnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_tstmpl_params record;

  begin

    -- Checking existence of FTS template
    select
      t."oid",
      n.nspname
    into v_tstmpl_params
    from pg_catalog.pg_ts_template t
    left join pg_catalog.pg_namespace n on n."oid" = t.tmplnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      t.tmplname = in_tstmpl
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tstmpl_params."oid" is null then
      raise warning 'Text search template %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tstmpl);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_ts_template'::regclass
      and d.objoid = v_tstmpl_params."oid";

    -- Result
    qry := 'COMMENT ON TEXT SEARCH TEMPLATE ' || quote_ident(v_tstmpl_params.nspname) || '.' || quote_ident(in_tstmpl)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_ts_template_comment(in_tstmpl text, in_schema text)
    is 'Generates COMMENT command for a FTS template by its name and schema (default - search_path)';


-- OID

create or replace function get_ts_template_comment
  (
    tstmpl_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_ts_template_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_ts_template'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_tstmpl_params record;

  begin

    -- Checking existence of FTS template
    select
      n.nspname,
      t.tmplname
    into v_tstmpl_params
    from pg_catalog.pg_ts_template t
    join pg_catalog.pg_namespace n on n."oid" = t.tmplnamespace
    where t."oid" = tstmpl_oid;

    if v_tstmpl_params.tmplname is null then
      raise warning 'Text search template with OID % does not exist', tstmpl_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_ts_template'::regclass
      and d.objoid = tstmpl_oid;

    -- Result
    qry := 'COMMENT ON TEXT SEARCH TEMPLATE ' || quote_ident(v_tstmpl_params.nspname) || '.' || quote_ident(v_tstmpl_params.tmplname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_ts_template_comment(tstmpl_oid oid)
    is 'Generates COMMENT command for a FTS template by its OID';