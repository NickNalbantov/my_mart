-- Examples:

-- select get_ts_dictionary_comment(s.dictname, n.nspname),
--        get_ts_dictionary_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_ts_dict s on s.tableoid = d.classoid and s."oid" = d.objoid
-- join pg_catalog.pg_namespace n on n."oid" = s.dictnamespace;

-- select get_ts_dictionary_comment('german_stem');


-- Name + schema

create or replace function get_ts_dictionary_comment
  (
    in_tsdict text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Examples:

-- select get_ts_dictionary_comment('german_stem');

-- select get_ts_dictionary_comment(s.dictname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_ts_dict s on s.tableoid = d.classoid and s."oid" = d.objoid
-- where s.dictnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_tsdict_params record;

  begin

    -- Checking existence of FTS dictionary
    select
      d."oid",
      n.nspname
    into v_tsdict_params
    from pg_catalog.pg_ts_dict d
    left join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where
      d.dictname = in_tsdict
      and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tsdict_params."oid" is null then
      raise warning 'Text search dictionary %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tsdict);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_ts_dict'::regclass
      and d.objoid = v_tsdict_params."oid";

    -- Result
    qry := 'COMMENT ON TEXT SEARCH DICTIONARY ' || quote_ident(v_tsdict_params.nspname) || '.' || quote_ident(in_tsdict)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_ts_dictionary_comment(in_tsdict text, in_schema text)
    is 'Generates COMMENT command for a FTS dictionary by its name and schema (default - search_path)';


-- OID

create or replace function get_ts_dictionary_comment
  (
    tsdict_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Examples:

-- select get_ts_dictionary_comment('german_stem'::regdictionary);

-- select get_ts_dictionary_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_ts_dict'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_tsdict_params record;

  begin

    -- Checking existence of FTS dictionary
    select
      n.nspname,
      d.dictname
    into v_tsdict_params
    from pg_catalog.pg_ts_dict d
    join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
    where d."oid" = tsdict_oid;

    if v_tsdict_params.dictname is null then
      raise warning 'Text search dictionary with OID % does not exist', tsdict_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_ts_dict'::regclass
      and d.objoid = tsdict_oid;

    -- Result
    qry := 'COMMENT ON TEXT SEARCH DICTIONARY ' || quote_ident(v_tsdict_params.nspname) || '.' || quote_ident(v_tsdict_params.dictname)
           || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_ts_dictionary_comment(tsdict_oid oid)
    is 'Generates COMMENT command for a FTS dictionary by its OID';