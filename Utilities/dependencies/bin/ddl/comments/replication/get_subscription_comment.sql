-- Examples:

-- select get_subscription_comment(s.subname),
--        get_subscription_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_subscription s on s.tableoid = d.classoid and s."oid" = d.objoid;


-- Name

create or replace function get_subscription_comment
  (
    in_sub text
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_subscription_comment(s.subname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_subscription s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_oid oid;

  begin

    -- Checking existence of subscription
    select "oid"
    into v_oid
    from pg_catalog.pg_subscription
    where subname = in_sub;

    if v_oid is null then
      raise warning 'Subscription % does not exist', quote_ident(in_sub);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_subscription'::regclass
      and d.objoid = v_oid;

    -- Result
    qry := 'COMMENT ON SUBSCRIPTION ' || quote_ident(in_sub) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_subscription_comment(in_sub text)
    is 'Generates COMMENT command for a subscription by its name';


-- OID

create or replace function get_subscription_comment
  (
    sub_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_subscription_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_subscription'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_subname text;

  begin

    -- Checking existence of subscription
    select subname
    into v_subname
    from pg_catalog.pg_subscription
    where "oid" = sub_oid;

    if v_subname is null then
      raise warning 'Subscription with OID % does not exist', sub_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_subscription'::regclass
      and d.objoid = sub_oid;

    -- Result
    qry := 'COMMENT ON SUBSCRIPTION ' || quote_ident(v_subname) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_subscription_comment(sub_oid oid)
    is 'Generates COMMENT command for a subscription by its OID';