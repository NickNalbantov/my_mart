-- Examples:

-- select get_publication_comment(s.pubname),
--        get_publication_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_publication s on s.tableoid = d.classoid and s."oid" = d.objoid;


-- Name

create or replace function get_publication_comment
  (
    in_pub text
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_publication_comment(s.pubname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_publication s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_oid oid;

  begin

    -- Checking existence of publication
    select "oid"
    into v_oid
    from pg_catalog.pg_publication
    where pubname = in_pub;

    if v_oid is null then
      raise warning 'Publication % does not exist', quote_ident(in_pub);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_publication'::regclass
      and d.objoid = v_oid;

    -- Result
    qry := 'COMMENT ON PUBLICATION ' || quote_ident(in_pub) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_publication_comment(in_pub text)
    is 'Generates COMMENT command for a publication by its name';


-- OID

create or replace function get_publication_comment
  (
    pub_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_publication_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_publication'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_pubname text;

  begin

    -- Checking existence of publication
    select pubname
    into v_pubname
    from pg_catalog.pg_publication
    where "oid" = pub_oid;

    if v_pubname is null then
      raise warning 'Publication with OID % does not exist', pub_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_publication'::regclass
      and d.objoid = pub_oid;

    -- Result
    qry := 'COMMENT ON PUBLICATION ' || quote_ident(v_pubname) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_publication_comment(pub_oid oid)
    is 'Generates COMMENT command for a publication by its OID';