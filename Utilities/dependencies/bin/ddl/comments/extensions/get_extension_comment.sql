-- Examples:

-- select get_extension_comment(s.extname),
--        get_extension_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_extension s on s.tableoid = d.classoid and s."oid" = d.objoid;


-- Name

create or replace function get_extension_comment
  (
    in_ext text
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_extension_comment(s.extname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_extension s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_oid oid;

  begin

    -- Checking existence of extension
    select "oid"
    into v_oid
    from pg_catalog.pg_extension
    where extname = in_ext;

    if v_oid is null then
      raise warning 'Extension % does not exist', quote_ident(in_ext);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_extension'::regclass
      and d.objoid = v_oid;

    -- Result
    qry := 'COMMENT ON EXTENSION ' || quote_ident(in_ext) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_extension_comment(in_ext text)
    is 'Generates COMMENT command for an extension by its name';


-- OID

create or replace function get_extension_comment
  (
    ext_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_extension_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_extension'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    in_ext text;

  begin

    -- Checking existence of extension
    select extname
    into in_ext
    from pg_catalog.pg_extension
    where "oid" = ext_oid;

    if in_ext is null then
      raise warning 'Extension with OID % does not exist', ext_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_extension'::regclass
      and d.objoid = ext_oid;

    -- Result
    qry := 'COMMENT ON EXTENSION ' || quote_ident(in_ext) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_extension_comment(ext_oid oid)
    is 'Generates COMMENT command for an extension by its OID';