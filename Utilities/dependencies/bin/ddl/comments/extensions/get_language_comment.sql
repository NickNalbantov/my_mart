-- Examples:

-- select get_language_comment(s.lanname),
--        get_language_comment(s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_language s on s.tableoid = d.classoid and s."oid" = d.objoid;


-- Name

create or replace function get_language_comment
  (
    in_lang text
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_language_comment(s.lanname)
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_language s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_comment text;
    v_oid oid;

  begin

    -- Checking existence of language
    select "oid"
    into v_oid
    from pg_catalog.pg_language
    where lanname = in_lang;

    if v_oid is null then
      raise warning 'Language % does not exist', quote_ident(in_lang);

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_language'::regclass
      and d.objoid = v_oid;

    -- Result
    qry := 'COMMENT ON LANGUAGE ' || quote_ident(in_lang) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_language_comment(in_lang text)
    is 'Generates COMMENT command for a language by its name';


-- OID

create or replace function get_language_comment
  (
    lang_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Same command can be obtained with get_object_comment function as well

-- Example:

-- select get_language_comment(objoid)
-- from pg_catalog.pg_description
-- where classoid = 'pg_catalog.pg_language'::regclass
-- limit 10;

  declare

    qry text;
    v_comment text;
    in_lang text;

  begin

    -- Checking existence of language
    select lanname
    into in_lang
    from pg_catalog.pg_language
    where "oid" = lang_oid;

    if in_lang is null then
      raise warning 'Language with OID % does not exist', lang_oid;

      return null;
    end if;

    -- Getting the comment
    select d."description"
    into v_comment
    from pg_catalog.pg_description d
    where
      d.classoid = 'pg_catalog.pg_language'::regclass
      and d.objoid = lang_oid;

    -- Result
    qry := 'COMMENT ON LANGUAGE ' || quote_ident(in_lang) || E'\n    IS ' || quote_literal(v_comment) || ';';

    return qry;

  end;

$$;

comment on function get_language_comment(lang_oid oid)
    is 'Generates COMMENT command for a language by its OID';