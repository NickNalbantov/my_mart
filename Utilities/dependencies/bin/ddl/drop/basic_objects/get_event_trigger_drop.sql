-- Examples:

-- select get_event_trigger_drop(evtname),
--        get_event_trigger_drop("oid", false)
-- from pg_catalog.pg_event_trigger;


-- Name

create or replace function get_event_trigger_drop
  (
    in_et text,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_event_trigger_drop(evtname)
-- from pg_catalog.pg_event_trigger
-- limit 10;

  declare

    qry text;
    v_oid oid;

  begin

    -- Checking existence of event trigger
    select "oid"
    into v_oid
    from pg_catalog.pg_event_trigger
    where evtname = in_et;

    if v_oid is null then
      raise warning 'Event trigger % does not exist', quote_ident(in_et);

      return null;
    end if;

    -- Result

    qry := 'DROP EVENT TRIGGER IF EXISTS ' || quote_ident(in_et) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_event_trigger_drop(in_et text, cascade_ bool)
    is 'Generates DROP command for an event trigger by its name; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_event_trigger_drop
  (
    et_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_event_trigger_drop("oid")
-- from pg_catalog.pg_event_trigger
-- limit 10;

  declare

    qry text;
    in_et text;

  begin

    -- Checking existence of event trigger
    select evtname
    into in_et
    from pg_catalog.pg_event_trigger
    where "oid" = et_oid;

    if in_et is null then
      raise warning 'Event trigger with OID % does not exist', et_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP EVENT TRIGGER IF EXISTS ' || quote_ident(in_et) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_event_trigger_drop(et_oid oid, cascade_ bool)
    is 'Generates DROP command for an event trigger by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';