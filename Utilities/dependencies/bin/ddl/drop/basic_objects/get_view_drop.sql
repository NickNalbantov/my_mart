-- Examples:

-- select get_view_drop(c.relname, n.nspname),
--        get_view_drop(c."oid", false)
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where c.relkind in ('v', 'm');

-- select get_view_drop('pg_settings');


-- Name + schema

create or replace function get_view_drop
  (
    in_view text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Generates DROP command for views and materialized views

-- Examples:

-- select get_view_drop('pg_settings');

-- select get_view_drop(relname)
-- from pg_catalog.pg_class
-- where relkind in ('v', 'm')
--       and relnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_view_params record;

  begin

    -- Checking existence of view
    select c."oid",
           c.relkind,
           n.nspname
    into v_view_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.relkind in ('v', 'm')
          and c.relname = in_view
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_view_params."oid" is null then
      raise warning 'View / materialized view %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_view);

      return null;
    end if;

    -- Result

    qry := 'DROP' || case when v_view_params.relkind = 'm' then ' MATERIALIZED ' else ' ' end || 'VIEW IF EXISTS '
           || quote_ident(v_view_params.nspname) || '.' || quote_ident(in_view)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_view_drop(in_view text, in_schema text, cascade_ bool)
    is 'Generates DROP command for a view by its name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_view_drop
  (
    view_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Generates DROP command for views and materialized views

-- Examples:

-- select get_view_drop('pg_settings');

-- select get_view_drop("oid")
-- from pg_catalog.pg_class
-- where relkind in ('v', 'm')
--       and relnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_view_params record;

  begin

    -- Checking existence of view
    select c.relname,
           c.relkind,
           n.nspname
    into v_view_params
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
    where c."oid" = view_oid;

    if v_view_params.relname is null then
      raise warning 'View / materialized view with OID % does not exist', view_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP' || case when v_view_params.relkind = 'm' then ' MATERIALIZED ' else ' ' end || 'VIEW IF EXISTS '
          || quote_ident(v_view_params.nspname) || '.' || quote_ident(v_view_params.relname)
          || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_view_drop(view_oid oid, cascade_ bool)
    is 'Generates DROP command for a view by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';