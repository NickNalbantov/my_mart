-- Example:

-- select "oid",
--        get_large_object_drop("oid")
-- from pg_catalog.pg_largeobject_metadata;


-- OID

create or replace function get_large_object_drop
  (
    lo_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select "oid",
--        get_large_object_drop("oid")
-- from pg_catalog.pg_largeobject_metadata
-- limit 10;

  declare

    qry text;
    v_oid oid;

  begin

    -- Checking existence of large object ACL
    select "oid"
    into v_oid
    from pg_catalog.pg_largeobject_metadata
    where "oid" = lo_oid;

    if v_oid is null then
      raise warning 'Large object with OID % does not exist', lo_oid;

      return null;
    end if;

    -- Result

    qry := 'SELECT lo_unlink(' || v_oid || ');';

    return qry;

  end;

$$;

comment on function get_large_object_drop(lo_oid oid)
    is 'Generates DROP command for a large object by its OID';