-- Examples:

-- select get_trigger_drop(t.tgname, c.relname, n.nspname),
--        get_trigger_drop(t."oid", false)
-- from pg_catalog.pg_trigger t
-- join pg_catalog.pg_class c on t.tgrelid = c."oid"
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace;

-- Name

create or replace function get_trigger_drop
  (
    in_trig text,
    in_parent text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

  declare

    qry text;
    v_trig_params record;

  begin

    -- Checking existence of trigger
    select t."oid",
           n.nspname
    into v_trig_params
    from pg_catalog.pg_trigger t
    join pg_catalog.pg_class c on t.tgrelid = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where t.tgname = in_trig
          and c.relname = in_parent
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_trig_params."oid" is null then
      raise warning 'Trigger % on %.% does not exist',
        quote_ident(in_trig),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_parent);

      return null;
    end if;

    -- Result

    qry := 'DROP TRIGGER IF EXISTS ' || quote_ident(in_trig) || ' ON ' || quote_ident(v_trig_params.nspname) || '.' || quote_ident(in_parent)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_trigger_drop(in_trig text, in_parent text, in_schema text, cascade_ bool)
    is 'Generates DROP command for a trigger by its name, name of the parent object and its schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_trigger_drop
  (
    trig_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_trigger_drop("oid")
-- from pg_catalog.pg_trigger
-- limit 10;

  declare

    qry text;
    v_trig_params record;

  begin

    -- Checking existence of trigger
    select t.tgname,
           c.relname,
           n.nspname
    into v_trig_params
    from pg_catalog.pg_trigger t
    join pg_catalog.pg_class c on t.tgrelid = c."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where t."oid" = trig_oid;

    if v_trig_params.tgname is null then
      raise warning 'Trigger with OID % does not exist', trig_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP TRIGGER IF EXISTS ' || quote_ident(v_trig_params.tgname) || ' ON ' || quote_ident(v_trig_params.nspname) || '.' || quote_ident(v_trig_params.relname)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_trigger_drop(trig_oid oid, cascade_ bool)
    is 'Generates DROP command for a trigger by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';