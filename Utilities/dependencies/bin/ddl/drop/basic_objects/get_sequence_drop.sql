-- Examples:

-- select get_sequence_drop(c.relname, n.nspname),
--        get_sequence_drop(c."oid", false)
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
-- where c.relkind = 'S';


-- Name + schema

create or replace function get_sequence_drop
  (
    in_seq text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

  declare

    qry text;
    v_seq_params record;

  begin

    -- Checking existence of sequence
    select c."oid",
           n.nspname
    into v_seq_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.relkind = 'S'
          and c.relname = in_seq
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_seq_params."oid" is null then
      raise warning 'Sequence %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_seq);

      return null;
    end if;

    -- Result
    qry := 'DROP SEQUENCE IF EXISTS ' || quote_ident(v_seq_params.nspname) || '.' || quote_ident(in_seq)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_sequence_drop(in_seq text, in_schema text, cascade_ bool)
    is 'Generates DROP command for a sequence by its name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_sequence_drop
  (
    seq_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_sequence_drop("oid")
-- from pg_catalog.pg_class
-- where relkind = 'S'
-- limit 10;

  declare

    qry text;
    v_seq_params record;

  begin

    -- Checking existence of sequence
    select c.relname,
           n.nspname
    into v_seq_params
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where c.relkind = 'S'
          and c."oid" = seq_oid;

    if v_seq_params.relname is null then
      raise warning 'Sequence with OID % does not exist', seq_oid;

      return null;
    end if;

    -- Result
    qry := 'DROP SEQUENCE IF EXISTS ' || quote_ident(v_seq_params.nspname) || '.' || quote_ident(v_seq_params.relname)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_sequence_drop(seq_oid oid, cascade_ bool)
    is 'Generates DROP command for a sequence by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';