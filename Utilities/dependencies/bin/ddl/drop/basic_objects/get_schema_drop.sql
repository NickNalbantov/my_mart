-- Examples:

-- select get_schema_drop(nspname),
--        get_schema_drop("oid", false)
-- from pg_catalog.pg_namespace;


-- Name

create or replace function get_schema_drop
  (
    in_schema text,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

--Example:

-- select get_schema_drop(nspname)
-- from pg_catalog.pg_namespace
-- limit 10;

  declare

    qry text;
    v_oid oid;

  begin

    -- Checking existence of schema
    select n."oid"
    into v_oid
    from pg_catalog.pg_namespace n
    where n.nspname = in_schema;

    if v_oid is null then
      raise warning 'Schema % does not exist', quote_ident(in_schema);

      return null;
    end if;

    -- Result

    qry := 'DROP SCHEMA IF EXISTS ' || quote_ident(in_schema) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_schema_drop(in_schema text, cascade_ bool)
    is 'Generates DROP command for a schema by its name; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_schema_drop
  (
    schema_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

--Examples:

-- select get_schema_drop('pg_catalog'::regnamespace);

-- select get_schema_drop("oid")
-- from pg_catalog.pg_namespace
-- limit 10;

  declare

    qry text;
    v_name text;

  begin

    -- Checking existence of schema
    select n.nspname
    into v_name
    from pg_catalog.pg_namespace n
    where n."oid" = schema_oid;

    if v_name is null then
      raise warning 'Schema with OID % does not exist', schema_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP SCHEMA IF EXISTS ' || quote_ident(v_name) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_schema_drop(schema_oid oid, cascade_ bool)
    is 'Generates DROP command for a schema by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';