-- Examples:

-- select get_policy_drop(p.polname, c.relname, n.nspname),
--        get_policy_drop(p."oid")
-- from pg_catalog.pg_policy p
-- join pg_catalog.pg_class c on p.polrelid = c."oid"
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace;


-- Policy name + table name + table schema

create or replace function get_policy_drop
  (
    in_pol text,
    in_tbl text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

  declare

    qry text;
    v_pol_params record;

  begin

    -- Checking existence of policy
    select p."oid",
           n.nspname
    into v_pol_params
    from pg_catalog.pg_policy p
    join pg_catalog.pg_class c on p.polrelid = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where p.polname = in_pol and c.relname = in_tbl
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_pol_params."oid" is null then
      raise warning 'Policy % for table %.% does not exist',
        quote_ident(in_pol),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tbl);

      return null;
    end if;

    -- Result
    qry := 'DROP POLICY IF EXISTS ' || quote_ident(in_pol) || ' ON ' || quote_ident(v_pol_params.nspname) || '.' || quote_ident(in_tbl) || ';';

    return qry;

  end;

$$;

comment on function get_policy_drop(in_pol text, in_tbl text, in_schema text)
    is 'Generates DROP command for a policy by its name and table''s name and schema (default - search_path)';


-- OID

create or replace function get_policy_drop
  (
    pol_oid oid
  )
returns text
language plpgsql
strict
as
$$

  declare

    qry text;
    v_pol_params record;
    v_pol_table text;

  begin

    -- Checking existence of policy
    select polname,
           polrelid
    into v_pol_params
    from pg_catalog.pg_policy
    where "oid" = pol_oid;

    if v_pol_params.polname is null then
      raise warning 'Policy with OID % does not exist', pol_oid;

      return null;
    end if;

    -- Table
    select quote_ident(n.nspname) || '.' || quote_ident(c.relname)
    into v_pol_table
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where c."oid" = v_pol_params.polrelid;

    -- Result
    qry := 'DROP POLICY IF EXISTS ' || quote_ident(v_pol_params.polname) || ' ON ' || v_pol_table || ';';

    return qry;

  end;

$$;

comment on function get_policy_drop(pol_oid oid)
    is 'Generates DROP command for a policy by its OID';