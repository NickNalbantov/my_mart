-- Examples:

-- select get_constraint_drop(c.conname, coalesce(cl.relname, t.typname), n.nspname),
--        get_constraint_drop(c."oid", false)
-- from pg_catalog.pg_constraint c
-- left join pg_catalog.pg_class cl on c.conrelid = cl."oid" and cl.relkind in ('r', 'p', 'f')
-- left join pg_catalog.pg_type t on c.contypid = t."oid" and t.typtype = 'd'
-- join pg_catalog.pg_namespace n on n."oid" = c.connamespace
-- where contype != 't';

-- select get_constraint_drop('pg_attrdef_adrelid_adnum_index', 'pg_attrdef');


-- Name

create or replace function get_constraint_drop
  (
    in_constr text,
    in_parent text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Generates ALTER TABLE / FOREIGN TABLE / DOMAIN...DROP CONSTRAINT commands for table / domain-level constraints
-- For columns' default value "constraints" (like DEFAULT, GENERATED ALWAYS / BY DEFAULT) use get_column_defaults_drop instead
  -- There are no functions for generating DROP DEFAULT command for domains - it's too simple and barely useful

-- Example:

-- select get_constraint_drop('pg_attrdef_adrelid_adnum_index', 'pg_attrdef');

  declare

    qry text;
    v_constr_params record;

  begin

    -- Checking existence of constraint
    select c."oid",
           n.nspname,
           c.conrelid,
           c.contypid,
           cl.relkind
    into v_constr_params
    from pg_catalog.pg_constraint c
    left join pg_catalog.pg_class cl on c.conrelid = cl."oid" and cl.relkind in ('r', 'p', 'f')
    left join pg_catalog.pg_type t on c.contypid = t."oid" and t.typtype = 'd'
    left join pg_catalog.pg_namespace n on n."oid" = c.connamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.conname = in_constr
          and n.nspname = coalesce(in_schema, s.sch)
          and coalesce(cl.relname, t.typname) = in_parent
          and c.contype != 't'
    order by s.rn
    limit 1;

    if v_constr_params."oid" is null then
      raise warning 'Constraint % for %.% does not exist',
        quote_ident(in_constr),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_parent);

      return null;
    end if;

    -- Building ALTER...DROP CONSTRAINT command

    qry := 'ALTER '
           || case
                when v_constr_params.contypid != 0::oid then 'DOMAIN '
                else
                  (
                    case when v_constr_params.relkind = 'f' then 'FOREIGN TABLE'
                          when v_constr_params.relkind in ('r', 'p') then 'TABLE'
                    end || ' IF EXISTS '
                  )
                end
            || quote_ident(v_constr_params.nspname) || '.' || quote_ident(in_parent)
            || E'\n    DROP CONSTRAINT IF EXISTS ' || quote_ident(in_constr)
            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_constraint_drop(in_constr text, in_parent text, in_schema text, cascade_ bool)
    is 'Generates ALTER TABLE/FOREIGN TABLE/DOMAIN...DROP CONSTRAINT... command by constraint''s name, name of the parent object and its schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_constraint_drop
  (
    constr_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Generates ALTER TABLE / FOREIGN TABLE / DOMAIN...DROP CONSTRAINT commands for table / domain-level constraints
-- For columns' default value "constraints" (like DEFAULT, GENERATED ALWAYS / BY DEFAULT) use get_column_defaults_drop instead
  -- There are no functions for generating DROP DEFAULT command for domains - it's too simple and barely useful

-- Example:

-- select get_constraint_drop("oid")
-- from pg_catalog.pg_constraint
-- limit 10;

  declare

    qry text;
    v_constr_params record;

  begin

    -- Checking existence of constraint
    select n.nspname as parent_schema,
           coalesce(cl.relname, t.typname) as parent_obj,
           c.conname,
           c.conrelid,
           c.contypid,
           cl.relkind
    into v_constr_params
    from pg_catalog.pg_constraint c
    left join pg_catalog.pg_class cl on c.conrelid = cl."oid" and cl.relkind in ('r', 'p', 'f')
    left join pg_catalog.pg_type t on c.contypid = t."oid" and t.typtype = 'd'
    join pg_catalog.pg_namespace n on n."oid" = c.connamespace
    where c."oid" = constr_oid
          and c.contype != 't';

    if v_constr_params.conname is null then
      raise warning 'Constraint with OID % does not exist', constr_oid;

      return null;
    end if;

    -- Building ALTER...ADD CONSTRAINT command

    qry := 'ALTER '
           || case
                when v_constr_params.contypid != 0::oid then 'DOMAIN '
                else
                  (
                    case when v_constr_params.relkind = 'f' then 'FOREIGN TABLE'
                          when v_constr_params.relkind in ('r', 'p') then 'TABLE'
                    end || ' IF EXISTS '
                  )
                end
            || quote_ident(v_constr_params.parent_schema) || '.' || quote_ident(v_constr_params.parent_obj)
            || E'\n    DROP CONSTRAINT IF EXISTS ' || quote_ident(v_constr_params.conname)
            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_constraint_drop(constr_oid oid, cascade_ bool)
    is 'Generates ALTER TABLE/FOREIGN TABLE/DOMAIN...DROP CONSTRAINT... command by constraint''s OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';