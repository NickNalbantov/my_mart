-- Examples:

-- select get_index_drop(c.relname, n.nspname),
--        get_index_drop(c."oid", false)
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where c.relkind in ('i', 'I');

-- select get_index_drop('pg_attrdef_adrelid_adnum_index');


-- Name + schema

create or replace function get_index_drop
  (
    in_idx text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_index_drop('pg_attrdef_adrelid_adnum_index');

-- select get_index_drop(relname)
-- from pg_catalog.pg_class
-- where relkind in ('i', 'I')
--       and relnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_index_params record;

  begin

    -- Checking existence of index
    select c."oid",
           n.nspname
    into v_index_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.relkind in ('i', 'I')
          and c.relname = in_idx
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_index_params."oid" is null then
      raise warning 'Index % in schema % not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_idx);

      return null;
    end if;

    -- Result

    qry := 'DROP INDEX IF EXISTS ' || quote_ident(v_index_params.nspname) || '.' || quote_ident(in_idx) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_index_drop(in_idx text, in_schema text, cascade_ bool)
    is 'Generates DROP command for an index by its name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_index_drop
  (
    idx_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_index_drop('pg_attrdef_adrelid_adnum_index'::regclass);

-- select get_index_drop("oid")
-- from pg_catalog.pg_class
-- where relkind in ('i', 'I')
-- limit 10;

  declare

    qry text;
    in_idx text;
    in_schema text;

  begin

    -- Checking existence of index
    select c.relname,
           n.nspname
    into in_idx,
         in_schema
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
    where c."oid" = idx_oid;

    if in_idx is null then
      raise warning 'Index with OID % does not exist', idx_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP INDEX IF EXISTS ' || quote_ident(in_schema) || '.' || quote_ident(in_idx) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_index_drop(idx_oid oid, cascade_ bool)
    is 'Generates DROP command for an index by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';