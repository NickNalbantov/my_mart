-- Examples:

-- select get_rule_drop(r.rulename, c.relname, n.nspname),
--        get_rule_drop(r."oid", false)
-- from pg_catalog.pg_rewrite r
-- join pg_catalog.pg_class c on r.ev_class = c."oid"
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace;

-- select get_rule_drop('_RETURN', 'pg_user');


-- Rule name + parent name + parent schema

create or replace function get_rule_drop
  (
    in_rule text,
    in_parent text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_rule_drop('_RETURN', 'pg_tables');

  declare

    qry text;
    v_rule_params record;

  begin

    -- Checking existence of rule
    select r."oid",
           n.nspname
    into v_rule_params
    from pg_catalog.pg_rewrite r
    join pg_catalog.pg_class c on r.ev_class = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where r.rulename = in_rule
          and c.relname = in_parent
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_rule_params."oid" is null then
      raise warning 'Rule % for table / view %.% does not exist',
        quote_ident(in_rule),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_parent);

      return null;
    end if;

    -- Result
    qry := 'DROP RULE IF EXISTS ' || quote_ident(in_rule) || ' ON ' || quote_ident(v_rule_params.nspname) || '.' || quote_ident(in_parent)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_rule_drop(in_rule text, in_parent text, in_schema text, cascade_ bool)
    is 'Generates DROP command for a rule by its name and parent''s name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_rule_drop
  (
    rule_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_rule_drop("oid")
-- from pg_catalog.pg_rewrite
-- limit 10;

  declare

    qry text;
    v_rule_params record;
    v_rule_parent text;

  begin

    -- Checking existence of rule
    select rulename,
           ev_class
    into v_rule_params
    from pg_catalog.pg_rewrite
    where "oid" = rule_oid;

    if v_rule_params.rulename is null then
      raise warning 'Rule with OID % does not exist', rule_oid;

      return null;
    end if;

    -- Parent
    select quote_ident(n.nspname) || '.' || quote_ident(c.relname)
    into v_rule_parent
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where c."oid" = v_rule_params.ev_class;

    -- Result
    qry := 'DROP RULE IF EXISTS ' || quote_ident(v_rule_params.rulename) || ' ON ' || v_rule_parent
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_rule_drop(rule_oid oid, cascade_ bool)
    is 'Generates DROP command for a rule by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';