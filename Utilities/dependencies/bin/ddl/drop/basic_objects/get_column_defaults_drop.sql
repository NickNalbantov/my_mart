-- Examples:

-- select get_column_defaults_drop(a.attname, c.relname, n.nspname),
--        get_column_defaults_drop(a.attname, c."oid"),
--        get_column_defaults_drop(a.attnum, c."oid")
-- from pg_catalog.pg_attribute a
-- join pg_catalog.pg_class c on c."oid" = a.attrelid
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- left join pg_catalog.pg_attrdef ad on ad.adrelid = a.attrelid and ad.adnum = a.attnum
-- where a.atthasdef is true or a.attidentity != '' or a.attgenerated != '';


-- Column name + table name + schema

create or replace function get_column_defaults_drop
  (
    in_column text,
    in_table text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Generates ALTER TABLE / VIEW...ALTER COLUMN...DROP command for columns' default / generated / identity values
  -- There are no functions for generating DROP DEFAULT command for domains - it's too simple and barely useful

  declare

    v_col_def_params record;
    qry text;

  begin

    -- Checking existence of default value
    select c."oid" as parent_oid,
           c.relkind,
           n.nspname,
           a.atthasdef,
           a.attidentity,
           a.attgenerated
    into v_col_def_params
    from pg_catalog.pg_attribute a
    join pg_catalog.pg_class c on a.attrelid = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where (a.atthasdef is true or a.attidentity != '' or a.attgenerated != '')
          and a.attisdropped is false
          and a.attname = in_column
          and c.relname = in_table
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_col_def_params.parent_oid is null then
      raise warning 'Default value for column % of table %.% does not exist',
        quote_ident(in_column),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_table);

      return null;
    end if;

    -- Result
    qry := 'ALTER '
           || case when v_col_def_params.relkind = 'f' then 'FOREIGN ' else '' end
           || case when v_col_def_params.relkind != 'v' then 'TABLE' else 'VIEW' end || ' IF EXISTS '
           || quote_ident(v_col_def_params.nspname) || '.' || quote_ident(in_table)
           || E'\n    ALTER COLUMN ' || quote_ident(in_column) || ' '
           || case when v_col_def_params.atthasdef is true and v_col_def_params.attgenerated = ''
                        then 'DROP DEFAULT'
                   when v_col_def_params.attidentity != ''
                        then 'DROP IDENTITY IF EXISTS'
                   when v_col_def_params.attgenerated = 's'
                        then 'DROP EXPRESSION IF EXISTS'
              end
           || ';';

    return qry;

  end;

$$;

comment on function get_column_defaults_drop(in_column text, in_table text, in_schema text)
    is 'Generates ALTER TABLE / VIEW...ALTER COLUMN...DROP command for a table''s column default value by name of the column and table''s name and schema (default - search_path)';


-- Column name + OID

create or replace function get_column_defaults_drop
  (
    in_column text,
    table_oid oid
  )
  returns text
  language plpgsql
  strict
  as
  $$

-- Generates ALTER TABLE / VIEW...ALTER COLUMN...DROP command for columns' default / generated / identity values
  -- There are no functions for generating DROP DEFAULT command for domains - it's too simple and barely useful

  declare

    v_col_def_params record;
    qry text;

  begin

    -- Checking existence of default value
    select c.relname,
           c.relkind,
           n.nspname,
           a.atthasdef,
           a.attidentity,
           a.attgenerated
    into v_col_def_params
    from pg_catalog.pg_attribute a
    join pg_catalog.pg_class c on a.attrelid = c."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where (a.atthasdef is true or a.attidentity != '' or a.attgenerated != '')
          and a.attisdropped is false
          and a.attname = in_column
          and c."oid" = table_oid;

    if v_col_def_params.relname is null then
      raise warning 'Default value for column % of table with OID % does not exist', quote_ident(in_column), table_oid;

      return null;
    end if;

    -- Result
    qry := 'ALTER '
           || case when v_col_def_params.relkind = 'f' then 'FOREIGN ' else '' end
           || case when v_col_def_params.relkind != 'v' then 'TABLE' else 'VIEW' end || ' IF EXISTS '
           || quote_ident(v_col_def_params.nspname) || '.' || quote_ident(v_col_def_params.relname)
           || E'\n    ALTER COLUMN ' || quote_ident(in_column) || ' '
           || case when v_col_def_params.atthasdef is true and v_col_def_params.attgenerated = ''
                        then 'DROP DEFAULT'
                   when v_col_def_params.attidentity != ''
                        then 'DROP IDENTITY IF EXISTS'
                   when v_col_def_params.attgenerated = 's'
                        then 'DROP EXPRESSION IF EXISTS'
              end
           || ';';

    return qry;
  end;
$$;

comment on function get_column_defaults_drop(in_column text, table_oid oid)
    is 'Generates ALTER TABLE / VIEW...ALTER COLUMN...DROP command for a table''s column default value by name of the column and table''s OID';


-- Column position + OID

create or replace function get_column_defaults_drop
  (
    column_num anycompatiblenonarray,
    table_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Generates ALTER TABLE / VIEW...ALTER COLUMN...DROP command for columns' default / generated / identity values
  -- There are no functions for generating DROP DEFAULT command for domains - it's too simple and barely useful

  declare

    v_col_def_params record;
    qry text;

  begin

    -- Checking existence of default value
    select c.relname,
           c.relkind,
           n.nspname,
           a.attname,
           a.atthasdef,
           a.attidentity,
           a.attgenerated
    into v_col_def_params
    from pg_catalog.pg_attribute a
    join pg_catalog.pg_class c on a.attrelid = c."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where (a.atthasdef is true or a.attidentity != '' or a.attgenerated != '')
          and a.attisdropped is false
          and a.attnum = column_num
          and c."oid" = table_oid;

    if v_col_def_params.relname is null then
      raise warning 'Default value for column at position % of table with OID % does not exist', column_num, table_oid;

      return null;
    end if;

    -- Result
    qry := 'ALTER '
           || case when v_col_def_params.relkind = 'f' then 'FOREIGN ' else '' end
           || case when v_col_def_params.relkind != 'v' then 'TABLE' else 'VIEW' end || ' IF EXISTS '
           || quote_ident(v_col_def_params.nspname) || '.' || quote_ident(v_col_def_params.relname)
           || E'\n    ALTER COLUMN ' || quote_ident(v_col_def_params.attname) || ' '
           || case when v_col_def_params.atthasdef is true and v_col_def_params.attgenerated = ''
                        then 'DROP DEFAULT'
                   when v_col_def_params.attidentity != ''
                        then 'DROP IDENTITY IF EXISTS'
                   when v_col_def_params.attgenerated = 's'
                        then 'DROP EXPRESSION IF EXISTS'
              end
           || ';';

    return qry;

  end;

$$;

comment on function get_column_defaults_drop(column_num anycompatiblenonarray, table_oid oid)
    is 'Generates ALTER TABLE / VIEW...ALTER COLUMN...DROP command for a table''s column default value by column''s position number and table''s OID';