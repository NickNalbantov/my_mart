-- Examples:

-- select get_publication_drop(pubname),
--        get_publication_drop("oid")
-- from pg_catalog.pg_publication;


-- Name

create or replace function get_publication_drop
  (
    in_pub text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_publication_drop(pubname)
-- from pg_catalog.pg_publication
-- limit 10;

  declare

    qry text;
    v_oid oid;

  begin

    -- Checking existence of publication
    select "oid"
    into v_oid
    from pg_catalog.pg_publication
    where pubname = in_pub;

    if v_oid is null then
      raise warning 'Publication % does not exist', quote_ident(in_pub);

      return null;
    end if;

    -- Result

    qry := 'DROP PUBLICATION IF EXISTS ' || quote_ident(in_pub) || ';';

    return qry;

  end;

$$;

comment on function get_publication_drop(in_pub text)
    is 'Generates DROP command for a publication by its name';


-- OID

create or replace function get_publication_drop
  (
    pub_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_publication_drop("oid")
-- from pg_catalog.pg_publication
-- limit 10;

  declare

    qry text;
    v_pubname text;

  begin

    -- Checking existence of publication
    select pubname
    into v_pubname
    from pg_catalog.pg_publication
    where "oid" = pub_oid;

    if v_pubname is null then
      raise warning 'Publication with OID % does not exist', pub_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP PUBLICATION IF EXISTS ' || quote_ident(v_pubname) || ';';

    return qry;

  end;

$$;

comment on function get_publication_drop(pub_oid oid)
    is 'Generates DROP command for a publication by its OID';