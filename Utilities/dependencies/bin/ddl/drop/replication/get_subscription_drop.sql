-- Examples:

-- select get_subscription_drop(subname),
--        get_subscription_drop("oid")
-- from pg_catalog.pg_subscription;


-- Name

create or replace function get_subscription_drop
  (
    in_sub text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_subscription_drop(subname)
-- from pg_catalog.pg_subscription
-- limit 10;

  declare

    qry text;
    v_oid oid;

  begin

    -- Checking existence of subscription
    select "oid"
    into v_oid
    from pg_catalog.pg_subscription
    where subname = in_sub;

    if v_oid is null then
      raise warning 'Subscription % does not exist', quote_ident(in_sub);

      return null;
    end if;

    -- Result

    qry := 'DROP SUBSCRIPTION IF EXISTS ' || quote_ident(in_sub) || ';';

    return qry;

  end;

$$;

comment on function get_subscription_drop(in_sub text)
    is 'Generates DROP command for a subscription by its name';


-- OID

create or replace function get_subscription_drop
  (
    sub_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_subscription_drop("oid")
-- from pg_catalog.pg_subscription
-- limit 10;

  declare

    qry text;
    v_subname text;

  begin

    -- Checking existence of subscription
    select subname
    into v_subname
    from pg_catalog.pg_subscription
    where "oid" = sub_oid;

    if v_subname is null then
      raise warning 'Subscription with OID % does not exist', sub_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP SUBSCRIPTION IF EXISTS ' || quote_ident(v_subname) || ';';

    return qry;

  end;

$$;

comment on function get_subscription_drop(sub_oid oid)
    is 'Generates DROP command for a subscription by its OID';