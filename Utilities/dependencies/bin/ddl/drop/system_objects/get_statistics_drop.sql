-- Examples:

-- select get_statistics_drop(s.stxname, n.nspname),
--        get_statistics_drop(s."oid", false)
-- from pg_catalog.pg_statistic_ext s
-- join pg_catalog.pg_namespace n on s.stxnamespace = n."oid";


-- Name

create or replace function get_statistics_drop
  (
    in_stat text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_statistics_drop(stxname)
-- from pg_catalog.pg_statistic_ext s
-- where stxnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_stat_params record;

  begin

    -- Checking existence of statistics object

    select st."oid",
           n.nspname
    into v_stat_params
    from pg_catalog.pg_statistic_ext st
    left join pg_catalog.pg_namespace n on n."oid" = st.stxnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where in_stat = st.stxname
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_stat_params."oid" is null then
      raise warning 'Statistics object %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_stat);

      return null;
    end if;

    -- Result

    qry := 'DROP STATISTICS IF EXISTS ' || quote_ident(v_stat_params.nspname) || '.' || quote_ident(in_stat)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_statistics_drop(in_stat text, in_schema text, cascade_ bool)
    is 'Generates DROP command for a statistics object by its name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_statistics_drop
  (
    stat_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_statistics_drop("oid")
-- from pg_catalog.pg_statistic_ext s
-- limit 10;

  declare

    qry text;
    v_stat_params record;

  begin

    -- Checking existence of statistics object

    select st.stxname,
           n.nspname
    into v_stat_params
    from pg_catalog.pg_statistic_ext st
    join pg_catalog.pg_namespace n on n."oid" = st.stxnamespace
    where stat_oid = st."oid";

    if v_stat_params.stxname is null then
      raise warning 'Statistics object with OID % does not exist', stat_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP STATISTICS IF EXISTS ' || quote_ident(v_stat_params.nspname) || '.' || quote_ident(v_stat_params.stxname)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_statistics_drop(stat_oid oid, cascade_ bool)
    is 'Generates DROP command for a statistics object by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';