-- Examples:

-- select get_conversion_drop(c.conname, n.nspname),
--        get_conversion_drop(c."oid", false)
-- from pg_catalog.pg_conversion c
-- join pg_catalog.pg_namespace n on c.connamespace = n."oid";

-- select get_conversion_drop('utf8_to_windows_1251');


-- Name + schema

create or replace function get_conversion_drop
  (
    in_con text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_conversion_drop('utf8_to_windows_1251');

-- select get_conversion_drop(conname)
-- from pg_catalog.pg_conversion
-- where connamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_con_params record;

  begin

    -- Checking existence of conversion
    select c."oid",
           n.nspname
    into v_con_params
    from pg_catalog.pg_conversion c
    left join pg_catalog.pg_namespace n on c.connamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.conname = in_con
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_con_params."oid" is null then
      raise warning 'Conversion %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_con);

      return null;
    end if;

    -- Result

    qry := 'DROP CONVERSION IF EXISTS ' || quote_ident(v_con_params.nspname) || '.' || quote_ident(in_con)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_conversion_drop(in_con text, in_schema text, cascade_ bool)
    is 'Generates DROP command for a conversion by its name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_conversion_drop
  (
    con_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_conversion_drop("oid")
-- from pg_catalog.pg_conversion
-- limit 10;

  declare

    qry text;
    v_con_params record;

  begin

    -- Checking existence of conversion
    select c.conname,
           n.nspname
    into v_con_params
    from pg_catalog.pg_conversion c
    join pg_catalog.pg_namespace n on c.connamespace = n."oid"
    where c."oid" = con_oid;

    if v_con_params.conname is null then
      raise warning 'Conversion with OID % does not exist', con_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP CONVERSION IF EXISTS ' || quote_ident(v_con_params.nspname) || '.' || quote_ident(v_con_params.conname)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_conversion_drop(con_oid oid, cascade_ bool)
    is 'Generates DROP command for a conversion by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';