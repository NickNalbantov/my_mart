-- Examples:

-- select
--       c."oid",
--       c.relnamespace::regnamespace || '.' || c.relname || '.' || a.attname as obj_name,
--       get_security_label_drop(c.tableoid, c."oid", a.attnum) as qry
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_attribute a on a.attrelid = c."oid"
-- where
--       a.attnum > 0
--       and a.attisdropped is false

-- union all

-- select
--       "oid",
--       rolname,
--       get_security_label_drop(tableoid, "oid")
-- from pg_catalog.pg_authid

-- union all

-- select
--       "oid",
--       relnamespace::regnamespace || '.' || relname,
--       get_security_label_drop(tableoid, "oid")
-- from pg_catalog.pg_class
-- where relkind in ('r', 'p', 'm', 'v', 'f', 'S')

-- union all

-- select
--       "oid",
--       datname,
--       get_security_label_drop(tableoid, "oid")
-- from pg_catalog.pg_database

-- union all

-- select
--       "oid",
--       evtname,
--       get_security_label_drop(tableoid, "oid")
-- from pg_catalog.pg_event_trigger

-- union all

-- select
--       "oid",
--       lanname,
--       get_security_label_drop(tableoid, "oid")
-- from pg_catalog.pg_language

-- union all

-- select
--       "oid",
--       "oid"::text,
--       get_security_label_drop(tableoid, "oid")
-- from pg_catalog.pg_largeobject_metadata

-- union all

-- select
--       "oid",
--       nspname,
--       get_security_label_drop(tableoid, "oid")
-- from pg_catalog.pg_namespace

-- union all

-- select
--       "oid",
--       pronamespace::regnamespace || '.' || proname,
--       get_security_label_drop(tableoid, "oid")
-- from pg_catalog.pg_proc

-- union all

-- select
--       "oid",
--       pubname,
--       get_security_label_drop(tableoid, "oid")
-- from pg_catalog.pg_publication

-- union all

-- select
--       "oid",
--       subname,
--       get_security_label_drop(tableoid, "oid")
-- from pg_catalog.pg_subscription

-- union all

-- select
--       "oid",
--       spcname,
--       get_security_label_drop(tableoid, "oid")
-- from pg_catalog.pg_tablespace

-- union all

-- select
--       "oid",
--       typnamespace::regnamespace || '.' || typname,
--       get_security_label_drop(tableoid, "oid")
-- from pg_catalog.pg_type;


-- OIDs

create or replace function get_security_label_drop
  (
    classid oid,
    objid oid,
    objsubid anycompatiblenonarray default 0
  )
returns text
language plpgsql
strict
as
$$

-- 'classid' argument is necessary because objects' OID are unique only within given catalog

-- Unlike get_xxx_comment functions DDL for security labels are currently generated only by this function, because this feature is not that widespread as COMMENT, and one general function is enough

-- Example:

-- select s."oid",
--        s.relnamespace::regnamespace || '.' || s.relname,
--        get_security_label_drop(s.tableoid, s."oid")
-- from pg_catalog.pg_description d
-- join pg_catalog.pg_class s on s.tableoid = d.classoid and s."oid" = d.objoid
-- limit 10;

  declare

    qry text;
    v_identity_rec record;
    v_object_clause text;
    v_domain_check boolean default false;
    v_objsubid int;
    v_shared bool;
    v_provider text;

  begin

    v_objsubid := objsubid::int; -- needed to avoid ambiguity with a column in pg_description

    -- Identifying object
    select
      case when i."type" like '%column' then quote_ident('pg_attribute')
        else quote_ident(c.relname)
      end as catalog_name,
      i."type" as obj_type,
      i."identity" as obj_identity
    into v_identity_rec
    from pg_catalog.pg_class c
    cross join lateral pg_identify_object(c."oid", objid, case when classid = 'pg_catalog.pg_class'::regclass then objsubid else 0 end) i
    where
        c."oid" = classid
        and c.relnamespace = 'pg_catalog'::regnamespace
        and c.relkind = 'r';


    if v_identity_rec.catalog_name is null then
      raise warning E'Can''t identify object with classid (system catalog table''s OID) % and object''s OID %',
        classid,
        objid;

      return null;
    end if;

    -- Check if object is a domain
    if classid = 'pg_catalog.pg_type'::regclass then
      select typtype = 'd'
      into v_domain_check
      from pg_catalog.pg_type
      where "oid" = objid;
    end if;

    -- Building object type clause
    select
      case
        when v_identity_rec.obj_type = 'type' and v_domain_check is true then 'DOMAIN'
        when v_identity_rec.obj_type = 'type' and v_domain_check is not true then 'TYPE'
        when v_identity_rec.obj_type = 'composite type' then 'TYPE'
        when v_identity_rec.obj_type like '%column' then 'COLUMN'
        when v_identity_rec.obj_type in (
                                          'table', 'foreign table', 'view', 'materialized view', 'sequence',
                                          'function', 'procedure', 'aggregate', 'event trigger',
                                          'language', 'large object',
                                          'publication', 'subscription', 'role',
                                          'schema', 'database', 'tablespace'
                                        )
              then upper(v_identity_rec.obj_type)
        else null::text
      end,
      case when lower(v_identity_rec.obj_type) in ('database', 'tablespace', 'role') then true else false end
    into
      v_object_clause,
      v_shared;

    -- Getting the label params
    if v_shared is true then
      select d.provider
      into v_provider
      from pg_catalog.pg_shseclabel d
      where
        classid = d.classoid
        and objid = d.objoid;

    else
      select d.provider
      into v_provider
      from pg_catalog.pg_seclabel d
      where
        classid = d.classoid
        and objid = d.objoid
        and v_objsubid = d.objsubid;

    end if;

    -- Result

    qry := 'SECURITY LABEL FOR ' || v_provider || ' ON ' || v_object_clause || ' ' || v_identity_rec.obj_identity
           || E'\n    IS NULL;';

    return qry;

  end;

$$;

comment on function get_security_label_drop(classid oid, objid oid, objsubid anycompatiblenonarray)
    is 'Generates command for removing SECURITY LABEL from an object by OID of parent catalog, object''s OID and object''s sub-id (e.g. column number)';