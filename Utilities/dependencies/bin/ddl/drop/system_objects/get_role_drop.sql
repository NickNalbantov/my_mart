-- Examples:

-- select get_role_drop('pg_write_all_data');

-- select get_role_drop(a.rolname, b.rolname),
--        get_role_drop(a."oid", b."oid")
-- from pg_catalog.pg_authid a
-- join pg_catalog.pg_authid b on b."oid" = 10
--                                and a."oid" != b."oid";


-- Name

create or replace function get_role_drop
  (
    in_rol text,
    new_owner text default 'postgres'
  )
returns text
language plpgsql
strict
as
$$

-- A role usually can't be dropped without reassigning owned objects to another role
-- This function generates REASSIGN OWNED BY and DROP OWNED BY commands before DROP ROLE command
-- Default new owner - 'postgres'

-- Example:

-- select get_role_def(rolname)
-- from pg_catalog.pg_authid;

  declare

    qry text;
    v_oid oid;
    v_new_owner_oid oid;

  begin

    -- Checking existence of roles

    if in_rol = new_owner then
      raise warning 'Old role (%) and new owner of its objects (%) are the same roles',
        quote_ident(in_rol),
        quote_ident(new_owner);

      return null;

    elsif lower(in_rol) = 'public' then
      raise warning 'Role ''public'' can''t be dropped';

      return null;

    elsif lower(new_owner) = 'public' then
      raise warning 'Role ''public'' can''t own objects';

      return null;

    end if;

    select "oid"
    into v_oid
    from pg_catalog.pg_authid
    where rolname = in_rol;

    if v_oid is null then
      raise warning 'Role % does not exist', quote_ident(in_rol);

      return null;
    end if;

    select "oid"
    into v_new_owner_oid
    from pg_catalog.pg_authid
    where rolname = quote_ident(new_owner);

    if v_new_owner_oid is null then
      raise warning 'Successor role % does not exist', quote_ident(new_owner);

      return null;
    end if;

    -- Result

    qry := 'REASSIGN OWNED BY ' || quote_ident(in_rol) || ' TO ' || quote_ident(new_owner) || E';\n\n'
           || 'DROP OWNED BY ' || quote_ident(in_rol) || E' CASCADE;\n\n'
           || 'DROP ROLE IF EXISTS ' || quote_ident(in_rol) || ';';

    return qry;

  end;

$$;

comment on function get_role_drop(in_rol text, new_owner text)
    is 'Generates DROP command for a role by its name + REASSIGN OWNED / DROP OWNED commands to another role by its name (default ''postgres'')';


-- OID

create or replace function get_role_drop
  (
    rol_oid oid,
    new_owner_oid oid default 'postgres'::text::regrole -- for some reason direct cast to regrole is not allowed
  )
returns text
language plpgsql
strict
as
$$

-- A role usually can't be dropped without reassigning owned objects to another role
-- This function generates REASSIGN OWNED BY and DROP OWNED BY commands before DROP ROLE command
-- Default new owner - 'postgres'

-- Example:

-- select get_role_def("oid")
-- from pg_catalog.pg_authid;

  declare

    qry text;
    v_rolname text;
    v_new_owner text;

  begin

    -- Checking existence of roles

    if rol_oid = new_owner_oid then
      raise warning 'Old role (OID %) and new owner of its objects (OID %) are the same roles', rol_oid, new_owner_oid;

      return null;
    end if;

    select rolname
    into v_rolname
    from pg_catalog.pg_authid
    where "oid" = rol_oid;

    if v_rolname is null then
      raise warning 'Role with OID % does not exist', rol_oid;

      return null;

    elsif rol_oid = 0::oid then
      raise warning 'Role ''public'' can''t be dropped';

      return null;

    elsif new_owner_oid = 0::oid then
      raise warning 'Role ''public'' can''t own objects';

      return null;

    end if;

    select rolname
    into v_new_owner
    from pg_catalog.pg_authid
    where "oid" = new_owner_oid;

    if v_new_owner is null then
      raise warning 'Successor role with OID % does not exist', new_owner_oid;

      return null;
    end if;

    -- Result

    qry := 'REASSIGN OWNED BY ' || quote_ident(v_rolname) || ' TO ' || quote_ident(v_new_owner) || E';\n\n'
           || 'DROP OWNED BY ' || quote_ident(v_rolname) || E';\n\n'
           || 'DROP ROLE IF EXISTS ' || quote_ident(v_rolname) || ';';
    return qry;

  end;

$$;

comment on function get_role_drop(rol_oid oid, new_owner_oid oid)
    is 'Generates DROP command for a role by its OID + REASSIGN / DROP OWNED commands to another role by its OID (default ''postgres''::regrole)';