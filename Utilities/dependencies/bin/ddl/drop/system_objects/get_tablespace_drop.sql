-- Examples:

-- select get_tablespace_drop(spcname),
--        get_tablespace_drop("oid")
-- from pg_catalog.pg_tablespace;


-- Name

create or replace function get_tablespace_drop
  (
    in_tblspc text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_tablespace_drop(spcname)
-- from pg_catalog.pg_tablespace;

  declare

    qry text;
    v_oid oid;

  begin

    -- Checking existence of tablespace
    select "oid"
    into v_oid
    from pg_catalog.pg_tablespace
    where spcname = in_tblspc;

    if v_oid is null then
      raise warning 'Tablespace % does not exist', quote_ident(in_tblspc);

      return null;
    end if;

    -- Result

    qry := 'DROP TABLESPACE IF EXISTS ' || quote_ident(in_tblspc) || ';';
    return qry;

  end;

$$;

comment on function get_tablespace_drop(in_tblspc text)
    is 'Generates DROP command for a tablespace by its name';


-- OID

create or replace function get_tablespace_drop
  (
    tblspc_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_tablespace_drop("oid")
-- from pg_catalog.pg_tablespace;

  declare

    qry text;
    v_spcname text;

  begin

    -- Checking existence of tablespace
    select spcname
    into v_spcname
    from pg_catalog.pg_tablespace
    where "oid" = tblspc_oid;

    if v_spcname is null then
      raise warning 'Tablespace with OID % does not exist', tblspc_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP TABLESPACE IF EXISTS ' || quote_ident(v_spcname) || ';';
    return qry;

  end;

$$;

comment on function get_tablespace_drop(tblspc_oid oid)
    is 'Generates DROP command for a tablespace by its OID';