-- Examples:

-- select get_database_drop(datname),
--        get_database_drop("oid", false)
-- from pg_catalog.pg_database;


-- Name

create or replace function get_database_drop
  (
    in_db text,
    force_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_database_drop(datname),
-- from pg_catalog.pg_database
-- limit 10;

  declare

    qry text;
    v_oid oid;

  begin

    -- Checking existence of database
    select d."oid"
    into v_oid
    from pg_catalog.pg_database d
    where d.datname = in_db;

    if v_oid is null then
      raise warning 'Database % does not exist', quote_ident(in_db);

      return null;
    end if;

    -- Result

    qry := 'DROP DATABASE IF EXISTS ' || quote_ident(in_db) || case when force_ is true then ' WITH (FORCE)' else '' end || ';';

    return qry;

  end;

$$;

comment on function get_database_drop(in_db text, force_ bool)
    is 'Generates DROP command for a database by its name; if force_ is true (by default) then it adds WITH FORCE option to DROP command';


-- OID

create or replace function get_database_drop
  (
    db_oid oid,
    force_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_database_drop("oid"),
-- from pg_catalog.pg_database
-- limit 10;

  declare

    qry text;
    v_db_name text;

  begin

    -- Checking existence of database
    select d.datname
    into v_db_name
    from pg_catalog.pg_database d
    where d."oid" = db_oid;

    if v_db_name is null then
      raise warning 'Database with OID % does not exist', db_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP DATABASE IF EXISTS ' || quote_ident(v_db_name) || case when force_ is true then ' WITH (FORCE)' else '' end || ';';

    return qry;
  end;
$$;

comment on function get_database_drop(db_oid oid, force_ bool)
    is 'Generates DROP command for a database by its OID; if force_ is true (by default) then it adds WITH FORCE option to DROP command';