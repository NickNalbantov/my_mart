-- Examples:

-- select get_collation_drop(c.collname, n.nspname),
--        get_collation_drop(c."oid", false)
-- from pg_catalog.pg_collation c
-- join pg_catalog.pg_namespace n on c.collnamespace = n."oid";

-- select get_collation_drop('ru-RU-x-icu');


-- Name + schema

create or replace function get_collation_drop
  (
    in_coll text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_collation_drop('ru-RU-x-icu');

-- select get_collation_drop(collname)
-- from pg_catalog.pg_collation
-- where collnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_coll_params record;

  begin

    -- Checking existence of collation
    select c."oid",
           n.nspname
    into v_coll_params
    from pg_catalog.pg_collation c
    left join pg_catalog.pg_namespace n on c.collnamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.collname = in_coll
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_coll_params."oid" is null then
      raise warning 'Collation %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_coll);

      return null;
    end if;

    -- Result

    qry := 'DROP COLLATION IF EXISTS ' || quote_ident(v_coll_params.nspname) || '.' || quote_ident(in_coll)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_collation_drop(in_coll text, in_schema text, cascade_ bool)
    is 'Generates DROP command for a collation by its name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_collation_drop
  (
    coll_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_collation_drop('"ru-RU-x-icu"'::regcollation);

-- select get_collation_drop("oid")
-- from pg_catalog.pg_collation
-- limit 10;

  declare

    qry text;
    v_coll_params record;

  begin

    -- Checking existence of collation
    select c.collname,
           n.nspname
    into v_coll_params
    from pg_catalog.pg_collation c
    join pg_catalog.pg_namespace n on c.collnamespace = n."oid"
    where c."oid" = coll_oid;

    if v_coll_params.collname is null then
      raise warning 'Collation with OID % does not exist', coll_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP COLLATION IF EXISTS ' || quote_ident(v_coll_params.nspname) || '.' || quote_ident(v_coll_params.collname)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_collation_drop(coll_oid oid, cascade_ bool)
    is 'Generates DROP command for a collation by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';