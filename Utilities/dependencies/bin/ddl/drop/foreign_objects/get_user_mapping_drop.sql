-- Examples:

-- select get_user_mapping_drop(s.srvname, a.rolname),
--        get_user_mapping_drop(u."oid"),
--        get_user_mapping_drop(s."oid", a."oid")
-- from pg_catalog.pg_user_mapping u
-- join pg_catalog.pg_foreign_server s on u.umserver = s."oid"
-- join (
--            select "oid",
--               rolname
--            from pg_catalog.pg_authid
--            union all
--            select 0::oid as "oid",
--              'public' as rolname
--        ) a on u.umuser = a."oid";


-- Server name + role name

create or replace function get_user_mapping_drop
  (
    in_srv text,
    in_role text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_user_mapping_drop(srvname, 'postgres')
-- from pg_catalog.pg_foreign_server
-- limit 10;

  declare

    qry text;
    v_um_role record;
    v_oid oid;

  begin

    -- Checking existence of user mapping
    select u."oid"
    into v_oid
    from pg_catalog.pg_user_mapping u
    join pg_catalog.pg_foreign_server s on u.umserver = s."oid"
    join (
            select "oid",
                   rolname
            from pg_catalog.pg_authid
            union all
            select 0::oid as "oid",
                   'public' as rolname
         ) a on u.umuser = a."oid"
    where s.srvname = in_srv
          and in_role = a.rolname;

    if v_oid is null then
      raise warning 'User mapping for role % at server % does not exist',
        quote_ident(in_role),
        quote_ident(in_srv);

      return null;
    end if;

    -- Result

    qry := 'DROP USER MAPPING IF EXISTS FOR ' || quote_ident(in_role) || E'\n    SERVER ' || quote_ident(in_srv) || ';';

    return qry;

  end;

$$;

comment on function get_user_mapping_drop(in_srv text, in_role text)
    is 'Generates DROP command for an user mapping by server name and role name';


-- OID

create or replace function get_user_mapping_drop
  (
    um_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_user_mapping_drop("oid")
-- from pg_catalog.pg_user_mapping
-- limit 10;

  declare

    qry text;
    v_um_params record;

  begin

    -- Checking existence of user mapping
    select u."oid",
           a.rolname,
           s.srvname
    into v_um_params
    from pg_catalog.pg_user_mapping u
    join pg_catalog.pg_foreign_server s on u.umserver = s."oid"
    join (
            select "oid",
                   rolname
            from pg_catalog.pg_authid
            union all
            select 0::oid as "oid",
                  'public' as rolname
         ) a on u.umuser = a."oid"
    where u."oid" = um_oid;

    if v_um_params."oid" is null then
      raise warning 'User mapping with OID % does not exist', um_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP USER MAPPING IF EXISTS FOR ' || quote_ident(v_um_params.rolname) || E'\n    SERVER ' || quote_ident(v_um_params.srvname) || ';';

    return qry;

  end;

$$;

comment on function get_user_mapping_drop(um_oid oid)
    is 'Generates DROP command for an user mapping by its OID';


-- Server and role OIDs

create or replace function get_user_mapping_drop
  (
    srv_oid oid,
    rol_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_user_mapping_drop("oid", 'postgres'::regrole)
-- from pg_catalog.pg_foreign_server
-- limit 10;

  declare

    qry text;
    v_um_params record;

  begin

    -- Checking existence of user mapping
    select u."oid",
           a.rolname,
           s.srvname
    into v_um_params
    from pg_catalog.pg_user_mapping u
    join pg_catalog.pg_foreign_server s on u.umserver = s."oid"
    join (
            select "oid",
                   rolname
            from pg_catalog.pg_authid
            union all
            select 0::oid as "oid",
                   'public' as rolname
         ) a on u.umuser = a."oid"
    where s."oid" = srv_oid
          and rol_oid = a."oid";

    if v_um_params."oid" is null then
      raise warning 'User mapping for server and role with OIDs % and % does not exist', srv_oid, rol_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP USER MAPPING IF EXISTS FOR ' || quote_ident(v_um_params.rolname) || E'\n    SERVER ' || quote_ident(v_um_params.srvname) || ';';

    return qry;

  end;

$$;

comment on function get_user_mapping_drop(srv_oid oid, rol_oid oid)
    is 'Generates DROP command for an user mapping by OIDs of server and role';