-- Examples:

-- select get_foreign_table_drop(c.relname, n.nspname),
--        get_foreign_table_drop(c."oid", false)
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where c.relkind = 'f';


-- Name + schema

create or replace function get_foreign_table_drop
  (
    in_table text,
    in_schema text default null,
    cascade_ bool default true
  )
  returns text
  language plpgsql
  as
  $$

-- Generates DROP command for foreign tables only
  -- For other types of tables use get_table_def functions

  declare

    qry text;
    v_table_params record;

  begin

    -- Checking existence of foreign table
    select c."oid",
           n.nspname
    into v_table_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.relkind = 'f'
          and c.relname = in_table
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_table_params."oid" is null then
      raise warning 'Foreign table %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_table);

      return null;
    end if;

    -- Result

    qry := 'DROP FOREIGN TABLE IF EXISTS ' || quote_ident(v_table_params.nspname) || '.' || quote_ident(in_table)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_foreign_table_drop(in_table text, in_schema text, cascade_ bool)
    is 'Generates DROP command for a foreign table by its name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_foreign_table_drop
  (
    table_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Generates DROP command for foreign tables only
  -- For other types of tables use get_table_def functions

-- Example:

-- select get_foreign_table_drop("oid")
-- from pg_catalog.pg_class
-- where relkind = 'f'
-- limit 10;

  declare

    qry text;
    v_table_params record;

  begin

    -- Checking existence of foreign table
    select n.nspname,
           c.relname
    into v_table_params
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where c.relkind = 'f'
          and c."oid" = table_oid;

    if v_table_params.relname is null then
      raise warning 'Foreign table with OID % does not exist', table_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP FOREIGN TABLE IF EXISTS ' || quote_ident(v_table_params.nspname) || '.' || quote_ident(v_table_params.relname)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_foreign_table_drop(table_oid oid, cascade_ bool)
    is 'Generates DROP command for a foreign table by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';