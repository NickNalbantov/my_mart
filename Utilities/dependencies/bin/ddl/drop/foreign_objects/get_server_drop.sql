-- Examples:

-- select get_server_drop(srvname),
--        get_server_drop("oid", false)
-- from pg_catalog.pg_foreign_server;


-- Name

create or replace function get_server_drop
  (
    in_srv text,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_server_drop(srvname)
-- from pg_catalog.pg_foreign_server
-- limit 10;

  declare

    qry text;
    v_oid oid;

  begin

    -- Checking existence of foreign server
    select "oid"
    into v_oid
    from pg_catalog.pg_foreign_server
    where srvname = in_srv;

    if v_oid is null then
      raise warning 'Foreign server % does not exist', quote_ident(in_srv);

      return null;
    end if;

    -- Result

    qry := 'DROP SERVER IF EXISTS ' || quote_ident(in_srv) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_server_drop(in_srv text, cascade_ bool)
    is 'Generates DROP command for a foreign server by its name; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_server_drop
  (
    srv_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_server_drop("oid")
-- from pg_catalog.pg_foreign_server
-- limit 10;

  declare

    qry text;
    in_srv text;

  begin

    -- Checking existence of foreign server
    select srvname
    into in_srv
    from pg_catalog.pg_foreign_server
    where "oid" = srv_oid;

    if in_srv is null then
      raise warning 'Foreign server with OID % does not exist', srv_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP SERVER IF EXISTS ' || quote_ident(in_srv) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_server_drop(srv_oid oid, cascade_ bool)
    is 'Generates DROP command for a foreign server by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';