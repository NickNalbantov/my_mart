-- Examples:

-- select get_fdw_drop(fdwname),
--        get_fdw_drop("oid", false)
-- from pg_catalog.pg_foreign_data_wrapper;


-- Name

create or replace function get_fdw_drop
  (
    in_fdw text,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_fdw_drop(fdwname)
-- from pg_catalog.pg_foreign_data_wrapper
-- limit 10;

  declare

    qry text;
    v_oid oid;

  begin

    -- Checking existence of FDW
    select "oid"
    into v_oid
    from pg_catalog.pg_foreign_data_wrapper
    where fdwname = in_fdw;

    if v_oid is null then
      raise warning 'Foreign data wrapper % does not exist', quote_ident(in_fdw);

      return null;
    end if;

    -- Result

    qry := 'DROP FOREIGN DATA WRAPPER IF EXISTS ' || quote_ident(in_fdw) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_fdw_drop(in_fdw text, cascade_ bool)
    is 'Generates DROP command for a foreign data wrapper by its name; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_fdw_drop
  (
    fdw_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_fdw_drop("oid")
-- from pg_catalog.pg_foreign_data_wrapper
-- limit 10;

  declare

    qry text;
    in_fdw text;

  begin

    -- Checking existence of FDW
    select fdwname
    into in_fdw
    from pg_catalog.pg_foreign_data_wrapper
    where "oid" = fdw_oid;

    if in_fdw is null then
      raise warning 'Foreign data wrapper with OID % does not exist', fdw_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP FOREIGN DATA WRAPPER IF EXISTS ' || quote_ident(in_fdw) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_fdw_drop(fdw_oid oid, cascade_ bool)
    is 'Generates DROP command for a foreign data wrapper by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';