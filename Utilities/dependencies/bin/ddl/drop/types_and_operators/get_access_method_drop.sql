-- Examples:

-- select get_access_method_drop(amname),
--        get_access_method_drop("oid", false)
-- from pg_catalog.pg_am;


-- Name

create or replace function get_access_method_drop
  (
    in_am text,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_access_method_drop(amname)
-- from pg_catalog.pg_am;

  declare

    v_oid oid;
    qry text;

  begin

    -- Checking existence of access method
    select "oid"
    into v_oid
    from pg_catalog.pg_am
    where amname = in_am;

    if v_oid is null then
      raise warning 'Access method % does not exist', quote_ident(in_am);

      return null;
    end if;

    -- Result

    qry := 'DROP ACCESS METHOD IF EXISTS ' || quote_ident(in_am) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_access_method_drop(in_am text, cascade_ bool)
    is 'Generates DROP command for an access method by its name; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_access_method_drop
  (
    am_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_access_method_drop("oid")
-- from pg_catalog.pg_am;

  declare

    v_name text;
    qry text;

  begin

    -- Checking existence of access method
    select amname
    into v_name
    from pg_catalog.pg_am
    where "oid" = am_oid;

    if v_name is null then
      raise warning 'Access method with OID % does not exist', am_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP ACCESS METHOD IF EXISTS ' || quote_ident(v_name) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_access_method_drop(am_oid oid, cascade_ bool)
    is 'Generates DROP command for an access method by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';