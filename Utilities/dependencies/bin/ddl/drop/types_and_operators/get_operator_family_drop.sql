-- Examples:

-- select get_operator_family_drop(o.opfname, a.amname, n.nspname),
--        get_operator_family_drop(o."oid", false)
-- from pg_catalog.pg_opfamily o
-- join pg_catalog.pg_am a on o.opfmethod = a."oid"
-- join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace;

-- select get_operator_family_drop('text_ops')
-- union all
-- select get_operator_family_drop('text_ops', 'hash', null, false);


-- Name + schema + access method

create or replace function get_operator_family_drop
  (
    in_opf text,
    in_method text default 'btree',
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_operator_family_drop('text_ops')
-- union all
-- select get_operator_family_drop('text_ops', 'hash');

  declare

    qry text;
    v_opf_params record;

  begin

    -- Checking existence of operator family
    select o."oid",
           n.nspname
    into v_opf_params
    from pg_catalog.pg_opfamily o
    join pg_catalog.pg_am a on a."oid" = o.opfmethod
    left join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where o.opfname = in_opf
          and a.amname = in_method
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_opf_params."oid" is null then
      raise warning 'Operator family %.% with access method % does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_opf),
        quote_ident(in_method);

      return null;
    end if;

    -- Result
    qry := 'DROP OPERATOR FAMILY IF EXISTS ' || quote_ident(v_opf_params.nspname) || '.' || quote_ident(in_opf) ||
           ' USING ' || quote_ident(in_method) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_operator_family_drop(in_opf text, in_method text, in_schema text, cascade_ bool)
    is 'Generates DROP command for an operator family by its name and schema (default - search_path) + used access method (def ''btree''); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_operator_family_drop
  (
    opf_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_operator_family_drop("oid")
-- from pg_catalog.pg_opfamily
-- limit 10;

  declare

    qry text;
    v_opf_params record;
    v_access_method text;

  begin

    -- Checking existence of operator family
    select o.opfname,
           n.nspname,
           o.opfmethod
    into v_opf_params
    from pg_catalog.pg_opfamily o
    join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace
    where o."oid" = opf_oid;

    if v_opf_params.opfname is null then
      raise warning 'Operator family with OID % does not exist', opf_oid;

      return null;
    end if;

    -- Access method
    select amname::text
    into v_access_method
    from pg_catalog.pg_am
    where v_opf_params.opfmethod = "oid";

    -- Result
    qry := 'DROP OPERATOR FAMILY IF EXISTS ' || quote_ident(v_opf_params.nspname) || '.' || quote_ident(v_opf_params.opfname) ||
           ' USING ' || quote_ident(v_access_method) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_operator_family_drop(opf_oid oid, cascade_ bool)
    is 'Generates DROP command for an operator family by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';