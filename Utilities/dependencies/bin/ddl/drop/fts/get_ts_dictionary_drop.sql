-- Examples:

-- select get_ts_dictionary_drop(d.dictname, n.nspname),
--        get_ts_dictionary_drop(d."oid", false)
-- from pg_catalog.pg_ts_dict d
-- join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace;

-- select get_ts_dictionary_drop('german_stem');


-- Name + schema

create or replace function get_ts_dictionary_drop
  (
    in_tsdict text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_ts_dictionary_drop('german_stem');

-- select get_ts_dictionary_drop(dictname)
-- from pg_catalog.pg_ts_dict
-- where dictnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_tsdict_params record;

  begin

    -- Checking existence of FTS dictionary
    select d."oid",
           n.nspname
    into v_tsdict_params
    from pg_catalog.pg_ts_dict d
    left join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where d.dictname = in_tsdict
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tsdict_params."oid" is null then
      raise warning 'Text search dictionary %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tsdict);

      return null;
    end if;

    -- Result

    qry := 'DROP TEXT SEARCH DICTIONARY IF EXISTS ' || quote_ident(v_tsdict_params.nspname) || '.' || quote_ident(in_tsdict)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_ts_dictionary_drop(in_tsdict text, in_schema text, cascade_ bool)
    is 'Generates DROP command for a FTS dictionary by its name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_ts_dictionary_drop
  (
    tsdict_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_ts_dictionary_drop('german_stem'::regdictionary);

-- select get_ts_dictionary_drop("oid")
-- from pg_catalog.pg_ts_dict
-- limit 10;

  declare

    qry text;
    v_tsdict_params record;

  begin

    -- Checking existence of FTS dictionary
    select n.nspname,
           d.dictname
    into v_tsdict_params
    from pg_catalog.pg_ts_dict d
    join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
    where d."oid" = tsdict_oid;

    if v_tsdict_params.dictname is null then
      raise warning 'Text search dictionary with OID % does not exist', tsdict_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP TEXT SEARCH DICTIONARY IF EXISTS ' || quote_ident(v_tsdict_params.nspname) || '.' || quote_ident(v_tsdict_params.dictname)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_ts_dictionary_drop(tsdict_oid oid, cascade_ bool)
    is 'Generates DROP command for a FTS dictionary by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';