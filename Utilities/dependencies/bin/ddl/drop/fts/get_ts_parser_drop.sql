-- Examples:

-- select get_ts_parser_drop(t.prsname, n.nspname),
--        get_ts_parser_drop(t."oid", false)
-- from pg_catalog.pg_ts_parser t
-- join pg_catalog.pg_namespace n on n."oid" = t.prsnamespace;

-- select get_ts_parser_drop('default');


-- Name + schema

create or replace function get_ts_parser_drop
  (
    in_tsprs text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_ts_parser_drop('default');

-- select get_ts_parser_drop(prsname)
-- from pg_catalog.pg_ts_parser
-- where prsnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_tsprs_params record;

  begin

    -- Checking existence of FTS parser
    select t."oid",
           n.nspname
    into v_tsprs_params
    from pg_catalog.pg_ts_parser t
    left join pg_catalog.pg_namespace n on n."oid" = t.prsnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where t.prsname = in_tsprs
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tsprs_params."oid" is null then
      raise warning 'Text search parser %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tsprs);

      return null;
    end if;

    -- Result

    qry := 'DROP TEXT SEARCH PARSER IF EXISTS ' || quote_ident(v_tsprs_params.nspname) || '.' || quote_ident(in_tsprs)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_ts_parser_drop(in_tsprs text, in_schema text, cascade_ bool)
    is 'Generates DROP command for a FTS parser by its name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_ts_parser_drop
  (
    tsprs_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_ts_parser_drop("oid")
-- from pg_catalog.pg_ts_parser
-- limit 10;

  declare

    qry text;
    v_tsprs_params record;

  begin

    -- Checking existence of FTS parser
    select n.nspname,
           t.prsname
    into v_tsprs_params
    from pg_catalog.pg_ts_parser t
    join pg_catalog.pg_namespace n on n."oid" = t.prsnamespace
    where t."oid" = tsprs_oid;

    if v_tsprs_params.prsname is null then
      raise warning 'Text search parser with OID % does not exist', tsprs_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP TEXT SEARCH PARSER IF EXISTS ' || quote_ident(v_tsprs_params.nspname) || '.' || quote_ident(v_tsprs_params.prsname)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_ts_parser_drop(tsprs_oid oid, cascade_ bool)
    is 'Generates DROP command for a FTS parser by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';