-- Examples:

-- select get_ts_template_drop(t.tmplname, n.nspname),
--        get_ts_template_drop(t."oid", false)
-- from pg_catalog.pg_ts_template t
-- join pg_catalog.pg_namespace n on n."oid" = t.tmplnamespace;

-- select get_ts_template_drop('synonym');


-- Name + schema

create or replace function get_ts_template_drop
  (
    in_tstmpl text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_ts_template_drop('synonym');

-- select get_ts_template_drop(tmplname)
-- from pg_catalog.pg_ts_template
-- where tmplnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_tstmpl_params record;

  begin

    -- Checking existence of FTS template
    select t."oid",
           n.nspname
    into v_tstmpl_params
    from pg_catalog.pg_ts_template t
    left join pg_catalog.pg_namespace n on n."oid" = t.tmplnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where t.tmplname = in_tstmpl
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tstmpl_params."oid" is null then
      raise warning 'Text search template %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tstmpl);

      return null;
    end if;

    -- Result

    qry := 'DROP TEXT SEARCH TEMPLATE IF EXISTS ' || quote_ident(v_tstmpl_params.nspname) || '.' || quote_ident(in_tstmpl)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_ts_template_drop(in_tstmpl text, in_schema text, cascade_ bool)
    is 'Generates DROP command for a FTS template by its name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_ts_template_drop
  (
    tstmpl_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_ts_template_drop("oid")
-- from pg_catalog.pg_ts_template
-- limit 10;

  declare

    qry text;
    v_tstmpl_params record;

  begin

    -- Checking existence of FTS template
    select n.nspname,
           t.tmplname
    into v_tstmpl_params
    from pg_catalog.pg_ts_template t
    join pg_catalog.pg_namespace n on n."oid" = t.tmplnamespace
    where t."oid" = tstmpl_oid;

    if v_tstmpl_params.tmplname is null then
      raise warning 'Text search template with OID % does not exist', tstmpl_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP TEXT SEARCH TEMPLATE IF EXISTS ' || quote_ident(v_tstmpl_params.nspname) || '.' || quote_ident(v_tstmpl_params.tmplname)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_ts_template_drop(tstmpl_oid oid, cascade_ bool)
    is 'Generates DROP command for a FTS template by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';