-- Examples:

-- select get_ts_config_drop(c.cfgname, n.nspname),
--        get_ts_config_drop(c."oid", false)
-- from pg_catalog.pg_ts_config c
-- join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace;

-- select get_ts_config_drop('german');


-- Name + schema

create or replace function get_ts_config_drop
  (
    in_tscfg text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_ts_config_drop('german');

-- select get_ts_config_drop(cfgname)
-- from pg_catalog.pg_ts_config
-- where cfgnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    qry text;
    v_tscfg_params record;

  begin

    -- Checking existence of FTS configuration
    select c."oid",
           n.nspname
    into v_tscfg_params
    from pg_catalog.pg_ts_config c
    left join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.cfgname = in_tscfg
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tscfg_params."oid" is null then
      raise warning 'Text search configuration %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tscfg);

      return null;
    end if;

    -- Result

    qry := 'DROP TEXT SEARCH CONFIGURATION IF EXISTS ' || quote_ident(v_tscfg_params.nspname) || '.' || quote_ident(in_tscfg)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_ts_config_drop(in_tscfg text, in_schema text, cascade_ bool)
    is 'Generates DROP command for a FTS configuration by its name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_ts_config_drop
  (
    tscfg_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_ts_config_drop('german'::regconfig);

-- select get_ts_config_drop("oid")
-- from pg_catalog.pg_ts_config
-- limit 10;

  declare

    qry text;
    v_tscfg_params record;

  begin

    -- Checking existence of FTS configuration
    select n.nspname,
           c.cfgname
    into v_tscfg_params
    from pg_catalog.pg_ts_config c
    join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace
    where c."oid" = tscfg_oid;

    if v_tscfg_params.cfgname is null then
      raise warning 'Text search configuration with OID % does not exist', tscfg_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP TEXT SEARCH CONFIGURATION IF EXISTS ' || quote_ident(v_tscfg_params.nspname) || '.' || quote_ident(v_tscfg_params.cfgname)
           || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_ts_config_drop(tscfg_oid oid, cascade_ bool)
    is 'Generates DROP command for a FTS configuration by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';