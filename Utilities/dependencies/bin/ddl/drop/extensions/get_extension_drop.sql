-- Examples:

-- select get_extension_drop(extname),
--        get_extension_drop("oid", false)
-- from pg_catalog.pg_extension;


-- Name

create or replace function get_extension_drop
  (
    in_ext text,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_extension_drop(extname)
-- from pg_catalog.pg_extension
-- limit 10;

  declare

    qry text;
    v_oid oid;

  begin

    -- Checking existence of extension
    select "oid"
    into v_oid
    from pg_catalog.pg_extension
    where extname = in_ext;

    if v_oid is null then
      raise warning 'Extension % does not exist', quote_ident(in_ext);

      return null;
    end if;

    -- Result

    qry := 'DROP EXTENSION IF EXISTS ' || quote_ident(in_ext) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_extension_drop(in_ext text, cascade_ bool)
    is 'Generates DROP command for an extension by its name; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_extension_drop
  (
    ext_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_extension_drop("oid")
-- from pg_catalog.pg_extension
-- limit 10;

  declare

    qry text;
    in_ext text;

  begin

    -- Checking existence of extension
    select extname
    into in_ext
    from pg_catalog.pg_extension
    where "oid" = ext_oid;

    if in_ext is null then
      raise warning 'Extension with OID % does not exist', ext_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP EXTENSION IF EXISTS ' || quote_ident(in_ext) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_extension_drop(ext_oid oid, cascade_ bool)
    is 'Generates DROP command for an extension by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';