-- Examples:

-- select get_language_drop(lanname),
--        get_language_drop("oid", false)
-- from pg_catalog.pg_language;


-- Name

create or replace function get_language_drop
  (
    in_lang text,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_language_drop(lanname)
-- from pg_catalog.pg_language
-- limit 10;

  declare

    qry text;
    v_oid oid;

  begin

    -- Checking existence of language
    select "oid"
    into v_oid
    from pg_catalog.pg_language
    where lanname = in_lang;

    if v_oid is null then
      raise warning 'Language % does not exist', quote_ident(in_lang);

      return null;
    end if;

    -- Result

    qry := 'DROP LANGUAGE IF EXISTS ' || quote_ident(in_lang) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_language_drop(in_lang text, cascade_ bool)
    is 'Generates DROP command for a language by its name; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';


-- OID

create or replace function get_language_drop
  (
    lang_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_language_drop("oid")
-- from pg_catalog.pg_language
-- limit 10;

  declare

    qry text;
    in_lang text;

  begin

    -- Checking existence of language
    select lanname
    into in_lang
    from pg_catalog.pg_language
    where "oid" = lang_oid;

    if in_lang is null then
      raise warning 'Language with OID % does not exist', lang_oid;

      return null;
    end if;

    -- Result

    qry := 'DROP LANGUAGE IF EXISTS ' || quote_ident(in_lang) || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end || ';';

    return qry;

  end;

$$;

comment on function get_language_drop(lang_oid oid, cascade_ bool)
    is 'Generates DROP command for a language by its OID; if cascade_ is true (by default) then it adds CASCADE to DROP command, else - RESTRICT';