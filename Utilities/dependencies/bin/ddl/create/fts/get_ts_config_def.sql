-- Examples:

-- select get_ts_config_def(c.cfgname, n.nspname),
--        get_ts_config_def(c."oid")
-- from pg_catalog.pg_ts_config c
-- join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace;

-- select get_ts_config_def('german');


-- Name + schema

create or replace function get_ts_config_def
  (
    in_tscfg text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_ts_config_def('german');

-- select get_ts_config_def(cfgname)
-- from pg_catalog.pg_ts_config
-- where cfgnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_tscfg_ddl text;
    v_tscfg_params record;
    v_parser text;
    v_tscfg_map text;

  begin

    -- Checking existence of FTS configuration
    select c."oid",
           c.cfgparser,
           n.nspname
    into v_tscfg_params
    from pg_catalog.pg_ts_config c
    left join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.cfgname = in_tscfg
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tscfg_params."oid" is null then
      raise warning 'Text search configuration %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tscfg);

      return null;
    end if;

    -- Parser
    select quote_ident(n.nspname) || '.' || quote_ident(p.prsname)
    into v_parser
    from pg_catalog.pg_ts_parser p
    join pg_catalog.pg_namespace n on n."oid" = p.prsnamespace
    where v_tscfg_params.cfgparser = p."oid";

    -- Building DDL
    v_tscfg_ddl := 'CREATE TEXT SEARCH CONFIGURATION ' || quote_ident(v_tscfg_params.nspname)  || '.' || quote_ident(in_tscfg)
                   || E'\n(\n    PARSER = ' || v_parser || E'\n);';

    -- Checking existence and parameters of FTS configuration mapping
    select string_agg(str, '' order by maptokentype)
    into v_tscfg_map
    from
    (
        select c.maptokentype,
               E'\n\nALTER TEXT SEARCH CONFIGURATION ' || quote_ident(v_tscfg_params.nspname) || '.' || quote_ident(in_tscfg) ||
               E'\n    ADD MAPPING FOR ' || t.alias || ' WITH ' ||
               string_agg(quote_ident(n.nspname) || '.' || quote_ident(d.dictname), ', ' order by c.mapseqno) || ';' as str
        from pg_catalog.pg_ts_config_map c
        join pg_catalog.pg_ts_dict d on c.mapdict = d."oid"
        join pg_catalog.ts_token_type(v_tscfg_params.cfgparser) t on t.tokid = c.maptokentype
        join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
        where c.mapcfg = v_tscfg_params."oid"
        group by c.maptokentype,
                 t.alias
    ) a;

    v_tscfg_ddl := v_tscfg_ddl || v_tscfg_map;

    return v_tscfg_ddl;

  end;

$$;

comment on function get_ts_config_def(in_tscfg text, in_schema text)
    is 'Generates CREATE command for a FTS configuration by its name and schema (default - search_path)';


-- OID

create or replace function get_ts_config_def
  (
    tscfg_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_ts_config_def('german'::regconfig);

-- select get_ts_config_def("oid")
-- from pg_catalog.pg_ts_config
-- limit 10;

  declare

    v_tscfg_ddl text;
    v_tscfg_params record;
    v_parser text;
    v_tscfg_map text;

  begin

    -- Checking existence of FTS configuration
    select n.nspname,
           c.cfgname,
           c.cfgparser
    into v_tscfg_params
    from pg_catalog.pg_ts_config c
    join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace
    where c."oid" = tscfg_oid;

    if v_tscfg_params.cfgname is null then
      raise warning 'Text search configuration with OID % does not exist', tscfg_oid;

      return null;
    end if;

    -- Parser
    select quote_ident(n.nspname) || '.' || quote_ident(p.prsname)
    into v_parser
    from pg_catalog.pg_ts_parser p
    join pg_catalog.pg_namespace n on n."oid" = p.prsnamespace
    where v_tscfg_params.cfgparser = p."oid";

    -- Building DDL
    v_tscfg_ddl := 'CREATE TEXT SEARCH CONFIGURATION ' || quote_ident(v_tscfg_params.nspname) || '.' || quote_ident(v_tscfg_params.cfgname)
                   || E'\n(\n    PARSER = ' || v_parser || E'\n);';

    -- Checking existence and parameters of FTS configuration mapping
    select string_agg(str, '' order by maptokentype)
    into v_tscfg_map
    from
    (
        select c.maptokentype,
               E'\n\nALTER TEXT SEARCH CONFIGURATION ' || quote_ident(v_tscfg_params.nspname) || '.' || quote_ident(v_tscfg_params.cfgname) ||
               E'\n    ADD MAPPING FOR ' || t.alias || ' WITH ' ||
               string_agg(quote_ident(n.nspname) || '.' || quote_ident(d.dictname), ', ' order by c.mapseqno) || ';' as str
        from pg_catalog.pg_ts_config_map c
        join pg_catalog.pg_ts_dict d on c.mapdict = d."oid"
        join pg_catalog.ts_token_type(v_tscfg_params.cfgparser) t on t.tokid = c.maptokentype
        join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
        where c.mapcfg = tscfg_oid
        group by c.maptokentype,
                 t.alias
    ) a;

    v_tscfg_ddl := v_tscfg_ddl || v_tscfg_map;

    return v_tscfg_ddl;

  end;

$$;

comment on function get_ts_config_def(tscfg_oid oid)
    is 'Generates CREATE command for a FTS configuration by its OID';