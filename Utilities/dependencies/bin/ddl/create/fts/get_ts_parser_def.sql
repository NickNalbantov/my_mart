-- Examples:

-- select get_ts_parser_def(t.prsname, n.nspname),
--        get_ts_parser_def(t."oid")
-- from pg_catalog.pg_ts_parser t
-- join pg_catalog.pg_namespace n on n."oid" = t.prsnamespace;

-- select get_ts_parser_def('default');


-- Name + schema

create or replace function get_ts_parser_def
  (
    in_tsprs text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_ts_parser_def('default');

-- select get_ts_parser_def(prsname)
-- from pg_catalog.pg_ts_parser
-- where prsnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_tsprs_ddl text;
    v_tsprs_params record;
    v_start text;
    v_gettoken text;
    v_end text;
    v_lextypes text;
    v_headline text;

  begin

    -- Checking existence of FTS parser
    select t."oid",
           t.prsstart,
           t.prstoken,
           t.prsend,
           t.prsheadline,
           t.prslextype,
           n.nspname
    into v_tsprs_params
    from pg_catalog.pg_ts_parser t
    left join pg_catalog.pg_namespace n on n."oid" = t.prsnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where t.prsname = in_tsprs
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tsprs_params."oid" is null then
      raise warning 'Text search parser %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tsprs);

      return null;
    end if;

    -- Functions
    select case when v_tsprs_params.prsstart != '-'::regproc then 'START = ' || get_function_identity_arguments(v_tsprs_params.prsstart, false, false) else '' end,
           case when v_tsprs_params.prstoken != '-'::regproc then E',\n    GETTOKEN = ' || get_function_identity_arguments(v_tsprs_params.prstoken, false, false) else '' end,
           case when v_tsprs_params.prsend != '-'::regproc then E',\n    END = ' || get_function_identity_arguments(v_tsprs_params.prsend, false, false) else '' end,
           case when v_tsprs_params.prslextype != '-'::regproc then E',\n    LEXTYPES = ' || get_function_identity_arguments(v_tsprs_params.prslextype, false, false) else '' end,
           case when v_tsprs_params.prsheadline != '-'::regproc then E',\n    HEADLINE = ' || get_function_identity_arguments(v_tsprs_params.prsheadline, false, false) else '' end
    into v_start,
         v_gettoken,
         v_end,
         v_lextypes,
         v_headline;

    -- Building DDL
    v_tsprs_ddl := 'CREATE TEXT SEARCH PARSER ' || quote_ident(v_tsprs_params.nspname) || '.' || quote_ident(in_tsprs) || E'\n(\n    '
                   || v_start
                   || v_gettoken
                   || v_end
                   || v_lextypes
                   || v_headline
                   || E'\n);';

    return v_tsprs_ddl;

  end;

$$;

comment on function get_ts_parser_def(in_tsprs text, in_schema text)
    is 'Generates CREATE command for a FTS parser by its name and schema (default - search_path)';


-- OID

create or replace function get_ts_parser_def
  (
    tsprs_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_ts_parser_def("oid")
-- from pg_catalog.pg_ts_parser
-- limit 10;

  declare

    v_tsprs_ddl text;
    v_tsprs_params record;
    v_start text;
    v_gettoken text;
    v_end text;
    v_lextypes text;
    v_headline text;

  begin

    -- Checking existence of FTS parser
    select n.nspname,
           t.prsname,
           t.prsstart,
           t.prstoken,
           t.prsend,
           t.prsheadline,
           t.prslextype
    into v_tsprs_params
    from pg_catalog.pg_ts_parser t
    join pg_catalog.pg_namespace n on n."oid" = t.prsnamespace
    where t."oid" = tsprs_oid;

    if v_tsprs_params.prsname is null then
      raise warning 'Text search parser with OID % does not exist', tsprs_oid;

      return null;
    end if;

    -- Functions
    select case when v_tsprs_params.prsstart != '-'::regproc then 'START = ' || get_function_identity_arguments(v_tsprs_params.prsstart, false, false) else '' end,
           case when v_tsprs_params.prstoken != '-'::regproc then E',\n    GETTOKEN = ' || get_function_identity_arguments(v_tsprs_params.prstoken, false, false) else '' end,
           case when v_tsprs_params.prsend != '-'::regproc then E',\n    END = ' || get_function_identity_arguments(v_tsprs_params.prsend, false, false) else '' end,
           case when v_tsprs_params.prslextype != '-'::regproc then E',\n    LEXTYPES = ' || get_function_identity_arguments(v_tsprs_params.prslextype, false, false) else '' end,
           case when v_tsprs_params.prsheadline != '-'::regproc then E',\n    HEADLINE = ' || get_function_identity_arguments(v_tsprs_params.prsheadline, false, false) else '' end
    into v_start,
         v_gettoken,
         v_end,
         v_lextypes,
         v_headline;

    -- Building DDL
    v_tsprs_ddl := 'CREATE TEXT SEARCH PARSER ' || quote_ident(v_tsprs_params.nspname) || '.' || quote_ident(v_tsprs_params.prsname) || E'\n(\n    '
                   || v_start
                   || v_gettoken
                   || v_end
                   || v_lextypes
                   || v_headline
                   || E'\n);';

    return v_tsprs_ddl;

  end;

$$;

comment on function get_ts_parser_def(tsprs_oid oid)
    is 'Generates CREATE command for a FTS parser by its OID';