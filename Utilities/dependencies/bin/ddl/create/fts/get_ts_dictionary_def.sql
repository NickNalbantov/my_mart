-- Examples:

-- select get_ts_dictionary_def(d.dictname, n.nspname),
--        get_ts_dictionary_def(d."oid")
-- from pg_catalog.pg_ts_dict d
-- join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace;

-- select get_ts_dictionary_def('german_stem');


-- Name + schema

create or replace function get_ts_dictionary_def
  (
    in_tsdict text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_ts_dictionary_def('german_stem');

-- select get_ts_dictionary_def(dictname)
-- from pg_catalog.pg_ts_dict
-- where dictnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_tsdict_ddl text;
    v_tsdict_params record;
    v_tstemplate text;
    v_options text;

  begin

    -- Checking existence of FTS dictionary
    select d."oid",
           d.dicttemplate,
           d.dictinitoption,
           n.nspname
    into v_tsdict_params
    from pg_catalog.pg_ts_dict d
    left join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where d.dictname = in_tsdict
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tsdict_params."oid" is null then
      raise warning 'Text search dictionary %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tsdict);

      return null;
    end if;

    -- Template
    select quote_ident(n.nspname) || '.' || quote_ident(t.tmplname)
    into v_tstemplate
    from pg_catalog.pg_ts_template t
    join pg_catalog.pg_namespace n on n."oid" = t.tmplnamespace
    where v_tsdict_params.dicttemplate = t."oid";

    -- Options
    if v_tsdict_params.dictinitoption is not null then
      select E',\n    ' || string_agg(upper(split_part(opt, '=', 1)) || '=' || split_part(opt, '=', 2), E',\n    ')
      into v_options
      from
      (
        select regexp_split_to_table(v_tsdict_params.dictinitoption, ', ') as opt
      ) a;
    else
      v_options := '';
    end if;

    -- Building DDL
    v_tsdict_ddl := 'CREATE TEXT SEARCH DICTIONARY ' || quote_ident(v_tsdict_params.nspname) || '.' || quote_ident(in_tsdict)
                    || E'\n(\n    TEMPLATE = '
                    || v_tstemplate
                    || v_options
                    || E'\n);';

    return v_tsdict_ddl;

  end;

$$;

comment on function get_ts_dictionary_def(in_tsdict text, in_schema text)
    is 'Generates CREATE command for a FTS dictionary by its name and schema (default - search_path)';


-- OID

create or replace function get_ts_dictionary_def
  (
    tsdict_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_ts_dictionary_def('german_stem'::regdictionary);

-- select get_ts_dictionary_def("oid")
-- from pg_catalog.pg_ts_dict
-- limit 10;

  declare

    v_tsdict_ddl text;
    v_tsdict_params record;
    v_tstemplate text;
    v_options text;

  begin

    -- Checking existence of FTS dictionary
    select d."oid", n.nspname, d.dictname, d.dicttemplate, d.dictinitoption
    into v_tsdict_params
    from pg_catalog.pg_ts_dict d
    join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
    where d."oid" = tsdict_oid;

    if v_tsdict_params.dictname is null then
      raise warning 'Text search dictionary with OID % does not exist', tsdict_oid;

      return null;
    end if;

    -- Template
    select quote_ident(n.nspname) || '.' || quote_ident(t.tmplname)
    into v_tstemplate
    from pg_catalog.pg_ts_template t
    join pg_catalog.pg_namespace n on n."oid" = t.tmplnamespace
    where v_tsdict_params.dicttemplate = t."oid";

    -- Options
    if v_tsdict_params.dictinitoption is not null then
      select E',\n    ' || string_agg(upper(split_part(opt, '=', 1)) || '=' || split_part(opt, '=', 2), E',\n    ')
      into v_options
      from
      (
        select regexp_split_to_table(v_tsdict_params.dictinitoption, ', ') as opt
      ) a;
    else
      v_options := '';
    end if;

    -- Building DDL
    v_tsdict_ddl := 'CREATE TEXT SEARCH DICTIONARY ' || quote_ident(v_tsdict_params.nspname) || '.' || quote_ident(v_tsdict_params.dictname)
                    || E'\n(\n    TEMPLATE = '
                    || v_tstemplate
                    || v_options
                    || E'\n);';

    return v_tsdict_ddl;

  end;

$$;

comment on function get_ts_dictionary_def(tsdict_oid oid)
    is 'Generates CREATE command for a FTS dictionary by its OID';