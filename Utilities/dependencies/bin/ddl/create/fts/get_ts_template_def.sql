-- Examples:

-- select get_ts_template_def(t.tmplname, n.nspname),
--        get_ts_template_def(t."oid")
-- from pg_catalog.pg_ts_template t
-- join pg_catalog.pg_namespace n on n."oid" = t.tmplnamespace;

-- select get_ts_template_def('synonym');


-- Name + schema

create or replace function get_ts_template_def
  (
    in_tstmpl text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_ts_template_def('synonym');

-- select get_ts_template_def(tmplname)
-- from pg_catalog.pg_ts_template
-- where tmplnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_tstmpl_ddl text;
    v_tstmpl_params record;
    v_init text;
    v_lexize text;

  begin

    -- Checking existence of FTS template
    select t."oid",
           t.tmplinit,
           t.tmpllexize,
           n.nspname
    into v_tstmpl_params
    from pg_catalog.pg_ts_template t
    left join pg_catalog.pg_namespace n on n."oid" = t.tmplnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where t.tmplname = in_tstmpl
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tstmpl_params."oid" is null then
      raise warning 'Text search template %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tstmpl);

      return null;
    end if;

    -- Functions
    select case when v_tstmpl_params.tmpllexize != '-'::regproc then 'LEXIZE = ' || get_function_identity_arguments(v_tstmpl_params.tmpllexize, false, false) else '' end,
           case when v_tstmpl_params.tmplinit != '-'::regproc then E',\n    INIT = ' || get_function_identity_arguments(v_tstmpl_params.tmplinit, false, false) else '' end
    into v_lexize,
         v_init;

    -- Building DDL
    v_tstmpl_ddl := 'CREATE TEXT SEARCH TEMPLATE ' || quote_ident(v_tstmpl_params.nspname) || '.' || quote_ident(in_tstmpl)
                    || E'\n(\n    '
                    || v_lexize
                    || v_init
                    || E'\n);';

    return v_tstmpl_ddl;

  end;

$$;

comment on function get_ts_template_def(in_tstmpl text, in_schema text)
    is 'Generates CREATE command for a FTS template by its name and schema (default - search_path)';


-- OID

create or replace function get_ts_template_def
  (
    tstmpl_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_ts_template_def("oid")
-- from pg_catalog.pg_ts_template
-- limit 10;

  declare

    v_tstmpl_ddl text;
    v_tstmpl_params record;
    v_init text;
    v_lexize text;

  begin

    -- Checking existence of FTS template
    select n.nspname,
           t.tmplname,
           t.tmplinit,
           t.tmpllexize
    into v_tstmpl_params
    from pg_catalog.pg_ts_template t
    join pg_catalog.pg_namespace n on n."oid" = t.tmplnamespace
    where t."oid" = tstmpl_oid;

    if v_tstmpl_params.tmplname is null then
      raise warning 'Text search template with OID % does not exist', tstmpl_oid;

      return null;
    end if;

    -- Functions
    select case when v_tstmpl_params.tmpllexize != '-'::regproc then 'LEXIZE = ' || get_function_identity_arguments(v_tstmpl_params.tmpllexize, false, false) else '' end,
           case when v_tstmpl_params.tmplinit != '-'::regproc then E',\n    INIT = ' || get_function_identity_arguments(v_tstmpl_params.tmplinit, false, false) else '' end
    into v_lexize,
         v_init;

    -- Building DDL
    v_tstmpl_ddl := 'CREATE TEXT SEARCH TEMPLATE ' || quote_ident(v_tstmpl_params.nspname) || '.' || quote_ident(v_tstmpl_params.tmplname)
                    || E'\n(\n    '
                    || v_lexize
                    || v_init
                    || E'\n);';

    return v_tstmpl_ddl;

  end;

$$;

comment on function get_ts_template_def(tstmpl_oid oid)
    is 'Generates CREATE command for a FTS template by its OID';