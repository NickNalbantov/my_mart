-- Examples:

-- select get_server_def(srvname),
--        get_server_def("oid")
-- from pg_catalog.pg_foreign_server;


-- Name

create or replace function get_server_def
  (
    in_srv text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_server_def(srvname)
-- from pg_catalog.pg_foreign_server
-- limit 10;

  declare

    v_srv_ddl text;
    v_srv_params record;
    v_options text;

  begin

    -- Checking existence of server
    select s."oid",
           w.fdwname,
           s.srvtype,
           s.srvversion,
           s.srvoptions
    into v_srv_params
    from pg_catalog.pg_foreign_server s
    join pg_catalog.pg_foreign_data_wrapper w on s.srvfdw = w."oid"
    where s.srvname = in_srv;

    if v_srv_params."oid" is null then
      raise warning 'Foreign server % does not exist', quote_ident(in_srv);

      return null;
    end if;

    -- Options
    if v_srv_params.srvoptions is not null then
      select E'\n    OPTIONS (' || string_agg(str, ', ') || ')'
      into v_options
      from
      (
        select regexp_replace(unnest(v_srv_params.srvoptions), '^(.+)=(.+)$', '\1 ''\2''') as str
      ) a;
    else
      v_options := '';
    end if;

    -- Building DDL

    v_srv_ddl := 'CREATE SERVER IF NOT EXISTS ' || quote_ident(in_srv) || E'\n    FOREIGN DATA WRAPPER ' || quote_ident(v_srv_params.fdwname)
                 || case when v_srv_params.srvtype is not null then E'\n    TYPE '''  || v_srv_params.srvtype  || '''' else '' end
                 || case when v_srv_params.srvversion is not null then E'\n    VERSION '''  || v_srv_params.srvversion  || '''' else '' end
                 || v_options || ';';

    return v_srv_ddl;

  end;

$$;

comment on function get_server_def(in_srv text)
    is 'Generates CREATE command for a server by its name';


-- OID

create or replace function get_server_def
  (
    srv_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_server_def("oid")
-- from pg_catalog.pg_foreign_server
-- limit 10;

  declare

    v_srv_ddl text;
    v_srv_params record;
    v_options text;

  begin

    -- Checking existence of server
    select s.srvname,
           w.fdwname,
           s.srvtype,
           s.srvversion,
           s.srvoptions
    into v_srv_params
    from pg_catalog.pg_foreign_server s
    join pg_catalog.pg_foreign_data_wrapper w on s.srvfdw = w."oid"
    where s."oid" = srv_oid;

    if v_srv_params.srvname is null then
      raise warning 'Foreign server with OID % does not exist', srv_oid;

      return null;
    end if;

    -- Options
    if v_srv_params.srvoptions is not null then
      select E'\n    OPTIONS (' || string_agg(str, ', ') || ')'
      into v_options
      from
      (
        select regexp_replace(unnest(v_srv_params.srvoptions), '^(.+)=(.+)$', '\1 ''\2''') as str
      ) a;
    else
      v_options := '';
    end if;

    -- Building DDL

    v_srv_ddl := 'CREATE SERVER IF NOT EXISTS ' || quote_ident(v_srv_params.srvname) || E'\n    FOREIGN DATA WRAPPER ' || quote_ident(v_srv_params.fdwname)
                 || case when v_srv_params.srvtype is not null then E'\n    TYPE '''  || v_srv_params.srvtype  || '''' else '' end
                 || case when v_srv_params.srvversion is not null then E'\n    VERSION '''  || v_srv_params.srvversion  || '''' else '' end
                 || v_options || ';';

    return v_srv_ddl;

  end;

$$;

comment on function get_server_def(srv_oid oid)
    is 'Generates CREATE command for a server by its OID';