-- Examples:

-- select get_user_mapping_def(s.srvname, a.rolname),
--        get_user_mapping_def(u."oid"),
--        get_user_mapping_def(s."oid", a."oid")
-- from pg_catalog.pg_user_mapping u
-- join pg_catalog.pg_foreign_server s on u.umserver = s."oid"
-- join (
--            select "oid",
--               rolname
--            from pg_catalog.pg_authid
--            union all
--            select 0::oid as "oid",
--               'public' as rolname
--        ) a on u.umuser = a."oid";


-- Server name + role name

create or replace function get_user_mapping_def
  (
    in_srv text,
    in_role text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_user_mapping_def(srvname, 'postgres')
-- from pg_catalog.pg_foreign_server
-- limit 10;

  declare

    v_um_ddl text;
    v_um_role record;
    v_um_params record;
    v_options text;

  begin

    -- Checking existence of user mapping
    select u."oid",
           a.rolname,
           u.umoptions
    into v_um_params
    from pg_catalog.pg_user_mapping u
    join pg_catalog.pg_foreign_server s on u.umserver = s."oid"
    join (
            select "oid",
                   rolname
            from pg_catalog.pg_authid
            union all
            select 0::oid as "oid",
                   'public' as rolname
         ) a on u.umuser = a."oid"
    where s.srvname = in_srv
          and in_role = a.rolname;

    if v_um_params."oid" is null then
      raise warning 'User mapping for role % at server % does not exist',
        quote_ident(in_role),
        quote_ident(in_srv);

      return null;
    end if;

    -- Options
    if v_um_params.umoptions is not null then
      select E'\n    OPTIONS (' || string_agg(str, ', ') || ')'
      into v_options
      from
      (
        select regexp_replace(unnest(v_um_params.umoptions), '^(.+)=(.+)$', '\1 ''\2''') as str
      ) a;
    else
      v_options := '';
    end if;

    -- Building DDL

    v_um_ddl := 'CREATE USER MAPPING IF NOT EXISTS FOR ' || quote_ident(in_role) || E'\n    SERVER ' || quote_ident(in_srv) || v_options || ';';

    return v_um_ddl;

  end;

$$;

comment on function get_user_mapping_def(in_srv text, in_role text)
    is 'Generates CREATE command for an user mapping by server name and role name';


-- OID

create or replace function get_user_mapping_def
  (
    um_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_user_mapping_def("oid")
-- from pg_catalog.pg_user_mapping
-- limit 10;

  declare

    v_um_ddl text;
    v_um_params record;
    v_options text;

  begin

    -- Checking existence of user mapping
    select u."oid",
           a.rolname,
           s.srvname,
           u.umoptions
    into v_um_params
    from pg_catalog.pg_user_mapping u
    join pg_catalog.pg_foreign_server s on u.umserver = s."oid"
    join (
            select "oid",
                   rolname
            from pg_catalog.pg_authid
            union all
            select 0::oid as "oid",
                   'public' as rolname
         ) a on u.umuser = a."oid"
    where u."oid" = um_oid;

    if v_um_params."oid" is null then
      raise warning 'User mapping with OID % does not exist', um_oid;

      return null;
    end if;

    -- Options
    if v_um_params.umoptions is not null then
      select E'\n    OPTIONS (' || string_agg(str, ', ') || ')'
      into v_options
      from
      (
        select regexp_replace(unnest(v_um_params.umoptions), '^(.+)=(.+)$', '\1 ''\2''') as str
      ) a;
    else
      v_options := '';
    end if;

    -- Building DDL

    v_um_ddl := 'CREATE USER MAPPING IF NOT EXISTS FOR ' || quote_ident(v_um_params.rolname) || E'\n    SERVER ' || quote_ident(v_um_params.srvname) || v_options || ';';

    return v_um_ddl;

  end;

$$;

comment on function get_user_mapping_def(um_oid oid)
    is 'Generates CREATE command for an user mapping by its OID';


-- Server and role OIDs

create or replace function get_user_mapping_def
  (
    srv_oid oid,
    rol_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_user_mapping_def("oid", 'postgres'::regrole)
-- from pg_catalog.pg_foreign_server
-- limit 10;

  declare

    v_um_ddl text;
    v_um_params record;
    v_options text;

  begin

    -- Checking existence of user mapping
    select u."oid",
           a.rolname,
           s.srvname,
           u.umoptions
    into v_um_params
    from pg_catalog.pg_user_mapping u
    join pg_catalog.pg_foreign_server s on u.umserver = s."oid"
    join (
            select "oid",
                   rolname
            from pg_catalog.pg_authid
            union all
            select 0::oid as "oid",
                   'public' as rolname
         ) a on u.umuser = a."oid"
    where s."oid" = srv_oid
          and a."oid" = rol_oid;

    if v_um_params."oid" is null then
      raise warning 'User mapping for server and role with OIDs % and % does not exist', srv_oid, rol_oid;

      return null;
    end if;

    -- Options
    if v_um_params.umoptions is not null then
      select E'\n    OPTIONS (' || string_agg(str, ', ') || ')'
      into v_options
      from
      (
        select regexp_replace(unnest(v_um_params.umoptions), '^(.+)=(.+)$', '\1 ''\2''') as str
      ) a;
    else
      v_options := '';
    end if;

    -- Building DDL

    v_um_ddl := 'CREATE USER MAPPING IF NOT EXISTS FOR ' || quote_ident(v_um_params.rolname) || E'\n    SERVER ' || quote_ident(v_um_params.srvname) || v_options || ';';

    return v_um_ddl;

  end;

$$;

comment on function get_user_mapping_def(srv_oid oid, rol_oid oid)
    is 'Generates CREATE command for an user mapping by OIDs of server and role';