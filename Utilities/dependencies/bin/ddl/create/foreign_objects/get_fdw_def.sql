-- Examples:

-- select get_fdw_def(fdwname),
--        get_fdw_def("oid")
-- from pg_catalog.pg_foreign_data_wrapper;


-- Name

create or replace function get_fdw_def
  (
    in_fdw text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_fdw_def(fdwname)
-- from pg_catalog.pg_foreign_data_wrapper
-- limit 10;

  declare

    v_fdw_ddl text;
    v_fdw_params record;
    v_handler text;
    v_validator text;
    v_options text;

  begin

    -- Checking existence of FDW
    select "oid",
           fdwhandler,
           fdwvalidator,
           fdwoptions
    into v_fdw_params
    from pg_catalog.pg_foreign_data_wrapper
    where fdwname = in_fdw;

    if v_fdw_params."oid" is null then
      raise warning 'Foreign data wrapper % does not exist', quote_ident(in_fdw);

      return null;
    end if;

    -- Functions
    if v_fdw_params.fdwhandler != 0 then
      select '    HANDLER ' || get_function_identity_arguments(v_fdw_params.fdwhandler, false, false) || E'\n'
      into v_handler;
    else
      v_handler := E'    NO HANDLER\n';
    end if;

    if v_fdw_params.fdwvalidator != 0 then
      select '    VALIDATOR ' || get_function_identity_arguments(v_fdw_params.fdwvalidator, false, false)
      into v_validator;
    else
      v_validator := '    NO VALIDATOR';
    end if;

    -- Опции
    if v_fdw_params.fdwoptions is not null then
      select E'\n    OPTIONS (' || string_agg(str, ', ') || ')'
      into v_options
      from
      (
        select regexp_replace(unnest(v_fdw_params.fdwoptions), '^(.+)=(.+)$', '\1 ''\2''') as str
      ) a;
    else
      v_options := '';
    end if;

    -- Building DDL

    v_fdw_ddl := 'CREATE FOREIGN DATA WRAPPER ' || quote_ident(in_fdw) || E'\n'
                 || v_handler
                 || v_validator
                 || v_options
                 || ';';

    return v_fdw_ddl;

  end;

$$;

comment on function get_fdw_def(in_fdw text)
    is 'Generates CREATE command for a foreign data wrapper by its name';


-- OID

create or replace function get_fdw_def
  (
    fdw_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_fdw_def("oid")
-- from pg_catalog.pg_foreign_data_wrapper
-- limit 10;

  declare

    v_fdw_ddl text;
    v_fdw_params record;
    v_handler text;
    v_validator text;
    v_options text;

  begin

    -- Checking existence of FDW
    select fdwname,
           fdwhandler,
           fdwvalidator,
           fdwoptions
    into v_fdw_params
    from pg_catalog.pg_foreign_data_wrapper
    where "oid" = fdw_oid;

    if v_fdw_params.fdwname is null then
      raise warning 'Foreign data wrapper with OID % does not exist', fdw_oid;

      return null;
    end if;

    -- Functions
    if v_fdw_params.fdwhandler != 0 then
      select '    HANDLER ' || get_function_identity_arguments(v_fdw_params.fdwhandler, false, false) || E'\n'
      into v_handler;
    else
      v_handler := E'    NO HANDLER\n';
    end if;

    if v_fdw_params.fdwvalidator != 0 then
      select '    VALIDATOR ' || get_function_identity_arguments(v_fdw_params.fdwvalidator, false, false)
      into v_validator;
    else
      v_validator := '    NO VALIDATOR';
    end if;

    -- Опции
    if v_fdw_params.fdwoptions is not null then
      select E'\n    OPTIONS (' || string_agg(regexp_replace(unnest(v_fdw_params.fdwoptions), '^(.+)=(.+)$', '\1 ''\2'''), ', ') || ')'
      into v_options
      from
      (
        select regexp_replace(unnest(v_fdw_params.fdwoptions), '^(.+)=(.+)$', '\1 ''\2''') as str
      ) a;
    else
      v_options := '';
    end if;

    -- Building DDL

    v_fdw_ddl := 'CREATE FOREIGN DATA WRAPPER ' || quote_ident(v_fdw_params.fdwname) || E'\n'
                 || v_handler
                 || v_validator
                 || v_options
                 || ';';

    return v_fdw_ddl;

  end;

$$;

comment on function get_fdw_def(fdw_oid oid)
    is 'Generates CREATE command for a foreign data wrapper by its OID';