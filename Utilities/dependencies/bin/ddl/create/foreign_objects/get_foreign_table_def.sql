-- Examples:

-- select get_foreign_table_def(c.relname, n.nspname),
--        get_foreign_table_def(c."oid")
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where c.relkind = 'f';


-- Name + schema

create or replace function get_foreign_table_def
  (
    in_table text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Generates DDL script for foreign tables only
  -- For other types of tables use get_table_def functions

  declare

    v_table_ddl text;
    v_alter text := '';

    v_table_params record;
    v_att_params record;
    v_constraint_rec record;
    v_partition_key text := '';
    v_partbound text;
    v_parent text;

    v_is_partition boolean;
    v_inherits boolean;
    v_is_child boolean;

    v_ft_params record;

  begin

    -- Checking existence of table
    select c."oid",
           n.nspname,
           c.reloptions,
           c.relpersistence,
           c.relkind,
           c.relispartition,
           c.relpartbound
    into v_table_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.relkind = 'f'
          and c.relname = in_table
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_table_params."oid" is null then
      raise warning 'Foreign table %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_table);

      return null;
    end if;

    -- Partitioning
        -- inheritance: pg_class.relkind = 'r', pg_class.relispartition = false, pg_class.relpartbound is null
        -- declarative: pg_class.relkind = 'r', pg_class.relispartition = true, pg_class.relpartbound is not null

    v_partbound := '';
    v_is_partition := false;
    v_inherits := false;

    select quote_ident(n.nspname) || '.' || quote_ident(c.relname),
           v_table_params.relispartition,
           pg_catalog.pg_get_expr(v_table_params.relpartbound, v_table_params."oid", true)
    into v_parent,
         v_is_child,
         v_partbound
    from pg_catalog.pg_inherits i
    join pg_catalog.pg_class c on i.inhparent = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where i.inhrelid = v_table_params."oid";

    if v_parent is not null then
      v_is_partition := true;

      if v_is_child is true then
        v_inherits := false;
      else
        v_inherits := true;
      end if;

    end if;

    -- Partitions' definition
    if v_is_partition is true then

      if v_inherits is true then
        -- inheritance
        v_table_ddl := 'CREATE FOREIGN TABLE IF NOT EXISTS ' || quote_ident(v_table_params.nspname) || '.' || quote_ident(in_table) || E'\n(';

      else
        -- declarative
          v_table_ddl := 'CREATE FOREIGN TABLE IF NOT EXISTS ' || quote_ident(v_table_params.nspname) || '.' || quote_ident(in_table)
                         || E'\nPARTITION OF ' || v_parent || E'\n(';
      end if;

    else
      -- Simple tables
      v_table_ddl := 'CREATE FOREIGN TABLE IF NOT EXISTS ' || quote_ident(v_table_params.nspname) || '.' || quote_ident(in_table) || E'\n(';

    end if;

    -- Columns' definitions

    for v_att_params in

      select a.attname,
             a.attstattarget,
             a.attndims,
             a.atttypmod,
             a.attnotnull,
             a.atthasdef,
             a.attgenerated,
             a.attstorage,
             string_agg(replace(ao.opts, '=', ' = '), ', ' order by ao.rn) as attoptions,
             ' OPTIONS (' || string_agg(regexp_replace(afo.opts, '^(.+)=(.+)$', '\1 ''\2'''), ', ' order by afo.rn) || ')' as attfdwoptions,
             t."oid" as typoid,
             t.typname,
             t.typtype,
             t.typstorage,
             t.typnotnull,
             nt.nspname as typschema,
             c.collname,
             nc.nspname as collschema,
             pg_catalog.pg_get_expr(ad.adbin, v_table_params."oid", true) as gen_expr
      from pg_catalog.pg_attribute a
      join pg_catalog.pg_type t on t."oid" = a.atttypid
      join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
      left join pg_catalog.pg_attrdef ad on ad.adrelid = v_table_params."oid"
                                            and ad.adnum = a.attnum
      left join pg_catalog.pg_collation c on c."oid" = a.attcollation
      left join pg_catalog.pg_namespace nc on nc."oid" = c.collnamespace
      left join lateral unnest(a.attoptions) with ordinality ao (opts, rn) on 1 = 1
      left join lateral unnest(a.attfdwoptions) with ordinality afo (opts, rn) on 1 = 1
      where a.attrelid = v_table_params."oid"
            and a.attnum > 0
            and a.attisdropped is false
      group by a.attnum,
               a.attname,
               a.attstattarget,
               a.attndims,
               a.atttypmod,
               a.attstorage,
               a.attnotnull,
               a.atthasdef,
               a.attgenerated,
               a.attcollation,
               t."oid",
               t.typname,
               t.typtype,
               t.typnotnull,
               t.typstorage,
               nt.nspname,
               c.collname,
               nc.nspname,
               ad.adbin
      order by a.attnum

    loop

      -- Simple tables and inherited tables
      if v_is_partition is false or v_inherits is true then

        v_table_ddl := v_table_ddl || E'\n    '
                       || quote_ident(v_att_params.attname) || ' '
                       || quote_ident(v_att_params.typschema) || '.'
                       || case when v_att_params.attndims > 1 then quote_ident(substring(v_att_params.typname, 2, length(v_att_params.typname)))
                          else quote_ident(v_att_params.typname) end
                       || case when v_att_params.atttypmod != '-1'::integer then '(' ||
                            -- bpchar, varchar max length
                            case when v_att_params.typoid = any(array['pg_catalog._bpchar'::regtype, 'pg_catalog._varchar'::regtype, 'pg_catalog.bpchar'::regtype, 'pg_catalog."varchar"'::regtype])
                                      then (v_att_params.atttypmod - 4)::text
                                 when v_att_params.typoid = any(array['pg_catalog."bit"'::regtype, 'pg_catalog._bit'::regtype, 'pg_catalog.varbit'::regtype, 'pg_catalog._varbit'::regtype])
                                      then v_att_params.atttypmod::text
                            -- numeric precision
                                 when v_att_params.typoid = any(array['pg_catalog."numeric"'::regtype, 'pg_catalog._numeric'::regtype])
                                      then (((v_att_params.atttypmod - 4) >> 16) & 65535)::text
                            -- timestamp, timestamptz, time, timetz precision
                                 when v_att_params.typoid = any(array['pg_catalog."time"'::regtype, 'pg_catalog."timestamp"'::regtype, 'pg_catalog.timestamptz'::regtype, 'pg_catalog.timetz'::regtype,
                                                                      'pg_catalog._time'::regtype, 'pg_catalog._timestamp'::regtype, 'pg_catalog._timestamptz'::regtype, 'pg_catalog._timetz'::regtype
                                                                     ])
                                      then
                                          case when v_att_params.atttypmod < 0 then 6::text
                                          else v_att_params.atttypmod::text end
                            -- interval precision
                                 when v_att_params.typoid = any(array['pg_catalog."interval"'::regtype, 'pg_catalog._interval'::regtype])
                                      then
                                          case when v_att_params.atttypmod < 0 or v_att_params.atttypmod & 65535 = 65535 then 6::text
                                          else (v_att_params.atttypmod & 65535)::text end
                            else '' end
                          else '' end
                       || case when v_att_params.atttypmod != '-1'::integer then
                            -- numeric scale
                            case when v_att_params.typoid = any(array['pg_catalog."numeric"'::regtype, 'pg_catalog._numeric'::regtype])
                                      then ', ' || ((v_att_params.atttypmod - 4) & 65535)::text || ')'
                            else ')' end
                          else '' end
                       || case when v_att_params.attndims > 1 then repeat('[]', v_att_params.attndims) else '' end
                       || coalesce(v_att_params.attfdwoptions, '')
                       || case when v_att_params.collname is not null
                               then ' COLLATE ' || quote_ident(v_att_params.collschema) || '.' || quote_ident(v_att_params.collname)
                          else '' end
                       || case when v_att_params.atthasdef is true and v_att_params.attgenerated = 's'
                               then ' GENERATED ALWAYS AS (' || v_att_params.gen_expr || ') STORED'
                          else '' end
                       || case when v_att_params.attnotnull is true or (v_att_params.typtype = 'd' and v_att_params.typnotnull is true)
                               then ' NOT NULL'
                          else ' NULL' end
                       || case when v_att_params.atthasdef is true and v_att_params.attgenerated = ''
                               then ' DEFAULT (' || v_att_params.gen_expr || ')'
                          else '' end
                       || ',';

        -- Partitions tables
        elsif v_is_partition is true and v_inherits is false then

          v_table_ddl := v_table_ddl || E'\n    '
                         || quote_ident(v_att_params.attname)
                         || case when v_att_params.atthasdef is true and v_att_params.attgenerated = 's'
                                 then ' GENERATED ALWAYS AS (' || v_att_params.gen_expr || ') STORED'
                            else '' end
                         || case when v_att_params.attnotnull is true or (v_att_params.typtype = 'd' and v_att_params.typnotnull is true)
                                 then ' NOT NULL'
                            else ' NULL' end
                         || case when v_att_params.atthasdef is true and v_att_params.attgenerated = ''
                                 then ' DEFAULT (' || v_att_params.gen_expr || ')'
                            else '' end
                         || ',';

        end if;

        -- Alter columns
        v_alter := v_alter
                   || case when v_att_params.attstorage != v_att_params.typstorage
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET STORAGE '
                                || case v_att_params.attstorage when 'm' then 'MAIN,'
                                                                when 'p' then 'PLAIN,'
                                                                when 'e' then 'EXTERNAL,'
                                                                when 'x' then 'EXTENDED,'
                                   else '' end
                      else '' end
                   || case when v_att_params.attoptions is not null
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET (' || v_att_params.attoptions || '),'
                      else '' end
                   || case when v_att_params.attstattarget != -1
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET STATISTICS ' || v_att_params.attstattarget || ','
                      else '' end;

      end loop;

    -- Constraints
    for v_constraint_rec in
      select case contype
                when 'p' then 1 -- primary key constraint
                when 'u' then 2 -- unique constraint
                when 'f' then 3 -- foreign key constraint
                when 'c' then 4 -- check constraint
                when 'x' then 5 -- exclude constraint
                else 6
             end as type_rank,
            get_constraint_def("oid") as constraint_definition
      from pg_catalog.pg_constraint
      where conrelid = v_table_params."oid"
      order by type_rank
    loop
      v_table_ddl := v_table_ddl || E'\n    '
                     || v_constraint_rec.constraint_definition
                     || ',';
    end loop;

    -- Trimming comma at the end
    v_table_ddl = rtrim(v_table_ddl, ',') || E'\n)';

    -- FT parameters
    select s.srvname,
           E'\nOPTIONS (' || string_agg(regexp_replace(fo.opts, '^(.+)=(.+)$', '\1 ''\2'''), ', ' order by fo.rn) || ')' as ftoptions
    into v_ft_params
    from pg_catalog.pg_foreign_table f
    join pg_catalog.pg_foreign_server s on f.ftserver = s."oid"
    left join unnest(f.ftoptions) with ordinality as fo (opts, rn) on 1 = 1
    where ftrelid = v_table_params."oid"
    group by s.srvname;

    -- Final result
    if v_inherits is true then
        v_table_ddl := v_table_ddl || E'\nINHERITS (' || v_parent || ')';
    elsif v_is_partition is true and v_inherits is false then
      v_table_ddl := v_table_ddl || E'\n' || v_partbound;
    end if;

    v_table_ddl := v_table_ddl
                   || E'\nSERVER ' || v_ft_params.srvname
                   || coalesce(v_ft_params.ftoptions, '')
                   || ';'

                   || case when v_alter != ''
                           then E'\n\nALTER FOREIGN TABLE IF EXISTS ' || quote_ident(v_table_params.nspname) || '.' || quote_ident(in_table) || rtrim(v_alter, ',') || ';'
                      else '' end;

    return v_table_ddl;

  end;

$$;

comment on function get_foreign_table_def(in_table text, in_schema text)
    is 'Generates CREATE command for a foreign table by its name and schema (default - search_path)';


-- OID

create or replace function get_foreign_table_def
  (
    table_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Generates DDL script for foreign tables only
  -- For other types of tables use get_table_def functions

-- Example:

-- select get_foreign_table_def("oid")
-- from pg_catalog.pg_class
-- where relkind = 'f'
-- limit 10;

  declare

    v_table_ddl text;
    v_alter text := '';

    v_table_params record;
    v_att_params record;
    v_constraint_rec record;
    v_partition_key text := '';
    v_partbound text;
    v_parent text;

    v_is_partition boolean;
    v_inherits boolean;
    v_is_child boolean;

    v_ft_params record;

  begin

    -- Checking existence of table
    select c.relname,
           n.nspname,
           c.reloptions,
           c.relpersistence,
           c.relkind,
           c.relispartition,
           c.relpartbound
    into v_table_params
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where c.relkind = 'f'
          and c."oid" = table_oid;

    if v_table_params.relname is null then
      raise warning 'Foreign table with OID % does not exist', table_oid;

      return null;
    end if;

    -- Partitioning
        -- inheritance: pg_class.relkind = 'r', pg_class.relispartition = false, pg_class.relpartbound is null
        -- declarative: pg_class.relkind = 'r', pg_class.relispartition = true, pg_class.relpartbound is not null

    v_partbound := '';
    v_is_partition := false;
    v_inherits := false;

    select quote_ident(n.nspname) || '.' || quote_ident(c.relname),
           v_table_params.relispartition,
           pg_catalog.pg_get_expr(v_table_params.relpartbound, table_oid, true)
    into v_parent,
         v_is_child,
         v_partbound
    from pg_catalog.pg_inherits i
    join pg_catalog.pg_class c on i.inhparent = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where i.inhrelid = table_oid;

    if v_parent is not null then
      v_is_partition := true;

      if v_is_child is true then
        v_inherits := false;
      else
        v_inherits := true;
      end if;

    end if;

    -- Partitions' definition
    if v_is_partition is true then

      if v_inherits is true then
        -- inheritance
        v_table_ddl := 'CREATE FOREIGN TABLE IF NOT EXISTS ' || quote_ident(v_table_params.nspname) || '.' || quote_ident(v_table_params.relname) || E'\n(';

      else
        -- declarative
          v_table_ddl := 'CREATE FOREIGN TABLE IF NOT EXISTS ' || quote_ident(v_table_params.nspname) || '.' || quote_ident(v_table_params.relname)
                         || E'\nPARTITION OF ' || v_parent || E'\n(';
      end if;

    else
      -- Simple tables
      v_table_ddl := 'CREATE FOREIGN TABLE IF NOT EXISTS ' || quote_ident(v_table_params.nspname) || '.' || quote_ident(v_table_params.relname) || E'\n(';

    end if;

    -- Columns' definitions

    for v_att_params in

      select a.attname,
             a.attstattarget,
             a.attndims,
             a.atttypmod,
             a.attnotnull,
             a.atthasdef,
             a.attgenerated,
             a.attstorage,
             string_agg(replace(ao.opts, '=', ' = '), ', ' order by ao.rn) as attoptions,
             ' OPTIONS (' || string_agg(regexp_replace(afo.opts, '^(.+)=(.+)$', '\1 ''\2'''), ', ' order by afo.rn) || ')' as attfdwoptions,
             t."oid" as typoid,
             t.typname,
             t.typtype,
             t.typstorage,
             t.typnotnull,
             nt.nspname as typschema,
             c.collname,
             nc.nspname as collschema,
             pg_catalog.pg_get_expr(ad.adbin, table_oid, true) as gen_expr
      from pg_catalog.pg_attribute a
      join pg_catalog.pg_type t on t."oid" = a.atttypid
      join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
      left join pg_catalog.pg_attrdef ad on ad.adrelid = table_oid
                                            and ad.adnum = a.attnum
      left join pg_catalog.pg_collation c on c."oid" = a.attcollation
      left join pg_catalog.pg_namespace nc on nc."oid" = c.collnamespace
      left join lateral unnest(a.attoptions) with ordinality ao (opts, rn) on 1 = 1
      left join lateral unnest(a.attfdwoptions) with ordinality afo (opts, rn) on 1 = 1
      where a.attrelid = table_oid
            and a.attnum > 0
            and a.attisdropped is false
      group by a.attnum,
               a.attname,
               a.attstattarget,
               a.attndims,
               a.atttypmod,
               a.attstorage,
               a.attnotnull,
               a.atthasdef,
               a.attgenerated,
               a.attcollation,
               t."oid",
               t.typname,
               t.typtype,
               t.typnotnull,
               t.typstorage,
               nt.nspname,
               c.collname,
               nc.nspname,
               ad.adbin
      order by a.attnum

    loop

      -- Simple tables and inherited tables
      if v_is_partition is false or v_inherits is true then

        v_table_ddl := v_table_ddl || E'\n    '
                       || quote_ident(v_att_params.attname) || ' '
                       || quote_ident(v_att_params.typschema) || '.'
                       || case when v_att_params.attndims > 1 then quote_ident(substring(v_att_params.typname, 2, length(v_att_params.typname)))
                          else quote_ident(v_att_params.typname) end
                       || case when v_att_params.atttypmod != '-1'::integer then '(' ||
                            -- bpchar, varchar max length
                            case when v_att_params.typoid = any(array['pg_catalog._bpchar'::regtype, 'pg_catalog._varchar'::regtype, 'pg_catalog.bpchar'::regtype, 'pg_catalog."varchar"'::regtype])
                                      then (v_att_params.atttypmod - 4)::text
                                 when v_att_params.typoid = any(array['pg_catalog."bit"'::regtype, 'pg_catalog._bit'::regtype, 'pg_catalog.varbit'::regtype, 'pg_catalog._varbit'::regtype])
                                      then v_att_params.atttypmod::text
                            -- numeric precision
                                 when v_att_params.typoid = any(array['pg_catalog."numeric"'::regtype, 'pg_catalog._numeric'::regtype])
                                      then (((v_att_params.atttypmod - 4) >> 16) & 65535)::text
                            -- timestamp, timestamptz, time, timetz precision
                                 when v_att_params.typoid = any(array['pg_catalog."time"'::regtype, 'pg_catalog."timestamp"'::regtype, 'pg_catalog.timestamptz'::regtype, 'pg_catalog.timetz'::regtype,
                                                                      'pg_catalog._time'::regtype, 'pg_catalog._timestamp'::regtype, 'pg_catalog._timestamptz'::regtype, 'pg_catalog._timetz'::regtype
                                                                     ])
                                      then
                                          case when v_att_params.atttypmod < 0 then 6::text
                                          else v_att_params.atttypmod::text end
                            -- interval precision
                                 when v_att_params.typoid = any(array['pg_catalog."interval"'::regtype, 'pg_catalog._interval'::regtype])
                                      then
                                          case when v_att_params.atttypmod < 0 or v_att_params.atttypmod & 65535 = 65535 then 6::text
                                          else (v_att_params.atttypmod & 65535)::text end
                            else '' end
                          else '' end
                       || case when v_att_params.atttypmod != '-1'::integer then
                            -- numeric scale
                            case when v_att_params.typoid = any(array['pg_catalog."numeric"'::regtype, 'pg_catalog._numeric'::regtype])
                                      then ', ' || ((v_att_params.atttypmod - 4) & 65535)::text || ')'
                            else ')' end
                          else '' end
                       || case when v_att_params.attndims > 1 then repeat('[]', v_att_params.attndims) else '' end
                       || coalesce(v_att_params.attfdwoptions, '')
                       || case when v_att_params.collname is not null
                               then ' COLLATE ' || quote_ident(v_att_params.collschema) || '.' || quote_ident(v_att_params.collname)
                          else '' end
                       || case when v_att_params.atthasdef is true and v_att_params.attgenerated = 's'
                               then ' GENERATED ALWAYS AS (' || v_att_params.gen_expr || ') STORED'
                          else '' end
                       || case when v_att_params.attnotnull is true or (v_att_params.typtype = 'd' and v_att_params.typnotnull is true)
                               then ' NOT NULL'
                          else ' NULL' end
                       || case when v_att_params.atthasdef is true and v_att_params.attgenerated = ''
                               then ' DEFAULT (' || v_att_params.gen_expr || ')'
                          else '' end
                       || ',';

        -- Partitions tables
        elsif v_is_partition is true and v_inherits is false then

          v_table_ddl := v_table_ddl || E'\n    '
                         || quote_ident(v_att_params.attname)
                         || case when v_att_params.atthasdef is true and v_att_params.attgenerated = 's'
                                 then ' GENERATED ALWAYS AS (' || v_att_params.gen_expr || ') STORED'
                            else '' end
                         || case when v_att_params.attnotnull is true or (v_att_params.typtype = 'd' and v_att_params.typnotnull is true)
                                 then ' NOT NULL'
                            else ' NULL' end
                         || case when v_att_params.atthasdef is true and v_att_params.attgenerated = ''
                                 then ' DEFAULT (' || v_att_params.gen_expr || ')'
                            else '' end
                         || ',';

        end if;

        -- Alter columns
        v_alter := v_alter
                   || case when v_att_params.attstorage != v_att_params.typstorage
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET STORAGE '
                                || case v_att_params.attstorage when 'm' then 'MAIN,'
                                                                when 'p' then 'PLAIN,'
                                                                when 'e' then 'EXTERNAL,'
                                                                when 'x' then 'EXTENDED,'
                                   else '' end
                      else '' end
                   || case when v_att_params.attoptions is not null
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET (' || v_att_params.attoptions || '),'
                      else '' end
                   || case when v_att_params.attstattarget != -1
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET STATISTICS ' || v_att_params.attstattarget || ','
                      else '' end;

      end loop;

    -- Constraints
    for v_constraint_rec in
      select case contype
                when 'p' then 1 -- primary key constraint
                when 'u' then 2 -- unique constraint
                when 'f' then 3 -- foreign key constraint
                when 'c' then 4 -- check constraint
                when 'x' then 5 -- exclude constraint
                else 6
             end as type_rank,
            get_constraint_def("oid") as constraint_definition
      from pg_catalog.pg_constraint
      where conrelid = table_oid
      order by type_rank
    loop
      v_table_ddl := v_table_ddl || E'\n    '
                     || v_constraint_rec.constraint_definition
                     || ',';
    end loop;

    -- Trimming comma at the end
    v_table_ddl = rtrim(v_table_ddl, ',') || E'\n)';

    -- FT parameters
    select s.srvname,
           E'\nOPTIONS (' || string_agg(regexp_replace(fo.opts, '^(.+)=(.+)$', '\1 ''\2'''), ', ' order by fo.rn) || ')' as ftoptions
    into v_ft_params
    from pg_catalog.pg_foreign_table f
    join pg_catalog.pg_foreign_server s on f.ftserver = s."oid"
    left join unnest(f.ftoptions) with ordinality as fo (opts, rn) on 1 = 1
    where ftrelid = table_oid
    group by s.srvname;

    -- Final result
    if v_inherits is true then
        v_table_ddl := v_table_ddl || E'\nINHERITS (' || v_parent || ')';
    elsif v_is_partition is true and v_inherits is false then
      v_table_ddl := v_table_ddl || E'\n' || v_partbound;
    end if;

    v_table_ddl := v_table_ddl
                   || E'\nSERVER ' || v_ft_params.srvname
                   || coalesce(v_ft_params.ftoptions, '')
                   || ';'

                   || case when v_alter != ''
                           then E'\n\nALTER FOREIGN TABLE IF EXISTS ' || quote_ident(v_table_params.nspname) || '.' || quote_ident(v_table_params.relname) || rtrim(v_alter, ',') || ';'
                      else '' end;

    return v_table_ddl;

  end;

$$;

comment on function get_foreign_table_def(table_oid oid)
    is 'Generates CREATE command for a foreign table by its OID';