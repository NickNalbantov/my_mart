-- Examples:

-- select get_extension_def(extname),
--        get_extension_def("oid")
-- from pg_catalog.pg_extension;


-- Name

create or replace function get_extension_def
  (
    in_ext text,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_extension_def(extname)
-- from pg_catalog.pg_extension
-- limit 10;

  declare

    v_ext_ddl text;
    v_ext_params record;

  begin

    -- Checking existence of extension
    select e."oid",
           e.extversion,
           n.nspname
    into v_ext_params
    from pg_catalog.pg_extension e
    join pg_catalog.pg_namespace n on n."oid" = e.extnamespace
    where e.extname = in_ext;

    if v_ext_params."oid" is null then
      raise warning 'Extension % does not exist', quote_ident(in_ext);

      return null;
    end if;

    -- Building DDL

    v_ext_ddl := 'CREATE EXTENSION IF NOT EXISTS ' || quote_ident(in_ext) || E'\n    SCHEMA ' || quote_ident(v_ext_params.nspname)
                 || E'\n    VERSION ' || v_ext_params.extversion
                 || case when cascade_ is true then E'\n    CASCADE' else '' end || ';';

    return v_ext_ddl;

  end;

$$;

comment on function get_extension_def(in_ext text, cascade_ bool)
    is 'Generates CREATE command for an extension by its name; if cascade_ is true (by default) then it adds CASCADE to CREATE command';


-- OID

create or replace function get_extension_def
  (
    ext_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_extension_def("oid")
-- from pg_catalog.pg_extension
-- limit 10;

  declare

    v_ext_ddl text;
    v_ext_params record;

  begin

    -- Checking existence of extension
    select e.extversion,
           e.extname,
           n.nspname
    into v_ext_params
    from pg_catalog.pg_extension e
    left join pg_catalog.pg_namespace n on n."oid" = e.extnamespace
    where e."oid" = ext_oid;

    if v_ext_params.extname is null then
      raise warning 'Extension with OID % does not exist', ext_oid;

      return null;
    end if;

    -- Building DDL

    v_ext_ddl := 'CREATE EXTENSION IF NOT EXISTS ' || quote_ident(v_ext_params.extname) || E'\n    SCHEMA ' || quote_ident(v_ext_params.nspname)
                 || E'\n    VERSION ' || v_ext_params.extversion
                 || case when cascade_ is true then E'\n    CASCADE' else '' end || ';';

    return v_ext_ddl;

  end;

$$;

comment on function get_extension_def(ext_oid oid, cascade_ bool)
    is 'Generates CREATE command for an extension by its OID; if cascade_ is true (by default) then it adds CASCADE to CREATE command';