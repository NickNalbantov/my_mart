
-- Examples:

-- select get_transform_def(l.lanname, t.typname, n.nspname),
--        get_transform_def(tr."oid"),
--        get_transform_def(l."oid", t."oid")
-- from pg_catalog.pg_transform tr
-- join pg_catalog.pg_type t on tr.trftype = t."oid"
-- join pg_catalog.pg_language l on tr.trflang = l."oid"
-- join pg_catalog.pg_namespace n on n."oid" = t.typnamespace;

-- select get_transform_def('plpython3u', 'hstore');


-- Language name + type name + type schema

create or replace function get_transform_def
  (
    trans_lang text,
    trans_type text,
    type_schema text default null
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_transform_def('plpython3u', 'hstore');

  declare

    v_trans_ddl text;
    v_trans_params record;
    v_from_func text;
    v_to_func text;
    in_type_fixed text;
    v_type_params record;

    int_syn text[] := array['integer', 'int'];
    smallint_syn text := 'smallint';
    bigint_syn text := 'bigint';
    num_syn text := 'decimal';
    real_syn text := 'real';
    dp_syn text[] := array['double precision', 'float'];
    time_syn text := 'time without time zone';
    timetz_syn text := 'time with time zone';
    timestamp_syn text := 'timestamp without time zone';
    timestamptz_syn text := 'timestamp with time zone';
    bpchar_syn text := 'character';
    varchar_syn text[] := array['character varying', 'char varying'];
    varbit_syn text := 'bit varying';
    bool_syn text := 'boolean';

  begin

    -- Catching synonyms for type
    select case when right(tp, 2) = '[]' then rtrim('_' || tp, '[]')
           else tp end
    into in_type_fixed
    from
    (
      select
        case when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = any(int_syn) then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1int4\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = smallint_syn then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1int2\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = bigint_syn then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1int8\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = num_syn then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1numeric\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = real_syn then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1float4\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = any(dp_syn) then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1float8\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = time_syn then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1time\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = timetz_syn then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1timetz\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = timestamp_syn then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1timestamp\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = timestamptz_syn then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1timestamptz\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = bpchar_syn then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1bpchar\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = any(varchar_syn) then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1varchar\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = varbit_syn then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1varbit\2')
             when lower(regexp_replace(rtrim(trans_type, '[]'), '^_', '')) = bool_syn then regexp_replace(trans_type, '(^|^_).+[^(\[\]|$)]', '\1bool\2')
        else trans_type end as tp
    ) a;

    -- Getting OIDs for type
    select t."oid",
           n.nspname
    into v_type_params
    from pg_catalog.pg_type t
    left join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where in_type_fixed = t.typname and coalesce(type_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    -- Checking existence of transform
    select tr."oid",
           tr.trffromsql::oid as trffromsql,
           tr.trftosql::oid as trftosql
    into v_trans_params
    from pg_catalog.pg_transform tr
    join pg_catalog.pg_type t on tr.trftype = t."oid"
    join pg_catalog.pg_language l on tr.trflang = l."oid"
    where t."oid" = v_type_params."oid"
          and l.lanname = trans_lang;

    if v_trans_params."oid" is null then
      raise warning 'Transform for type %.% and language % does not exist',
        coalesce(quote_ident(type_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(trans_type),
        quote_ident(trans_lang);

      return null;
    end if;

    -- Function FROM
    if v_trans_params.trffromsql != 0::oid then
      select get_function_identity_arguments(v_trans_params.trffromsql, false, false)
      into v_from_func;
    else v_from_func := null;
    end if;

    -- Function TO
    if v_trans_params.trftosql != 0::oid then
      select get_function_identity_arguments(v_trans_params.trftosql, false, false)
      into v_to_func;
    else v_to_func := null;
    end if;


    -- Building DDL

    v_trans_ddl := 'CREATE OR REPLACE TRANSFORM FOR ' || quote_ident(v_type_params.nspname) || '.' || quote_ident(in_type_fixed)
                   || ' LANGUAGE ' || quote_ident(trans_lang) || E'\n    '
                   || case when v_from_func is not null then 'FROM SQL WITH FUNCTION ' || v_from_func else '' end
                   || case when v_to_func is not null then E'\n    TO SQL WITH FUNCTION ' || v_to_func else '' end || ';';

    return v_trans_ddl;

  end;

$$;

comment on function get_transform_def(trans_lang text, trans_type text, type_schema text)
    is 'Generates CREATE command for a transform by the name of language and name and schema (default - search_path) of transformed type';


-- OID

create or replace function get_transform_def
  (
    trans_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_transform_def("oid")
-- from pg_catalog.pg_transform
-- limit 10;

  declare

    v_trans_ddl text;
    v_trans_params record;
    v_from_func text;
    v_to_func text;

  begin

    -- Checking existence of transform
    select n.nspname,
           t.typname,
           l.lanname,
           tr.trffromsql::oid as trffromsql,
           tr.trftosql::oid as trftosql
    into v_trans_params
    from pg_catalog.pg_transform tr
    join pg_catalog.pg_type t on tr.trftype = t."oid"
    join pg_catalog.pg_language l on tr.trflang = l."oid"
    join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    where tr."oid" = trans_oid;

    if v_trans_params.typname is null then
      raise warning 'Transform with OID % does not exist', trans_oid;

      return null;
    end if;

    -- Function FROM
    if v_trans_params.trffromsql != 0::oid then
      select get_function_identity_arguments(v_trans_params.trffromsql, false, false)
      into v_from_func;
    else v_from_func := null;
    end if;

    -- Function TO
    if v_trans_params.trftosql != 0::oid then
      select get_function_identity_arguments(v_trans_params.trftosql, false, false)
      into v_to_func;
    else v_to_func := null;
    end if;


    -- Building DDL

    v_trans_ddl := 'CREATE OR REPLACE TRANSFORM FOR ' || quote_ident(v_trans_params.nspname) || '.' || quote_ident(v_trans_params.typname)
                   || ' LANGUAGE ' || quote_ident(v_trans_params.lanname) || E'\n    '
                   || case when v_from_func is not null then 'FROM SQL WITH FUNCTION ' || v_from_func else '' end
                   || case when v_to_func is not null then E'\n    TO SQL WITH FUNCTION ' || v_to_func else '' end || ';';

    return v_trans_ddl;
  end;
$$;

comment on function get_transform_def(trans_oid oid)
    is 'Generates CREATE command for a transform by its OID';


-- Language and type OIDs

create or replace function get_transform_def
  (
    lang_oid oid,
    type_oid oid
  )
returns text
language plpgsql
strict
as
$$

  declare

    v_trans_ddl text;
    v_trans_params record;
    v_from_func text;
    v_to_func text;

  begin

    -- Checking existence of transform
    select n.nspname,
           t.typname,
           l.lanname,
           tr.trffromsql::oid as trffromsql,
           tr.trftosql::oid as trftosql
    into v_trans_params
    from pg_catalog.pg_transform tr
    join pg_catalog.pg_type t on tr.trftype = t."oid"
    join pg_catalog.pg_language l on tr.trflang = l."oid"
    join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    where t."oid" = type_oid and l."oid" = lang_oid;

    if v_trans_params.typname is null then
      raise warning 'Transform for language and type with OIDs % and % does not exist', lang_oid, type_oid;

      return null;
    end if;

    -- Function FROM
    if v_trans_params.trffromsql != 0::oid then
      select get_function_identity_arguments(v_trans_params.trffromsql, false, false)
      into v_from_func;
    else v_from_func := null;
    end if;

    -- Function TO
    if v_trans_params.trftosql != 0::oid then
      select get_function_identity_arguments(v_trans_params.trftosql, false, false)
      into v_to_func;
    else v_to_func := null;
    end if;


    -- Building DDL

    v_trans_ddl := 'CREATE OR REPLACE TRANSFORM FOR ' || quote_ident(v_trans_params.nspname) || '.' || quote_ident(v_trans_params.typname)
                   || ' LANGUAGE ' || v_trans_params.lanname || E'\n    '
                   || case when v_from_func is not null then 'FROM SQL WITH FUNCTION ' || v_from_func else '' end
                   || case when v_to_func is not null then E'\n    TO SQL WITH FUNCTION ' || v_to_func else '' end || ';';

    return v_trans_ddl;
  end;
$$;

comment on function get_transform_def(lang_oid oid, type_oid oid)
    is 'Generates CREATE command for a transform by OIDs of language and type';