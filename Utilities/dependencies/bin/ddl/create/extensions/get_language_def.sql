-- Examples:

-- select get_language_def(lanname),
--        get_language_def("oid")
-- from pg_catalog.pg_language;


-- Name

create or replace function get_language_def
  (
    in_lang text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_language_def(lanname)
-- from pg_catalog.pg_language
-- limit 10;

  declare

    v_lang_ddl text;
    v_lang_params record;
    v_handler text;
    v_inline text;
    v_validator text;

  begin

    -- Checking existence of language
    select l."oid",
           l.lanpltrusted,
           l.lanplcallfoid,
           l.laninline,
           l.lanvalidator
    into v_lang_params
    from pg_catalog.pg_language l
    where l.lanname = in_lang;

    if v_lang_params."oid" is null then
      raise warning 'Language % does not exist', quote_ident(in_lang);

      return null;
    end if;

    -- Functions
    select case when v_lang_params.lanplcallfoid = 0::oid then null else get_function_identity_arguments(v_lang_params.lanplcallfoid, false, false) end,
           case when v_lang_params.laninline = 0::oid then null else get_function_identity_arguments(v_lang_params.laninline, false, false) end,
           case when v_lang_params.lanvalidator = 0::oid then null else get_function_identity_arguments(v_lang_params.lanvalidator, false, false) end
    into v_handler,
         v_inline,
         v_validator;

    -- Building DDL

    v_lang_ddl := 'CREATE OR REPLACE'
                  || case when v_lang_params.lanpltrusted = true then ' TRUSTED' else '' end
                  || ' LANGUAGE ' || quote_ident(in_lang)
                  || case when v_handler is not null then E'\n    HANDLER ' || v_handler else '' end
                  || case when v_inline is not null then E'\n    INLINE ' || v_inline else '' end
                  || case when v_validator is not null then E'\n    VALIDATOR ' || v_validator else '' end
                  || ';';

    return v_lang_ddl;

  end;

$$;

comment on function get_language_def(in_lang text)
    is 'Generates CREATE command for a language by its name';


-- OID

create or replace function get_language_def
  (
    lang_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_language_def("oid")
-- from pg_catalog.pg_language
-- limit 10;

  declare

    v_lang_ddl text;
    v_lang_params record;
    v_handler text;
    v_inline text;
    v_validator text;

  begin

    -- Checking existence of language
    select l.lanname,
           l.lanpltrusted,
           l.lanplcallfoid,
           l.laninline,
           l.lanvalidator
    into v_lang_params
    from pg_catalog.pg_language l
    where l.oid = lang_oid;

    if v_lang_params.lanname is null then
      raise warning 'Language with OID % does not exist', lang_oid;

      return null;
    end if;

    -- Functions
    select case when v_lang_params.lanplcallfoid = 0::oid then null else get_function_identity_arguments(v_lang_params.lanplcallfoid, false, false) end,
           case when v_lang_params.laninline = 0::oid then null else get_function_identity_arguments(v_lang_params.laninline, false, false) end,
           case when v_lang_params.lanvalidator = 0::oid then null else get_function_identity_arguments(v_lang_params.lanvalidator, false, false) end
    into v_handler,
         v_inline,
         v_validator;

    -- Building DDL

    v_lang_ddl := 'CREATE OR REPLACE'
                  || case when v_lang_params.lanpltrusted = true then ' TRUSTED' else '' end
                  || ' LANGUAGE ' || quote_ident(v_lang_params.lanname)
                  || case when v_handler is not null then E'\n    HANDLER ' || v_handler else '' end
                  || case when v_inline is not null then E'\n    INLINE ' || v_inline else '' end
                  || case when v_validator is not null then E'\n    VALIDATOR ' || v_validator else '' end
                  || ';';

    return v_lang_ddl;

  end;

$$;

comment on function get_language_def(lang_oid oid)
    is 'Generates CREATE command for a language by its OID';