-- Examples:

-- select get_publication_def(pubname),
--        get_publication_def("oid")
-- from pg_catalog.pg_publication;


-- Name

create or replace function get_publication_def
  (
    in_pub text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_publication_def(pubname)
-- from pg_catalog.pg_publication
-- limit 10;

  declare

    v_pub_ddl text;
    v_pub_params record;
    v_pub_sch_map text;
    v_pub_rel_map text;

  begin

    -- Checking existence of publication
    select "oid",
           puballtables,
           pubviaroot,
           pubinsert,
           pubupdate,
           pubdelete,
           pubtruncate
    into v_pub_params
    from pg_catalog.pg_publication
    where pubname = in_pub;

    if v_pub_params."oid" is null then
      raise warning 'Publication % does not exist', in_pub;

      return null;
    end if;

    -- Mapping for schemas
    select 'TABLES IN SCHEMA ' || string_agg(quote_ident(n.nspname), ', ')
    into v_pub_sch_map
    from pg_catalog.pg_publication_namespace p
    join pg_catalog.pg_namespace n on n."oid" = p.pnnspid
    where p.pnpubid = v_pub_params."oid";

    -- Mapping for tables
    select string_agg(table_opts, E',\n        ' order by rn)
    into v_pub_rel_map
    from
    (
      select 'TABLE ' || quote_ident(n.nspname) || '.' || quote_ident(c.relname) ||
             coalesce(' (' || string_agg(quote_ident(a.attname), ', ' order by a.attnum) || ')', '') ||
             coalesce(' WHERE (' || pg_catalog.pg_get_expr(r.prqual, r.prrelid, true) || ')', '') as table_opts,
             row_number() over(order by c."oid") as rn
      from pg_catalog.pg_publication_rel r
      join pg_catalog.pg_class c on c."oid" = r.prrelid
      join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
      left join pg_catalog.pg_attribute a on a.attrelid = r.prrelid
                                          and a.attnum = any(r.prattrs)
      where r.prpubid = v_pub_params."oid"
      group by n.nspname,
               c.relname,
               r.prqual,
               r.prrelid,
               c."oid"
    ) a;

    -- Building DDL
    v_pub_ddl := 'CREATE PUBLICATION ' || quote_ident(in_pub) || E'\n    FOR '
                 || case when v_pub_params.puballtables is true then 'ALL TABLES'
                    else coalesce(v_pub_sch_map, v_pub_rel_map) end
                 || E'\n    WITH (\n            publish = '''
                 || rtrim(case when v_pub_params.pubinsert is true then 'insert, ' else '' end ||
                          case when v_pub_params.pubupdate is true then 'update, ' else '' end ||
                          case when v_pub_params.pubdelete is true then 'delete, ' else '' end ||
                          case when v_pub_params.pubtruncate is true then 'truncate' else '' end
                          , ', '
                         )
                 || E''',\n            publish_via_partition_root = ' || v_pub_params.pubviaroot::text
                 || E'\n         );';

    return v_pub_ddl;

  end;

$$;

comment on function get_publication_def(in_pub text)
    is 'Generates CREATE command for a publication by its name';


-- OID

create or replace function get_publication_def
  (
    pub_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_publication_def("oid")
-- from pg_catalog.pg_publication
-- limit 10;

  declare

    v_pub_ddl text;
    v_pub_params record;
    v_pub_sch_map text;
    v_pub_rel_map text;

  begin

    -- Checking existence of publication
    select pubname,
           puballtables,
           pubviaroot,
           pubinsert,
           pubupdate,
           pubdelete,
           pubtruncate
    into v_pub_params
    from pg_catalog.pg_publication
    where "oid" = pub_oid;

    if v_pub_params.pubname is null then
      raise warning 'Publication with OID % does not exist', pub_oid;

      return null;
    end if;

    -- Mapping for schemas
    select 'TABLES IN SCHEMA ' || string_agg(quote_ident(n.nspname), ', ')
    into v_pub_sch_map
    from pg_catalog.pg_publication_namespace p
    join pg_catalog.pg_namespace n on n."oid" = p.pnnspid
    where p.pnpubid = pub_oid;

    -- Mapping for tables
    select string_agg(table_opts, E',\n        ' order by rn)
    into v_pub_rel_map
    from
    (
      select 'TABLE ' || quote_ident(n.nspname) || '.' || quote_ident(c.relname) ||
             coalesce(' (' || string_agg(quote_ident(a.attname), ', ' order by a.attnum) || ')', '') ||
             coalesce(' WHERE (' || pg_catalog.pg_get_expr(r.prqual, r.prrelid, true) || ')', '') as table_opts,
             row_number() over(order by c."oid") as rn
      from pg_catalog.pg_publication_rel r
      join pg_catalog.pg_class c on c."oid" = r.prrelid
      join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
      left join pg_catalog.pg_attribute a on a.attrelid = r.prrelid
                                             and a.attnum = any(r.prattrs)
      where r.prpubid = pub_oid
      group by n.nspname,
               c.relname,
               r.prqual,
               r.prrelid,
               c."oid"
    ) a;

    -- Building DDL
    v_pub_ddl := 'CREATE PUBLICATION ' || quote_ident(v_pub_params.pubname) || E'\n    FOR '
                 || case when v_pub_params.puballtables is true then 'ALL TABLES'
                    else coalesce(v_pub_sch_map, v_pub_rel_map) end
                 || E'\n    WITH (\n            publish = '''
                 || rtrim(case when v_pub_params.pubinsert is true then 'insert, ' else '' end ||
                          case when v_pub_params.pubupdate is true then 'update, ' else '' end ||
                          case when v_pub_params.pubdelete is true then 'delete, ' else '' end ||
                          case when v_pub_params.pubtruncate is true then 'truncate' else '' end
                          , ', '
                         )
                 || E''',\n            publish_via_partition_root = ' || v_pub_params.pubviaroot::text
                 || E'\n         );';

    return v_pub_ddl;

  end;

$$;

comment on function get_publication_def(pub_oid oid)
    is 'Generates CREATE command for a publication by its OID';