-- Examples:

-- select get_subscription_def(subname),
--        get_subscription_def("oid")
-- from pg_catalog.pg_subscription;


-- Name

create or replace function get_subscription_def
  (
    in_sub text
  )
returns text
language plpgsql
strict
as
$$

  declare

    v_sub_ddl text;
    v_sub_params record;
    v_sub_rel record;

  begin

    -- Checking existence of subscription
    select "oid",
           subenabled,
           subbinary,
           substream,
           subtwophasestate,
           subdisableonerr,
           subconninfo,
           subslotname,
           subsynccommit,
           subpublications
    into v_sub_params
    from pg_catalog.pg_subscription
    where subname = in_sub;

    if v_sub_params."oid" is null then
      raise warning 'Subscription % does not exist', in_sub;

      return null;
    end if;

  -- Status of subscription
    select string_agg(srsubstate, ', ') as substate, string_agg(srsublsn::text, ', ') as sublsn
    into v_sub_rel
    from pg_catalog.pg_subscription_rel
    where srsubid = v_sub_params.oid;

    -- Building DDL
    v_sub_ddl := 'CREATE SUBSCRIPTION ' || quote_ident(in_sub)
                 || E'\n    CONNECTION ''' || v_sub_params.subconninfo
                 || E'''\n    PUBLICATION ' || array_to_string(v_sub_params.subpublications, ', ')
                 || E'\n    WITH (\n            connect = ' || case when v_sub_rel.substate is not null then 'true' else 'false' end
                 || E',\n            enabled = ' || v_sub_params.subenabled
                 || E',\n            slot_name = ' || coalesce('''' || v_sub_params.subslotname || '''', 'NONE')
                 || E',\n            binary = ' || v_sub_params.subbinary
                 || E',\n            copy_data = ' || case when v_sub_rel.substate is not null and v_sub_rel.sublsn is not null then 'true' else 'false' end
                 || E',\n            streaming = ' || v_sub_params.substream
                 || E',\n            synchronous_commit = ''' || v_sub_params.subsynccommit
                 || E''',\n            two_phase = ' || case when v_sub_params.subtwophasestate = 'd' then 'false' else 'true' end
                 || E',\n            disable_on_error = ' || v_sub_params.subdisableonerr
                 || E'\n         );';

    return v_sub_ddl;

  end;

$$;

comment on function get_subscription_def(in_sub text)
    is 'Generates CREATE command for a subscription by its name';


-- OID

create or replace function get_subscription_def
  (
    sub_oid oid
  )
returns text
language plpgsql
strict
as
$$

  declare

    v_sub_ddl text;
    v_sub_params record;
    v_sub_rel record;

  begin

    -- Checking existence of subscription
    select subname,
           subenabled,
           subbinary,
           substream,
           subtwophasestate,
           subdisableonerr,
           subconninfo,
           subslotname,
           subsynccommit,
           subpublications
    into v_sub_params
    from pg_catalog.pg_subscription
    where "oid" = sub_oid;

    if v_sub_params.subname is null then
      raise warning 'Subscription with OID % does not exist', sub_oid;

      return null;
    end if;

  -- Status of subscription
    select string_agg(srsubstate, ', ') as substate, string_agg(srsublsn::text, ', ') as sublsn
    into v_sub_rel
    from pg_catalog.pg_subscription_rel
    where srsubid = sub_oid;

    -- Building DDL
    v_sub_ddl := 'CREATE SUBSCRIPTION ' || quote_ident(v_sub_params.subname)
                 || E'\n    CONNECTION ''' || v_sub_params.subconninfo
                 || E'''\n    PUBLICATION ' || array_to_string(v_sub_params.subpublications, ', ')
                 || E'\n    WITH (\n            connect = ' || case when v_sub_rel.substate is not null then 'true' else 'false' end
                 || E',\n            enabled = ' || v_sub_params.subenabled
                 || E',\n            slot_name = ' || coalesce('''' || v_sub_params.subslotname || '''', 'NONE')
                 || E',\n            binary = ' || v_sub_params.subbinary
                 || E',\n            copy_data = ' || case when v_sub_rel.substate is not null and v_sub_rel.sublsn is not null then 'true' else 'false' end
                 || E',\n            streaming = ' || v_sub_params.substream
                 || E',\n            synchronous_commit = ''' || v_sub_params.subsynccommit
                 || E''',\n            two_phase = ' || case when v_sub_params.subtwophasestate = 'd' then 'false' else 'true' end
                 || E',\n            disable_on_error = ' || v_sub_params.subdisableonerr
                 || E'\n         );';

    return v_sub_ddl;

  end;

$$;

comment on function get_subscription_def(sub_oid oid)
    is 'Generates CREATE command for a subscription by its OID';