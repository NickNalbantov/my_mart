-- Examples:

-- select get_database_def(datname),
--        get_database_def("oid")
-- from pg_catalog.pg_database;


-- Name

create or replace function get_database_def
  (
    in_db text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_database_def(datname),
-- from pg_catalog.pg_database
-- limit 10;

  declare

    v_db_ddl text;
    v_db_params record;

  begin

    -- Checking existence of database
    select d."oid",
           u.rolname,
           d.datistemplate,
           d.datconnlimit,
           d.datallowconn,
           d.datlocprovider,
           d.daticulocale,
           d.datcollate,
           d.datctype,
           d.datcollversion,
           d.encoding,
           t.spcname
    into v_db_params
    from pg_catalog.pg_database d
    join pg_catalog.pg_authid u on d.datdba = u."oid"
    join pg_catalog.pg_tablespace t on d.dattablespace = t."oid"
    where d.datname = in_db;

    if v_db_params."oid" is null then
      raise warning 'Database % does not exist', quote_ident(in_db);

      return null;
    end if;

    -- Building DDL

    v_db_ddl := 'CREATE DATABASE ' || quote_ident(in_db)
                || E'\n    OWNER = ' || quote_ident(v_db_params.rolname)
                || E'\n    ENCODING = ''' || pg_catalog.pg_encoding_to_char(v_db_params.encoding) || E'''\n    '
                || case when v_db_params.daticulocale is null and v_db_params.datcollate is null then ''
                        when v_db_params.daticulocale is not null then 'ICU_LOCALE = ''' || v_db_params.daticulocale || E'''\n    '
                   else 'LC_COLLATE = ''' || v_db_params.datcollate || E'''\n    LC_CTYPE = '''|| v_db_params.datctype || E'''\n    ' end
                || 'LOCALE_PROVIDER = ' ||
                   case v_db_params.datlocprovider when 'i' then 'icu'
                                                   when 'c' then 'libc'
                                                   when 'd' then 'default'
                   end || E'\n    '
                || case when v_db_params.datcollversion is not null then 'COLLATION_VERSION = ''' || v_db_params.datcollversion || E'''\n    ' else '' end
                || 'TABLESPACE = ' || quote_ident(v_db_params.spcname)
                || E'\n    ALLOW_CONNECTIONS = ' || v_db_params.datallowconn || E'\n    '
                || case when v_db_params.datconnlimit != -1 then 'CONNECTION LIMIT = ' || v_db_params.datconnlimit || E'\n    '  else '' end
                || 'IS_TEMPLATE = ' || v_db_params.datistemplate
                || E'\n    OID = ' || v_db_params."oid" || E';';

    return v_db_ddl;

  end;

$$;

comment on function get_database_def(in_db text)
    is 'Generates CREATE command for a database by its name';


-- OID

create or replace function get_database_def
  (
    db_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_database_def("oid"),
-- from pg_catalog.pg_database
-- limit 10;

  declare

    v_db_ddl text;
    v_db_params record;
    v_options text;

  begin

    -- Checking existence of database
    select d.datname,
           u.rolname,
           d.datistemplate,
           d.datconnlimit,
           d.datallowconn,
           d.datlocprovider,
           d.daticulocale,
           d.datcollate,
           d.datctype,
           d.datcollversion,
           d.encoding,
           t.spcname
    into v_db_params
    from pg_catalog.pg_database d
    join pg_catalog.pg_authid u on d.datdba = u."oid"
    join pg_catalog.pg_tablespace t on d.dattablespace = t."oid"
    where d."oid" = db_oid;

    if v_db_params.datname is null then
      raise warning 'Database with OID % does not exist', db_oid;

      return null;
    end if;

    -- Building DDL

    v_db_ddl := 'CREATE DATABASE ' || quote_ident(v_db_params.datname)
                || E'\n    OWNER = ' || quote_ident(v_db_params.rolname)
                || E'\n    ENCODING = ''' || pg_catalog.pg_encoding_to_char(v_db_params.encoding) || E'''\n    '
                || case when v_db_params.daticulocale is null and v_db_params.datcollate is null then ''
                        when v_db_params.daticulocale is not null then 'ICU_LOCALE = ''' || v_db_params.daticulocale || E'''\n    '
                   else 'LC_COLLATE = ''' || v_db_params.datcollate || E'''\n    LC_CTYPE = '''|| v_db_params.datctype || E'''\n    ' end
                || 'LOCALE_PROVIDER = ' ||
                   case v_db_params.datlocprovider when 'i' then 'icu'
                                                   when 'c' then 'libc'
                                                   when 'd' then 'default'
                   end || E'\n    '
                || case when v_db_params.datcollversion is not null then 'COLLATION_VERSION = ''' || v_db_params.datcollversion || E'''\n    ' else '' end
                || 'TABLESPACE = ' || quote_ident(v_db_params.spcname)
                || E'\n    ALLOW_CONNECTIONS = ' || v_db_params.datallowconn || E'\n    '
                || case when v_db_params.datconnlimit != -1 then 'CONNECTION LIMIT = ' || v_db_params.datconnlimit || E'\n    '  else '' end
                || 'IS_TEMPLATE = ' || v_db_params.datistemplate
                || E'\n    OID = ' || db_oid || E';';

    return v_db_ddl;

  end;

$$;

comment on function get_database_def(db_oid oid)
    is 'Generates CREATE command for a database by its OID';