-- Examples:

-- select get_collation_def(c.collname, n.nspname),
--        get_collation_def(c."oid")
-- from pg_catalog.pg_collation c
-- join pg_catalog.pg_namespace n on c.collnamespace = n."oid";

-- select get_collation_def('ru-RU-x-icu');


-- Name + schema

create or replace function get_collation_def
  (
    in_coll text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_collation_def('ru-RU-x-icu');

-- select get_collation_def(collname)
-- from pg_catalog.pg_collation
-- where collnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_coll_ddl text;
    v_coll_params record;
    v_options text;

  begin

    -- Checking existence of collation
    select c."oid",
           c.collprovider,
           c.collisdeterministic,
           c.collcollate,
           c.collctype,
           c.colliculocale,
           c.collversion,
           n.nspname
    into v_coll_params
    from pg_catalog.pg_collation c
    left join pg_catalog.pg_namespace n on c.collnamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.collname = in_coll
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_coll_params."oid" is null then
      raise warning 'Collation %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_coll);

      return null;
    end if;

    -- Building DDL

    v_coll_ddl := 'CREATE COLLATION IF NOT EXISTS ' || quote_ident(v_coll_params.nspname) || '.' || quote_ident(in_coll) || E'\n(\n    '
                  || case when v_coll_params.colliculocale is null and v_coll_params.collcollate is null then ''
                          when v_coll_params.colliculocale is not null then 'LOCALE = ''' || v_coll_params.colliculocale || E''',\n    '
                     else 'LC_COLLATE = ''' || v_coll_params.collcollate || E''',\n    LC_CTYPE = '''|| v_coll_params.collctype || E''',\n    ' end
                  || 'PROVIDER = ' ||
                     case v_coll_params.collprovider when 'i' then 'icu'
                                                     when 'c' then 'libc'
                                                     when 'd' then 'default'
                     end || E',\n    '
                  || 'DETERMINISTIC = ' || v_coll_params.collisdeterministic::text
                  || case when v_coll_params.collversion is not null then E',\n    VERSION = ''' || v_coll_params.collversion || E'''\n);'
                     else E'\n);' end;

    return v_coll_ddl;

  end;

$$;

comment on function get_collation_def(in_coll text, in_schema text)
    is 'Generates CREATE command for a collation by its name and schema (default - search_path)';


-- OID

create or replace function get_collation_def
  (
    coll_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_collation_def('"ru-RU-x-icu"'::regcollation);

-- select get_collation_def("oid")
-- from pg_catalog.pg_collation
-- limit 10;

  declare

    v_coll_ddl text;
    v_coll_params record;
    v_options text;

  begin

    -- Checking existence of collation
    select c.collname,
           n.nspname,
           c.collprovider,
           c.collisdeterministic,
           c.collcollate,
           c.collctype,
           c.colliculocale,
           c.collversion
    into v_coll_params
    from pg_catalog.pg_collation c
    join pg_catalog.pg_namespace n on c.collnamespace = n."oid"
    where c."oid" = coll_oid;

    if v_coll_params.collname is null then
      raise warning 'Collation with OID % does not exist', coll_oid;

      return null;
    end if;

    -- Building DDL

    v_coll_ddl := 'CREATE COLLATION IF NOT EXISTS ' || quote_ident(v_coll_params.nspname) || '.' || quote_ident(v_coll_params.collname) || E'\n(\n    '
                  || case when v_coll_params.colliculocale is null and v_coll_params.collcollate is null then ''
                          when v_coll_params.colliculocale is not null then 'LOCALE = ''' || v_coll_params.colliculocale || E''',\n    '
                     else 'LC_COLLATE = ''' || v_coll_params.collcollate || E''',\n    LC_CTYPE = '''|| v_coll_params.collctype || E''',\n    ' end
                  || 'PROVIDER = ' ||
                     case v_coll_params.collprovider when 'i' then 'icu'
                                                     when 'c' then 'libc'
                                                     when 'd' then 'default'
                     end || E',\n    '
                  || 'DETERMINISTIC = ' || v_coll_params.collisdeterministic::text
                  || case when v_coll_params.collversion is not null then E',\n    VERSION = ''' || v_coll_params.collversion || E'''\n);'
                     else E'\n);' end;

    return v_coll_ddl;

  end;

$$;

comment on function get_collation_def(coll_oid oid)
    is 'Generates CREATE command for a collation by its OID';