-- Examples:

-- select pg_catalog.pg_get_statisticsobjdef(s."oid"), -- comparison with built-in system function
--        get_statistics_def(s.stxname, n.nspname),
--        get_statistics_def(s."oid")
-- from pg_catalog.pg_statistic_ext s
-- join pg_catalog.pg_namespace n on s.stxnamespace = n."oid";


-- Name

create or replace function get_statistics_def
  (
    in_stat text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_statistics_def(stxname)
-- from pg_catalog.pg_statistic_ext s
-- where stxnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_stat_ddl text;
    v_stat_params record;
    v_stat_kind text;

    v_col_expr_arr text[];
    expr_parts int;
    check_start int;
    check_end int;
    check_expr text;
    v_stat_expr_split text[];
    is_sql boolean := false;

  begin

    -- Checking existence of statistics object

    select st."oid",
           st.stxrelid,
           st.stxstattarget,
           st.stxkeys,
           st.stxkind,
           st.stxexprs,
           c.relname as parent_name,
           nt.nspname as parent_schema,
           n.nspname as stat_schema
    into v_stat_params
    from pg_catalog.pg_statistic_ext st
    join pg_catalog.pg_class c on st.stxrelid = c."oid"
    join pg_catalog.pg_namespace nt on nt."oid" = c.relnamespace
    left join pg_catalog.pg_namespace n on n."oid" = st.stxnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where in_stat = st.stxname
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_stat_params."oid" is null then
      raise warning 'Statistics object %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_stat);

      return null;
    end if;

    -- Statistics kinds
    if cardinality(v_stat_params.stxkind) = 1 and v_stat_params.stxkind[1] = 'e' then
        v_stat_kind = '';
    else
        select E'\n    (' || string_agg(str, ', ' order by rn) || ')'
        into v_stat_kind
        from
        (
            select
                case str when 'd' then 'ndistinct'
                         when 'f' then 'dependencies'
                         when 'm' then 'mcv'
                end as str,
                rn
            from unnest(v_stat_params.stxkind) with ordinality k (str, rn)
            where str != 'e'
        ) s;
    end if;

    -- Getting an array of statistics keys (columns/expressions)
    select array_agg(quote_ident(a.attname) order by ik.cn)
    into v_col_expr_arr
    from unnest(v_stat_params.stxkeys) with ordinality ik (colnum, cn)
    left join pg_catalog.pg_attribute a on v_stat_params.stxrelid = a.attrelid
                                           and a.attnum = ik.colnum;

    -- Parsing expressions
      -- pg_get_expr(pg_statistic_ext.stxexprs, pg_statistic_ext.stxrelid) basically yields a string of key expressions delimited by ', ' (a proper array here would be nice, but pg_node_tree can't be parsed in other way)
      -- It brings a problem that if expression has ', ' inside we can't determine start and end of each expression
      -- So, code below splits pg_get_expr output into tokens by ', ' delimiter, then check each consequent combination as a valid SQL-expression
          -- Using is_sql function from https://github.com/rin-nas/postgresql-patterns-library/blob/master/functions/is_sql.sql
      -- Since no SQL-expression could end with trailing ', ' it gives us a proper delimitation between statistics key expressions

    if v_stat_params.stxexprs is not null then

      -- Checking count of parts between ', '
      select (length(expr) - length(replace(expr, ', ', ''))) / 2 + 1
      into expr_parts
      from
      (
        select pg_catalog.pg_get_expr(v_stat_params.stxexprs, v_stat_params.stxrelid, true) as expr
      ) e;

      -- Initial point
      check_start := 1;
      check_end := 1;

      -- Splitted expressions
      select array_agg(split_part(stat_expr, ', ', parts) order by parts)
      into v_stat_expr_split
      from
      (
        select sp.parts,
               pg_catalog.pg_get_expr(v_stat_params.stxexprs, v_stat_params.stxrelid, true) as stat_expr
        from generate_series(1, expr_parts) sp (parts)
      ) a;

      -- Looping through all available SQL-expressions
        -- There are no indicators of how many expressions are specified in DDL (like indnkeys for indices)
      for i in 1..expr_parts
      loop

        while is_sql is false
        loop

          -- If part_1 is not valid SQL expression, then it checks part_1 || ', ' || part_2, and so on until it gets valid SQL expression
          -- Valid SQL expression put into v_col_expr_arr at the corresponding key index
          -- Next cycle starts with part_n where n = *number of part that made previous expression valid* + 1

          check_expr := array_to_string(v_stat_expr_split[check_start:check_end], ', ');

          select is_sql('select ' || check_expr || ' from ' || v_stat_params.parent_schema || '.' || v_stat_params.parent_name || ';') as is_sql
          into is_sql;

          check_end := check_end + 1;

        end loop;

        check_start := check_end;
        is_sql := false;
        v_col_expr_arr := array_append(v_col_expr_arr, '(' || check_expr || ')');

      end loop;

      -- Cleaning up nulls
      v_col_expr_arr := array_remove(v_col_expr_arr, '()');

    end if;

    -- Building DDL

    v_stat_ddl := 'CREATE STATISTICS IF NOT EXISTS ' || quote_ident(v_stat_params.stat_schema) || '.' || quote_ident(in_stat)
                  || v_stat_kind
                  || E'\n    ON ' || array_to_string(v_col_expr_arr, ', ')
                  || E'\n    FROM '|| quote_ident(v_stat_params.parent_schema) || '.' || quote_ident(v_stat_params.parent_name) ||';'

                  || case when v_stat_params.stxstattarget != -1
                               then E'\n\nALTER STATISTICS ' || quote_ident(v_stat_params.stat_schema) || '.' || quote_ident(in_stat)
                                    || ' SET STATISTICS ' || v_stat_params.stxstattarget || ';'
                     else '' end;

    return v_stat_ddl;

  end;

$$;

comment on function get_statistics_def(in_stat text, in_schema text)
    is 'Generates CREATE command for a statistics object by its name and schema (default - search_path)';


-- OID

create or replace function get_statistics_def
  (
    stat_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_statistics_def("oid")
-- from pg_catalog.pg_statistic_ext s
-- limit 10;

  declare

    v_stat_ddl text;
    v_stat_params record;
    v_stat_kind text;

    v_col_expr_arr text[];
    expr_parts int;
    check_start int;
    check_end int;
    check_expr text;
    v_stat_expr_split text[];
    is_sql boolean := false;

  begin

    -- Checking existence of statistics object

    select st.stxname,
           st.stxrelid,
           st.stxstattarget,
           st.stxkeys,
           st.stxkind,
           st.stxexprs,
           c.relname as parent_name,
           nt.nspname as parent_schema,
           n.nspname as stat_schema
    into v_stat_params
    from pg_catalog.pg_statistic_ext st
    join pg_catalog.pg_class c on st.stxrelid = c."oid"
    join pg_catalog.pg_namespace nt on nt."oid" = c.relnamespace
    join pg_catalog.pg_namespace n on n."oid" = st.stxnamespace
    where st."oid" = stat_oid;

    if v_stat_params.stxname is null then
      raise warning 'Statistics object with OID % does not exist', stat_oid;

      return null;
    end if;

    -- Statistics kinds
    if cardinality(v_stat_params.stxkind) = 1 and v_stat_params.stxkind[1] = 'e' then
        v_stat_kind = '';
    else
        select E'\n    (' || string_agg(str, ', ' order by rn) || ')'
        into v_stat_kind
        from
        (
            select
                case str when 'd' then 'ndistinct'
                         when 'f' then 'dependencies'
                         when 'm' then 'mcv'
                end as str,
                rn
            from unnest(v_stat_params.stxkind) with ordinality k (str, rn)
            where str != 'e'
        ) s;
    end if;

    -- Getting an array of statistics keys (columns/expressions)
    select array_agg(quote_ident(a.attname) order by ik.cn)
    into v_col_expr_arr
    from unnest(v_stat_params.stxkeys) with ordinality ik (colnum, cn)
    left join pg_catalog.pg_attribute a on v_stat_params.stxrelid = a.attrelid
                                           and a.attnum = ik.colnum;

    -- Parsing expressions
      -- pg_get_expr(pg_statistic_ext.stxexprs, pg_statistic_ext.stxrelid) basically yields a string of key expressions delimited by ', ' (a proper array here would be nice, but pg_node_tree can't be parsed in other way)
      -- It brings a problem that if expression has ', ' inside we can't determine start and end of each expression
      -- So, code below splits pg_get_expr output into tokens by ', ' delimiter, then check each consequent combination as a valid SQL-expression
          -- Using is_sql function from https://github.com/rin-nas/postgresql-patterns-library/blob/master/functions/is_sql.sql
      -- Since no SQL-expression could end with trailing ', ' it gives us a proper delimitation between statistics key expressions

    if v_stat_params.stxexprs is not null then

      -- Checking count of parts between ', '
      select (length(expr) - length(replace(expr, ', ', ''))) / 2 + 1
      into expr_parts
      from
      (
        select pg_catalog.pg_get_expr(v_stat_params.stxexprs, v_stat_params.stxrelid, true) as expr
      ) e;

      -- Initial point
      check_start := 1;
      check_end := 1;

      -- Splitted expressions
      select array_agg(split_part(stat_expr, ', ', parts) order by parts)
      into v_stat_expr_split
      from
      (
        select sp.parts,
               pg_catalog.pg_get_expr(v_stat_params.stxexprs, v_stat_params.stxrelid, true) as stat_expr
        from generate_series(1, expr_parts) sp (parts)
      ) a;

      -- Looping through all stat key positions with SQL-expressions
      for i in 1..expr_parts
      loop

        while is_sql is false
        loop

          -- If part_1 is not valid SQL expression, then it checks part_1 || ', ' || part_2, and so on until it gets valid SQL expression
          -- Valid SQL expression put into v_col_expr_arr at the corresponding key index
          -- Next cycle starts with part_n where n = *number of part that made previous expression valid* + 1

          check_expr := array_to_string(v_stat_expr_split[check_start:check_end], ', ');

          select is_sql('select ' || check_expr || ' from ' || v_stat_params.parent_schema || '.' || v_stat_params.parent_name || ';') as is_sql
          into is_sql;

          check_end := check_end + 1;

        end loop;

        check_start := check_end;
        is_sql := false;
        v_col_expr_arr := array_append(v_col_expr_arr, '(' || check_expr || ')');

      end loop;

      -- Cleaning up empty expressions
      v_col_expr_arr := array_remove(v_col_expr_arr, '()');

    end if;

    -- Building DDL

    v_stat_ddl := 'CREATE STATISTICS IF NOT EXISTS ' || quote_ident(v_stat_params.stat_schema) || '.' || quote_ident(v_stat_params.stxname)
                  || v_stat_kind
                  || E'\n    ON ' || array_to_string(v_col_expr_arr, ', ')
                  || E'\n    FROM '|| quote_ident(v_stat_params.parent_schema) || '.' || quote_ident(v_stat_params.parent_name) ||';'

                  || case when v_stat_params.stxstattarget != -1
                               then E'\n\nALTER STATISTICS ' || quote_ident(v_stat_params.stat_schema) || '.' || quote_ident(v_stat_params.stxname)
                                    || ' SET STATISTICS ' || v_stat_params.stxstattarget || ';'
                     else '' end;

    return v_stat_ddl;

  end;

$$;

comment on function get_statistics_def(stat_oid oid)
    is 'Generates CREATE command for a statistics object by its OID';