-- Examples:

-- select get_conversion_def(c.conname, n.nspname),
--        get_conversion_def(c."oid")
-- from pg_catalog.pg_conversion c
-- join pg_catalog.pg_namespace n on c.connamespace = n."oid";

-- select get_conversion_def('utf8_to_windows_1251');


-- Name + schema

create or replace function get_conversion_def
  (
    in_con text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_conversion_def('utf8_to_windows_1251');

-- select get_conversion_def(conname)
-- from pg_catalog.pg_conversion
-- where connamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_con_ddl text;
    v_con_params record;
    v_func text;

  begin

    -- Checking existence of conversion
    select c."oid",
           c.conforencoding,
           c.contoencoding,
           c.conproc,
           c.condefault,
           n.nspname
    into v_con_params
    from pg_catalog.pg_conversion c
    left join pg_catalog.pg_namespace n on c.connamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.conname = in_con
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_con_params."oid" is null then
      raise warning 'Conversion %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_con);

      return null;
    end if;

    -- Conversion function
    select get_function_identity_arguments(v_con_params.conproc, false, false)
    into v_func;

    -- Building DDL

    v_con_ddl := 'CREATE '
                 || case when v_con_params.condefault is true then 'DEFAULT ' else '' end
                 || 'CONVERSION ' || quote_ident(v_con_params.nspname) || '.' || quote_ident(in_con)
                 || E'\n    FOR ''' || pg_catalog.pg_encoding_to_char(v_con_params.conforencoding) || ''' TO ''' || pg_catalog.pg_encoding_to_char(v_con_params.contoencoding)
                 || E'''\n    FROM ' || v_func || ';';

    return v_con_ddl;

  end;

$$;

comment on function get_conversion_def(in_con text, in_schema text)
    is 'Generates CREATE command for a conversion by its name and schema (default - search_path)';


-- OID

create or replace function get_conversion_def
  (
    con_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_conversion_def("oid")
-- from pg_catalog.pg_conversion
-- limit 10;

  declare

    v_con_ddl text;
    v_con_params record;
    v_func text;

  begin

    -- Checking existence of conversion
    select c.conname,
           n.nspname,
           c.conforencoding,
           c.contoencoding,
           c.conproc,
           c.condefault
    into v_con_params
    from pg_catalog.pg_conversion c
    join pg_catalog.pg_namespace n on c.connamespace = n."oid"
    where c."oid" = con_oid;

    if v_con_params.conname is null then
      raise warning 'Conversion with OID % does not exist', con_oid;

      return null;
    end if;

    -- Conversion function
    select get_function_identity_arguments(v_con_params.conproc, false, false)
    into v_func;

    -- Building DDL

    v_con_ddl := 'CREATE '
                 || case when v_con_params.condefault is true then 'DEFAULT ' else '' end
                 || 'CONVERSION ' || quote_ident(v_con_params.nspname) || '.' || quote_ident(v_con_params.conname)
                 || E'\n    FOR ''' || pg_catalog.pg_encoding_to_char(v_con_params.conforencoding) || ''' TO ''' || pg_catalog.pg_encoding_to_char(v_con_params.contoencoding)
                 || E'''\n    FROM ' || v_func || ';';

    return v_con_ddl;

  end;

$$;

comment on function get_conversion_def(con_oid oid)
    is 'Generates CREATE command for a conversion by its OID';