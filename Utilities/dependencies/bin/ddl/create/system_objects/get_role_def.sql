-- Examples:

-- select get_role_def(rolname),
--        get_role_def("oid")
-- from pg_catalog.pg_authid;


-- Name

create or replace function get_role_def
  (
    in_rol text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_role_def(rolname)
-- from pg_catalog.pg_authid;

  declare

    v_rol_ddl text;
    v_rol_params record;
    v_group_rec record;
    v_group_members text := '';
    v_group_admins text := '';
    v_member_of_groups text;

  begin

    if lower(in_rol) = 'public' then
      raise warning 'Role ''public'' is a pseudo-role';

      return null;
    end if;

    -- Checking existence of role
    select "oid",
           rolsuper,
           rolinherit,
           rolcreaterole,
           rolcreatedb,
           rolcanlogin,
           rolreplication,
           rolbypassrls,
           rolconnlimit,
           rolpassword,
           rolvaliduntil
    into v_rol_params
    from pg_catalog.pg_authid
    where rolname = in_rol;

    if v_rol_params."oid" is null then
      raise warning 'Role % does not exist', quote_ident(in_rol);

      return null;
    end if;

    -- Group members
    for v_group_rec in
      select r.rolname, g.admin_option
      from pg_catalog.pg_auth_members g
      join pg_catalog.pg_authid r on g.member = r."oid"
      where g.roleid = v_rol_params."oid"

    loop
      if v_group_rec.admin_option is false then
        v_group_members := v_group_members || ', ' || quote_ident(v_group_rec.rolname);
      elsif v_group_rec.admin_option is true then
        v_group_admins := v_group_admins || ', ' || quote_ident(v_group_rec.rolname);
      end if;
    end loop;

    -- Member of groups
    select 'IN ROLE ' || string_agg(quote_ident(r.rolname), ', ')
    into v_member_of_groups
    from pg_catalog.pg_auth_members g
    join pg_catalog.pg_authid r on g.roleid = r."oid"
    where g.member = v_rol_params."oid";

    -- Building DDL

    v_rol_ddl := 'CREATE ROLE ' || quote_ident(in_rol) || E'\n    '
                 || case when v_rol_params.rolsuper is true then 'SUPERUSER' else 'NOSUPERUSER' end || E'\n    '
                 || case when v_rol_params.rolcreatedb is true then 'CREATEDB' else 'NOCREATEDB' end || E'\n    '
                 || case when v_rol_params.rolcreaterole is true then 'CREATEROLE' else 'NOCREATEROLE' end || E'\n    '
                 || case when v_rol_params.rolinherit is true then 'INHERIT' else 'NOINHERIT' end || E'\n    '
                 || case when v_rol_params.rolcanlogin is true then 'LOGIN' else 'NOLOGIN' end || E'\n    '
                 || case when v_rol_params.rolreplication is true then 'REPLICATION' else 'NOREPLICATION' end || E'\n    '
                 || case when v_rol_params.rolbypassrls is true then 'BYPASSRLS' else 'NOBYPASSRLS' end || E'\n    '
                 || case when v_rol_params.rolconnlimit != -1 then 'CONNECTION LIMIT ' || v_rol_params.rolconnlimit || E'\n    '  else '' end
                 || case when v_rol_params.rolpassword is null then 'PASSWORD NULL' else 'PASSWORD ''*encrypted_password*''' end
                 || case when v_rol_params.rolvaliduntil is not null then E'\n    VALID UNTIL ''' || v_rol_params.rolvaliduntil || ''''  else '' end
                 || case when v_member_of_groups is not null then E'\n    ' || v_member_of_groups else '' end
                 || case when v_group_members != '' then E'\n    ROLE ' || ltrim(v_group_members, ', ') else '' end
                 || case when v_group_admins != '' then E'\n    ADMIN ' || ltrim(v_group_admins, ', ') else '' end
                 || ';';

    return v_rol_ddl;

  end;

$$;

comment on function get_role_def(in_rol text)
    is 'Generates CREATE command for a role by its name';


-- OID

create or replace function get_role_def
  (
    rol_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_role_def("oid")
-- from pg_catalog.pg_authid;

  declare

    v_rol_ddl text;
    v_rol_params record;
    v_group_rec record;
    v_group_members text := '';
    v_group_admins text := '';
    v_member_of_groups text;

  begin

    if rol_oid = 0::oid then
      raise warning 'Role ''public'' is a pseudo-role';

      return null;
    end if;

    -- Checking existence of role
    select rolname,
           rolsuper,
           rolinherit,
           rolcreaterole,
           rolcreatedb,
           rolcanlogin,
           rolreplication,
           rolbypassrls,
           rolconnlimit,
           rolpassword,
           rolvaliduntil
    into v_rol_params
    from pg_catalog.pg_authid
    where "oid" = rol_oid;

    if v_rol_params.rolname is null then
      raise warning 'Role with OID % does not exist', rol_oid;

      return null;
    end if;

    -- Group members
    for v_group_rec in
      select r.rolname, g.admin_option
      from pg_catalog.pg_auth_members g
      join pg_catalog.pg_authid r on g.member = r."oid"
      where g.roleid = rol_oid

    loop
      if v_group_rec.admin_option is false then
        v_group_members := v_group_members || ', ' || quote_ident(v_group_rec.rolname);
      elsif v_group_rec.admin_option is true then
        v_group_admins := v_group_admins || ', ' || quote_ident(v_group_rec.rolname);
      end if;
    end loop;

    -- Member of groups
    select 'IN ROLE ' || string_agg(quote_ident(r.rolname), ', ')
    into v_member_of_groups
    from pg_catalog.pg_auth_members g
    join pg_catalog.pg_authid r on g.roleid = r."oid"
    where g.member = rol_oid;

    -- Building DDL

    v_rol_ddl := 'CREATE ROLE ' || quote_ident(v_rol_params.rolname) || E'\n    '
                 || case when v_rol_params.rolsuper is true then 'SUPERUSER' else 'NOSUPERUSER' end || E'\n    '
                 || case when v_rol_params.rolcreatedb is true then 'CREATEDB' else 'NOCREATEDB' end || E'\n    '
                 || case when v_rol_params.rolcreaterole is true then 'CREATEROLE' else 'NOCREATEROLE' end || E'\n    '
                 || case when v_rol_params.rolinherit is true then 'INHERIT' else 'NOINHERIT' end || E'\n    '
                 || case when v_rol_params.rolcanlogin is true then 'LOGIN' else 'NOLOGIN' end || E'\n    '
                 || case when v_rol_params.rolreplication is true then 'REPLICATION' else 'NOREPLICATION' end || E'\n    '
                 || case when v_rol_params.rolbypassrls is true then 'BYPASSRLS' else 'NOBYPASSRLS' end || E'\n    '
                 || case when v_rol_params.rolconnlimit != -1 then 'CONNECTION LIMIT ' || v_rol_params.rolconnlimit || E'\n    '  else '' end
                 || case when v_rol_params.rolpassword is null then 'PASSWORD NULL' else 'PASSWORD ''*encrypted_password*''' end
                 || case when v_rol_params.rolvaliduntil is not null then E'\n    VALID UNTIL ''' || v_rol_params.rolvaliduntil || ''''  else '' end
                 || case when v_member_of_groups is not null then E'\n    ' || v_member_of_groups else '' end
                 || case when v_group_members != '' then E'\n    ROLE ' || ltrim(v_group_members, ', ') else '' end
                 || case when v_group_admins != '' then E'\n    ADMIN ' || ltrim(v_group_admins, ', ') else '' end
                 || ';';

    return v_rol_ddl;

  end;

$$;

comment on function get_role_def(rol_oid oid)
    is 'Generates CREATE command for a role by its OID';