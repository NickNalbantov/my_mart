-- Examples:

-- select get_tablespace_def(spcname),
--        get_tablespace_def("oid")
-- from pg_catalog.pg_tablespace;


-- Name

create or replace function get_tablespace_def
  (
    in_tblspc text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_tablespace_def(spcname)
-- from pg_catalog.pg_tablespace;

  declare

    v_tblspc_ddl text;
    v_tblspc_params record;
    v_options text;

  begin

    -- Checking existence of tablespace
    select t."oid",
           u.rolname,
           case when t.spcname::text in ('pg_default', 'pg_global') then dd.setting
                else pg_catalog.pg_tablespace_location(t."oid") end as dir,
           t.spcoptions
    into v_tblspc_params
    from pg_catalog.pg_tablespace t
    join pg_catalog.pg_authid u on t.spcowner = u."oid"
    cross join (
                select setting
                from pg_catalog.pg_settings
                where "name" = 'data_directory'
               ) as dd
    where t.spcname = in_tblspc;

    if v_tblspc_params."oid" is null then
      raise warning 'Tablespace % does not exist', quote_ident(in_tblspc);

      return null;
    end if;

    -- Options
    if v_tblspc_params.spcoptions is not null then
      select E'\n    OPTIONS (' || string_agg(str, ', ') || ')'
      into v_options
      from
      (
        select regexp_replace(unnest(coalesce(v_tblspc_params.spcoptions, array[null::text])), '^(.+)=(.+)$', '\1 ''\2''') as str
      ) a;
    else
      v_options := '';
    end if;

    -- Building DDL

    v_tblspc_ddl := 'CREATE TABLESPACE ' || quote_ident(in_tblspc )
                    || E'\n    OWNER ' || quote_ident(v_tblspc_params.rolname)
                    || E'\n    LOCATION ''' || v_tblspc_params.dir || ''''
                    || v_options || ';';

    return v_tblspc_ddl;

  end;

$$;

comment on function get_tablespace_def(in_tblspc text)
    is 'Generates CREATE command for a tablespace by its name';


-- OID

create or replace function get_tablespace_def
  (
    tblspc_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_tablespace_def("oid")
-- from pg_catalog.pg_tablespace;

  declare

    v_tblspc_ddl text;
    v_tblspc_params record;
    v_options text;

  begin

    -- Checking existence of tablespace
    select t.spcname,
           u.rolname,
           case when t.spcname::text in ('pg_default', 'pg_global') then dd.setting
                else pg_tablespace_location(t."oid") end as dir,
           t.spcoptions
    into v_tblspc_params
    from pg_catalog.pg_tablespace t
    join pg_catalog.pg_authid u on t.spcowner = u."oid"
    cross join (
                select setting
                from pg_catalog.pg_settings
                where "name" = 'data_directory'
               ) as dd
    where t."oid" = tblspc_oid;

    if v_tblspc_params.spcname is null then
      raise warning 'Tablespace with OID % does not exist', tblspc_oid;

      return null;
    end if;

    -- Options
    if v_tblspc_params.spcoptions is not null then
      select E'\n    OPTIONS (' || string_agg(str, ', ') || ')'
      into v_options
      from
      (
        select regexp_replace(unnest(coalesce(v_tblspc_params.spcoptions, array[null::text])), '^(.+)=(.+)$', '\1 ''\2''') as str
      ) a;
    else
      v_options := '';
    end if;

    -- Building DDL

    v_tblspc_ddl := 'CREATE TABLESPACE ' || quote_ident(v_tblspc_params.spcname)
                    || E'\n    OWNER ' || quote_ident(v_tblspc_params.rolname)
                    || E'\n    LOCATION ''' || v_tblspc_params.dir || ''''
                    || v_options || ';';
    return v_tblspc_ddl;

  end;

$$;

comment on function get_tablespace_def(tblspc_oid oid)
    is 'Generates CREATE command for a tablespace by its OID';