-- Examples:

-- select get_type_def(t.typname, n.nspname),
--        get_type_def(t."oid")
-- from pg_catalog.pg_type t
-- join pg_catalog.pg_namespace n on t.typnamespace = n."oid";

-- select get_type_def('integer');


-- Name + schema

create or replace function get_type_def
  (
    in_type text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Generates DDL script for all kind of types (CREATE TYPE... for base types, composite types, pseudotypes, ranges, multiranges, enums) and domains (CREATE DOMAIN...)

-- Examples:

-- select get_type_def('integer');

-- select get_type_def(typname)
-- from pg_catalog.pg_type
-- where typnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_ddl text;
    v_type_params record;
    v_enum_labels text;
    v_range_params record;
    v_domain_collation text;
    v_domain_basetype record;
    v_domain_type text;
    v_domain_constraint_params record;
    v_domain_constraints text;
    v_att_params record;
    in_type_fixed text;

    int_syn text[] := array['integer', 'int'];
    smallint_syn text := 'smallint';
    bigint_syn text := 'bigint';
    num_syn text := 'decimal';
    real_syn text := 'real';
    dp_syn text[] := array['double precision', 'float'];
    time_syn text := 'time without time zone';
    timetz_syn text := 'time with time zone';
    timestamp_syn text := 'timestamp without time zone';
    timestamptz_syn text := 'timestamp with time zone';
    bpchar_syn text := 'character';
    varchar_syn text[] := array['character varying', 'char varying'];
    varbit_syn text := 'bit varying';
    bool_syn text := 'boolean';

  begin

    -- Catching synonyms for type
    select case when right(tp, 2) = '[]' then rtrim('_' || tp, '[]')
           else tp end
    into in_type_fixed
    from
    (
      select
        case when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = any(int_syn) then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1int4\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = smallint_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1int2\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = bigint_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1int8\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = num_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1numeric\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = real_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1float4\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = any(dp_syn) then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1float8\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = time_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1time\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = timetz_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1timetz\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = timestamp_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1timestamp\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = timestamptz_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1timestamptz\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = bpchar_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1bpchar\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = any(varchar_syn) then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1varchar\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = varbit_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1varbit\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = bool_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1bool\2')
        else in_type end as tp
    ) a;

    -- Checking existence of type
    select t."oid",
           t.typtype, -- type class: normal, composite, enum, range, multirange etc.
           case when t.typlen > 0 then t.typlen else 0 end as typlen,
           t.typbyval,
           t.typcategory::text as typcategory,
           t.typispreferred,
           t.typisdefined, -- false - shell type
           t.typdelim::text as typdelim,
           t.typsubscript as typsubscript,
           coalesce(e.typname::text, 'none') as element,
           t.typinput as typinput,
           t.typoutput as typoutput,
           t.typsend as typsend,
           t.typreceive as typreceive,
           t.typmodin as typmodin,
           t.typmodout as typmodout,
           t.typanalyze as typanalyze,
           case t.typalign when 'c' then 'char' when 's' then 'int2' when 'i' then 'int4' when 'd' then 'double' end as typalign,
           case t.typstorage when 'p' then 'plain' when 'e' then 'external' when 'm' then 'main' when 'x' then 'extended' end as typstorage,
           t.typnotnull, -- domain only
           t.typbasetype, -- base type for domain
           t.typtypmod,
           t.typndims, -- domain-arrays only
           t.typcollation,
           t.typdefault,
           n.nspname
    into v_type_params
    from pg_catalog.pg_type t
    left join pg_catalog.pg_type e on t.typelem = e."oid"
    left join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where in_type_fixed = t.typname
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_type_params."oid" is null then
      raise warning 'Type %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_type);

      return null;
    end if;

--===========================================================================================
    -- Building DDL
--===========================================================================================

      -- Shell type
    if v_type_params.typisdefined = false then
      v_ddl := 'CREATE TYPE ' || quote_ident(v_type_params.nspname) || '.' || quote_ident(in_type_fixed) || ';';

--=========================================

      -- Enum
    elsif v_type_params.typisdefined = true and v_type_params.typtype = 'e'::char then
      select string_agg('''' || enumlabel || '''', E',\n    ' order by enumsortorder)
      into v_enum_labels
      from pg_catalog.pg_enum
      where enumtypid = v_type_params."oid";

      v_ddl := 'CREATE TYPE ' || quote_ident(v_type_params.nspname) || '.' || quote_ident(in_type_fixed) || E' AS ENUM\n(\n    ' || v_enum_labels || E'\n);';

--=========================================

      -- Range (but not multirange)
    elsif v_type_params.typisdefined = true and v_type_params.typtype = 'r'::char then
      select quote_ident(nt.nspname) || '.' || quote_ident(t.typname) as rngsubtype,
             case when r.rngsubopc != 0 then quote_ident(n_o.nspname) || '.' || quote_ident(o.opcname) else null end as rngsubopc,
             case when r.rngcollation != 0 then quote_ident(nc.nspname) || '.' || quote_ident(c.collname) else null end as rngcollation,
             case when r.rngcanonical != '-'::regproc then get_function_identity_arguments(r.rngcanonical::oid, false, false) else '-' end as rngcanonical,
             case when r.rngsubdiff != '-'::regproc then get_function_identity_arguments(r.rngsubdiff::oid, false, false) else '-' end as rngsubdiff,
             case when r.rngmultitypid != 0 then quote_ident(ntm.nspname) || '.' || quote_ident(tm.typname) else null end as multirange_type_name
      into v_range_params
      from pg_catalog.pg_range r
      join pg_catalog.pg_type t on r.rngsubtype = t."oid"
      left join pg_catalog.pg_type tm on r.rngmultitypid = tm."oid"
      left join pg_catalog.pg_opclass o on r.rngsubopc = o."oid"
      left join pg_catalog.pg_collation c on r.rngcollation = c."oid"
      left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
      left join pg_catalog.pg_namespace ntm on ntm."oid" = tm.typnamespace
      left join pg_catalog.pg_namespace n_o on n_o."oid" = o.opcnamespace
      left join pg_catalog.pg_namespace nc on nc."oid" = c.collnamespace
      where r.rngtypid = v_type_params."oid";

      v_ddl := 'CREATE TYPE ' || quote_ident(v_type_params.nspname) || '.' || quote_ident(in_type_fixed)
               || E' AS RANGE\n(\n    SUBTYPE = ' || v_range_params.rngsubtype
               || case when v_range_params.rngsubopc is not null then E',\n    SUBTYPE_OPCLASS = ' || v_range_params.rngsubopc || E',\n    ' else '' end
               || case when v_range_params.rngcollation is not null then 'COLLATION = ' || v_range_params.rngcollation || E',\n    ' else '' end
               || case when v_range_params.rngcanonical != '-' then 'CANONICAL = ' || v_range_params.rngcanonical || E',\n    ' else '' end
               || case when v_range_params.rngsubdiff != '-' then 'SUBTYPE_DIFF = ' || v_range_params.rngsubdiff || E',\n    ' else '' end
               || case when v_range_params.multirange_type_name is not null then 'MULTIRANGE_TYPE_NAME = ' || v_range_params.multirange_type_name else '' end
               || E'\n);';

--=========================================

      -- Domain
    elsif v_type_params.typisdefined = true and v_type_params.typtype = 'd'::char then
      -- collation
      if v_type_params.typcollation != 0 then
        select quote_ident(n.nspname) || '.' || quote_ident(c.collname)
        into v_domain_collation
        from pg_catalog.pg_collation c
        join pg_catalog.pg_namespace n on n."oid" = c.collnamespace
        where c."oid" = v_type_params.typcollation;
      end if;

        -- Building base type
      select t."oid",
             quote_ident(n.nspname) || '.'
                || case when v_type_params.typndims > 1 then quote_ident(substring(t.typname, 2, length(t.typname))) else quote_ident(t.typname) end as typname
      into v_domain_basetype
      from pg_catalog.pg_type t
      join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
      where t."oid" = v_type_params.typbasetype;

     -- precision and scale
        -- PG actually does not store info about float precision - it's either float4 or float8
        -- not using format_type function because it gives weird type names and not counting array dimensions correctly (not checking typtypmod / atttypmod)
      v_domain_type :=
        v_domain_basetype.typname
        || case when v_type_params.typtypmod != '-1'::integer then '(' ||
              -- bpchar, varchar max length
              case when v_domain_basetype."oid" = any(array['pg_catalog._bpchar'::regtype, 'pg_catalog._varchar'::regtype, 'pg_catalog.bpchar'::regtype, 'pg_catalog."varchar"'::regtype])
                        then (v_type_params.typtypmod - 4)::text
                   when v_domain_basetype."oid" = any(array['pg_catalog."bit"'::regtype, 'pg_catalog._bit'::regtype, 'pg_catalog.varbit'::regtype, 'pg_catalog._varbit'::regtype])
                        then v_type_params.typtypmod::text
              -- numeric precision
                   when v_domain_basetype."oid" = any(array['pg_catalog."numeric"'::regtype, 'pg_catalog._numeric'::regtype])
                        then (((v_type_params.typtypmod - 4) >> 16) & 65535)::text
              -- timestamp, timestamptz, time, timetz precision
                   when v_domain_basetype."oid" = any(array[
                                                            'pg_catalog."time"'::regtype, 'pg_catalog."timestamp"'::regtype, 'pg_catalog.timestamptz'::regtype, 'pg_catalog.timetz'::regtype,
                                                            'pg_catalog._time'::regtype, 'pg_catalog._timestamp'::regtype, 'pg_catalog._timestamptz'::regtype, 'pg_catalog._timetz'::regtype
                                                           ])
                        then
                            case when v_type_params.typtypmod < 0 then 6::text
                            else v_type_params.typtypmod::text end
              -- interval precision
                   when v_domain_basetype."oid" = any(array['pg_catalog."interval"'::regtype, 'pg_catalog._interval'::regtype])
                        then
                            case when v_type_params.typtypmod < 0 or v_type_params.typtypmod & 65535 = 65535 then 6::text
                            else (v_type_params.typtypmod & 65535)::text end
              else '' end
           else '' end
        || case when v_type_params.typtypmod != '-1'::integer then
              -- numeric scale
                  case when v_domain_basetype."oid" = any(array['pg_catalog."numeric"'::regtype, 'pg_catalog._numeric'::regtype])
                            then ', ' || ((v_type_params.typtypmod - 4) & 65535)::text || ')'
                  else ')' end
           else '' end
        -- there's no info about array length limit, so bpchar(3)[4][5] = bpchar(3)[][]
        || case when v_type_params.typndims > 1 then repeat('[]', v_type_params.typndims) else '' end;

      -- constraints
      v_domain_constraints := case when v_type_params.typnotnull = true then 'NULL' else 'NOT NULL' end;

      for v_domain_constraint_params in
        select "oid",
               get_constraint_def(c."oid") as condef
        from pg_catalog.pg_constraint c
        where v_type_params."oid" = c.contypid
      loop
        v_domain_constraints := v_domain_constraints
                                || case when v_domain_constraint_params."oid" is not null then E'\n    ' || v_domain_constraint_params.condef
                                   else '' end;
      end loop;

      -- Building DDL
      v_ddl := 'CREATE DOMAIN ' || quote_ident(v_type_params.nspname) || '.' || quote_ident(in_type_fixed) || ' AS ' || v_domain_type || E'\n    '
               || case when v_type_params.typcollation != 0 then 'COLLATE ' || v_domain_collation ||  E'\n    ' else '' end
               || case when v_type_params.typdefault is not null then 'DEFAULT ' || v_type_params.typdefault ||  E'\n    ' else '' end
               || v_domain_constraints || ';';

--=========================================

      -- Composite type
    elsif v_type_params.typisdefined = true and v_type_params.typtype = 'c'::char then
      v_ddl := 'CREATE TYPE ' || quote_ident(v_type_params.nspname) || '.' || quote_ident(in_type_fixed) || E'\n(\n    ';

      for v_att_params in
        select quote_ident(a.attname) as attname,
               quote_ident(n.nspname) || '.'
                  || case when a.attndims > 1 then quote_ident(substring(tc.typname, 2, length(tc.typname))) else quote_ident(tc.typname) end as typname,
               case when a.attndims > 1 then repeat('[]', a.attndims) else '' end as arr_dim,
               case when a.atttypmod != '-1'::integer then '(' ||
                    -- bpchar, varchar max length
                    case when tc."oid" = any(array['pg_catalog._bpchar'::regtype, 'pg_catalog._varchar'::regtype, 'pg_catalog.bpchar'::regtype, 'pg_catalog."varchar"'::regtype])
                              then (a.atttypmod - 4)::text
                         when tc."oid" = any(array['pg_catalog."bit"'::regtype, 'pg_catalog._bit'::regtype, 'pg_catalog.varbit'::regtype, 'pg_catalog._varbit'::regtype])
                              then a.atttypmod::text
                    -- numeric precision
                         when tc."oid" = any(array['pg_catalog."numeric"'::regtype, 'pg_catalog._numeric'::regtype])
                              then (((a.atttypmod - 4) >> 16) & 65535)::text
                    -- timestamp, timestamptz, time, timetz precision
                         when tc."oid" = any(array['pg_catalog."time"'::regtype, 'pg_catalog."timestamp"'::regtype, 'pg_catalog.timestamptz'::regtype, 'pg_catalog.timetz'::regtype,
                                                   'pg_catalog._time'::regtype, 'pg_catalog._timestamp'::regtype, 'pg_catalog._timestamptz'::regtype, 'pg_catalog._timetz'::regtype
                                                  ])
                              then
                                  case when a.atttypmod < 0 then 6::text
                                  else a.atttypmod::text end
                    -- interval precision
                         when tc."oid" = any(array['pg_catalog."interval"'::regtype, 'pg_catalog._interval'::regtype])
                              then
                                  case when a.atttypmod < 0 or a.atttypmod & 65535 = 65535 then 6::text
                                  else (a.atttypmod & 65535)::text end
                    else '' end
               else '' end
               || case when a.atttypmod != '-1'::integer then
                    -- numeric scale
                       case when tc."oid" = any(array['pg_catalog."numeric"'::regtype, 'pg_catalog._numeric'::regtype])
                            then ', ' || ((a.atttypmod - 4) & 65535)::text || ')'
                       else ')' end
                  else '' end
               as atttypmod
        from pg_catalog.pg_attribute a
        join pg_catalog.pg_type t on t.typrelid = a.attrelid
        join pg_catalog.pg_type tc on tc."oid" = a.atttypid
        join pg_catalog.pg_namespace n on n."oid" = tc.typnamespace
        where t."oid" = v_type_params."oid"
              and a.attnum > 0
        order by a.attnum
      loop
        v_ddl := v_ddl || v_att_params.attname || ' ' || v_att_params.typname || v_att_params.atttypmod || v_att_params.arr_dim || E',\n    ';
      end loop;

      v_ddl := rtrim(v_ddl, E',\n ') || E'\n);';

--=========================================

      -- Basetype, pseudotype, multirange
    elsif v_type_params.typisdefined = true and v_type_params.typtype in ('b'::char, 'p'::char, 'm'::char)  then
      v_ddl := 'CREATE TYPE ' || quote_ident(v_type_params.nspname) || '.' || quote_ident(in_type_fixed)
               || E'\n(\n    INPUT = ' || get_function_identity_arguments(v_type_params.typinput::oid, false, false) || E',\n    '
               || 'OUTPUT = ' || get_function_identity_arguments(v_type_params.typoutput::oid, false, false) || E',\n    '
               || case when v_type_params.typreceive != '-'::regproc then 'RECEIVE = ' || get_function_identity_arguments(v_type_params.typreceive::oid, false, false) || E',\n    ' else '' end
               || case when v_type_params.typsend != '-'::regproc then 'SEND = ' || get_function_identity_arguments(v_type_params.typsend::oid, false, false) || E',\n    ' else '' end
               || case when v_type_params.typmodin != '-'::regproc then 'TYPMOD_IN = ' || get_function_identity_arguments(v_type_params.typmodin::oid, false, false) || E',\n    ' else '' end
               || case when v_type_params.typmodout != '-'::regproc then 'TYPMOD_OUT = ' || get_function_identity_arguments(v_type_params.typmodout::oid, false, false) || E',\n    ' else '' end
               || case when v_type_params.typanalyze != '-'::regproc then 'ANALYZE = ' || get_function_identity_arguments(v_type_params.typanalyze::oid, false, false) || E',\n    ' else '' end
               || case when v_type_params.typsubscript != '-'::regproc then 'SUBSCRIPT = ' || get_function_identity_arguments(v_type_params.typsubscript::oid, false, false) || E',\n    ' else '' end
               || case when v_type_params.typlen != 0 then 'INTERNALLENGTH = ' || v_type_params.typlen || E',\n    ' else '' end
               || case when v_type_params.typbyval = true then 'PASSEDBYVALUE' || E',\n    ' else '' end
               || 'ALIGNMENT = ' || v_type_params.typalign || E',\n    '
               || 'STORAGE = ' || v_type_params.typstorage || E',\n    '
               || 'CATEGORY = ' || v_type_params.typcategory || E',\n    '
               || 'PREFERRED = ' || v_type_params.typispreferred || E',\n    '
               || case when v_type_params.typdefault is not null then 'DEFAULT = ' || v_type_params.typdefault || E',\n    ' else '' end
               || case when v_type_params.element != 'none' then 'ELEMENT = ' || v_type_params.element || E',\n    ' else '' end
               || 'DELIMITER = ''' || v_type_params.typdelim || E''',\n    '
               || case when v_type_params.typcollation != 0 then 'COLLATABLE = true' else 'COLLATABLE = false' end
               || E'\n);';

    end if;

--===========================================================================================
    -- End of building DDL
--===========================================================================================

    return v_ddl;

  end;

$$;

comment on function get_type_def(in_type text, in_schema text)
    is 'Generates CREATE command for a type / domain by its name and schema (default - search_path)';


-- OID

create or replace function get_type_def
  (
    type_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Generates DDL script for all kind of types (CREATE TYPE... for base types, composite types, pseudotypes, ranges, multiranges, enums) and domains (CREATE DOMAIN...)

-- Examples:

-- select get_type_def('integer'::regtype);

-- select get_type_def("oid")
-- from pg_catalog.pg_type
-- limit 10;

  declare

    v_ddl text;
    v_type_params record;
    v_enum_labels text;
    v_range_params record;
    v_domain_collation text;
    v_domain_basetype record;
    v_domain_type text;
    v_domain_constraint_params record;
    v_domain_constraints text;
    v_att_params record;

  begin

    -- Checking existence of type
    select t.typname as in_type,
           n.nspname as in_schema,
           t.typtype, -- type class: normal, composite, enum, range, multirange etc.
           case when t.typlen > 0 then t.typlen else 0 end as typlen,
           t.typbyval,
           t.typcategory::text as typcategory,
           t.typispreferred,
           t.typisdefined, -- false - shell type
           t.typdelim::text as typdelim,
           t.typsubscript as typsubscript,
           coalesce(e.typname::text, 'none') as element,
           t.typinput as typinput,
           t.typoutput as typoutput,
           t.typsend as typsend,
           t.typreceive as typreceive,
           t.typmodin as typmodin,
           t.typmodout as typmodout,
           t.typanalyze as typanalyze,
           case t.typalign when 'c' then 'char' when 's' then 'int2' when 'i' then 'int4' when 'd' then 'double' end as typalign,
           case t.typstorage when 'p' then 'plain' when 'e' then 'external' when 'm' then 'main' when 'x' then 'extended' end as typstorage,
           t.typnotnull, -- domain only
           t.typbasetype, -- base type for domain
           t.typtypmod,
           t.typndims, -- domain-arrays only
           t.typcollation,
           t.typdefault
    into v_type_params
    from pg_catalog.pg_type t
    join pg_catalog.pg_namespace n on n.oid = t.typnamespace
    left join pg_catalog.pg_type e on t.typelem = e."oid"
    where type_oid = t."oid";

    if v_type_params.in_type is null then
      raise warning 'Type with OID % does not exist', type_oid;

      return null;
    end if;

--===========================================================================================
    -- Building DDL
--===========================================================================================

      -- Shell type
    if v_type_params.typisdefined = false then
      v_ddl := 'CREATE TYPE ' || quote_ident(v_type_params.in_schema) || '.' || quote_ident(v_type_params.in_type) || ';';

--=========================================

      -- Enum
    elsif v_type_params.typisdefined = true and v_type_params.typtype = 'e'::char then
      select string_agg('''' || enumlabel || '''', E',\n    ' order by enumsortorder)
      into v_enum_labels
      from pg_catalog.pg_enum
      where enumtypid = type_oid;

      v_ddl := 'CREATE TYPE ' || quote_ident(v_type_params.in_schema) || '.' || quote_ident(v_type_params.in_type) || E' AS ENUM\n(\n    ' || v_enum_labels || E'\n);';

--=========================================

      -- Range (but not multirange)
    elsif v_type_params.typisdefined = true and v_type_params.typtype = 'r'::char then
      select quote_ident(nt.nspname) || '.' || quote_ident(t.typname) as rngsubtype,
             case when r.rngsubopc != 0 then quote_ident(n_o.nspname) || '.' || quote_ident(o.opcname) else null end as rngsubopc,
             case when r.rngcollation != 0 then quote_ident(nc.nspname) || '.' || quote_ident(c.collname) else null end as rngcollation,
             case when r.rngcanonical != '-'::regproc then get_function_identity_arguments(r.rngcanonical::oid, false, false) else '-' end as rngcanonical,
             case when r.rngsubdiff != '-'::regproc then get_function_identity_arguments(r.rngsubdiff::oid, false, false) else '-' end as rngsubdiff,
             case when r.rngmultitypid != 0 then quote_ident(ntm.nspname) || '.' || quote_ident(tm.typname) else null end as multirange_type_name
      into v_range_params
      from pg_catalog.pg_range r
      join pg_catalog.pg_type t on r.rngsubtype = t."oid"
      left join pg_catalog.pg_type tm on r.rngmultitypid = tm."oid"
      left join pg_catalog.pg_opclass o on r.rngsubopc = o."oid"
      left join pg_catalog.pg_collation c on r.rngcollation = c."oid"
      left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
      left join pg_catalog.pg_namespace ntm on ntm."oid" = tm.typnamespace
      left join pg_catalog.pg_namespace n_o on n_o."oid" = o.opcnamespace
      left join pg_catalog.pg_namespace nc on nc."oid" = c.collnamespace
      where r.rngtypid = type_oid;

      v_ddl := 'CREATE TYPE ' || quote_ident(v_type_params.in_schema) || '.' || quote_ident(v_type_params.in_type)
               || E' AS RANGE\n(\n    SUBTYPE = ' || v_range_params.rngsubtype
               || case when v_range_params.rngsubopc is not null then E',\n    SUBTYPE_OPCLASS = ' || v_range_params.rngsubopc || E',\n    ' else '' end
               || case when v_range_params.rngcollation is not null then 'COLLATION = ' || v_range_params.rngcollation || E',\n    ' else '' end
               || case when v_range_params.rngcanonical != '-' then 'CANONICAL = ' || v_range_params.rngcanonical || E',\n    ' else '' end
               || case when v_range_params.rngsubdiff != '-' then 'SUBTYPE_DIFF = ' || v_range_params.rngsubdiff || E',\n    ' else '' end
               || case when v_range_params.multirange_type_name is not null then 'MULTIRANGE_TYPE_NAME = ' || v_range_params.multirange_type_name else '' end
               || E'\n);';

--=========================================

      -- Domain
    elsif v_type_params.typisdefined = true and v_type_params.typtype = 'd'::char then
      -- collation
      if v_type_params.typcollation != 0 then
        select quote_ident(n.nspname) || '.' || quote_ident(c.collname)
        into v_domain_collation
        from pg_catalog.pg_collation c
        join pg_catalog.pg_namespace n on n."oid" = c.collnamespace
        where c."oid" = v_type_params.typcollation;
      end if;

        -- Building base type
      select t."oid",
             quote_ident(n.nspname) || '.'
                || case when v_type_params.typndims > 1 then quote_ident(substring(t.typname, 2, length(t.typname))) else quote_ident(t.typname) end as typname
      into v_domain_basetype
      from pg_catalog.pg_type t
      join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
      where t."oid" = v_type_params.typbasetype;

          -- precision and scale
            -- PG actually does not store info about float precision - it's either float4 or float8
      v_domain_type :=
        v_domain_basetype.typname
        || case when v_type_params.typtypmod != '-1'::integer then '(' ||
              -- bpchar, varchar max length
              case when v_domain_basetype."oid" = any(array['pg_catalog._bpchar'::regtype, 'pg_catalog._varchar'::regtype, 'pg_catalog.bpchar'::regtype, 'pg_catalog."varchar"'::regtype])
                        then (v_type_params.typtypmod - 4)::text
                   when v_domain_basetype."oid" = any(array['pg_catalog."bit"'::regtype, 'pg_catalog._bit'::regtype, 'pg_catalog.varbit'::regtype, 'pg_catalog._varbit'::regtype])
                        then v_type_params.typtypmod::text
              -- numeric precision
                   when v_domain_basetype."oid" = any(array['pg_catalog."numeric"'::regtype, 'pg_catalog._numeric'::regtype])
                        then (((v_type_params.typtypmod - 4) >> 16) & 65535)::text
              -- timestamp, timestamptz, time, timetz precision
                   when v_domain_basetype."oid" = any(array['pg_catalog."time"'::regtype, 'pg_catalog."timestamp"'::regtype, 'pg_catalog.timestamptz'::regtype, 'pg_catalog.timetz'::regtype,
                                                            'pg_catalog._time'::regtype, 'pg_catalog._timestamp'::regtype, 'pg_catalog._timestamptz'::regtype, 'pg_catalog._timetz'::regtype
                                                           ])
                        then
                            case when v_type_params.typtypmod < 0 then 6::text
                            else v_type_params.typtypmod::text end
              -- interval precision
                   when v_domain_basetype."oid" = any(array['pg_catalog."interval"'::regtype, 'pg_catalog._interval'::regtype])
                        then
                            case when v_type_params.typtypmod < 0 or v_type_params.typtypmod & 65535 = 65535 then 6::text
                            else (v_type_params.typtypmod & 65535)::text end
              else '' end
           else '' end
        || case when v_type_params.typtypmod != '-1'::integer then
              -- numeric scale
                  case when v_domain_basetype."oid" = any(array['pg_catalog."numeric"'::regtype, 'pg_catalog._numeric'::regtype])
                            then ', ' || ((v_type_params.typtypmod - 4) & 65535)::text || ')'
                  else ')' end
           else '' end
        -- there's no info about array length limit, so bpchar(3)[4][5] = bpchar(3)[][]
        || case when v_type_params.typndims > 1 then repeat('[]', v_type_params.typndims) else '' end;

      -- constraints
      v_domain_constraints := case when v_type_params.typnotnull = true then 'NULL' else 'NOT NULL' end;

      for v_domain_constraint_params in
        select "oid",
               get_constraint_def(c."oid") as condef
        from pg_catalog.pg_constraint c
        where type_oid = c.contypid
      loop
        v_domain_constraints := v_domain_constraints
                                || case when v_domain_constraint_params."oid" is not null then E'\n    ' || v_domain_constraint_params.condef
                                   else '' end;
      end loop;

      -- Building DDL
      v_ddl := 'CREATE DOMAIN ' || quote_ident(v_type_params.in_schema) || '.' || quote_ident(v_type_params.in_type) || ' AS ' || v_domain_type || E'\n    '
               || case when v_type_params.typcollation != 0 then 'COLLATE ' || v_domain_collation ||  E'\n    ' else '' end
               || case when v_type_params.typdefault is not null then 'DEFAULT ' || v_type_params.typdefault ||  E'\n    ' else '' end
               || v_domain_constraints || ';';

--=========================================

      -- Composite type
    elsif v_type_params.typisdefined = true and v_type_params.typtype = 'c'::char then
      v_ddl := 'CREATE TYPE ' || quote_ident(v_type_params.in_schema) || '.' || quote_ident(v_type_params.in_type) || E'\n(\n    ';

      for v_att_params in
        select quote_ident(a.attname) as attname,
               quote_ident(n.nspname) || '.'
                  || case when a.attndims > 1 then quote_ident(substring(tc.typname, 2, length(tc.typname))) else quote_ident(tc.typname) end as typname,
               case when a.attndims > 1 then repeat('[]', a.attndims) else '' end as arr_dim,
               case when a.atttypmod != '-1'::integer then '(' ||
                    -- bpchar, varchar max length
                    case when tc."oid" = any(array['pg_catalog._bpchar'::regtype, 'pg_catalog._varchar'::regtype, 'pg_catalog.bpchar'::regtype, 'pg_catalog."varchar"'::regtype])
                              then (a.atttypmod - 4)::text
                         when tc."oid" = any(array['pg_catalog."bit"'::regtype, 'pg_catalog._bit'::regtype, 'pg_catalog.varbit'::regtype, 'pg_catalog._varbit'::regtype])
                              then a.atttypmod::text
                    -- numeric precision
                         when tc."oid" = any(array['pg_catalog."numeric"'::regtype, 'pg_catalog._numeric'::regtype])
                              then (((a.atttypmod - 4) >> 16) & 65535)::text
                    -- timestamp, timestamptz, time, timetz precision
                         when tc."oid" = any(array[
                                                   'pg_catalog."time"'::regtype, 'pg_catalog."timestamp"'::regtype, 'pg_catalog.timestamptz'::regtype, 'pg_catalog.timetz'::regtype,
                                                   'pg_catalog._time'::regtype, 'pg_catalog._timestamp'::regtype, 'pg_catalog._timestamptz'::regtype, 'pg_catalog._timetz'::regtype
                                                  ])
                              then
                                  case when a.atttypmod < 0 then 6::text
                                  else a.atttypmod::text end
                    -- interval precision
                         when tc."oid" = any(array['pg_catalog."interval"'::regtype, 'pg_catalog._interval'::regtype])
                              then
                                  case when a.atttypmod < 0 or a.atttypmod & 65535 = 65535 then 6::text
                                  else (a.atttypmod & 65535)::text end
                    else '' end
               else '' end
               || case when a.atttypmod != '-1'::integer then
                    -- numeric scale
                       case when tc."oid" = any(array['pg_catalog."numeric"'::regtype, 'pg_catalog._numeric'::regtype])
                            then ', ' || ((a.atttypmod - 4) & 65535)::text || ')'
                       else ')' end
                  else '' end
               as atttypmod
        from pg_catalog.pg_attribute a
        join pg_catalog.pg_type t on t.typrelid = a.attrelid
        join pg_catalog.pg_type tc on tc."oid" = a.atttypid
        join pg_catalog.pg_namespace n on n."oid" = tc.typnamespace
        where t."oid" = type_oid
              and a.attnum > 0
        order by a.attnum
      loop
        v_ddl := v_ddl || v_att_params.attname || ' ' || v_att_params.typname || v_att_params.atttypmod || v_att_params.arr_dim || E',\n    ';
      end loop;

      v_ddl := rtrim(v_ddl, E',\n ') || E'\n);';

--=========================================

      -- Basetype, pseudotype, multirange
    elsif v_type_params.typisdefined = true and v_type_params.typtype in ('b'::char, 'p'::char, 'm'::char)  then
      v_ddl := 'CREATE TYPE ' || quote_ident(v_type_params.in_schema) || '.' || quote_ident(v_type_params.in_type)
               || E'\n(\n    INPUT = ' || get_function_identity_arguments(v_type_params.typinput::oid, false, false) || E',\n    '
               || 'OUTPUT = ' || get_function_identity_arguments(v_type_params.typoutput::oid, false, false) || E',\n    '
               || case when v_type_params.typreceive != '-'::regproc then 'RECEIVE = ' || get_function_identity_arguments(v_type_params.typreceive::oid, false, false) || E',\n    ' else '' end
               || case when v_type_params.typsend != '-'::regproc then 'SEND = ' || get_function_identity_arguments(v_type_params.typsend::oid, false, false) || E',\n    ' else '' end
               || case when v_type_params.typmodin != '-'::regproc then 'TYPMOD_IN = ' || get_function_identity_arguments(v_type_params.typmodin::oid, false, false) || E',\n    ' else '' end
               || case when v_type_params.typmodout != '-'::regproc then 'TYPMOD_OUT = ' || get_function_identity_arguments(v_type_params.typmodout::oid, false, false) || E',\n    ' else '' end
               || case when v_type_params.typanalyze != '-'::regproc then 'ANALYZE = ' || get_function_identity_arguments(v_type_params.typanalyze::oid, false, false) || E',\n    ' else '' end
               || case when v_type_params.typsubscript != '-'::regproc then 'SUBSCRIPT = ' || get_function_identity_arguments(v_type_params.typsubscript::oid, false, false) || E',\n    ' else '' end
               || case when v_type_params.typlen != 0 then 'INTERNALLENGTH = ' || v_type_params.typlen || E',\n    ' else '' end
               || case when v_type_params.typbyval = true then 'PASSEDBYVALUE' || E',\n    ' else '' end
               || 'ALIGNMENT = ' || v_type_params.typalign || E',\n    '
               || 'STORAGE = ' || v_type_params.typstorage || E',\n    '
               || 'CATEGORY = ' || v_type_params.typcategory || E',\n    '
               || 'PREFERRED = ' || v_type_params.typispreferred || E',\n    '
               || case when v_type_params.typdefault is not null then 'DEFAULT = ' || v_type_params.typdefault || E',\n    ' else '' end
               || case when v_type_params.element != 'none' then 'ELEMENT = ' || v_type_params.element || E',\n    ' else '' end
               || 'DELIMITER = ''' || v_type_params.typdelim || E''',\n    '
               || case when v_type_params.typcollation != 0 then 'COLLATABLE = true' else 'COLLATABLE = false' end
               || E'\n);';

    end if;

--===========================================================================================
    -- End of building DDL
--===========================================================================================

    return v_ddl;

  end;

$$;

comment on function get_type_def(type_oid oid)
    is 'Generates CREATE command for a type / domain by its OID';