-- Examples:

-- select get_operator_family_def(o.opfname, a.amname, n.nspname),
--        get_operator_family_def(o."oid")
-- from pg_catalog.pg_opfamily o
-- join pg_catalog.pg_am a on o.opfmethod = a."oid"
-- join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace;

-- select get_operator_family_def('text_ops')
-- union all
-- select get_operator_family_def('text_ops', 'hash');


-- Name + schema + access method

create or replace function get_operator_family_def
  (
    in_opf text,
    in_method text default 'btree',
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- ALTER command lists all operators and functions of the family even if they were originally attached via CREATE OPERATOR CLASS command
  -- So there is redundancy between ALTER OPERATOR FAMILY and CREATE OPERATOR CLASS commands

-- Example:

-- select get_operator_family_def('text_ops')
-- union all
-- select get_operator_family_def('text_ops', 'hash');

  declare

    v_opf_ddl text;
    v_opf_params record;
    v_class_ops oid[];
    v_ops_params text;
    v_class_funcs oid[];
    v_func_params text;

  begin

    -- Checking existence of operator family
    select o."oid",
           n.nspname
    into v_opf_params
    from pg_catalog.pg_opfamily o
    join pg_catalog.pg_am a on a."oid" = o.opfmethod
    left join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where o.opfname = in_opf
          and a.amname = in_method
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_opf_params."oid" is null then
      raise warning 'Operator family %.% with access method % does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_opf),
        quote_ident(in_method);

      return null;
    end if;

    -- Getting operators and functions attached to operator family (without regards to operator classes)

      -- Operators of family
    with nsp as
    (
      select "oid",
             nspname
      from pg_catalog.pg_namespace
    ),

    tp as
    (
      select t."oid",
             quote_ident(n.nspname) || '.' || quote_ident(t.typname) as typname
      from pg_catalog.pg_type t
      join nsp n on t.typnamespace = n."oid"
    ),

    op as
    (
      select o."oid",
             quote_ident(n.nspname) || '.' || o.oprname as oprname, -- do not use quote_ident on oprname here, it'll make script invalid
             o.oprkind,
             o.oprleft,
             o.oprright
      from pg_catalog.pg_operator o
      join nsp n on o.oprnamespace = n."oid"
    )

    select coalesce(string_agg(str, ',' order by amopstrategy), '')
    into v_ops_params
    from
    (
      select distinct
              a.amopstrategy,
              E'\n    OPERATOR ' || a.amopstrategy || ' ' || o.oprname || ' ('
                || case o.oprkind when 'b' then ts.typname || ', ' || tt.typname
                                  when 'r' then ts.typname || ', NONE'
                                  when 'l' then 'NONE, ' || tt.typname
                   end
                || ') '
                || case a.amoppurpose when 's' then 'FOR SEARCH'
                                      when 'o' then 'FOR ORDER BY ' || quote_ident(nf.nspname) || '.' || quote_ident(sf.opfname)
                   end as str
      from pg_catalog.pg_amop a
      join op o on a.amopopr = o."oid"
      left join tp ts on o.oprleft = ts."oid"
      left join tp tt on o.oprright = tt."oid"
      left join pg_catalog.pg_opfamily sf on a.amopsortfamily = sf."oid"
      left join nsp nf on sf.opfnamespace = nf."oid"
      where a.amopfamily = v_opf_params."oid"
    ) a;


      -- Functions of family
    with nsp as
    (
      select "oid",
             nspname
      from pg_catalog.pg_namespace
    ),

    tp as
    (
      select t."oid",
             quote_ident(n.nspname) || '.' || quote_ident(t.typname) as typname
      from pg_catalog.pg_type t
      join nsp n on t.typnamespace = n."oid"
    )

    select coalesce(string_agg(str, ',' order by amprocnum), '')
    into v_func_params
    from
    (
      select distinct
              p.amprocnum,
              E'\n    FUNCTION ' || p.amprocnum || ' (' || ts.typname || ', ' || tt.typname || ') ' || get_function_identity_arguments(p.amproc, false, false) as str
      from pg_catalog.pg_amproc p
      left join tp ts on p.amproclefttype = ts."oid"
      left join tp tt on p.amprocrighttype = tt."oid"
      where p.amprocfamily = v_opf_params."oid"
    ) a;


    -- Building DDL
    v_opf_ddl := 'CREATE OPERATOR FAMILY ' || quote_ident(v_opf_params.nspname) || '.' || quote_ident(in_opf)
                 || E'\nUSING ' || quote_ident(in_method) || ';';

    if v_ops_params is not null and v_ops_params != '' and v_func_params is not null and v_func_params != '' then
      v_opf_ddl := v_opf_ddl || E'\n\nALTER OPERATOR FAMILY ' || quote_ident(v_opf_params.nspname) || '.' || quote_ident(in_opf)
                   || E'\nUSING ' || quote_ident(in_method)
                   || E'\nADD'
                   || v_ops_params
                   || case when v_func_params != '' then ',' else '' end || v_func_params
                   || ';';
    end if;

    return v_opf_ddl;

  end;

$$;

comment on function get_operator_family_def(in_opf text, in_method text, in_schema text)
    is 'Generates CREATE command for an operator family by its name and schema (default - search_path) + used access method (def ''btree'')';


-- OID

create or replace function get_operator_family_def
  (
    opf_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- ALTER command lists all operators and functions of the family even if they were originally attached via CREATE OPERATOR CLASS command
  -- So there is redundancy between ALTER OPERATOR FAMILY and CREATE OPERATOR CLASS commands

-- Example:

-- select get_operator_family_def("oid")
-- from pg_catalog.pg_opfamily
-- limit 10;

  declare

    v_opf_ddl text;
    v_opf_params record;
    v_class_ops oid[];
    v_ops_params text;
    v_class_funcs oid[];
    v_func_params text;

  begin

    -- Checking existence of operator family
    select o.opfname,
           n.nspname,
           a.amname
    into v_opf_params
    from pg_catalog.pg_opfamily o
    join pg_catalog.pg_am a on a."oid" = o.opfmethod
    join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace
    where o."oid" = opf_oid;

    if v_opf_params.opfname is null then
      raise warning 'Operator family with OID % does not exist', opf_oid;

      return null;
    end if;

    -- Getting operators and functions attached to operator family (without regards to operator classes)

      -- Operators of family
    with nsp as
    (
      select "oid",
             nspname
      from pg_catalog.pg_namespace
    ),

    tp as
    (
      select t."oid",
             quote_ident(n.nspname) || '.' || quote_ident(t.typname) as typname
      from pg_catalog.pg_type t
      join nsp n on t.typnamespace = n."oid"
    ),

    op as
    (
      select o."oid",
             quote_ident(n.nspname) || '.' || o.oprname as oprname, -- do not use quote_ident on oprname here, it'll make script invalid
             o.oprkind,
             o.oprleft,
             o.oprright
      from pg_catalog.pg_operator o
      join nsp n on o.oprnamespace = n."oid"
    )

    select coalesce(string_agg(str, ',' order by amopstrategy), '')
    into v_ops_params
    from
    (
      select distinct
              a.amopstrategy,
              E'\n    OPERATOR ' || a.amopstrategy || ' ' || o.oprname || ' ('
                || case o.oprkind when 'b' then ts.typname || ', ' || tt.typname
                                  when 'r' then ts.typname || ', NONE'
                                  when 'l' then 'NONE, ' || tt.typname
                   end
                || ') '
                || case a.amoppurpose when 's' then 'FOR SEARCH'
                                      when 'o' then 'FOR ORDER BY ' || quote_ident(nf.nspname) || '.' || quote_ident(sf.opfname)
                   end as str
      from pg_catalog.pg_amop a
      join op o on a.amopopr = o."oid"
      left join tp ts on o.oprleft = ts."oid"
      left join tp tt on o.oprright = tt."oid"
      left join pg_catalog.pg_opfamily sf on a.amopsortfamily = sf."oid"
      left join nsp nf on sf.opfnamespace = nf."oid"
      where a.amopfamily = opf_oid
    ) a;


      -- Functions of family
    with nsp as
    (
      select "oid",
             nspname
      from pg_catalog.pg_namespace
    ),

    tp as
    (
      select t."oid",
             quote_ident(n.nspname) || '.' || quote_ident(t.typname) as typname
      from pg_catalog.pg_type t
      join nsp n on t.typnamespace = n."oid"
    )

    select coalesce(string_agg(str, ',' order by amprocnum), '')
    into v_func_params
    from
    (
      select distinct
              p.amprocnum,
              E'\n    FUNCTION ' || p.amprocnum || ' (' || ts.typname || ', ' || tt.typname || ') ' || get_function_identity_arguments(p.amproc, false, false) as str
      from pg_catalog.pg_amproc p
      left join tp ts on p.amproclefttype = ts."oid"
      left join tp tt on p.amprocrighttype = tt."oid"
      where p.amprocfamily = opf_oid
    ) a;


    -- Building DDL
    v_opf_ddl := 'CREATE OPERATOR FAMILY ' || quote_ident(v_opf_params.nspname) || '.' || quote_ident(v_opf_params.opfname)
                 || E'\nUSING ' || quote_ident(v_opf_params.amname) || ';';

    if v_ops_params is not null and v_ops_params != '' and v_func_params is not null and v_func_params != '' then
      v_opf_ddl := v_opf_ddl || E'\n\nALTER OPERATOR FAMILY ' || quote_ident(v_opf_params.nspname) || '.' || quote_ident(v_opf_params.opfname)
                   || E'\nUSING ' || quote_ident(v_opf_params.amname)
                   || E'\nADD'
                   || v_ops_params
                   || case when v_func_params != '' then ',' else '' end || v_func_params
                   || ';';
    end if;

    return v_opf_ddl;

  end;

$$;

comment on function get_operator_family_def(opf_oid oid)
    is 'Generates CREATE command for an operator family by its OID';