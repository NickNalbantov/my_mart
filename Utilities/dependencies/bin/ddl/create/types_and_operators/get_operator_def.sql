-- Examples:

-- select get_operator_def(o.oprname, tl.typname, tr.typname, nop.nspname, ntl.nspname, ntr.nspname),
--           get_operator_def(o."oid")
-- from pg_catalog.pg_operator o
-- join pg_catalog.pg_namespace nop on o.oprnamespace = nop."oid"
-- left join pg_catalog.pg_type tl on o.oprleft = tl."oid"
-- left join pg_catalog.pg_namespace ntl on tl.typnamespace = ntl."oid"
-- left join pg_catalog.pg_type tr on o.oprright = tr."oid"
-- left join pg_catalog.pg_namespace ntr on tr.typnamespace = ntr."oid";

-- select get_operator_def('>=', 'int', 'bigint');


-- Name + schema

create or replace function get_operator_def
  (
    in_oper text,
    left_type text default null,
    right_type text default null,
    op_schema text default null,
    left_type_schema text default null,
    right_type_schema text default null
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_operator_def('>=', 'int', 'bigint');

  declare

    v_oper_ddl text;
    v_oper_params record;
    v_typeleft text;
    v_typeright text;
    v_commutator text;
    v_negator text;
    v_func text;
    v_restrict text;
    v_join text;
    in_types_fixed text[];
    source_params record;
    target_params record;

    int_syn text[] := array['integer', 'int'];
    smallint_syn text := 'smallint';
    bigint_syn text := 'bigint';
    num_syn text := 'decimal';
    real_syn text := 'real';
    dp_syn text[] := array['double precision', 'float'];
    time_syn text := 'time without time zone';
    timetz_syn text := 'time with time zone';
    timestamp_syn text := 'timestamp without time zone';
    timestamptz_syn text := 'timestamp with time zone';
    bpchar_syn text := 'character';
    varchar_syn text[] := array['character varying', 'char varying'];
    varbit_syn text := 'bit varying';
    bool_syn text := 'boolean';

  begin

    -- Catching synonyms for types
    select array_agg(case when right(tps, 2) = '[]' then rtrim('_' || tps, '[]')
                          else tps end
                     order by rn
                    )
    into in_types_fixed
    from
    (
      select rn,
        case when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = any(int_syn) then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1int4\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = smallint_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1int2\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = bigint_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1int8\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = num_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1numeric\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = real_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1float4\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = any(dp_syn) then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1float8\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = time_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1time\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = timetz_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1timetz\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = timestamp_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1timestamp\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = timestamptz_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1timestamptz\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = bpchar_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1bpchar\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = any(varchar_syn) then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1varchar\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = varbit_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1varbit\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = bool_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1bool\2')
        else tps end as tps
      from
        (
          select left_type as tps, 1 as rn
          union all
          select right_type as tps, 2 as rn
        ) a
    ) b;

    -- Getting types parameters
    select case when in_types_fixed[1] is null then 0::oid else t."oid" end as "oid",
           n.nspname,
           t.typname
    into source_params
    from pg_catalog.pg_type t
    left join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where (in_types_fixed[1] = t.typname or in_types_fixed[1] is null)
          and coalesce(left_type_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    select case when in_types_fixed[2] is null then 0::oid else t."oid" end as "oid",
           n.nspname,
           t.typname
    into target_params
    from pg_catalog.pg_type t
    left join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where (in_types_fixed[2] = t.typname or in_types_fixed[2] is null)
          and coalesce(right_type_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    -- Checking existence of operator
    select o."oid",
           o.oprkind,
           o.oprcanmerge,
           o.oprcanhash,
           o.oprleft,
           o.oprright,
           o.oprcom,
           o.oprnegate,
           o.oprcode,
           o.oprrest,
           o.oprjoin,
           n.nspname
    into v_oper_params
    from pg_catalog.pg_operator o
    left join pg_catalog.pg_namespace n on n."oid" = o.oprnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where o.oprname = in_oper
          and source_params."oid" = o.oprleft
          and target_params."oid" = o.oprright
          and coalesce(op_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_oper_params."oid" is null then
      raise warning 'Operator %.% for type % does not exist',
        coalesce(quote_ident(op_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_oper),
        case when left_type is not null
          then coalesce(quote_ident(left_type_schema), '[' || array_to_string(current_schemas(true), ', ') || ']') || '.' || quote_ident(left_type) else '' end ||
        case when left_type is not null and right_type is not null
          then ' and type ' else '' end ||
        case when right_type is not null
          then coalesce(quote_ident(right_type_schema), '[' || array_to_string(current_schemas(true), ', ') || ']') || '.' || quote_ident(right_type) else '' end;

      return null;
    end if;

    -- Building DDL

    -- Commutator and negator
    if v_oper_params.oprcom != 0 then
      select E',\n    COMMUTATOR = OPERATOR(' || quote_ident(n.nspname) || '.' || o.oprname || ')'
      into v_commutator
      from pg_catalog.pg_operator o
      join pg_catalog.pg_namespace n on n."oid" = o.oprnamespace
      where o."oid" = v_oper_params.oprcom;
    else
      v_commutator := '';
    end if;

    if v_oper_params.oprnegate != 0 then
      select E',\n    NEGATOR = OPERATOR(' || quote_ident(n.nspname) || '.' || o.oprname || ')'
      into v_negator
      from pg_catalog.pg_operator o
      join pg_catalog.pg_namespace n on n."oid" = o.oprnamespace
      where o."oid" = v_oper_params.oprnegate;
    else
      v_negator := '';
    end if;

    -- Types
    if v_oper_params.oprkind in ('b', 'r') then
      select E',\n    LEFTARG = ' || quote_ident(n.nspname) || '.' || quote_ident(t.typname)
      into v_typeleft
      from pg_catalog.pg_type t
      join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
      where t."oid" = v_oper_params.oprleft;
    else
      v_typeleft = '';
    end if;

    if v_oper_params.oprkind in ('b', 'l') then
      select E',\n    RIGHTARG = ' || quote_ident(n.nspname) || '.' || quote_ident(t.typname)
      into v_typeright
      from pg_catalog.pg_type t
      join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
      where t."oid" = v_oper_params.oprright;
    else
      v_typeright = '';
    end if;

    -- Functions
    if v_oper_params.oprcode != '-'::regproc then
      select E'\n    FUNCTION = ' || get_function_identity_arguments(v_oper_params.oprcode, false, false)
      into v_func;
    else
      v_func := E'\n    FUNCTION = -';
    end if;

    if v_oper_params.oprrest != '-'::regproc then
      select E',\n    RESTRICT = ' || get_function_identity_arguments(v_oper_params.oprrest, false, false)
      into v_restrict;
    else
      v_restrict := '';
    end if;

    if v_oper_params.oprjoin != '-'::regproc then
      select E',\n    JOIN = ' || get_function_identity_arguments(v_oper_params.oprjoin, false, false)
      into v_join;
    else
      v_join := '';
    end if;

      -- Full DDL
    v_oper_ddl := 'CREATE OPERATOR ' || quote_ident(v_oper_params.nspname) || '.' || in_oper || E'\n(' -- do not use quote_ident on oprname here, it'll make script invalid
                  || v_func
                  || v_typeleft
                  || v_typeright
                  || v_commutator
                  || v_negator
                  || v_restrict
                  || v_join
                  || case when v_oper_params.oprcanhash = true then E'\n    HASHES' else '' end
                  || case when v_oper_params.oprcanmerge = true then E'\n    MERGES' else '' end
                  || E'\n);';

    return v_oper_ddl;

  end;

$$;

comment on function get_operator_def(in_oper text, left_type text, right_type text, op_schema text, left_type_schema text, right_type_schema text)
    is 'Generates CREATE command for an operator by its name, schema (default - search_path) and names and schemas (default - search_path) of types of left and right arguments (null by default)';


-- OID

create or replace function get_operator_def
  (
    oper_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_operator_def("oid")
-- from pg_catalog.pg_operator
-- limit 10;

  declare

    v_oper_ddl text;
    v_oper_params record;
    v_typeleft text;
    v_typeright text;
    v_commutator text;
    v_negator text;
    v_func text;
    v_restrict text;
    v_join text;

  begin

    -- Checking existence of operator
    select n.nspname,
           o.oprname,
           o.oprkind,
           o.oprcanmerge,
           o.oprcanhash,
           o.oprleft,
           o.oprright,
           o.oprcom,
           o.oprnegate,
           o.oprcode,
           o.oprrest,
           o.oprjoin
    into v_oper_params
    from pg_catalog.pg_operator o
    join pg_catalog.pg_namespace n on n."oid" = o.oprnamespace
    where o."oid" = oper_oid;

    if v_oper_params.oprname is null then
      raise warning 'Operator with OID % does not exist', oper_oid;

      return null;
    end if;

    -- Building DDL

    -- Commutator and negator
    if v_oper_params.oprcom != 0 then
      select E',\n    COMMUTATOR = OPERATOR(' || quote_ident(n.nspname) || '.' || o.oprname || ')'
      into v_commutator
      from pg_catalog.pg_operator o
      join pg_catalog.pg_namespace n on n."oid" = o.oprnamespace
      where o."oid" = v_oper_params.oprcom;
    else
      v_commutator := '';
    end if;

    if v_oper_params.oprnegate != 0 then
      select E',\n    NEGATOR = OPERATOR(' || quote_ident(n.nspname) || '.' || o.oprname || ')'
      into v_negator
      from pg_catalog.pg_operator o
      join pg_catalog.pg_namespace n on n."oid" = o.oprnamespace
      where o."oid" = v_oper_params.oprnegate;
    else
      v_negator := '';
    end if;

    -- Types
    if v_oper_params.oprkind in ('b', 'r') then
      select E',\n    LEFTARG = ' || quote_ident(n.nspname) || '.' || quote_ident(t.typname)
      into v_typeleft
      from pg_catalog.pg_type t
      join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
      where t."oid" = v_oper_params.oprleft;
    else
      v_typeleft = '';
    end if;

    if v_oper_params.oprkind in ('b', 'l') then
      select E',\n    RIGHTARG = ' || quote_ident(n.nspname) || '.' || quote_ident(t.typname)
      into v_typeright
      from pg_catalog.pg_type t
      join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
      where t."oid" = v_oper_params.oprright;
    else
      v_typeright = '';
    end if;

    -- Functions
    if v_oper_params.oprcode != '-'::regproc then
      select E'\n    FUNCTION = ' || get_function_identity_arguments(v_oper_params.oprcode, false, false)
      into v_func;
    else
      v_func := E'\n    FUNCTION = -';
    end if;

    if v_oper_params.oprrest != '-'::regproc then
      select E',\n    RESTRICT = ' || get_function_identity_arguments(v_oper_params.oprrest, false, false)
      into v_restrict;
    else
      v_restrict := '';
    end if;

    if v_oper_params.oprjoin != '-'::regproc then
      select E',\n    JOIN = ' || get_function_identity_arguments(v_oper_params.oprjoin, false, false)
      into v_join;
    else
      v_join := '';
    end if;

    -- Full DDL
    v_oper_ddl := 'CREATE OPERATOR ' || quote_ident(v_oper_params.nspname) || '.' || v_oper_params.oprname || E'\n(' -- do not use quote_ident on oprname here, it'll make script invalid
                  || v_func
                  || v_typeleft
                  || v_typeright
                  || v_commutator
                  || v_negator
                  || v_restrict
                  || v_join
                  || case when v_oper_params.oprcanhash = true then E',\n    HASHES' else '' end
                  || case when v_oper_params.oprcanmerge = true then E',\n    MERGES' else '' end
                  || E'\n);';

    return v_oper_ddl;

  end;

$$;

comment on function get_operator_def(oper_oid oid)
    is 'Generates CREATE command for an operator by its OID';