-- Examples:

-- select get_access_method_def(amname),
--        get_access_method_def("oid")
-- from pg_catalog.pg_am;


-- Name

create or replace function get_access_method_def
  (
    in_am text
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_access_method_def(amname)
-- from pg_catalog.pg_am;

  declare

    v_am_ddl text;
    v_am_params record;
    v_handler text;

  begin

    -- Checking existence of access method
    select "oid",
           amhandler,
           case amtype when 't' then 'TABLE' when 'i' then 'INDEX' end as amtype
    into v_am_params
    from pg_catalog.pg_am
    where amname = in_am;

    if v_am_params."oid" is null then
      raise warning 'Access method % does not exist', in_am;

      return null;
    end if;

    -- Function
    select get_function_identity_arguments(v_am_params.amhandler, false, false)
    into v_handler;

    -- Building DDL

    v_am_ddl := 'CREATE ACCESS METHOD ' || quote_ident(in_am) || E'\n    TYPE ' || v_am_params.amtype || E'\n    HANDLER ' || v_handler || ';';

    return v_am_ddl;

  end;

$$;

comment on function get_access_method_def(in_am text)
    is 'Generates CREATE command for an access method by its name';


-- OID

create or replace function get_access_method_def
  (
    am_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_access_method_def("oid")
-- from pg_catalog.pg_am;

  declare

    v_am_ddl text;
    v_am_params record;
    v_handler text;

  begin

    -- Checking existence of access method
    select "oid",
           amname,
           amhandler,
           case amtype when 't' then 'TABLE' when 'i' then 'INDEX' end as amtype
    into v_am_params
    from pg_catalog.pg_am
    where "oid" = am_oid;

    if v_am_params.amname is null then
      raise warning 'Access method with OID % does not exist', am_oid;

      return null;
    end if;

    -- Function
    select get_function_identity_arguments(v_am_params.amhandler, false, false)
    into v_handler;

    -- Building DDL

    v_am_ddl := 'CREATE ACCESS METHOD ' || quote_ident(v_am_params.amname) || E'\n    TYPE ' || v_am_params.amtype || E'\n    HANDLER ' || v_handler || ';';

    return v_am_ddl;

  end;

$$;

comment on function get_access_method_def(am_oid oid)
    is 'Generates CREATE command for an access method by its OID';