-- Examples:

-- select get_cast_def(ts.typname, tt.typname, ns.nspname, nt.nspname),
--        get_cast_def(c."oid"),
--        get_cast_def(c.castsource, c.casttarget)
-- from pg_catalog.pg_cast c
-- join pg_catalog.pg_type ts on c.castsource = ts."oid"
-- join pg_catalog.pg_type tt on c.casttarget = tt."oid"
-- join pg_catalog.pg_namespace ns on ns."oid" = ts.typnamespace
-- join pg_catalog.pg_namespace nt on nt."oid" = tt.typnamespace;

-- select get_cast_def('int', 'bigint')
-- union all
-- select get_cast_def('text[]', 'hstore');


-- Source name, target name, source schema, target schema

create or replace function get_cast_def
  (
    source_type text,
    target_type text,
    source_schema text default null,
    target_schema text default null
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_cast_def('int', 'bigint')
-- union all
-- select get_cast_def('text[]', 'hstore');

  declare

    v_cast_ddl text;
    v_cast_func text;
    v_cast_params record;
    in_types_fixed text[];
    source_params record;
    target_params record;

    int_syn text[] := array['integer', 'int'];
    smallint_syn text := 'smallint';
    bigint_syn text := 'bigint';
    num_syn text := 'decimal';
    real_syn text := 'real';
    dp_syn text[] := array['double precision', 'float'];
    time_syn text := 'time without time zone';
    timetz_syn text := 'time with time zone';
    timestamp_syn text := 'timestamp without time zone';
    timestamptz_syn text := 'timestamp with time zone';
    bpchar_syn text := 'character';
    varchar_syn text[] := array['character varying', 'char varying'];
    varbit_syn text := 'bit varying';
    bool_syn text := 'boolean';

  begin

    -- Catching synonyms for types
    select array_agg(case when right(tps, 2) = '[]' then rtrim('_' || tps, '[]')
                          else tps end
                     order by rn
                    )
    into in_types_fixed
    from
    (
      select rn,
        case when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = any(int_syn) then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1int4\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = smallint_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1int2\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = bigint_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1int8\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = num_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1numeric\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = real_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1float4\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = any(dp_syn) then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1float8\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = time_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1time\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = timetz_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1timetz\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = timestamp_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1timestamp\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = timestamptz_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1timestamptz\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = bpchar_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1bpchar\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = any(varchar_syn) then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1varchar\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = varbit_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1varbit\2')
             when lower(regexp_replace(rtrim(tps, '[]'), '^_', '')) = bool_syn then regexp_replace(tps, '(^|^_).+[^(\[\]|$)]', '\1bool\2')
        else tps end as tps
      from
        (
          select source_type as tps, 1 as rn
          union all
          select target_type as tps, 2 as rn
        ) a
    ) b;

    -- Getting types parameters
    select t."oid",
           n.nspname
    into source_params
    from pg_catalog.pg_type t
    left join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where in_types_fixed[1] = t.typname
          and coalesce(source_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    select t."oid",
           n.nspname
    into target_params
    from pg_catalog.pg_type t
    left join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where in_types_fixed[2] = t.typname
          and coalesce(target_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    -- Checking existence of cast
    select c."oid",
           c.castfunc,
           c.castmethod,
           c.castcontext,
           ts.typname as source_type,
           tt.typname as target_type
    into v_cast_params
    from pg_catalog.pg_cast c
    join pg_catalog.pg_type ts on c.castsource = ts."oid"
    join pg_catalog.pg_type tt on c.casttarget = tt."oid"
    where source_params."oid" = ts."oid"
          and target_params."oid" = tt."oid";

    if v_cast_params."oid" is null then
      raise warning 'Cast from %.% to %.% does not exist',
        coalesce(quote_ident(source_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(source_type),
        coalesce(quote_ident(target_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(target_type);

      return null;
    end if;

    -- Cast function
    if v_cast_params.castmethod = 'f' then
      select get_function_identity_arguments(v_cast_params.castfunc, false, false)
      into v_cast_func;
    end if;

    -- Building DDL

    v_cast_ddl := 'CREATE CAST (' || quote_ident(source_params.nspname) || '.' || quote_ident(v_cast_params.source_type) || ' AS '
                  || quote_ident(target_params.nspname) || '.' || quote_ident(v_cast_params.target_type) || ')' || E'\n    '
                  || case when v_cast_params.castmethod = 'f' then 'WITH FUNCTION ' || v_cast_func
                          when v_cast_params.castmethod = 'i' then 'WITH INOUT'
                     else 'WITHOUT FUNCTION' end
                  || case when v_cast_params.castcontext = 'a' then E'\n    AS ASSIGNMENT'
                          when v_cast_params.castcontext = 'i' then E'\n    AS IMPLICIT'
                     else '' end
                  || ';';

    return v_cast_ddl;

  end;

$$;

comment on function get_cast_def(source_type text, target_type text, source_schema text, target_schema text)
    is 'Generates CREATE command for a cast by names and schemas of source and target types (default - search_path)';


-- Cast OID

create or replace function get_cast_def
  (
    cast_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_cast_def("oid")
-- from pg_catalog.pg_cast
-- limit 10;

  declare

    v_cast_ddl text;
    v_cast_func text;
    source_params record;
    target_params record;
    v_cast_params record;

  begin

    -- Checking existence of cast
    select "oid",
           castfunc,
           castsource,
           casttarget,
           castmethod,
           castcontext
    into v_cast_params
    from pg_catalog.pg_cast
    where "oid" = cast_oid;

    if v_cast_params."oid" is null then
      raise warning 'Cast with OID % does not exist', cast_oid;

      return null;
    end if;

    -- Cast function
    if v_cast_params.castmethod = 'f' then
      select get_function_identity_arguments(v_cast_params.castfunc, false, false)
      into v_cast_func;
    end if;

    -- Source and target types
    select n.nspname,
           t.typname
    into source_params
    from pg_catalog.pg_type t
    join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    where t."oid" = v_cast_params.castsource;

    select n.nspname,
           t.typname
    into target_params
    from pg_catalog.pg_type t
    join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    where t."oid" = v_cast_params.casttarget;

    -- Building DDL

    v_cast_ddl := 'CREATE CAST (' || quote_ident(source_params.nspname) || '.' || quote_ident(source_params.typname) || ' AS '
                  || quote_ident(target_params.nspname) || '.' || quote_ident(target_params.typname) || ')' || E'\n    '
                  || case when v_cast_params.castmethod = 'f' then 'WITH FUNCTION ' || v_cast_func
                          when v_cast_params.castmethod = 'i' then 'WITH INOUT'
                     else 'WITHOUT FUNCTION' end
                  || case when v_cast_params.castcontext = 'a' then E'\n    AS ASSIGNMENT'
                          when v_cast_params.castcontext = 'i' then E'\n    AS IMPLICIT'
                     else '' end
                  || ';';

    return v_cast_ddl;

  end;

$$;

comment on function get_cast_def(cast_oid oid)
    is 'Generates CREATE command for a cast by its OID';


-- Type OIDs

create or replace function get_cast_def
  (
    source_type_oid oid,
    target_type_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_cast_def('int'::regtype, 'bigint'::regtype)
-- union all
-- select get_cast_def('text[]'::regtype, 'hstore'::regtype);

  declare

    v_cast_ddl text;
    v_cast_func text;
    v_cast_params record;

  begin

    -- Checking existence of cast
    select c."oid",
           c.castfunc,
           c.castmethod,
           c.castcontext,
           ns.nspname as source_schema,
           ts.typname as source_type,
           nt.nspname as target_schema,
           tt.typname as target_type
    into v_cast_params
    from pg_catalog.pg_cast c
    join pg_catalog.pg_type ts on c.castsource = ts."oid"
    join pg_catalog.pg_type tt on c.casttarget = tt."oid"
    join pg_catalog.pg_namespace ns on ns."oid" = ts.typnamespace
    join pg_catalog.pg_namespace nt on nt."oid" = tt.typnamespace
    where source_type_oid = c.castsource
          and target_type_oid = c.casttarget;

    if v_cast_params."oid" is null then
      raise warning 'Cast between types with OIDs % and % does not exist', source_type_oid, target_type_oid;

      return null;
    end if;

    -- Cast function
    if v_cast_params.castmethod = 'f' then
      select get_function_identity_arguments(v_cast_params.castfunc, false, false)
      into v_cast_func;
    end if;

    -- Building DDL

    v_cast_ddl := 'CREATE CAST (' || quote_ident(v_cast_params.source_schema) || '.' || quote_ident(v_cast_params.source_type) || ' AS '
                  || quote_ident(v_cast_params.target_schema) || '.' || quote_ident(v_cast_params.target_type) || ')' || E'\n    '
                  || case when v_cast_params.castmethod = 'f' then 'WITH FUNCTION ' || v_cast_func
                          when v_cast_params.castmethod = 'i' then 'WITH INOUT'
                     else 'WITHOUT FUNCTION' end
                  || case when v_cast_params.castcontext = 'a' then E'\n    AS ASSIGNMENT'
                          when v_cast_params.castcontext = 'i' then E'\n    AS IMPLICIT'
                     else '' end
                  || ';';

    return v_cast_ddl;

  end;

$$;

comment on function get_cast_def(source_type_oid oid, target_type_oid oid)
    is 'Generates DDL cast by OIDs of source and target types';