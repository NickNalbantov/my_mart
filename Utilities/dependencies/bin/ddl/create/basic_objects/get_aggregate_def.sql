-- Examples:

-- select get_aggregate_def('max', array['bigint']),
--        get_aggregate_def('count'),
--        get_aggregate_def('percentile_cont', array['float'], array['interval']),
--        get_aggregate_def('mode', array['anyelement']);

-- select get_aggregate_def(proname, direct_typnames, order_typnames, nspname, direct_typschemas, order_typschemas),
--        get_aggregate_def("oid")
-- from
-- (
--     select p."oid",
--            p.proname,
--            n.nspname,
--            case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typnames,
--            case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typnames,
--            case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typschemas,
--            case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typschemas
--     from pg_catalog.pg_proc p
--     join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
--     join pg_catalog.pg_aggregate a on a.aggfnoid = p."oid"
--     cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
--     left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
--     left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
--     where prokind = 'a'
--     group by p."oid",
--              p.proname,
--              n.nspname,
--              a.aggnumdirectargs
-- ) a;


-- Name + schema + signature arguments

create or replace function get_aggregate_def
  (
    in_agg text,
    in_direct_arg_types text[] default null,
    in_order_arg_types text[] default null,
    in_schema text default null,
    in_direct_arg_type_schemas text[] default null,
    in_order_arg_type_schemas text[] default null
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_aggregate_def('max', array['bigint']),
--        get_aggregate_def('percentile_cont', array['float'], array['interval']);

  declare

    v_agg_ddl text;
    v_agg_params record;
    v_direct_def text := '';
    v_order_def text := '';
    v_sfunc text;
    v_finalfunc text;
    v_combinefunc text;
    v_serialfunc text;
    v_deserialfunc text;
    v_msfunc text;
    v_minvfunc text;
    v_mfinalfunc text;
    v_stype text;
    v_mstype text;
    v_sortop text;

    in_arg_types_fixed text[];
    in_arg_types_oids oid[];
    v_typoids_rec record;

    input_direct_def_excptn text;
    input_order_def_excptn text;

    int_syn text[] := array['integer', 'int'];
    smallint_syn text := 'smallint';
    bigint_syn text := 'bigint';
    num_syn text := 'decimal';
    real_syn text := 'real';
    dp_syn text[] := array['double precision', 'float'];
    time_syn text := 'time without time zone';
    timetz_syn text := 'time with time zone';
    timestamp_syn text := 'timestamp without time zone';
    timestamptz_syn text := 'timestamp with time zone';
    bpchar_syn text := 'character';
    varchar_syn text[] := array['character varying', 'char varying'];
    varbit_syn text := 'bit varying';
    bool_syn text := 'boolean';

  begin

    -- Comparing in_direct_arg_types and in_direct_arg_type_schemas input arguments
    if coalesce(cardinality(in_direct_arg_type_schemas), 0) < coalesce(cardinality(in_direct_arg_types), 0) then
      for in_card in 1..coalesce(cardinality(in_direct_arg_types), 0) - coalesce(cardinality(in_direct_arg_type_schemas), 0)
      loop
        in_direct_arg_type_schemas := array_append(in_direct_arg_type_schemas, null);
      end loop;
    elsif coalesce(cardinality(in_direct_arg_type_schemas), 0) > coalesce(cardinality(in_direct_arg_types), 0) then
      raise warning 'There are more types'' schemas specified in in_direct_arg_type_schemas argument than types'' names specified in in_direct_arg_types argument';

      return null;
    end if;

    -- Comparing in_order_arg_types and in_order_arg_type_schemas input arguments
    if coalesce(cardinality(in_order_arg_type_schemas), 0) < coalesce(cardinality(in_order_arg_types), 0) then
      for in_card in 1..coalesce(cardinality(in_order_arg_types), 0) - coalesce(cardinality(in_order_arg_type_schemas), 0)
      loop
        in_order_arg_type_schemas := array_append(in_order_arg_type_schemas, null);
      end loop;
    elsif coalesce(cardinality(in_order_arg_type_schemas), 0) > coalesce(cardinality(in_order_arg_types), 0) then
      raise warning 'There are more types'' schemas specified in in_order_arg_type_schemas argument than types'' names specified in in_order_arg_types argument';

      return null;
    end if;

    -- Catching synonyms for arguments' types
    select array_agg(case when right(tps_sw, 2) = '[]' then rtrim('_' || tps_sw, '[]')
                          else tps_sw end
                     order by rn
                    )
    into in_arg_types_fixed
    from
    (
      select rn,
        case when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = any(int_syn) then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1int4\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = smallint_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1int2\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = bigint_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1int8\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = num_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1numeric\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = real_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1float4\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = any(dp_syn) then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1float8\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = time_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1time\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = timetz_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1timetz\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = timestamp_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1timestamp\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = timestamptz_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1timestamptz\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = bpchar_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1bpchar\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = any(varchar_syn) then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1varchar\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = varbit_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1varbit\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = bool_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1bool\2')
        else tps end as tps_sw
      from
        (
          select lower(u.tps) as tps, u.rn
          from unnest(coalesce(in_direct_arg_types, array['']::text[]) || coalesce(in_order_arg_types, array['']::text[])) with ordinality u (tps, rn)
        ) a
        where tps != ''
    ) b;

    -- Getting OIDs for signature arguments
    if in_direct_arg_types is null and in_order_arg_types is null then
      in_arg_types_oids := array[0]::oid[];

    else
      for v_typoids_rec in
          select t."oid",
                 tf.rn
          from pg_catalog.pg_type t
          join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
          join unnest(in_arg_types_fixed) with ordinality tf (tps, rn) on tf.tps = t.typname
          join unnest(in_direct_arg_type_schemas || in_order_arg_type_schemas) with ordinality ts (arg_sch, rn) on tf.rn = ts.rn
          left join lateral unnest(current_schemas(true)) with ordinality s (sch, rn) on 1 = 1
          where coalesce(ts.arg_sch, s.sch) = n.nspname
          group by t."oid",
                   tf.rn
          order by tf.rn
      loop
        in_arg_types_oids[v_typoids_rec.rn] := v_typoids_rec."oid";
      end loop;

      in_arg_types_oids := array_replace(in_arg_types_oids, null, 0::oid);

    end if;

    -- Checking existence of aggregate function
    select b."oid",
           n.nspname,
           b.aggkind,
           b.aggnumdirectargs,
           b.aggtransfn,
           b.aggfinalfn,
           b.aggcombinefn,
           b.aggserialfn,
           b.aggdeserialfn,
           b.aggmtransfn,
           b.aggminvtransfn,
           b.aggmfinalfn,
           b.aggfinalextra,
           b.aggmfinalextra,
           b.aggfinalmodify,
           b.aggmfinalmodify,
           b.aggsortop,
           b.aggtranstype,
           b.aggtransspace,
           b.aggmtranstype,
           b.aggmtransspace,
           b.agginitval,
           b.aggminitval,
           b.proparallel,
           jsonb_object_agg(b.rn, jsonb_build_object('argtypes', b.argtypes, 'argmodes', b.argmodes, 'argnames', b.argnames)) as arg_params,
           max(b.rn) as arg_quantity
    into v_agg_params
    from
    (
        select a."oid",
               a.pronamespace,
               a.aggkind,
               a.aggnumdirectargs,
               a.aggtransfn,
               a.aggfinalfn,
               a.aggcombinefn,
               a.aggserialfn,
               a.aggdeserialfn,
               a.aggmtransfn,
               a.aggminvtransfn,
               a.aggmfinalfn,
               a.aggfinalextra,
               a.aggmfinalextra,
               a.aggfinalmodify,
               a.aggmfinalmodify,
               a.aggsortop,
               a.aggtranstype,
               a.aggtransspace,
               a.aggmtranstype,
               a.aggmtransspace,
               a.agginitval,
               a.aggminitval,
               a.proparallel,
               quote_ident(nt.nspname) || '.' || quote_ident(t.typname) as argtypes,
               (case a.argmodes when 'o' then 'OUT'
                                when 'b' then 'INOUT'
                                when 'v' then 'VARIADIC'
                                when 't' then 'TABLE'
                else 'IN' end
               )::text as argmodes,
               a.argnames,
               case when in_argtypes is null then 0 else row_number() over(partition by a."oid") end as rn
        from
        (
          select p."oid",
                 p.pronamespace,
                 a.aggkind,
                 a.aggnumdirectargs,
                 a.aggtransfn, -- SFUNC
                 a.aggfinalfn, -- FINALFUNC
                 a.aggcombinefn, -- COMBINEFUNC
                 a.aggserialfn, -- SERIALFUNC
                 a.aggdeserialfn, -- DESERIALFUNC
                 a.aggmtransfn, -- MSFUNC
                 a.aggminvtransfn, -- MINVFUNC
                 a.aggmfinalfn, -- MFINALFUNC
                 a.aggfinalextra, -- FINALFUNC_EXTRA
                 a.aggmfinalextra, -- MFINALFUNC_EXTRA
                 a.aggfinalmodify, -- FINALFUNC_MODIFY
                 a.aggmfinalmodify, -- MFINALFUNC_MODIFY
                 a.aggsortop, -- SORTOP
                 a.aggtranstype, -- STYPE
                 a.aggtransspace, -- SSPACE
                 a.aggmtranstype, -- MSTYPE
                 a.aggmtransspace, -- MSSPACE
                 a.agginitval, -- INITCOND
                 a.aggminitval, -- MINITCOND
                 p.proparallel, -- PARALLEL
                 unnest(string_to_array(case when p.proargtypes = ''::oidvector then null::text else p.proargtypes::text end, ' ')::oid[]) as in_argtypes,
                 unnest(coalesce(p.proallargtypes, array[0]::oid[])) as argtypes,
                 unnest(p.proargmodes) as argmodes,
                 unnest(p.proargnames) as argnames
          from pg_catalog.pg_aggregate a
          join pg_catalog.pg_proc p on a.aggfnoid = p."oid"
          where p.prokind = 'a'
                and p.proname = in_agg
                and string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[] = in_arg_types_oids
        ) a
        left join pg_catalog.pg_type t on coalesce(a.in_argtypes, a.argtypes) = t."oid"
        left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    ) b
    left join pg_catalog.pg_namespace n on n."oid" = b.pronamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where n.nspname = coalesce(in_schema, s.sch)
    group by b."oid",
             n.nspname,
             b.aggkind,
             b.aggnumdirectargs,
             b.aggtransfn,
             b.aggfinalfn,
             b.aggcombinefn,
             b.aggserialfn,
             b.aggdeserialfn,
             b.aggmtransfn,
             b.aggminvtransfn,
             b.aggmfinalfn,
             b.aggfinalextra,
             b.aggmfinalextra,
             b.aggfinalmodify,
             b.aggmfinalmodify,
             b.aggsortop,
             b.aggtranstype,
             b.aggtransspace,
             b.aggmtranstype,
             b.aggmtransspace,
             b.agginitval,
             b.aggminitval,
             b.proparallel,
             s.rn
    order by s.rn
    limit 1;

    if v_agg_params."oid" is null then

      select string_agg(coalesce(quote_ident(ts.arg_sch), '[' || array_to_string(current_schemas(true), ', ') || ']') || '.' || quote_ident(tf.tps), ', ' order by ts.rn)
      into input_direct_def_excptn
      from unnest(in_direct_arg_type_schemas) with ordinality ts (arg_sch, rn)
      join unnest(in_arg_types_fixed[1:cardinality(in_direct_arg_types)]) with ordinality tf (tps, rn) on tf.rn = ts.rn;

      select string_agg(coalesce(quote_ident(ts.arg_sch), '[' || array_to_string(current_schemas(true), ', ') || ']') || '.' || quote_ident(tf.tps), ', ' order by ts.rn)
      into input_order_def_excptn
      from unnest(in_order_arg_type_schemas) with ordinality ts (arg_sch, rn)
      join unnest(in_arg_types_fixed[cardinality(in_direct_arg_types) + 1:]) with ordinality tf (tps, rn) on tf.rn = ts.rn;

      raise warning 'Aggregate %.%(%) does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_agg),
        case when in_direct_arg_types is not null then input_direct_def_excptn else '' end ||
        case when in_direct_arg_types is not null and in_order_arg_types is not null then ' ' else '' end ||
        case when in_order_arg_types is not null then 'ORDER BY ' || input_order_def_excptn else '' end;

      return null;
    end if;

    -- Getting final list of arguments

    -- Direct arguments
    if v_agg_params.arg_quantity = 0 then  -- Functions with no parameters, like count(*)
        v_direct_def = '*';

    elsif v_agg_params.arg_quantity != 0 and v_agg_params.aggkind = 'n' then -- normal aggregates
      <<direct_def>>
      for arg_num in 1..v_agg_params.arg_quantity
      loop
          v_direct_def := v_direct_def || ', ' ||
                           (v_agg_params.arg_params -> arg_num::text ->> 'argmodes')::text ||
                           case when coalesce((v_agg_params.arg_params -> arg_num::text ->> 'argnames')::text, '') != ''
                                then ' ' || (v_agg_params.arg_params -> arg_num::text ->> 'argnames')::text
                                else '' end ||
                           case when (v_agg_params.arg_params -> arg_num::text ->> 'argtypes')::text is null
                                then ''
                                else ' ' || (v_agg_params.arg_params -> arg_num::text ->> 'argtypes')::text end;
                    end loop direct_def;

    else -- hypothetical and ordered-set aggregates
      <<direct_def_ord>>
      for arg_num in 1..v_agg_params.aggnumdirectargs
      loop
          v_direct_def := v_direct_def || ', ' ||
                           (v_agg_params.arg_params -> arg_num::text ->> 'argmodes')::text ||
                           case when coalesce((v_agg_params.arg_params -> arg_num::text ->> 'argnames')::text, '') != ''
                                then ' ' || (v_agg_params.arg_params -> arg_num::text ->> 'argnames')::text
                                else '' end ||
                           case when (v_agg_params.arg_params -> arg_num::text ->> 'argtypes')::text is null
                                then ''
                                else ' ' || (v_agg_params.arg_params -> arg_num::text ->> 'argtypes')::text end;
      end loop direct_def_ord;

    end if;

    -- Sorting arguments
    if v_agg_params.aggkind in ('o', 'h') and (v_agg_params.arg_params -> v_agg_params.aggnumdirectargs::text ->> 'argmodes')::text = 'VARIADIC' then -- hypothetical functions with VARIADIC arguments
      v_order_def := 'VARIADIC'
                     || case when coalesce((v_agg_params.arg_params -> v_agg_params.aggnumdirectargs::text ->> 'argnames')::text, '') != ''
                                   then ' ' || (v_agg_params.arg_params -> v_agg_params.aggnumdirectargs::text ->> 'argnames')::text
                        else '' end
                     || ' pg_catalog.any';
    else
      <<order_def>>
      for arg_num in (v_agg_params.aggnumdirectargs + 1)..v_agg_params.arg_quantity
      loop
          v_order_def := v_order_def || ', ' ||
                          (v_agg_params.arg_params -> arg_num::text ->> 'argmodes')::text ||
                          case when coalesce((v_agg_params.arg_params -> arg_num::text ->> 'argnames')::text, '') != ''
                               then ' ' || (v_agg_params.arg_params -> arg_num::text ->> 'argnames')::text
                               else '' end ||
                          case when (v_agg_params.arg_params -> arg_num::text ->> 'argtypes')::text is null
                               then ''
                               else ' ' || (v_agg_params.arg_params -> arg_num::text ->> 'argtypes')::text end;
      end loop order_def;

    end if;


    -- Common parameters

      -- Processing SFUNC
    select get_function_identity_arguments(v_agg_params.aggtransfn, false, false)
    into v_sfunc;

      -- Processing FINALFUNC
    if v_agg_params.aggfinalfn != '-'::regproc then
      select get_function_identity_arguments(v_agg_params.aggfinalfn, false, false)
      into v_finalfunc;
    else v_finalfunc := null;
    end if;

      -- Processing STYPE
    select quote_ident(n.nspname) || '.' || quote_ident(t.typname)
    into v_stype
    from pg_catalog.pg_type t
    join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    where t."oid" = v_agg_params.aggtranstype;


    -- Hypothetical and ordered-set aggregates

    if v_agg_params.aggkind in ('o', 'h') then

        -- Building DDL

      v_agg_ddl := 'CREATE OR REPLACE AGGREGATE ' || quote_ident(v_agg_params.nspname) || '.' || quote_ident(in_agg) || '('
                   || case when v_direct_def != '' then trim(v_direct_def, ', ') || ' ' else '' end
                   || 'ORDER BY ' || trim(v_order_def, ', ') || E')\n(\n    '
                   || 'SFUNC = ' || v_sfunc || E',\n    '
                   || 'STYPE = ' || v_stype || E',\n    '
                   || case when v_agg_params.aggtransspace != 0 then 'SSPACE = ' || v_agg_params.aggtransspace || E',\n    ' else '' end
                   || case when v_finalfunc is not null then 'FINALFUNC = ' || v_finalfunc || E',\n    ' else '' end
                   || case when v_agg_params.aggfinalextra is true then E'FINALFUNC_EXTRA,\n    ' else '' end
                   || case when v_finalfunc is not null and v_agg_params.aggfinalmodify = 'r' then E'FINALFUNC_MODIFY = READ_ONLY,\n    '
                           when v_finalfunc is not null and v_agg_params.aggfinalmodify = 'w' then E'FINALFUNC_MODIFY = READ_WRITE,\n    '
                           when v_finalfunc is not null and v_agg_params.aggfinalmodify = 's' then E'FINALFUNC_MODIFY = SHAREABLE,\n    '
                      else '' end
                   || case when v_agg_params.agginitval is not null then 'INITCOND = ' || v_agg_params.agginitval || E',\n    ' else '' end
                   || case when v_agg_params.aggkind = 'h' then E'HYPOTHETICAL,\n    ' else '' end
                   || case when v_agg_params.proparallel = 'r' then E'PARALLEL = RESTRICTED\n);'
                           when v_agg_params.proparallel = 'u' then E'PARALLEL = UNSAFE\n);'
                           when v_agg_params.proparallel = 's' then E'PARALLEL = SAFE\n);' end;


      -- Normal aggregates

    elsif v_agg_params.aggkind = 'n' then

        -- Optional parameters

          -- Processing COMBINEFUNC
          if v_agg_params.aggcombinefn != '-'::regproc then
            select get_function_identity_arguments(v_agg_params.aggcombinefn, false, false)
            into v_combinefunc;
          else v_combinefunc := null;
          end if;

          -- Processing SERIALFUNC
          if v_agg_params.aggserialfn != '-'::regproc then
            select get_function_identity_arguments(v_agg_params.aggserialfn, false, false)
            into v_serialfunc;
          else v_serialfunc := null;
          end if;

          -- Processing DESERIALFUNC
          if v_agg_params.aggdeserialfn != '-'::regproc then
            select get_function_identity_arguments(v_agg_params.aggdeserialfn, false, false)
            into v_deserialfunc;
          else v_deserialfunc := null;
          end if;

          -- Processing MSFUNC
          if v_agg_params.aggmtransfn != '-'::regproc then
            select get_function_identity_arguments(v_agg_params.aggmtransfn, false, false)
            into v_msfunc;
          else v_msfunc := null;
          end if;

          -- Processing MINVFUNC
          if v_agg_params.aggminvtransfn != '-'::regproc then
            select get_function_identity_arguments(v_agg_params.aggminvtransfn, false, false)
            into v_minvfunc;
          else v_minvfunc := null;
          end if;

          -- Processing MFINALFUNC
          if v_agg_params.aggmfinalfn != '-'::regproc then
            select get_function_identity_arguments(v_agg_params.aggmfinalfn, false, false)
            into v_mfinalfunc;
          else v_mfinalfunc := null;
          end if;

          -- Processing MSTYPE
          if v_agg_params.aggmtranstype != 0::oid then
            select quote_ident(n.nspname) || '.' || quote_ident(t.typname)
            into v_mstype
            from pg_catalog.pg_type t
            join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
            where t."oid" = v_agg_params.aggmtranstype;
          else v_mstype := null;
          end if;

          -- Processing SORTOP
          if v_agg_params.aggsortop != 0::oid then
            select quote_ident(n.nspname) || '.' || o.oprname
            into v_sortop
            from pg_catalog.pg_operator o
            join pg_catalog.pg_namespace n on n."oid" = o.oprnamespace
            where o."oid" = v_agg_params.aggsortop;
          else v_sortop := null;
          end if;

        -- Building DDL

      v_agg_ddl := 'CREATE OR REPLACE AGGREGATE ' || quote_ident(v_agg_params.nspname) || '.' || quote_ident(in_agg) || '('
                   || trim(v_direct_def, ', ') || E')\n(\n    '
                   || 'SFUNC = ' || v_sfunc || E',\n    '
                   || 'STYPE = ' || v_stype || E',\n    '
                   || case when v_agg_params.aggtransspace != 0 then 'SSPACE = ' || v_agg_params.aggtransspace || E',\n    ' else '' end
                   || case when v_finalfunc is not null then 'FINALFUNC = ' || v_finalfunc || E',\n    ' else '' end
                   || case when v_agg_params.aggfinalextra is true then E'FINALFUNC_EXTRA,\n    ' else '' end
                   || case when v_finalfunc is not null and v_agg_params.aggfinalmodify = 'r' then E'FINALFUNC_MODIFY = READ_ONLY,\n    '
                           when v_finalfunc is not null and v_agg_params.aggfinalmodify = 'w' then E'FINALFUNC_MODIFY = READ_WRITE,\n    '
                           when v_finalfunc is not null and v_agg_params.aggfinalmodify = 's' then E'FINALFUNC_MODIFY = SHAREABLE,\n    '
                      else '' end
                   || case when v_combinefunc is not null then 'COMBINEFUNC = ' || v_combinefunc || E',\n    ' else '' end
                   || case when v_serialfunc is not null then 'SERIALFUNC = ' || v_serialfunc || E',\n    ' else '' end
                   || case when v_deserialfunc is not null then 'DESERIALFUNC = ' || v_deserialfunc || E',\n    ' else '' end
                   || case when v_agg_params.agginitval is not null then 'INITCOND = ' || v_agg_params.agginitval || E',\n    ' else '' end
                   || case when v_msfunc is not null then 'MSFUNC = ' || v_msfunc || E',\n    ' else '' end
                   || case when v_minvfunc is not null then 'MINVFUNC = ' || v_minvfunc || E',\n    ' else '' end
                   || case when v_mstype is not null then 'MSTYPE = ' || v_mstype || E',\n    ' else '' end
                   || case when v_agg_params.aggmtransspace != 0 then 'MSSPACE = ' || v_agg_params.aggmtransspace || E',\n    ' else '' end
                   || case when v_mfinalfunc is not null then 'MFINALFUNC = ' || v_mfinalfunc || E',\n    ' else '' end
                   || case when v_agg_params.aggmfinalextra is true then E'MFINALFUNC_EXTRA,\n    ' else '' end
                   || case when v_mfinalfunc is not null and v_agg_params.aggmfinalmodify = 'r' then E'MFINALFUNC_MODIFY = READ_ONLY,\n    '
                           when v_mfinalfunc is not null and v_agg_params.aggmfinalmodify = 'w' then E'MFINALFUNC_MODIFY = READ_WRITE,\n    '
                           when v_mfinalfunc is not null and v_agg_params.aggmfinalmodify = 's' then E'MFINALFUNC_MODIFY = SHAREABLE,\n    '
                      else '' end
                   || case when v_agg_params.aggminitval is not null then 'MINITCOND = ' || v_agg_params.aggminitval || E',\n    ' else '' end
                   || case when v_sortop is not null then 'SORTOP = ' || v_sortop || E',\n    ' else '' end
                   || case when v_agg_params.proparallel = 'r' then E'PARALLEL = RESTRICTED\n);'
                           when v_agg_params.proparallel = 'u' then E'PARALLEL = UNSAFE\n);'
                           when v_agg_params.proparallel = 's' then E'PARALLEL = SAFE\n);' end;

    end if;

    return v_agg_ddl;

  end;

$$;

comment on function get_aggregate_def(in_agg text, in_direct_arg_types text[], in_order_arg_types text[], in_schema text, in_direct_arg_type_schemas text[], in_order_arg_type_schemas text[])
    is 'Generates CREATE command for an aggregate function by its name, schema (default - search_path) and signature arguments (as text arrays of types'' names and schemas (default - search_path))';


-- OID

create or replace function get_aggregate_def
  (
    agg_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_aggregate_def('max(bigint)'::regprocedure),
--        get_aggregate_def('percentile_cont(float, interval)'::regprocedure);

-- select get_aggregate_def(aggfnoid)
-- from pg_catalog.pg_aggregate
-- limit 10;

  declare

    v_agg_ddl text;
    v_agg_params record;
    v_direct_def text := '';
    v_order_def text := '';
    v_sfunc text;
    v_finalfunc text;
    v_combinefunc text;
    v_serialfunc text;
    v_deserialfunc text;
    v_msfunc text;
    v_minvfunc text;
    v_mfinalfunc text;
    v_stype text;
    v_mstype text;
    v_sortop text;

  begin

    -- Checking existence of aggregate function
    select proname,
           nspname,
           aggkind,
           aggnumdirectargs,
           aggtransfn,
           aggfinalfn,
           aggcombinefn,
           aggserialfn,
           aggdeserialfn,
           aggmtransfn,
           aggminvtransfn,
           aggmfinalfn,
           aggfinalextra,
           aggmfinalextra,
           aggfinalmodify,
           aggmfinalmodify,
           aggsortop,
           aggtranstype,
           aggtransspace,
           aggmtranstype,
           aggmtransspace,
           agginitval,
           aggminitval,
           proparallel,
           jsonb_object_agg(rn, jsonb_build_object('argtypes', argtypes, 'argmodes', argmodes, 'argnames', argnames)) as arg_params,
           max(rn) as arg_quantity
    into v_agg_params
    from
    (
        select a.proname,
               a.nspname,
               a.aggkind,
               a.aggnumdirectargs,
               a.aggtransfn,
               a.aggfinalfn,
               a.aggcombinefn,
               a.aggserialfn,
               a.aggdeserialfn,
               a.aggmtransfn,
               a.aggminvtransfn,
               a.aggmfinalfn,
               a.aggfinalextra,
               a.aggmfinalextra,
               a.aggfinalmodify,
               a.aggmfinalmodify,
               a.aggsortop,
               a.aggtranstype,
               a.aggtransspace,
               a.aggmtranstype,
               a.aggmtransspace,
               a.agginitval,
               a.aggminitval,
               a.proparallel,
               quote_ident(nt.nspname) || '.' || quote_ident(t.typname) as argtypes,
               (case a.argmodes when 'o' then 'OUT'
                                when 'b' then 'INOUT'
                                when 'v' then 'VARIADIC'
                                when 't' then 'TABLE'
                else 'IN' end
               )::text as argmodes,
               a.argnames,
               case when in_argtypes is null then 0 else row_number() over(partition by a."oid") end as rn
        from
        (
          select p."oid",
                 p.proname,
                 n.nspname,
                 a.aggkind,
                 a.aggnumdirectargs,
                 a.aggtransfn, -- SFUNC
                 a.aggfinalfn, -- FINALFUNC
                 a.aggcombinefn, -- COMBINEFUNC
                 a.aggserialfn, -- SERIALFUNC
                 a.aggdeserialfn, -- DESERIALFUNC
                 a.aggmtransfn, -- MSFUNC
                 a.aggminvtransfn, -- MINVFUNC
                 a.aggmfinalfn, -- MFINALFUNC
                 a.aggfinalextra, -- FINALFUNC_EXTRA
                 a.aggmfinalextra, -- MFINALFUNC_EXTRA
                 a.aggfinalmodify, -- FINALFUNC_MODIFY
                 a.aggmfinalmodify, -- MFINALFUNC_MODIFY
                 a.aggsortop, -- SORTOP
                 a.aggtranstype, -- STYPE
                 a.aggtransspace, -- SSPACE
                 a.aggmtranstype, -- MSTYPE
                 a.aggmtransspace, -- MSSPACE
                 a.agginitval, -- INITCOND
                 a.aggminitval, -- MINITCOND
                 p.proparallel, -- PARALLEL
                 unnest(string_to_array(case when p.proargtypes = ''::oidvector then null::text else p.proargtypes::text end, ' ')::oid[]) as in_argtypes,
                 unnest(coalesce(p.proallargtypes, array[0]::oid[])) as argtypes,
                 unnest(p.proargmodes) as argmodes,
                 unnest(p.proargnames) as argnames
          from pg_catalog.pg_aggregate a
          join pg_catalog.pg_proc p on a.aggfnoid = p."oid"
          join pg_catalog.pg_namespace n on n."oid" = p.pronamespace
          where p.prokind = 'a'
                and p."oid" = agg_oid
        ) a
        left join pg_catalog.pg_type t on coalesce(a.in_argtypes, a.argtypes) = t."oid"
        left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    ) b
    group by proname,
             nspname,
             aggkind,
             aggnumdirectargs,
             aggtransfn,
             aggfinalfn,
             aggcombinefn,
             aggserialfn,
             aggdeserialfn,
             aggmtransfn,
             aggminvtransfn,
             aggmfinalfn,
             aggfinalextra,
             aggmfinalextra,
             aggfinalmodify,
             aggmfinalmodify,
             aggsortop,
             aggtranstype,
             aggtransspace,
             aggmtranstype,
             aggmtransspace,
             agginitval,
             aggminitval,
             proparallel;

    if v_agg_params.proname is null then
      raise warning 'Aggregate with OID % does not exist', agg_oid;

      return null;
    end if;


    -- Getting final list of arguments

    -- Direct arguments
    if v_agg_params.arg_quantity = 0 then  -- Functions with no parameters, like count(*)
        v_direct_def = '*';

    elsif v_agg_params.arg_quantity != 0 and v_agg_params.aggkind = 'n' then -- normal aggregates
      <<direct_def>>
      for arg_num in 1..v_agg_params.arg_quantity
      loop
          v_direct_def := v_direct_def || ', ' ||
                           (v_agg_params.arg_params -> arg_num::text ->> 'argmodes')::text ||
                           case when coalesce((v_agg_params.arg_params -> arg_num::text ->> 'argnames')::text, '') != ''
                                then ' ' || (v_agg_params.arg_params -> arg_num::text ->> 'argnames')::text
                                else '' end ||
                           case when (v_agg_params.arg_params -> arg_num::text ->> 'argtypes')::text is null
                                then ''
                                else ' ' || (v_agg_params.arg_params -> arg_num::text ->> 'argtypes')::text end;
      end loop direct_def;

    else -- hypothetical and ordered-set aggregates
      <<direct_def_ord>>
      for arg_num in 1..v_agg_params.aggnumdirectargs
      loop
          v_direct_def := v_direct_def || ', ' ||
                           (v_agg_params.arg_params -> arg_num::text ->> 'argmodes')::text ||
                           case when coalesce((v_agg_params.arg_params -> arg_num::text ->> 'argnames')::text, '') != ''
                                then ' ' || (v_agg_params.arg_params -> arg_num::text ->> 'argnames')::text
                                else '' end ||
                           case when (v_agg_params.arg_params -> arg_num::text ->> 'argtypes')::text is null
                                then ''
                                else ' ' || (v_agg_params.arg_params -> arg_num::text ->> 'argtypes')::text end;
      end loop direct_def_ord;

    end if;

    -- Sorting arguments
    if v_agg_params.aggkind in ('o', 'h') and (v_agg_params.arg_params -> v_agg_params.aggnumdirectargs::text ->> 'argmodes')::text = 'VARIADIC' then -- hypothetical functions with VARIADIC arguments
      v_order_def := 'VARIADIC'
                     || case when coalesce((v_agg_params.arg_params -> v_agg_params.aggnumdirectargs::text ->> 'argnames')::text, '') != ''
                                   then ' ' || (v_agg_params.arg_params -> v_agg_params.aggnumdirectargs::text ->> 'argnames')::text
                        else '' end
                     || ' pg_catalog.any';
    else
      <<order_def>>
      for arg_num in (v_agg_params.aggnumdirectargs + 1)..v_agg_params.arg_quantity
      loop
          v_order_def := v_order_def || ', ' ||
                          (v_agg_params.arg_params -> arg_num::text ->> 'argmodes')::text ||
                          case when coalesce((v_agg_params.arg_params -> arg_num::text ->> 'argnames')::text, '') != ''
                               then ' ' || (v_agg_params.arg_params -> arg_num::text ->> 'argnames')::text
                               else '' end ||
                          case when (v_agg_params.arg_params -> arg_num::text ->> 'argtypes')::text is null
                               then ''
                               else ' ' || (v_agg_params.arg_params -> arg_num::text ->> 'argtypes')::text end;
      end loop order_def;

    end if;


    -- Common parameters

      -- Processing SFUNC
    select get_function_identity_arguments(v_agg_params.aggtransfn, false, false)
    into v_sfunc;

      -- Processing FINALFUNC
    if v_agg_params.aggfinalfn != '-'::regproc then
      select get_function_identity_arguments(v_agg_params.aggfinalfn, false, false)
      into v_finalfunc;
    else v_finalfunc := null;
    end if;

      -- Processing STYPE
    select quote_ident(n.nspname) || '.' || quote_ident(t.typname)
    into v_stype
    from pg_catalog.pg_type t
    join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    where t."oid" = v_agg_params.aggtranstype;


    -- Hypothetical and ordered-set aggregates

    if v_agg_params.aggkind in ('o', 'h') then

        -- Building DDL

      v_agg_ddl := 'CREATE OR REPLACE AGGREGATE ' || quote_ident(v_agg_params.nspname) || '.' || quote_ident(v_agg_params.proname) || '('
                   || case when v_direct_def != '' then trim(v_direct_def, ', ') || ' ' else '' end
                   || 'ORDER BY ' || trim(v_order_def, ', ') || E')\n(\n    '
                   || 'SFUNC = ' || v_sfunc || E',\n    '
                   || 'STYPE = ' || v_stype || E',\n    '
                   || case when v_agg_params.aggtransspace != 0 then 'SSPACE = ' || v_agg_params.aggtransspace || E',\n    ' else '' end
                   || case when v_finalfunc is not null then 'FINALFUNC = ' || v_finalfunc || E',\n    ' else '' end
                   || case when v_agg_params.aggfinalextra is true then E'FINALFUNC_EXTRA,\n    ' else '' end
                   || case when v_finalfunc is not null and v_agg_params.aggfinalmodify = 'r' then E'FINALFUNC_MODIFY = READ_ONLY,\n    '
                           when v_finalfunc is not null and v_agg_params.aggfinalmodify = 'w' then E'FINALFUNC_MODIFY = READ_WRITE,\n    '
                           when v_finalfunc is not null and v_agg_params.aggfinalmodify = 's' then E'FINALFUNC_MODIFY = SHAREABLE,\n    '
                      else '' end
                   || case when v_agg_params.agginitval is not null then 'INITCOND = ' || v_agg_params.agginitval || E',\n    ' else '' end
                   || case when v_agg_params.aggkind = 'h' then E'HYPOTHETICAL,\n    ' else '' end
                   || case when v_agg_params.proparallel = 'r' then E'PARALLEL = RESTRICTED\n);'
                           when v_agg_params.proparallel = 'u' then E'PARALLEL = UNSAFE\n);'
                           when v_agg_params.proparallel = 's' then E'PARALLEL = SAFE\n);' end;


      -- Normal aggregates

    elsif v_agg_params.aggkind = 'n' then

        -- Optional parameters

          -- Processing COMBINEFUNC
          if v_agg_params.aggcombinefn != '-'::regproc then
            select get_function_identity_arguments(v_agg_params.aggcombinefn, false, false)
            into v_combinefunc;
          else v_combinefunc := null;
          end if;

          -- Processing SERIALFUNC
          if v_agg_params.aggserialfn != '-'::regproc then
            select get_function_identity_arguments(v_agg_params.aggserialfn, false, false)
            into v_serialfunc;
          else v_serialfunc := null;
          end if;

          -- Processing DESERIALFUNC
          if v_agg_params.aggdeserialfn != '-'::regproc then
            select get_function_identity_arguments(v_agg_params.aggdeserialfn, false, false)
            into v_deserialfunc;
          else v_deserialfunc := null;
          end if;

          -- Processing MSFUNC
          if v_agg_params.aggmtransfn != '-'::regproc then
            select get_function_identity_arguments(v_agg_params.aggmtransfn, false, false)
            into v_msfunc;
          else v_msfunc := null;
          end if;

          -- Processing MINVFUNC
          if v_agg_params.aggminvtransfn != '-'::regproc then
            select get_function_identity_arguments(v_agg_params.aggminvtransfn, false, false)
            into v_minvfunc;
          else v_minvfunc := null;
          end if;

          -- Processing MFINALFUNC
          if v_agg_params.aggmfinalfn != '-'::regproc then
            select get_function_identity_arguments(v_agg_params.aggmfinalfn, false, false)
            into v_mfinalfunc;
          else v_mfinalfunc := null;
          end if;

          -- Processing MSTYPE
          if v_agg_params.aggmtranstype != 0::oid then
            select quote_ident(n.nspname) || '.' || quote_ident(t.typname)
            into v_mstype
            from pg_catalog.pg_type t
            join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
            where t."oid" = v_agg_params.aggmtranstype;
          else v_mstype := null;
          end if;

          -- Processing SORTOP
          if v_agg_params.aggsortop != 0::oid then
            select quote_ident(n.nspname) || '.' || o.oprname
            into v_sortop
            from pg_catalog.pg_operator o
            join pg_catalog.pg_namespace n on n."oid" = o.oprnamespace
            where o."oid" = v_agg_params.aggsortop;
          else v_sortop := null;
          end if;

        -- Building DDL

      v_agg_ddl := 'CREATE OR REPLACE AGGREGATE ' || quote_ident(v_agg_params.nspname) || '.' || quote_ident(v_agg_params.proname) || '('
                   || trim(v_direct_def, ', ') || E')\n(\n    '
                   || 'SFUNC = ' || v_sfunc || E',\n    '
                   || 'STYPE = ' || v_stype || E',\n    '
                   || case when v_agg_params.aggtransspace != 0 then 'SSPACE = ' || v_agg_params.aggtransspace || E',\n    ' else '' end
                   || case when v_finalfunc is not null then 'FINALFUNC = ' || v_finalfunc || E',\n    ' else '' end
                   || case when v_agg_params.aggfinalextra is true then E'FINALFUNC_EXTRA,\n    ' else '' end
                   || case when v_finalfunc is not null and v_agg_params.aggfinalmodify = 'r' then E'FINALFUNC_MODIFY = READ_ONLY,\n    '
                           when v_finalfunc is not null and v_agg_params.aggfinalmodify = 'w' then E'FINALFUNC_MODIFY = READ_WRITE,\n    '
                           when v_finalfunc is not null and v_agg_params.aggfinalmodify = 's' then E'FINALFUNC_MODIFY = SHAREABLE,\n    '
                      else '' end
                   || case when v_combinefunc is not null then 'COMBINEFUNC = ' || v_combinefunc || E',\n    ' else '' end
                   || case when v_serialfunc is not null then 'SERIALFUNC = ' || v_serialfunc || E',\n    ' else '' end
                   || case when v_deserialfunc is not null then 'DESERIALFUNC = ' || v_deserialfunc || E',\n    ' else '' end
                   || case when v_agg_params.agginitval is not null then 'INITCOND = ' || v_agg_params.agginitval || E',\n    ' else '' end
                   || case when v_msfunc is not null then 'MSFUNC = ' || v_msfunc || E',\n    ' else '' end
                   || case when v_minvfunc is not null then 'MINVFUNC = ' || v_minvfunc || E',\n    ' else '' end
                   || case when v_mstype is not null then 'MSTYPE = ' || v_mstype || E',\n    ' else '' end
                   || case when v_agg_params.aggmtransspace != 0 then 'MSSPACE = ' || v_agg_params.aggmtransspace || E',\n    ' else '' end
                   || case when v_mfinalfunc is not null then 'MFINALFUNC = ' || v_mfinalfunc || E',\n    ' else '' end
                   || case when v_agg_params.aggmfinalextra is true then E'MFINALFUNC_EXTRA,\n    ' else '' end
                   || case when v_mfinalfunc is not null and v_agg_params.aggmfinalmodify = 'r' then E'MFINALFUNC_MODIFY = READ_ONLY,\n    '
                           when v_mfinalfunc is not null and v_agg_params.aggmfinalmodify = 'w' then E'MFINALFUNC_MODIFY = READ_WRITE,\n    '
                           when v_mfinalfunc is not null and v_agg_params.aggmfinalmodify = 's' then E'MFINALFUNC_MODIFY = SHAREABLE,\n    '
                      else '' end
                   || case when v_agg_params.aggminitval is not null then 'MINITCOND = ' || v_agg_params.aggminitval || E',\n    ' else '' end
                   || case when v_sortop is not null then 'SORTOP = ' || v_sortop || E',\n    ' else '' end
                   || case when v_agg_params.proparallel = 'r' then E'PARALLEL = RESTRICTED\n);'
                           when v_agg_params.proparallel = 'u' then E'PARALLEL = UNSAFE\n);'
                           when v_agg_params.proparallel = 's' then E'PARALLEL = SAFE\n);' end;

    end if;

    return v_agg_ddl;

  end;

$$;

comment on function get_aggregate_def(agg_oid oid)
    is 'Generates CREATE command for an aggregate function by its OID';