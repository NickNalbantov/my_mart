-- Examples:

-- select get_event_trigger_def(evtname),
--        get_event_trigger_def("oid")
-- from pg_catalog.pg_event_trigger;


-- Name

create or replace function get_event_trigger_def
  (
    in_et text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_event_trigger_def(evtname)
-- from pg_catalog.pg_event_trigger;

  declare

    v_et_ddl text;
    v_et_tags text;
    v_et_func text;
    v_et_params record;

  begin

    -- Checking existence of event trigger
    select "oid",
           evtevent,
           evtfoid,
           evttags
    into v_et_params
    from pg_catalog.pg_event_trigger
    where evtname = in_et;

    if v_et_params."oid" is null then
      raise warning 'Event trigger % does not exist', quote_ident(in_et);

      return null;
    end if;

    -- Tag list
    if v_et_params.evttags is not null then
      select 'WHEN TAG IN (' || string_agg(t.i, ', ') || E')\n    '
      into v_et_tags
      from unnest(v_et_params.evttags) t(i);
    else
      v_et_tags := '';
    end if;

    -- Function
    select quote_ident(n.nspname) || '.' || quote_ident(p.proname) || '()'
    into v_et_func
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on n."oid" = p.pronamespace
    where p."oid" = v_et_params.evtfoid;

    -- Building DDL
    v_et_ddl := 'CREATE EVENT TRIGGER ' || quote_ident(in_et)
                || E'\n    ON ' || v_et_params.evtevent || E'\n    '
                || v_et_tags
                || 'EXECUTE FUNCTION ' || v_et_func
                || ';';

    return v_et_ddl;

  end;

$$;

comment on function get_event_trigger_def(in_et text)
    is 'Generates CREATE command for an event trigger by its name';


-- OID

create or replace function get_event_trigger_def
  (
    et_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_event_trigger_def("oid")
-- from pg_catalog.pg_event_trigger;

  declare

    v_et_ddl text;
    v_et_tags text;
    v_et_func text;
    v_et_params record;

  begin

    -- Checking existence of event trigger
    select evtname,
           evtevent,
           evtfoid,
           evttags
    into v_et_params
    from pg_catalog.pg_event_trigger
    where "oid" = et_oid;

    if v_et_params.evtname is null then
      raise warning 'Event trigger with OID % does not exist', et_oid;

      return null;
    end if;

    -- Tag list
    if v_et_params.evttags is not null then
      select 'WHEN TAG IN (' || string_agg(t.i, ', ') || E')\n    '
      into v_et_tags
      from unnest(v_et_params.evttags) t(i);
    else
      v_et_tags := '';
    end if;

    -- Function
    select quote_ident(n.nspname) || '.' || quote_ident(p.proname) || '()'
    into v_et_func
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on n."oid" = p.pronamespace
    where p."oid" = v_et_params.evtfoid;

    -- Building DDL
    v_et_ddl := 'CREATE EVENT TRIGGER ' || quote_ident(v_et_params.evtname)
                || E'\n    ON ' || v_et_params.evtevent || E'\n    '
                || v_et_tags
                || 'EXECUTE FUNCTION ' || v_et_func
                || ';';

    return v_et_ddl;

  end;

$$;

comment on function get_event_trigger_def(et_oid oid)
    is 'Generates CREATE command for an event trigger by its OID';