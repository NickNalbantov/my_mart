-- Examples:

-- select get_view_def(c.relname, n.nspname),
--        get_view_def(c."oid")
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where c.relkind in ('v', 'm');

-- select get_view_def('pg_settings');


-- Name + schema

create or replace function get_view_def
  (
    in_view text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Generates DDL script for views and materialized views

-- Examples:

-- select get_view_def('pg_settings');

-- select get_view_def(relname)
-- from pg_catalog.pg_class
-- where relkind in ('v', 'm')
--       and relnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_view_ddl text;
    v_view_params record;
    v_columns text;
    v_view_opts text;

    v_am text := '';
    v_tablespace text := '';
    v_att_params record;
    v_alter text := '';
    v_alter_cl text := '';

  begin

    -- Checking existence of view
    select c."oid",
           c.relam,
           c.reltablespace,
           c.relpersistence,
           c.relkind,
           c.relispopulated,
           c.reloptions,
           n.nspname
    into v_view_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.relkind in ('v', 'm')
          and c.relname = in_view
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_view_params."oid" is null then
      raise warning 'View / materialized view %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_view);

      return null;
    end if;

    -- View attributes
    select string_agg(attname, E',\n    ' order by attnum)
    into v_columns
    from pg_catalog.pg_attribute
    where attrelid = v_view_params."oid"
          and attnum > 0;

    -- View and storage options
    if v_view_params.reloptions is not null then
      select E'\nWITH (' || string_agg(replace(str, '=', ' = '), ', ' order by rn) || ')'
      into v_view_opts
      from unnest(v_view_params.reloptions) with ordinality a (str, rn);
    else
      v_view_opts := '';
    end if;


    -- Regular views-specific options
    if v_view_params.relkind = 'v' then

      for v_att_params in

        select a.attname,
               ad.adbin
        from pg_catalog.pg_attribute a
        join pg_catalog.pg_attrdef ad on ad.adrelid = a.attrelid and ad.adnum = a.attnum
        where a.atthasdef is true
              and a.attisdropped is false
              and a.attrelid = v_view_params."oid"
        order by a.attnum

      loop

        -- Alter columns
        v_alter := v_alter
                   || case when v_att_params.adbin is not null
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname)
                                || ' SET DEFAULT (' || pg_catalog.pg_get_expr(v_att_params.adbin, 0, true) || '),'
                      else '' end;

      end loop;


    -- Materialized views-specific options
    elsif v_view_params.relkind = 'm' then

      -- Access method
      select E'\nUSING ' || quote_ident(a.amname)
      into v_am
      from pg_catalog.pg_am a
      where a."oid" = v_view_params.relam;

      -- Tablespace
      if v_view_params.reltablespace = 0::oid then
        v_tablespace := E'\nTABLESPACE pg_default';
      else
        select E'\nTABLESPACE ' || quote_ident(spcname)
        into v_tablespace
        from pg_catalog.pg_tablespace
        where "oid" = v_view_params.reltablespace;
      end if;

      -- Attribute parameters
      for v_att_params in

        select a.attname,
               a.attstattarget,
               a.attstorage,
               a.attcompression,
               string_agg(replace(ao.opts, '=', ' = '), ', ' order by ao.rn) as attoptions,
               t.typstorage
        from pg_catalog.pg_attribute a
        join pg_catalog.pg_type t on t."oid" = a.atttypid
        left join unnest(a.attoptions) with ordinality ao (opts, rn) on 1 = 1
        where a.attrelid = v_view_params."oid"
              and a.attnum > 0
              and a.attisdropped is false
        group by a.attnum,
                 a.attname,
                 a.attstattarget,
                 a.attstorage,
                 a.attcompression,
                 t.typstorage
        order by a.attnum

      loop

        -- Alter columns
        v_alter := v_alter
                   || case when v_att_params.attstorage != v_att_params.typstorage
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET STORAGE '
                                || case v_att_params.attstorage when 'm' then 'MAIN,'
                                                                when 'p' then 'PLAIN,'
                                                                when 'e' then 'EXTERNAL,'
                                                                when 'x' then 'EXTENDED,'
                                   else '' end
                      else '' end
                   || case when v_att_params.attstorage in ('m', 'x')
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET COMPRESSION '
                                || case when v_att_params.attcompression = 'l' then 'lz4,'
                                        when v_att_params.attcompression = 'p' then 'pglz,'
                                   else 'default,' end
                      else '' end
                   || case when v_att_params.attoptions is not null
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET (' || v_att_params.attoptions || '),'
                      else '' end
                   || case when v_att_params.attstattarget != -1
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET STATISTICS ' || v_att_params.attstattarget || ','
                      else '' end;

      end loop;

      -- Clustering
      select E'\n    CLUSTER ON ' || quote_ident(ci.relname)
      into v_alter_cl
      from pg_catalog.pg_index i
      join pg_catalog.pg_class ci on i.indexrelid = ci."oid"
      join pg_catalog.pg_class c on i.indrelid = c."oid"
      where c."oid" = v_view_params."oid"
            and i.indisclustered is true;

      if not found then
        v_alter_cl := '';
      end if;

    end if;

    -- Finalizing ALTER command
    v_alter := rtrim(v_alter || v_alter_cl, ',');

    -- Building DDL

    v_view_ddl := 'CREATE '
                  || case when v_view_params.relkind = 'm' then 'MATERIALIZED VIEW IF NOT EXISTS '
                          when v_view_params.relkind = 'v' then 'OR REPLACE '
                                || case when v_view_params.relpersistence = 't' then 'TEMPORARY ' else '' end
                                || 'VIEW '
                     end
                  || quote_ident(v_view_params.nspname) || '.' || quote_ident(in_view)
                  || E'\n(\n    ' || v_columns || E'\n)'
                  || v_am
                  || v_view_opts
                  || v_tablespace
                  || E'\nAS (\n' || rtrim(pg_catalog.pg_get_viewdef(v_view_params."oid", true), ';')
                  || case when v_view_params.relkind = 'm' then
                               case when v_view_params.relispopulated is true then E'\n)\nWITH DATA;'
                                    else E'\n)\nWITH NO DATA;' end
                          else E'\n);' end

                  || case when v_alter != '' and v_view_params.relkind = 'm'
                               then E'\n\nALTER MATERIALIZED VIEW IF EXISTS ' || quote_ident(v_view_params.nspname) || '.' || quote_ident(in_view)
                                    || v_alter || ';'
                          when v_alter != '' and v_view_params.relkind = 'v'
                               then E'\n\nALTER VIEW IF EXISTS ' || quote_ident(v_view_params.nspname) || '.' || quote_ident(in_view)
                                    || v_alter || ';'
                     else '' end;

    return v_view_ddl;

  end;

$$;

comment on function get_view_def(in_view text, in_schema text)
    is 'Generates CREATE command for a view / materialized view by its name and schema (default - search_path)';


-- OID

create or replace function get_view_def
  (
    view_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Generates DDL script for views and materialized views

-- Examples:

-- select get_view_def('pg_settings'::regclass);

-- select get_view_def("oid")
-- from pg_catalog.pg_class
-- where relkind in ('v', 'm')
-- limit 10;

  declare

    v_view_ddl text;
    v_view_params record;
    v_columns text;
    v_view_opts text;

    v_am text := '';
    v_tablespace text := '';
    v_att_params record;
    v_alter text := '';
    v_alter_cl text := '';

  begin

    -- Checking existence of view
    select c.relname,
           c.relam,
           c.reltablespace,
           c.relpersistence,
           c.relkind,
           c.relispopulated,
           c.reloptions,
           n.nspname
    into v_view_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
    where c.relkind in ('v', 'm')
          and c."oid" = view_oid;

    if v_view_params.relname is null then
      raise warning 'View / materialized view with OID % does not exist', view_oid;

      return null;
    end if;

    -- View attributes
    select string_agg(attname, E',\n    ' order by attnum)
    into v_columns
    from pg_catalog.pg_attribute
    where attrelid = view_oid
          and attnum > 0;

    -- View and storage options
    if v_view_params.reloptions is not null then
      select E'\nWITH (' || string_agg(replace(str, '=', ' = '), ', ' order by rn) || ')'
      into v_view_opts
      from unnest(v_view_params.reloptions) with ordinality a (str, rn);
    else
      v_view_opts := '';
    end if;


    -- Regular views-specific options
    if v_view_params.relkind = 'v' then

      for v_att_params in

        select a.attname,
               ad.adbin
        from pg_catalog.pg_attribute a
        join pg_catalog.pg_attrdef ad on ad.adrelid = a.attrelid and ad.adnum = a.attnum
        where a.atthasdef is true
              and a.attisdropped is false
              and a.attrelid = view_oid
        order by a.attnum

      loop

        -- Alter columns
        v_alter := v_alter
                   || case when v_att_params.adbin is not null
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname)
                                || ' SET DEFAULT (' || pg_catalog.pg_get_expr(v_att_params.adbin, 0, true) || '),'
                      else '' end;

      end loop;


    -- Materialized views-specific options
    elsif v_view_params.relkind = 'm' then

      -- Access method
      select E'\nUSING ' || quote_ident(a.amname)
      into v_am
      from pg_catalog.pg_am a
      where a."oid" = v_view_params.relam;

      -- Tablespace
      if v_view_params.reltablespace = 0::oid then
        v_tablespace := E'\nTABLESPACE pg_default';
      else
        select E'\nTABLESPACE ' || quote_ident(spcname)
        into v_tablespace
        from pg_catalog.pg_tablespace
        where "oid" = v_view_params.reltablespace;
      end if;

      -- Attribute parameters
      for v_att_params in

        select a.attname,
               a.attstattarget,
               a.attstorage,
               a.attcompression,
               string_agg(replace(ao.opts, '=', ' = '), ', ' order by ao.rn) as attoptions,
               t.typstorage
        from pg_catalog.pg_attribute a
        join pg_catalog.pg_type t on t."oid" = a.atttypid
        left join unnest(a.attoptions) with ordinality ao (opts, rn) on 1 = 1
        where a.attrelid = view_oid
              and a.attnum > 0
              and a.attisdropped is false
        group by a.attnum,
                 a.attname,
                 a.attstattarget,
                 a.attstorage,
                 a.attcompression,
                 t.typstorage
        order by a.attnum

      loop

        -- Alter columns
        v_alter := v_alter
                   || case when v_att_params.attstorage != v_att_params.typstorage
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET STORAGE '
                                || case v_att_params.attstorage when 'm' then 'MAIN,'
                                                                when 'p' then 'PLAIN,'
                                                                when 'e' then 'EXTERNAL,'
                                                                when 'x' then 'EXTENDED,'
                                   else '' end
                      else '' end
                   || case when v_att_params.attstorage in ('m', 'x')
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET COMPRESSION '
                                || case when v_att_params.attcompression = 'l' then 'lz4,'
                                        when v_att_params.attcompression = 'p' then 'pglz,'
                                   else 'default,' end
                      else '' end
                   || case when v_att_params.attoptions is not null
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET (' || v_att_params.attoptions || '),'
                      else '' end
                   || case when v_att_params.attstattarget != -1
                           then E'\n    ALTER COLUMN ' || quote_ident(v_att_params.attname) || ' SET STATISTICS ' || v_att_params.attstattarget || ','
                      else '' end;

      end loop;

      -- Clustering
      select E'\n    CLUSTER ON ' || quote_ident(ci.relname)
      into v_alter_cl
      from pg_catalog.pg_index i
      join pg_catalog.pg_class ci on i.indexrelid = ci."oid"
      join pg_catalog.pg_class c on i.indrelid = c."oid"
      where c."oid" = view_oid
            and i.indisclustered is true;

      if not found then
        v_alter_cl := '';
      end if;

    end if;

    -- Finalizing ALTER command
    v_alter := rtrim(v_alter || v_alter_cl, ',');

    -- Building DDL

    v_view_ddl := 'CREATE '
                  || case when v_view_params.relkind = 'm' then 'MATERIALIZED VIEW IF NOT EXISTS '
                          when v_view_params.relkind = 'v' then 'OR REPLACE '
                                || case when v_view_params.relpersistence = 't' then 'TEMPORARY ' else '' end
                                || 'VIEW '
                     end
                  || quote_ident(v_view_params.nspname) || '.' || quote_ident(v_view_params.relname)
                  || E'\n(\n    ' || v_columns || E'\n)'
                  || v_am
                  || v_view_opts
                  || v_tablespace
                  || E'\nAS (\n' || rtrim(pg_catalog.pg_get_viewdef(view_oid, true), ';')
                  || case when v_view_params.relkind = 'm' then
                               case when v_view_params.relispopulated is true then E'\n)\nWITH DATA;'
                                    else E'\n)\nWITH NO DATA;' end
                          else E'\n);' end

                  || case when v_alter != '' and v_view_params.relkind = 'm'
                               then E'\n\nALTER MATERIALIZED VIEW IF EXISTS ' || quote_ident(v_view_params.nspname) || '.' || quote_ident(v_view_params.relname)
                                    || v_alter || ';'
                          when v_alter != '' and v_view_params.relkind = 'v'
                               then E'\n\nALTER VIEW IF EXISTS ' || quote_ident(v_view_params.nspname) || '.' || quote_ident(v_view_params.relname)
                                    || v_alter || ';'
                     else '' end;

    return v_view_ddl;

  end;

$$;

comment on function get_view_def(view_oid oid)
    is 'Generates CREATE command for a view / materialized view by its OID';