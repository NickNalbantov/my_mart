-- Examples:

-- select get_policy_def(p.polname, c.relname, n.nspname),
--        get_policy_def(p."oid")
-- from pg_catalog.pg_policy p
-- join pg_catalog.pg_class c on p.polrelid = c."oid"
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace;


-- Policy name + table name + table schema

create or replace function get_policy_def
  (
    in_pol text,
    in_tbl text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

  declare

    v_pol_ddl text;
    v_pol_params record;
    v_pol_roles text;

  begin

    -- Checking existence of policy
    select p."oid",
           n.nspname,
           p.polrelid,
           p.polcmd,
           p.polpermissive,
           p.polroles,
           p.polqual,
           p.polwithcheck
    into v_pol_params
    from pg_catalog.pg_policy p
    join pg_catalog.pg_class c on p.polrelid = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where p.polname = in_pol
          and c.relname = in_tbl
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_pol_params."oid" is null then
      raise warning 'Policy % for table %.% does not exist',
        quote_ident(in_pol),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tbl);

      return null;
    end if;

    -- List of roles
    if cardinality(v_pol_params.polroles) = 1 and v_pol_params.polroles[1] = 0::oid then
      v_pol_roles := 'public';
    else
      select string_agg(quote_ident(r.rolname), ', ')
      into v_pol_roles
      from unnest(v_pol_params.polroles) p(roleoid)
      join pg_catalog.pg_authid r on p.roleoid = r."oid";
    end if;

    -- Building DDL
    v_pol_ddl := 'CREATE POLICY ' || quote_ident(in_pol) || ' ON ' || quote_ident(v_pol_params.nspname) || '.' || quote_ident(in_tbl)
      || E'\n    AS '
      || case when v_pol_params.polpermissive is true then 'PERMISSIVE' else 'RESTRICTIVE' end
      || E'\n    FOR '
      || case v_pol_params.polcmd when 'r' then 'SELECT'
                                  when 'a' then 'INSERT'
                                  when 'w' then 'UPDATE'
                                  when 'd' then 'DELETE'
                                  when '*' then 'ALL' end
      || E'\n    TO ' || v_pol_roles
      || case when v_pol_params.polqual is not null
                then E'\n    USING (' || pg_catalog.pg_get_expr(v_pol_params.polqual, v_pol_params.polrelid, true) || ')'
         else '' end
      || case when v_pol_params.polwithcheck is not null
                then E'\n    WITH CHECK (' || pg_catalog.pg_get_expr(v_pol_params.polwithcheck, v_pol_params.polrelid, true) || ')'
         else '' end
      || ';';

    return v_pol_ddl;

  end;

$$;

comment on function get_policy_def(in_pol text, in_tbl text, in_schema text)
    is 'Generates CREATE command for a policy by its name and table''s name and schema (default - search_path)';


-- OID

create or replace function get_policy_def
  (
    pol_oid oid
  )
returns text
language plpgsql
strict
as
$$

  declare

    v_pol_ddl text;
    v_pol_params record;
    v_pol_table text;
    v_pol_roles text;

  begin

    -- Checking existence of policy
    select polname,
           polrelid,
           polcmd,
           polpermissive,
           polroles,
           polqual,
           polwithcheck
    into v_pol_params
    from pg_catalog.pg_policy
    where "oid" = pol_oid;

    if v_pol_params.polname is null then
      raise warning 'Policy with OID % does not exist', pol_oid;

      return null;
    end if;

    -- Table
    select quote_ident(n.nspname) || '.' || quote_ident(c.relname)
    into v_pol_table
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where c."oid" = v_pol_params.polrelid;

    -- List of roles
    if cardinality(v_pol_params.polroles) = 1 and v_pol_params.polroles[1] = 0::oid then
      v_pol_roles := 'public';
    else
      select string_agg(quote_ident(r.rolname), ', ')
      into v_pol_roles
      from unnest(v_pol_params.polroles) p(roleoid)
      join pg_catalog.pg_authid r on p.roleoid = r."oid";
    end if;

    -- Building DDL
    v_pol_ddl := 'CREATE POLICY ' || quote_ident(v_pol_params.polname) || ' ON ' || v_pol_table
      || E'\n    AS '
      || case when v_pol_params.polpermissive is true then 'PERMISSIVE' else 'RESTRICTIVE' end
      || E'\n    FOR '
      || case v_pol_params.polcmd when 'r' then 'SELECT'
                                  when 'a' then 'INSERT'
                                  when 'w' then 'UPDATE'
                                  when 'd' then 'DELETE'
                                  when '*' then 'ALL' end
      || E'\n    TO ' || v_pol_roles
      || case when v_pol_params.polqual is not null
                then E'\n    USING (' || pg_catalog.pg_get_expr(v_pol_params.polqual, v_pol_params.polrelid, true) || ')'
         else '' end
      || case when v_pol_params.polwithcheck is not null
                then E'\n    WITH CHECK (' || pg_catalog.pg_get_expr(v_pol_params.polwithcheck, v_pol_params.polrelid, true) || ')'
         else '' end
      || ';';

    return v_pol_ddl;

  end;

$$;

comment on function get_policy_def(pol_oid oid)
    is 'Generates CREATE command for a policy by its OID';