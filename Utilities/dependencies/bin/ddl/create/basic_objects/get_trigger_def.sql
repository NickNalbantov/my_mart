-- Examples:

-- select pg_catalog.pg_get_triggerdef(t."oid", true), -- comparison with built-in system function
--        get_trigger_def(t.tgname, c.relname, n.nspname),
--        get_trigger_def(t."oid")
-- from pg_catalog.pg_trigger t
-- join pg_catalog.pg_class c on t.tgrelid = c."oid"
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace;


-- Name

create or replace function get_trigger_def
  (
    in_trig text,
    in_parent text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Aside from generating trigger's DDL this function also checks current state of the trigger (enabled / disabled) and generates corrsponding ALTER TABLE script

  declare

    v_trig_ddl text;
    v_trig_params record;
    v_tg_func text;
    v_tg_when text;
    v_tg_action text;
    v_tg_level text;
    v_fk_ref text;

  begin

    -- Checking existence of trigger
    select t."oid",
           t.tgrelid,
           t.tgfoid,
           t.tgtype,
           t.tgenabled,
           t.tgconstrrelid,
           t.tgconstraint,
           t.tgdeferrable,
           t.tginitdeferred,
           t.tgnargs,
           t.tgattr,
           t.tgargs,
           t.tgqual,
           t.tgoldtable,
           t.tgnewtable,
           c.relkind,
           n.nspname
    into v_trig_params
    from pg_catalog.pg_trigger t
    join pg_catalog.pg_class c on t.tgrelid = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where t.tgname = in_trig
          and c.relname = in_parent
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_trig_params."oid" is null then
      raise warning 'Trigger % on %.% does not exist',
        quote_ident(in_trig),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_parent);

      return null;
    end if;

    -- Trigger function
    select quote_ident(n.nspname) || '.' || quote_ident(p.proname) || '('
           || case when v_trig_params.tgargs = ''::bytea then ''::text
                   else '''' || regexp_replace(replace(encode(v_trig_params.tgargs, 'escape'), '\000', ''', '''), ', ''$', '')
              end
           || ')'
    into v_tg_func
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    where p."oid" = v_trig_params.tgfoid;

    -- Firing conditions
    select case v_trig_params.tgtype & 66
            when 2 then 'BEFORE '
            when 64 then 'INSTEAD OF '
            else 'AFTER ' end
    into v_tg_when;

    select case v_trig_params.tgtype & 1
            when 1 then 'ROW'
            else 'STATEMENT' end
    into v_tg_level;

    select
      rtrim(
        case when t.tgtype & 4 = 4 then 'INSERT OR ' else '' end ||
        case when t.tgtype & 8 = 8 then 'DELETE OR ' else '' end ||
        replace(
                case when t.tgtype & 16 = 16
                          then 'UPDATE OF ' || coalesce(string_agg(quote_ident(a.attname), ', '), '0') || ' OR ' -- column can't be named as a digit
                     else '' end
                , ' OF 0', ''
               ) ||
        case when t.tgtype & 32 = 32 then 'TRUNCATE' else '' end
            , ' OR ')
    into v_tg_action
    from (
          select v_trig_params.tgrelid,
                 v_trig_params.tgtype,
                 v_trig_params.tgattr
         ) t
    left join lateral unnest(string_to_array(t.tgattr::text, ' ')::int2[]) ta (cn) on 1 = 1
    left join pg_catalog.pg_attribute a on a.attrelid = t.tgrelid
                                           and a.attnum = ta.cn
    group by t.tgtype;

    -- FK reference
    if v_trig_params.tgconstrrelid != 0 then
      select E'\n    FROM ' || quote_ident(n.nspname) || '.' || quote_ident(c.relname)
      into v_fk_ref
      from pg_catalog.pg_class c
      join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
      where v_trig_params.tgconstrrelid = c."oid";
    else
      v_fk_ref := '';
    end if;

   -- Building DDL

    v_trig_ddl := 'CREATE ' || case when v_trig_params.tgconstraint != 0 then 'CONSTRAINT ' else 'OR REPLACE ' end || 'TRIGGER ' || quote_ident(in_trig)
                  || E'\n    ' || v_tg_when || v_tg_action
                  || E'\n    ON ' || quote_ident(v_trig_params.nspname) || '.' || quote_ident(in_parent)
                  || v_fk_ref
                  || case when v_trig_params.tgconstraint = 0 then ''
                          else
                              case when v_trig_params.tgdeferrable is false then E'\n    NOT DEFERRABLE INITIALLY IMMEDIATE'
                                   else E'\n    DEFERRABLE ' ||
                                        case when v_trig_params.tginitdeferred is true then 'INITIALLY DEFERRED'
                                             else 'INITIALLY IMMEDIATE'
                                        end
                              end
                     end
                  || case when coalesce(v_trig_params.tgoldtable, v_trig_params.tgnewtable) is not null then
                        E'\n    REFERENCING' ||
                        case when v_trig_params.tgoldtable is not null then ' OLD TABLE AS ' || v_trig_params.tgoldtable else '' end ||
                        case when v_trig_params.tgnewtable is not null then ' NEW TABLE AS ' || v_trig_params.tgnewtable else '' end
                     else '' end
                  || E'\n    FOR EACH ' || v_tg_level
                  || case when v_trig_params.tgqual is not null then E'\n    WHEN (' || pg_catalog.pg_get_expr(v_trig_params.tgqual, v_trig_params."oid", true) || ')' else '' end
                  || E'\n    EXECUTE FUNCTION ' || v_tg_func
                  || ';' ||

                  -- State of the trigger
                  case when v_trig_params.relkind != 'v' then
                    E'\n\nALTER '
                    || case when v_trig_params.relkind = 'f' then 'FOREIGN ' else '' end
                    || 'TABLE IF EXISTS ' || quote_ident(v_trig_params.nspname) || '.' || quote_ident(in_parent)
                    || E'\n    '
                    || case v_trig_params.tgenabled when 'O' then 'ENABLE TRIGGER '
                                                    when 'R' then 'ENABLE REPLICA TRIGGER '
                                                    when 'A' then 'ENABLE ALWAYS TRIGGER '
                                                    when 'D' then 'DISABLE TRIGGER '
                       end
                    || quote_ident(in_trig)
                    || ';'
                  else '' end;

    return v_trig_ddl;

  end;

$$;

comment on function get_trigger_def(in_trig text, in_parent text, in_schema text)
    is 'Generates CREATE command for a trigger by its name, name of the parent object and its schema (default - search_path)';


-- OID

create or replace function get_trigger_def
  (
    trig_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Aside from generating trigger's DDL this function also checks current state of the trigger (enabled / disabled) and generates corrsponding ALTER TABLE script

-- Example:

-- select get_trigger_def("oid")
-- from pg_catalog.pg_trigger
-- limit 10;

  declare

    v_trig_ddl text;
    v_trig_params record;
    v_tg_func text;
    v_tg_when text;
    v_tg_action text;
    v_tg_level text;
    v_fk_ref text;

  begin

    -- Checking existence of trigger
    select t.tgname,
           t.tgrelid,
           t.tgfoid,
           t.tgtype,
           t.tgenabled,
           t.tgconstrrelid,
           t.tgconstraint,
           t.tgdeferrable,
           t.tginitdeferred,
           t.tgnargs,
           t.tgattr,
           t.tgargs,
           t.tgqual,
           t.tgoldtable,
           t.tgnewtable,
           c.relname,
           c.relkind,
           n.nspname
    into v_trig_params
    from pg_catalog.pg_trigger t
    join pg_catalog.pg_class c on t.tgrelid = c."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where t."oid" = trig_oid;

    if v_trig_params.tgname is null then
      raise warning 'Trigger with OID % does not exist', trig_oid;

      return null;
    end if;

    -- Trigger function
    select quote_ident(n.nspname) || '.' || quote_ident(p.proname) || '('
           || case when v_trig_params.tgargs = ''::bytea then ''::text
                   else '''' || regexp_replace(replace(encode(v_trig_params.tgargs, 'escape'), '\000', ''', '''), ', ''$', '')
              end
           || ')'
    into v_tg_func
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    where p."oid" = v_trig_params.tgfoid;

    -- Firing conditions
    select case v_trig_params.tgtype & 66
            when 2 then 'BEFORE '
            when 64 then 'INSTEAD OF '
            else 'AFTER ' end
    into v_tg_when;

    select case v_trig_params.tgtype & 1
            when 1 then 'ROW'
            else 'STATEMENT' end
    into v_tg_level;

    select
      rtrim(
        case when t.tgtype & 4 = 4 then 'INSERT OR ' else '' end ||
        case when t.tgtype & 8 = 8 then 'DELETE OR ' else '' end ||
        replace(
                case when t.tgtype & 16 = 16
                          then 'UPDATE OF ' || coalesce(string_agg(quote_ident(a.attname), ', '), '0') || ' OR ' -- column can't be named as a digit
                     else '' end
                , ' OF 0', ''
               ) ||
        case when t.tgtype & 32 = 32 then 'TRUNCATE' else '' end
            , ' OR ')
    into v_tg_action
    from (
          select v_trig_params.tgrelid,
                 v_trig_params.tgtype,
                 v_trig_params.tgattr
         ) t
    left join lateral unnest(string_to_array(t.tgattr::text, ' ')::int2[]) ta (cn) on 1 = 1
    left join pg_catalog.pg_attribute a on a.attrelid = t.tgrelid
                                           and a.attnum = ta.cn
    group by t.tgtype;

    -- FK reference
    if v_trig_params.tgconstrrelid != 0 then
      select E'\n    FROM ' || quote_ident(n.nspname) || '.' || quote_ident(c.relname)
      into v_fk_ref
      from pg_catalog.pg_class c
      join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
      where v_trig_params.tgconstrrelid = c."oid";
    else
      v_fk_ref := '';
    end if;

   -- Building DDL

    v_trig_ddl := 'CREATE ' || case when v_trig_params.tgconstraint != 0 then 'CONSTRAINT ' else 'OR REPLACE ' end || 'TRIGGER ' || quote_ident(v_trig_params.tgname)
                  || E'\n    ' || v_tg_when || v_tg_action
                  || E'\n    ON ' || quote_ident(v_trig_params.nspname) || '.' || quote_ident(v_trig_params.relname)
                  || v_fk_ref
                  || case when v_trig_params.tgconstraint = 0 then ''
                          else
                              case when v_trig_params.tgdeferrable is false then E'\n    NOT DEFERRABLE INITIALLY IMMEDIATE'
                                   else E'\n    DEFERRABLE ' ||
                                        case when v_trig_params.tginitdeferred is true then 'INITIALLY DEFERRED'
                                             else 'INITIALLY IMMEDIATE'
                                        end
                              end
                     end
                  || case when coalesce(v_trig_params.tgoldtable, v_trig_params.tgnewtable) is not null then
                        E'\n    REFERENCING' ||
                        case when v_trig_params.tgoldtable is not null then ' OLD TABLE AS ' || v_trig_params.tgoldtable else '' end ||
                        case when v_trig_params.tgnewtable is not null then ' NEW TABLE AS ' || v_trig_params.tgnewtable else '' end
                     else '' end
                  || E'\n    FOR EACH ' || v_tg_level
                  || case when v_trig_params.tgqual is not null then E'\n    WHEN (' || pg_catalog.pg_get_expr(v_trig_params.tgqual, trig_oid, true) || ')' else '' end
                  || E'\n    EXECUTE FUNCTION ' || v_tg_func
                  || ';' ||

                  -- State of the trigger
                  case when v_trig_params.relkind != 'v' then
                    E'\n\nALTER '
                    || case when v_trig_params.relkind = 'f' then 'FOREIGN ' else '' end
                    || 'TABLE IF EXISTS ' || quote_ident(v_trig_params.nspname) || '.' || quote_ident(v_trig_params.relname)
                    || E'\n    '
                    || case v_trig_params.tgenabled when 'O' then 'ENABLE TRIGGER '
                                                    when 'R' then 'ENABLE REPLICA TRIGGER '
                                                    when 'A' then 'ENABLE ALWAYS TRIGGER '
                                                    when 'D' then 'DISABLE TRIGGER '
                      end
                    || quote_ident(v_trig_params.tgname)
                    || ';'
                  else '' end;

    return v_trig_ddl;

  end;

$$;

comment on function get_trigger_def(trig_oid oid)
    is 'Generates CREATE command for a trigger by its OID';