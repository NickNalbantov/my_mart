-- Examples:

-- select get_schema_def(nspname),
--        get_schema_def("oid")
-- from pg_catalog.pg_namespace;


-- Name

create or replace function get_schema_def
  (
    in_schema text
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_schema_def(nspname)
-- from pg_catalog.pg_namespace
-- limit 10;

  declare

    v_ddl text;
    v_oid oid;
    v_owner text;

  begin

    -- Checking existence of schema
    select n."oid", u.rolname
    into v_oid, v_owner
    from pg_catalog.pg_namespace n
    join pg_catalog.pg_authid u on n.nspowner = u."oid"
    where n.nspname = in_schema;

    if v_oid is null then
      raise warning 'Schema % does not exist', quote_ident(in_schema);

      return null;
    end if;

    -- Building DDL

    v_ddl := 'CREATE SCHEMA IF NOT EXISTS ' || quote_ident(in_schema) || ' AUTHORIZATION ' || quote_ident(v_owner) || ';';

    return v_ddl;

  end;

$$;

comment on function get_schema_def(in_schema text)
    is 'Generates CREATE command for a schema by its name';


-- OID

create or replace function get_schema_def
  (
    schema_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_schema_def('pg_catalog'::regnamespace);

-- select get_schema_def("oid")
-- from pg_catalog.pg_namespace
-- limit 10;

  declare

    v_ddl text;
    v_name text;
    v_owner text;

  begin

    -- Checking existence of schema
    select n.nspname, u.rolname
    into v_name, v_owner
    from pg_catalog.pg_namespace n
    join pg_catalog.pg_authid u on n.nspowner = u."oid"
    where n."oid" = schema_oid;

    if v_name is null then
      raise warning 'Schema with OID % does not exist', schema_oid;

      return null;
    end if;

    -- Building DDL

    v_ddl := 'CREATE SCHEMA IF NOT EXISTS ' || quote_ident(v_name) || ' AUTHORIZATION ' || quote_ident(v_owner) || ';';

    return v_ddl;

  end;

$$;

comment on function get_schema_def(schema_oid oid)
    is 'Generates CREATE command for a schema by its OID';