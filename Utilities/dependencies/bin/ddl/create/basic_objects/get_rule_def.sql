-- Examples:

-- select get_rule_def(r.rulename, c.relname, n.nspname),
--        get_rule_def(r."oid")
-- from pg_catalog.pg_rewrite r
-- join pg_catalog.pg_class c on r.ev_class = c."oid"
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace;

-- select get_rule_def('_RETURN', 'pg_user');



-- Rule name + parent name + parent schema

create or replace function get_rule_def
  (
    in_rule text,
    in_parent text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- There are no tools to parse rule's pg_node_tree (and almost nothing to improve in output of system function except omitting "pg_catalog" and "public" schemas)
-- So here is only a wrapper to call it by rule name and parent's name and schema
-- Function also checks current state of the rule (enabled / disabled) and generates corrsponding ALTER TABLE script

-- Example:

-- select get_rule_def('_RETURN', 'pg_tables');

  declare

    qry text;
    v_rule_params record;

  begin

    -- Checking existence of rule
    select r."oid",
           r.ev_type,
           r.ev_enabled,
           c.relkind,
           n.nspname
    into v_rule_params
    from pg_catalog.pg_rewrite r
    join pg_catalog.pg_class c on r.ev_class = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where r.rulename = in_rule
          and c.relname = in_parent
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_rule_params."oid" is null then
      raise warning 'Rule % for table / view %.% does not exist',
        quote_ident(in_rule),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_parent);

      return null;
    end if;

    -- Building DDL

    qry := regexp_replace(pg_catalog.pg_get_ruledef(v_rule_params."oid", true), '^CREATE', 'CREATE OR REPLACE') ||

           -- State of the rule
           case when v_rule_params.relkind != 'v' and v_rule_params.ev_type != '1' then
              E'\n\nALTER TABLE IF EXISTS ' || quote_ident(v_rule_params.nspname) || '.' || quote_ident(in_parent)
              || E'\n    '
              || case v_rule_params.ev_enabled when 'O' then 'ENABLE RULE '
                                               when 'R' then 'ENABLE REPLICA RULE '
                                               when 'A' then 'ENABLE ALWAYS RULE '
                                               when 'D' then 'DISABLE RULE '
                 end
              || quote_ident(in_rule)
              || ';'
           else '' end;

    return qry;

  end;

$$;

comment on function get_rule_def(in_rule text, in_parent text, in_schema text)
    is 'Generates CREATE command for a rule by its name and its parent''s name and schema (default - search_path)';


-- Rule name + parent name + parent schema

create or replace function get_rule_def
  (
    rule_oid oid
  )
returns text
language plpgsql
as
$$

-- There are no tools to parse rule's pg_node_tree (and almost nothing to improve in output of system function except omitting "pg_catalog" and "public" schemas)
-- So here is only a wrapper to call it by rule name and parent's name and schema
-- Function also checks current state of the rule (enabled / disabled) and generates corrsponding ALTER TABLE script

-- Example:

-- select get_rule_def("oid")
-- from pg_catalog.pg_rewrite
-- limit 10;

  declare

    qry text;
    v_rule_params record;

  begin

    -- Checking existence of rule
    select r.rulename,
           r.ev_type,
           r.ev_enabled,
           c.relname,
           c.relkind,
           n.nspname
    into v_rule_params
    from pg_catalog.pg_rewrite r
    join pg_catalog.pg_class c on r.ev_class = c."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where r."oid" = rule_oid;

    if v_rule_params.rulename is null then
      raise warning 'Rule with OID % does not exist', rule_oid;

      return null;
    end if;

    -- Building DDL

    qry := regexp_replace(pg_catalog.pg_get_ruledef(rule_oid, true), '^CREATE', 'CREATE OR REPLACE') ||

           -- State of the rule
           case when v_rule_params.relkind != 'v' and v_rule_params.ev_type != '1' then
              E'\n\nALTER TABLE IF EXISTS ' || quote_ident(v_rule_params.nspname) || '.' || quote_ident(v_rule_params.relname)
              || E'\n    '
              || case v_rule_params.ev_enabled when 'O' then 'ENABLE RULE '
                                               when 'R' then 'ENABLE REPLICA RULE '
                                               when 'A' then 'ENABLE ALWAYS RULE '
                                               when 'D' then 'DISABLE RULE '
                 end
              || quote_ident(v_rule_params.rulename)
              || ';'
           else '' end;

    return qry;

  end;

$$;

comment on function get_rule_def(rule_oid oid)
    is 'Generates CREATE command for a rule by its OID';