-- Examples:

-- select 'CONSTRAINT ' || c.conname || ' ' || pg_catalog.pg_get_constraintdef(c."oid"), -- comparison with built-in system function
--        get_constraint_def(c.conname, coalesce(cl.relname, t.typname), n.nspname),
--        get_constraint_def(c.conname, coalesce(cl.relname, t.typname), n.nspname, true),
--        get_constraint_def(c."oid", true)
-- from pg_catalog.pg_constraint c
-- left join pg_catalog.pg_class cl on c.conrelid = cl."oid" and cl.relkind in ('r', 'p', 'f')
-- left join pg_catalog.pg_type t on c.contypid = t."oid" and t.typtype = 'd'
-- join pg_catalog.pg_namespace n on n."oid" = c.connamespace
-- where contype != 't';

-- select get_constraint_def('pg_attrdef_adrelid_adnum_index', 'pg_attrdef');


-- Name

create or replace function get_constraint_def
  (
    in_constr text,
    in_parent text,
    in_schema text default null,
    alter_ bool default false
  )
returns text
language plpgsql
as
$$

-- This function support all kind of table and domain constraints with full table constraint syntax (it differs from column constraint syntax)
-- For columns' default value "constraints" (like DEFAULT, GENERATED ALWAYS / BY DEFAULT) use get_column_defaults_def instead
  -- Domain default values are integrated into get_type_def functions

-- Example:
-- select get_constraint_def('pg_attrdef_adrelid_adnum_index', 'pg_attrdef');

  declare

    v_constr_ddl text;
    v_constr_params record;
    v_defer text;
    v_tblspc text;

    v_excl_params record;

    v_fk_local_keys text;
    v_fk_foreign_keys text;
    v_fk_on_delete_keys text;
    v_fk_reftable text;

    v_index_params record;
    v_index_columns record;
    v_indopts text;

    v_col_expr_arr text[];
    expr_parts int;
    check_start int;
    check_end int;
    check_expr text;
    v_col_expr_rec record;
    is_sql boolean := false;

  begin

    -- Checking existence of constraint
    select c."oid",
           n.nspname,
           c.contype,
           c.condeferrable,
           c.condeferred,
           c.convalidated,
           c.conrelid,
           c.contypid,
           c.conindid,
           c.confrelid,
           c.confupdtype,
           c.confdeltype,
           c.confmatchtype,
           c.connoinherit,
           c.conkey,
           c.confkey,
           c.confdelsetcols,
           c.conexclop,
           c.conbin,
           cl.relkind
    into v_constr_params
    from pg_catalog.pg_constraint c
    left join pg_catalog.pg_class cl on c.conrelid = cl."oid" and cl.relkind in ('r', 'p', 'f')
    left join pg_catalog.pg_type t on c.contypid = t."oid" and t.typtype = 'd'
    left join pg_catalog.pg_namespace n on n."oid" = c.connamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.conname = in_constr and n.nspname = coalesce(in_schema, s.sch)
          and coalesce(cl.relname, t.typname) = in_parent
          and c.contype != 't'
    order by s.rn
    limit 1;

    if v_constr_params."oid" is null then
      raise warning 'Constraint % for %.% does not exist',
        quote_ident(in_constr),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_parent);

      return null;
    end if;

    v_constr_ddl := 'CONSTRAINT ' || quote_ident(in_constr) || ' ';

    -- Deferrability

    if v_constr_params.condeferrable is false then
      v_defer := 'NOT DEFERRABLE INITIALLY IMMEDIATE';
    else
      if v_constr_params.condeferred is false then
        v_defer := 'DEFERRABLE INITIALLY IMMEDIATE';
      else
        v_defer := 'DEFERRABLE INITIALLY DEFERRED';
      end if;
    end if;

    -- Exclude constraint parameters
    if v_constr_params.contype = 'x' then
      select quote_ident(a.amname) as amname,
             array_agg(quote_ident(n.nspname) || '.' || o.oprname order by x.oprnum) as oprnames
      into v_excl_params
      from pg_catalog.pg_index i
      cross join unnest(v_constr_params.conexclop) with ordinality x (opr, oprnum)
      join pg_catalog.pg_operator o on o.oid = x.opr
      join pg_catalog.pg_namespace n on n."oid" = o.oprnamespace
      join pg_catalog.pg_opclass oc on oc.oid = any(i.indclass)
      join pg_catalog.pg_am a on a.oid = oc.opcmethod
      where v_constr_params.conindid = i.indexrelid
      group by a.amname;
    else
      select null::text as amname, null::text[] as oprnames
      into v_excl_params;
    end if;

    -- Index parameters

    if v_constr_params.contype in ('u', 'p', 'x') then

      select i.indexrelid,
             i.indrelid,
             i.indnullsnotdistinct,
             i.indnkeyatts,
             i.indkey,
             i.indcollation,
             i.indclass,
             i.indexprs,
             i.indpred,
             c.reltablespace,
             c.reloptions
      into v_index_params
      from pg_catalog.pg_index i
      join pg_catalog.pg_class c on i.indexrelid = c."oid"
      left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
      where v_constr_params.conindid = i.indexrelid;

      -- Index options

      if v_index_params.reloptions is not null then
          select 'WITH (' || string_agg(replace(str, '=', ' = '), ', ' order by rn) || ') '
          into v_indopts
          from unnest(v_index_params.reloptions) with ordinality a (str, rn);
      else
          v_indopts := '';
      end if;

      -- Index tablespace
      if v_index_params.reltablespace = 0 then
          v_tblspc := 'USING INDEX TABLESPACE pg_default';
      else
          select 'USING INDEX TABLESPACE ' || quote_ident(spcname)
          into v_tblspc
          from pg_catalog.pg_tablespace
          where v_index_params.reltablespace = "oid";
      end if;

      -- Getting an array of index keys (columns/expressions)
      select array_agg(quote_ident(a.attname) order by ik.cn)
      into v_col_expr_arr
      from unnest(v_index_params.indkey) with ordinality ik (colnum, cn)
      left join pg_catalog.pg_attribute a on v_index_params.indrelid = a.attrelid and a.attnum = ik.colnum
      where ik.cn <= v_index_params.indnkeyatts;

      -- Parsing expressions
        -- pg_get_expr(pg_index.indexprs, pg_index.indrelid) basically yields a string of key expressions delimited by ', ' (a proper array here would be nice, but pg_node_tree can't be parsed in other way)
        -- It brings a problem that if expression has ', ' inside we can't determine start and end of each expression
        -- So, code below splits pg_get_expr output into tokens by ', ' delimiter, then check each consequent combination as a valid SQL-expression
            -- Using is_sql function from https://github.com/rin-nas/postgresql-patterns-library/blob/master/functions/is_sql.sql
        -- Since no SQL-expression could end with trailing ', ' it gives us a proper delimitation between index key expressions

      if v_index_params.indexprs is not null then

        -- Checking count of parts between ', '
        select (length(expr) - length(replace(expr, ', ', ''))) / 2 + 1
        into expr_parts
        from
        (
          select pg_catalog.pg_get_expr(v_index_params.indexprs, v_index_params.indrelid, true) as expr
        ) e;

        -- Initial point
        check_start := 1;
        check_end := 1;

        -- Looping through all index key positions with SQL-expressions
        for v_col_expr_rec in
          select cn,
                 array_agg(split_part(index_expr, ', ', parts) order by cn, parts) as index_expr_split
          from
          (
            select ik.cn,
                   sp.parts,
                   pg_catalog.pg_get_expr(v_index_params.indexprs, v_index_params.indrelid, true) as index_expr
            from unnest(v_index_params.indkey) with ordinality ik (colnum, cn)
            cross join generate_series(1, expr_parts) sp (parts)
            where ik.colnum = 0
          ) a
          group by cn
          order by cn

        loop

          while is_sql is false
          loop

            -- If part_1 is not valid SQL expression, then it checks part_1 || ', ' || part_2, and so on until it gets valid SQL expression
            -- Valid SQL expression put into v_col_expr_arr at the corresponding key index
            -- Next cycle starts with part_n where n = *number of part that made previous expression valid* + 1

            check_expr := array_to_string(v_col_expr_rec.index_expr_split[check_start:check_end], ', ');

            select is_sql('select ' || check_expr || ' from ' || v_constr_params.nspname || '.' || in_parent || ';') as is_sql
            into is_sql;

            check_end := check_end + 1;

          end loop;

          check_start := check_end;
          is_sql := false;
          v_col_expr_arr[v_col_expr_rec.cn] := '(' || check_expr || ')';

        end loop;

      end if;

      -- Index columns
      select string_agg(key_columns, ', ' order by cn) as key_columns,
            'INCLUDE (' || string_agg(include_columns, ', ' order by cn) || ')' as include_columns
      into v_index_columns
      from
      (
           select ik.cn,
                  case when ik.cn <= v_index_params.indnkeyatts then
                          v_col_expr_arr[ik.cn]
                          || case when v_constr_params.contype = 'x'
                                       then ' ' || quote_ident(nopc.nspname) || '.' || quote_ident(opc.opcname) ||
                                            case pg_catalog.pg_indexam_has_property(opc.opcmethod, 'can_order')
                                                 when true then
                                                      ' ' || case pg_catalog.pg_index_column_has_property(v_index_params.indexrelid::regclass, ik.cn::int, 'asc')
                                                                  when true then 'ASC'
                                                                  when false then 'DESC'
                                                                  else '' end || ' '
                                                      ' ' || case pg_catalog.pg_index_column_has_property(v_index_params.indexrelid::regclass, ik.cn::int, 'nulls_last')
                                                                  when true then 'NULLS LAST'
                                                                  when false then 'NULLS FIRST'
                                                                  else '' end
                                                 else '' end
                                            || case when xop.oprname is not null then ' WITH ' || xop.oprname else '' end
                             else '' end
                       else null end as key_columns,
                  case when ik.cn > v_index_params.indnkeyatts then quote_ident(a.attname) else null end as include_columns
          from unnest(v_index_params.indkey) with ordinality ik (colnum, cn)

          left join unnest(v_index_params.indclass) with ordinality ioc (opclass, cn) on ik.cn = ioc.cn
          left join pg_catalog.pg_opclass opc on ioc.opclass = opc."oid"
          left join pg_catalog.pg_namespace nopc on nopc."oid" = opc.opcnamespace

          left join unnest(v_excl_params.oprnames) with ordinality xop (oprname, cn) on ik.cn = xop.cn

          left join pg_catalog.pg_attribute a on v_index_params.indrelid = a.attrelid and a.attnum = ik.colnum
      ) a;

    end if;

    -- CHECK constraints
    if v_constr_params.contype = 'c' then
      v_constr_ddl := v_constr_ddl || 'CHECK (' || pg_catalog.pg_get_expr(v_constr_params.conbin, v_constr_params.conrelid, true) ||')'
                      || case when v_constr_params.connoinherit is true then ' NO INHERIT' else '' end;

    -- UNIQUE constraints
    elsif v_constr_params.contype = 'u' then
      v_constr_ddl := v_constr_ddl || 'UNIQUE ' || case when v_index_params.indnullsnotdistinct is false then 'NULLS DISTINCT' else 'NULLS NOT DISTINCT' end
                      || ' (' || v_index_columns.key_columns || ') '
                      || case when v_index_columns.include_columns is not null then v_index_columns.include_columns || ' ' else '' end
                      || v_indopts
                      || v_tblspc || ' '
                      || v_defer;

    -- Primary keys
    elsif v_constr_params.contype = 'p' then
      v_constr_ddl := v_constr_ddl || 'PRIMARY KEY (' || v_index_columns.key_columns || ') '
                      || case when v_index_columns.include_columns is not null then v_index_columns.include_columns || ' ' else '' end
                      || v_indopts
                      || v_tblspc || ' '
                      || v_defer;

    -- EXCLUDE constraints
    elsif v_constr_params.contype = 'x' then
      v_constr_ddl := v_constr_ddl || 'EXCLUDE USING ' || v_excl_params.amname || ' (' || v_index_columns.key_columns || ') '
                      || case when v_index_columns.include_columns is not null then v_index_columns.include_columns || ' ' else '' end
                      || v_indopts
                      || v_tblspc
                      || case when v_index_params.indpred is not null then ' WHERE (' || pg_catalog.pg_get_expr(v_index_params.indpred, v_index_params.indrelid, true) || ') ' else ' ' end
                      || v_defer;

    -- Foreign keys
    elsif v_constr_params.contype = 'f' then

      -- Foreign key links

      select string_agg(quote_ident(al.attname), ', ' order by cl.rn)
      into v_fk_local_keys
      from unnest(v_constr_params.conkey) with ordinality as cl (conkey, rn)
      join pg_catalog.pg_attribute al on al.attrelid = v_constr_params.conrelid and al.attnum = cl.conkey;

      select string_agg(quote_ident(af.attname), ', ' order by cf.rn)
      into v_fk_foreign_keys
      from unnest(v_constr_params.confkey) with ordinality as cf (confkey, rn)
      join pg_catalog.pg_attribute af on af.attrelid = v_constr_params.confrelid and af.attnum = cf.confkey;

      select string_agg(quote_ident(ad.attname), ', ' order by cd.rn)
      into v_fk_on_delete_keys
      from unnest(v_constr_params.confdelsetcols) with ordinality as cd (confdelsetcols, rn)
      join pg_catalog.pg_attribute ad on ad.attrelid = v_constr_params.conrelid and ad.attnum = cd.confdelsetcols;

      select quote_ident(n.nspname) || '.' || quote_ident(c.relname)
      into v_fk_reftable
      from pg_catalog.pg_class c
      join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
      where c."oid" = v_constr_params.confrelid;

      -- FK definition

      v_constr_ddl := v_constr_ddl || 'FOREIGN KEY (' || v_fk_local_keys || ') REFERENCES ' || v_fk_reftable || ' (' || v_fk_foreign_keys || ') '
                      || case v_constr_params.confmatchtype when 'f' then 'MATCH FULL '
                                                            when 'p' then 'MATCH PARTIAL '
                                                            when 's' then 'MATCH SIMPLE '
                         end
                      || 'ON DELETE '
                      || case v_constr_params.confdeltype when 'a' then 'NO ACTION '
                                                          when 'r' then 'RESTRICT '
                                                          when 'c' then 'CASCADE '
                                                          when 'n' then 'SET NULL ' ||
                                                            case when v_fk_on_delete_keys is null then ''
                                                            else '(' || v_fk_on_delete_keys || ') ' end
                                                          when 'd' then 'SET DEFAULT ' ||
                                                            case when v_fk_on_delete_keys is null then ''
                                                            else '(' || v_fk_on_delete_keys || ') ' end
                         end
                      || 'ON UPDATE '
                      || case v_constr_params.confupdtype when 'a' then 'NO ACTION '
                                                          when 'r' then 'RESTRICT '
                                                          when 'c' then 'CASCADE '
                                                          when 'n' then 'SET NULL '
                                                          when 'd' then 'SET DEFAULT '
                         end
                      || v_defer;

    end if;

    -- Building ALTER...ADD CONSTRAINT command

    if alter_ is true then

      v_constr_ddl := 'ALTER '
                      || case
                          when v_constr_params.contypid != 0::oid then 'DOMAIN '
                          else
                            (
                              case when v_constr_params.relkind = 'f' then 'FOREIGN TABLE'
                                    when v_constr_params.relkind in ('r', 'p') then 'TABLE'
                              end || ' IF EXISTS '
                            )
                         end
                      || quote_ident(v_constr_params.nspname) || '.' || quote_ident(in_parent)
                      || E'\n    ADD ' || v_constr_ddl
                      || case when v_constr_params.convalidated is false then E'\n    NOT VALID' else '' end
                      || ';';
    end if;

    return v_constr_ddl;

  end;

$$;

comment on function get_constraint_def(in_constr text, in_parent text, in_schema text, alter_ bool)
    is 'Generates table/domain constraint definition by its name, name of the parent object and its schema (default - search_path); if alter_ := true (default := false) then generates ALTER TABLE/FOREIGN TABLE/DOMAIN...ADD CONSTRAINT... command for this contraint';


-- OID

create or replace function get_constraint_def
  (
    constr_oid oid,
    alter_ bool default false
  )
returns text
language plpgsql
strict
as
$$

-- This function support all kind of table and domain constraints with full table constraint syntax (it differs from column constraint syntax)
-- For columns' default value "constraints" (like DEFAULT, GENERATED ALWAYS / BY DEFAULT) use get_column_defaults_def instead
  -- Domain default values are integrated into get_type_def functions

-- Example:

-- select get_constraint_def("oid")
-- from pg_catalog.pg_constraint;

  declare

    v_constr_ddl text;
    v_constr_params record;
    v_defer text;
    v_tblspc text;

    v_excl_params record;

    v_fk_local_keys text;
    v_fk_foreign_keys text;
    v_fk_on_delete_keys text;
    v_fk_reftable text;

    v_index_params record;
    v_index_columns record;
    v_indopts text;

    v_col_expr_arr text[];
    expr_parts int;
    check_start int;
    check_end int;
    check_expr text;
    v_col_expr_rec record;
    is_sql boolean := false;

  begin

    -- Checking existence of constraint
    select n.nspname as parent_schema,
           coalesce(cl.relname, t.typname) as parent_obj,
           c.conname,
           c.contype,
           c.condeferrable,
           c.condeferred,
           c.convalidated,
           c.conrelid,
           c.contypid,
           c.conindid,
           c.confrelid,
           c.confupdtype,
           c.confdeltype,
           c.confmatchtype,
           c.connoinherit,
           c.conkey,
           c.confkey,
           c.confdelsetcols,
           c.conexclop,
           c.conbin,
           cl.relkind
    into v_constr_params
    from pg_catalog.pg_constraint c
    left join pg_catalog.pg_class cl on c.conrelid = cl."oid" and cl.relkind in ('r', 'p', 'f')
    left join pg_catalog.pg_type t on c.contypid = t."oid" and t.typtype = 'd'
    join pg_catalog.pg_namespace n on n."oid" = c.connamespace
    where c."oid" = constr_oid
          and c.contype != 't';

    if v_constr_params.conname is null then
      raise warning 'Constraint with OID % does not exist', constr_oid;

      return null;
    end if;

    v_constr_ddl := 'CONSTRAINT ' || quote_ident(v_constr_params.conname) || ' ';

    -- Deferrability

    if v_constr_params.condeferrable is false then
      v_defer := 'NOT DEFERRABLE INITIALLY IMMEDIATE';
    else
      if v_constr_params.condeferred is false then
        v_defer := 'DEFERRABLE INITIALLY IMMEDIATE';
      else
        v_defer := 'DEFERRABLE INITIALLY DEFERRED';
      end if;
    end if;

    -- Exclude constraint parameters
    if v_constr_params.contype = 'x' then
      select quote_ident(a.amname) as amname,
             array_agg(quote_ident(n.nspname) || '.' || o.oprname order by x.oprnum) as oprnames
      into v_excl_params
      from pg_catalog.pg_index i
      cross join unnest(v_constr_params.conexclop) with ordinality x (opr, oprnum)
      join pg_catalog.pg_operator o on o.oid = x.opr
      join pg_catalog.pg_namespace n on n."oid" = o.oprnamespace
      join pg_catalog.pg_opclass oc on oc.oid = any(i.indclass)
      join pg_catalog.pg_am a on a.oid = oc.opcmethod
      where v_constr_params.conindid = i.indexrelid
      group by a.amname;
    else
      select null::text as amname, null::text[] as oprnames
      into v_excl_params;
    end if;

    if v_constr_params.contype in ('u', 'p', 'x') then

      -- Index parameters

      select i.indexrelid,
             i.indrelid,
             i.indnullsnotdistinct,
             i.indnkeyatts,
             i.indkey,
             i.indcollation,
             i.indclass,
             i.indexprs,
             i.indpred,
             c.reltablespace,
             c.reloptions
      into v_index_params
      from pg_catalog.pg_index i
      join pg_catalog.pg_class c on i.indexrelid = c."oid"
      left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
      where v_constr_params.conindid = i.indexrelid;

      -- Index options

      if v_index_params.reloptions is not null then
          select 'WITH (' || string_agg(replace(str, '=', ' = '), ', ' order by rn) || ') '
          into v_indopts
          from unnest(v_index_params.reloptions) with ordinality a (str, rn);
      else
          v_indopts := '';
      end if;

      -- Index tablespace
      if v_index_params.reltablespace = 0 then
          v_tblspc := 'USING INDEX TABLESPACE pg_default';
      else
          select 'USING INDEX TABLESPACE ' || quote_ident(spcname)
          into v_tblspc
          from pg_catalog.pg_tablespace
          where v_index_params.reltablespace = "oid";
      end if;

      -- Getting an array of index keys (columns/expressions)
      select array_agg(quote_ident(a.attname) order by ik.cn)
      into v_col_expr_arr
      from unnest(v_index_params.indkey) with ordinality ik (colnum, cn)
      left join pg_catalog.pg_attribute a on v_index_params.indrelid = a.attrelid and a.attnum = ik.colnum
      where ik.cn <= v_index_params.indnkeyatts;

      -- Parsing expressions
        -- pg_get_expr(pg_index.indexprs, pg_index.indrelid) basically yields a string of key expressions delimited by ', ' (a proper array here would be nice, but pg_node_tree can't be parsed in other way)
        -- It brings a problem that if expression has ', ' inside we can't determine start and end of each expression
        -- So, code below splits pg_get_expr output into tokens by ', ' delimiter, then check each consequent combination as a valid SQL-expression
            -- Using is_sql function from https://github.com/rin-nas/postgresql-patterns-library/blob/master/functions/is_sql.sql
        -- Since no SQL-expression could end with trailing ', ' it gives us a proper delimitation between index key expressions

      if v_index_params.indexprs is not null then

        -- Checking count of parts between ', '
        select (length(expr) - length(replace(expr, ', ', ''))) / 2 + 1
        into expr_parts
        from
        (
          select pg_catalog.pg_get_expr(v_index_params.indexprs, v_index_params.indrelid, true) as expr
        ) e;

        -- Initial point
        check_start := 1;
        check_end := 1;

        -- Looping through all index key positions with SQL-expressions
        for v_col_expr_rec in
          select cn,
                 array_agg(split_part(index_expr, ', ', parts) order by cn, parts) as index_expr_split
          from
          (
            select ik.cn,
                   sp.parts,
                   pg_catalog.pg_get_expr(v_index_params.indexprs, v_index_params.indrelid, true) as index_expr
            from unnest(v_index_params.indkey) with ordinality ik (colnum, cn)
            cross join generate_series(1, expr_parts) sp (parts)
            where ik.colnum = 0
          ) a
          group by cn
          order by cn

        loop

          while is_sql is false
          loop

            -- If part_1 is not valid SQL expression, then it checks part_1 || ', ' || part_2, and so on until it gets valid SQL expression
            -- Valid SQL expression put into v_col_expr_arr at the corresponding key index
            -- Next cycle starts with part_n where n = *number of part that made previous expression valid* + 1

            check_expr := array_to_string(v_col_expr_rec.index_expr_split[check_start:check_end], ', ');

            select is_sql('select ' || check_expr || ' from ' || v_constr_params.parent_schema || '.' ||  v_constr_params.parent_obj || ';') as is_sql
            into is_sql;

            check_end := check_end + 1;

          end loop;

          check_start := check_end;
          is_sql := false;
          v_col_expr_arr[v_col_expr_rec.cn] := '(' || check_expr || ')';

        end loop;

      end if;

      -- Index columns
      select string_agg(key_columns, ', ' order by cn) as key_columns,
            'INCLUDE (' || string_agg(include_columns, ', ' order by cn) || ')' as include_columns
      into v_index_columns
      from
      (
          select ik.cn,
                 case when ik.cn <= v_index_params.indnkeyatts then
                          v_col_expr_arr[ik.cn]
                          || case when v_constr_params.contype = 'x'
                                       then ' ' || quote_ident(nopc.nspname) || '.' || quote_ident(opc.opcname) ||
                                            case pg_catalog.pg_indexam_has_property(opc.opcmethod, 'can_order')
                                                 when true then
                                                      ' ' || case pg_catalog.pg_index_column_has_property(v_index_params.indexrelid::regclass, ik.cn::int, 'asc')
                                                                  when true then 'ASC'
                                                                  when false then 'DESC'
                                                                  else '' end || ' '
                                                      ' ' || case pg_catalog.pg_index_column_has_property(v_index_params.indexrelid::regclass, ik.cn::int, 'nulls_last')
                                                                  when true then 'NULLS LAST'
                                                                  when false then 'NULLS FIRST'
                                                                  else '' end
                                                 else '' end
                                            || case when xop.oprname is not null then ' WITH ' || xop.oprname else '' end
                             else '' end
                      else null end as key_columns,
                 case when ik.cn > v_index_params.indnkeyatts then quote_ident(a.attname) else null end as include_columns
          from unnest(v_index_params.indkey) with ordinality ik (colnum, cn)

          left join unnest(v_index_params.indclass) with ordinality ioc (opclass, cn) on ik.cn = ioc.cn
          left join pg_catalog.pg_opclass opc on ioc.opclass = opc."oid"
          left join pg_catalog.pg_namespace nopc on nopc."oid" = opc.opcnamespace

          left join unnest(v_excl_params.oprnames) with ordinality xop (oprname, cn) on ik.cn = xop.cn

          left join pg_catalog.pg_attribute a on v_index_params.indrelid = a.attrelid and a.attnum = ik.colnum
      ) a;

    end if;

    -- CHECK constraints
    if v_constr_params.contype = 'c' then
      v_constr_ddl := v_constr_ddl || 'CHECK (' || pg_catalog.pg_get_expr(v_constr_params.conbin, v_constr_params.conrelid, true) ||')'
                      || case when v_constr_params.connoinherit is true then ' NO INHERIT' else '' end;

    -- UNIQUE constraints
    elsif v_constr_params.contype = 'u' then
      v_constr_ddl := v_constr_ddl || 'UNIQUE ' || case when v_index_params.indnullsnotdistinct is false then 'NULLS DISTINCT' else 'NULLS NOT DISTINCT' end
                      || ' (' || v_index_columns.key_columns || ') '
                      || case when v_index_columns.include_columns is not null then v_index_columns.include_columns || ' ' else '' end
                      || v_indopts
                      || v_tblspc || ' '
                      || v_defer;

    -- Primary keys
    elsif v_constr_params.contype = 'p' then
      v_constr_ddl := v_constr_ddl || 'PRIMARY KEY (' || v_index_columns.key_columns || ') '
                      || case when v_index_columns.include_columns is not null then v_index_columns.include_columns || ' ' else '' end
                      || v_indopts
                      || v_tblspc || ' '
                      || v_defer;

    -- EXCLUDE constraints
    elsif v_constr_params.contype = 'x' then
      v_constr_ddl := v_constr_ddl || 'EXCLUDE USING ' || v_excl_params.amname || ' (' || v_index_columns.key_columns || ') '
                      || case when v_index_columns.include_columns is not null then v_index_columns.include_columns || ' ' else '' end
                      || v_indopts
                      || v_tblspc
                      || case when v_index_params.indpred is not null then ' WHERE (' || pg_catalog.pg_get_expr(v_index_params.indpred, v_index_params.indrelid, true) || ') ' else ' ' end
                      || v_defer;

    -- Foreign keys
    elsif v_constr_params.contype = 'f' then

      -- Foreign key links

      select string_agg(quote_ident(al.attname), ', ' order by cl.rn)
      into v_fk_local_keys
      from unnest(v_constr_params.conkey) with ordinality as cl (conkey, rn)
      join pg_catalog.pg_attribute al on al.attrelid = v_constr_params.conrelid and al.attnum = cl.conkey;

      select string_agg(quote_ident(af.attname), ', ' order by cf.rn)
      into v_fk_foreign_keys
      from unnest(v_constr_params.confkey) with ordinality as cf (confkey, rn)
      join pg_catalog.pg_attribute af on af.attrelid = v_constr_params.confrelid and af.attnum = cf.confkey;

      select string_agg(quote_ident(ad.attname), ', ' order by cd.rn)
      into v_fk_on_delete_keys
      from unnest(v_constr_params.confdelsetcols) with ordinality as cd (confdelsetcols, rn)
      join pg_catalog.pg_attribute ad on ad.attrelid = v_constr_params.conrelid and ad.attnum = cd.confdelsetcols;

      select quote_ident(n.nspname) || '.' || quote_ident(c.relname)
      into v_fk_reftable
      from pg_catalog.pg_class c
      join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
      where c."oid" = v_constr_params.confrelid;

      -- FK definition

      v_constr_ddl := v_constr_ddl || 'FOREIGN KEY (' || v_fk_local_keys || ') REFERENCES ' || v_fk_reftable || ' (' || v_fk_foreign_keys || ') '
                      || case v_constr_params.confmatchtype when 'f' then 'MATCH FULL '
                                                            when 'p' then 'MATCH PARTIAL '
                                                            when 's' then 'MATCH SIMPLE '
                         end
                      || 'ON DELETE '
                      || case v_constr_params.confdeltype when 'a' then 'NO ACTION '
                                                          when 'r' then 'RESTRICT '
                                                          when 'c' then 'CASCADE '
                                                          when 'n' then 'SET NULL ' ||
                                                            case when v_fk_on_delete_keys is null then ''
                                                            else '(' || v_fk_on_delete_keys || ') ' end
                                                          when 'd' then 'SET DEFAULT ' ||
                                                            case when v_fk_on_delete_keys is null then ''
                                                            else '(' || v_fk_on_delete_keys || ') ' end
                         end
                      || 'ON UPDATE '
                      || case v_constr_params.confupdtype when 'a' then 'NO ACTION '
                                                          when 'r' then 'RESTRICT '
                                                          when 'c' then 'CASCADE '
                                                          when 'n' then 'SET NULL '
                                                          when 'd' then 'SET DEFAULT '
                         end
                      || v_defer;

    end if;

    -- Building ALTER...ADD CONSTRAINT command

    if alter_ is true then

      v_constr_ddl := 'ALTER '
                      || case
                          when v_constr_params.contypid != 0::oid then 'DOMAIN '
                          else
                            (
                              case when v_constr_params.relkind = 'f' then 'FOREIGN TABLE'
                                    when v_constr_params.relkind in ('r', 'p') then 'TABLE'
                              end || ' IF EXISTS '
                            )
                         end
                      || quote_ident(v_constr_params.parent_schema) || '.' || quote_ident(v_constr_params.parent_obj)
                      || E'\n    ADD ' || v_constr_ddl
                      || case when v_constr_params.convalidated is false then E'\n    NOT VALID' else '' end
                      || ';';

    end if;

    return v_constr_ddl;

  end;

$$;

comment on function get_constraint_def(constr_oid oid, alter_ bool)
    is 'Generates table/domain constraint definition by its OID; if alter_ := true (default := false) then generates ALTER TABLE/FOREIGN TABLE/DOMAIN...ADD CONSTRAINT... command for this contraint';