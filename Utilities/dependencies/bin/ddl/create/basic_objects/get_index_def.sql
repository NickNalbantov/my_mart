-- Examples:

-- select pg_catalog.pg_get_indexdef(c."oid"), -- comparison with built-in system function
--        get_index_def(c.relname, n.nspname),
--        get_index_def(c."oid")
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where c.relkind in ('i', 'I');

-- select get_index_def('pg_attrdef_adrelid_adnum_index');


-- Name

create or replace function get_index_def
  (
    in_index text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_index_def('pg_attrdef_adrelid_adnum_index');

-- select get_index_def(relname)
-- from pg_catalog.pg_class
-- where relkind in ('i', 'I')
--       and relnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_index_ddl text;
    v_index_params record;
    v_index_columns record;

    v_relopts text;
    v_opclass_opts text[];
    v_tblspc text;
    v_am text;

    v_alter text;

    v_col_expr_arr text[];
    expr_parts int;
    check_start int;
    check_end int;
    check_expr text;
    v_col_expr_rec record;
    is_sql boolean := false;

  begin

    -- Checking existence of index

    select i.indexrelid,
           i.indrelid,
           i.indisunique,
           i.indnullsnotdistinct,
           i.indnkeyatts,
           i.indkey,
           i.indcollation,
           i.indclass,
           i.indexprs,
           i.indpred,
           c.reltablespace,
           c.relam,
           c.relhassubclass as is_index_partitioned,
           c.reloptions,
           cr.relname as parent_name,
           cr.relhassubclass as is_table_partitioned,
           n.nspname
    into v_index_params
    from pg_catalog.pg_index i
    join pg_catalog.pg_class c on i.indexrelid = c."oid"
    join pg_catalog.pg_class cr on i.indrelid = cr."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where in_index = c.relname
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_index_params.indexrelid is null then
      raise warning 'Index % in schema % does not exist',
        quote_ident(in_index),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']');

      return null;
    end if;

    -- Storage options
    if v_index_params.reloptions is not null then
        select E'\nWITH (' || string_agg(replace(str, '=', ' = '), ', ' order by rn) || ')'
        into v_relopts
        from unnest(v_index_params.reloptions) with ordinality a (str, rn);
    else
        v_relopts := '';
    end if;

    -- Operator class-specific index column options
    select array_agg(opts order by attnum)
    into v_opclass_opts
    from
    (
      select a.attnum, coalesce(string_agg(replace(o.opts, '=', ' = '), ', ' order by o.rn), '') as opts

      from pg_catalog.pg_attribute a
      cross join lateral unnest(a.attoptions) with ordinality o (opts, rn)
      where a.attrelid = v_index_params.indexrelid
      group by a.attnum
    ) r;

    -- Tablespace
    if v_index_params.reltablespace = 0 then
        v_tblspc := E'\nTABLESPACE pg_default';
    else
        select E'\nTABLESPACE ' || quote_ident(spcname)
        into v_tblspc
        from pg_catalog.pg_tablespace
        where v_index_params.reltablespace = "oid";
    end if;

    -- Access method
    select E'\nUSING ' || quote_ident(amname)
    into v_am
    from pg_catalog.pg_am
    where v_index_params.relam = "oid";

    -- Getting an array of index keys (columns/expressions)
    select array_agg(quote_ident(a.attname) order by ik.cn)
    into v_col_expr_arr
    from unnest(v_index_params.indkey) with ordinality ik (colnum, cn)
    left join pg_catalog.pg_attribute a on v_index_params.indrelid = a.attrelid
                                           and a.attnum = ik.colnum
    where ik.cn <= v_index_params.indnkeyatts;

    -- Parsing expressions
      -- pg_get_expr(pg_index.indexprs, pg_index.indrelid) basically yields a string of key expressions delimited by ', ' (a proper array here would be nice, but pg_node_tree can't be parsed in other way)
      -- It brings a problem that if expression has ', ' inside we can't determine start and end of each expression
      -- So, code below splits pg_get_expr output into tokens by ', ' delimiter, then check each consequent combination as a valid SQL-expression
          -- Using is_sql function from https://github.com/rin-nas/postgresql-patterns-library/blob/master/functions/is_sql.sql
      -- Since no SQL-expression could end with trailing ', ' it gives us a proper delimitation between index key expressions

    if v_index_params.indexprs is not null then

      -- Checking count of parts between ', '
      select (length(expr) - length(replace(expr, ', ', ''))) / 2 + 1
      into expr_parts
      from
      (
        select pg_catalog.pg_get_expr(v_index_params.indexprs, v_index_params.indrelid, true) as expr
      ) e;

      -- Initial point
      check_start := 1;
      check_end := 1;

      -- Looping through all index key positions with SQL-expressions
      for v_col_expr_rec in
        select cn,
               array_agg(split_part(index_expr, ', ', parts) order by cn, parts) as index_expr_split
        from
        (
          select ik.cn,
                 sp.parts,
                 pg_catalog.pg_get_expr(v_index_params.indexprs, v_index_params.indrelid, true) as index_expr
          from unnest(v_index_params.indkey) with ordinality ik (colnum, cn)
          cross join generate_series(1, expr_parts) sp (parts)
          where ik.colnum = 0
        ) a
        group by cn
        order by cn

      loop

        while is_sql is false
        loop

          -- If part_1 is not valid SQL expression, then it checks part_1 || ', ' || part_2, and so on until it gets valid SQL expression
          -- Valid SQL expression put into v_col_expr_arr at the corresponding key index
          -- Next cycle starts with part_n where n = *number of part that made previous expression valid* + 1

          check_expr := array_to_string(v_col_expr_rec.index_expr_split[check_start:check_end], ', ');

          select is_sql('select ' || check_expr || ' from ' || v_index_params.nspname || '.' || v_index_params.parent_name || ';') as is_sql
          into is_sql;

          check_end := check_end + 1;

        end loop;

        check_start := check_end;
        is_sql := false;
        v_col_expr_arr[v_col_expr_rec.cn] := '(' || check_expr || ')';

      end loop;

    end if;

    -- Index columns
    select string_agg(key_columns, E',\n    ' order by cn) as key_columns,
           'INCLUDE (' || string_agg(include_columns, ', ' order by cn) || ')' as include_columns
    into v_index_columns
    from
    (
         select ik.cn,
                case when ik.cn <= v_index_params.indnkeyatts then
                        v_col_expr_arr[ik.cn]
                        || case when ic.coll = 0 then ' ' else ' COLLATE ' || quote_ident(ncol.nspname) || '.' || quote_ident(col.collname) || ' ' end
                        || quote_ident(nopc.nspname) || '.' || quote_ident(opc.opcname)
                        || case when coalesce(iao.opc_opts, '') = '' then '' else ' (' || iao.opc_opts || ')' end
                        || case pg_catalog.pg_indexam_has_property(opc.opcmethod, 'can_order')
                                when true then
                                    ' ' || case pg_catalog.pg_index_column_has_property(v_index_params.indexrelid::regclass, ik.cn::int, 'asc')
                                                when true then 'ASC'
                                                when false then 'DESC'
                                           else '' end ||
                                    ' ' || case pg_catalog.pg_index_column_has_property(v_index_params.indexrelid::regclass, ik.cn::int, 'nulls_last')
                                                when true then 'NULLS LAST'
                                                when false then 'NULLS FIRST'
                                           else '' end
                                else '' end
                     else null end as key_columns,
                case when ik.cn > v_index_params.indnkeyatts then quote_ident(a.attname) else null end as include_columns
         from unnest(v_index_params.indkey) with ordinality ik (colnum, cn)

         left join unnest(v_index_params.indcollation) with ordinality ic (coll, cn) on ik.cn = ic.cn
         left join pg_catalog.pg_collation col on ic.coll = col."oid"
         left join pg_catalog.pg_namespace ncol on ncol."oid" = col.collnamespace

         left join unnest(v_index_params.indclass) with ordinality ioc (opclass, cn) on ik.cn = ioc.cn
         left join pg_catalog.pg_opclass opc on ioc.opclass = opc."oid"
         left join pg_catalog.pg_namespace nopc on nopc."oid" = opc.opcnamespace

         left join unnest(v_opclass_opts) with ordinality iao (opc_opts, cn) on iao.cn = ik.cn

         left join pg_catalog.pg_attribute a on v_index_params.indrelid = a.attrelid and a.attnum = ik.colnum
    ) a;

    -- ALTER command
    if v_index_params.indexprs is not null then
      select E';\n\n' ||
             string_agg
              (
                'ALTER INDEX IF EXISTS ' || quote_ident(v_index_params.nspname) || '.' || quote_ident(in_index)
                || E'\n    ALTER COLUMN ' || attnum || ' SET STATISTICS ' || attstattarget || ';',
                E'\n\n' order by attnum
              )
      into v_alter
      from pg_catalog.pg_attribute
      where attrelid = v_index_params.indexrelid
            and attstattarget != -1;
    end if;

    -- Building DDL

    v_index_ddl := 'CREATE ' || case when v_index_params.indisunique is true then 'UNIQUE ' else '' end || 'INDEX IF NOT EXISTS ' || quote_ident(in_index) || ' ON '
                   || case when v_index_params.is_table_partitioned is true and v_index_params.is_index_partitioned is false then 'ONLY ' else '' end
                   || quote_ident(v_index_params.nspname) || '.' || quote_ident(v_index_params.parent_name) || v_am
                   || E'\n(\n    ' || v_index_columns.key_columns || E'\n)\n'
                   || case when v_index_columns.include_columns is not null then v_index_columns.include_columns || E'\n' else '' end
                   || case when v_index_params.indnullsnotdistinct is false then 'NULLS DISTINCT' else 'NULLS NOT DISTINCT' end
                   || v_relopts
                   || v_tblspc
                   || case when v_index_params.indpred is not null then E'\nWHERE (' || pg_catalog.pg_get_expr(v_index_params.indpred, v_index_params.indrelid, true) || ')' else '' end
                   || coalesce(v_alter, ';');

    return v_index_ddl;

  end;

$$;

comment on function get_index_def(in_index text, in_schema text)
    is 'Generates CREATE command for an index by its name and schema (default - search_path)';


-- OID

create or replace function get_index_def
  (
    index_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_index_def('pg_attrdef_adrelid_adnum_index'::regclass);

-- select get_index_def("oid")
-- from pg_catalog.pg_class
-- where relkind in ('i', 'I')
-- limit 10;

  declare

    v_index_ddl text;
    v_index_params record;
    v_index_columns record;

    v_relopts text;
    v_opclass_opts text[];
    v_tblspc text;
    v_am text;

    v_alter text;

    v_col_expr_arr text[];
    expr_parts int;
    check_start int;
    check_end int;
    check_expr text;
    v_col_expr_rec record;
    is_sql boolean := false;

  begin

    -- Checking existence of index

    select i.indrelid,
           i.indisunique,
           i.indnullsnotdistinct,
           i.indnkeyatts,
           i.indkey,
           i.indcollation,
           i.indclass,
           i.indexprs,
           i.indpred,
           c.relname as index_name,
           c.reltablespace,
           c.relam,
           c.relhassubclass as is_index_partitioned,
           c.reloptions,
           cr.relname as parent_name,
           cr.relhassubclass as is_table_partitioned,
           n.nspname
    into v_index_params
    from pg_catalog.pg_index i
    join pg_catalog.pg_class c on i.indexrelid = c."oid"
    join pg_catalog.pg_class cr on i.indrelid = cr."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where index_oid = i.indexrelid;

    if v_index_params.indrelid is null then
      raise warning 'Index with OID % does not exist', index_oid;

      return null;
    end if;

    -- Storage options
    if v_index_params.reloptions is not null then
        select E'\nWITH (' || string_agg(replace(str, '=', ' = '), ', ' order by rn) || ')'
        into v_relopts
        from unnest(v_index_params.reloptions) with ordinality a (str, rn);
    else
        v_relopts := '';
    end if;

    -- Operator class-specific index column options
    select array_agg(opts order by attnum)
    into v_opclass_opts
    from
    (
      select a.attnum, coalesce(string_agg(replace(o.opts, '=', ' = '), ', ' order by o.rn), '') as opts

      from pg_catalog.pg_attribute a
      cross join lateral unnest(a.attoptions) with ordinality o (opts, rn)
      where a.attrelid = index_oid
      group by a.attnum
    ) r;

    -- Tablespace
    if v_index_params.reltablespace = 0 then
        v_tblspc := E'\nTABLESPACE pg_default';
    else
        select E'\nTABLESPACE ' || quote_ident(spcname)
        into v_tblspc
        from pg_catalog.pg_tablespace
        where v_index_params.reltablespace = "oid";
    end if;

    -- Access method
    select E'\nUSING ' || quote_ident(amname)
    into v_am
    from pg_catalog.pg_am
    where v_index_params.relam = "oid";

    -- Getting an array of index keys (columns/expressions)
    select array_agg(quote_ident(a.attname) order by ik.cn)
    into v_col_expr_arr
    from unnest(v_index_params.indkey) with ordinality ik (colnum, cn)
    left join pg_catalog.pg_attribute a on v_index_params.indrelid = a.attrelid
                                           and a.attnum = ik.colnum
    where ik.cn <= v_index_params.indnkeyatts;

    -- Parsing expressions
      -- pg_get_expr(pg_index.indexprs, pg_index.indrelid) basically yields a string of key expressions delimited by ', ' (a proper array here would be nice, but pg_node_tree can't be parsed in other way)
      -- It brings a problem that if expression has ', ' inside we can't determine start and end of each expression
      -- So, code below splits pg_get_expr output into tokens by ', ' delimiter, then check each consequent combination as a valid SQL-expression
          -- Using is_sql function from https://github.com/rin-nas/postgresql-patterns-library/blob/master/functions/is_sql.sql
      -- Since no SQL-expression could end with trailing ', ' it gives us a proper delimitation between index key expressions

    if v_index_params.indexprs is not null then

      -- Checking count of parts between ', '
      select (length(expr) - length(replace(expr, ', ', ''))) / 2 + 1
      into expr_parts
      from
      (
        select pg_catalog.pg_get_expr(v_index_params.indexprs, v_index_params.indrelid, true) as expr
      ) e;

      -- Initial point
      check_start := 1;
      check_end := 1;

      -- Looping through all index key positions with SQL-expressions
      for v_col_expr_rec in
        select cn,
               array_agg(split_part(index_expr, ', ', parts) order by cn, parts) as index_expr_split
        from
        (
          select ik.cn,
                 sp.parts,
                 pg_catalog.pg_get_expr(v_index_params.indexprs, v_index_params.indrelid, true) as index_expr
          from unnest(v_index_params.indkey) with ordinality ik (colnum, cn)
          cross join generate_series(1, expr_parts) sp (parts)
          where ik.colnum = 0
        ) a
        group by cn
        order by cn

      loop

        while is_sql is false
        loop

          -- If part_1 is not valid SQL expression, then it checks part_1 || ', ' || part_2, and so on until it gets valid SQL expression
          -- Valid SQL expression put into v_col_expr_arr at the corresponding key index
          -- Next cycle starts with part_n where n = *number of part that made previous expression valid* + 1

          check_expr := array_to_string(v_col_expr_rec.index_expr_split[check_start:check_end], ', ');

          select is_sql('select ' || check_expr || ' from ' || v_index_params.nspname || '.' || v_index_params.parent_name || ';') as is_sql
          into is_sql;

          check_end := check_end + 1;

        end loop;

        check_start := check_end;
        is_sql := false;
        v_col_expr_arr[v_col_expr_rec.cn] := '(' || check_expr || ')';

      end loop;

    end if;

    -- Index columns
    select string_agg(key_columns, E',\n    ' order by cn) as key_columns,
           'INCLUDE (' || string_agg(include_columns, ', ' order by cn) || ')' as include_columns
    into v_index_columns
    from
    (
         select ik.cn,
                case when ik.cn <= v_index_params.indnkeyatts then
                        v_col_expr_arr[ik.cn]
                        || case when ic.coll = 0 then ' ' else ' COLLATE ' || quote_ident(ncol.nspname) || '.' || quote_ident(col.collname) || ' ' end
                        || quote_ident(nopc.nspname) || '.' || quote_ident(opc.opcname)
                        || case when coalesce(iao.opc_opts, '') = '' then '' else ' (' || iao.opc_opts || ')' end
                        || case pg_catalog.pg_indexam_has_property(opc.opcmethod, 'can_order')
                                when true then
                                     ' ' || case pg_catalog.pg_index_column_has_property(index_oid::regclass, ik.cn::int, 'asc')
                                                 when true then 'ASC'
                                                 when false then 'DESC'
                                            else '' end ||
                                     ' ' || case pg_catalog.pg_index_column_has_property(index_oid::regclass, ik.cn::int, 'nulls_last')
                                                 when true then 'NULLS LAST'
                                                 when false then 'NULLS FIRST'
                                            else '' end
                                else '' end
                     else null end as key_columns,
                case when ik.cn > v_index_params.indnkeyatts then quote_ident(a.attname) else null end as include_columns
         from unnest(v_index_params.indkey) with ordinality ik (colnum, cn)

         left join unnest(v_index_params.indcollation) with ordinality ic (coll, cn) on ik.cn = ic.cn
         left join pg_catalog.pg_collation col on ic.coll = col."oid"
         left join pg_catalog.pg_namespace ncol on ncol."oid" = col.collnamespace

         left join unnest(v_index_params.indclass) with ordinality ioc (opclass, cn) on ik.cn = ioc.cn
         left join pg_catalog.pg_opclass opc on ioc.opclass = opc."oid"
         left join pg_catalog.pg_namespace nopc on nopc."oid" = opc.opcnamespace

         left join unnest(v_opclass_opts) with ordinality iao (opc_opts, cn) on iao.cn = ik.cn

         left join pg_catalog.pg_attribute a on v_index_params.indrelid = a.attrelid and a.attnum = ik.colnum
    ) a;

    -- ALTER command
    if v_index_params.indexprs is not null then
      select E';\n\n' ||
             string_agg
              (
                'ALTER INDEX IF EXISTS ' || quote_ident(v_index_params.nspname) || '.' || quote_ident(v_index_params.index_name)
                || E'\n    ALTER COLUMN ' || attnum || ' SET STATISTICS ' || attstattarget || ';',
                E'\n\n' order by attnum
              )
      into v_alter
      from pg_catalog.pg_attribute
      where attrelid = index_oid
            and attstattarget != -1;
    end if;

    -- Building DDL

    v_index_ddl := 'CREATE ' || case when v_index_params.indisunique is true then 'UNIQUE ' else '' end || 'INDEX IF NOT EXISTS ' || quote_ident(v_index_params.index_name) || ' ON '
                   || case when v_index_params.is_table_partitioned is true and v_index_params.is_index_partitioned is false then 'ONLY ' else '' end
                   || quote_ident(v_index_params.nspname) || '.' || quote_ident(v_index_params.parent_name) || v_am
                   || E'\n(\n    ' || v_index_columns.key_columns || E'\n)\n'
                   || case when v_index_columns.include_columns is not null then v_index_columns.include_columns || E'\n' else '' end
                   || case when v_index_params.indnullsnotdistinct is false then 'NULLS DISTINCT' else 'NULLS NOT DISTINCT' end
                   || v_relopts
                   || v_tblspc
                   || case when v_index_params.indpred is not null then E'\nWHERE (' || pg_catalog.pg_get_expr(v_index_params.indpred, v_index_params.indrelid, true) || ')' else '' end
                   || coalesce(v_alter, ';');

    return v_index_ddl;

  end;

$$;

comment on function get_index_def(index_oid oid)
    is 'Generates CREATE command for an index by its OID';