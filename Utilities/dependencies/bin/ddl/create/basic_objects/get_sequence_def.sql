-- Examples:

-- select get_sequence_def(c.relname, n.nspname),
--        get_sequence_def(c."oid")
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
-- where c.relkind = 'S';


-- Name + schema

create or replace function get_sequence_def
  (
    in_seq text,
    in_schema text default null
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_sequence_def(c.relname, n.nspname)
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
-- where c.relkind = 'S'
-- limit 10;

  declare

    v_seq_ddl text;
    v_seq_oid oid;
    v_seq_owner text;
    v_seq_params record;
    v_persist text;
    v_schema text;

  begin

    -- Checking existence of sequence
    select c."oid",
           n.nspname,
           case relpersistence when 'u' then ' UNLOGGED ' when 't' then ' TEMPORARY ' else ' ' end
    into v_seq_oid,
         v_schema,
         v_persist
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.relkind = 'S'
          and c.relname = in_seq
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_seq_oid is null then
      raise warning 'Sequence %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_seq);

      return null;
    end if;

    -- Sequence owner
    select coalesce(quote_ident(tab.relname) || '.' || quote_ident(attr.attname), 'NONE')
    into v_seq_owner
    from pg_catalog.pg_class as seq
    join pg_catalog.pg_depend as dep on dep.objid in (seq.relfilenode, seq."oid")
    join pg_catalog.pg_class as tab on dep.refobjid = tab."oid"
    join pg_catalog.pg_attribute as attr on attr.attnum = dep.refobjsubid
                                            and attr.attrelid = dep.refobjid
    where seq."oid" = v_seq_oid
          and dep.refclassid = 'pg_catalog.pg_class'::regclass::oid;

    -- Parameters
    select c.seqrelid,
           quote_ident(n.nspname) || '.' || quote_ident(t.typname) as typname,
           c.seqstart,
           c.seqincrement,
           c.seqmax,
           c.seqmin,
           c.seqcache,
           c.seqcycle
    into v_seq_params
    from pg_catalog.pg_sequence c
    join pg_catalog.pg_type t on t."oid" = c.seqtypid
    join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    where c.seqrelid = v_seq_oid;

    -- Building DDL
    v_seq_ddl := 'CREATE' || v_persist || 'SEQUENCE IF NOT EXISTS ' || quote_ident(v_schema) || '.' || quote_ident(in_seq) || E'\n(\n    '
                 || 'AS ' || v_seq_params.typname || E'\n    '
                 || 'INCREMENT BY ' || v_seq_params.seqincrement || E'\n    '
                 || 'MINVALUE ' || v_seq_params.seqmin || E'\n    '
                 || 'MAXVALUE ' || v_seq_params.seqmax || E'\n    '
                 || 'START ' || v_seq_params.seqstart || E'\n    '
                 || 'CACHE ' || v_seq_params.seqcache || E'\n    '
                 || case when v_seq_params.seqcycle is false then 'NO CYCLE' else 'CYCLE' end
                 || case when v_seq_owner is not null then E'\n    OWNED BY ' || v_seq_owner else '' end
                 || E'\n);';

    return v_seq_ddl;

  end;

$$;

comment on function get_sequence_def(in_seq text, in_schema text)
    is 'Generates CREATE command for a sequence by its name and schema (default - search_path)';


-- OID

create or replace function get_sequence_def
  (
    seq_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_sequence_def("oid")
-- from pg_catalog.pg_class
-- where relkind = 'S'
-- limit 10;

  declare

    v_seq_ddl text;
    in_seq text;
    in_schema text;
    v_seq_owner text;
    v_seq_params record;
    v_persist text;

  begin

    -- Checking existence of sequence
    select c.relname,
           n.nspname,
           case c.relpersistence when 'u' then ' UNLOGGED ' when 't' then ' TEMPORARY ' else ' ' end
    into in_seq,
         in_schema,
         v_persist
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where c.relkind = 'S'
          and c."oid" = seq_oid;

    if in_seq is null then
      raise warning 'Sequence with OID % does not exist', seq_oid;

      return null;
    end if;

    -- Sequence owner
    select coalesce(quote_ident(tab.relname) || '.' || quote_ident(attr.attname), 'NONE')
    into v_seq_owner
    from pg_catalog.pg_class as seq
    join pg_catalog.pg_depend as dep on dep.objid in (seq.relfilenode, seq."oid")
    join pg_catalog.pg_class as tab on dep.refobjid = tab."oid"
    join pg_catalog.pg_attribute as attr on attr.attnum = dep.refobjsubid
                                            and attr.attrelid = dep.refobjid
    where seq."oid" = seq_oid
          and dep.refclassid = 'pg_catalog.pg_class'::regclass::oid;

    -- Parameters
    select c.seqrelid,
           quote_ident(n.nspname) || '.' || quote_ident(t.typname) as typname,
           c.seqstart,
           c.seqincrement,
           c.seqmax,
           c.seqmin,
           c.seqcache,
           c.seqcycle
    into v_seq_params
    from pg_catalog.pg_sequence c
    join pg_catalog.pg_type t on t."oid" = c.seqtypid
    join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    where c.seqrelid = seq_oid;

    -- Building DDL
    v_seq_ddl := 'CREATE' || v_persist || 'SEQUENCE IF NOT EXISTS ' || quote_ident(in_schema) || '.' || quote_ident(in_seq) || E'\n(\n    '
                 || 'AS ' || v_seq_params.typname || E'\n    '
                 || 'INCREMENT BY ' || v_seq_params.seqincrement || E'\n    '
                 || 'MINVALUE ' || v_seq_params.seqmin || E'\n    '
                 || 'MAXVALUE ' || v_seq_params.seqmax || E'\n    '
                 || 'START ' || v_seq_params.seqstart || E'\n    '
                 || 'CACHE ' || v_seq_params.seqcache || E'\n    '
                 || case when v_seq_params.seqcycle is false then 'NO CYCLE' else 'CYCLE' end
                 || case when v_seq_owner is not null then E'\n    OWNED BY ' || v_seq_owner else '' end
                 || E'\n);';

    return v_seq_ddl;

  end;

$$;

comment on function get_sequence_def(seq_oid oid)
    is 'Generates CREATE command for a sequence by its OID';