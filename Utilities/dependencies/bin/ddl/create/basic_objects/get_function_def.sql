-- Examples:

-- select pg_catalog.pg_get_functiondef('version'::regproc), -- comparison with built-in system function
--        get_function_def('version')
-- union all
-- select pg_catalog.pg_get_functiondef('substring(text, int4)'::regprocedure), -- comparison with built-in system function
--        get_function_def('substring', array['text', 'int'])
-- union all
-- select pg_catalog.pg_get_functiondef('get_function_def(text, text[], text, text[])'::regprocedure), -- comparison with built-in system function
--        get_function_def('get_function_def', array['text', 'text[]', 'text', 'text[]'])
-- union all
-- select pg_catalog.pg_get_functiondef('get_function_def(oid)'::regprocedure), -- comparison with built-in system function
--        get_function_def('get_function_def', array['oid']);


-- select pg_catalog.pg_get_functiondef("oid"), -- comparison with built-in system function
--        get_function_def(proname, typnames, nspname, typschemas),
--        get_function_def("oid")
-- from
-- (
--     select p."oid",
--            p.proname,
--            n.nspname,
--            case when array_agg(t.typname) = array[null]::name[] then null::text[] else array_agg(t.typname order by pat.rn) end as typnames,
--            case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else array_agg(nt.nspname order by pat.rn) end as typschemas
--     from pg_catalog.pg_proc p
--     join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
--     cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
--     left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
--     left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
--     where prokind != 'a'
--     group by p."oid",
--              p.proname,
--              n.nspname
-- ) a;


-- Name + schema + signature arguments

create or replace function get_function_def
  (
    in_proc text,
    in_arg_types text[] default null,
    in_schema text default null,
    in_arg_type_schemas text[] default null
  )
returns text
language plpgsql
as
$get_function_def$

-- Generates definition for all functions and procedures, but not for aggregates - use get_aggregate_def instead

-- Example:

-- select get_function_def('version'),
--        get_function_def('substring', array['text', 'int']);

  declare

    v_proc_ddl text;
    v_proc_params record;
    arg_list text := ''::text;
    v_return_type text := ''::text;
    v_args_rec record;
    v_lang text;
    v_transform text;
    v_set_conf text;

    in_arg_types_fixed text[];
    in_arg_types_oids oid[];
    v_typoids_rec record;

    input_args_excptn text;

    v_def_expr_arr text[];
    expr_parts int;
    check_start int;
    check_end int;
    check_expr text;
    v_def_expr_split text[];
    is_sql boolean := false;

    int_syn text[] := array['integer', 'int'];
    smallint_syn text := 'smallint';
    bigint_syn text := 'bigint';
    num_syn text := 'decimal';
    real_syn text := 'real';
    dp_syn text[] := array['double precision', 'float'];
    time_syn text := 'time without time zone';
    timetz_syn text := 'time with time zone';
    timestamp_syn text := 'timestamp without time zone';
    timestamptz_syn text := 'timestamp with time zone';
    bpchar_syn text := 'character';
    varchar_syn text[] := array['character varying', 'char varying'];
    varbit_syn text := 'bit varying';
    bool_syn text := 'boolean';

  begin

    -- Comparing in_arg_types and in_arg_type_schemas input arguments
    if coalesce(cardinality(in_arg_type_schemas), 0) < coalesce(cardinality(in_arg_types), 0) then
      for in_card in 1..coalesce(cardinality(in_arg_types), 0) - coalesce(cardinality(in_arg_type_schemas), 0)
      loop
        in_arg_type_schemas := array_append(in_arg_type_schemas, null);
      end loop;
    elsif coalesce(cardinality(in_arg_type_schemas), 0) > coalesce(cardinality(in_arg_types), 0) then
      raise warning 'There are more types'' schemas specified in in_arg_type_schemas argument than types'' names specified in in_arg_types argument';

      return null;
    end if;

    -- Catching synonyms for arguments' types
    select array_agg(case when right(tps_sw, 2) = '[]' then rtrim('_' || tps_sw, '[]')
                          else tps_sw end
                     order by rn
                    )
    into in_arg_types_fixed
    from
    (
      select rn,
        case when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = any(int_syn) then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1int4\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = smallint_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1int2\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = bigint_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1int8\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = num_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1numeric\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = real_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1float4\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = any(dp_syn) then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1float8\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = time_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1time\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = timetz_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1timetz\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = timestamp_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1timestamp\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = timestamptz_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1timestamptz\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = bpchar_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1bpchar\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = any(varchar_syn) then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1varchar\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = varbit_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1varbit\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = bool_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1bool\2')
        else tps end as tps_sw
      from
        (
          select tps,
                 rn
          from unnest(coalesce(in_arg_types, array['']::text[])) with ordinality u (tps, rn)
        ) a
    ) b;

    -- Getting OIDs for signature arguments
    if in_arg_types is null then
      in_arg_types_oids := array[0]::oid[];

    else
      for v_typoids_rec in
          select t."oid",
                 tf.rn
          from pg_catalog.pg_type t
          join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
          join unnest(in_arg_types_fixed) with ordinality tf (tps, rn) on tf.tps = t.typname
          join unnest(in_arg_type_schemas) with ordinality ts (arg_sch, rn) on tf.rn = ts.rn
          left join lateral unnest(current_schemas(true)) with ordinality s (sch, rn) on 1 = 1
          where coalesce(ts.arg_sch, s.sch) = n.nspname
          group by t."oid",
                   tf.rn
          order by tf.rn
      loop
        in_arg_types_oids[v_typoids_rec.rn] := v_typoids_rec."oid";
      end loop;

      in_arg_types_oids := array_replace(in_arg_types_oids, null, 0::oid);

    end if;

    -- Checking existence of function
    select b."oid",
           b.prolang,
           b.procost,
           b.prorows,
           b.prosupport,
           b.prokind,
           b.prosecdef,
           b.proleakproof,
           b.proisstrict,
           b.proretset,
           b.provolatile,
           b.proparallel,
           b.pronargs,
           b.pronargdefaults,
           b.prorettype,
           b.proargdefaults,
           b.protrftypes,
           b.prosrc,
           b.probin,
           b.prosqlbody,
           b.proconfig,
           jsonb_object_agg(b.rn,
                            jsonb_build_object(
                                               'argtypes', b.argtypes,
                                               'argmodes', b.argmodes,
                                               'argnames', b.argnames,
                                               'argdefaults', case when b.rn > (b.pronargs - b.pronargdefaults) and b.rn <= b.pronargs
                                                                        then 'DEFAULT '
                                                              else null end
                                              )
                           ) as arg_params,
           max(b.rn) as arg_quantity,
           n.nspname
    into v_proc_params
    from
    (
        select a."oid",
               a.pronamespace,
               a.prolang,
               a.procost,
               a.prorows,
               a.prosupport,
               a.prokind,
               a.prosecdef,
               a.proleakproof,
               a.proisstrict,
               a.proretset,
               a.provolatile,
               a.proparallel,
               a.pronargs,
               a.pronargdefaults,
               a.prorettype,
               a.proargdefaults,
               a.protrftypes,
               a.prosrc,
               a.probin,
               a.prosqlbody,
               a.proconfig,
               quote_ident(nt.nspname) || '.' || quote_ident(t.typname) as argtypes,
               (case a.argmodes when 'o' then 'OUT'
                                when 'b' then 'INOUT'
                                when 'v' then 'VARIADIC'
                                when 't' then 'TABLE'
                else 'IN' end
               )::text as argmodes,
               a.argnames,
               row_number() over() as rn
        from
        (
            select p."oid",
                   p.pronamespace,
                   p.prolang,
                   p.procost,
                   p.prorows,
                   p.prosupport,
                   p.prokind,
                   p.prosecdef,
                   p.proleakproof,
                   p.proisstrict,
                   p.proretset,
                   p.provolatile,
                   p.proparallel,
                   p.pronargs,
                   p.pronargdefaults,
                   p.prorettype,
                   unnest(string_to_array(case when p.proargtypes = ''::oidvector then null else p.proargtypes::text end, ' ')::oid[]) as in_argtypes,
                   unnest(coalesce(p.proallargtypes, array[0]::oid[])) as argtypes,
                   unnest(p.proargmodes) as argmodes,
                   unnest(p.proargnames) as argnames,
                   p.proargdefaults,
                   p.protrftypes,
                   p.prosrc,
                   p.probin,
                   p.prosqlbody,
                   p.proconfig
            from pg_catalog.pg_proc p
            where p.prokind != 'a'
                  and p.proname = in_proc
                  and string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[] = in_arg_types_oids
        ) a
        left join pg_catalog.pg_type t on coalesce(a.in_argtypes, a.argtypes) = t."oid"
        left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    ) b
    left join pg_catalog.pg_namespace n on n."oid" = b.pronamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where n.nspname = coalesce(in_schema, s.sch)
    group by b."oid",
             b.prolang,
             b.procost,
             b.prorows,
             b.prosupport,
             b.prokind,
             b.prosecdef,
             b.proleakproof,
             b.proisstrict,
             b.proretset,
             b.provolatile,
             b.proparallel,
             b.pronargs,
             b.pronargdefaults,
             b.prorettype,
             b.proargdefaults,
             b.protrftypes,
             b.prosrc,
             b.probin,
             b.prosqlbody,
             b.proconfig,
             n.nspname,
             s.rn
    order by s.rn
    limit 1;

    if v_proc_params."oid" is null then

      select string_agg(coalesce(quote_ident(ts.arg_sch), '[' || array_to_string(current_schemas(true), ', ') || ']') || '.' || quote_ident(tf.tps), ', ' order by ts.rn)
      into input_args_excptn
      from unnest(in_arg_type_schemas) with ordinality ts (arg_sch, rn)
      join unnest(in_arg_types_fixed) with ordinality tf (tps, rn) on tf.rn = ts.rn;

      raise warning 'Function / procedure %.%(%) does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_proc),
        input_args_excptn;

      return null;
    end if;

    -- Parsing expressions
      -- pg_get_expr(pg_proc.proargdefaults, 0) basically yields a string of key expressions delimited by ', ' (a proper array here would be nice, but pg_node_tree can't be parsed in other way)
      -- It brings a problem that if expression has ', ' inside we can't determine start and end of each expression
      -- So, code below splits pg_get_expr output into tokens by ', ' delimiter, then check each consequent combination as a valid SQL-expression
          -- Using is_sql function from https://github.com/rin-nas/postgresql-patterns-library/blob/master/functions/is_sql.sql
      -- Since no SQL-expression could end with trailing ', ' it gives us a proper delimitation between default expressions

    if v_proc_params.proargdefaults is not null then

      -- Checking count of parts between ', '
      select (length(expr) - length(replace(expr, ', ', ''))) / 2 + 1
      into expr_parts
      from
      (
        select pg_catalog.pg_get_expr(v_proc_params.proargdefaults, 0, true) as expr
      ) e;

      -- Initial point
      check_start := 1;
      check_end := 1;

      -- Splitted expressions
      select array_agg(split_part(def_expr, ', ', parts) order by parts)
      into v_def_expr_split
      from
      (
        select sp.parts,
               pg_catalog.pg_get_expr(v_proc_params.proargdefaults, 0, true) as def_expr
        from generate_series(1, expr_parts) sp (parts)
      ) a;

      -- Looping through all available SQL-expressions
      for i in 1..expr_parts
      loop

        while is_sql is false
        loop

          -- If part_1 is not valid SQL expression, then it checks part_1 || ', ' || part_2, and so on until it gets valid SQL expression
          -- Valid SQL expression put into v_def_expr_arr at the corresponding key index
          -- Next cycle starts with part_n where n = *number of part that made previous expression valid* + 1

          check_expr := array_to_string(v_def_expr_split[check_start:check_end], ', ');

          select is_sql('select ' || check_expr || ';') as is_sql
          into is_sql;

          check_end := check_end + 1;

        end loop;

        check_start := check_end;
        is_sql := false;
        v_def_expr_arr := array_append(v_def_expr_arr,  '(' || check_expr || ')');

      end loop;

    end if;


    -- Getting final list of IN/INOUT/OUT/VARIADIC arguments and TABLE return arguments
    for v_args_rec in
      select r.arg_num,
             r.argmodes,
             r.argnames,
             r.argtypes,
             r.argdefaults
      from
      (
        select f.arg_num,
               a.argmodes,
               a.argnames,
               a.argtypes,
               a.argdefaults
        from
        (
            select n."key"::int as arg_num,
                   jsonb_agg(n."value") as vals
            from
            (
              select "key",
                     "value"
              from jsonb_each(v_proc_params.arg_params)
            ) n
            group by n."key"
        ) f
        cross join lateral jsonb_to_recordset(f.vals) a (argmodes text, argnames text, argtypes text, argdefaults text)
      ) r
      order by r.arg_num

    loop

      if v_args_rec.argmodes != 'TABLE' then
        arg_list := arg_list
                    || v_args_rec.argmodes || ' '
                    || case when coalesce(v_args_rec.argnames, '') != '' then v_args_rec.argnames || ' '  else '' end
                    || v_args_rec.argtypes
                    || case when coalesce(v_args_rec.argdefaults, '') != ''
                                 then ' ' || v_args_rec.argdefaults || v_def_expr_arr[v_args_rec.arg_num - (v_proc_params.pronargs - v_proc_params.pronargdefaults)]
                            else '' end
                    || E',\n    ';
      else
        v_return_type := v_return_type
                         || case when coalesce(v_args_rec.argnames, '') != '' then v_args_rec.argnames || ' '  else '' end
                         || v_args_rec.argtypes
                         || E',\n    ';
      end if;

    end loop;


    -- Simple return type or returns set of some type
    if jsonb_extract_path_text(v_proc_params.arg_params, v_proc_params.arg_quantity::text, 'argmodes') != 'TABLE' then
      select case when v_proc_params.proretset is true then 'SETOF ' else '' end
             || quote_ident(n.nspname) || '.' || quote_ident(t.typname)
      into v_return_type
      from pg_catalog.pg_type t
      join pg_catalog.pg_namespace n on t.typnamespace = n."oid"
      where t."oid" = v_proc_params.prorettype;
    else
      v_return_type := E'TABLE\n(\n    ' || v_return_type;
    end if;

    -- Language
    select quote_ident(lanname)
    into v_lang
    from pg_catalog.pg_language
    where "oid" = v_proc_params.prolang;

    -- Transforms
    if v_proc_params.protrftypes is not null then
      select E'\n  TRANSFORM ' || string_agg('FOR TYPE ' || quote_ident(n.nspname) || '.' || quote_ident(t.typname), ', ' order by tr.rn)
      into v_transform
      from pg_catalog.pg_type t
      join pg_catalog.pg_namespace n on t.typnamespace = n."oid"
      join unnest(v_proc_params.protrftypes) with ordinality tr (tps, rn) on tr.tps = t."oid";
    else
      v_transform := '';
    end if;

    -- Function-level configuration parameters
    if v_proc_params.proconfig is not null then
      select E'\n  ' || string_agg('SET ' || replace(str, '=', ' = '), E'\n' order by rn)
      into v_set_conf
      from unnest(v_proc_params.proconfig) with ordinality a (str, rn);
    else
      v_set_conf := '';
    end if;


    -- Building DDL

    if v_proc_params.prokind in ('f', 'w') then
      v_proc_ddl := 'CREATE OR REPLACE FUNCTION ' || quote_ident(v_proc_params.nspname) || '.' || quote_ident(in_proc)
                    || case when arg_list is null then '()' else E'\n(\n    ' || trim(arg_list, E',\n    ') || E'\n)' end
                    || E'\nRETURNS ' || rtrim(v_return_type, E',\n    ')
                    || case when jsonb_extract_path_text(v_proc_params.arg_params, v_proc_params.arg_quantity::text, 'argmodes') = 'TABLE'
                                 then E'\n)'
                       else '' end
                    || E'\n  LANGUAGE ' || v_lang
                    || v_transform
                    || case when v_proc_params.prokind = 'w' then E'\n  WINDOW' else '' end
                    || E'\n  ' || case v_proc_params.provolatile when 'i' then 'IMMUTABLE'
                                                                 when 's' then 'STABLE'
                                                                 when 'v' then 'VOLATILE'
                                  end
                    || E'\n  ' || case when v_proc_params.proleakproof is false then 'NOT ' else '' end || 'LEAKPROOF'
                    || E'\n  ' || case when v_proc_params.proisstrict is true then 'RETURNS NULL ON NULL INPUT' else 'CALLED ON NULL INPUT' end
                    || E'\n  ' || case when v_proc_params.prosecdef is true then 'SECURITY DEFINER' else 'SECURITY INVOKER' end
                    || E'\n  ' || case v_proc_params.proparallel when 's' then 'PARALLEL SAFE'
                                                                 when 'r' then 'PARALLEL RESTRICTED'
                                                                 when 'u' then 'PARALLEL UNSAFE'
                                  end
                    || E'\n  COST ' || v_proc_params.procost
                    || case when v_proc_params.proretset is false then '' else E'\n  ROWS ' || v_proc_params.prorows end
                    || case when v_proc_params.prosupport != '-'::regproc then E'\n  SUPPORT ' || get_function_identity_arguments(v_proc_params.prosupport, false, false) else '' end
                    || v_set_conf
                    || case when v_proc_params.prosqlbody is not null
                                 then E'\n' || rtrim(substring(pg_catalog.pg_get_functiondef(v_proc_params."oid"), '((?:RETURN ).+$|(?:BEGIN ATOMIC).+$)'), E'\n') -- this is kinda sad, but there are no better options to parse such expressions as of PostgreSQL 15
                            when v_proc_params.probin is not null
                                 then E'\nAS ' || quote_literal(v_proc_params.probin)
                                      || ', $c_func_name$' || quote_literal(v_proc_params.prosrc) || '$c_func_name$'
                            else
                                E'\nAS\n$function_def_string$'
                                || E'\n' || trim(v_proc_params.prosrc, E'\r\n')
                                || E'\n' || '$function_def_string$'
                       end
                    || ';';


    elsif v_proc_params.prokind = 'p' then
      v_proc_ddl := 'CREATE OR REPLACE PROCEDURE ' || quote_ident(v_proc_params.nspname) || '.' || quote_ident(in_proc)
                    || case when arg_list is null then '()' else E'\n(\n    ' || trim(arg_list, E',\n    ') || E'\n)' end
                    || E'\n  LANGUAGE ' || v_lang
                    || v_transform
                    || E'\n  ' || case when v_proc_params.prosecdef is true then 'SECURITY DEFINER' else 'SECURITY INVOKER' end
                    || v_set_conf
                    || case when v_proc_params.prosqlbody is not null
                                 then E'\n' || rtrim(substring(pg_catalog.pg_get_functiondef(v_proc_params."oid"), '((?:RETURN ).+$|(?:BEGIN ATOMIC).+$)'), E'\n') -- this is kinda sad, but there are no better options to parse such expressions as of PostgreSQL 15
                            when v_proc_params.probin is not null
                                 then E'\nAS ' || quote_literal(v_proc_params.probin)
                                      || ', $c_func_name$' || quote_literal(v_proc_params.prosrc) || '$c_func_name$'
                            else
                                E'\nAS\n$function_def_string$'
                                || E'\n' || trim(v_proc_params.prosrc, E'\r\n')
                                || E'\n' || '$function_def_string$'
                       end
                    || ';';

    end if;

    return v_proc_ddl;

  end;

$get_function_def$;

comment on function get_function_def(in_proc text, in_arg_types text[], in_schema text, in_arg_type_schemas text[])
    is 'Generates CREATE command for a function by its name, schema (default - search_path) and signature arguments (as text arrays of types'' names and schemas (default - search_path))';


-- OID

create or replace function get_function_def
  (
    proc_oid oid
  )
returns text
language plpgsql
strict
as
$$

-- Generates definition for all functions and procedures, but not for aggregates - use get_aggregate_def instead

-- Examples:

-- select get_function_def('version'::regproc),
--        get_function_def('substring(text, int)'::regprocedure);

-- select get_function_def("oid")
-- from pg_catalog.pg_proc
-- where prokind != 'a'
-- limit 10;

  declare

    v_proc_ddl text;
    v_proc_params record;
    arg_list text := ''::text;
    v_return_type text := ''::text;
    v_args_rec record;
    v_lang text;
    v_transform text;
    v_set_conf text;

    in_arg_types_fixed text[];
    in_arg_types_oids oid[];
    v_typoids_rec record;

    v_def_expr_arr text[];
    expr_parts int;
    check_start int;
    check_end int;
    check_expr text;
    v_def_expr_split text[];
    is_sql boolean := false;

  begin

    -- Checking existence of function
    select proname,
           prolang,
           procost,
           prorows,
           prosupport,
           prokind,
           prosecdef,
           proleakproof,
           proisstrict,
           proretset,
           provolatile,
           proparallel,
           pronargs,
           pronargdefaults,
           prorettype,
           proargdefaults,
           protrftypes,
           prosrc,
           probin,
           prosqlbody,
           proconfig,
           nspname,
           jsonb_object_agg(rn,
                            jsonb_build_object(
                                               'argtypes', argtypes,
                                               'argmodes', argmodes,
                                               'argnames', argnames,
                                               'argdefaults', case when rn > (pronargs - pronargdefaults) and rn <= pronargs
                                                                        then 'DEFAULT '
                                                              else null end
                                              )
                           ) as arg_params,
           max(rn) as arg_quantity
    into v_proc_params
    from
    (
        select a.proname,
               a.prolang,
               a.procost,
               a.prorows,
               a.prosupport,
               a.prokind,
               a.prosecdef,
               a.proleakproof,
               a.proisstrict,
               a.proretset,
               a.provolatile,
               a.proparallel,
               a.pronargs,
               a.pronargdefaults,
               a.prorettype,
               a.proargdefaults,
               a.protrftypes,
               a.prosrc,
               a.probin,
               a.prosqlbody,
               a.proconfig,
               quote_ident(nt.nspname) || '.' || quote_ident(t.typname) as argtypes,
               (case a.argmodes when 'o' then 'OUT'
                                when 'b' then 'INOUT'
                                when 'v' then 'VARIADIC'
                                when 't' then 'TABLE'
                else 'IN' end
               )::text as argmodes,
               a.argnames,
               a.nspname,
               row_number() over() as rn
        from
        (
            select p.proname,
                   p.prolang,
                   p.procost,
                   p.prorows,
                   p.prosupport,
                   p.prokind,
                   p.prosecdef,
                   p.proleakproof,
                   p.proisstrict,
                   p.proretset,
                   p.provolatile,
                   p.proparallel,
                   p.pronargs,
                   p.pronargdefaults,
                   p.prorettype,
                   unnest(string_to_array(case when p.proargtypes = ''::oidvector then null else p.proargtypes::text end, ' ')::oid[]) as in_argtypes,
                   unnest(coalesce(p.proallargtypes, array[0]::oid[])) as argtypes,
                   unnest(p.proargmodes) as argmodes,
                   unnest(p.proargnames) as argnames,
                   p.proargdefaults,
                   p.protrftypes,
                   p.prosrc,
                   p.probin,
                   p.prosqlbody,
                   p.proconfig,
                   n.nspname
            from pg_catalog.pg_proc p
            join pg_catalog.pg_namespace n on n."oid" = p.pronamespace
            where p.prokind != 'a'
                  and p."oid" = proc_oid
        ) a
        left join pg_catalog.pg_type t on coalesce(a.in_argtypes, a.argtypes) = t."oid"
        left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    ) b
    group by proname,
             prolang,
             procost,
             prorows,
             prosupport,
             prokind,
             prosecdef,
             proleakproof,
             proisstrict,
             proretset,
             provolatile,
             proparallel,
             pronargs,
             pronargdefaults,
             prorettype,
             proargdefaults,
             protrftypes,
             prosrc,
             probin,
             prosqlbody,
             proconfig,
             nspname;

    if v_proc_params.proname is null then
      raise warning 'Function / procedure with OID % does not exist', proc_oid;

      return null;
    end if;

    -- Parsing expressions
      -- pg_get_expr(pg_proc.proargdefaults, 0) basically yields a string of key expressions delimited by ', ' (a proper array here would be nice, but pg_node_tree can't be parsed in other way)
      -- It brings a problem that if expression has ', ' inside we can't determine start and end of each expression
      -- So, code below splits pg_get_expr output into tokens by ', ' delimiter, then check each consequent combination as a valid SQL-expression
          -- Using is_sql function from https://github.com/rin-nas/postgresql-patterns-library/blob/master/functions/is_sql.sql
      -- Since no SQL-expression could end with trailing ', ' it gives us a proper delimitation between default expressions

    if v_proc_params.proargdefaults is not null then

      -- Checking count of parts between ', '
      select (length(expr) - length(replace(expr, ', ', ''))) / 2 + 1
      into expr_parts
      from
      (
        select pg_catalog.pg_get_expr(v_proc_params.proargdefaults, 0, true) as expr
      ) e;

      -- Initial point
      check_start := 1;
      check_end := 1;

      -- Splitted expressions
      select array_agg(split_part(def_expr, ', ', parts) order by parts)
      into v_def_expr_split
      from
      (
        select sp.parts,
               pg_catalog.pg_get_expr(v_proc_params.proargdefaults, 0, true) as def_expr
        from generate_series(1, expr_parts) sp (parts)
      ) a;

      -- Looping through all available SQL-expressions
      for i in 1..expr_parts
      loop

        while is_sql is false
        loop

          -- If part_1 is not valid SQL expression, then it checks part_1 || ', ' || part_2, and so on until it gets valid SQL expression
          -- Valid SQL expression put into v_def_expr_arr at the corresponding key index
          -- Next cycle starts with part_n where n = *number of part that made previous expression valid* + 1

          check_expr := array_to_string(v_def_expr_split[check_start:check_end], ', ');

          select is_sql('select ' || check_expr || ';') as is_sql
          into is_sql;

          check_end := check_end + 1;

        end loop;

        check_start := check_end;
        is_sql := false;
        v_def_expr_arr := array_append(v_def_expr_arr,  '(' || check_expr || ')');

      end loop;

    end if;


    -- Getting final list of IN/INOUT/OUT/VARIADIC arguments and TABLE return arguments
    for v_args_rec in
      select r.arg_num,
             r.argmodes,
             r.argnames,
             r.argtypes,
             r.argdefaults
      from
      (
        select f.arg_num,
               a.argmodes,
               a.argnames,
               a.argtypes,
               a.argdefaults
        from
        (
            select n."key"::int as arg_num,
                   jsonb_agg(n."value") as vals
            from
            (
              select "key",
                     "value"
              from jsonb_each(v_proc_params.arg_params)
            ) n
            group by n."key"
        ) f
        cross join lateral jsonb_to_recordset(f.vals) a (argmodes text, argnames text, argtypes text, argdefaults text)
      ) r
      order by r.arg_num

    loop

      if v_args_rec.argmodes != 'TABLE' then
        arg_list := arg_list
                    || v_args_rec.argmodes || ' '
                    || case when coalesce(v_args_rec.argnames, '') != '' then v_args_rec.argnames || ' '  else '' end
                    || v_args_rec.argtypes
                    || case when coalesce(v_args_rec.argdefaults, '') != ''
                                 then ' ' || v_args_rec.argdefaults || v_def_expr_arr[v_args_rec.arg_num - (v_proc_params.pronargs - v_proc_params.pronargdefaults)]
                            else '' end
                    || E',\n    ';
      else
        v_return_type := v_return_type
                         || case when coalesce(v_args_rec.argnames, '') != '' then v_args_rec.argnames || ' '  else '' end
                         || v_args_rec.argtypes
                         || E',\n    ';
      end if;

    end loop;


    -- Simple return type or returns set of some type
    if jsonb_extract_path_text(v_proc_params.arg_params, v_proc_params.arg_quantity::text, 'argmodes') != 'TABLE' then
      select case when v_proc_params.proretset is true then 'SETOF ' else '' end
             || quote_ident(n.nspname) || '.' || quote_ident(t.typname)
      into v_return_type
      from pg_catalog.pg_type t
      join pg_catalog.pg_namespace n on t.typnamespace = n."oid"
      where t."oid" = v_proc_params.prorettype;
    else
      v_return_type := E'TABLE\n(\n    ' || v_return_type;
    end if;

    -- Language
    select quote_ident(lanname)
    into v_lang
    from pg_catalog.pg_language
    where "oid" = v_proc_params.prolang;

    -- Transforms
    if v_proc_params.protrftypes is not null then
      select E'\n  TRANSFORM ' || string_agg('FOR TYPE ' || quote_ident(n.nspname) || '.' || quote_ident(t.typname), ', ' order by tr.rn)
      into v_transform
      from pg_catalog.pg_type t
      join pg_catalog.pg_namespace n on t.typnamespace = n."oid"
      join unnest(v_proc_params.protrftypes) with ordinality tr (tps, rn) on tr.tps = t."oid";
    else
      v_transform := '';
    end if;

    -- Function-level configuration parameters
    if v_proc_params.proconfig is not null then
      select E'\n  ' || string_agg('SET ' || replace(str, '=', ' = '), E'\n' order by rn)
      into v_set_conf
      from unnest(v_proc_params.proconfig) with ordinality a (str, rn);
    else
      v_set_conf := '';
    end if;


    -- Building DDL

    if v_proc_params.prokind in ('f', 'w') then
      v_proc_ddl := 'CREATE OR REPLACE FUNCTION ' || quote_ident(v_proc_params.nspname) || '.' || quote_ident(v_proc_params.proname)
                    || case when arg_list is null then '()' else E'\n(\n    ' || trim(arg_list, E',\n    ') || E'\n)' end
                    || E'\nRETURNS ' || rtrim(v_return_type, E',\n    ')
                    || case when jsonb_extract_path_text(v_proc_params.arg_params, v_proc_params.arg_quantity::text, 'argmodes') = 'TABLE'
                                 then E'\n)'
                       else '' end
                    || E'\n  LANGUAGE ' || v_lang
                    || v_transform
                    || case when v_proc_params.prokind = 'w' then E'\n  WINDOW' else '' end
                    || E'\n  ' || case v_proc_params.provolatile when 'i' then 'IMMUTABLE'
                                                                 when 's' then 'STABLE'
                                                                 when 'v' then 'VOLATILE'
                                  end
                    || E'\n  ' || case when v_proc_params.proleakproof is false then 'NOT ' else '' end || 'LEAKPROOF'
                    || E'\n  ' || case when v_proc_params.proisstrict is true then 'RETURNS NULL ON NULL INPUT' else 'CALLED ON NULL INPUT' end
                    || E'\n  ' || case when v_proc_params.prosecdef is true then 'SECURITY DEFINER' else 'SECURITY INVOKER' end
                    || E'\n  ' || case v_proc_params.proparallel when 's' then 'PARALLEL SAFE'
                                                                 when 'r' then 'PARALLEL RESTRICTED'
                                                                 when 'u' then 'PARALLEL UNSAFE'
                                  end
                    || E'\n  COST ' || v_proc_params.procost
                    || case when v_proc_params.proretset is false then '' else E'\n  ROWS ' || v_proc_params.prorows end
                    || case when v_proc_params.prosupport != '-'::regproc then E'\n  SUPPORT ' || get_function_identity_arguments(v_proc_params.prosupport, false, false) else '' end
                    || v_set_conf
                    || case when v_proc_params.prosqlbody is not null
                                 then E'\n' || rtrim(substring(pg_catalog.pg_get_functiondef(proc_oid), '((?:RETURN ).+$|(?:BEGIN ATOMIC).+$)'), E'\n') -- this is kinda sad, but there are no better options to parse such expressions as of PostgreSQL 15
                            when v_proc_params.probin is not null
                                 then E'\nAS ' || quote_literal(v_proc_params.probin)
                                      || ', $c_func_name$' || quote_literal(v_proc_params.prosrc) || '$c_func_name$'
                            else
                                E'\nAS\n$function_def_string$'
                                || E'\n' || trim(v_proc_params.prosrc, E'\r\n')
                                || E'\n' || '$function_def_string$'
                       end
                    || ';';


    elsif v_proc_params.prokind = 'p' then
      v_proc_ddl := 'CREATE OR REPLACE PROCEDURE ' || quote_ident(v_proc_params.nspname) || '.' || quote_ident(v_proc_params.proname)
                    || case when arg_list is null then '()' else E'\n(\n    ' || trim(arg_list, E',\n    ') || E'\n)' end
                    || E'\n  LANGUAGE ' || v_lang
                    || v_transform
                    || E'\n  ' || case when v_proc_params.prosecdef is true then 'SECURITY DEFINER' else 'SECURITY INVOKER' end
                    || v_set_conf
                    || case when v_proc_params.prosqlbody is not null
                                 then E'\n' || rtrim(substring(pg_catalog.pg_get_functiondef(proc_oid), '((?:RETURN ).+$|(?:BEGIN ATOMIC).+$)'), E'\n') -- this is kinda sad, but there are no better options to parse such expressions as of PostgreSQL 15
                            when v_proc_params.probin is not null
                                 then E'\nAS ' || quote_literal(v_proc_params.probin)
                                      || ', $c_func_name$' || quote_literal(v_proc_params.prosrc) || '$c_func_name$'
                            else
                                E'\nAS\n$function_def_string$'
                                || E'\n' || trim(v_proc_params.prosrc, E'\r\n')
                                || E'\n' || '$function_def_string$'
                       end
                    || ';';

    end if;

    return v_proc_ddl;

  end;

$$;

comment on function get_function_def(proc_oid oid)
    is 'Generates CREATE command for a function by its OID';