-- Examples:

-- select get_operator_family_owner(o.opfname, a.amname, n.nspname),
--        get_operator_family_owner(o."oid", true)
-- from pg_catalog.pg_opfamily o
-- join pg_catalog.pg_am a on o.opfmethod = a."oid"
-- join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace;

-- select get_operator_family_owner('text_ops')
-- union all
-- select get_operator_family_owner('text_ops', 'hash', null, false);


-- Name + schema + access method

create or replace function get_operator_family_owner
  (
    in_opf text,
    in_method text default 'btree',
    in_schema text default null,
    rolename_only bool default false
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_operator_family_owner('text_ops')
-- union all
-- select get_operator_family_owner('text_ops', 'hash');

  declare

    v_owner_ddl text;
    v_opf_params record;

  begin

    -- Checking existence of operator family
    select
      o.opfowner,
      n.nspname
    into v_opf_params
    from pg_catalog.pg_opfamily o
    join pg_catalog.pg_am a on a."oid" = o.opfmethod
    left join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where o.opfname = in_opf
          and a.amname = in_method
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_opf_params.opfowner is null then
      raise warning 'Operator family %.% with access method % does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_opf),
        quote_ident(in_method);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER OPERATOR FAMILY '
                     || quote_ident(v_opf_params.nspname) || '.' || quote_ident(in_opf)
                     || ' USING ' || quote_ident(in_method)
                     || ' OWNER TO ' || v_opf_params.opfowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_opf_params.opfowner::regrole::text;

    end if;

  end;

$$;

comment on function get_operator_family_owner(in_opf text, in_method text, in_schema text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for an operator family by its name and schema (default - search_path) + used access method (def ''btree''); if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_operator_family_owner
  (
    opf_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_operator_family_owner("oid", true)
-- from pg_catalog.pg_opfamily
-- limit 10;

  declare

    v_owner_ddl text;
    v_opf_params record;
    v_access_method text;

  begin

    -- Checking existence of operator family
    select
      o.opfname,
      n.nspname,
      o.opfmethod,
      o.opfowner
    into v_opf_params
    from pg_catalog.pg_opfamily o
    join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace
    where o."oid" = opf_oid;

    if v_opf_params.opfname is null then
      raise warning 'Operator family with OID % does not exist', opf_oid;

      return null;
    end if;

    -- Access method
    select amname::text
    into v_access_method
    from pg_catalog.pg_am
    where v_opf_params.opfmethod = "oid";

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER OPERATOR FAMILY '
                     || quote_ident(v_opf_params.nspname) || '.' || quote_ident(v_opf_params.opfname)
                     || ' USING ' || quote_ident(v_access_method)
                     || ' OWNER TO ' || v_opf_params.opfowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_opf_params.opfowner::regrole::text;

    end if;

  end;

$$;

comment on function get_operator_family_owner(opf_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for an operator family by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';