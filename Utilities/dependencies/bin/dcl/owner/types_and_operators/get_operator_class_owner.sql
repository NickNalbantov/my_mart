-- Examples:

-- select get_operator_class_owner(o.opcname, a.amname, n.nspname),
--        get_operator_class_owner(o."oid", true)
-- from pg_catalog.pg_opclass o
-- join pg_catalog.pg_am a on o.opcmethod = a."oid"
-- join pg_catalog.pg_namespace n on n."oid" = o.opcnamespace;

-- select get_operator_class_owner('text_ops')
-- union all
-- select get_operator_class_owner('text_ops', 'hash', null, false);


-- Name + schema + access method

create or replace function get_operator_class_owner
  (
    in_opc text,
    in_method text default 'btree',
    in_schema text default null,
    rolename_only bool default false
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_operator_class_owner('text_ops')
-- union all
-- select get_operator_class_owner('text_ops', 'hash');

  declare

    v_owner_ddl text;
    v_opc_params record;

  begin

    -- Checking existence of operator class
    select
      o.opcowner,
      n.nspname
    into v_opc_params
    from pg_catalog.pg_opclass o
    join pg_catalog.pg_am a on o.opcmethod = a."oid"
    left join pg_catalog.pg_namespace n on n."oid" = o.opcnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where o.opcname = in_opc
          and a.amname = in_method
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_opc_params.opcowner is null then
      raise warning 'Operator class %.% with access method % does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_opc),
        quote_ident(in_method);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER OPERATOR CLASS '
                     || quote_ident(v_opc_params.nspname) || '.' || quote_ident(in_opc)
                     || ' USING ' || quote_ident(in_method)
                     || ' OWNER TO ' || v_opc_params.opcowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_opc_params.opcowner::regrole::text;

    end if;

  end;

$$;

comment on function get_operator_class_owner(in_opc text, in_schema text, in_method text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for an operator class by its name and schema (default - search_path) + used access method (def ''btree''); if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_operator_class_owner
  (
    opc_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_operator_class_owner("oid", true)
-- from pg_catalog.pg_opclass
-- limit 10;

  declare

    v_owner_ddl text;
    v_opc_params record;
    v_access_method text;

  begin

    -- Checking existence of operator class
    select
      o.opcname,
      n.nspname,
      o.opcmethod,
      o.opcowner
    into v_opc_params
    from pg_catalog.pg_opclass o
    join pg_catalog.pg_namespace n on n."oid" = o.opcnamespace
    where o."oid" = opc_oid;

    if v_opc_params.opcname is null then
      raise warning 'Operator class with OID % does not exist', opc_oid;

      return null;
    end if;

    -- Access method
    select amname::text
    into v_access_method
    from pg_catalog.pg_am
    where v_opc_params.opcmethod = "oid";

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER OPERATOR CLASS '
                     || quote_ident(v_opc_params.nspname) || '.' || quote_ident(v_opc_params.opcname)
                     || ' USING ' || quote_ident(v_access_method)
                     || ' OWNER TO ' || v_opc_params.opcowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_opc_params.opcowner::regrole::text;

    end if;

  end;

$$;

comment on function get_operator_class_owner(opc_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for an operator class by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';