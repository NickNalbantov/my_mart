-- Examples:

-- select get_fdw_owner(fdwname),
--        get_fdw_owner("oid", true)
-- from pg_catalog.pg_foreign_data_wrapper;


-- Name

create or replace function get_fdw_owner
  (
    in_fdw text,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_fdw_owner(fdwname)
-- from pg_catalog.pg_foreign_data_wrapper
-- limit 10;

  declare

    v_owner oid;
    v_owner_ddl text;

  begin

    -- Checking existence of FDW
    select fdwowner
    into v_owner
    from pg_catalog.pg_foreign_data_wrapper
    where fdwname = in_fdw;

    if v_owner is null then
      raise warning 'Foreign data wrapper % does not exist', quote_ident(in_fdw);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER FOREIGN DATA WRAPPER ' || quote_ident(in_fdw) || ' OWNER TO ' || v_owner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_owner::regrole::text;

    end if;

  end;

$$;

comment on function get_fdw_owner(in_fdw text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a foreign data wrapper by its name; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_fdw_owner
  (
    fdw_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_fdw_owner("oid", true)
-- from pg_catalog.pg_foreign_data_wrapper
-- limit 10;

  declare

    v_fdw_params record;
    v_owner_ddl text;

  begin

    -- Checking existence of FDW
    select
      fdwname,
      fdwowner
    into v_fdw_params
    from pg_catalog.pg_foreign_data_wrapper
    where "oid" = fdw_oid;

    if v_fdw_params.fdwname is null then
      raise warning 'Foreign data wrapper with OID % does not exist', fdw_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER FOREIGN DATA WRAPPER ' || quote_ident(v_fdw_params.fdwname) || ' OWNER TO ' || v_fdw_params.fdwowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_fdw_params.fdwowner::regrole::text;

    end if;

  end;

$$;

comment on function get_fdw_owner(fdw_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a foreign data wrapper by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';