-- Examples:

-- select get_server_owner(srvname),
--        get_server_owner("oid", true)
-- from pg_catalog.pg_foreign_server;


-- Name

create or replace function get_server_owner
  (
    in_srv text,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_server_owner(srvname)
-- from pg_catalog.pg_foreign_server
-- limit 10;

  declare

    v_owner oid;
    v_owner_ddl text;

  begin

    -- Checking existence of foreign server
    select srvowner
    into v_owner
    from pg_catalog.pg_foreign_server
    where srvname = in_srv;

    if v_owner is null then
      raise warning 'Foreign server % does not exist', quote_ident(in_srv);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER SERVER ' || quote_ident(in_srv) || ' OWNER TO ' || v_owner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_owner::regrole::text;

    end if;

  end;

$$;

comment on function get_server_owner(in_srv text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a foreign server by its name; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_server_owner
  (
    srv_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_server_owner("oid", true)
-- from pg_catalog.pg_foreign_server
-- limit 10;

  declare

    v_srv_params record;
    v_owner_ddl text;

  begin

    -- Checking existence of foreign server
    select
      srvname,
      srvowner
    into v_srv_params
    from pg_catalog.pg_foreign_server
    where "oid" = srv_oid;

    if v_srv_params.srvname is null then
      raise warning 'Foreign server with OID % does not exist', srv_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER SERVER ' || quote_ident(v_srv_params.srvname) || ' OWNER TO ' || v_srv_params.srvowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_srv_params.srvowner::regrole::text;

    end if;

  end;

$$;

comment on function get_server_owner(srv_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a foreign server by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';