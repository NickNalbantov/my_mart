-- Examples:

-- select get_foreign_table_owner(c.relname, n.nspname),
--        get_foreign_table_owner(c."oid", true)
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where c.relkind = 'f';


-- Name + schema

create or replace function get_foreign_table_owner
  (
    in_table text,
    in_schema text default null,
    rolename_only bool default false
  )
  returns text
  language plpgsql
  as
  $$

-- Generates ALTER...OWNER TO... command for foreign tables only
  -- For other types of tables use get_table_def functions

  declare

    v_owner_ddl text;
    v_table_params record;

  begin

    -- Checking existence of foreign table
    select
      c.relowner,
      n.nspname
    into v_table_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.relkind = 'f'
          and c.relname = in_table
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_table_params.relowner is null then
      raise warning 'Foreign table %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_table);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER FOREIGN TABLE IF EXISTS '
                     || quote_ident(v_table_params.nspname) || '.' || quote_ident(in_table)
                     || ' OWNER TO ' || v_table_params.relowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_table_params.relowner::regrole::text;

    end if;

  end;

$$;

comment on function get_foreign_table_owner(in_table text, in_schema text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a foreign table by its name and schema (default - search_path); if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_foreign_table_owner
  (
    table_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Generates ALTER...OWNER TO... command for foreign tables only
  -- For other types of tables use get_table_def functions

-- Example:

-- select get_foreign_table_owner("oid", true)
-- from pg_catalog.pg_class
-- where relkind = 'f'
-- limit 10;

  declare

    v_owner_ddl text;
    v_table_params record;

  begin

    -- Checking existence of foreign table
    select
      n.nspname,
      c.relname,
      c.relowner
    into v_table_params
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where c.relkind = 'f' and c."oid" = table_oid;

    if v_table_params.relname is null then
      raise warning 'Foreign table with OID % does not exist', table_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER FOREIGN TABLE IF EXISTS '
                     || quote_ident(v_table_params.nspname) || '.' || quote_ident(v_table_params.relname)
                     || ' OWNER TO ' || v_table_params.relowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_table_params.relowner::regrole::text;

    end if;

  end;

$$;

comment on function get_foreign_table_owner(table_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a foreign table by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';