-- Examples:

-- select get_ts_config_owner(c.cfgname, n.nspname),
--        get_ts_config_owner(c."oid", true)
-- from pg_catalog.pg_ts_config c
-- join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace;

-- select get_ts_config_owner('german');


-- Name + schema

create or replace function get_ts_config_owner
  (
    in_tscfg text,
    in_schema text default null,
    rolename_only bool default false
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_ts_config_owner('german');

-- select get_ts_config_owner(cfgname)
-- from pg_catalog.pg_ts_config
-- where cfgnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_owner_ddl text;
    v_tscfg_params record;

  begin

    -- Checking existence of FTS configuration
    select
      c.cfgowner,
      n.nspname
    into v_tscfg_params
    from pg_catalog.pg_ts_config c
    left join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.cfgname = in_tscfg
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tscfg_params.cfgowner is null then
      raise warning 'Text search configuration %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tscfg);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER TEXT SEARCH CONFIGURATION '
                     || quote_ident(v_tscfg_params.nspname) || '.' || quote_ident(in_tscfg)
                     || ' OWNER TO ' || v_tscfg_params.cfgowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_tscfg_params.cfgowner::regrole::text;

    end if;

  end;

$$;

comment on function get_ts_config_owner(in_tscfg text, in_schema text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a FTS configuration by its name and schema (default - search_path); if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_ts_config_owner
  (
    tscfg_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_ts_config_owner('german'::regconfig);

-- select get_ts_config_owner("oid", true)
-- from pg_catalog.pg_ts_config
-- limit 10;

  declare

    v_owner_ddl text;
    v_tscfg_params record;

  begin

    -- Checking existence of FTS configuration
    select n.nspname,
           c.cfgname,
           c.cfgowner
    into v_tscfg_params
    from pg_catalog.pg_ts_config c
    join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace
    where c."oid" = tscfg_oid;

    if v_tscfg_params.cfgname is null then
      raise warning 'Text search configuration with OID % does not exist', tscfg_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER TEXT SEARCH CONFIGURATION '
                     || quote_ident(v_tscfg_params.nspname) || '.' || quote_ident(v_tscfg_params.cfgname)
                     || ' OWNER TO ' || v_tscfg_params.cfgowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_tscfg_params.cfgowner::regrole::text;

    end if;

  end;

$$;

comment on function get_ts_config_owner(tscfg_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a FTS configuration by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';