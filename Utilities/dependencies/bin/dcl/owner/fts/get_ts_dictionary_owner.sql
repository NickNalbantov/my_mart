-- Examples:

-- select get_ts_dictionary_owner(d.dictname, n.nspname),
--        get_ts_dictionary_owner(d."oid", true)
-- from pg_catalog.pg_ts_dict d
-- join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace;

-- select get_ts_dictionary_owner('german_stem');


-- Name + schema

create or replace function get_ts_dictionary_owner
  (
    in_tsdict text,
    in_schema text default null,
    rolename_only bool default false
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_ts_dictionary_owner('german_stem');

-- select get_ts_dictionary_owner(dictname)
-- from pg_catalog.pg_ts_dict
-- where dictnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_owner_ddl text;
    v_tsdict_params record;

  begin

    -- Checking existence of FTS dictionary
    select
      d.dictowner,
      n.nspname
    into v_tsdict_params
    from pg_catalog.pg_ts_dict d
    left join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where d.dictname = in_tsdict
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_tsdict_params.dictowner is null then
      raise warning 'Text search dictionary %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_tsdict);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER TEXT SEARCH DICTIONARY '
                     || quote_ident(v_tsdict_params.nspname) || '.' || quote_ident(in_tsdict)
                     || ' OWNER TO ' || v_tsdict_params.dictowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_tsdict_params.dictowner::regrole::text;

    end if;

  end;

$$;

comment on function get_ts_dictionary_owner(in_tsdict text, in_schema text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a FTS dictionary by its name and schema (default - search_path); if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_ts_dictionary_owner
  (
    tsdict_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_ts_dictionary_owner('german_stem'::regdictionary);

-- select get_ts_dictionary_owner("oid", true)
-- from pg_catalog.pg_ts_dict
-- limit 10;

  declare

    v_owner_ddl text;
    v_tsdict_params record;

  begin

    -- Checking existence of FTS dictionary
    select
      n.nspname,
      d.dictname,
      d.dictowner
    into v_tsdict_params
    from pg_catalog.pg_ts_dict d
    join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
    where d."oid" = tsdict_oid;

    if v_tsdict_params.dictname is null then
      raise warning 'Text search dictionary with OID % does not exist', tsdict_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER TEXT SEARCH DICTIONARY '
                     || quote_ident(v_tsdict_params.nspname) || '.' || quote_ident(v_tsdict_params.dictname)
                     || ' OWNER TO ' || v_tsdict_params.dictowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_tsdict_params.dictowner::regrole::text;

    end if;

  end;

$$;

comment on function get_ts_dictionary_owner(tsdict_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a FTS dictionary by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';