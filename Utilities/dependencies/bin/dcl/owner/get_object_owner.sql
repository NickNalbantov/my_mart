-- Examples:

-- select "oid",
--        relnamespace::regnamespace || '.' || relname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_class
-- where relkind in ('r', 'p', 'm', 'v', 'f', 'S', 'c')

-- union all

-- select "oid",
--        collnamespace::regnamespace || '.' || collname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_collation

-- union all

-- select "oid",
--        connamespace::regnamespace || '.' || conname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_conversion

-- union all

-- select "oid",
--        datname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_database

-- union all

-- select "oid",
--        evtname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_event_trigger

-- union all

-- select "oid",
--        fdwname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_foreign_data_wrapper

-- union all

-- select "oid",
--        srvname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_foreign_server

-- union all

-- select "oid",
--        lanname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_language

-- union all

-- select "oid",
--        "oid"::text as lomname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_largeobject_metadata

-- union all

-- select "oid",
--        nspname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_namespace

-- union all

-- select "oid",
--        opcnamespace::regnamespace || '.' || opcname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_opclass

-- union all

-- select "oid",
--        oprnamespace::regnamespace || '.' || oprname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_operator

-- union all

-- select "oid",
--        opfnamespace::regnamespace || '.' || opfname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_opfamily

-- union all

-- select "oid",
--        pronamespace::regnamespace || '.' || proname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_proc

-- union all

-- select "oid",
--        pubname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_publication

-- union all

-- select "oid",
--        stxnamespace::regnamespace || '.' || stxname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_statistic_ext

-- union all

-- select "oid",
--        subname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_subscription

-- union all

-- select "oid",
--        spcname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_tablespace

-- union all

-- select "oid",
--        cfgnamespace::regnamespace || '.' || cfgname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_ts_config

-- union all

-- select "oid",
--        dictnamespace::regnamespace || '.' || dictname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_ts_dict

-- union all

-- select "oid",
--        typnamespace::regnamespace || '.' || typname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_type;


-- OIDs

create or replace function get_object_owner
  (
    classid oid,
    objid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- This is a general-use version of get_xxx_owner function, works only with OIDs/sub-ids. For text-based inputs and faster OID recognition use get_xxx_owner functions

-- 'classid' argument is necessary because objects' OID are unique only within given catalog

-- Example:

-- select "oid",
--        relnamespace::regnamespace || '.' || relname,
--        get_object_owner(tableoid, "oid")
-- from pg_catalog.pg_class
-- limit 10;

  declare

    v_owner_ddl text;
    v_identity_rec record;
    v_object_clause text;
    v_owner_role text;
    v_domain_check boolean default false;

  begin

    -- Identifying object
    with catalogs as
    (
        select
          cl."oid",
          cl.relname,
          ao.attname as catalog_owner_column
           from pg_catalog.pg_class cl
           join pg_catalog.pg_attribute ao on cl."oid" = ao.attrelid
           where
          cl."oid" = classid
          and cl.relnamespace = 'pg_catalog'::regnamespace
          and cl.relkind = 'r'
          and cl.relname not in ('pg_largeobject_metadata', 'pg_extension') -- either not an object catalog recognizable by pg_identify_object, or don't have ALTER...OWNER TO... command
          and substring(ao.attname, '(owner|dba)$') is not null
    )

    select *
    into v_identity_rec
    from
    (
      select
        quote_ident(c.relname) as catalog_name,
        c.catalog_owner_column,
        i."type" as obj_type,
        i."identity" as obj_identity
      from catalogs c
      cross join lateral pg_identify_object(c."oid", objid, 0) i

      union all
      -- pg_largeobject_metadata is not qualified as object catalog for pg_identify_object (because of pg_largeobject)
      select
        quote_ident('pg_largeobject_metadata') as catalog_name,
        'lomowner' as catalog_owner_column,
        'large object' as obj_type,
        "oid"::text as obj_identity
      from pg_catalog.pg_largeobject_metadata
      where "oid" = objid
    ) a;


    if v_identity_rec.catalog_name is null then
      raise warning E'Can''t identify object with classid (system catalog table''s OID) % and object''s OID % or this object does not support owner change',
        classid,
        objid;

      return null;
    end if;

    -- Check if object is a domain
    if classid = 'pg_catalog.pg_type'::regclass then
      select typtype = 'd'
      into v_domain_check
      from pg_catalog.pg_type
      where "oid" = objid;
    end if;

    -- Building object type clause
    select
      case
        when v_identity_rec.obj_type = 'type' and v_domain_check is true then 'DOMAIN'
        when v_identity_rec.obj_type = 'type' and v_domain_check is not true then 'TYPE'
        when v_identity_rec.obj_type = 'composite type' then 'TYPE'
        when v_identity_rec.obj_type = 'foreign-data wrapper' then 'FOREIGN DATA WRAPPER'
        when v_identity_rec.obj_type = 'statistics object' then 'STATISTICS'
        when v_identity_rec.obj_type in (
                                          'table', 'foreign table', 'view', 'materialized view', 'sequence', 'large object',
                                          'function', 'procedure', 'aggregate', 'event trigger',
                                          'collation', 'conversion', 'language',
                                          'server', 'publication', 'subscription',
                                          'schema', 'database', 'tablespace',
                                          'text search configuration', 'text search dictionary',
                                          'operator', 'operator class', 'operator family'
                                        )
              then upper(v_identity_rec.obj_type)
        else null::text
      end ||
      case when v_identity_rec.obj_type in ('table', 'foreign table', 'index', 'materialized view', 'sequence', 'view')
              then ' IF EXISTS '
          else ''
      end
    into v_object_clause;

    -- Getting owner role
    execute format
                (
                    $fmt$
                            select quote_ident(r.rolname)
                            from pg_catalog.%1$I c
                            join pg_catalog.pg_authid r on r."oid" = c.%2$I
                    $fmt$,
                    v_identity_rec.catalog_name,
                    v_identity_rec.catalog_owner_column
                )
    into v_owner_role;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER ' || v_object_clause || ' ' || v_identity_rec.obj_identity || ' OWNER TO ' || v_owner_role || ';';

      return v_owner_ddl;

    else
      return v_owner_role;

    end if;

  end;

$$;

comment on function get_object_owner(classid oid, objid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for an object by OID of parent catalog and object''s OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';