-- Examples:

-- select get_statistics_owner(s.stxname, n.nspname),
--        get_statistics_owner(s."oid", true)
-- from pg_catalog.pg_statistic_ext s
-- join pg_catalog.pg_namespace n on s.stxnamespace = n."oid";


-- Name

create or replace function get_statistics_owner
  (
    in_stat text,
    in_schema text default null,
    rolename_only bool default false
  )
returns text
language plpgsql
as
$$

-- Example:

-- select get_statistics_owner(stxname)
-- from pg_catalog.pg_statistic_ext s
-- where stxnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_owner_ddl text;
    v_stat_params record;

  begin

    -- Checking existence of statistics object

    select
      st.stxowner,
      n.nspname
    into v_stat_params
    from pg_catalog.pg_statistic_ext st
    left join pg_catalog.pg_namespace n on n."oid" = st.stxnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where in_stat = st.stxname
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_stat_params.stxowner is null then
      raise warning 'Statistics object %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_stat);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER STATISTICS '
                     || quote_ident(v_stat_params.nspname) || '.' || quote_ident(in_stat)
                     || ' OWNER TO ' || v_stat_params.stxowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_stat_params.stxowner::regrole::text;

    end if;

  end;

$$;

comment on function get_statistics_owner(in_stat text, in_schema text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a statistics object by its name and schema (default - search_path); if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_statistics_owner
  (
    stat_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_statistics_owner("oid", true)
-- from pg_catalog.pg_statistic_ext s
-- limit 10;

  declare

    v_owner_ddl text;
    v_stat_params record;

  begin

    -- Checking existence of statistics object

    select
      st.stxname,
      st.stxowner,
      n.nspname
    into v_stat_params
    from pg_catalog.pg_statistic_ext st
    join pg_catalog.pg_namespace n on n."oid" = st.stxnamespace
    where stat_oid = st."oid";

    if v_stat_params.stxname is null then
      raise warning 'Statistics object with OID % does not exist', stat_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER STATISTICS '
                     || quote_ident(v_stat_params.nspname) || '.' || quote_ident(v_stat_params.stxname)
                     || ' OWNER TO ' || v_stat_params.stxowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_stat_params.stxowner::regrole::text;

    end if;

  end;

$$;

comment on function get_statistics_owner(stat_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a statistics object by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';