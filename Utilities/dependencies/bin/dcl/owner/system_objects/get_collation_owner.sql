-- Examples:

-- select get_collation_owner(c.collname, n.nspname),
--        get_collation_owner(c."oid", true)
-- from pg_catalog.pg_collation c
-- join pg_catalog.pg_namespace n on c.collnamespace = n."oid";

-- select get_collation_owner('ru-RU-x-icu');


-- Name + schema

create or replace function get_collation_owner
  (
    in_coll text,
    in_schema text default null,
    rolename_only bool default false
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_collation_owner('ru-RU-x-icu');

-- select get_collation_owner(collname)
-- from pg_catalog.pg_collation
-- where collnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_owner_ddl text;
    v_coll_params record;

  begin

    -- Checking existence of collation
    select
      c.collowner,
      n.nspname
    into v_coll_params
    from pg_catalog.pg_collation c
    left join pg_catalog.pg_namespace n on c.collnamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.collname = in_coll
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_coll_params.collowner is null then
      raise warning 'Collation %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_coll);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER COLLATION '
                     || quote_ident(v_coll_params.nspname) || '.' || quote_ident(in_coll)
                     || ' OWNER TO ' || v_coll_params.collowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_coll_params.collowner::regrole::text;

    end if;

  end;

$$;

comment on function get_collation_owner(in_coll text, in_schema text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a collation by its name and schema (default - search_path); if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_collation_owner
  (
    coll_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Examples:

-- select get_collation_owner('"ru-RU-x-icu"'::regcollation);

-- select get_collation_owner("oid", true)
-- from pg_catalog.pg_collation
-- limit 10;

  declare

    v_owner_ddl text;
    v_coll_params record;

  begin

    -- Checking existence of collation
    select
      c.collname,
      c.collowner,
      n.nspname
    into v_coll_params
    from pg_catalog.pg_collation c
    join pg_catalog.pg_namespace n on c.collnamespace = n."oid"
    where c."oid" = coll_oid;

    if v_coll_params.collname is null then
      raise warning 'Collation with OID % does not exist', coll_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER COLLATION '
                     || quote_ident(v_coll_params.nspname) || '.' || quote_ident(v_coll_params.collname)
                     || ' OWNER TO ' || v_coll_params.collowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_coll_params.collowner::regrole::text;

    end if;

  end;

$$;

comment on function get_collation_owner(coll_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a collation by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';