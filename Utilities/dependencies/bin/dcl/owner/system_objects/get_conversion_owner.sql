-- Examples:

-- select get_conversion_owner(c.conname, n.nspname),
--        get_conversion_owner(c."oid", true)
-- from pg_catalog.pg_conversion c
-- join pg_catalog.pg_namespace n on c.connamespace = n."oid";

-- select get_conversion_owner('utf8_to_windows_1251');


-- Name + schema

create or replace function get_conversion_owner
  (
    in_con text,
    in_schema text default null,
    rolename_only bool default false
  )
returns text
language plpgsql
as
$$

-- Examples:

-- select get_conversion_owner('utf8_to_windows_1251');

-- select get_conversion_owner(conname)
-- from pg_catalog.pg_conversion
-- where connamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_owner_ddl text;
    v_con_params record;

  begin

    -- Checking existence of conversion
    select
      c.conowner,
      n.nspname
    into v_con_params
    from pg_catalog.pg_conversion c
    left join pg_catalog.pg_namespace n on c.connamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.conname = in_con
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_con_params.conowner is null then
      raise warning 'Conversion %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_con);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER CONVERSION '
                     || quote_ident(v_con_params.nspname) || '.' || quote_ident(in_con)
                     || ' OWNER TO ' || v_con_params.conowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_con_params.conowner::regrole::text;

    end if;

  end;

$$;

comment on function get_conversion_owner(in_con text, in_schema text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a conversion by its name and schema (default - search_path); if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_conversion_owner
  (
    con_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_conversion_owner("oid", true)
-- from pg_catalog.pg_conversion
-- limit 10;

  declare

    v_owner_ddl text;
    v_con_params record;

  begin

    -- Checking existence of conversion
    select
      c.conname,
      c.conowner,
      n.nspname
    into v_con_params
    from pg_catalog.pg_conversion c
    join pg_catalog.pg_namespace n on c.connamespace = n."oid"
    where c."oid" = con_oid;

    if v_con_params.conname is null then
      raise warning 'Conversion with OID % does not exist', con_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER CONVERSION '
                     || quote_ident(v_con_params.nspname) || '.' || quote_ident(v_con_params.conname)
                     || ' OWNER TO ' || v_con_params.conowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_con_params.conowner::regrole::text;

    end if;

  end;

$$;

comment on function get_conversion_owner(con_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a conversion by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';