-- Examples:

-- select get_database_owner(datname),
--        get_database_owner("oid", true)
-- from pg_catalog.pg_database;


-- Name

create or replace function get_database_owner
  (
    in_db text,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_database_owner(datname),
-- from pg_catalog.pg_database
-- limit 10;

  declare

    v_owner oid;
    v_owner_ddl text;

  begin

    -- Checking existence of database
    select datdba
    into v_owner
    from pg_catalog.pg_database d
    where d.datname = in_db;

    if v_owner is null then
      raise warning 'Database % does not exist', quote_ident(in_db);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER DATABASE ' || quote_ident(in_db) || ' OWNER TO ' || v_owner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_owner::regrole::text;

    end if;

  end;

$$;

comment on function get_database_owner(in_db text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a database by its name; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_database_owner
  (
    db_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_database_owner("oid"),
-- from pg_catalog.pg_database
-- limit 10;

  declare

    v_db_params record;
    v_owner_ddl text;

  begin

    -- Checking existence of database
    select
      datname,
      datdba
    into v_db_params
    from pg_catalog.pg_database
    where "oid" = db_oid;

    if v_db_params.datname is null then
      raise warning 'Database with OID % does not exist', db_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER DATABASE ' || quote_ident(v_db_params.datname) || ' OWNER TO ' || v_db_params.datdba::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_db_params.datdba::regrole::text;

    end if;

  end;
$$;

comment on function get_database_owner(db_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a database by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';