-- Examples:

-- select get_tablespace_owner(spcname),
--        get_tablespace_owner("oid", true)
-- from pg_catalog.pg_tablespace;


-- Name

create or replace function get_tablespace_owner
  (
    in_tblspc text,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_tablespace_owner(spcname)
-- from pg_catalog.pg_tablespace;

  declare

    v_owner oid;
    v_owner_ddl text;

  begin

    -- Checking existence of tablespace
    select spcowner
    into v_owner
    from pg_catalog.pg_tablespace
    where spcname = in_tblspc;

    if v_owner is null then
      raise warning 'Tablespace % does not exist', quote_ident(in_tblspc);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER TABLESPACE ' || quote_ident(in_tblspc) || ' OWNER TO ' || v_owner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_owner::regrole::text;

    end if;

  end;

$$;

comment on function get_tablespace_owner(in_tblspc text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a tablespace by its name; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_tablespace_owner
  (
    tblspc_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_tablespace_owner("oid", true)
-- from pg_catalog.pg_tablespace;

  declare

    v_tblspc_params record;
    v_owner_ddl text;

  begin

    -- Checking existence of tablespace
    select
      spcname,
      spcowner
    into v_tblspc_params
    from pg_catalog.pg_tablespace
    where "oid" = tblspc_oid;

    if v_tblspc_params.spcname is null then
      raise warning 'Tablespace with OID % does not exist', tblspc_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER TABLESPACE ' || quote_ident(v_tblspc_params.spcname) || ' OWNER TO ' || v_tblspc_params.spcowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_tblspc_params.spcowner::regrole::text;

    end if;

  end;

$$;

comment on function get_tablespace_owner(tblspc_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a tablespace by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';