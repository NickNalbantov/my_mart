-- Examples:

-- select get_event_trigger_owner(evtname),
--        get_event_trigger_owner("oid", true)
-- from pg_catalog.pg_event_trigger;


-- Name

create or replace function get_event_trigger_owner
  (
    in_et text,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_event_trigger_owner(evtname)
-- from pg_catalog.pg_event_trigger
-- limit 10;

  declare

    v_owner oid;
    v_owner_ddl text;

  begin

    -- Checking existence of event trigger
    select evtowner
    into v_owner
    from pg_catalog.pg_event_trigger
    where evtname = in_et;

    if v_owner is null then
      raise warning 'Event trigger % does not exist', quote_ident(in_et);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER EVENT TRIGGER ' || quote_ident(in_et) || ' OWNER TO ' || v_owner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_owner::regrole::text;

    end if;

  end;

$$;

comment on function get_event_trigger_owner(in_et text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for an event trigger by its name; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_event_trigger_owner
  (
    et_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select
--    evtname,
--    get_event_trigger_owner("oid", true)
-- from pg_catalog.pg_event_trigger
-- limit 10;

  declare

    v_et_params record;
    v_owner_ddl text;

  begin

    -- Checking existence of event trigger
    select
      evtname,
      evtowner
    into v_et_params
    from pg_catalog.pg_event_trigger
    where "oid" = et_oid;

    if v_et_params.evtname is null then
      raise warning 'Event trigger with OID % does not exist', et_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER EVENT TRIGGER ' || quote_ident(v_et_params.evtname) || ' OWNER TO ' || v_et_params.evtowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_et_params.evtowner::regrole::text;

    end if;

  end;

$$;

comment on function get_event_trigger_owner(et_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for an event trigger by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';