-- Example:

-- select "oid",
--        get_large_object_owner("oid")
-- from pg_catalog.pg_largeobject_metadata;


-- OID

create or replace function get_large_object_owner
  (
    lo_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select "oid",
--        get_large_object_owner("oid")
-- from pg_catalog.pg_largeobject_metadata
-- limit 10;

  declare

    v_owner oid;
    v_owner_ddl text;

  begin

    -- Checking existence of large object ACL
    select lomowner
    into v_owner
    from pg_catalog.pg_largeobject_metadata
    where "oid" = lo_oid;

    if v_owner is null then
      raise warning 'Large object with OID % does not exist', lo_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER LARGE OBJECT ' || lo_oid || ' OWNER TO ' || v_owner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_owner::regrole::text;

    end if;

  end;

$$;

comment on function get_large_object_owner(lo_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a large object by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';