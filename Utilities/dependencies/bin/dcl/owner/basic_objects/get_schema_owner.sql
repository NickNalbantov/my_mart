-- Examples:

-- select get_schema_owner(nspname),
--        get_schema_owner("oid", true)
-- from pg_catalog.pg_namespace;


-- Name

create or replace function get_schema_owner
  (
    in_schema text,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

--Example:

-- select get_schema_owner(nspname)
-- from pg_catalog.pg_namespace
-- limit 10;

  declare

    v_owner oid;
    v_owner_ddl text;

  begin

    -- Checking existence of schema
    select nspowner
    into v_owner
    from pg_catalog.pg_namespace
    where nspname = in_schema;

    if v_owner is null then
      raise warning 'Schema % does not exist', quote_ident(in_schema);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER SCHEMA ' || quote_ident(in_schema) || ' OWNER TO ' || v_owner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_owner::regrole::text;

    end if;

  end;

$$;

comment on function get_schema_owner(in_schema text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a schema by its name; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_schema_owner
  (
    schema_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

--Examples:

-- select get_schema_owner('pg_catalog'::regnamespace);

-- select get_schema_owner("oid", true)
-- from pg_catalog.pg_namespace
-- limit 10;

  declare

    v_sch_params record;
    v_owner_ddl text;

  begin

    -- Checking existence of schema
    select
      nspname,
      nspowner
    into v_sch_params
    from pg_catalog.pg_namespace
    where "oid" = schema_oid;

    if v_sch_params.nspname is null then
      raise warning 'Schema with OID % does not exist', schema_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER SCHEMA ' || quote_ident(v_sch_params.nspname) || ' OWNER TO ' || v_sch_params.nspowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_sch_params.nspowner::regrole::text;

    end if;

  end;

$$;

comment on function get_schema_owner(schema_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a schema by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';