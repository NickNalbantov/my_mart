-- Examples:

-- select get_view_owner(c.relname, n.nspname),
--        get_view_owner(c."oid", true)
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where c.relkind in ('v', 'm');

-- select get_view_owner('pg_settings');


-- Name + schema

create or replace function get_view_owner
  (
    in_view text,
    in_schema text default null,
    rolename_only bool default false
  )
returns text
language plpgsql
as
$$

-- Generates ALTER...OWNER TO... command for views and materialized views

-- Examples:

-- select get_view_owner('pg_settings');

-- select get_view_owner(relname)
-- from pg_catalog.pg_class
-- where relkind in ('v', 'm')
--       and relnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_owner_ddl text;
    v_view_params record;

  begin

    -- Checking existence of view
    select c.relowner,
           c.relkind,
           n.nspname
    into v_view_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.relkind in ('v', 'm')
          and c.relname = in_view
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_view_params.relowner is null then
      raise warning 'View / materialized view %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_view);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER'
                     || case when v_view_params.relkind = 'm' then ' MATERIALIZED ' else ' ' end
                     || 'VIEW IF EXISTS '
                     || quote_ident(v_view_params.nspname) || '.' || quote_ident(in_view)
                     || ' OWNER TO ' || v_view_params.relowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_view_params.relowner::regrole::text;

    end if;

  end;

$$;

comment on function get_view_owner(in_view text, in_schema text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a view by its name and schema (default - search_path); if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_view_owner
  (
    view_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Generates ALTER...OWNER TO... command for views and materialized views

-- Examples:

-- select get_view_owner('pg_settings');

-- select get_view_owner("oid", true)
-- from pg_catalog.pg_class
-- where relkind in ('v', 'm')
--       and relnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_owner_ddl text;
    v_view_params record;

  begin

    -- Checking existence of view
    select c.relname,
           c.relkind,
           c.relowner,
           n.nspname
    into v_view_params
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
    where c."oid" = view_oid;

    if v_view_params.relname is null then
      raise warning 'View / materialized view with OID % does not exist', view_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER'
                     || case when v_view_params.relkind = 'm' then ' MATERIALIZED ' else ' ' end
                     || 'VIEW IF EXISTS '
                     || quote_ident(v_view_params.nspname) || '.' || quote_ident(v_view_params.relname)
                     || ' OWNER TO ' || v_view_params.relowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_view_params.relowner::regrole::text;

    end if;

  end;

$$;

comment on function get_view_owner(view_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a view by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';