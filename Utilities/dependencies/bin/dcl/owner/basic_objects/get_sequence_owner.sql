-- Examples:

-- select get_sequence_owner(c.relname, n.nspname),
--        get_sequence_owner(c."oid", true)
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
-- where c.relkind = 'S';


-- Name + schema

create or replace function get_sequence_owner
  (
    in_seq text,
    in_schema text default null,
    rolename_only bool default false
  )
returns text
language plpgsql
as
$$

  declare

    v_owner_ddl text;
    v_seq_params record;

  begin

    -- Checking existence of sequence
    select c.relowner,
           n.nspname
    into v_seq_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.relkind = 'S'
          and c.relname = in_seq
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_seq_params.relowner is null then
      raise warning 'Sequence %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_seq);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER SEQUENCE IF EXISTS '
                     || quote_ident(v_seq_params.nspname) || '.' || quote_ident(in_seq)
                     || ' OWNER TO ' || v_seq_params.relowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_seq_params.relowner::regrole::text;

    end if;

  end;

$$;

comment on function get_sequence_owner(in_seq text, in_schema text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a sequence by its name and schema (default - search_path); if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_sequence_owner
  (
    seq_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_sequence_owner("oid", true)
-- from pg_catalog.pg_class
-- where relkind = 'S'
-- limit 10;

  declare

    v_owner_ddl text;
    v_seq_params record;

  begin

    -- Checking existence of sequence
    select c.relname,
           c.relowner,
           n.nspname
    into v_seq_params
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where c.relkind = 'S'
          and c."oid" = seq_oid;

    if v_seq_params.relname is null then
      raise warning 'Sequence with OID % does not exist', seq_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER SEQUENCE IF EXISTS '
                     || quote_ident(v_seq_params.nspname) || '.' || quote_ident(v_seq_params.relname)
                     || ' OWNER TO ' || v_seq_params.relowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_seq_params.relowner::regrole::text;

    end if;

  end;

$$;

comment on function get_sequence_owner(seq_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a sequence by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';