-- Examples:

-- select get_subscription_owner(subname),
--        get_subscription_owner("oid", true)
-- from pg_catalog.pg_subscription;


-- Name

create or replace function get_subscription_owner
  (
    in_sub text,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_subscription_owner(subname)
-- from pg_catalog.pg_subscription
-- limit 10;

  declare

    v_owner oid;
    v_owner_ddl text;

  begin

    -- Checking existence of subscription
    select subowner
    into v_owner
    from pg_catalog.pg_subscription
    where subname = in_sub;

    if v_owner is null then
      raise warning 'Subscription % does not exist', quote_ident(in_sub);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER SUBSCRIPTION ' || quote_ident(in_sub) || ' OWNER TO ' || v_owner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_owner::regrole::text;

    end if;

  end;

$$;

comment on function get_subscription_owner(in_sub text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a subscription by its name; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_subscription_owner
  (
    sub_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_subscription_owner("oid", true)
-- from pg_catalog.pg_subscription
-- limit 10;

  declare

    v_sub_params record;
    v_owner_ddl text;

  begin

    -- Checking existence of subscription
    select
      subname,
      subowner
    into v_sub_params
    from pg_catalog.pg_subscription
    where "oid" = sub_oid;

    if v_sub_params.subname is null then
      raise warning 'Subscription with OID % does not exist', sub_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER SUBSCRIPTION ' || quote_ident(v_sub_params.subname) || ' OWNER TO ' || v_sub_params.subowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_sub_params.subowner::regrole::text;

    end if;

  end;

$$;

comment on function get_subscription_owner(sub_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a subscription by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';