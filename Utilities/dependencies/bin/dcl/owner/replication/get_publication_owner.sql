-- Examples:

-- select get_publication_owner(pubname),
--        get_publication_owner("oid", true)
-- from pg_catalog.pg_publication;


-- Name

create or replace function get_publication_owner
  (
    in_pub text,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_publication_owner(pubname)
-- from pg_catalog.pg_publication
-- limit 10;

  declare

    v_owner oid;
    v_owner_ddl text;

  begin

    -- Checking existence of publication
    select pubowner
    into v_owner
    from pg_catalog.pg_publication
    where pubname = in_pub;

    if v_owner is null then
      raise warning 'Publication % does not exist', quote_ident(in_pub);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER PUBLICATION ' || quote_ident(in_pub) || ' OWNER TO ' || v_owner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_owner::regrole::text;

    end if;

  end;

$$;

comment on function get_publication_owner(in_pub text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a publication by its name; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_publication_owner
  (
    pub_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_publication_owner("oid", true)
-- from pg_catalog.pg_publication
-- limit 10;

  declare

    v_pub_params record;
    v_owner_ddl text;

  begin

    -- Checking existence of publication
    select
      pubname,
      pubowner
    into v_pub_params
    from pg_catalog.pg_publication
    where "oid" = pub_oid;

    if v_pub_params.pubname is null then
      raise warning 'Publication with OID % does not exist', pub_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER PUBLICATION ' || quote_ident(v_pub_params.pubname) || ' OWNER TO ' || v_pub_params.pubowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_pub_params.pubowner::regrole::text;

    end if;

  end;

$$;

comment on function get_publication_owner(pub_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a publication by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';