-- Examples:

-- select get_language_owner(lanname),
--        get_language_owner("oid", true)
-- from pg_catalog.pg_language;


-- Name

create or replace function get_language_owner
  (
    in_lang text,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_language_owner(lanname)
-- from pg_catalog.pg_language
-- limit 10;

  declare

    v_owner oid;
    v_owner_ddl text;

  begin

    -- Checking existence of language
    select lanowner
    into v_owner
    from pg_catalog.pg_language
    where lanname = in_lang;

    if v_owner is null then
      raise warning 'Language % does not exist', quote_ident(in_lang);

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER LANGUAGE ' || quote_ident(in_lang) || ' OWNER TO ' || v_owner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_owner::regrole::text;

    end if;

  end;

$$;

comment on function get_language_owner(in_lang text, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a language by its name; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';


-- OID

create or replace function get_language_owner
  (
    lang_oid oid,
    rolename_only bool default false
  )
returns text
language plpgsql
strict
as
$$

-- Example:

-- select get_language_owner("oid", true)
-- from pg_catalog.pg_language
-- limit 10;

  declare

    v_lan_params record;
    v_owner_ddl text;

  begin

    -- Checking existence of language
    select
      lanname,
      lanowner
    into v_lan_params
    from pg_catalog.pg_language
    where "oid" = lang_oid;

    if v_lan_params.lanname is null then
      raise warning 'Language with OID % does not exist', lang_oid;

      return null;
    end if;

    -- Result

    if rolename_only is false then
      v_owner_ddl := 'ALTER LANGUAGE ' || quote_ident(v_lan_params.lanname) || ' OWNER TO ' || v_lan_params.lanowner::regrole::text || ';';

      return v_owner_ddl;

    else

      return v_lan_params.lanowner::regrole::text;

    end if;

  end;

$$;

comment on function get_language_owner(lang_oid oid, rolename_only bool)
    is 'Generates ALTER...OWNER TO... command for a language by its OID; if rolename_only is true (default false) then only owner''s role name will be returned instead of full ALTER...OWNER TO.. command';