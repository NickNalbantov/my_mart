-- Examples:

--  select c."oid",
--         c.relnamespace::regnamespace || '.' || c.relname || '.' || a.attname,
--         get_object_acl_drop(c.tableoid, c."oid", a.attnum)
--  from pg_catalog.pg_class c
--  join pg_catalog.pg_attribute a on a.attrelid = c."oid"
--  where a.attnum > 0
--        and a.attisdropped is false
--        and a.attacl is not null -- default ACL for columns is always null (as of PG15)

--  union all

--  select "oid",
--         relnamespace::regnamespace || '.' || relname,
--         get_object_acl_drop(tableoid, "oid", 0)
--  from pg_catalog.pg_class
--  where relkind in ('r', 'p', 'm', 'v', 'f', 'S', 't')

--  union all

--  select "oid",
--         datname,
--         get_object_acl_drop(tableoid, "oid", 0)
--  from pg_catalog.pg_database

--  union all

--  select "oid",
--         fdwname,
--         get_object_acl_drop(tableoid, "oid", 0)
--  from pg_catalog.pg_foreign_data_wrapper

--  union all

--  select "oid",
--         srvname,
--         get_object_acl_drop(tableoid, "oid", 0)
--  from pg_catalog.pg_foreign_server

--  union all

--  select "oid",
--         lanname,
--         get_object_acl_drop(tableoid, "oid", 0)
--  from pg_catalog.pg_language

--  union all

--  select "oid",
--         "oid"::text as lomname,
--         get_object_acl_drop(tableoid, "oid", 0)
--  from pg_catalog.pg_largeobject_metadata

--  union all

--  select "oid",
--         nspname,
--         get_object_acl_drop(tableoid, "oid", 0)
--  from pg_catalog.pg_namespace

--  union all

--  select "oid",
--         pronamespace::regnamespace || '.' || proname,
--         get_object_acl_drop(tableoid, "oid", 0)
--  from pg_catalog.pg_proc

--  union all

--  select "oid",
--         spcname,
--         get_object_acl_drop(tableoid, "oid", 0)
--  from pg_catalog.pg_tablespace

--  union all

--  select "oid",
--         typnamespace::regnamespace || '.' || typname,
--         get_object_acl_drop(tableoid, "oid", 0)
--  from pg_catalog.pg_type;


-- OIDs

create or replace function get_object_acl_drop
  (
    classid oid,
    objid oid,
    objsubid anycompatiblenonarray default 0,
    grant_option_only bool default false,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- This is a general-use version of get_xxx_acl_drop function, works only with OIDs/sub-ids. For text-based inputs and faster OID recognition use get_xxx_acl_drop functions

-- 'classid' argument is necessary because objects' OID are unique only within given catalog
-- objsubid must equals 0 for all catalogs except pg_class - function is fixing it automatically
-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command

-- For generating DCL commands for pg_default_acl and pg_parameter_acl entries use get_default_acl_drop and get_parameter_acl_drop instead

-- Note that not only REVOKE commands are removing ACL objects, but some GRANT commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command
-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:
  -- Removing ACLs for pg_catalog.pg_subscription columns

-- select c.relnamespace::regnamespace || '.' || c.relname || '.' || a.attname,
--        get_object_acl_drop(cl.tableoid, cl."oid", a.attnum)
-- from pg_catalog.pg_class cl
-- join pg_catalog.pg_attribute a on a.attrelid = cl."oid"
-- where cl."oid" = 'pg_catalog.pg_subscription'::regclass
--       and a.attnum > 0;

  declare

    v_acl_dcl text;
    v_identity_rec record;
    v_acl_rec record;

    v_object_clause text;
    v_domain_check boolean default false;

    v_def_acl text;
    v_obj_acl text;

  begin

    -- Identifying object
    with catalogs as
    (
        select
          cl."oid",
          cl.relname,
          aa.attname as catalog_acl_column,
          ao.attname as catalog_owner_column
           from pg_catalog.pg_class cl
           join pg_catalog.pg_attribute aa on
          cl."oid" = aa.attrelid
          and aa.attname like '%acl'
        left join pg_catalog.pg_attribute ao on
          cl."oid" = ao.attrelid
          and substring(ao.attname, '(owner|dba)$') is not null
        where
          cl."oid" = classid
          and cl.relnamespace = 'pg_catalog'::regnamespace
          and cl.relkind = 'r'
          and cl.relname not in ('pg_attribute', 'pg_largeobject_metadata', 'pg_default_acl', 'pg_parameter_acl')
    )

    select *
    into v_identity_rec
    from
    (
      select
        case when i."type" like '%column' then quote_ident('pg_attribute')
          else quote_ident(c.relname)
        end as catalog_name,
        case when i."type" like '%column' then 'attacl'
          else c.catalog_acl_column
        end as catalog_acl_column,
        c.catalog_owner_column,
        i."type" as obj_type,
        i."schema" as obj_schema,
        i."name" as obj_name,
        i."identity" as obj_identity,
        case
          when i."type" like '%column' then 'c'
          when i."type" in ('function', 'procedure', 'aggregate') then 'f'
          when i."type" in ('table', 'toast table', 'foreign table', 'view', 'materialized view') then 'r'
          when i."type" = 'sequence' then 's'
          when i."type" = 'type' then 'T'
          when i."type" = 'schema' then 'n'
          when i."type" = 'language' then 'l'
          when i."type" = 'tablespace' then 't'
          when i."type" = 'foreign-data wrapper' then 'F'
          when i."type" = 'server' then 'S'
          else null::text
        end as acl_type
      from catalogs c
      cross join lateral pg_identify_object(c."oid", objid, case when classid = 'pg_catalog.pg_class'::regclass then objsubid else 0 end) i

      union all
      -- pg_largeobject_metadata is not qualified as object catalog for pg_identify_object (because of pg_largeobject)
      select
        quote_ident('pg_largeobject_metadata') as catalog_name,
        'lomacl' as catalog_acl_column,
        'lomowner' as catalog_owner_column,
        'large object' as obj_type,
        null as obj_schema,
        null as obj_name,
        "oid"::text as obj_identity,
        'L' as acl_type
      from pg_catalog.pg_largeobject_metadata
      where "oid" = objid
    ) a;


    if v_identity_rec.catalog_name is null then
      raise warning 'Can''t identify object with classid (system catalog table''s OID) % and object''s OID % and objsubid (column number) % or this object does not support GRANT/REVOKE permissions on it',
        classid,
        objid,
        objsubid;

      return null;
    end if;

    -- Check if object is a domain
    if classid = 'pg_catalog.pg_type'::regclass then
      select typtype = 'd'
      into v_domain_check
      from pg_catalog.pg_type
      where "oid" = objid;
    end if;

    -- Building object type clause
    select
      case
        when v_identity_rec.obj_type = 'type' and v_domain_check is true then 'DOMAIN'
        when v_identity_rec.obj_type = 'type' and v_domain_check is not true then 'TYPE'
        when v_identity_rec.obj_type in ('table', 'foreign table', 'view', 'materialized view', 'toast table') then 'TABLE'
        when v_identity_rec.obj_type like '%column' then 'TABLE'
        when v_identity_rec.obj_type = 'foreign-data wrapper' then 'FOREIGN DATA WRAPPER'
        when v_identity_rec.obj_type = 'server' then 'FOREIGN SERVER'
        when v_identity_rec.obj_type = 'statistics object' then 'STATISTICS'
        when v_identity_rec.obj_type = 'aggregate' then 'FUNCTION'
        when v_identity_rec.obj_type in ('sequence', 'schema', 'tablespace', 'database', 'function', 'procedure', 'language', 'large object') then upper(v_identity_rec.obj_type)
        else null::text
      end
    into v_object_clause;

    -- Constructing ACLs for an object
    execute format
                (
                    $fmt$
                            select c.%1$I as obj_acl,
                                   acldefault(%2$L, coalesce(%3$I, 'postgres'::regrole)) as default_acl
                            from pg_catalog.%4$I c
                            %5$s
                            where %6$s = %7$L::oid
                                  and %8$s = %9L
                    $fmt$,
                    v_identity_rec.catalog_acl_column,
                    v_identity_rec.acl_type,
                    v_identity_rec.catalog_owner_column,
                    v_identity_rec.catalog_name,
                    case when v_identity_rec.catalog_name = quote_ident('pg_attribute')
                              then 'join pg_catalog.pg_class cl on c.attrelid = cl."oid"'
                         else ''
                    end,
                    case when v_identity_rec.catalog_name = quote_ident('pg_attribute')
                              then 'cl."oid"'
                         else 'c."oid"'
                    end,
                    objid,
                    case when v_identity_rec.catalog_name = quote_ident('pg_attribute')
                              then 'c.attnum'
                         else '0'
                    end,
                    objsubid
                )
    into v_acl_rec;

    -- Default ACL
      -- All lines are commented because default ACLs is for information purposes only and normally shouldn't be changed without serious considerations
    if v_acl_rec.default_acl is not null and v_acl_rec.default_acl::text[] != '{}'::text[] then

      select E'-- Implicit default ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_rec.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON ' || v_object_clause || ' ' || v_identity_rec.obj_identity || ' TO public GRANTED BY ' || quote_ident(rg.rolname)
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       when a.grantee = 0::oid and cardinality(v_acl_rec.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON ' || v_object_clause || ' ' || v_identity_rec.obj_identity || ' TO ' || quote_ident(rg.rolname)
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            '-- REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON ' || v_object_clause || ' ' || v_identity_rec.obj_identity || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || ';'
      into v_def_acl
      from aclexplode(v_acl_rec.default_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_def_acl := '';

    end if;

    -- Non-default ACL
    if v_acl_rec.obj_acl is not null and v_acl_rec.obj_acl::text[] != '{}'::text[] then

      select E'-- Explicitly defined ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_rec.obj_acl) = 1
                            then 'GRANT '
                                 || a.privilege_type
                                 || case when v_identity_rec.obj_type like '%column'
                                              then ' (' || replace(v_identity_rec.obj_identity, v_identity_rec.obj_schema || '.' || v_identity_rec.obj_name || '.', '') || ')'
                                         else ''
                                    end
                                 || ' ON ' || v_object_clause || ' '
                                 || case when v_identity_rec.obj_type like '%column'
                                              then v_identity_rec.obj_schema || '.' || v_identity_rec.obj_name
                                         else v_identity_rec.obj_identity
                                    end
                                 || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)

                       else
                            'REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type
                            || case when v_identity_rec.obj_type like '%column'
                                         then ' (' || replace(v_identity_rec.obj_identity, v_identity_rec.obj_schema || '.' || v_identity_rec.obj_name || '.', '') || ')'
                                    else ''
                               end
                            || ' ON ' || v_object_clause || ' '
                            || case when v_identity_rec.obj_type like '%column'
                                         then v_identity_rec.obj_schema || '.' || v_identity_rec.obj_name
                                    else v_identity_rec.obj_identity
                               end
                            || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
            || E';\n\n\n'
      into v_obj_acl
      from aclexplode(v_acl_rec.obj_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_obj_acl := '';

    end if;

    -- Building DCL

    v_acl_dcl := nullif(rtrim(v_obj_acl || v_def_acl, E'\n'), '');

    return v_acl_dcl;

  end;

$$;

comment on function get_object_acl_drop(classid oid, objid oid, objsubid anycompatiblenonarray, grant_option_only bool, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for removing ACL item from an object by OID of parent catalog, object''s OID and object''s sub-id (e.g. column number); if grant_option_only is true (false by default) then it adds GRANT OPTION FOR to REVOKE command; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';