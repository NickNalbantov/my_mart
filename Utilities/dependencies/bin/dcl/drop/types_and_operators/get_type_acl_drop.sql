-- Examples:

-- select get_type_acl_drop(t.typname, n.nspname),
--        get_type_acl_drop(t."oid", true, false)
-- from pg_catalog.pg_type t
-- join pg_catalog.pg_namespace n on t.typnamespace = n."oid";

-- select get_type_acl_drop('integer');


-- Name + schema

create or replace function get_type_acl_drop
  (
    in_type text,
    in_schema text default null,
    grant_option_only bool default false,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Same DCL can be obtained with get_object_acl_drop function as well

-- Note that not only REVOKE commands are removing ACL objects, but some GRANT commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command
-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Generates DCL script for all kind of types (base types, composite types, pseudotypes, ranges, multiranges, enums) and domains

-- Examples:

-- select get_type_acl_drop('integer');

-- select get_type_acl_drop(typname)
-- from pg_catalog.pg_type
-- where typnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_acl_dcl text;
    v_acl_params record;

    v_def_acl text;
    v_obj_acl text;

    in_type_fixed text;
    int_syn text[] := array['integer', 'int'];
    smallint_syn text := 'smallint';
    bigint_syn text := 'bigint';
    num_syn text := 'decimal';
    real_syn text := 'real';
    dp_syn text[] := array['double precision', 'float'];
    time_syn text := 'time without time zone';
    timetz_syn text := 'time with time zone';
    timestamp_syn text := 'timestamp without time zone';
    timestamptz_syn text := 'timestamp with time zone';
    bpchar_syn text := 'character';
    varchar_syn text[] := array['character varying', 'char varying'];
    varbit_syn text := 'bit varying';
    bool_syn text := 'boolean';

  begin

    -- Catching synonyms for type
    select case when right(tp, 2) = '[]' then rtrim('_' || tp, '[]')
           else tp end
    into in_type_fixed
    from
    (
      select
        case when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = any(int_syn) then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1int4\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = smallint_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1int2\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = bigint_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1int8\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = num_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1numeric\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = real_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1float4\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = any(dp_syn) then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1float8\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = time_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1time\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = timetz_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1timetz\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = timestamp_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1timestamp\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = timestamptz_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1timestamptz\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = bpchar_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1bpchar\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = any(varchar_syn) then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1varchar\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = varbit_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1varbit\2')
             when lower(regexp_replace(rtrim(in_type, '[]'), '^_', '')) = bool_syn then regexp_replace(in_type, '(^|^_).+[^(\[\]|$)]', '\1bool\2')
        else in_type end as tp
    ) a;

    -- Checking existence of type
    select acldefault('T', current_user::regrole) as default_acl,
           t.typacl,
           t.typtype, -- type class: normal or domain
           n.nspname
    into v_acl_params
    from pg_catalog.pg_type t
    left join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where in_type_fixed = t.typname
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_acl_params.default_acl is null then
      raise warning 'Type %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_type);

      return null;
    end if;

    -- Default ACL
      -- All lines are commented because default ACLs is for information purposes only and normally shouldn't be changed without serious considerations
    if v_acl_params.default_acl is not null and v_acl_params.default_acl::text[] != '{}'::text[] then

      select E'-- Implicit default ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON '
                                 || case when v_acl_params.typtype = 'd' then 'DOMAIN ' else 'TYPE ' end
                                 || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_type_fixed) || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       when a.grantee = 0::oid and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON '
                                 || case when v_acl_params.typtype = 'd' then 'DOMAIN ' else 'TYPE ' end
                                 || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_type_fixed) || ' TO ' || quote_ident(rg.rolname)
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            '-- REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON '
                            || case when v_acl_params.typtype = 'd' then 'DOMAIN ' else 'TYPE ' end
                            || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_type_fixed) || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || ';'
      into v_def_acl
      from aclexplode(v_acl_params.default_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_def_acl := '';

    end if;

    -- Non-default ACL
    if v_acl_params.typacl is not null and v_acl_params.typacl::text[] != '{}'::text[] then

      select E'-- Explicitly defined ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.typacl) = 1
                            then 'GRANT ' || a.privilege_type || ' ON '
                                 || case when v_acl_params.typtype = 'd' then 'DOMAIN ' else 'TYPE ' end
                                 || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_type_fixed) || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            'REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON '
                            || case when v_acl_params.typtype = 'd' then 'DOMAIN ' else 'TYPE ' end
                            || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_type_fixed) || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || E';\n\n\n'
      into v_obj_acl
      from aclexplode(v_acl_params.typacl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_obj_acl := '';

    end if;

    -- Building DCL

    v_acl_dcl := nullif(rtrim(v_obj_acl || v_def_acl, E'\n'), '');

    return v_acl_dcl;

  end;

$$;

comment on function get_type_acl_drop(in_type text, in_schema text, grant_option_only bool, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for removing ACL item for a type / domain by its name and schema (default - search_path); if grant_option_only is true (false by default) then it adds GRANT OPTION FOR to REVOKE command; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';


-- OID

create or replace function get_type_acl_drop
  (
    type_oid oid,
    grant_option_only bool default false,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Same DCL can be obtained with get_object_acl_drop function as well

-- Note that not only REVOKE commands are removing ACL objects, but some GRANT commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command
-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Generates DCL script for all kind of types (base types, composite types, pseudotypes, ranges, multiranges, enums) and domains

-- Examples:

-- select get_type_acl_drop('integer'::regtype);

-- select get_type_acl_drop("oid", true, false)
-- from pg_catalog.pg_type
-- limit 10;

  declare

    v_acl_dcl text;
    v_acl_params record;

    v_def_acl text;
    v_obj_acl text;

  begin

    -- Checking existence of type
    select acldefault('T', current_user::regrole) as default_acl,
           t.typacl,
           t.typtype, -- type class: normal or domain
           t.typname,
           n.nspname
    into v_acl_params
    from pg_catalog.pg_type t
    join pg_catalog.pg_namespace n on n.oid = t.typnamespace
    where type_oid = t."oid";

    if v_acl_params.default_acl is null then
      raise warning 'Type with OID % does not exist', type_oid;

      return null;
    end if;

    -- Default ACL
      -- All lines are commented because default ACLs is for information purposes only and normally shouldn't be changed without serious considerations
    if v_acl_params.default_acl is not null and v_acl_params.default_acl::text[] != '{}'::text[] then

      select E'-- Implicit default ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON '
                                 || case when v_acl_params.typtype = 'd' then 'DOMAIN ' else 'TYPE ' end
                                 || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.typname) || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       when a.grantee = 0::oid and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON '
                                 || case when v_acl_params.typtype = 'd' then 'DOMAIN ' else 'TYPE ' end
                                 || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.typname) || ' TO ' || quote_ident(rg.rolname)
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            '-- REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON '
                            || case when v_acl_params.typtype = 'd' then 'DOMAIN ' else 'TYPE ' end
                            || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.typname) || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || ';'
      into v_def_acl
      from aclexplode(v_acl_params.default_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_def_acl := '';

    end if;

    -- Non-default ACL
    if v_acl_params.typacl is not null and v_acl_params.typacl::text[] != '{}'::text[] then

      select E'-- Explicitly defined ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.typacl) = 1
                            then 'GRANT ' || a.privilege_type || ' ON '
                                 || case when v_acl_params.typtype = 'd' then 'DOMAIN ' else 'TYPE ' end
                                 || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.typname) || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            'REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON '
                            || case when v_acl_params.typtype = 'd' then 'DOMAIN ' else 'TYPE ' end
                            || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.typname) || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || E';\n\n\n'
      into v_obj_acl
      from aclexplode(v_acl_params.typacl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_obj_acl := '';

    end if;

    -- Building DCL

    v_acl_dcl := nullif(rtrim(v_obj_acl || v_def_acl, E'\n'), '');

    return v_acl_dcl;

  end;

$$;

comment on function get_type_acl_drop(type_oid oid, grant_option_only bool, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for removing ACL item for a type / domain by its OID; if grant_option_only is true (false by default) then it adds GRANT OPTION FOR to REVOKE command; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';