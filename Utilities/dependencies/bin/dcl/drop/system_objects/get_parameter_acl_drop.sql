--- Examples:

-- select get_parameter_acl_drop(parname),
--        get_parameter_acl_drop("oid", true, false)
-- from pg_catalog.pg_parameter_acl;


-- Parameter name

create or replace function get_parameter_acl_drop
  (
    in_parname text,
    grant_option_only bool default false,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Note that not only REVOKE commands are removing ACL objects, but some GRANT commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command
-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select get_parameter_acl_drop(parname)
-- from pg_catalog.pg_parameter_acl;

  declare

    v_acl_dcl text;
    v_acl_params record;

    v_def_acl text;
    v_obj_acl text;

  begin

    -- Checking existence of ACL entry
    select acldefault('p', current_user::regrole) as default_acl,
           paracl
    into v_acl_params
    from pg_catalog.pg_parameter_acl
    where parname = in_parname;

    if v_acl_params.default_acl is null then
      raise warning 'Parameter ACL entry for parameter ''%'' does not exist', in_parname;

      return null;
    end if;

    -- Default ACL
      -- All lines are commented because default ACLs is for information purposes only and normally shouldn't be changed without serious considerations
    if v_acl_params.default_acl is not null and v_acl_params.default_acl::text[] != '{}'::text[] then

      select E'-- Implicit default ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON PARAMETER '
                                 || in_parname || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       when a.grantee = 0::oid and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON PARAMETER '
                                 || in_parname || ' TO ' || quote_ident(rg.rolname)
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            '-- REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON PARAMETER '
                            || in_parname || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || ';'
      into v_def_acl
      from aclexplode(v_acl_params.default_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_def_acl := '';

    end if;

    -- Non-default ACL
    if v_acl_params.paracl is not null and v_acl_params.paracl::text[] != '{}'::text[] then

      select E'-- Explicitly defined ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.paracl) = 1
                            then 'GRANT ' || a.privilege_type
                                 || ' ON PARAMETER ' || in_parname || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            'REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON PARAMETER ' || in_parname || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || E';\n\n\n'
      into v_obj_acl
      from aclexplode(v_acl_params.paracl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_obj_acl := '';

    end if;

    -- Building DCL

    v_acl_dcl := nullif(rtrim(v_obj_acl || v_def_acl, E'\n'), '');

    return v_acl_dcl;

  end;

$$;

comment on function get_parameter_acl_drop(paracl_oid text, grant_option_only bool, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for removing ACL item for a parameter by its name; if grant_option_only is true (false by default) then it adds GRANT OPTION FOR to REVOKE command; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';


-- OID

create or replace function get_parameter_acl_drop
  (
    paracl_oid oid,
    grant_option_only bool default false,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Note that not only GRANT commands are creating ACL objects, but some REVOKE commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command
-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select get_parameter_acl_drop("oid", true, false)
-- from pg_catalog.pg_parameter_acl;

  declare

    v_acl_dcl text;
    v_acl_params record;

    v_def_acl text;
    v_obj_acl text;

  begin

    -- Checking existence of ACL entry
    select parname,
           acldefault('p', current_user::regrole) as default_acl,
           paracl
    into v_acl_params
    from pg_catalog.pg_parameter_acl
    where "oid" = paracl_oid;

    if v_acl_params.default_acl is null then
      raise warning 'Parameter ACL entry with OID % does not exist', paracl_oid;

      return null;
    end if;

    -- Default ACL
      -- All lines are commented because default ACLs is for information purposes only and normally shouldn't be changed without serious considerations
    if v_acl_params.default_acl is not null and v_acl_params.default_acl::text[] != '{}'::text[] then

      select E'-- Implicit default ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON PARAMETER '
                                 || v_acl_params.parname || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       when a.grantee = 0::oid and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON PARAMETER '
                                 || v_acl_params.parname || ' TO ' || quote_ident(rg.rolname)
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            '-- REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON PARAMETER '
                            || v_acl_params.parname || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || ';'
      into v_def_acl
      from aclexplode(v_acl_params.default_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_def_acl := '';

    end if;

    -- Non-default ACL
    if v_acl_params.paracl is not null and v_acl_params.paracl::text[] != '{}'::text[] then

      select E'-- Explicitly defined ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.paracl) = 1
                            then 'GRANT ' || a.privilege_type
                                 || ' ON PARAMETER ' || v_acl_params.parname || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            'REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON PARAMETER ' || v_acl_params.parname || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || E';\n\n\n'
      into v_obj_acl
      from aclexplode(v_acl_params.paracl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_obj_acl := '';

    end if;

    -- Building DCL

    v_acl_dcl := nullif(rtrim(v_obj_acl || v_def_acl, E'\n'), '');

    return v_acl_dcl;

  end;

$$;

comment on function get_parameter_acl_drop(paracl_oid oid, grant_option_only bool, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for removing ACL item for a parameter by its OID; if grant_option_only is true (false by default) then it adds GRANT OPTION FOR to REVOKE command; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';