-- Examples:

-- select get_table_acl_drop(c.relname, n.nspname),
--        get_table_acl_drop(c."oid", true, false)
-- from pg_catalog.pg_class c
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where c.relkind in ('r', 'p', 't', 'f', 'v', 'm');

-- select get_table_acl_drop('pg_trigger');


-- Name + schema

create or replace function get_table_acl_drop
  (
    in_table text,
    in_schema text default null,
    grant_option_only bool default false,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Same DCL can be obtained with get_object_acl_drop function as well

-- Note that not only REVOKE commands are removing ACL objects, but some GRANT commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command
-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Examples:

-- select get_table_acl_drop('pg_trigger');

-- select get_table_acl_drop(relname)
-- from pg_catalog.pg_class
-- where relkind in ('r', 'p', 't', 'f', 'v', 'm')
--       and relnamespace = 'pg_catalog'::regnamespace
-- limit 10;

  declare

    v_acl_dcl text;
    v_acl_params record;

    v_def_acl text;
    v_obj_acl text;

  begin

    -- Checking existence of table
    select acldefault('r', current_user::regrole) as default_acl,
           c.relacl,
           n.nspname
    into v_acl_params
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where c.relkind in ('r', 'p', 't', 'f', 'v', 'm')
          and c.relname = in_table
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_acl_params.default_acl is null then

      raise warning 'Table %.% does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_table);

      return null;

    end if;

    -- Default ACL
      -- All lines are commented because default ACLs is for information purposes only and normally shouldn't be changed without serious considerations
    if v_acl_params.default_acl is not null and v_acl_params.default_acl::text[] != '{}'::text[] then

      select E'-- Implicit default ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON TABLE '
                                 || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_table) || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       when a.grantee = 0::oid and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON TABLE '
                                 || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_table) || ' TO ' || quote_ident(rg.rolname)
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            '-- REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON TABLE '
                            || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_table) || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || ';'
      into v_def_acl
      from aclexplode(v_acl_params.default_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_def_acl := '';

    end if;

    -- Non-default ACL
    if v_acl_params.relacl is not null and v_acl_params.relacl::text[] != '{}'::text[] then

      select E'-- Explicitly defined ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.relacl) = 1
                            then 'GRANT ' || a.privilege_type
                                 || ' ON TABLE ' || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_table) || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            'REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON TABLE ' || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_table) || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || E';\n\n\n'
      into v_obj_acl
      from aclexplode(v_acl_params.relacl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_obj_acl := '';

    end if;

    -- Building DCL

    v_acl_dcl := nullif(rtrim(v_obj_acl || v_def_acl, E'\n'), '');

    return v_acl_dcl;

  end;

$$;

comment on function get_table_acl_drop(in_table text, in_schema text, grant_option_only bool, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for removing ACL item for a table by its name and schema (default - search_path); if grant_option_only is true (false by default) then it adds GRANT OPTION FOR to REVOKE command; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';


-- OID

create or replace function get_table_acl_drop
  (
    table_oid oid,
    grant_option_only bool default false,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Same DCL can be obtained with get_object_acl_drop function as well

-- Note that not only REVOKE commands are removing ACL objects, but some GRANT commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command
-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Examples:

-- select get_table_acl_drop('pg_trigger'::regclass);

-- select get_table_acl_drop("oid", true, false)
-- from pg_catalog.pg_class
-- where relkind in ('r', 'p', 't', 'f', 'v', 'm')
-- limit 10;

  declare

    v_acl_dcl text;
    v_acl_params record;

    v_def_acl text;
    v_obj_acl text;

  begin

    -- Checking existence of table
    select acldefault('r', current_user::regrole) as default_acl,
           c.relacl,
           c.relname,
           n.nspname
    into v_acl_params
    from pg_catalog.pg_class c
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where c.relkind in ('r', 'p', 't', 'f', 'v', 'm')
          and c."oid" = table_oid;

    if v_acl_params.default_acl is null then
      raise warning 'Table with OID % does not exist', table_oid;

      return null;
    end if;

    -- Default ACL
      -- All lines are commented because default ACLs is for information purposes only and normally shouldn't be changed without serious considerations
    if v_acl_params.default_acl is not null and v_acl_params.default_acl::text[] != '{}'::text[] then

      select E'-- Implicit default ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON TABLE '
                                 || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname) || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       when a.grantee = 0::oid and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type || ' ON TABLE '
                                 || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname) || ' TO ' || quote_ident(rg.rolname)
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            '-- REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON TABLE '
                            || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname) || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || ';'
      into v_def_acl
      from aclexplode(v_acl_params.default_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_def_acl := '';

    end if;

    -- Non-default ACL
    if v_acl_params.relacl is not null and v_acl_params.relacl::text[] != '{}'::text[] then

      select E'-- Explicitly defined ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.relacl) = 1
                            then 'GRANT ' || a.privilege_type
                                 || ' ON TABLE ' || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname) || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            'REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON TABLE ' || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname) || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || E';\n\n\n'
      into v_obj_acl
      from aclexplode(v_acl_params.relacl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_obj_acl := '';

    end if;

    -- Building DCL

    v_acl_dcl := nullif(rtrim(v_obj_acl || v_def_acl, E'\n'), '');

    return v_acl_dcl;

  end;

$$;

comment on function get_table_acl_drop(table_oid oid, grant_option_only bool, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for removing ACL item for a table by its OID; if grant_option_only is true (false by default) then it adds GRANT OPTION FOR to REVOKE command; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';