-- Examples:

-- select get_aggregate_acl_drop('max', array['bigint']),
--        get_aggregate_acl_drop('count'),
--        get_aggregate_acl_drop('percentile_cont', array['float'], array['interval']),
--        get_aggregate_acl_drop('mode', array['anyelement']);

-- select get_aggregate_acl_drop(proname, direct_typnames, order_typnames, nspname, direct_typschemas, order_typschemas),
--        get_aggregate_acl_drop("oid", true, false)
-- from
-- (
--     select p."oid",
--            p.proname,
--            n.nspname,
--            case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typnames,
--            case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typnames,
--            case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typschemas,
--            case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typschemas
--     from pg_catalog.pg_proc p
--     join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
--     join pg_catalog.pg_aggregate a on a.aggfnoid = p."oid"
--     cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
--     left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
--     left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
--     where prokind = 'a'
--     group by p."oid",
--              p.proname,
--              n.nspname,
--              a.aggnumdirectargs
-- ) a;


-- Name + schema + signature arguments

create or replace function get_aggregate_acl_drop
  (
    in_agg text,
    in_direct_arg_types text[] default null,
    in_order_arg_types text[] default null,
    in_schema text default null,
    in_direct_arg_type_schemas text[] default null,
    in_order_arg_type_schemas text[] default null,
    grant_option_only bool default false,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Same DCL can be obtained with get_object_acl_drop function as well

-- Generates DCL for all aggregates, but not for functions and procedures - use get_function_acl_drop instead

-- Note that not only REVOKE commands are removing ACL objects, but some GRANT commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command
-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select get_aggregate_acl_drop('max', array['bigint']),
--        get_aggregate_acl_drop('percentile_cont', array['float'], array['interval']);

  declare

    v_acl_params record;
    v_direct_args text := '';
    v_order_args text := '';
    v_agg_identity text;

    v_acl_dcl text;
    v_def_acl text;
    v_obj_acl text;

    in_arg_types_fixed text[];
    in_arg_types_oids oid[];
    v_typoids_rec record;

    input_direct_args_excptn text;
    input_order_args_excptn text;

    int_syn text[] := array['integer', 'int'];
    smallint_syn text := 'smallint';
    bigint_syn text := 'bigint';
    num_syn text := 'decimal';
    real_syn text := 'real';
    dp_syn text[] := array['double precision', 'float'];
    time_syn text := 'time without time zone';
    timetz_syn text := 'time with time zone';
    timestamp_syn text := 'timestamp without time zone';
    timestamptz_syn text := 'timestamp with time zone';
    bpchar_syn text := 'character';
    varchar_syn text[] := array['character varying', 'char varying'];
    varbit_syn text := 'bit varying';
    bool_syn text := 'boolean';

  begin

    -- Comparing in_direct_arg_types and in_direct_arg_type_schemas input arguments
    if coalesce(cardinality(in_direct_arg_type_schemas), 0) < coalesce(cardinality(in_direct_arg_types), 0) then
      for in_card in 1..coalesce(cardinality(in_direct_arg_types), 0) - coalesce(cardinality(in_direct_arg_type_schemas), 0)
      loop
        in_direct_arg_type_schemas := array_append(in_direct_arg_type_schemas, null);
      end loop;
    elsif coalesce(cardinality(in_direct_arg_type_schemas), 0) > coalesce(cardinality(in_direct_arg_types), 0) then
      raise warning 'There are more types'' schemas specified in in_direct_arg_type_schemas argument than types'' names specified in in_direct_arg_types argument';

      return null;
    end if;

    -- Comparing in_order_arg_types and in_order_arg_type_schemas input arguments
    if coalesce(cardinality(in_order_arg_type_schemas), 0) < coalesce(cardinality(in_order_arg_types), 0) then
      for in_card in 1..coalesce(cardinality(in_order_arg_types), 0) - coalesce(cardinality(in_order_arg_type_schemas), 0)
      loop
        in_order_arg_type_schemas := array_append(in_order_arg_type_schemas, null);
      end loop;
    elsif coalesce(cardinality(in_order_arg_type_schemas), 0) > coalesce(cardinality(in_order_arg_types), 0) then
      raise warning 'There are more types'' schemas specified in in_order_arg_type_schemas argument than types'' names specified in in_order_arg_types argument';

      return null;
    end if;

    -- Catching synonyms for arguments' types
    select array_agg(case when right(tps_sw, 2) = '[]' then rtrim('_' || tps_sw, '[]')
                          else tps_sw end
                     order by rn
                    )
    into in_arg_types_fixed
    from
    (
      select rn,
        case when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = any(int_syn) then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1int4\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = smallint_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1int2\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = bigint_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1int8\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = num_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1numeric\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = real_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1float4\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = any(dp_syn) then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1float8\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = time_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1time\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = timetz_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1timetz\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = timestamp_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1timestamp\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = timestamptz_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1timestamptz\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = bpchar_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1bpchar\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = any(varchar_syn) then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1varchar\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = varbit_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1varbit\2')
             when trim(regexp_replace(tps, '(^|^_|.+\.|.+\._)', ''), '_[]') = bool_syn then regexp_replace(tps, '(^|_|.+\.|.+\._).+[^(\[\]|$)]', '\1bool\2')
        else tps end as tps_sw
      from
        (
          select lower(u.tps) as tps, u.rn
          from unnest(coalesce(in_direct_arg_types, array['']::text[]) || coalesce(in_order_arg_types, array['']::text[])) with ordinality u (tps, rn)
        ) a
        where tps != ''
    ) b;

    -- Getting OIDs for signature arguments
    if in_direct_arg_types is null and in_order_arg_types is null then
      in_arg_types_oids := array[0]::oid[];

    else
      for v_typoids_rec in
          select t."oid",
                 tf.rn
          from pg_catalog.pg_type t
          join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
          join unnest(in_arg_types_fixed) with ordinality tf (tps, rn) on tf.tps = t.typname
          join unnest(in_direct_arg_type_schemas || in_order_arg_type_schemas) with ordinality ts (arg_sch, rn) on tf.rn = ts.rn
          left join lateral unnest(current_schemas(true)) with ordinality s (sch, rn) on 1 = 1
          where coalesce(ts.arg_sch, s.sch) = n.nspname
          group by t."oid",
                   tf.rn
          order by tf.rn
      loop
        in_arg_types_oids[v_typoids_rec.rn] := v_typoids_rec."oid";
      end loop;

      in_arg_types_oids := array_replace(in_arg_types_oids, null, 0::oid);

    end if;

    -- Checking existence of aggregate function
    select b."oid",
           n.nspname,
           b.proacl,
           b.default_acl,
           b.aggkind,
           b.aggnumdirectargs,
           jsonb_object_agg(b.rn, jsonb_build_object('argtypes', b.argtypes, 'argmodes', b.argmodes, 'argnames', b.argnames)) as arg_params,
           max(b.rn) as arg_quantity
    into v_acl_params
    from
    (
        select a."oid",
               a.pronamespace,
               a.proacl,
               a.default_acl,
               a.aggkind,
               a.aggnumdirectargs,
               quote_ident(nt.nspname) || '.' || quote_ident(t.typname) as argtypes,
               (case a.argmodes when 'o' then 'OUT'
                                when 'b' then 'INOUT'
                                when 'v' then 'VARIADIC'
                                when 't' then 'TABLE'
                else 'IN' end
               )::text as argmodes,
               a.argnames,
               case when in_argtypes is null then 0 else row_number() over(partition by a."oid") end as rn
        from
        (
          select p."oid",
                 p.pronamespace,
                 p.proacl,
                 acldefault('f', current_user::regrole) as default_acl,
                 a.aggkind,
                 a.aggnumdirectargs,
                 unnest(string_to_array(case when p.proargtypes = ''::oidvector then null::text else p.proargtypes::text end, ' ')::oid[]) as in_argtypes,
                 unnest(coalesce(p.proallargtypes, array[0]::oid[])) as argtypes,
                 unnest(p.proargmodes) as argmodes,
                 unnest(p.proargnames) as argnames
          from pg_catalog.pg_aggregate a
          join pg_catalog.pg_proc p on a.aggfnoid = p."oid"
          where p.prokind = 'a'
                and p.proname = in_agg
                and string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[] = in_arg_types_oids
        ) a
        left join pg_catalog.pg_type t on coalesce(a.in_argtypes, a.argtypes) = t."oid"
        left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    ) b
    left join pg_catalog.pg_namespace n on n."oid" = b.pronamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where n.nspname = coalesce(in_schema, s.sch)
    group by b."oid",
             n.nspname,
             b.proacl,
             b.default_acl,
             b.aggkind,
             b.aggnumdirectargs,
             s.rn
    order by s.rn
    limit 1;

    if v_acl_params."oid" is null then

      select string_agg(coalesce(quote_ident(ts.arg_sch), '[' || array_to_string(current_schemas(true), ', ') || ']') || '.' || quote_ident(tf.tps), ', ' order by ts.rn)
      into input_direct_args_excptn
      from unnest(in_direct_arg_type_schemas) with ordinality ts (arg_sch, rn)
      join unnest(in_arg_types_fixed[1:cardinality(in_direct_arg_types)]) with ordinality tf (tps, rn) on tf.rn = ts.rn;

      select string_agg(coalesce(quote_ident(ts.arg_sch), '[' || array_to_string(current_schemas(true), ', ') || ']') || '.' || quote_ident(tf.tps), ', ' order by ts.rn)
      into input_order_args_excptn
      from unnest(in_order_arg_type_schemas) with ordinality ts (arg_sch, rn)
      join unnest(in_arg_types_fixed[cardinality(in_direct_arg_types) + 1:]) with ordinality tf (tps, rn) on tf.rn = ts.rn;

      raise warning 'Aggregate %.%(%) does not exist',
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_agg),
        case when in_direct_arg_types is not null then input_direct_args_excptn else '' end ||
        case when in_direct_arg_types is not null and in_order_arg_types is not null then ' ' else '' end ||
        case when in_order_arg_types is not null then 'ORDER BY ' || input_order_args_excptn else '' end;

      return null;
    end if;

    -- Getting final list of arguments

    -- Direct arguments
    if v_acl_params.arg_quantity = 0 then  -- Functions with no parameters, like count(*)
        v_direct_args = '*';

    elsif v_acl_params.arg_quantity != 0 and v_acl_params.aggkind = 'n' then -- normal aggregates
      <<direct_args>>
      for arg_num in 1..v_acl_params.arg_quantity
      loop
          v_direct_args := v_direct_args || ', ' ||
                           (v_acl_params.arg_params -> arg_num::text ->> 'argmodes')::text ||
                           case when coalesce((v_acl_params.arg_params -> arg_num::text ->> 'argnames')::text, '') != ''
                                then ' ' || (v_acl_params.arg_params -> arg_num::text ->> 'argnames')::text
                                else '' end ||
                           case when (v_acl_params.arg_params -> arg_num::text ->> 'argtypes')::text is null
                                then ''
                                else ' ' || (v_acl_params.arg_params -> arg_num::text ->> 'argtypes')::text end;
      end loop direct_args;

    else -- hypothetical and ordered-set aggregates
      <<direct_args_ord>>
      for arg_num in 1..v_acl_params.aggnumdirectargs
      loop
          v_direct_args := v_direct_args || ', ' ||
                           (v_acl_params.arg_params -> arg_num::text ->> 'argmodes')::text ||
                           case when coalesce((v_acl_params.arg_params -> arg_num::text ->> 'argnames')::text, '') != ''
                                then ' ' || (v_acl_params.arg_params -> arg_num::text ->> 'argnames')::text
                                else '' end ||
                           case when (v_acl_params.arg_params -> arg_num::text ->> 'argtypes')::text is null
                                then ''
                                else ' ' || (v_acl_params.arg_params -> arg_num::text ->> 'argtypes')::text end;
      end loop direct_args_ord;

    end if;

    -- Sorting arguments
    if v_acl_params.aggkind in ('o', 'h') and (v_acl_params.arg_params -> v_acl_params.aggnumdirectargs::text ->> 'argmodes')::text = 'VARIADIC' then -- hypothetical functions with VARIADIC arguments
      v_order_args := 'VARIADIC'
                      || case when coalesce((v_acl_params.arg_params -> v_acl_params.aggnumdirectargs::text ->> 'argnames')::text, '') != ''
                                   then ' ' || (v_acl_params.arg_params -> v_acl_params.aggnumdirectargs::text ->> 'argnames')::text
                         else '' end
                      || ' pg_catalog.any';
    else
      <<order_args>>
      for arg_num in (v_acl_params.aggnumdirectargs + 1)..v_acl_params.arg_quantity
      loop
          v_order_args := v_order_args || ', ' ||
                          (v_acl_params.arg_params -> arg_num::text ->> 'argmodes')::text ||
                          case when coalesce((v_acl_params.arg_params -> arg_num::text ->> 'argnames')::text, '') != ''
                               then ' ' || (v_acl_params.arg_params -> arg_num::text ->> 'argnames')::text
                               else '' end ||
                          case when (v_acl_params.arg_params -> arg_num::text ->> 'argtypes')::text is null
                               then ''
                               else ' ' || (v_acl_params.arg_params -> arg_num::text ->> 'argtypes')::text end;
      end loop order_args;

    end if;

    -- Hypothetical and ordered-set aggregates
    if v_acl_params.aggkind in ('o', 'h') then
      v_agg_identity := quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_agg) || '('
                        || case when v_direct_args != '' then trim(v_direct_args, ', ') || ' ' else '' end
                        || 'ORDER BY ' || trim(v_order_args, ', ') || ')';

    -- Normal aggregates
    elsif v_acl_params.aggkind = 'n' then
      v_agg_identity := quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_agg) || '(' || trim(v_direct_args, ', ') || ')';
    end if;

    -- Default ACL
      -- All lines are commented because default ACLs is for information purposes only and normally shouldn't be changed without serious considerations
    if v_acl_params.default_acl is not null and v_acl_params.default_acl::text[] != '{}'::text[] then

      select E'-- Implicit default ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type
                                 || ' ON FUNCTION ' || v_agg_identity || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       when a.grantee = 0::oid and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type
                                 || ' ON FUNCTION ' || v_agg_identity || ' TO ' || quote_ident(rg.rolname)
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            '-- REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type
                            || ' ON FUNCTION ' || v_agg_identity || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || ';'
      into v_def_acl
      from aclexplode(v_acl_params.default_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_def_acl := '';

    end if;

    -- Non-default ACL
    if v_acl_params.proacl is not null and v_acl_params.proacl::text[] != '{}'::text[] then

      select E'-- Explicitly defined ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.proacl) = 1
                            then 'GRANT ' || a.privilege_type
                                 || ' ON FUNCTION ' || v_agg_identity || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            'REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON FUNCTION ' || v_agg_identity || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || E';\n\n\n'
      into v_obj_acl
      from aclexplode(v_acl_params.proacl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_obj_acl := '';

    end if;

    -- Building DCL

    v_acl_dcl := nullif(rtrim(v_obj_acl || v_def_acl, E'\n'), '');

    return v_acl_dcl;

  end;

$$;

comment on function get_aggregate_acl_drop(in_agg text, in_direct_arg_types text[], in_order_arg_types text[], in_schema text, in_direct_arg_type_schemas text[], in_order_arg_type_schemas text[], grant_option_only bool, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for removing ACL item for an aggregate function by its name, schema (default - search_path) and signature arguments (as text arrays of types'' names and schemas (default - search_path)); if grant_option_only is true (false by default) then it adds GRANT OPTION FOR to REVOKE command; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';


-- OID

create or replace function get_aggregate_acl_drop
  (
    agg_oid oid,
    grant_option_only bool default false,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Same DCL can be obtained with get_object_acl_drop function as well

-- Generates DCL for all aggregates, but not for functions and procedures - use get_function_acl_drop instead

-- Note that not only REVOKE commands are removing ACL objects, but some GRANT commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command
-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Examples:

-- select get_aggregate_acl_drop('max(bigint)'::regprocedure),
--        get_aggregate_acl_drop('percentile_cont(float, interval)'::regprocedure);

-- select get_aggregate_acl_drop(aggfnoid, true, false)
-- from pg_catalog.pg_aggregate
-- limit 10;

  declare

    v_acl_params record;
    v_direct_args text := '';
    v_order_args text := '';
    v_agg_identity text;

    v_acl_dcl text;
    v_def_acl text;
    v_obj_acl text;

  begin

    -- Checking existence of aggregate function
    select proname,
           nspname,
           proacl,
           default_acl,
           aggkind,
           aggnumdirectargs,
           jsonb_object_agg(rn, jsonb_build_object('argtypes', argtypes, 'argmodes', argmodes, 'argnames', argnames)) as arg_params,
           max(rn) as arg_quantity
    into v_acl_params
    from
    (
        select a.proname,
               a.nspname,
               a.proacl,
               a.default_acl,
               a.aggkind,
               a.aggnumdirectargs,
               quote_ident(nt.nspname) || '.' || quote_ident(t.typname) as argtypes,
               (case a.argmodes when 'o' then 'OUT'
                                when 'b' then 'INOUT'
                                when 'v' then 'VARIADIC'
                                when 't' then 'TABLE'
                else 'IN' end
               )::text as argmodes,
               a.argnames,
               case when in_argtypes is null then 0 else row_number() over(partition by a."oid") end as rn
        from
        (
          select p."oid",
                 p.proname,
                 n.nspname,
                 p.proacl,
                 acldefault('f', current_user::regrole) as default_acl,
                 a.aggkind,
                 a.aggnumdirectargs,
                 unnest(string_to_array(case when p.proargtypes = ''::oidvector then null::text else p.proargtypes::text end, ' ')::oid[]) as in_argtypes,
                 unnest(coalesce(p.proallargtypes, array[0]::oid[])) as argtypes,
                 unnest(p.proargmodes) as argmodes,
                 unnest(p.proargnames) as argnames
          from pg_catalog.pg_aggregate a
          join pg_catalog.pg_proc p on a.aggfnoid = p."oid"
          join pg_catalog.pg_namespace n on n."oid" = p.pronamespace
          where p.prokind = 'a'
                and p."oid" = agg_oid
        ) a
        left join pg_catalog.pg_type t on coalesce(a.in_argtypes, a.argtypes) = t."oid"
        left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    ) b
    group by proname,
             nspname,
             proacl,
             default_acl,
             aggkind,
             aggnumdirectargs;

    if v_acl_params.proname is null then
      raise warning 'Aggregate with OID % does not exist', agg_oid;

      return null;
    end if;

    -- Getting final list of arguments

    -- Direct arguments
    if v_acl_params.arg_quantity = 0 then  -- Functions with no parameters, like count(*)
        v_direct_args = '*';

    elsif v_acl_params.arg_quantity != 0 and v_acl_params.aggkind = 'n' then -- normal aggregates
      <<direct_args>>
      for arg_num in 1..v_acl_params.arg_quantity
      loop
          v_direct_args := v_direct_args || ', ' ||
                           (v_acl_params.arg_params -> arg_num::text ->> 'argmodes')::text ||
                           case when coalesce((v_acl_params.arg_params -> arg_num::text ->> 'argnames')::text, '') != ''
                                then ' ' || (v_acl_params.arg_params -> arg_num::text ->> 'argnames')::text
                                else '' end ||
                           case when (v_acl_params.arg_params -> arg_num::text ->> 'argtypes')::text is null
                                then ''
                                else ' ' || (v_acl_params.arg_params -> arg_num::text ->> 'argtypes')::text end;
      end loop direct_args;

    else -- hypothetical and ordered-set aggregates
      <<direct_args_ord>>
      for arg_num in 1..v_acl_params.aggnumdirectargs
      loop
          v_direct_args := v_direct_args || ', ' ||
                           (v_acl_params.arg_params -> arg_num::text ->> 'argmodes')::text ||
                           case when coalesce((v_acl_params.arg_params -> arg_num::text ->> 'argnames')::text, '') != ''
                                then ' ' || (v_acl_params.arg_params -> arg_num::text ->> 'argnames')::text
                                else '' end ||
                           case when (v_acl_params.arg_params -> arg_num::text ->> 'argtypes')::text is null
                                then ''
                                else ' ' || (v_acl_params.arg_params -> arg_num::text ->> 'argtypes')::text end;
      end loop direct_args_ord;

    end if;

    -- Sorting arguments
    if v_acl_params.aggkind in ('o', 'h') and (v_acl_params.arg_params -> v_acl_params.aggnumdirectargs::text ->> 'argmodes')::text = 'VARIADIC' then -- hypothetical functions with VARIADIC arguments
      v_order_args := 'VARIADIC'
                      || case when coalesce((v_acl_params.arg_params -> v_acl_params.aggnumdirectargs::text ->> 'argnames')::text, '') != ''
                                   then ' ' || (v_acl_params.arg_params -> v_acl_params.aggnumdirectargs::text ->> 'argnames')::text
                         else '' end
                      || ' pg_catalog.any';
    else
      <<order_args>>
      for arg_num in (v_acl_params.aggnumdirectargs + 1)..v_acl_params.arg_quantity
      loop
          v_order_args := v_order_args || ', ' ||
                          (v_acl_params.arg_params -> arg_num::text ->> 'argmodes')::text ||
                          case when coalesce((v_acl_params.arg_params -> arg_num::text ->> 'argnames')::text, '') != ''
                               then ' ' || (v_acl_params.arg_params -> arg_num::text ->> 'argnames')::text
                               else '' end ||
                          case when (v_acl_params.arg_params -> arg_num::text ->> 'argtypes')::text is null
                               then ''
                               else ' ' || (v_acl_params.arg_params -> arg_num::text ->> 'argtypes')::text end;
      end loop order_args;

    end if;

    -- Hypothetical and ordered-set aggregates
    if v_acl_params.aggkind in ('o', 'h') then
      v_agg_identity := quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.proname) || '('
                        || case when v_direct_args != '' then trim(v_direct_args, ', ') || ' ' else '' end
                        || 'ORDER BY ' || trim(v_order_args, ', ') || ')';

    -- Normal aggregates
    elsif v_acl_params.aggkind = 'n' then
      v_agg_identity := quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.proname) || '(' || trim(v_direct_args, ', ') || ')';

    end if;

    -- Default ACL
      -- All lines are commented because default ACLs is for information purposes only and normally shouldn't be changed without serious considerations
    if v_acl_params.default_acl is not null and v_acl_params.default_acl::text[] != '{}'::text[] then

      select E'-- Implicit default ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type
                                 || ' ON FUNCTION ' || v_agg_identity || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       when a.grantee = 0::oid and cardinality(v_acl_params.default_acl) = 1
                            then '-- GRANT ' || a.privilege_type
                                 || ' ON FUNCTION ' || v_agg_identity || ' TO ' || quote_ident(rg.rolname)
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            '-- REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type
                            || ' ON FUNCTION ' || v_agg_identity || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || ';'
      into v_def_acl
      from aclexplode(v_acl_params.default_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_def_acl := '';

    end if;

    -- Non-default ACL
    if v_acl_params.proacl is not null and v_acl_params.proacl::text[] != '{}'::text[] then

      select E'-- Explicitly defined ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.proacl) = 1
                            then 'GRANT ' || a.privilege_type
                                 || ' ON FUNCTION ' || v_agg_identity || ' TO public'
                                 || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                       else
                            'REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                            || a.privilege_type || ' ON FUNCTION ' || v_agg_identity || ' FROM '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                            || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || E';\n\n\n'
      into v_obj_acl
      from aclexplode(v_acl_params.proacl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_obj_acl := '';

    end if;

    -- Building DCL

    v_acl_dcl := nullif(rtrim(v_obj_acl || v_def_acl, E'\n'), '');

    return v_acl_dcl;

  end;

$$;

comment on function get_aggregate_acl_drop(agg_oid oid, grant_option_only bool, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for removing ACL item for an aggregate function by its OID; if grant_option_only is true (false by default) then it adds GRANT OPTION FOR to REVOKE command; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';