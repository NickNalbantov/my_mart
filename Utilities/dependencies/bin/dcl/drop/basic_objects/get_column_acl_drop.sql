-- Examples:

-- select get_column_acl_drop(a.attname, c.relname, n.nspname),
--        get_column_acl_drop(a.attname, c."oid"),
--        get_column_acl_drop(a.attnum, c."oid", true, false)
-- from pg_catalog.pg_attribute a
-- join pg_catalog.pg_class c on c."oid" = a.attrelid
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where a.attacl is not null;


-- Column name + table name + schema

create or replace function get_column_acl_drop
  (
    in_column text,
    in_table text,
    in_schema text default null,
    grant_option_only bool default false,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Same DCL can be obtained with get_object_acl_drop function as well

-- Note that not only REVOKE commands are removing ACL objects, but some GRANT commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command
-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select attname,
--        get_column_acl_drop(a.attname, 'pg_subscription')
-- from pg_catalog.pg_attribute
-- where attrelid = 'pg_catalog.pg_subscription'::regclass
--       and a.attnum > 0;

  declare

    v_acl_dcl text;
    v_acl_params record;

  begin

    -- Checking existence of column ACL
    select a.attrelid,
           n.nspname,
           a.attacl
    into v_acl_params
    from pg_catalog.pg_attribute a
    join pg_catalog.pg_class c on a.attrelid = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where a.attisdropped is false
          and a.attname = in_column
          and c.relname = in_table
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_acl_params.attrelid is null then

      raise warning 'Column % of object %.% does not exist',
        quote_ident(in_column),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_table);

      return null;

    elsif v_acl_params.attrelid is not null and v_acl_params.attacl is null then

      raise warning 'ACL for column % of object %.% is empty',
        quote_ident(in_column),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_table);

      return null;

    end if;

    -- Building DCL

    select E'-- Explicitly defined ACL\n\n' ||
           string_agg
              (
                case when a.grantor = a.grantee and cardinality(v_acl_params.attacl) = 1
                          then 'GRANT '
                                || a.privilege_type
                                || ' (' || quote_ident(in_column) || ')'
                                || ' ON TABLE '
                                || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_table)
                                || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                || ' TO public GRANTED BY ' || quote_ident(rg.rolname)
                     else
                          'REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                          || a.privilege_type
                          || ' (' || quote_ident(in_column) || ')'
                          || ' ON TABLE '
                          || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_table)
                          || ' FROM '
                          || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                          || ' GRANTED BY ' || quote_ident(rg.rolname)
                          || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                end,
                E';\n\n'
                order by a.grantor, a.grantee, a.privilege_type
              )
            || ';'
    into v_acl_dcl
    from aclexplode(v_acl_params.attacl) a
    left join pg_catalog.pg_authid r on a.grantee = r."oid"
    left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    return v_acl_dcl;

  end;

$$;

comment on function get_column_acl_drop(in_column text, in_table text, in_schema text, grant_option_only bool, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for removing ACL item for a table-like object''s column by column''s name and table''s name and schema (default - search_path); if grant_option_only is true (false by default) then it adds GRANT OPTION FOR to REVOKE command; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';


-- Column name + OID

create or replace function get_column_acl_drop
  (
    in_column text,
    table_oid oid,
    grant_option_only bool default false,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Same DCL can be obtained with get_object_acl_drop function as well

-- Note that not only REVOKE commands are removing ACL objects, but some GRANT commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command
-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select attname,
--        get_column_acl_drop(attname, attrelid)
-- from pg_catalog.pg_attribute
-- where attrelid = 'pg_catalog.pg_subscription'::regclass
--       and a.attnum > 0;

  declare

    v_acl_dcl text;
    v_acl_params record;

  begin

    -- Checking existence of column ACL
    select c.relname,
           n.nspname,
           a.attacl
    into v_acl_params
    from pg_catalog.pg_attribute a
    join pg_catalog.pg_class c on a.attrelid = c."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where a.attisdropped is false
          and a.attname = in_column
          and c."oid" = table_oid;

    if v_acl_params.relname is null then

      raise warning 'Column % of object with OID % does not exist',
        quote_ident(in_column),
        table_oid;

      return null;

    elsif v_acl_params.relname is not null and v_acl_params.attacl is null then

      raise warning 'ACL for column % of object with OID % is empty',
        quote_ident(in_column),
        table_oid;

      return null;

    end if;

    -- Building DCL

    select E'-- Explicitly defined ACL\n\n' ||
           string_agg
              (
                case when a.grantor = a.grantee and cardinality(v_acl_params.attacl) = 1
                          then 'GRANT '
                                || a.privilege_type
                                || ' (' || quote_ident(in_column) || ')'
                                || ' ON TABLE '
                                || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname)
                                || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                || ' TO public GRANTED BY ' || quote_ident(rg.rolname)
                     else
                          'REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                          || a.privilege_type
                          || ' (' || quote_ident(in_column) || ')'
                          || ' ON TABLE '
                          || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname)
                          || ' FROM '
                          || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                          || ' GRANTED BY ' || quote_ident(rg.rolname)
                          || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                end,
                E';\n\n'
                order by a.grantor, a.grantee, a.privilege_type
              )
            || ';'
    into v_acl_dcl
    from aclexplode(v_acl_params.attacl) a
    left join pg_catalog.pg_authid r on a.grantee = r."oid"
    left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    return v_acl_dcl;

  end;

$$;

comment on function get_column_acl_drop(in_column text, table_oid oid, grant_option_only bool, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for removing ACL item for a table-like object''s column by column''s name and OID of parent object; if grant_option_only is true (false by default) then it adds GRANT OPTION FOR to REVOKE command; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';


-- Column position + OID

create or replace function get_column_acl_drop
  (
    column_num anycompatiblenonarray,
    table_oid oid,
    grant_option_only bool default false,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Same DCL can be obtained with get_object_acl_drop function as well

-- Note that not only REVOKE commands are removing ACL objects, but some GRANT commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- grant_option_only switch adds 'GRANT OPTION FOR' clause to REVOKE command
-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select attnum,
--        attname,
--        get_column_acl_drop(attnum, attrelid, true, false)
-- from pg_catalog.pg_attribute
-- where attrelid = 'pg_catalog.pg_subscription'::regclass
--       and a.attnum > 0;

  declare

    v_acl_dcl text;
    v_acl_params record;

  begin

    -- Checking existence of column ACL
    select c.relname,
           n.nspname,
           a.attname,
           a.attacl
    into v_acl_params
    from pg_catalog.pg_attribute a
    join pg_catalog.pg_class c on a.attrelid = c."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where a.attisdropped is false
          and a.attnum = column_num
          and c."oid" = table_oid;

    if v_acl_params.relname is null then

      raise warning 'Column at position % of object with OID % does not exist',
        column_num,
        table_oid;

      return null;

    elsif v_acl_params.relname is not null and v_acl_params.attacl is null then

      raise warning 'ACL for column at position % of object with OID % is empty',
        column_num,
        table_oid;

      return null;

    end if;

    -- Building DCL

    select E'-- Explicitly defined ACL\n\n' ||
           string_agg
              (
                case when a.grantor = a.grantee and cardinality(v_acl_params.attacl) = 1
                          then 'GRANT '
                                || a.privilege_type
                                || ' (' || quote_ident(v_acl_params.attname) || ')'
                                || ' ON TABLE '
                                || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname)
                                || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                                || ' TO public GRANTED BY ' || quote_ident(rg.rolname)
                     else
                          'REVOKE ' || case when grant_option_only is true then 'GRANT OPTION FOR ' else '' end
                          || a.privilege_type
                          || ' (' || quote_ident(v_acl_params.attname) || ')'
                          || ' ON TABLE '
                          || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname)
                          || ' FROM '
                          || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                          || ' GRANTED BY ' || quote_ident(rg.rolname)
                          || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                end,
                E';\n\n'
                order by a.grantor, a.grantee, a.privilege_type
              )
            || ';'
    into v_acl_dcl
    from aclexplode(v_acl_params.attacl) a
    left join pg_catalog.pg_authid r on a.grantee = r."oid"
    left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    return v_acl_dcl;

  end;

$$;

comment on function get_column_acl_drop(column_num anycompatiblenonarray, table_oid oid, grant_option_only bool, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for removing ACL item for a table-like object''s column by column number and OID of parent object; if grant_option_only is true (false by default) then it adds GRANT OPTION FOR to REVOKE command; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';