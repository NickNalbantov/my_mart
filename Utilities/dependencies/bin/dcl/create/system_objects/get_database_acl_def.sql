-- Examples:

-- select get_database_acl_def(datname),
--        get_database_acl_def("oid")
-- from pg_catalog.pg_database;


-- Name

create or replace function get_database_acl_def
  (
    in_db text,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Same DCL can be obtained with get_object_acl_def function as well

-- Note that not only GRANT commands are creating ACL objects, but some REVOKE commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select get_database_acl_def(datname),
-- from pg_catalog.pg_database
-- limit 10;

  declare

    v_acl_dcl text;
    v_acl_params record;

    v_def_acl text;
    v_obj_acl text;

  begin

    -- Checking existence of database
    select acldefault('d', datdba) as default_acl,
           datacl
    into v_acl_params
    from pg_catalog.pg_database
    where datname = in_db;

    if v_acl_params.default_acl is null then
      raise warning 'Database % does not exist', quote_ident(in_db);

      return null;
    end if;

    -- Default ACL
      -- All lines are commented because default ACLs is for information purposes only and normally shouldn't be changed without serious considerations
    if v_acl_params.default_acl is not null and v_acl_params.default_acl::text[] != '{}'::text[] then

      select E'-- Implicit default ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.default_acl) = 1
                            then '-- REVOKE ' || a.privilege_type || ' ON DATABASE '
                                 || quote_ident(in_db) || ' FROM public'
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                                 || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                       when a.grantee = 0::oid and cardinality(v_acl_params.default_acl) = 1
                            then '-- REVOKE ' || a.privilege_type || ' ON DATABASE '
                                 || quote_ident(in_db) || ' FROM ' || quote_ident(rg.rolname)
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                                 || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                       else
                            '-- GRANT ' || a.privilege_type || ' ON DATABASE '
                            || quote_ident(in_db) || ' TO '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || ';'
      into v_def_acl
      from aclexplode(v_acl_params.default_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_def_acl := '';

    end if;

    -- Non-default ACL
    if v_acl_params.datacl is not null and v_acl_params.datacl::text[] != '{}'::text[] then

      select E'-- Explicitly defined ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.datacl) = 1
                            then 'REVOKE ' || a.privilege_type
                                 || ' ON DATABASE ' || quote_ident(in_db)
                                 || ' FROM public GRANTED BY ' || quote_ident(rg.rolname)
                                 || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                       else
                            'GRANT ' || a.privilege_type
                            || ' ON DATABASE ' || quote_ident(in_db) || ' TO '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || E';\n\n\n'
      into v_obj_acl
      from aclexplode(v_acl_params.datacl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_obj_acl := '';

    end if;

    -- Building DCL

    v_acl_dcl := nullif(rtrim(v_obj_acl || v_def_acl, E'\n'), '');

    return v_acl_dcl;

  end;

$$;

comment on function get_database_acl_def(in_db text, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for creating ACL item for a database by its name; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';


-- OID

create or replace function get_database_acl_def
  (
    db_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Same DCL can be obtained with get_object_acl_def function as well

-- Note that not only GRANT commands are creating ACL objects, but some REVOKE commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select get_database_acl_def("oid"),
-- from pg_catalog.pg_database
-- limit 10;

  declare

    v_acl_dcl text;
    v_acl_params record;

    v_def_acl text;
    v_obj_acl text;

  begin

    -- Checking existence of database
    select datname,
           acldefault('d', datdba) as default_acl,
           datacl
    into v_acl_params
    from pg_catalog.pg_database
    where "oid" = db_oid;

    if v_acl_params.default_acl is null then
      raise warning 'Database with OID % does not exist', db_oid;

      return null;
    end if;

    -- Default ACL
      -- All lines are commented because default ACLs is for information purposes only and normally shouldn't be changed without serious considerations
    if v_acl_params.default_acl is not null and v_acl_params.default_acl::text[] != '{}'::text[] then

      select E'-- Implicit default ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.default_acl) = 1
                            then '-- REVOKE ' || a.privilege_type || ' ON DATABASE '
                                 || quote_ident(v_acl_params.datname) || ' FROM public'
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                                 || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                       when a.grantee = 0::oid and cardinality(v_acl_params.default_acl) = 1
                            then '-- REVOKE ' || a.privilege_type || ' ON DATABASE '
                                 || quote_ident(v_acl_params.datname) || ' FROM ' || quote_ident(rg.rolname)
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                                 || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                       else
                            '-- GRANT ' || a.privilege_type || ' ON DATABASE '
                            || quote_ident(v_acl_params.datname) || ' TO '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || ';'
      into v_def_acl
      from aclexplode(v_acl_params.default_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_def_acl := '';

    end if;

    -- Non-default ACL
    if v_acl_params.datacl is not null and v_acl_params.datacl::text[] != '{}'::text[] then

      select E'-- Explicitly defined ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.datacl) = 1
                            then 'REVOKE ' || a.privilege_type
                                 || ' ON DATABASE ' || quote_ident(v_acl_params.datname)
                                 || ' FROM public GRANTED BY ' || quote_ident(rg.rolname)
                                 || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                       else
                            'GRANT ' || a.privilege_type
                            || ' ON DATABASE ' || quote_ident(v_acl_params.datname) || ' TO '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || E';\n\n\n'
      into v_obj_acl
      from aclexplode(v_acl_params.datacl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_obj_acl := '';

    end if;

    -- Building DCL

    v_acl_dcl := nullif(rtrim(v_obj_acl || v_def_acl, E'\n'), '');

    return v_acl_dcl;

  end;

$$;

comment on function get_database_acl_def(db_oid oid, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for creating ACL item for a database by its OID; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';