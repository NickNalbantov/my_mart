-- Examples:

-- select get_default_acl_def(r.rolname, d.defaclobjtype, coalesce(n.nspname, nullif(d.defaclnamespace, 0)::text)),
--        get_default_acl_def(d."oid")
-- from pg_catalog.pg_default_acl d
-- join pg_catalog.pg_authid r on d.defaclrole = r."oid"
-- left join pg_catalog.pg_namespace n on d.defaclnamespace = n."oid";


-- Grantor role + object type + schema

create or replace function get_default_acl_def
  (
    grantor_role text,
    obj_type text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Note that not only ALTER DEFAULT PRIVILEGES...GRANT... commands are creating default ACL objects, but some ALTER DEFAULT PRIVILEGES...REVOKE... commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select get_default_acl_def(r.rolname, d.defaclobjtype, coalesce(n.nspname, nullif(d.defaclnamespace, 0)::text))
-- from pg_catalog.pg_default_acl d
-- join pg_catalog.pg_authid r on d.defaclrole = r."oid"
-- left join pg_catalog.pg_namespace n on d.defaclnamespace = n."oid"
-- limit 10;

  declare

    v_defacl_dcl text;
    v_defacl_params record;

    v_table_types text[] := array['table', 'foreign table', 'view', 'materialized view', 'tables', 'foreign tables', 'views', 'materialized views', 'r'];
    v_sequence_types text[] := array['SEQUENCE', 'SEQUENCES', 'S'];
    v_function_types text[] := array['function', 'procedure', 'aggregate', 'routine', 'functions', 'procedures', 'aggregates', 'routines', 'f'];
    v_type_types text[] := array['TYPE', 'DOMAIN', 'COMPOSITE TYPE', 'T'];
    v_schema_types text[] := array['schema', 'n'];

    obj_type_fixed text;
    obj_type_ddl text;

  begin

    -- Catching synonyms for object types
    if lower(obj_type) != all(v_table_types || v_function_types || v_schema_types)
       and upper(obj_type) != all(v_sequence_types || v_type_types)
    then

      raise warning E'Object type ''%'' is not recognized.\nAvailable options: %',
        obj_type,
        '[' || array_to_string(v_table_types || v_function_types || v_schema_types || v_sequence_types || v_type_types, ', ') || ']';

      return null;

    else

      select case when lower(obj_type) = any(v_table_types) then 'r'
                  when upper(obj_type) = any(v_sequence_types) then 'S'
                  when lower(obj_type) = any(v_function_types) then 'f'
                  when upper(obj_type) = any(v_type_types) then 'T'
                  when lower(obj_type) = any(v_schema_types) then 'n'
             end,
             case when lower(obj_type) = any(v_table_types) then 'TABLES'
                  when upper(obj_type) = any(v_sequence_types) then 'SEQUENCES'
                  when lower(obj_type) = any(v_function_types) then 'ROUTINES'
                  when upper(obj_type) = any(v_type_types) then 'TYPES'
                  when lower(obj_type) = any(v_schema_types) then 'SCHEMAS'
             end
      into obj_type_fixed,
           obj_type_ddl;

    end if;

    -- Checking existence of ACL entry
    select "oid",
           defaclnamespace,
           defaclacl
    into v_defacl_params
    from pg_catalog.pg_default_acl
    where grantor_role::regrole = defaclrole
          and obj_type_fixed = defaclobjtype
          and coalesce(in_schema::regnamespace, 0) = defaclnamespace;

    if v_defacl_params."oid" is null then
      raise warning 'Default ACL for role % granting privileges for object type ''%'' % does not exist',
        quote_ident(grantee),
        obj_type,
        case when in_schema is null then '' else 'in schema ' || quote_ident(in_schema) end;

      return null;
    end if;

    -- Building DCL

    select string_agg
              (
                'ALTER DEFAULT PRIVILEGES FOR ROLE ' || quote_ident(grantor_role) ||
                case when in_schema is not null then ' IN SCHEMA ' || quote_ident(in_schema) else '' end || E'\n    ' ||
                case when v_defacl_params.defaclnamespace = 0::oid and a.grantor = a.grantee and cardinality(v_defacl_params.defaclacl) = 1
                          then 'REVOKE ' || a.privilege_type || ' ON ' || obj_type_ddl || ' FROM public'
                               || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                     when v_defacl_params.defaclnamespace = 0::oid and a.grantee = 0::oid and cardinality(v_defacl_params.defaclacl) = 1
                          then 'REVOKE ' || a.privilege_type || ' ON ' || obj_type_ddl || ' FROM ' || quote_ident(grantor_role)
                               || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                     else
                          'GRANT ' || a.privilege_type || ' ON ' || obj_type_ddl || ' TO '
                          || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                          || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                end,
                E';\n\n'
                order by a.grantor, a.grantee, a.privilege_type
              )
           || ';'
    into v_defacl_dcl
    from aclexplode(v_defacl_params.defaclacl) a
    left join pg_catalog.pg_authid r on a.grantee = r."oid";

    return v_defacl_dcl;

  end;

$$;

comment on function get_default_acl_def(grantor_role text, obj_type text, in_schema text, cascade_ bool)
    is 'Generates ALTER DEFAULT PRIVILEGES command for creating default ACL object by grantor role name, object type and schema name (default null for global ACLs); if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';


-- OID

create or replace function get_default_acl_def
  (
    defacl_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Note that not only ALTER DEFAULT PRIVILEGES...GRANT... commands are creating default ACL objects, but some ALTER DEFAULT PRIVILEGES...REVOKE... commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select get_default_acl_def("oid")
-- from pg_catalog.pg_default_acl;

  declare

    v_defacl_dcl text;
    v_defacl_params record;
    v_grantor text;
    v_object_type text;

  begin

    -- Checking existence of ACL entry
    select d.defaclrole,
           case when d.defaclnamespace = 0::oid then '0' else quote_ident(n.nspname) end as nspname,
           d.defaclobjtype,
           d.defaclacl
    into v_defacl_params
    from pg_catalog.pg_default_acl d
    left join pg_catalog.pg_namespace n on d.defaclnamespace = n."oid"
    where d."oid" = defacl_oid;

    if v_defacl_params.defaclacl is null then
      raise warning 'Default ACL with OID % does not exist', defacl_oid;

      return null;
    end if;

    -- Grantor
    select quote_ident(rolname)
    into v_grantor
    from pg_catalog.pg_authid
    where v_defacl_params.defaclrole = "oid";

    -- Objects' type
    select case v_defacl_params.defaclobjtype
                when 'r' then 'TABLES'
                when 'S' then 'SEQUENCES'
                when 'f' then 'ROUTINES'
                when 'T' then 'TYPES'
                when 'n' then 'SCHEMAS'
           end
    into v_object_type;

    -- Building DCL

    select string_agg
              (
                'ALTER DEFAULT PRIVILEGES FOR ROLE ' || v_grantor ||
                case when v_defacl_params.nspname != '0' then ' IN SCHEMA ' || v_defacl_params.nspname else '' end || E'\n    ' ||
                case when v_defacl_params.nspname = '0' and a.grantor = a.grantee and cardinality(v_defacl_params.defaclacl) = 1
                          then 'REVOKE ' || a.privilege_type || ' ON ' || v_object_type || ' FROM public'
                               || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                     when v_defacl_params.nspname = '0' and a.grantee = 0::oid and cardinality(v_defacl_params.defaclacl) = 1
                          then 'REVOKE ' || a.privilege_type || ' ON ' || v_object_type || ' FROM ' || v_grantor
                               || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                     else
                          'GRANT ' || a.privilege_type || ' ON ' || v_object_type || ' TO '
                          || case when a.grantee = 0::oid then 'public' else quote_ident(r.rolname) end
                          || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                end,
                E';\n\n'
                order by a.grantor, a.grantee, a.privilege_type
              )
           || ';'
    into v_defacl_dcl
    from aclexplode(v_defacl_params.defaclacl) a
    left join pg_catalog.pg_authid r on a.grantee = r."oid";

    return v_defacl_dcl;

  end;

$$;

comment on function get_default_acl_def(defacl_oid oid, cascade_ bool)
    is 'Generates ALTER DEFAULT PRIVILEGES command for creating default ACL object by its OID; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';