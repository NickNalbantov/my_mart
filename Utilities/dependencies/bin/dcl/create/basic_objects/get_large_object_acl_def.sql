-- Example:

-- select "oid",
--        get_large_object_acl_def("oid")
-- from pg_catalog.pg_largeobject_metadata;


-- OID

create or replace function get_large_object_acl_def
  (
    lo_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Same DCL can be obtained with get_object_acl_def function as well

-- Note that not only GRANT commands are creating ACL objects, but some REVOKE commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select "oid",
--        get_large_object_acl_def("oid")
-- from pg_catalog.pg_largeobject_metadata
-- limit 10;

  declare

    v_acl_dcl text;
    v_acl_params record;

    v_def_acl text;
    v_obj_acl text;

  begin

    -- Checking existence of large object ACL
    select acldefault('L', current_user::regrole) as default_acl,
           lomacl
    into v_acl_params
    from pg_catalog.pg_largeobject_metadata
    where "oid" = lo_oid;

    if v_acl_params.default_acl is null then
      raise warning 'Large object with OID % does not exist', lo_oid;

      return null;
    end if;

    -- Default ACL
      -- All lines are commented because default ACLs is for information purposes only and normally shouldn't be changed without serious considerations
    if v_acl_params.default_acl is not null and v_acl_params.default_acl::text[] != '{}'::text[] then

      select E'-- Implicit default ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.default_acl) = 1
                            then '-- REVOKE ' || a.privilege_type || ' ON LARGE OBJECT ' || lo_oid || ' FROM public'
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                                 || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                       when a.grantee = 0::oid and cardinality(v_acl_params.default_acl) = 1
                            then '-- REVOKE ' || a.privilege_type || ' ON LARGE OBJECT ' || lo_oid || ' FROM ' || quote_ident(rg.rolname)
                                 || ' GRANTED BY ' || quote_ident(rg.rolname)
                                 || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                       else
                            '-- GRANT ' || a.privilege_type || ' ON LARGE OBJECT ' || lo_oid || ' TO '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || ';'
      into v_def_acl
      from aclexplode(v_acl_params.default_acl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_def_acl := '';

    end if;

    -- Non-default ACL
    if v_acl_params.lomacl is not null and v_acl_params.lomacl::text[] != '{}'::text[] then

      select E'-- Explicitly defined ACL\n\n' ||
             string_agg
                (
                  case when a.grantor = a.grantee and cardinality(v_acl_params.lomacl) = 1
                            then 'REVOKE '
                                 || a.privilege_type
                                 || ' ON LARGE OBJECT ' || lo_oid || ' FROM public GRANTED BY ' || quote_ident(rg.rolname)
                                 || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                       else
                            'GRANT '
                            || a.privilege_type
                            || ' ON LARGE OBJECT ' || lo_oid || ' TO '
                            || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                            || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                            || ' GRANTED BY ' || quote_ident(rg.rolname)
                  end,
                  E';\n\n'
                  order by a.grantor, a.grantee, a.privilege_type
                )
             || E';\n\n\n'
      into v_obj_acl
      from aclexplode(v_acl_params.lomacl) a
      left join pg_catalog.pg_authid r on a.grantee = r."oid"
      left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    else

      v_obj_acl := '';

    end if;

    -- Building DCL

    v_acl_dcl := nullif(rtrim(v_obj_acl || v_def_acl, E'\n'), '');

    return v_acl_dcl;

  end;

$$;

comment on function get_large_object_acl_def(lo_oid oid, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for creating ACL item for a large object by its OID; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';