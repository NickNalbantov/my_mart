-- Examples:

-- select get_column_acl_def(a.attname, c.relname, n.nspname),
--        get_column_acl_def(a.attname, c."oid"),
--        get_column_acl_def(a.attnum, c."oid", false)
-- from pg_catalog.pg_attribute a
-- join pg_catalog.pg_class c on c."oid" = a.attrelid
-- join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
-- where a.attacl is not null;


-- Column name + table name + schema

create or replace function get_column_acl_def
  (
    in_column text,
    in_table text,
    in_schema text default null,
    cascade_ bool default true
  )
returns text
language plpgsql
as
$$

-- Same DCL can be obtained with get_object_acl_def function as well

-- Note that not only GRANT commands are creating ACL objects, but some REVOKE commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select attname,
--        get_column_acl_def(a.attname, 'pg_subscription')
-- from pg_catalog.pg_attribute
-- where attrelid = 'pg_catalog.pg_subscription'::regclass
--       and a.attnum > 0;

  declare

    v_acl_dcl text;
    v_acl_params record;

  begin

    -- Checking existence of column ACL
    select a.attrelid,
           n.nspname,
           a.attacl
    into v_acl_params
    from pg_catalog.pg_attribute a
    join pg_catalog.pg_class c on a.attrelid = c."oid"
    left join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    cross join unnest(current_schemas(true)) with ordinality s (sch, rn)
    where a.attisdropped is false
          and a.attname = in_column
          and c.relname = in_table
          and coalesce(in_schema, s.sch) = n.nspname
    order by s.rn
    limit 1;

    if v_acl_params.attrelid is null then

      raise warning 'Column % of object %.% does not exist',
        quote_ident(in_column),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_table);

      return null;

    elsif v_acl_params.attrelid is not null and v_acl_params.attacl is null then

      raise warning 'ACL for column % of object %.% is empty',
        quote_ident(in_column),
        coalesce(quote_ident(in_schema), '[' || array_to_string(current_schemas(true), ', ') || ']'),
        quote_ident(in_table);

      return null;

    end if;

    -- Building DCL

    select E'-- Explicitly defined ACL\n\n' ||
           string_agg
              (
                case when a.grantor = a.grantee and cardinality(v_acl_params.attacl) = 1
                          then 'REVOKE '
                                || a.privilege_type
                                || ' (' || quote_ident(in_column) || ')'
                                || ' ON TABLE '
                                || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_table)
                                || ' FROM public GRANTED BY ' || quote_ident(rg.rolname)
                                || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                     else
                          'GRANT '
                          || a.privilege_type
                          || ' (' || quote_ident(in_column) || ')'
                          || ' ON TABLE '
                          || quote_ident(v_acl_params.nspname) || '.' || quote_ident(in_table)
                          || ' TO '
                          || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                          || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                          || ' GRANTED BY ' || quote_ident(rg.rolname)
                end,
                E';\n\n'
                order by a.grantor, a.grantee, a.privilege_type
              )
            || ';'
    into v_acl_dcl
    from aclexplode(v_acl_params.attacl) a
    left join pg_catalog.pg_authid r on a.grantee = r."oid"
    left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    return v_acl_dcl;

  end;

$$;

comment on function get_column_acl_def(in_column text, in_table text, in_schema text, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for creating ACL item for a table-like object''s column by column''s name and table''s name and schema (default - search_path); if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';


-- Column name + OID

create or replace function get_column_acl_def
  (
    in_column text,
    table_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Same DCL can be obtained with get_object_acl_def function as well

-- Note that not only GRANT commands are creating ACL objects, but some REVOKE commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select attname,
--        get_column_acl_def(attname, attrelid)
-- from pg_catalog.pg_attribute
-- where attrelid = 'pg_catalog.pg_subscription'::regclass
--       and a.attnum > 0;

  declare

    v_acl_dcl text;
    v_acl_params record;

  begin

    -- Checking existence of column ACL
    select c.relname,
           n.nspname,
           a.attacl
    into v_acl_params
    from pg_catalog.pg_attribute a
    join pg_catalog.pg_class c on a.attrelid = c."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where a.attisdropped is false
          and a.attname = in_column
          and c."oid" = table_oid;

    if v_acl_params.relname is null then

      raise warning 'Column % of object with OID % does not exist',
        quote_ident(in_column),
        table_oid;

      return null;

    elsif v_acl_params.relname is not null and v_acl_params.attacl is null then

      raise warning 'ACL for column % of object with OID % is empty',
        quote_ident(in_column),
        table_oid;

      return null;

    end if;

    -- Building DCL

    select E'-- Explicitly defined ACL\n\n' ||
           string_agg
              (
                case when a.grantor = a.grantee and cardinality(v_acl_params.attacl) = 1
                          then 'REVOKE '
                                || a.privilege_type
                                || ' (' || quote_ident(in_column) || ')'
                                || ' ON TABLE '
                                || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname)
                                || ' FROM public GRANTED BY ' || quote_ident(rg.rolname)
                                || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                     else
                          'GRANT '
                          || a.privilege_type
                          || ' (' || quote_ident(in_column) || ')'
                          || ' ON TABLE '
                          || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname)
                          || ' TO '
                          || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                          || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                          || ' GRANTED BY ' || quote_ident(rg.rolname)
                end,
                E';\n\n'
                order by a.grantor, a.grantee, a.privilege_type
              )
           || ';'
    into v_acl_dcl
    from aclexplode(v_acl_params.attacl) a
    left join pg_catalog.pg_authid r on a.grantee = r."oid"
    left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    return v_acl_dcl;

  end;

$$;

comment on function get_column_acl_def(in_column text, table_oid oid, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for creating ACL item for a table-like object''s column by column''s name and OID of parent object; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';


-- Column position + OID

create or replace function get_column_acl_def
  (
    column_num anycompatiblenonarray,
    table_oid oid,
    cascade_ bool default true
  )
returns text
language plpgsql
strict
as
$$

-- Same DCL can be obtained with get_object_acl_def function as well

-- Note that not only GRANT commands are creating ACL objects, but some REVOKE commands can do this as well. This function catches these cases.
-- Default ACL is constructed for the current user. To change that use SET ROLE ... clause before the function call.

-- There's also cascade_ (bool default true) parameter to switch CASCADE/RESTRICT options for REVOKE commands

-- Example:

-- select attnum,
--        attname,
--        get_column_acl_def(attnum, attrelid)
-- from pg_catalog.pg_attribute
-- where attrelid = 'pg_catalog.pg_subscription'::regclass
--       and a.attnum > 0;

  declare

    v_acl_dcl text;
    v_acl_params record;

  begin

    -- Checking existence of column ACL
    select c.relname,
           n.nspname,
           a.attname,
           a.attacl
    into v_acl_params
    from pg_catalog.pg_attribute a
    join pg_catalog.pg_class c on a.attrelid = c."oid"
    join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
    where a.attisdropped is false
          and a.attnum = column_num
          and c."oid" = table_oid;

    if v_acl_params.relname is null then

      raise warning 'Column at position % of object with OID % does not exist',
        column_num,
        table_oid;

      return null;

    elsif v_acl_params.relname is not null and v_acl_params.attacl is null then

      raise warning 'ACL for column at position % of object with OID % is empty',
        column_num,
        table_oid;

      return null;

    end if;

    -- Building DCL

    select E'-- Explicitly defined ACL\n\n' ||
           string_agg
              (
                case when a.grantor = a.grantee and cardinality(v_acl_params.attacl) = 1
                          then 'REVOKE '
                                || a.privilege_type
                                || ' (' || quote_ident(v_acl_params.attname) || ')'
                                || ' ON TABLE '
                                || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname)
                                || ' FROM public GRANTED BY ' || quote_ident(rg.rolname)
                                || case when cascade_ is true then ' CASCADE' else ' RESTRICT' end
                     else
                          'GRANT '
                          || a.privilege_type
                          || ' (' || quote_ident(v_acl_params.attname) || ')'
                          || ' ON TABLE '
                          || quote_ident(v_acl_params.nspname) || '.' || quote_ident(v_acl_params.relname)
                          || ' TO '
                          || case when a.grantee = 0 then 'public' else quote_ident(r.rolname) end
                          || case when a.is_grantable is true then ' WITH GRANT OPTION' else '' end
                          || ' GRANTED BY ' || quote_ident(rg.rolname)
                end,
                E';\n\n'
                order by a.grantor, a.grantee, a.privilege_type
              )
           || ';'
    into v_acl_dcl
    from aclexplode(v_acl_params.attacl) a
    left join pg_catalog.pg_authid r on a.grantee = r."oid"
    left join pg_catalog.pg_authid rg on a.grantor = rg."oid";

    return v_acl_dcl;

  end;

$$;

comment on function get_column_acl_def(column_num anycompatiblenonarray, table_oid oid, cascade_ bool)
    is 'Generates GRANT/REVOKE commands for creating ACL item for a table-like object''s column by column number and OID of parent object; if cascade_ is true (by default) then it adds CASCADE to REVOKE command, else - RESTRICT';