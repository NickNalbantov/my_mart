-- Launching script below:
-- \i .../psql_create_all.sql


-- Lib

-- /lib/is_sql
\ir ./lib/is_sql.sql



-- DDL

-- Arguments

-- /bin/create/arguments
\ir ./bin/ddl/arguments/get_aggregate_identity_arguments.sql
\ir ./bin/ddl/arguments/get_function_identity_arguments.sql



-- Create

-- /bin/ddl/create/basic_objects
\ir ./bin/ddl/create/basic_objects/get_aggregate_def.sql -- depends on get_function_identity_arguments
\ir ./bin/ddl/create/basic_objects/get_constraint_def.sql -- depends on is_sql
\ir ./bin/ddl/create/basic_objects/get_column_defaults_def.sql -- depends on get_constraint_def
\ir ./bin/ddl/create/basic_objects/get_event_trigger_def.sql
\ir ./bin/ddl/create/basic_objects/get_function_def.sql -- depends on get_function_identity_arguments and is_sql
\ir ./bin/ddl/create/basic_objects/get_index_def.sql -- depends on is_sql
\ir ./bin/ddl/create/basic_objects/get_policy_def.sql
\ir ./bin/ddl/create/basic_objects/get_rule_def.sql
\ir ./bin/ddl/create/basic_objects/get_schema_def.sql
\ir ./bin/ddl/create/basic_objects/get_table_def.sql -- depends on get_constraint_def and is_sql
\ir ./bin/ddl/create/basic_objects/get_trigger_def.sql
\ir ./bin/ddl/create/basic_objects/get_sequence_def.sql
\ir ./bin/ddl/create/basic_objects/get_view_def.sql


-- /bin/ddl/create/extensions
\ir ./bin/ddl/create/extensions/get_extension_def.sql
\ir ./bin/ddl/create/extensions/get_language_def.sql -- depends on get_function_identity_arguments
\ir ./bin/ddl/create/extensions/get_transform_def.sql -- depends on get_function_identity_arguments


-- /bin/ddl/create/foreign_objects
\ir ./bin/ddl/create/foreign_objects/get_fdw_def.sql -- depends on get_function_identity_arguments
\ir ./bin/ddl/create/foreign_objects/get_foreign_table_def.sql -- depends on get_constraint_def
\ir ./bin/ddl/create/foreign_objects/get_server_def.sql
\ir ./bin/ddl/create/foreign_objects/get_user_mapping_def.sql


-- /bin/ddl/create/fts
\ir ./bin/ddl/create/fts/get_ts_config_def.sql
\ir ./bin/ddl/create/fts/get_ts_dictionary_def.sql
\ir ./bin/ddl/create/fts/get_ts_parser_def.sql -- depends on get_function_identity_arguments
\ir ./bin/ddl/create/fts/get_ts_template_def.sql -- depends on get_function_identity_arguments


-- /bin/ddl/create/replication
\ir ./bin/ddl/create/replication/get_publication_def.sql
\ir ./bin/ddl/create/replication/get_subscription_def.sql


-- /bin/ddl/create/system_objects
\ir ./bin/ddl/create/system_objects/get_collation_def.sql
\ir ./bin/ddl/create/system_objects/get_conversion_def.sql -- depends on get_function_identity_arguments
\ir ./bin/ddl/create/system_objects/get_database_def.sql
\ir ./bin/ddl/create/system_objects/get_role_def.sql
\ir ./bin/ddl/create/system_objects/get_security_label_def.sql
\ir ./bin/ddl/create/system_objects/get_statistics_def.sql -- depends on is_sql
\ir ./bin/ddl/create/system_objects/get_tablespace_def.sql


-- /bin/ddl/create/types_and_operators
\ir ./bin/ddl/create/types_and_operators/get_access_method_def.sql -- depends on get_function_identity_arguments
\ir ./bin/ddl/create/types_and_operators/get_cast_def.sql -- depends on get_function_identity_arguments
\ir ./bin/ddl/create/types_and_operators/get_operator_class_def.sql -- depends on get_function_identity_arguments
\ir ./bin/ddl/create/types_and_operators/get_operator_def.sql -- depends on get_function_identity_arguments
\ir ./bin/ddl/create/types_and_operators/get_operator_family_def.sql -- depends on get_function_identity_arguments
\ir ./bin/ddl/create/types_and_operators/get_type_def.sql -- depends on get_constraint_def and get_function_identity_arguments



-- Drop

-- /bin/ddl/drop/basic_objects
\ir ./bin/ddl/drop/basic_objects/get_aggregate_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_column_defaults_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_constraint_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_event_trigger_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_function_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_index_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_large_object_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_policy_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_rule_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_schema_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_sequence_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_table_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_trigger_drop.sql
\ir ./bin/ddl/drop/basic_objects/get_view_drop.sql


-- /bin/ddl/drop/extensions
\ir ./bin/ddl/drop/extensions/get_extension_drop.sql
\ir ./bin/ddl/drop/extensions/get_language_drop.sql
\ir ./bin/ddl/drop/extensions/get_transform_drop.sql


-- /bin/ddl/drop/foreign_objects
\ir ./bin/ddl/drop/foreign_objects/get_fdw_drop.sql
\ir ./bin/ddl/drop/foreign_objects/get_foreign_table_drop.sql
\ir ./bin/ddl/drop/foreign_objects/get_server_drop.sql
\ir ./bin/ddl/drop/foreign_objects/get_user_mapping_drop.sql


-- /bin/ddl/drop/fts
\ir ./bin/ddl/drop/fts/get_ts_config_drop.sql
\ir ./bin/ddl/drop/fts/get_ts_dictionary_drop.sql
\ir ./bin/ddl/drop/fts/get_ts_parser_drop.sql
\ir ./bin/ddl/drop/fts/get_ts_template_drop.sql


-- /bin/ddl/drop/replication
\ir ./bin/ddl/drop/replication/get_publication_drop.sql
\ir ./bin/ddl/drop/replication/get_subscription_drop.sql


-- /bin/ddl/drop/system_objects
\ir ./bin/ddl/drop/system_objects/get_collation_drop.sql
\ir ./bin/ddl/drop/system_objects/get_conversion_drop.sql
\ir ./bin/ddl/drop/system_objects/get_database_drop.sql
\ir ./bin/ddl/drop/system_objects/get_role_drop.sql
\ir ./bin/ddl/drop/system_objects/get_security_label_drop.sql
\ir ./bin/ddl/drop/system_objects/get_statistics_drop.sql
\ir ./bin/ddl/drop/system_objects/get_tablespace_drop.sql


-- /bin/ddl/drop/types_and_operators
\ir ./bin/ddl/drop/types_and_operators/get_access_method_drop.sql
\ir ./bin/ddl/drop/types_and_operators/get_cast_drop.sql
\ir ./bin/ddl/drop/types_and_operators/get_operator_class_drop.sql
\ir ./bin/ddl/drop/types_and_operators/get_operator_drop.sql
\ir ./bin/ddl/drop/types_and_operators/get_operator_family_drop.sql
\ir ./bin/ddl/drop/types_and_operators/get_type_drop.sql



-- Comments

-- /bin/ddl/comments
\ir ./bin/ddl/comments/get_object_comment.sql


-- /bin/comments/basic_objects
\ir ./bin/ddl/comments/basic_objects/get_aggregate_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_column_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_constraint_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_event_trigger_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_function_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_index_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_large_object_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_policy_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_rule_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_schema_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_sequence_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_table_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_trigger_comment.sql
\ir ./bin/ddl/comments/basic_objects/get_view_comment.sql


-- /bin/comments/extension
\ir ./bin/ddl/comments/extensions/get_extension_comment.sql
\ir ./bin/ddl/comments/extensions/get_language_comment.sql
\ir ./bin/ddl/comments/extensions/get_transform_comment.sql


-- /bin/comments/foreign_objects
\ir ./bin/ddl/comments/foreign_objects/get_fdw_comment.sql
\ir ./bin/ddl/comments/foreign_objects/get_foreign_table_comment.sql
\ir ./bin/ddl/comments/foreign_objects/get_server_comment.sql


-- /bin/comments/fts
\ir ./bin/ddl/comments/fts/get_ts_config_comment.sql
\ir ./bin/ddl/comments/fts/get_ts_dictionary_comment.sql
\ir ./bin/ddl/comments/fts/get_ts_parser_comment.sql
\ir ./bin/ddl/comments/fts/get_ts_template_comment.sql


-- /bin/comments/replication
\ir ./bin/ddl/comments/replication/get_publication_comment.sql
\ir ./bin/ddl/comments/replication/get_subscription_comment.sql


-- /bin/comments/system_objects
\ir ./bin/ddl/comments/system_objects/get_collation_comment.sql
\ir ./bin/ddl/comments/system_objects/get_conversion_comment.sql
\ir ./bin/ddl/comments/system_objects/get_database_comment.sql
\ir ./bin/ddl/comments/system_objects/get_role_comment.sql
\ir ./bin/ddl/comments/system_objects/get_statistics_comment.sql
\ir ./bin/ddl/comments/system_objects/get_tablespace_comment.sql


-- /bin/comments/types_and_operators
\ir ./bin/ddl/comments/types_and_operators/get_access_method_comment.sql
\ir ./bin/ddl/comments/types_and_operators/get_cast_comment.sql
\ir ./bin/ddl/comments/types_and_operators/get_operator_class_comment.sql
\ir ./bin/ddl/comments/types_and_operators/get_operator_comment.sql
\ir ./bin/ddl/comments/types_and_operators/get_operator_family_comment.sql
\ir ./bin/ddl/comments/types_and_operators/get_type_comment.sql




-- DCL

-- Create

-- /bin/dcl/create/
\ir ./bin/dcl/create/get_object_acl_def.sql


-- /bin/dcl/create/basic_objects
\ir ./bin/dcl/create/basic_objects/get_aggregate_acl_def.sql
\ir ./bin/dcl/create/basic_objects/get_column_acl_def.sql
\ir ./bin/dcl/create/basic_objects/get_function_acl_def.sql
\ir ./bin/dcl/create/basic_objects/get_large_object_acl_def.sql
\ir ./bin/dcl/create/basic_objects/get_schema_acl_def.sql
\ir ./bin/dcl/create/basic_objects/get_sequence_acl_def.sql
\ir ./bin/dcl/create/basic_objects/get_table_acl_def.sql


-- /bin/dcl/create/extensions
\ir ./bin/dcl/create/extensions/get_language_acl_def.sql


-- /bin/dcl/create/foreign_objects
\ir ./bin/dcl/create/foreign_objects/get_fdw_acl_def.sql
\ir ./bin/dcl/create/foreign_objects/get_server_acl_def.sql


-- /bin/dcl/create/system_objects
\ir ./bin/dcl/create/system_objects/get_database_acl_def.sql
\ir ./bin/dcl/create/system_objects/get_default_acl_def.sql
\ir ./bin/dcl/create/system_objects/get_parameter_acl_def.sql
\ir ./bin/dcl/create/system_objects/get_tablespace_acl_def.sql


-- /bin/dcl/create/types_and_operators
\ir ./bin/dcl/create/types_and_operators/get_type_acl_def.sql



-- Drop

-- /bin/dcl/drop
\ir ./bin/dcl/drop/get_object_acl_drop.sql


-- /bin/dcl/drop/basic_objects
\ir ./bin/dcl/drop/basic_objects/get_aggregate_acl_drop.sql
\ir ./bin/dcl/drop/basic_objects/get_column_acl_drop.sql
\ir ./bin/dcl/drop/basic_objects/get_function_acl_drop.sql
\ir ./bin/dcl/drop/basic_objects/get_large_object_acl_drop.sql
\ir ./bin/dcl/drop/basic_objects/get_schema_acl_drop.sql
\ir ./bin/dcl/drop/basic_objects/get_sequence_acl_drop.sql
\ir ./bin/dcl/drop/basic_objects/get_table_acl_drop.sql


-- /bin/dcl/drop/extension
\ir ./bin/dcl/drop/extensions/get_language_acl_drop.sql


-- /bin/dcl/drop/foreign_objects
\ir ./bin/dcl/drop/foreign_objects/get_fdw_acl_drop.sql
\ir ./bin/dcl/drop/foreign_objects/get_server_acl_drop.sql


-- /bin/dcl/drop/system_objects
\ir ./bin/dcl/drop/system_objects/get_database_acl_drop.sql
\ir ./bin/dcl/drop/system_objects/get_default_acl_drop.sql
\ir ./bin/dcl/drop/system_objects/get_parameter_acl_drop.sql
\ir ./bin/dcl/drop/system_objects/get_tablespace_acl_drop.sql


-- /bin/dcl/drop/types_and_operators
\ir ./bin/dcl/drop/types_and_operators/get_type_acl_drop.sql



-- Owner

-- /bin/dcl/owner
\ir ./bin/dcl/owner/get_object_owner.sql


-- /bin/dcl/owner/basic_objects
\ir ./bin/dcl/owner/basic_objects/get_aggregate_owner.sql
\ir ./bin/dcl/owner/basic_objects/get_event_trigger_owner.sql
\ir ./bin/dcl/owner/basic_objects/get_function_owner.sql
\ir ./bin/dcl/owner/basic_objects/get_large_object_owner.sql
\ir ./bin/dcl/owner/basic_objects/get_schema_owner.sql
\ir ./bin/dcl/owner/basic_objects/get_sequence_owner.sql
\ir ./bin/dcl/owner/basic_objects/get_table_owner.sql
\ir ./bin/dcl/owner/basic_objects/get_view_owner.sql


-- /bin/dcl/owner/extension
\ir ./bin/dcl/owner/extensions/get_language_owner.sql


-- /bin/dcl/owner/foreign_objects
\ir ./bin/dcl/owner/foreign_objects/get_fdw_owner.sql
\ir ./bin/dcl/owner/foreign_objects/get_foreign_table_owner.sql
\ir ./bin/dcl/owner/foreign_objects/get_server_owner.sql


-- /bin/dcl/owner/fts
\ir ./bin/dcl/owner/fts/get_ts_config_owner.sql
\ir ./bin/dcl/owner/fts/get_ts_dictionary_owner.sql


-- /bin/dcl/owner/replication
\ir ./bin/dcl/owner/replication/get_publication_owner.sql
\ir ./bin/dcl/owner/replication/get_subscription_owner.sql


-- /bin/dcl/owner/system_objects
\ir ./bin/dcl/owner/system_objects/get_collation_owner.sql
\ir ./bin/dcl/owner/system_objects/get_conversion_owner.sql
\ir ./bin/dcl/owner/system_objects/get_database_owner.sql
\ir ./bin/dcl/owner/system_objects/get_statistics_owner.sql
\ir ./bin/dcl/owner/system_objects/get_tablespace_owner.sql


-- /bin/dcl/owner/types_and_operators
\ir ./bin/dcl/owner/types_and_operators/get_operator_class_owner.sql
\ir ./bin/dcl/owner/types_and_operators/get_operator_owner.sql
\ir ./bin/dcl/owner/types_and_operators/get_operator_family_owner.sql
\ir ./bin/dcl/owner/types_and_operators/get_type_owner.sql




-- Dependency

-- /bin/dependency.sql
\ir ./bin/dependency.sql