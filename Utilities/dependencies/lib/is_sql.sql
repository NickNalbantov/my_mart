CREATE OR REPLACE FUNCTION is_sql(sql text, is_notice boolean default false)
    returns boolean
    returns null on null input
    parallel unsafe --(ERROR:  cannot start subtransactions during a parallel operation)
    language plpgsql
    cost 5
AS
$$
DECLARE
    exception_sqlstate text;
    exception_message text;
    exception_context text;
    id text;
BEGIN
    BEGIN
        --https://github.com/rin-nas/postgresql-patterns-library/blob/master/functions/is_sql.sql

        --Speed improves. Shortest commands are "abort" or "do ''"
        IF octet_length(sql) < 5 OR sql !~ '[A-Za-z]{2}' THEN
            return false;
        END IF;

        -- add ";" in end of string, if ";" does not exist
        sql := regexp_replace(sql, $regexp$
            ;?
            ((?: #1
                 --[^\r\n]*                     #singe-line comment
              |  /\*                            #multi-line comment (can be nested)
                   [^*/]* #speed improves
                   (?: [^*/]+
                     | \*[^/] #not end comment
                     | /[^*]  #not begin comment
                     |   #recursive:
                         /\*                            #multi-line comment (can be nested)
                           [^*/]* #speed improves
                           (?: [^*/]+
                             | \*[^/] #not end comment
                             | /[^*]  #not begin comment
                             |   #recursive:
                                 /\*                            #multi-line comment (can be nested)
                                   [^*/]* #speed improves
                                   (?: [^*/]+
                                     | \*[^/] #not end comment
                                     | /[^*]  #not begin comment
                                     #| #recursive
                                   )*
                                 \*/
                           )*
                         \*/
                   )*
                 \*/
              |  \s+
            )*)
            $
        $regexp$, ';\1', 'x');

        id  := to_char(now(), 'YYYYMMDDHH24MISSMS');
        sql := E'DO $IS_SQL' || id || E'$ BEGIN\nRETURN;\n' || sql || E'\nEND; $IS_SQL' || id || E'$;';

        EXECUTE sql;
    EXCEPTION WHEN others THEN
        IF is_notice THEN
            GET STACKED DIAGNOSTICS
                exception_sqlstate := RETURNED_SQLSTATE,
                exception_message  := MESSAGE_TEXT,
                exception_context  := PG_EXCEPTION_CONTEXT;
            RAISE EXCEPTION 'exception_sqlstate = %', exception_sqlstate;
            RAISE EXCEPTION 'exception_context = %', exception_context;
            RAISE EXCEPTION 'exception_message = %', exception_message;
        END IF;
        RETURN FALSE;
    END;
    RETURN TRUE;
END
$$;


comment on FUNCTION is_sql(sql text, is_notice boolean)
    is 'Check SQL syntax exactly in your PostgreSQL version';