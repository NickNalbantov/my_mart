-- Lib

-- /lib/is_sql
drop function if exists is_sql(in pg_catalog.text, in pg_catalog.bool) cascade;




-- DDL


-- Arguments

-- /bin/ddl/arguments
drop function if exists get_aggregate_identity_arguments(in pg_catalog.text, in pg_catalog._text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text, in pg_catalog._text) cascade;
drop function if exists get_aggregate_identity_arguments(in pg_catalog.oid) cascade;

drop function if exists get_function_identity_arguments(in pg_catalog.text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_function_identity_arguments(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;


-- Create

-- /bin/ddl/create/basic_objects
drop function if exists get_aggregate_def(in pg_catalog.text, in pg_catalog._text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text, in pg_catalog._text) cascade;
drop function if exists get_aggregate_def(in pg_catalog.oid) cascade;

drop function if exists get_column_defaults_def(in_column text, in_table text, in_schema text) cascade;
drop function if exists get_column_defaults_def(in_column text, table_oid oid) cascade;
drop function if exists get_column_defaults_def(column_num anycompatiblenonarray, table_oid oid) cascade;

drop function if exists get_constraint_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_constraint_def(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_event_trigger_def(in pg_catalog.text) cascade;
drop function if exists get_event_trigger_def(in pg_catalog.oid) cascade;

drop function if exists get_function_def(in pg_catalog.text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text) cascade;
drop function if exists get_function_def(in pg_catalog.oid) cascade;

drop function if exists get_index_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_index_def(in pg_catalog.oid) cascade;

drop function if exists get_policy_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_policy_def(in pg_catalog.oid) cascade;

drop function if exists get_rule_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_rule_def(in pg_catalog.oid) cascade;

drop function if exists get_schema_def(in pg_catalog.text) cascade;
drop function if exists get_schema_def(in pg_catalog.oid) cascade;

drop function if exists get_sequence_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_sequence_def(in pg_catalog.oid) cascade;

drop function if exists get_table_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_table_def(in pg_catalog.oid) cascade;

drop function if exists get_trigger_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_trigger_def(in pg_catalog.oid) cascade;

drop function if exists get_view_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_view_def(in pg_catalog.oid) cascade;


-- /bin/ddl/create/extensions
drop function if exists get_extension_def(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_extension_def(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_language_def(in pg_catalog.text) cascade;
drop function if exists get_language_def(in pg_catalog.oid) cascade;

drop function if exists get_transform_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_transform_def(in pg_catalog.oid, in pg_catalog.oid) cascade;
drop function if exists get_transform_def(in pg_catalog.oid) cascade;


-- /bin/ddl/create/foreign_objects
drop function if exists get_fdw_def(in pg_catalog.text) cascade;
drop function if exists get_fdw_def(in pg_catalog.oid) cascade;

drop function if exists get_foreign_table_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_foreign_table_def(in pg_catalog.oid) cascade;

drop function if exists get_server_def(in pg_catalog.text) cascade;
drop function if exists get_server_def(in pg_catalog.oid) cascade;

drop function if exists get_user_mapping_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_user_mapping_def(in pg_catalog.oid, in pg_catalog.oid) cascade;
drop function if exists get_user_mapping_def(in pg_catalog.oid) cascade;


-- /bin/ddl/create/fts
drop function if exists get_ts_config_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_ts_config_def(in pg_catalog.oid) cascade;

drop function if exists get_ts_dictionary_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_ts_dictionary_def(in pg_catalog.oid) cascade;

drop function if exists get_ts_parser_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_ts_parser_def(in pg_catalog.oid) cascade;

drop function if exists get_ts_template_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_ts_template_def(in pg_catalog.oid) cascade;


-- /bin/ddl/create/replication
drop function if exists get_publication_def(in pg_catalog.text) cascade;
drop function if exists get_publication_def(in pg_catalog.oid) cascade;

drop function if exists get_subscription_def(in pg_catalog.text) cascade;
drop function if exists get_subscription_def(in pg_catalog.oid) cascade;


-- /bin/ddl/create/system_objects
drop function if exists get_collation_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_collation_def(in pg_catalog.oid) cascade;

drop function if exists get_conversion_def(in pg_catalog.oid) cascade;
drop function if exists get_conversion_def(in pg_catalog.text, in pg_catalog.text) cascade;

drop function if exists get_database_def(in pg_catalog.text) cascade;
drop function if exists get_database_def(in pg_catalog.oid) cascade;

drop function if exists get_role_def(in pg_catalog.text) cascade;
drop function if exists get_role_def(in pg_catalog.oid) cascade;

drop function if exists get_security_label_def(in pg_catalog.oid, in pg_catalog.oid, in pg_catalog.anycompatiblenonarray) cascade;

drop function if exists get_statistics_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_statistics_def(in pg_catalog.oid) cascade;

drop function if exists get_tablespace_def(in pg_catalog.text) cascade;
drop function if exists get_tablespace_def(in pg_catalog.oid) cascade;


-- /bin/ddl/create/types_and_operators
drop function if exists get_access_method_def(in pg_catalog.text) cascade;
drop function if exists get_access_method_def(in pg_catalog.oid) cascade;

drop function if exists get_cast_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_cast_def(in pg_catalog.oid, in pg_catalog.oid) cascade;
drop function if exists get_cast_def(in pg_catalog.oid) cascade;

drop function if exists get_operator_class_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_operator_class_def(in pg_catalog.oid) cascade;

drop function if exists get_operator_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_operator_def(in pg_catalog.oid) cascade;

drop function if exists get_operator_family_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_operator_family_def(in pg_catalog.oid) cascade;

drop function if exists get_type_def(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_type_def(in pg_catalog.oid) cascade;



-- Drop

-- /bin/ddl/drop/basic_objects
drop function if exists get_aggregate_drop(in pg_catalog.text, in pg_catalog._text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text, in pg_catalog._text, in pg_catalog.bool) cascade;
drop function if exists get_aggregate_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_column_defaults_drop(in_column text, in_table text, in_schema text) cascade;
drop function if exists get_column_defaults_drop(in_column text, table_oid oid) cascade;
drop function if exists get_column_defaults_drop(column_num anycompatiblenonarray, table_oid oid) cascade;

drop function if exists get_constraint_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_constraint_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_event_trigger_drop(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_event_trigger_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_function_drop(in pg_catalog.text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text, in pg_catalog.bool) cascade;
drop function if exists get_function_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_index_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_index_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_large_object_drop(in pg_catalog.oid) cascade;

drop function if exists get_policy_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_policy_drop(in pg_catalog.oid) cascade;

drop function if exists get_rule_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_rule_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_schema_drop(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_schema_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_sequence_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_sequence_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_table_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_table_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_trigger_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_trigger_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_view_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_view_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/ddl/drop/extensions
drop function if exists get_extension_drop(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_extension_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_language_drop(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_language_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_transform_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_transform_drop(in pg_catalog.oid, in pg_catalog.oid, in pg_catalog.bool) cascade;
drop function if exists get_transform_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/ddl/drop/foreign_objects
drop function if exists get_fdw_drop(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_fdw_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_foreign_table_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_foreign_table_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_server_drop(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_server_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_user_mapping_drop(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_user_mapping_drop(in pg_catalog.oid, in pg_catalog.oid) cascade;
drop function if exists get_user_mapping_drop(in pg_catalog.oid) cascade;


-- /bin/ddl/drop/fts
drop function if exists get_ts_config_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_ts_config_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_ts_dictionary_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_ts_dictionary_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_ts_parser_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_ts_parser_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_ts_template_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_ts_template_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/ddl/drop/replication
drop function if exists get_publication_drop(in pg_catalog.text) cascade;
drop function if exists get_publication_drop(in pg_catalog.oid) cascade;

drop function if exists get_subscription_drop(in pg_catalog.text) cascade;
drop function if exists get_subscription_drop(in pg_catalog.oid) cascade;


-- /bin/ddl/drop/system_objects
drop function if exists get_collation_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_collation_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_conversion_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_conversion_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_database_drop(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_database_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_role_drop(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_role_drop(in pg_catalog.oid, in pg_catalog.oid) cascade;

drop function if exists get_security_label_drop(in pg_catalog.oid, in pg_catalog.oid, in pg_catalog.anycompatiblenonarray) cascade;

drop function if exists get_statistics_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_statistics_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_tablespace_drop(in pg_catalog.text) cascade;
drop function if exists get_tablespace_drop(in pg_catalog.oid) cascade;


-- /bin/ddl/drop/types_and_operators
drop function if exists get_access_method_drop(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_access_method_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_cast_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_cast_drop(in pg_catalog.oid, in pg_catalog.oid, in pg_catalog.bool) cascade;
drop function if exists get_cast_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_operator_class_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_operator_class_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_operator_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_operator_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_operator_family_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_operator_family_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_type_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_type_drop(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- Comments

-- /bin/ddl/comments
drop function if exists get_object_comment(in pg_catalog.oid, in pg_catalog.oid, in pg_catalog.anycompatiblenonarray) cascade;


-- /bin/ddl/comments/basic_objects
drop function if exists get_aggregate_comment(in pg_catalog.text, in pg_catalog._text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text, in pg_catalog._text) cascade;
drop function if exists get_aggregate_comment(in pg_catalog.oid) cascade;

drop function if exists get_column_comment(in_column text, in_table text, in_schema text) cascade;
drop function if exists get_column_comment(in_column text, table_oid oid) cascade;
drop function if exists get_column_comment(column_num anycompatiblenonarray, table_oid oid) cascade;

drop function if exists get_constraint_comment(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_constraint_comment(in pg_catalog.oid) cascade;

drop function if exists get_event_trigger_comment(in pg_catalog.text) cascade;
drop function if exists get_event_trigger_comment(in pg_catalog.oid) cascade;

drop function if exists get_function_comment(in pg_catalog.text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text) cascade;
drop function if exists get_function_comment(in pg_catalog.oid) cascade;

drop function if exists get_index_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_index_comment(in pg_catalog.oid) cascade;

drop function if exists get_large_object_comment(in pg_catalog.oid) cascade;

drop function if exists get_policy_comment(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_policy_comment(in pg_catalog.oid) cascade;

drop function if exists get_rule_comment(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_rule_comment(in pg_catalog.oid) cascade;

drop function if exists get_schema_comment(in pg_catalog.text) cascade;
drop function if exists get_schema_comment(in pg_catalog.oid) cascade;

drop function if exists get_sequence_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_sequence_comment(in pg_catalog.oid) cascade;

drop function if exists get_table_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_table_comment(in pg_catalog.oid) cascade;

drop function if exists get_trigger_comment(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_trigger_comment(in pg_catalog.oid) cascade;

drop function if exists get_view_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_view_comment(in pg_catalog.oid) cascade;


-- /bin/ddl/comments/extension
drop function if exists get_extension_comment(in pg_catalog.text) cascade;
drop function if exists get_extension_comment(in pg_catalog.oid) cascade;

drop function if exists get_language_comment(in pg_catalog.text) cascade;
drop function if exists get_language_comment(in pg_catalog.oid) cascade;

drop function if exists get_transform_comment(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_transform_comment(in pg_catalog.oid, in pg_catalog.oid) cascade;
drop function if exists get_transform_comment(in pg_catalog.oid) cascade;


-- /bin/ddl/comments/foreign_objects
drop function if exists get_fdw_comment(in pg_catalog.text) cascade;
drop function if exists get_fdw_comment(in pg_catalog.oid) cascade;

drop function if exists get_foreign_table_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_foreign_table_comment(in pg_catalog.oid) cascade;

drop function if exists get_server_comment(in pg_catalog.text) cascade;
drop function if exists get_server_comment(in pg_catalog.oid) cascade;


-- /bin/ddl/comments/fts
drop function if exists get_ts_config_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_ts_config_comment(in pg_catalog.oid) cascade;

drop function if exists get_ts_dictionary_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_ts_dictionary_comment(in pg_catalog.oid) cascade;

drop function if exists get_ts_parser_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_ts_parser_comment(in pg_catalog.oid) cascade;

drop function if exists get_ts_template_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_ts_template_comment(in pg_catalog.oid) cascade;


-- /bin/ddl/comments/replication
drop function if exists get_publication_comment(in pg_catalog.text) cascade;
drop function if exists get_publication_comment(in pg_catalog.oid) cascade;

drop function if exists get_subscription_comment(in pg_catalog.text) cascade;
drop function if exists get_subscription_comment(in pg_catalog.oid) cascade;


-- /bin/ddl/comments/system_objects
drop function if exists get_collation_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_collation_comment(in pg_catalog.oid) cascade;

drop function if exists get_conversion_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_conversion_comment(in pg_catalog.oid) cascade;

drop function if exists get_database_comment(in pg_catalog.text) cascade;
drop function if exists get_database_comment(in pg_catalog.oid) cascade;

drop function if exists get_role_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_role_comment(in pg_catalog.oid, in pg_catalog.oid) cascade;

drop function if exists get_statistics_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_statistics_comment(in pg_catalog.oid) cascade;

drop function if exists get_tablespace_comment(in pg_catalog.text) cascade;
drop function if exists get_tablespace_comment(in pg_catalog.oid) cascade;


-- /bin/ddl/comments/types_and_operators
drop function if exists get_access_method_comment(in pg_catalog.text) cascade;
drop function if exists get_access_method_comment(in pg_catalog.oid) cascade;

drop function if exists get_cast_comment(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_cast_comment(in pg_catalog.oid, in pg_catalog.oid) cascade;
drop function if exists get_cast_comment(in pg_catalog.oid) cascade;

drop function if exists get_operator_class_comment(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_operator_class_comment(in pg_catalog.oid) cascade;

drop function if exists get_operator_comment(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_operator_comment(in pg_catalog.oid) cascade;

drop function if exists get_operator_family_comment(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_operator_family_comment(in pg_catalog.oid) cascade;

drop function if exists get_type_comment(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists get_type_comment(in pg_catalog.oid) cascade;




-- DCL

-- Create

-- /bin/dcl/create/
drop function if exists get_object_acl_def(in pg_catalog.oid, in pg_catalog.oid, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;


-- /bin/dcl/create/basic_objects
drop function if exists get_aggregate_acl_def(in pg_catalog.text, in pg_catalog._text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text, in pg_catalog._text, in pg_catalog.bool) cascade;
drop function if exists get_aggregate_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_column_acl_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_column_acl_def(in pg_catalog.text, in pg_catalog.oid, in pg_catalog.bool) cascade;
drop function if exists get_column_acl_def(in pg_catalog.anycompatiblenonarray, in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_function_acl_def(in pg_catalog.text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text, in pg_catalog.bool) cascade;
drop function if exists get_function_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_large_object_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_schema_acl_def(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_schema_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_sequence_acl_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_sequence_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_table_acl_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_table_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/dcl/create/extensions
drop function if exists get_language_acl_def(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_language_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/dcl/create/foreign_objects
drop function if exists get_fdw_acl_def(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_fdw_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_server_acl_def(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_server_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/dcl/create/system_objects
drop function if exists get_database_acl_def(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_database_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_default_acl_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_default_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_parameter_acl_def(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_parameter_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_tablespace_acl_def(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_tablespace_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/dcl/create/types_and_operators
drop function if exists get_type_acl_def(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_type_acl_def(in pg_catalog.oid, in pg_catalog.bool) cascade;



-- Drop

-- /bin/dcl/drop/
drop function if exists get_object_acl_drop(in pg_catalog.oid, in pg_catalog.oid, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool, in pg_catalog.bool) cascade;


-- /bin/dcl/drop/basic_objects
drop function if exists get_aggregate_acl_drop(in pg_catalog.text, in pg_catalog._text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text, in pg_catalog._text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_aggregate_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;

drop function if exists get_column_acl_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_column_acl_drop(in pg_catalog.text, in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_column_acl_drop(in pg_catalog.anycompatiblenonarray, in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;

drop function if exists get_function_acl_drop(in pg_catalog.text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_function_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;

drop function if exists get_large_object_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;

drop function if exists get_schema_acl_drop(in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_schema_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;

drop function if exists get_sequence_acl_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_sequence_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;

drop function if exists get_table_acl_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_table_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;


-- /bin/dcl/drop/extension
drop function if exists get_language_acl_drop(in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_language_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;


-- /bin/dcl/drop/foreign_objects
drop function if exists get_fdw_acl_drop(in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_fdw_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;

drop function if exists get_server_acl_drop(in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_server_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;


-- /bin/dcl/drop/system_objects
drop function if exists get_database_acl_drop(in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_database_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;

drop function if exists get_default_acl_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_default_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;

drop function if exists get_parameter_acl_drop(in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_parameter_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;

drop function if exists get_tablespace_acl_drop(in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_tablespace_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;


-- /bin/dcl/drop/types_and_operators
drop function if exists get_type_acl_drop(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists get_type_acl_drop(in pg_catalog.oid, in pg_catalog.bool, in pg_catalog.bool) cascade;



-- Owner

-- /bin/dcl/owner
drop function if exists get_object_owner(in pg_catalog.oid, in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/dcl/owner/basic_objects
drop function if exists get_aggregate_owner(in pg_catalog.text, in pg_catalog._text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text, in pg_catalog._text, in pg_catalog.bool) cascade;
drop function if exists get_aggregate_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_event_trigger_owner(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_event_trigger_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_function_owner(in pg_catalog.text, in pg_catalog._text, in pg_catalog.text, in pg_catalog._text, in pg_catalog.bool) cascade;
drop function if exists get_function_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_large_object_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_schema_owner(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_schema_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_sequence_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_sequence_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_table_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_table_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_view_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_view_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/dcl/owner/extension
drop function if exists get_language_owner(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_language_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/dcl/owner/foreign_objects
drop function if exists get_fdw_owner(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_fdw_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_foreign_table_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_foreign_table_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_server_owner(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_server_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/dcl/owner/fts
drop function if exists get_ts_config_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_ts_config_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_ts_dictionary_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_ts_dictionary_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/dcl/owner/replication
drop function if exists get_publication_owner(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_publication_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_subscription_owner(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_subscription_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;


-- /bin/dcl/owner/system_objects
drop function if exists get_collation_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_collation_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_conversion_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_conversion_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_database_owner(in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_database_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_statistics_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_statistics_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_tablespace_owner(in pg_catalog.text) cascade;
drop function if exists get_tablespace_owner(in pg_catalog.oid) cascade;


-- /bin/dcl/owner/types_and_operators
drop function if exists get_operator_class_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_operator_class_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_operator_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_operator_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_operator_family_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_operator_family_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;

drop function if exists get_type_owner(in pg_catalog.text, in pg_catalog.text, in pg_catalog.bool) cascade;
drop function if exists get_type_owner(in pg_catalog.oid, in pg_catalog.bool) cascade;



-- Dependency

-- /bin/dependency.sql
drop view if exists pg_dependency cascade;

drop function if exists dependency_childs(in pg_catalog.oid, in pg_catalog.bool) cascade;
drop function if exists dependency_parents(in pg_catalog.oid, in pg_catalog.bool) cascade;