--========================================
-- DDL
--========================================


--########################################
-- Arguments
--########################################


-- get_aggregate_identity_arguments

select get_aggregate_identity_arguments('max', array['bigint']),
       get_aggregate_identity_arguments('count'),
       get_aggregate_identity_arguments('percentile_cont', array['float'], array['interval']),
       get_aggregate_identity_arguments('mode', array['anyelement']);

select get_aggregate_identity_arguments(proname, direct_typnames, order_typnames, nspname, direct_typschemas, order_typschemas),
       get_aggregate_identity_arguments("oid")
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typnames,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typschemas,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typschemas
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    join pg_catalog.pg_aggregate a on a.aggfnoid = p."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind = 'a'
    group by p."oid",
             p.proname,
             n.nspname,
             a.aggnumdirectargs
) a
limit 10;

-- get_function_identity_arguments

select get_function_identity_arguments('version'),
       get_function_identity_arguments('substring', array['text', 'int']),
       get_function_identity_arguments('get_function_identity_arguments', array['text', $$text[]$$, 'text', $$text[]$$, 'bool', 'bool']),
       get_function_identity_arguments('get_function_identity_arguments', array['oid', 'bool', 'bool']);

select get_function_identity_arguments(proname, typnames, nspname, typschemas),
       get_function_identity_arguments("oid", false, false)
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else array_agg(t.typname order by pat.rn) end as typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else array_agg(nt.nspname order by pat.rn) end as typschemas
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind != 'a'
    group by p."oid",
             p.proname,
             n.nspname
) a
limit 10;



--########################################
-- Create
--########################################


--////////////////////////////////////////
-- Basic objects
--////////////////////////////////////////


-- get_aggregate_def

select get_aggregate_def('max', array['bigint']),
       get_aggregate_def('count'),
       get_aggregate_def('percentile_cont', array['float'], array['interval']),
       get_aggregate_def('mode', array['anyelement']);

select get_aggregate_def(proname, direct_typnames, order_typnames, nspname, direct_typschemas, order_typschemas),
       get_aggregate_def("oid")
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typnames,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typschemas,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typschemas
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    join pg_catalog.pg_aggregate a on a.aggfnoid = p."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind = 'a'
    group by p."oid",
             p.proname,
             n.nspname,
             a.aggnumdirectargs
    limit 10
) a;

-- get_column_defaults_def

select get_column_defaults_def(a.attname, c.relname, n.nspname),
       get_column_defaults_def(a.attname, c."oid"),
       get_column_defaults_def(a.attnum, c."oid")
from pg_catalog.pg_attribute a
join pg_catalog.pg_class c on c."oid" = a.attrelid
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
left join pg_catalog.pg_attrdef ad on ad.adrelid = a.attrelid and ad.adnum = a.attnum
where a.atthasdef is true or a.attidentity != '' or a.attgenerated != ''
limit 10;

-- get_constraint_def

 select 'CONSTRAINT ' || c.conname || ' ' || pg_catalog.pg_get_constraintdef(c."oid"), -- comparison with built-in system function
        get_constraint_def(c.conname, coalesce(cl.relname, t.typname), n.nspname),
        get_constraint_def(c.conname, coalesce(cl.relname, t.typname), n.nspname, true),
        get_constraint_def(c."oid", true)
 from pg_catalog.pg_constraint c
 left join pg_catalog.pg_class cl on c.conrelid = cl."oid"
 left join pg_catalog.pg_type t on c.contypid = t."oid"
 join pg_catalog.pg_namespace n on n."oid" = c.connamespace
 where contype != 't'
limit 10;

select get_constraint_def('pg_attrdef_adrelid_adnum_index', 'pg_attrdef', null, true);

-- get_event_trigger_def

select get_event_trigger_def(evtname),
       get_event_trigger_def("oid")
from pg_catalog.pg_event_trigger
limit 10;

-- get_function_def

select pg_catalog.pg_get_functiondef('version'::regproc), -- comparison with built-in system function
       get_function_def('version')
union all
select pg_catalog.pg_get_functiondef('substring(text, int4)'::regprocedure), -- comparison with built-in system function
       get_function_def('substring', array['text', 'int'])
union all
select pg_catalog.pg_get_functiondef('get_function_def(text, text[], text, text[])'::regprocedure), -- comparison with built-in system function
       get_function_def('get_function_def', array['text', $$text[]$$, 'text', $$text[]$$])
union all
select pg_catalog.pg_get_functiondef('get_function_def(oid)'::regprocedure), -- comparison with built-in system function
       get_function_def('get_function_def', array['oid']);


select pg_catalog.pg_get_functiondef("oid"), -- comparison with built-in system function
       get_function_def(proname, typnames, nspname, typschemas),
       get_function_def("oid")
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else array_agg(t.typname order by pat.rn) end as typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else array_agg(nt.nspname order by pat.rn) end as typschemas
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind != 'a'
    group by p."oid",
             p.proname,
             n.nspname
    limit 10
) a;

-- get_index_def

select pg_catalog.pg_get_indexdef(c."oid"), -- comparison with built-in system function
       get_index_def(c.relname, n.nspname),
       get_index_def(c."oid")
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('i', 'I')
limit 10;

select get_index_def('pg_attrdef_adrelid_adnum_index');

-- get_policy_def

select get_policy_def(p.polname, c.relname, n.nspname),
       get_policy_def(p."oid")
from pg_catalog.pg_policy p
join pg_catalog.pg_class c on p.polrelid = c."oid"
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
limit 10;

-- get_rule_def

select get_rule_def(r.rulename, c.relname, n.nspname),
       get_rule_def(r."oid")
from pg_catalog.pg_rewrite r
join pg_catalog.pg_class c on r.ev_class = c."oid"
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
limit 10;

select get_rule_def('_RETURN', 'pg_user');

-- get_schema_def

select get_schema_def(nspname),
       get_schema_def("oid")
from pg_catalog.pg_namespace
limit 10;

-- get_sequence_def

select get_sequence_def(c.relname, n.nspname),
       get_sequence_def(c."oid")
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
where c.relkind = 'S'
limit 10;

-- get_table_def

select get_table_def(c.relname, n.nspname),
       get_table_def(c."oid")
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('r', 'p', 't')
limit 10;

select get_table_def('pg_trigger');

-- get_trigger_def

select pg_catalog.pg_get_triggerdef(t."oid", true), -- comparing with built-in function
       get_trigger_def(t.tgname, c.relname, n.nspname),
       get_trigger_def(t."oid")
from pg_catalog.pg_trigger t
join pg_catalog.pg_class c on t.tgrelid = c."oid"
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
limit 10;

-- get_view_def

select get_view_def(c.relname, n.nspname),
       get_view_def(c."oid")
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('v', 'm')
limit 10;

select get_view_def('pg_settings');


--////////////////////////////////////////
-- Extensions
--////////////////////////////////////////

-- get_extension_def

select get_extension_def(extname),
       get_extension_def("oid")
from pg_catalog.pg_extension
limit 10;

-- get_language_def

select get_language_def(lanname),
       get_language_def("oid")
from pg_catalog.pg_language
limit 10;

-- get_transform_def

select get_transform_def(l.lanname, t.typname, n.nspname),
       get_transform_def(tr."oid"),
       get_transform_def(l."oid", t."oid")
from pg_catalog.pg_transform tr
join pg_catalog.pg_type t on tr.trftype = t."oid"
join pg_catalog.pg_language l on tr.trflang = l."oid"
join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
limit 10;

select get_transform_def('plpython3u', 'hstore');


--////////////////////////////////////////
-- Foreign objects
--////////////////////////////////////////


-- get_fdw_def

select get_fdw_def(fdwname),
       get_fdw_def("oid")
from pg_catalog.pg_foreign_data_wrapper
limit 10;

-- get_foreign_table_def

select get_foreign_table_def(c.relname, n.nspname),
       get_foreign_table_def(c."oid")
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind = 'f'
limit 10;

-- get_server_def

select get_server_def(srvname),
       get_server_def("oid")
from pg_catalog.pg_foreign_server
limit 10;

-- get_user_mapping_def

select get_user_mapping_def(s.srvname, a.rolname),
       get_user_mapping_def(u."oid"),
       get_user_mapping_def(s."oid", a."oid")
from pg_catalog.pg_user_mapping u
join pg_catalog.pg_foreign_server s on u.umserver = s."oid"
join (
       select "oid",
              rolname
       from pg_catalog.pg_authid
       union all
       select 0::oid as "oid",
              'public' as rolname
     ) a on u.umuser = a."oid"
limit 10;


--////////////////////////////////////////
-- Full-text search
--////////////////////////////////////////


-- get_ts_config_def

select get_ts_config_def(c.cfgname, n.nspname),
       get_ts_config_def(c."oid")
from pg_catalog.pg_ts_config c
join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace
limit 10;

select get_ts_config_def('german');

-- get_ts_dictionary_def

select get_ts_dictionary_def(d.dictname, n.nspname),
       get_ts_dictionary_def(d."oid")
from pg_catalog.pg_ts_dict d
join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
limit 10;

select get_ts_dictionary_def('german_stem');

-- get_ts_parser_def

select get_ts_parser_def(t.prsname, n.nspname),
       get_ts_parser_def(t."oid")
from pg_catalog.pg_ts_parser t
join pg_catalog.pg_namespace n on n."oid" = t.prsnamespace
limit 10;

select get_ts_parser_def('default');

-- get_ts_template_def

select get_ts_template_def(t.tmplname, n.nspname),
       get_ts_template_def(t."oid")
from pg_catalog.pg_ts_template t
join pg_catalog.pg_namespace n on n."oid" = t.tmplnamespace
limit 10;

select get_ts_template_def('synonym');


--////////////////////////////////////////
-- Replication
--////////////////////////////////////////


-- get_publication_def

select get_publication_def(pubname),
       get_publication_def("oid")
from pg_catalog.pg_publication
limit 10;

-- get_subscription_def

select get_subscription_def(subname),
       get_subscription_def("oid")
from pg_catalog.pg_subscription
limit 10;


--////////////////////////////////////////
-- System objects
--////////////////////////////////////////


-- get_collation_def

select get_collation_def(c.collname, n.nspname),
       get_collation_def(c."oid")
from pg_catalog.pg_collation c
join pg_catalog.pg_namespace n on c.collnamespace = n."oid"
limit 10;

select get_collation_def('ru-RU-x-icu');

-- get_conversion_def

select get_conversion_def(c.conname, n.nspname),
       get_conversion_def(c."oid")
from pg_catalog.pg_conversion c
join pg_catalog.pg_namespace n on c.connamespace = n."oid"
limit 10;

select get_conversion_def('utf8_to_windows_1251');

-- get_database_def

select get_database_def(datname),
       get_database_def("oid")
from pg_catalog.pg_database
limit 10;

-- get_role_def

select get_role_def(rolname),
       get_role_def("oid")
from pg_catalog.pg_authid
limit 10;

-- get_security_label_def

select *
from
(
       select
              c."oid",
              c.relnamespace::regnamespace || '.' || c.relname || '.' || a.attname as obj_name,
              get_security_label_def(c.tableoid, c."oid", a.attnum) as qry
       from pg_catalog.pg_class c
       join pg_catalog.pg_attribute a on a.attrelid = c."oid"
       where
              a.attnum > 0
              and a.attisdropped is false

       union all

       select
              "oid",
              rolname,
              get_security_label_def(tableoid, "oid")
       from pg_catalog.pg_authid

       union all

       select
              "oid",
              relnamespace::regnamespace || '.' || relname,
              get_security_label_def(tableoid, "oid")
       from pg_catalog.pg_class
       where relkind in ('r', 'p', 'm', 'v', 'f', 'S')

       union all

       select
              "oid",
              datname,
              get_security_label_def(tableoid, "oid")
       from pg_catalog.pg_database

       union all

       select
              "oid",
              evtname,
              get_security_label_def(tableoid, "oid")
       from pg_catalog.pg_event_trigger

       union all

       select
              "oid",
              lanname,
              get_security_label_def(tableoid, "oid")
       from pg_catalog.pg_language

       union all

       select
              "oid",
              "oid"::text,
              get_security_label_def(tableoid, "oid")
       from pg_catalog.pg_largeobject_metadata

       union all

       select
              "oid",
              nspname,
              get_security_label_def(tableoid, "oid")
       from pg_catalog.pg_namespace

       union all

       select
              "oid",
              pronamespace::regnamespace || '.' || proname,
              get_security_label_def(tableoid, "oid")
       from pg_catalog.pg_proc

       union all

       select
              "oid",
              pubname,
              get_security_label_def(tableoid, "oid")
       from pg_catalog.pg_publication

       union all

       select
              "oid",
              subname,
              get_security_label_def(tableoid, "oid")
       from pg_catalog.pg_subscription

       union all

       select
              "oid",
              spcname,
              get_security_label_def(tableoid, "oid")
       from pg_catalog.pg_tablespace

       union all

       select
              "oid",
              typnamespace::regnamespace || '.' || typname,
              get_security_label_def(tableoid, "oid")
       from pg_catalog.pg_type
) a
where qry is not null
limit 1000;

-- get_statistics_def

select pg_catalog.pg_get_statisticsobjdef(s."oid"), -- comparison with built-in system function
       get_statistics_def(s.stxname, n.nspname),
       get_statistics_def(s."oid")
from pg_catalog.pg_statistic_ext s
join pg_catalog.pg_namespace n on s.stxnamespace = n."oid"
limit 10;

-- get_tablespace_def

select get_tablespace_def(spcname),
       get_tablespace_def("oid")
from pg_catalog.pg_tablespace
limit 10;


--////////////////////////////////////////
-- Types and operators
--////////////////////////////////////////


-- get_access_method_def

select get_access_method_def(amname),
       get_access_method_def("oid")
from pg_catalog.pg_am
limit 10;

-- get_cast_def

select get_cast_def(ts.typname, tt.typname, ns.nspname, nt.nspname),
       get_cast_def(c."oid"),
       get_cast_def(c.castsource, c.casttarget)
from pg_catalog.pg_cast c
join pg_catalog.pg_type ts on c.castsource = ts."oid"
join pg_catalog.pg_type tt on c.casttarget = tt."oid"
join pg_catalog.pg_namespace ns on ns."oid" = ts.typnamespace
join pg_catalog.pg_namespace nt on nt."oid" = tt.typnamespace
limit 10;

select get_cast_def('int', 'bigint')
union all
select get_cast_def('text[]', 'hstore');

-- get_operator_class_def

select get_operator_class_def(o.opcname, a.amname, n.nspname),
       get_operator_class_def(o."oid")
from pg_catalog.pg_opclass o
join pg_catalog.pg_am a on o.opcmethod = a."oid"
join pg_catalog.pg_namespace n on n."oid" = o.opcnamespace
limit 10;

select get_operator_class_def('text_ops')
union all
select get_operator_class_def('text_ops', 'hash');

-- get_operator_def

select get_operator_def(o.oprname, tl.typname, tr.typname, nop.nspname, ntl.nspname, ntr.nspname),
    get_operator_def(o."oid")
from pg_catalog.pg_operator o
join pg_catalog.pg_namespace nop on o.oprnamespace = nop."oid"
left join pg_catalog.pg_type tl on o.oprleft = tl."oid"
left join pg_catalog.pg_namespace ntl on tl.typnamespace = ntl."oid"
left join pg_catalog.pg_type tr on o.oprright = tr."oid"
left join pg_catalog.pg_namespace ntr on tr.typnamespace = ntr."oid"
limit 10;

select get_operator_def('>=', 'int', 'bigint');

-- get_operator_family_def

select get_operator_family_def(o.opfname, a.amname, n.nspname),
       get_operator_family_def(o."oid")
from pg_catalog.pg_opfamily o
join pg_catalog.pg_am a on o.opfmethod = a."oid"
join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace
limit 10;

select get_operator_family_def('text_ops')
union all
select get_operator_family_def('text_ops', 'hash');

-- get_type_def

select get_type_def(t.typname, n.nspname),
       get_type_def(t."oid")
from pg_catalog.pg_type t
join pg_catalog.pg_namespace n on t.typnamespace = n."oid"
limit 10;

select get_type_def('integer');



--########################################
-- Drop
--########################################


--////////////////////////////////////////
-- Basic objects
--////////////////////////////////////////


-- get_aggregate_drop

select get_aggregate_drop('max', array['bigint']),
       get_aggregate_drop('count'),
       get_aggregate_drop('percentile_cont', array['float'], array['interval']),
       get_aggregate_drop('mode', array['anyelement']);

select get_aggregate_drop(proname, direct_typnames, order_typnames, nspname, direct_typschemas, order_typschemas),
       get_aggregate_drop("oid", false)
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typnames,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typschemas,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typschemas
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    join pg_catalog.pg_aggregate a on a.aggfnoid = p."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind = 'a'
    group by p."oid",
             p.proname,
             n.nspname,
             a.aggnumdirectargs
    limit 10
) a;

-- get_column_defaults_drop

select get_column_defaults_drop(a.attname, c.relname, n.nspname),
       get_column_defaults_drop(a.attname, c."oid"),
       get_column_defaults_drop(a.attnum, c."oid")
from pg_catalog.pg_attribute a
join pg_catalog.pg_class c on c."oid" = a.attrelid
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
left join pg_catalog.pg_attrdef ad on ad.adrelid = a.attrelid and ad.adnum = a.attnum
where a.atthasdef is true or a.attidentity != '' or a.attgenerated != ''
limit 10;

-- get_constraint_drop

 select get_constraint_drop(c.conname, coalesce(cl.relname, t.typname), n.nspname),
        get_constraint_drop(c."oid", false)
 from pg_catalog.pg_constraint c
 left join pg_catalog.pg_class cl on c.conrelid = cl."oid"
 left join pg_catalog.pg_type t on c.contypid = t."oid"
 join pg_catalog.pg_namespace n on n."oid" = c.connamespace
 where contype != 't'
limit 10;

select get_constraint_drop('pg_attrdef_adrelid_adnum_index', 'pg_attrdef');

-- get_event_trigger_drop

select get_event_trigger_drop(evtname),
       get_event_trigger_drop("oid", false)
from pg_catalog.pg_event_trigger
limit 10;

-- get_function_drop

select get_function_drop('version'),
       get_function_drop('substring', array['text', 'int']),
       get_function_drop('get_function_drop', array['text', $$text[]$$, 'text', $$text[]$$, 'bool']),
       get_function_drop('get_function_drop', array['oid', 'bool']);

select get_function_drop(proname, typnames, nspname, typschemas, false),
       get_function_drop("oid")
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else array_agg(t.typname order by pat.rn) end as typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else array_agg(nt.nspname order by pat.rn) end as typschemas
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind != 'a'
    group by p."oid",
             p.proname,
             n.nspname
    limit 10
) a;

-- get_index_drop

select get_index_drop(c.relname, n.nspname),
       get_index_drop(c."oid", false)
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('i', 'I')
limit 10;

-- get_large_object_drop

select "oid",
       get_large_object_drop("oid")
from pg_catalog.pg_largeobject_metadata
limit 10;

-- get_policy_drop

select get_policy_drop(p.polname, c.relname, n.nspname),
       get_policy_drop(p."oid")
from pg_catalog.pg_policy p
join pg_catalog.pg_class c on p.polrelid = c."oid"
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace

-- get_rule_drop

select get_rule_drop(r.rulename, c.relname, n.nspname),
       get_rule_drop(r."oid", false)
from pg_catalog.pg_rewrite r
join pg_catalog.pg_class c on r.ev_class = c."oid"
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace;

select get_rule_drop('_RETURN', 'pg_user');

-- get_schema_drop

select get_schema_drop(nspname),
       get_schema_drop("oid", false)
from pg_catalog.pg_namespace
limit 10;

-- get_sequence_drop

select get_sequence_drop(c.relname, n.nspname),
       get_sequence_drop(c."oid", false)
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
where c.relkind = 'S'
limit 10;

-- get_table_drop

select get_table_drop(c.relname, n.nspname),
       get_table_drop(c."oid", false)
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('r', 'p', 't')
limit 10;

select get_table_drop('pg_trigger');

-- get_trigger_drop

select get_trigger_drop(t.tgname, c.relname, n.nspname),
       get_trigger_drop(t."oid", false)
from pg_catalog.pg_trigger t
join pg_catalog.pg_class c on t.tgrelid = c."oid"
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
limit 10;

-- get_view_drop

select get_view_drop(c.relname, n.nspname),
       get_view_drop(c."oid", false)
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('v', 'm')
limit 10;

select get_view_drop('pg_settings');


--////////////////////////////////////////
-- Extension
--////////////////////////////////////////


-- get_extension_drop

select get_extension_drop(extname),
       get_extension_drop("oid", false)
from pg_catalog.pg_extension
limit 10;

-- get_language_drop

select get_language_drop(lanname),
       get_language_drop("oid", false)
from pg_catalog.pg_language
limit 10;

-- get_transform_drop

select get_transform_drop(l.lanname, t.typname, n.nspname),
       get_transform_drop(tr."oid"),
       get_transform_drop(l."oid", t."oid", false)
from pg_catalog.pg_transform tr
join pg_catalog.pg_type t on tr.trftype = t."oid"
join pg_catalog.pg_language l on tr.trflang = l."oid"
join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
limit 10;

select get_transform_drop('plpython3u', 'hstore');


--////////////////////////////////////////
-- Foreign objects
--////////////////////////////////////////


-- get_fdw_drop

select get_fdw_drop(fdwname),
       get_fdw_drop("oid", false)
from pg_catalog.pg_foreign_data_wrapper
limit 10;

-- get_foreign_table_drop

select get_foreign_table_drop(c.relname, n.nspname),
       get_foreign_table_drop(c."oid", false)
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind = 'f'
limit 10;

-- get_server_drop

select get_server_drop(srvname),
       get_server_drop("oid", false)
from pg_catalog.pg_foreign_server
limit 10;

-- get_user_mapping_drop

select get_user_mapping_drop(s.srvname, a.rolname),
       get_user_mapping_drop(u."oid"),
       get_user_mapping_drop(s."oid", a."oid")
from pg_catalog.pg_user_mapping u
join pg_catalog.pg_foreign_server s on u.umserver = s."oid"
join (
       select "oid",
              rolname
       from pg_catalog.pg_authid
       union all
       select 0::oid as "oid",
              'public' as rolname
     ) a on u.umuser = a."oid"
limit 10;


--////////////////////////////////////////
-- Full-text search
--////////////////////////////////////////


-- get_ts_config_drop

select get_ts_config_drop(c.cfgname, n.nspname),
       get_ts_config_drop(c."oid", false)
from pg_catalog.pg_ts_config c
join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace
limit 10;

select get_ts_config_drop('german');

-- get_ts_dictionary_drop

select get_ts_dictionary_drop(d.dictname, n.nspname),
       get_ts_dictionary_drop(d."oid", false)
from pg_catalog.pg_ts_dict d
join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
limit 10;

select get_ts_dictionary_drop('german_stem');

-- get_ts_parser_drop

select get_ts_parser_drop(t.prsname, n.nspname),
       get_ts_parser_drop(t."oid", false)
from pg_catalog.pg_ts_parser t
join pg_catalog.pg_namespace n on n."oid" = t.prsnamespace
limit 10;

select get_ts_parser_drop('default');

-- get_ts_template_drop

select get_ts_template_drop(t.tmplname, n.nspname),
       get_ts_template_drop(t."oid", false)
from pg_catalog.pg_ts_template t
join pg_catalog.pg_namespace n on n."oid" = t.tmplnamespace
limit 10;

select get_ts_template_drop('synonym');


--////////////////////////////////////////
-- Replication
--////////////////////////////////////////


-- get_publication_drop

select get_publication_drop(pubname),
       get_publication_drop("oid")
from pg_catalog.pg_publication
limit 10;

-- get_subscription_drop

select get_subscription_drop(subname),
       get_subscription_drop("oid")
from pg_catalog.pg_subscription
limit 10;


--////////////////////////////////////////
-- System objects
--////////////////////////////////////////


-- get_collation_drop

select get_collation_drop(c.collname, n.nspname),
       get_collation_drop(c."oid", false)
from pg_catalog.pg_collation c
join pg_catalog.pg_namespace n on c.collnamespace = n."oid"
limit 10;

select get_collation_drop('ru-RU-x-icu');

-- get_conversion_drop

select get_conversion_drop(c.conname, n.nspname),
       get_conversion_drop(c."oid", false)
from pg_catalog.pg_conversion c
join pg_catalog.pg_namespace n on c.connamespace = n."oid"
limit 10;

select get_conversion_drop('utf8_to_windows_1251');

-- get_database_drop

select get_database_drop(datname),
       get_database_drop("oid", false)
from pg_catalog.pg_database
limit 10;

-- get_role_drop

select get_role_drop('pg_write_all_data');

select get_role_drop(a.rolname, b.rolname),
       get_role_drop(a."oid", b."oid")
from pg_catalog.pg_authid a
join pg_catalog.pg_authid b on b."oid" = 10
                               and a."oid" != b."oid";

-- get_security_label_drop

select *
from
(
       select
              c."oid",
              c.relnamespace::regnamespace || '.' || c.relname || '.' || a.attname as obj_name,
              get_security_label_drop(c.tableoid, c."oid", a.attnum) as qry
       from pg_catalog.pg_class c
       join pg_catalog.pg_attribute a on a.attrelid = c."oid"
       where
              a.attnum > 0
              and a.attisdropped is false

       union all

       select
              "oid",
              rolname,
              get_security_label_drop(tableoid, "oid")
       from pg_catalog.pg_authid

       union all

       select
              "oid",
              relnamespace::regnamespace || '.' || relname,
              get_security_label_drop(tableoid, "oid")
       from pg_catalog.pg_class
       where relkind in ('r', 'p', 'm', 'v', 'f', 'S')

       union all

       select
              "oid",
              datname,
              get_security_label_drop(tableoid, "oid")
       from pg_catalog.pg_database

       union all

       select
              "oid",
              evtname,
              get_security_label_drop(tableoid, "oid")
       from pg_catalog.pg_event_trigger

       union all

       select
              "oid",
              lanname,
              get_security_label_drop(tableoid, "oid")
       from pg_catalog.pg_language

       union all

       select
              "oid",
              "oid"::text,
              get_security_label_drop(tableoid, "oid")
       from pg_catalog.pg_largeobject_metadata

       union all

       select
              "oid",
              nspname,
              get_security_label_drop(tableoid, "oid")
       from pg_catalog.pg_namespace

       union all

       select
              "oid",
              pronamespace::regnamespace || '.' || proname,
              get_security_label_drop(tableoid, "oid")
       from pg_catalog.pg_proc

       union all

       select
              "oid",
              pubname,
              get_security_label_drop(tableoid, "oid")
       from pg_catalog.pg_publication

       union all

       select
              "oid",
              subname,
              get_security_label_drop(tableoid, "oid")
       from pg_catalog.pg_subscription

       union all

       select
              "oid",
              spcname,
              get_security_label_drop(tableoid, "oid")
       from pg_catalog.pg_tablespace

       union all

       select
              "oid",
              typnamespace::regnamespace || '.' || typname,
              get_security_label_drop(tableoid, "oid")
       from pg_catalog.pg_type
) a
where qry is not null
limit 1000;

-- get_tablespace_drop

select get_tablespace_drop(spcname),
       get_tablespace_drop("oid")
from pg_catalog.pg_tablespace
limit 10;

-- get_statistics_drop

select get_statistics_drop(s.stxname, n.nspname),
       get_statistics_drop(s."oid", false)
from pg_catalog.pg_statistic_ext s
join pg_catalog.pg_namespace n on s.stxnamespace = n."oid"
limit 10;


--////////////////////////////////////////
-- Types and operators
--////////////////////////////////////////


-- get_access_method_drop

select get_access_method_drop(amname),
       get_access_method_drop("oid", false)
from pg_catalog.pg_am
limit 10;

-- get_cast_drop

select get_cast_drop(ts.typname, tt.typname, ns.nspname, nt.nspname),
       get_cast_drop(c."oid"),
       get_cast_drop(c.castsource, c.casttarget, false)
from pg_catalog.pg_cast c
join pg_catalog.pg_type ts on c.castsource = ts."oid"
join pg_catalog.pg_type tt on c.casttarget = tt."oid"
join pg_catalog.pg_namespace ns on ns."oid" = ts.typnamespace
join pg_catalog.pg_namespace nt on nt."oid" = tt.typnamespace
limit 10;

select get_cast_drop('int', 'bigint')
union all
select get_cast_drop('text[]', 'hstore', cascade_ := false);

-- get_operator_class_drop

select get_operator_class_drop(o.opcname, a.amname, n.nspname),
       get_operator_class_drop(o."oid", false)
from pg_catalog.pg_opclass o
join pg_catalog.pg_am a on o.opcmethod = a."oid"
join pg_catalog.pg_namespace n on n."oid" = o.opcnamespace
limit 10;

select get_operator_class_drop('text_ops')
union all
select get_operator_class_drop('text_ops', 'hash', null, false);

-- get_operator_drop

select get_operator_drop(o.oprname, tl.typname, tr.typname, nop.nspname, ntl.nspname, ntr.nspname),
    get_operator_drop(o."oid", false)
from pg_catalog.pg_operator o
join pg_catalog.pg_namespace nop on o.oprnamespace = nop."oid"
left join pg_catalog.pg_type tl on o.oprleft = tl."oid"
left join pg_catalog.pg_namespace ntl on tl.typnamespace = ntl."oid"
left join pg_catalog.pg_type tr on o.oprright = tr."oid"
left join pg_catalog.pg_namespace ntr on tr.typnamespace = ntr."oid"
limit 10;

select get_operator_drop('>=', 'int', 'bigint', cascade_ := false);

-- get_operator_family_drop

select get_operator_family_drop(o.opfname, a.amname, n.nspname),
       get_operator_family_drop(o."oid", false)
from pg_catalog.pg_opfamily o
join pg_catalog.pg_am a on o.opfmethod = a."oid"
join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace
limit 10;

select get_operator_family_drop('text_ops')
union all
select get_operator_family_drop('text_ops', 'hash', null, false);

-- get_type_drop

select get_type_drop(t.typname, n.nspname),
       get_type_drop(t."oid", false)
from pg_catalog.pg_type t
join pg_catalog.pg_namespace n on t.typnamespace = n."oid"
limit 10;

select get_type_drop('integer');



--########################################
-- Comments
--########################################


-- get_object_comment

select *
from
(
    select
       "oid",
       amname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_am

    union all

    select
       c."oid",
       c.relnamespace::regnamespace || '.' || c.relname || '.' || a.attname as obj_name,
       get_object_comment(c.tableoid, c."oid", a.attnum) as qry
    from pg_catalog.pg_class c
    join pg_catalog.pg_attribute a on a.attrelid = c."oid"
    where
       a.attnum > 0
       and a.attisdropped is false

    union all

    select
       "oid",
       rolname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_authid

    union all

    select
       "oid",
       "oid"::text as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_cast

    union all

    select
       "oid",
       relnamespace::regnamespace || '.' || relname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_class
    where relkind != 't'

    union all

    select
       "oid",
       collnamespace::regnamespace || '.' || collname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_collation

    union all

    select
       "oid",
       connamespace::regnamespace || '.' || conname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_constraint

    union all

    select
       "oid",
       connamespace::regnamespace || '.' || conname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_conversion

    union all

    select
       "oid",
       datname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_database

    union all

    select
       "oid",
       evtname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_event_trigger

    union all

    select
       "oid",
       extname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_extension

    union all

    select
       "oid",
       fdwname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_foreign_data_wrapper

    union all

    select
       "oid",
       srvname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_foreign_server

    union all

    select
       "oid",
       lanname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_language

    union all

    select
       "oid",
       "oid"::text as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_largeobject_metadata

    union all

    select
       "oid",
       nspname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_namespace

    union all

    select
       "oid",
       opcnamespace::regnamespace || '.' || opcname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_opclass

    union all

    select
       "oid",
       oprnamespace::regnamespace || '.' || oprname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_operator

    union all

    select
       "oid",
       opfnamespace::regnamespace || '.' || opfname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_opfamily

    union all

    select
       "oid",
       polname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_policy

    union all

    select
       "oid",
       pronamespace::regnamespace || '.' || proname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_proc

    union all

    select
       "oid",
       pubname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_publication

    union all

    select
       "oid",
       rulename as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_rewrite

    union all

    select
       "oid",
       stxnamespace::regnamespace || '.' || stxname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_statistic_ext

    union all

    select
       "oid",
       subname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_subscription

    union all

    select
       "oid",
       spcname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_tablespace

    union all

    select
       "oid",
       "oid"::text as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_transform

    union all

    select
       "oid",
       tgname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_trigger

    union all

    select
       "oid",
       cfgnamespace::regnamespace || '.' || cfgname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_ts_config

    union all

    select
       "oid",
       dictnamespace::regnamespace || '.' || dictname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_ts_dict

    union all

    select
       "oid",
       tmplnamespace::regnamespace || '.' || tmplname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_ts_template

    union all

    select
       "oid",
       prsnamespace::regnamespace || '.' || prsname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_ts_parser

    union all

    select
       "oid",
       typnamespace::regnamespace || '.' || typname as obj_name,
       get_object_comment(tableoid, "oid") as qry
    from pg_catalog.pg_type
) a
where qry is not null
limit 1000;


--////////////////////////////////////////
-- Basic objects
--////////////////////////////////////////


-- get_aggregate_comment

select get_aggregate_comment('max', array['bigint']),
       get_aggregate_comment('count'),
       get_aggregate_comment('percentile_cont', array['float'], array['interval']),
       get_aggregate_comment('mode', array['anyelement']);

select get_aggregate_comment(proname, direct_typnames, order_typnames, nspname, direct_typschemas, order_typschemas),
       get_aggregate_comment("oid")
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typnames,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typschemas,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typschemas
    from pg_catalog.pg_description d
    join pg_catalog.pg_proc p on p.tableoid = d.classoid and p."oid" = d.objoid
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    join pg_catalog.pg_aggregate a on a.aggfnoid = p."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind = 'a'
    group by p."oid",
             p.proname,
             n.nspname,
             a.aggnumdirectargs
    limit 10
) a;

-- get_column_comment

select get_column_comment(a.attname, c.relname, n.nspname),
       get_column_comment(a.attname, c."oid"),
       get_column_comment(a.attnum, c."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_class c on c.tableoid = d.classoid and c."oid" = d.objoid
join pg_catalog.pg_attribute a on c."oid" = a.attrelid and a.attnum = d.objsubid
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where a.attisdropped is false
limit 10;

-- get_constraint_comment

select get_constraint_comment(c.conname, coalesce(cl.relname, t.typname), n.nspname),
       get_constraint_comment(c."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_constraint c on c.tableoid = d.classoid and c."oid" = d.objoid
left join pg_catalog.pg_class cl on c.conrelid = cl."oid" and cl.relkind in ('r', 'p', 'f')
left join pg_catalog.pg_type t on c.contypid = t."oid" and t.typtype = 'd'
join pg_catalog.pg_namespace n on n."oid" = c.connamespace
where contype != 't'
limit 10;

select get_constraint_comment('pg_attrdef_adrelid_adnum_index', 'pg_attrdef');

-- get_event_trigger_comment

select get_event_trigger_comment(s.evtname),
       get_event_trigger_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_event_trigger s on s.tableoid = d.classoid and s."oid" = d.objoid
limit 10;

-- get_function_comment

select get_function_comment('version'),
       get_function_comment('substring', array['text', 'int']),
       get_function_comment('get_function_comment', array['text', $$text[]$$, 'text', $$text[]$$]),
       get_function_comment('get_function_comment', array['oid']);

select get_function_comment(proname, typnames, nspname, typschemas),
       get_function_comment("oid")
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else array_agg(t.typname order by pat.rn) end as typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else array_agg(nt.nspname order by pat.rn) end as typschemas
    from pg_catalog.pg_description d
    join pg_catalog.pg_proc p on p.tableoid = d.classoid and p."oid" = d.objoid
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind != 'a'
    group by p."oid",
             p.proname,
             n.nspname
    limit 10
) a;

-- get_index_comment

select get_index_comment(c.relname, n.nspname),
       get_index_comment(c."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_class c on c.tableoid = d.classoid and c."oid" = d.objoid
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('i', 'I')
limit 10;

-- get_large_object_comment

select s."oid",
      get_large_object_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_largeobject_metadata s on s."oid" = d.objoid
where d.classoid = 'pg_catalog.pg_largeobject'::regclass
limit 10;

-- get_policy_comment

select get_policy_comment(p.polname, c.relname, n.nspname),
       get_policy_comment(p."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_policy p on p.tableoid = d.classoid and p."oid" = d.objoid
join pg_catalog.pg_class c on p.polrelid = c."oid"
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
limit 10;

-- get_rule_comment

select get_rule_comment(r.rulename, c.relname, n.nspname),
       get_rule_comment(r."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_rewrite r on r.tableoid = d.classoid and r."oid" = d.objoid
join pg_catalog.pg_class c on r.ev_class = c."oid"
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
limit 10;

-- get_schema_comment

select get_schema_comment(s.nspname),
       get_schema_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_namespace s on s.tableoid = d.classoid and s."oid" = d.objoid
limit 10;

-- get_sequence_comment

select get_sequence_comment(c.relname, n.nspname),
       get_sequence_comment(c."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_class c on c.tableoid = d.classoid and c."oid" = d.objoid
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
where c.relkind = 'S'
limit 10;

-- get_table_comment

select get_table_comment(c.relname, n.nspname),
       get_table_comment(c."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_class c on c.tableoid = d.classoid and c."oid" = d.objoid
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('r', 'p')
limit 10;

-- get_trigger_comment

select get_trigger_comment(t.tgname, c.relname, n.nspname),
       get_trigger_comment(t."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_trigger t on t.tableoid = d.classoid and t."oid" = d.objoid
join pg_catalog.pg_class c on t.tgrelid = c."oid"
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
limit 10;

-- get_view_comment

select get_view_comment(c.relname, n.nspname),
       get_view_comment(c."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_class c on c.tableoid = d.classoid and c."oid" = d.objoid
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('v', 'm')
limit 10;


--////////////////////////////////////////
-- Extension
--////////////////////////////////////////

-- get_extension_comment

select get_extension_comment(s.extname),
       get_extension_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_extension s on s.tableoid = d.classoid and s."oid" = d.objoid
limit 10;

-- get_language_comment

select get_language_comment(objoid)
from pg_catalog.pg_description
where classoid = 'pg_catalog.pg_language'::regclass
limit 10;

-- get_transform_comment

select get_transform_comment(l.lanname, t.typname, n.nspname),
       get_transform_comment(tr."oid"),
       get_transform_comment(l."oid", t."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_transform tr on tr.tableoid = d.classoid and tr."oid" = d.objoid
join pg_catalog.pg_type t on tr.trftype = t."oid"
join pg_catalog.pg_language l on tr.trflang = l."oid"
join pg_catalog.pg_namespace n on n."oid" = t.typnamespace
limit 10;

select get_transform_comment('plpython3u', 'hstore');


--////////////////////////////////////////
-- Foreign objects
--////////////////////////////////////////

-- get_fdw_comment

select get_fdw_comment(s.fdwname),
       get_fdw_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_foreign_data_wrapper s on s.tableoid = d.classoid and s."oid" = d.objoid
limit 10;

-- get_foreign_table_comment

select get_foreign_table_comment(c.relname, n.nspname),
       get_foreign_table_comment(c."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_class c on c.tableoid = d.classoid and c."oid" = d.objoid
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind = 'f'
limit 10;

-- get_server_comment

select get_server_comment(s.srvname),
       get_server_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_foreign_server s on s.tableoid = d.classoid and s."oid" = d.objoid
limit 10;


--////////////////////////////////////////
-- Full-text search
--////////////////////////////////////////

-- get_ts_config_comment

select get_ts_config_comment(s.cfgname, n.nspname),
       get_ts_config_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_ts_config s on s.tableoid = d.classoid and s."oid" = d.objoid
join pg_catalog.pg_namespace n on n."oid" = s.cfgnamespace
limit 10;

select get_ts_config_comment('german');

-- get_ts_dictionary_comment

select get_ts_dictionary_comment(s.dictname, n.nspname),
       get_ts_dictionary_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_ts_dict s on s.tableoid = d.classoid and s."oid" = d.objoid
join pg_catalog.pg_namespace n on n."oid" = s.dictnamespace
limit 10;

select get_ts_dictionary_comment('german_stem');

-- get_ts_parser_comment

select get_ts_parser_comment(s.prsname, n.nspname),
       get_ts_parser_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_ts_parser s on s.tableoid = d.classoid and s."oid" = d.objoid
join pg_catalog.pg_namespace n on n."oid" = s.prsnamespace
limit 10;

select get_ts_parser_comment('default');

-- get_ts_template_comment

select get_ts_template_comment(s.tmplname, n.nspname),
       get_ts_template_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_ts_template s on s.tableoid = d.classoid and s."oid" = d.objoid
join pg_catalog.pg_namespace n on n."oid" = s.tmplnamespace
limit 10;

select get_ts_template_comment('synonym');


--////////////////////////////////////////
-- Replication
--////////////////////////////////////////

-- get_publication_comment

select get_publication_comment(s.pubname),
       get_publication_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_publication s on s.tableoid = d.classoid and s."oid" = d.objoid
limit 10;

-- get_subscription_comment

select get_subscription_comment(s.subname),
       get_subscription_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_subscription s on s.tableoid = d.classoid and s."oid" = d.objoid
limit 10;


--////////////////////////////////////////
-- System objects
--////////////////////////////////////////

-- get_collation_comment

select get_collation_comment(s.collname, n.nspname),
       get_collation_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_collation s on s.tableoid = d.classoid and s."oid" = d.objoid
join pg_catalog.pg_namespace n on s.collnamespace = n."oid"
limit 10;

select get_collation_comment('ru-RU-x-icu');

-- get_conversion_comment

select get_conversion_comment(s.conname, n.nspname),
       get_conversion_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_conversion s on s.tableoid = d.classoid and s."oid" = d.objoid
join pg_catalog.pg_namespace n on s.connamespace = n."oid"
limit 10;

select get_conversion_comment('utf8_to_windows_1251');

-- get_database_comment

select get_database_comment(s.datname),
       get_database_comment(s."oid")
from pg_catalog.pg_shdescription d
join pg_catalog.pg_database s on s.tableoid = d.classoid and s."oid" = d.objoid
limit 10;

-- get_role_comment

select get_role_comment(s.rolname),
       get_role_comment(s."oid")
from pg_catalog.pg_shdescription d
join pg_catalog.pg_authid s on s.tableoid = d.classoid and s."oid" = d.objoid
limit 10;

-- get_statistics_comment

select get_statistics_comment(s.stxname, n.nspname),
       get_statistics_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_statistic_ext s on s.tableoid = d.classoid and s."oid" = d.objoid
join pg_catalog.pg_namespace n on s.stxnamespace = n."oid"
limit 10;

-- get_tablespace_comment

select get_tablespace_comment(s.spcname),
       get_tablespace_comment(s."oid")
from pg_catalog.pg_shdescription d
join pg_catalog.pg_tablespace s on s.tableoid = d.classoid and s."oid" = d.objoid
limit 10;


--////////////////////////////////////////
-- Types and operators
--////////////////////////////////////////

-- get_access_method_comment

select get_access_method_comment(s.amname),
       get_access_method_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_am s on s.tableoid = d.classoid and s."oid" = d.objoid;

-- get_cast_comment

select get_cast_comment(ts.typname, tt.typname, ns.nspname, nt.nspname),
       get_cast_comment(c."oid"),
       get_cast_comment(c.castsource, c.casttarget)
from pg_catalog.pg_description d
join pg_catalog.pg_cast c on c.tableoid = d.classoid and c."oid" = d.objoid
join pg_catalog.pg_type ts on c.castsource = ts."oid"
join pg_catalog.pg_type tt on c.casttarget = tt."oid"
join pg_catalog.pg_namespace ns on ns."oid" = ts.typnamespace
join pg_catalog.pg_namespace nt on nt."oid" = tt.typnamespace
limit 10;

select get_cast_comment('int', 'bigint')
union all
select get_cast_comment('text[]', 'hstore');

-- get_operator_class_comment

select get_operator_class_comment(o.opcname, a.amname, n.nspname),
       get_operator_class_comment(o."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_opclass o on o.tableoid = d.classoid and o."oid" = d.objoid
join pg_catalog.pg_am a on o.opcmethod = a."oid"
join pg_catalog.pg_namespace n on n."oid" = o.opcnamespace
limit 10;

select get_operator_class_comment('text_ops')
union all
select get_operator_class_comment('text_ops', 'hash');

-- get_operator_comment

select get_operator_comment(o.oprname, tl.typname, tr.typname, nop.nspname, ntl.nspname, ntr.nspname),
            get_operator_comment(o."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_operator o on o.tableoid = d.classoid and o."oid" = d.objoid
join pg_catalog.pg_namespace nop on o.oprnamespace = nop."oid"
left join pg_catalog.pg_type tl on o.oprleft = tl."oid"
left join pg_catalog.pg_namespace ntl on tl.typnamespace = ntl."oid"
left join pg_catalog.pg_type tr on o.oprright = tr."oid"
left join pg_catalog.pg_namespace ntr on tr.typnamespace = ntr."oid"
limit 10;

select get_operator_comment('>=', 'int', 'bigint');

-- get_operator_family_comment

select get_operator_family_comment(o.opfname, a.amname, n.nspname),
       get_operator_family_comment(o."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_opfamily o on o.tableoid = d.classoid and o."oid" = d.objoid
join pg_catalog.pg_am a on o.opfmethod = a."oid"
join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace
limit 10;

select get_operator_family_comment('text_ops')
union all
select get_operator_family_comment('text_ops', 'hash');

-- get_type_comment

select get_type_comment(s.typname, n.nspname),
       get_type_comment(s."oid")
from pg_catalog.pg_description d
join pg_catalog.pg_type s on s.tableoid = d.classoid and s."oid" = d.objoid
join pg_catalog.pg_namespace n on s.typnamespace = n."oid"
limit 10;

select get_type_comment('integer');




--========================================
-- DCL
--========================================

--########################################
-- Create
--########################################


-- get_object_acl_def

select *
from
(
       select c."oid",
              c.relnamespace::regnamespace || '.' || c.relname || '.' || a.attname,
              get_object_acl_def(c.tableoid, c."oid", a.attnum)
       from pg_catalog.pg_class c
       join pg_catalog.pg_attribute a on a.attrelid = c."oid"
       where
              a.attnum > 0
              and a.attisdropped is false
              and a.attacl is not null -- default ACL for columns is always null (as of PG15)
       limit 10
) a

union all

select *
from
(
       select "oid",
              relnamespace::regnamespace || '.' || relname,
              get_object_acl_def(tableoid, "oid", 0)
       from pg_catalog.pg_class
       where relkind in ('r', 'p', 'm', 'v', 'f', 'S', 't')
       limit 10
) a

union all

select *
from
(
       select "oid",
              datname,
              get_object_acl_def(tableoid, "oid", 0)
       from pg_catalog.pg_database
       limit 10
) a

union all

select *
from
(
       select "oid",
              fdwname,
              get_object_acl_def(tableoid, "oid", 0)
       from pg_catalog.pg_foreign_data_wrapper
       limit 10
) a

union all

select *
from
(
       select "oid",
              srvname,
              get_object_acl_def(tableoid, "oid", 0)
       from pg_catalog.pg_foreign_server
       limit 10
) a

union all

select *
from
(
       select "oid",
              lanname,
              get_object_acl_def(tableoid, "oid", 0)
       from pg_catalog.pg_language
       limit 10
) a

union all

select *
from
(
       select "oid",
              "oid"::text as lomname,
              get_object_acl_def(tableoid, "oid", 0)
       from pg_catalog.pg_largeobject_metadata
       limit 10
) a

union all

select *
from
(
       select "oid",
              nspname,
              get_object_acl_def(tableoid, "oid", 0)
       from pg_catalog.pg_namespace
       limit 10
) a

union all

select *
from
(
       select "oid",
              pronamespace::regnamespace || '.' || proname,
              get_object_acl_def(tableoid, "oid", 0)
       from pg_catalog.pg_proc
       limit 10
) a

union all

select *
from
(
       select "oid",
              spcname,
              get_object_acl_def(tableoid, "oid", 0)
       from pg_catalog.pg_tablespace
       limit 10
) a

union all

select *
from
(
       select "oid",
              typnamespace::regnamespace || '.' || typname,
              get_object_acl_def(tableoid, "oid", 0)
       from pg_catalog.pg_type
       limit 10
) a;


--////////////////////////////////////////
-- Basic objects
--////////////////////////////////////////


-- get_aggregate_acl_def

select get_aggregate_acl_def('max', array['bigint']),
       get_aggregate_acl_def('count'),
       get_aggregate_acl_def('percentile_cont', array['float'], array['interval']),
       get_aggregate_acl_def('mode', array['anyelement']);

select get_aggregate_acl_def(proname, direct_typnames, order_typnames, nspname, direct_typschemas, order_typschemas),
       get_aggregate_acl_def("oid", false)
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typnames,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typschemas,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typschemas
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    join pg_catalog.pg_aggregate a on a.aggfnoid = p."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind = 'a'
    group by p."oid",
             p.proname,
             n.nspname,
             a.aggnumdirectargs
    limit 10
) a;

-- get_column_acl_def

select get_column_acl_def(a.attname, c.relname, n.nspname),
       get_column_acl_def(a.attname, c."oid"),
       get_column_acl_def(a.attnum, c."oid", false)
from pg_catalog.pg_attribute a
join pg_catalog.pg_class c on c."oid" = a.attrelid
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where a.attacl is not null
limit 10;

-- get_function_acl_def

select get_function_acl_def('version'),
       get_function_acl_def('substring', array['text', 'int']),
       get_function_acl_def('get_function_acl_def', array['text', $$text[]$$, 'text', $$text[]$$, 'bool']),
       get_function_acl_def('get_function_acl_def', array['oid', 'bool']);

select get_function_acl_def(proname, typnames, nspname, typschemas),
       get_function_acl_def("oid", false)
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else array_agg(t.typname order by pat.rn) end as typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else array_agg(nt.nspname order by pat.rn) end as typschemas
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind != 'a'
    group by p."oid",
             p.proname,
             n.nspname
    limit 10
) a;

-- get_large_object_acl_def

select "oid",
       get_large_object_acl_def("oid")
from pg_catalog.pg_largeobject_metadata
limit 10;

-- get_schema_acl_def

select get_schema_acl_def(nspname),
       get_schema_acl_def("oid")
from pg_catalog.pg_namespace
limit 10;

-- get_sequence_acl_def

select get_sequence_acl_def(c.relname, n.nspname),
       get_sequence_acl_def(c."oid")
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
where c.relkind = 'S'
limit 10;

-- get_table_acl_def

select get_table_acl_def(c.relname, n.nspname),
       get_table_acl_def(c."oid")
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('r', 'p', 't', 'f', 'v', 'm')
limit 10;

select get_table_acl_def('pg_trigger');


--////////////////////////////////////////
-- Extensions
--////////////////////////////////////////

-- get_language_acl_def

select get_language_acl_def(lanname),
       get_language_acl_def("oid")
from pg_catalog.pg_language
limit 10;


--////////////////////////////////////////
-- Foreign objects
--////////////////////////////////////////

-- get_fdw_acl_def

select get_fdw_acl_def(fdwname),
       get_fdw_acl_def("oid")
from pg_catalog.pg_foreign_data_wrapper
limit 10;

-- get_server_acl_def

select get_server_acl_def(srvname),
       get_server_acl_def("oid")
from pg_catalog.pg_foreign_server
limit 10;


--////////////////////////////////////////
-- System objects
--////////////////////////////////////////

-- get_database_acl_def

select get_database_acl_def(datname),
       get_database_acl_def("oid")
from pg_catalog.pg_database
limit 10;

-- get_default_acl_def

select get_default_acl_def(r.rolname, d.defaclobjtype, coalesce(n.nspname, nullif(d.defaclnamespace, 0)::text)),
       get_default_acl_def(d."oid")
from pg_catalog.pg_default_acl d
join pg_catalog.pg_authid r on d.defaclrole = r."oid"
left join pg_catalog.pg_namespace n on d.defaclnamespace = n."oid"
limit 10;

-- get_parameter_acl_def

select get_parameter_acl_def(parname),
       get_parameter_acl_def("oid")
from pg_catalog.pg_parameter_acl
limit 10;

-- get_tablespace_acl_def

select get_tablespace_acl_def(spcname),
       get_tablespace_acl_def("oid")
from pg_catalog.pg_tablespace
limit 10;


--////////////////////////////////////////
-- Types and operators
--////////////////////////////////////////

-- get_type_acl_def

select get_type_acl_def(t.typname, n.nspname),
       get_type_acl_def(t."oid")
from pg_catalog.pg_type t
join pg_catalog.pg_namespace n on t.typnamespace = n."oid"
limit 10;

select get_type_acl_def('integer');



--########################################
-- Drop
--########################################


-- get_object_acl_drop

select *
from
(
       select c."oid",
              c.relnamespace::regnamespace || '.' || c.relname || '.' || a.attname,
              get_object_acl_drop(c.tableoid, c."oid", a.attnum)
       from pg_catalog.pg_class c
       join pg_catalog.pg_attribute a on a.attrelid = c."oid"
       where
              a.attnum > 0
              and a.attisdropped is false
              and a.attacl is not null -- default ACL for columns is always null (as of PG15)
       limit 10
) a

union all

select *
from
(
       select "oid",
              relnamespace::regnamespace || '.' || relname,
              get_object_acl_drop(tableoid, "oid", 0)
       from pg_catalog.pg_class
       where relkind in ('r', 'p', 'm', 'v', 'f', 'S', 't')
       limit 10
) a

union all

select *
from
(
       select "oid",
              datname,
              get_object_acl_drop(tableoid, "oid", 0)
       from pg_catalog.pg_database
       limit 10
) a

union all

select *
from
(
       select "oid",
              fdwname,
              get_object_acl_drop(tableoid, "oid", 0)
       from pg_catalog.pg_foreign_data_wrapper
       limit 10
) a

union all

select *
from
(
       select "oid",
              srvname,
              get_object_acl_drop(tableoid, "oid", 0)
       from pg_catalog.pg_foreign_server
       limit 10
) a

union all

select *
from
(
       select "oid",
              lanname,
              get_object_acl_drop(tableoid, "oid", 0)
       from pg_catalog.pg_language
       limit 10
) a

union all

select *
from
(
       select "oid",
              "oid"::text as lomname,
              get_object_acl_drop(tableoid, "oid", 0)
       from pg_catalog.pg_largeobject_metadata
       limit 10
) a

union all

select *
from
(
       select "oid",
              nspname,
              get_object_acl_drop(tableoid, "oid", 0)
       from pg_catalog.pg_namespace
       limit 10
) a

union all

select *
from
(
       select "oid",
              pronamespace::regnamespace || '.' || proname,
              get_object_acl_drop(tableoid, "oid", 0)
       from pg_catalog.pg_proc
       limit 10
) a

union all

select *
from
(
       select "oid",
              spcname,
              get_object_acl_drop(tableoid, "oid", 0)
       from pg_catalog.pg_tablespace
       limit 10
) a

union all

select *
from
(
       select "oid",
              typnamespace::regnamespace || '.' || typname,
              get_object_acl_drop(tableoid, "oid", 0)
       from pg_catalog.pg_type
       limit 10
) a;


--////////////////////////////////////////
-- Basic objects
--////////////////////////////////////////

-- get_aggregate_acl_drop

select get_aggregate_acl_drop('max', array['bigint']),
       get_aggregate_acl_drop('count'),
       get_aggregate_acl_drop('percentile_cont', array['float'], array['interval']),
       get_aggregate_acl_drop('mode', array['anyelement']);

select get_aggregate_acl_drop(proname, direct_typnames, order_typnames, nspname, direct_typschemas, order_typschemas),
       get_aggregate_acl_drop("oid", true, false)
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typnames,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typschemas,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typschemas
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    join pg_catalog.pg_aggregate a on a.aggfnoid = p."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind = 'a'
    group by p."oid",
             p.proname,
             n.nspname,
             a.aggnumdirectargs
) a
limit 10;

-- get_column_acl_drop

select get_column_acl_drop(a.attname, c.relname, n.nspname),
       get_column_acl_drop(a.attname, c."oid"),
       get_column_acl_drop(a.attnum, c."oid", true, false)
from pg_catalog.pg_attribute a
join pg_catalog.pg_class c on c."oid" = a.attrelid
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where a.attacl is not null
limit 10;

-- get_function_acl_drop

select get_function_acl_drop('version'),
       get_function_acl_drop('substring', array['text', 'int']),
       get_function_acl_drop('get_function_acl_drop', array['text', $$text[]$$, 'text', $$text[]$$, 'bool', 'bool']),
       get_function_acl_drop('get_function_acl_drop', array['oid', 'bool', 'bool']);

select get_function_acl_drop(proname, typnames, nspname, typschemas),
       get_function_acl_drop("oid", true, false)
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else array_agg(t.typname order by pat.rn) end as typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else array_agg(nt.nspname order by pat.rn) end as typschemas
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind != 'a'
    group by p."oid",
             p.proname,
             n.nspname
) a
limit 10;

-- get_large_object_acl_drop

select "oid",
       get_large_object_acl_drop("oid", true, false)
from pg_catalog.pg_largeobject_metadata
limit 10;

-- get_schema_acl_drop

select get_schema_acl_drop(nspname),
       get_schema_acl_drop("oid", true, false)
from pg_catalog.pg_namespace
limit 10;

-- get_sequence_acl_drop

select get_sequence_acl_drop(c.relname, n.nspname),
       get_sequence_acl_drop(c."oid", true, false)
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
where c.relkind = 'S'
limit 10;

-- get_table_acl_drop

select get_table_acl_drop(c.relname, n.nspname),
       get_table_acl_drop(c."oid", true, false)
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('r', 'p', 't', 'f', 'v', 'm')
limit 10;

select get_table_acl_drop('pg_trigger');


--////////////////////////////////////////
-- Extension
--////////////////////////////////////////

-- get_language_acl_drop

select get_language_acl_drop(lanname),
       get_language_acl_drop("oid", true, false)
from pg_catalog.pg_language
limit 10;

--////////////////////////////////////////
-- Foreign objects
--////////////////////////////////////////

-- get_fdw_acl_drop

select get_fdw_acl_drop(fdwname),
       get_fdw_acl_drop("oid", true, false)
from pg_catalog.pg_foreign_data_wrapper
limit 10;

-- get_server_acl_drop

select get_server_acl_drop(srvname),
       get_server_acl_drop("oid", true, false)
from pg_catalog.pg_foreign_server
limit 10;


--////////////////////////////////////////
-- System objects
--////////////////////////////////////////

-- get_database_acl_drop

select get_database_acl_drop(datname),
       get_database_acl_drop("oid", true, false)
from pg_catalog.pg_database
limit 10;

-- get_default_acl_drop

select get_default_acl_drop(r.rolname, d.defaclobjtype, coalesce(n.nspname, nullif(d.defaclnamespace, 0)::text)),
       get_default_acl_drop(d."oid", true, false)
from pg_catalog.pg_default_acl d
join pg_catalog.pg_authid r on d.defaclrole = r."oid"
left join pg_catalog.pg_namespace n on d.defaclnamespace = n."oid"
limit 10;

-- get_parameter_acl_drop

select get_parameter_acl_drop(parname),
       get_parameter_acl_drop("oid", true, false)
from pg_catalog.pg_parameter_acl
limit 10;

-- get_tablespace_acl_drop

select get_tablespace_acl_drop(spcname),
       get_tablespace_acl_drop("oid", true, false)
from pg_catalog.pg_tablespace
limit 10;


--////////////////////////////////////////
-- Types and operators
--////////////////////////////////////////

-- get_type_acl_drop

select get_type_acl_drop(t.typname, n.nspname),
       get_type_acl_drop(t."oid", true, false)
from pg_catalog.pg_type t
join pg_catalog.pg_namespace n on t.typnamespace = n."oid"
limit 10;

select get_type_acl_drop('integer');



--########################################
-- Owner
--########################################


-- get_object_owner

select *
from
(
       select "oid",
              relnamespace::regnamespace || '.' || relname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_class
       where relkind in ('r', 'p', 'm', 'v', 'f', 'S', 'c')
       limit 10
) a

union all

select *
from
(
       select "oid",
              collnamespace::regnamespace || '.' || collname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_collation
       limit 10
) a

union all

select *
from
(
       select "oid",
              connamespace::regnamespace || '.' || conname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_conversion
       limit 10
) a

union all

select *
from
(
       select "oid",
              datname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_database
       limit 10
) a

union all

select *
from
(
       select "oid",
              evtname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_event_trigger
       limit 10
) a

union all

select *
from
(
       select "oid",
              fdwname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_foreign_data_wrapper
       limit 10
) a

union all

select *
from
(
       select "oid",
              srvname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_foreign_server
       limit 10
) a

union all

select *
from
(
       select "oid",
              lanname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_language
       limit 10
) a

union all

select *
from
(
       select "oid",
              "oid"::text as lomname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_largeobject_metadata
       limit 10
) a

union all

select *
from
(
       select "oid",
              nspname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_namespace
       limit 10
) a

union all

select *
from
(
       select "oid",
              opcnamespace::regnamespace || '.' || opcname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_opclass
       limit 10
) a

union all

select *
from
(
       select "oid",
              oprnamespace::regnamespace || '.' || oprname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_operator
       limit 10
) a

union all

select *
from
(
       select "oid",
              opfnamespace::regnamespace || '.' || opfname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_opfamily
       limit 10
) a

union all

select *
from
(
       select "oid",
              pronamespace::regnamespace || '.' || proname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_proc
       limit 10
) a

union all

select *
from
(
       select "oid",
              pubname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_publication
       limit 10
) a

union all

select *
from
(
       select "oid",
              stxnamespace::regnamespace || '.' || stxname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_statistic_ext
       limit 10
) a

union all

select *
from
(
       select "oid",
              subname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_subscription
       limit 10
) a

union all

select *
from
(
       select "oid",
              spcname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_tablespace
       limit 10
) a

union all

select *
from
(
       select "oid",
              cfgnamespace::regnamespace || '.' || cfgname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_ts_config
       limit 10
) a

union all

select *
from
(
       select "oid",
              dictnamespace::regnamespace || '.' || dictname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_ts_dict
       limit 10
) a

union all

select *
from
(
       select "oid",
              typnamespace::regnamespace || '.' || typname,
              get_object_owner(tableoid, "oid")
       from pg_catalog.pg_type
       limit 10
) a;


--////////////////////////////////////////
-- Basic objects
--////////////////////////////////////////

-- get_aggregate_owner

select get_aggregate_owner('max', array['bigint']),
       get_aggregate_owner('count'),
       get_aggregate_owner('percentile_cont', array['float'], array['interval']),
       get_aggregate_owner('mode', array['anyelement']);

select get_aggregate_owner(proname, direct_typnames, order_typnames, nspname, direct_typschemas, order_typschemas),
       get_aggregate_owner("oid", true)
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typnames,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else (array_agg(t.typname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[1:a.aggnumdirectargs] end as direct_typschemas,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else (array_agg(nt.nspname order by pat.rn))[a.aggnumdirectargs + 1:] end as order_typschemas
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    join pg_catalog.pg_aggregate a on a.aggfnoid = p."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind = 'a'
    group by p."oid",
             p.proname,
             n.nspname,
             a.aggnumdirectargs
    limit 10
) a;

-- get_event_trigger_owner

select get_event_trigger_owner(evtname),
       get_event_trigger_owner("oid", true)
from pg_catalog.pg_event_trigger
limit 10;

-- get_function_owner

select get_function_owner('version'),
       get_function_owner('substring', array['text', 'int']),
       get_function_owner('get_function_owner', array['text', $$text[]$$, 'text', $$text[]$$, 'bool']),
       get_function_owner('get_function_owner', array['oid', 'bool']);

select get_function_owner(proname, typnames, nspname, typschemas, false),
       get_function_owner("oid", true)
from
(
    select p."oid",
           p.proname,
           n.nspname,
           case when array_agg(t.typname) = array[null]::name[] then null::text[] else array_agg(t.typname order by pat.rn) end as typnames,
           case when array_agg(nt.nspname) = array[null]::name[] then null::text[] else array_agg(nt.nspname order by pat.rn) end as typschemas
    from pg_catalog.pg_proc p
    join pg_catalog.pg_namespace n on p.pronamespace = n."oid"
    cross join lateral unnest(string_to_array(case when p.proargtypes = ''::oidvector then '0'::text else p.proargtypes::text end, ' ')::oid[]) with ordinality pat (in_argtypes, rn)
    left join pg_catalog.pg_type t on pat.in_argtypes = t."oid"
    left join pg_catalog.pg_namespace nt on nt."oid" = t.typnamespace
    where prokind != 'a'
    group by p."oid",
             p.proname,
             n.nspname
    limit 10
) a;

-- get_large_object_owner

select "oid",
       get_large_object_owner("oid")
from pg_catalog.pg_largeobject_metadata
limit 10;

-- get_schema_owner

select get_schema_owner(nspname),
       get_schema_owner("oid", true)
from pg_catalog.pg_namespace
limit 10;

-- get_sequence_owner

select get_sequence_owner(c.relname, n.nspname),
       get_sequence_owner(c."oid", true)
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on n."oid" = c.relnamespace
where c.relkind = 'S'
limit 10;

-- get_table_owner

select get_table_owner(c.relname, n.nspname),
       get_table_owner(c."oid", true)
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('r', 'p')
limit 10;

-- get_view_owner

select get_view_owner(c.relname, n.nspname),
       get_view_owner(c."oid", true)
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind in ('v', 'm')
limit 10;


--////////////////////////////////////////
-- Extension
--////////////////////////////////////////

-- get_language_owner

select get_language_owner(lanname),
       get_language_owner("oid", true)
from pg_catalog.pg_language
limit 10;


--////////////////////////////////////////
-- Foreign objects
--////////////////////////////////////////

-- get_fdw_owner

select get_fdw_owner(fdwname),
       get_fdw_owner("oid", true)
from pg_catalog.pg_foreign_data_wrapper
limit 10;

-- get_foreign_table_owner

select get_foreign_table_owner(c.relname, n.nspname),
       get_foreign_table_owner(c."oid", true)
from pg_catalog.pg_class c
join pg_catalog.pg_namespace n on c.relnamespace = n."oid"
where c.relkind = 'f'
limit 10;

-- get_server_owner

select get_server_owner(srvname),
       get_server_owner("oid", true)
from pg_catalog.pg_foreign_server
limit 10;


--////////////////////////////////////////
-- Full-text search
--////////////////////////////////////////

-- get_ts_config_owner

select get_ts_config_owner(c.cfgname, n.nspname),
       get_ts_config_owner(c."oid", true)
from pg_catalog.pg_ts_config c
join pg_catalog.pg_namespace n on n."oid" = c.cfgnamespace
limit 10;

select get_ts_config_owner('german');

-- get_ts_dictionary_owner

select get_ts_dictionary_owner(d.dictname, n.nspname),
       get_ts_dictionary_owner(d."oid", true)
from pg_catalog.pg_ts_dict d
join pg_catalog.pg_namespace n on n."oid" = d.dictnamespace
limit 10;

select get_ts_dictionary_owner('german_stem');


--////////////////////////////////////////
-- Replication
--////////////////////////////////////////

-- get_publication_owner

select get_publication_owner(pubname),
       get_publication_owner("oid", true)
from pg_catalog.pg_publication
limit 10;

-- get_subscription_owner

select get_subscription_owner(subname),
       get_subscription_owner("oid", true)
from pg_catalog.pg_subscription
limit 10;


--////////////////////////////////////////
-- System objects
--////////////////////////////////////////

-- get_collation_owner

select get_collation_owner(c.collname, n.nspname),
       get_collation_owner(c."oid", true)
from pg_catalog.pg_collation c
join pg_catalog.pg_namespace n on c.collnamespace = n."oid"
limit 10;

select get_collation_owner('ru-RU-x-icu');

-- get_conversion_owner

select get_conversion_owner(c.conname, n.nspname),
       get_conversion_owner(c."oid", true)
from pg_catalog.pg_conversion c
join pg_catalog.pg_namespace n on c.connamespace = n."oid"
limit 10;

select get_conversion_owner('utf8_to_windows_1251');

-- get_database_owner

select get_database_owner(datname),
       get_database_owner("oid", true)
from pg_catalog.pg_database
limit 10;

-- get_statistics_owner

select get_statistics_owner(s.stxname, n.nspname),
       get_statistics_owner(s."oid", true)
from pg_catalog.pg_statistic_ext s
join pg_catalog.pg_namespace n on s.stxnamespace = n."oid"
limit 10;

-- get_tablespace_owner

select get_tablespace_owner(spcname),
       get_tablespace_owner("oid", true)
from pg_catalog.pg_tablespace
limit 10;


--////////////////////////////////////////
-- Types and operators
--////////////////////////////////////////

-- get_operator_class_owner

select get_operator_class_owner(o.opcname, a.amname, n.nspname),
       get_operator_class_owner(o."oid", true)
from pg_catalog.pg_opclass o
join pg_catalog.pg_am a on o.opcmethod = a."oid"
join pg_catalog.pg_namespace n on n."oid" = o.opcnamespace
limit 10;

select get_operator_class_owner('text_ops')
union all
select get_operator_class_owner('text_ops', 'hash', null, false);

-- get_operator_family_owner

select get_operator_family_owner(o.opfname, a.amname, n.nspname),
       get_operator_family_owner(o."oid", true)
from pg_catalog.pg_opfamily o
join pg_catalog.pg_am a on o.opfmethod = a."oid"
join pg_catalog.pg_namespace n on n."oid" = o.opfnamespace
limit 10;

select get_operator_family_owner('text_ops')
union all
select get_operator_family_owner('text_ops', 'hash', null, false);

-- get_operator_owner

select get_operator_owner(o.oprname, tl.typname, tr.typname, nop.nspname, ntl.nspname, ntr.nspname),
    get_operator_owner(o."oid", true)
from pg_catalog.pg_operator o
join pg_catalog.pg_namespace nop on o.oprnamespace = nop."oid"
left join pg_catalog.pg_type tl on o.oprleft = tl."oid"
left join pg_catalog.pg_namespace ntl on tl.typnamespace = ntl."oid"
left join pg_catalog.pg_type tr on o.oprright = tr."oid"
left join pg_catalog.pg_namespace ntr on tr.typnamespace = ntr."oid"
limit 10;

select get_operator_owner('>=', 'int', 'bigint');

-- get_type_owner

select get_type_owner(t.typname, n.nspname),
       get_type_owner(t."oid", true)
from pg_catalog.pg_type t
join pg_catalog.pg_namespace n on t.typnamespace = n."oid"
limit 10;

select get_type_owner('integer');