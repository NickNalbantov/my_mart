-- Bit string to numeric
    -- Supporting fractional values

create or replace function bitstring_to_num
    (
        bit_string text
    )
returns numeric
language plpgsql
immutable
strict
as
$func$

    declare
        intg_text text;
        frac_text text;
        intg_num numeric;
        frac_num numeric := 0;
        res numeric;

    begin

        if substring(lower(bit_string),'^([0-1]+$|[0-1]+\.[0-1]+$)') is not null then

            -- integer part
            intg_text := substring(lower(bit_string),'^[0-1]+');
            execute format (
                            $fmt$
                                select sum(parts)
                                from
                                (
                                    select substring(%1$L, length(%1$L) - n + 1, 1)::numeric * pow(2::numeric, n - 1) as parts
                                    from generate_series(1, length(%1$L), 1) g(n)
                                ) sub
                            $fmt$,
                            intg_text
                           )
            into intg_num;

            -- fractional part
            if bit_string like '%.%' then
                frac_text := substring(lower(bit_string),'[^\.][0-1]*$');
                execute format (
                                $fmt$
                                    select sum(parts)
                                    from
                                    (
                                        select substring(%1$L, n, 1)::numeric * pow(2::numeric, -n) as parts
                                        from generate_series(1, length(%1$L), 1) g(n)
                                    ) sub
                                $fmt$,
                                frac_text
                               )
                into frac_num;
            end if;

        else
            raise warning 'Invalid bit string - %', bit_string;
            return null;

        end if;

        res := intg_num + frac_num;
        return res;

    end;
$func$;
comment on function bitstring_to_num(bit_string text) is 'Text bit string to numeric value';


-- varbit to num types
    -- Not supporting fractional values

create or replace function varbit_to_num
    (
        bit_string varbit
    )
returns numeric
language sql
immutable
strict
parallel safe
as
$func$

    select sum(parts)
    from
    (
        select get_bit(bit_string, length(bit_string) - n)::numeric * pow(2::numeric, n - 1) as parts
        from generate_series(1, length(bit_string), 1) g(n)
    ) sub;

$func$;
comment on function varbit_to_num(bit_string varbit) is 'Varbit to numeric value';


-- hex to num types
    -- Supporting fractional values
create or replace function hex_to_num
    (
        hex_string text
    )
returns numeric
language plpgsql
immutable
strict
as
$func$

    declare

        intg_text text;
        frac_text text;
        intg_num numeric;
        frac_num numeric := 0;
        res numeric;

    begin

        if substring(lower(hex_string),'^([0-9a-f]+$|[0-9a-f]+\.[0-9a-f]+$)') is not null then

            -- integer part
            intg_text := substring(lower(hex_string),'^[0-9a-f]+');
            execute format (
                            $fmt$
                                select sum(parts)
                                from (
                                        select (case substring(%1$L, length(%1$L) - n + 1, 1)
                                                when 'a' then 10::numeric
                                                when 'b' then 11::numeric
                                                when 'c' then 12::numeric
                                                when 'd' then 13::numeric
                                                when 'e' then 14::numeric
                                                when 'f' then 15::numeric
                                                else substring(%1$L, length(%1$L) - n + 1, 1)::numeric
                                                end) * pow(16::numeric, n - 1) as parts
                                        from generate_series(1, length(%1$L)) g(n)
                                    ) sub
                            $fmt$,
                            intg_text
                           )
            into intg_num;

            -- fractional part
            if strpos(hex_string, '.') != 0 then
                frac_text := substring(lower(hex_string),'[^\.][0-9a-f]*$');
                execute format (
                                $fmt$
                                select sum(parts)
                                from (
                                        select (case substring(%1$L, n, 1)
                                                    when 'a' then 10::numeric
                                                    when 'b' then 11::numeric
                                                    when 'c' then 12::numeric
                                                    when 'd' then 13::numeric
                                                    when 'e' then 14::numeric
                                                    when 'f' then 15::numeric
                                                    else substring(%1$L, n, 1)::numeric
                                                end) * pow(16::numeric, -n) as parts
                                        from generate_series(1, length(%1$L)) g(n)
                                        ) sub
                                $fmt$,
                                frac_text
                              )
                into frac_num;
            end if;

        else
            raise warning 'Invalid hex-string - %', hex_string;
            return null;

        end if;

        res := intg_num + frac_num;
        return res;

    end;
$func$;
comment on function hex_to_num(hex_string text) is 'Hex string to numeric value';


-- numeric to bit string
    -- Supporting fractional values

create or replace function num_to_bitstring
    (
        num anycompatiblenonarray,
        frac_limit integer default 100 -- max loop limit in case of infinite fractions (periodic, irrational)
    )
returns text
language plpgsql
strict
immutable
as
$func$

    declare

        intg numeric := trunc(num::numeric);
        int_res text := '';
        frac numeric := num::numeric - trunc(num::numeric);
        frac_res text := '';
        res text;

    begin

        -- Checking loop breaker value
        if frac_limit not between 0 and 16383 then
            raise warning 'Invalid value for fractional part length limit. Must be from 0 to 16383';
            return null;
        end if;

        -- int
        while intg > 0 loop
            int_res := mod(intg, 2) || int_res;
            intg := trunc(intg / 2.0::numeric); -- should be numeric, or there'll be problems with exact calculations near limits for values of bigint type
        end loop;

        -- frac
        if frac != 0 then
            while frac != 0 and length(frac_res) < frac_limit loop
                frac := frac * 2.0::numeric;
                frac_res := frac_res || trunc(frac);
                frac := frac - trunc(frac);
            end loop;
        end if;

        -- Result

        res := int_res || (case when frac_res = '' then frac_res else '.' || frac_res end);
        return res;

    end;
$func$;
comment on function num_to_bitstring(num anycompatiblenonarray, frac_limit integer) is 'Any number to text bit string';


-- num types to varbit
    -- Not supporting fractional values

create or replace function num_to_varbit
    (
        num anycompatiblenonarray
    )
returns varbit
language plpgsql
strict
immutable
as
$func$

    declare

        quotient numeric := num::numeric;
        text_res text := '';
        res varbit;

    begin

        -- Checking start point
        if num < 0 or num - round(num) != 0 then
            raise warning 'Argument must be a natural number';
            return null;
        end if;

        --  Calculation
        if num = 0 then
            res := '0'::varbit;

        else

            while quotient != 1 loop
                if mod(quotient, 2) = 1 then
                    text_res := '1' || text_res;
                    quotient := floor(quotient / 2 - 0.5); -- hack to solve the problem with precision of division for values > 3*10^17
                else
                    text_res :=  '0' || text_res;
                    quotient := quotient / 2;
                end if;
            end loop;

            res := ('1' || text_res)::varbit;

        end if;

        return res;
    end;
$func$;
comment on function num_to_varbit(num anycompatiblenonarray) is 'Natural number to varbit';


-- num types to hex
    -- Supporting fractional values

create or replace function num_to_hex
    (
        num anycompatiblenonarray,
        frac_limit integer default 100 -- max loop limit in case of infinite fractions (periodic, irrational)
    )
returns text
language plpgsql
strict
immutable
as
$func$

    declare

        intg numeric := trunc(num::numeric);
        int_res text := '';
        frac numeric := num::numeric - trunc(num::numeric);
        frac_res text := '';
        res text;

    begin

        -- Checking loop breaker value
        if frac_limit not between 0 and 16383 then
            raise warning 'Invalid value for fractional part length limit. Must be from 0 to 16383';
            return null;
        end if;

        -- int
        while intg > 0 loop
            int_res := (case mod(intg, 16) when 15 then 'F'
                                           when 14 then 'E'
                                           when 13 then 'D'
                                           when 12 then 'C'
                                           when 11 then 'B'
                                           when 10 then 'A'
                                           else mod(intg, 16)::text end) || int_res;
            intg := trunc(intg / 16.0::numeric); -- should be numeric, or there'll be problems with exact calculations near limits for values of bigint type
        end loop;

        -- frac
        if frac != 0 then
            while frac != 0 and length(frac_res) < frac_limit loop
                frac := frac * 16.0::numeric;
                frac_res := frac_res || (case trunc(frac) when 15 then 'F'
                                                          when 14 then 'E'
                                                          when 13 then 'D'
                                                          when 12 then 'C'
                                                          when 11 then 'B'
                                                          when 10 then 'A'
                                                          else trunc(frac::numeric)::text end);
                frac := frac - trunc(frac);
            end loop;
        end if;

        -- Result

        res := int_res || (case when frac_res = '' then frac_res else '.' || frac_res end);
        return res;

    end;
$func$;
comment on function num_to_hex(num anycompatiblenonarray, frac_limit integer) is 'Any number to hex string';