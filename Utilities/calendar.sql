-- Examples:

-- select *
-- from month_calendar();

-- select *
-- from month_calendar(7);

-- select *
-- from month_calendar(7, 2000);

-- select *
-- from month_calendar_text('jul', '2000');

-- select *
-- from month_calendar_text('October', '2023');

-- select *
-- from month_calendar_date('15.07.2000'::date);

-- select *
-- from year_calendar();

-- select *
-- from year_calendar(2024);

-- select *
-- from year_calendar_text('2025');

-- select *
-- from year_calendar_date((current_date + '2 year'::interval)::date);



-- Month calendar

    -- Digits input (anycompatiblenonarray)

create or replace function month_calendar
(
    month_ anycompatiblenonarray default extract(month from clock_timestamp()),
    year_ anycompatiblenonarray default extract(year from clock_timestamp())
)
returns table
(
    yr smallint,
    mon smallint,
    week_num smallint,
    monday smallint,
    tuesday smallint,
    wednesday smallint,
    thursday smallint,
    friday smallint,
    saturday smallint,
    sunday smallint
)
language plpgsql
strict
immutable
rows 6
as
$func$

-- Examples:

-- select *
-- from month_calendar();

-- select *
-- from month_calendar(7);

-- select *
-- from month_calendar(7, 2000);

    begin

        return query execute format
            (
             $fmt$
                    select  %2$L::smallint as yr,
                            %1$L::smallint as mon,
                            week_num::smallint as week_num,
                            (array_agg(distinct case when week_day = 1 then dy else null end))[1]::smallint as monday,
                            (array_agg(distinct case when week_day = 2 then dy else null end))[1]::smallint as tuesday,
                            (array_agg(distinct case when week_day = 3 then dy else null end))[1]::smallint as wednesday,
                            (array_agg(distinct case when week_day = 4 then dy else null end))[1]::smallint as thursday,
                            (array_agg(distinct case when week_day = 5 then dy else null end))[1]::smallint as friday,
                            (array_agg(distinct case when week_day = 6 then dy else null end))[1]::smallint as saturday,
                            (array_agg(distinct case when week_day = 7 then dy else null end))[1]::smallint as sunday
                    from
                    (
                        select extract(day from dt) as dy,
                               extract(isodow from dt) as week_day,
                               extract(week from dt) as week_num
                        from generate_series(
                                                to_date('01.%1$s.%2$s', 'dd.mm.yyyy')::timestamp, -- cast to timestamp is required as a workaround for specific timezones with DST
                                                to_date('01.%1$s.%2$s', 'dd.mm.yyyy')::timestamp + '1 month'::interval - '1 day'::interval,
                                                '1 day'::interval
                                            ) d (dt)
                    ) r
                    group by week_num
             $fmt$, month_, year_
            );
    end;

$func$;

comment on function month_calendar(month_ anycompatiblenonarray, year_ anycompatiblenonarray)
    is 'Returns calendar for a month as a table';


    -- Text input

create or replace function month_calendar_text
(
    month_ text default to_char(clock_timestamp(), 'Month'),
    year_ text default to_char(clock_timestamp(), 'YYYY')
)
returns table
(
    yr smallint,
    mon smallint,
    week_num smallint,
    monday smallint,
    tuesday smallint,
    wednesday smallint,
    thursday smallint,
    friday smallint,
    saturday smallint,
    sunday smallint
)
language plpgsql
strict
immutable
rows 6
as
$func$

-- Example:

-- select *
-- from month_calendar_text('jul', '2000');

-- select *
-- from month_calendar_text('October', '2023');

    declare
        mon_norm text;
        months_names text[];
        mon_name_position int;

    begin

        -- Parsing input text

        mon_norm := trim(ltrim(lower(month_), '0'), ' ');

        select array[
                        trim(to_char('01.01.2000'::date, 'mon'), ' '),
                        trim(to_char('01.01.2000'::date, 'month'), ' '),
                        extract(month from '01.01.2000'::date)::text,
                        trim(to_char('01.02.2000'::date, 'mon'), ' '),
                        trim(to_char('01.02.2000'::date, 'month'), ' '),
                        extract(month from '01.02.2000'::date)::text,
                        trim(to_char('01.03.2000'::date, 'mon'), ' '),
                        trim(to_char('01.03.2000'::date, 'month'), ' '),
                        extract(month from '01.03.2000'::date)::text,
                        trim(to_char('01.04.2000'::date, 'mon'), ' '),
                        trim(to_char('01.04.2000'::date, 'month'), ' '),
                        extract(month from '01.04.2000'::date)::text,
                        trim(to_char('01.05.2000'::date, 'mon'), ' '),
                        trim(to_char('01.05.2000'::date, 'month'), ' '),
                        extract(month from '01.05.2000'::date)::text,
                        trim(to_char('01.06.2000'::date, 'mon'), ' '),
                        trim(to_char('01.06.2000'::date, 'month'), ' '),
                        extract(month from '01.06.2000'::date)::text,
                        trim(to_char('01.07.2000'::date, 'mon'), ' '),
                        trim(to_char('01.07.2000'::date, 'month'), ' '),
                        extract(month from '01.07.2000'::date)::text,
                        trim(to_char('01.08.2000'::date, 'mon'), ' '),
                        trim(to_char('01.08.2000'::date, 'month'), ' '),
                        extract(month from '01.08.2000'::date)::text,
                        trim(to_char('01.09.2000'::date, 'mon'), ' '),
                        trim(to_char('01.09.2000'::date, 'month'), ' '),
                        extract(month from '01.09.2000'::date)::text,
                        trim(to_char('01.10.2000'::date, 'mon'), ' '),
                        trim(to_char('01.10.2000'::date, 'month'), ' '),
                        extract(month from '01.10.2000'::date)::text,
                        trim(to_char('01.11.2000'::date, 'mon'), ' '),
                        trim(to_char('01.11.2000'::date, 'month'), ' '),
                        extract(month from '01.11.2000'::date)::text,
                        trim(to_char('01.12.2000'::date, 'mon'), ' '),
                        trim(to_char('01.12.2000'::date, 'month'), ' '),
                        extract(month from '01.12.2000'::date)::text
                    ]::text[]
        into months_names;

        if mon_norm != all(months_names) then
            raise exception E'Invalid input (''%'') of month''s name.\nValid formats (in any case, could be with or without leading ''0''):\n%',
                month_,
                array_to_string(months_names, ', ');
        end if;

        mon_name_position := array_position(months_names, mon_norm);

        -- Result

        return query execute format
            (
             $fmt$
                    select  extract(year from to_date('01.%1$s.%2$s', 'dd.%3$s.yyyy'))::smallint as yr,
                            extract(month from to_date('01.%1$s.%2$s', 'dd.%3$s.yyyy'))::smallint as mon,
                            week_num::smallint as week_num,
                            (array_agg(distinct case when week_day = 1 then dy else null end))[1]::smallint as monday,
                            (array_agg(distinct case when week_day = 2 then dy else null end))[1]::smallint as tuesday,
                            (array_agg(distinct case when week_day = 3 then dy else null end))[1]::smallint as wednesday,
                            (array_agg(distinct case when week_day = 4 then dy else null end))[1]::smallint as thursday,
                            (array_agg(distinct case when week_day = 5 then dy else null end))[1]::smallint as friday,
                            (array_agg(distinct case when week_day = 6 then dy else null end))[1]::smallint as saturday,
                            (array_agg(distinct case when week_day = 7 then dy else null end))[1]::smallint as sunday
                    from
                    (
                        select extract(day from dt) as dy,
                               extract(isodow from dt) as week_day,
                               extract(week from dt) as week_num
                        from generate_series(
                                                to_date('01.%1$s.%2$s', 'dd.%3$s.yyyy')::timestamp, -- cast to timestamp is required as a workaround for specific timezones with DST
                                                to_date('01.%1$s.%2$s', 'dd.%3$s.yyyy')::timestamp + '1 month'::interval - '1 day'::interval,
                                                '1 day'::interval
                                            ) d (dt)
                    ) r
                    group by week_num
             $fmt$,
             initcap(months_names[mon_name_position]),
             year_,
             case when length(mon_norm) > 3 then 'Month'
                  when length(mon_norm) = 3 then 'Mon'
                  else 'mm' end
            );
    end;

$func$;

comment on function month_calendar_text(month_ text, year_ text)
    is 'Returns calendar for a month as a table';


    -- Date input

create or replace function month_calendar_date
(
    in_date date default current_date
)
returns table
(
    yr smallint,
    mon smallint,
    week_num smallint,
    monday smallint,
    tuesday smallint,
    wednesday smallint,
    thursday smallint,
    friday smallint,
    saturday smallint,
    sunday smallint
)
language sql
strict
immutable
rows 6
as
$func$

-- Example:

-- select *
-- from month_calendar_date('15.07.2000'::date);

    select (extract(year from in_date))::smallint as yr,
           (extract(month from in_date))::smallint as mon,
           week_num_::smallint as week_num,
           (array_agg(distinct case when week_day = 1 then dy else null end))[1]::smallint as monday,
           (array_agg(distinct case when week_day = 2 then dy else null end))[1]::smallint as tuesday,
           (array_agg(distinct case when week_day = 3 then dy else null end))[1]::smallint as wednesday,
           (array_agg(distinct case when week_day = 4 then dy else null end))[1]::smallint as thursday,
           (array_agg(distinct case when week_day = 5 then dy else null end))[1]::smallint as friday,
           (array_agg(distinct case when week_day = 6 then dy else null end))[1]::smallint as saturday,
           (array_agg(distinct case when week_day = 7 then dy else null end))[1]::smallint as sunday
    from
    (
        select extract(day from dt) as dy,
               extract(isodow from dt) as week_day,
               extract(week from dt) as week_num_
        from generate_series(
                                date_trunc('month', in_date)::timestamp, -- cast to timestamp is required as a workaround for specific timezones with DST
                                date_trunc('month', in_date)::timestamp + '1 month'::interval - '1 day'::interval,
                                '1 day'::interval
                            ) d (dt)
    ) r
    group by week_num;

$func$;

comment on function month_calendar_date(in_date date)
    is 'Returns calendar for a month as a table';


-- Year calendar

    -- Digits input (anycompatiblenonarray)

create or replace function year_calendar
(
    year_ anycompatiblenonarray default extract(year from clock_timestamp())
)
returns table
(
    yr smallint,
    quart smallint,
    mon smallint,
    week_num smallint,
    monday smallint,
    tuesday smallint,
    wednesday smallint,
    thursday smallint,
    friday smallint,
    saturday smallint,
    sunday smallint
)
language plpgsql
strict
immutable
rows 54
as
$func$

-- Examples:

-- select *
-- from year_calendar();

-- select *
-- from year_calendar(2024);

    begin

        return query execute format
            (
             $fmt$
                    select isoyr::smallint as yr,
                           min(quart)::smallint as quart,
                           min(mon)::smallint as mon,
                           week_num::smallint as week_num,
                           (array_agg(distinct case when week_day = 1 then dy else null end))[1]::smallint as monday,
                           (array_agg(distinct case when week_day = 2 then dy else null end))[1]::smallint as tuesday,
                           (array_agg(distinct case when week_day = 3 then dy else null end))[1]::smallint as wednesday,
                           (array_agg(distinct case when week_day = 4 then dy else null end))[1]::smallint as thursday,
                           (array_agg(distinct case when week_day = 5 then dy else null end))[1]::smallint as friday,
                           (array_agg(distinct case when week_day = 6 then dy else null end))[1]::smallint as saturday,
                           (array_agg(distinct case when week_day = 7 then dy else null end))[1]::smallint as sunday
                    from
                    (
                        select extract(day from dt) as dy,
                               extract(isodow from dt) as week_day,
                               extract(week from dt) as week_num,
                               extract(quarter from dt) as quart,
                               extract(month from dt) as mon,
                               extract(isoyear from dt) as isoyr
                        from generate_series(
                                                to_date('01.01.%1$s', 'dd.mm.yyyy')::timestamp, -- cast to timestamp is required as a workaround for specific timezones with DST
                                                to_date('01.01.%1$s', 'dd.mm.yyyy')::timestamp + '1 year'::interval - '1 day'::interval,
                                                '1 day'::interval
                                            ) d (dt)
                    ) r
                    group by isoyr, week_num
             $fmt$, year_
            );
    end;
$func$;

comment on function year_calendar(year_ anycompatiblenonarray)
    is 'Returns calendar for an year as a table';


    -- Text input

create or replace function year_calendar_text
(
    year_ text default to_char(clock_timestamp(), 'YYYY')
)
returns table
(
    yr smallint,
    quart smallint,
    mon smallint,
    week_num smallint,
    monday smallint,
    tuesday smallint,
    wednesday smallint,
    thursday smallint,
    friday smallint,
    saturday smallint,
    sunday smallint
)
language plpgsql
strict
immutable
rows 54
as
$func$

-- Example:

-- select *
-- from year_calendar_text('2025');

    begin

        return query execute format
            (
             $fmt$
                    select isoyr::smallint as yr,
                           min(quart)::smallint as quart,
                           min(mon)::smallint as mon,
                           week_num::smallint as week_num,
                           (array_agg(distinct case when week_day = 1 then dy else null end))[1]::smallint as monday,
                           (array_agg(distinct case when week_day = 2 then dy else null end))[1]::smallint as tuesday,
                           (array_agg(distinct case when week_day = 3 then dy else null end))[1]::smallint as wednesday,
                           (array_agg(distinct case when week_day = 4 then dy else null end))[1]::smallint as thursday,
                           (array_agg(distinct case when week_day = 5 then dy else null end))[1]::smallint as friday,
                           (array_agg(distinct case when week_day = 6 then dy else null end))[1]::smallint as saturday,
                           (array_agg(distinct case when week_day = 7 then dy else null end))[1]::smallint as sunday
                    from
                    (
                        select extract(day from dt) as dy,
                               extract(isodow from dt) as week_day,
                               extract(week from dt) as week_num,
                               extract(quarter from dt) as quart,
                               extract(month from dt) as mon,
                               extract(isoyear from dt) as isoyr
                        from generate_series(
                                                to_date('01.01.%1$s', 'dd.mm.yyyy')::timestamp, -- cast to timestamp is required as a workaround for specific timezones with DST
                                                to_date('01.01.%1$s', 'dd.mm.yyyy')::timestamp + '1 year'::interval - '1 day'::interval,
                                                '1 day'::interval
                                            ) d (dt)
                    ) r
                    group by isoyr, week_num
             $fmt$, year_
            );
    end;

$func$;

comment on function year_calendar_text(year_ text)
    is 'Returns calendar for an year as a table';


    -- Date input

create or replace function year_calendar_date
(
    in_date date default current_date
)
returns table
(
    yr smallint,
    quart smallint,
    mon smallint,
    week_num smallint,
    monday smallint,
    tuesday smallint,
    wednesday smallint,
    thursday smallint,
    friday smallint,
    saturday smallint,
    sunday smallint
)
language sql
strict
immutable
rows 54
as
$func$

-- Example:

-- select *
-- from year_calendar_date((current_date + '2 year'::interval)::date);

    select isoyr::smallint as yr,
           min(quart_)::smallint as quart,
           min(mon_)::smallint as mon,
           week_num_::smallint as week_num,
           (array_agg(distinct case when week_day = 1 then dy else null end))[1]::smallint as monday,
           (array_agg(distinct case when week_day = 2 then dy else null end))[1]::smallint as tuesday,
           (array_agg(distinct case when week_day = 3 then dy else null end))[1]::smallint as wednesday,
           (array_agg(distinct case when week_day = 4 then dy else null end))[1]::smallint as thursday,
           (array_agg(distinct case when week_day = 5 then dy else null end))[1]::smallint as friday,
           (array_agg(distinct case when week_day = 6 then dy else null end))[1]::smallint as saturday,
           (array_agg(distinct case when week_day = 7 then dy else null end))[1]::smallint as sunday
    from
    (
        select extract(day from dt) as dy,
               extract(isodow from dt) as week_day,
               extract(week from dt) as week_num_,
               extract(quarter from dt) as quart_,
               extract(month from dt) as mon_,
               extract(isoyear from dt) as isoyr
            from generate_series(
                                    date_trunc('year', in_date)::timestamp, -- cast to timestamp is required as a workaround for specific timezones with DST
                                    date_trunc('year', in_date)::timestamp + '1 year'::interval - '1 day'::interval,
                                    '1 day'::interval
                                ) d (dt)
    ) r
    group by isoyr, week_num;

$func$;

comment on function year_calendar_date(in_date date)
    is 'Returns calendar for an year as a table';