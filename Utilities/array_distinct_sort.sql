-- Plain SQL version

create or replace function public.array_distinct_sort
    (
        arr_in anyarray, -- input array
        do_grouping bool default false, -- grouping (distinct) - true / no grouping - false
        do_sorting bool default true, -- sorting - true / no sorting - false
        asc_order bool default true, -- sorting order, asc - true / desc - false
        nulls_last bool default true -- nulls sorting, last - true / first - false
    )
returns anyarray
language sql
strict
immutable
parallel safe
as
$func$

    select
        case
            when do_grouping and do_sorting and asc_order and nulls_last then -- 1111
            (
                select array
                (
                    select distinct unnest(arr_in)
                    order by 1 asc nulls last
                )
            )

            when do_grouping and do_sorting and asc_order and not nulls_last then -- 1110
            (
                select array
                (
                    select distinct unnest(arr_in)
                    order by 1 asc nulls first
                )
            )

            when do_grouping and do_sorting and not asc_order and nulls_last then --1101
            (
                select array
                (
                    select distinct unnest(arr_in)
                    order by 1 desc nulls last
                )
            )

            when do_grouping and do_sorting and not asc_order and not nulls_last then -- 1100
            (
                select array
                (
                    select distinct unnest(arr_in)
                    order by 1 desc nulls first
                )
            )

            when not do_grouping and do_sorting and asc_order and nulls_last then -- 0111
            (
                select array
                (
                    select unnest(arr_in)
                    order by 1 asc nulls last
                )
            )

            when not do_grouping and do_sorting and asc_order and not nulls_last then --0110
            (
                select array
                (
                    select unnest(arr_in)
                    order by 1 asc nulls first
                )
            )

            when not do_grouping and do_sorting and not asc_order and nulls_last then -- 0101
            (
                select array
                (
                    select unnest(arr_in)
                    order by 1 desc nulls last
                )
            )

            when not do_grouping and do_sorting and not asc_order and not nulls_last then -- 0100
            (
                select array
                (
                    select unnest(arr_in)
                    order by 1 desc nulls first
                )
            )

            when do_grouping and not do_sorting then -- 10xx
            (
                select array_agg(i order by n) -- plain distinct does not keep original order
                from
                (
                    select distinct on (i)
                        i,
                        n
                    from unnest(arr_in) with ordinality a (i, n)
                    order by
                        i,
                        n
                ) x
            )

            else arr_in end; -- 00xx

$func$;
comment on function array_distinct_sort(arr_in anyarray, do_grouping bool, do_sorting bool, asc_order bool, nulls_last bool)
    is 'Function for sorting and grouping for any array, returns one-dimensional array of same type';