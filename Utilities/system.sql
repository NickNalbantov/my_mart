-- Switch for triggers

create or replace procedure switch_triggers
(
    enable_ boolean default true,
    schemas_ text[] default null,
    tables_ text[] default null,
    triggers_ text[] default null,
    user_only boolean default false
)
language plpgsql
as
$$
declare
    act text;
    trigs text;
    r record;
    qry text;
begin

    if enable_ is false then
        act = 'disable';
    else
        act = 'enable';
    end if;

    if triggers_ is null then
        if user_only is false then
           trigs := 'all';
        else
           trigs := 'user';
        end if;
    end if;

  -- Looping through chosen triggers
  for r in

    select
        n.nspname,
        c.relname,
        t.trig
    from pg_catalog.pg_class c
    left join pg_catalog.pg_namespace n on c.relnamespace = n.oid
    cross join unnest(current_schemas(false)) s (sch)
    full outer join unnest(schemas_) si (sch) on 1 = 1
    full outer join unnest(triggers_) with ordinality t (trig, rn) on 1 = 1
    where
        c.relhastriggers = true
        and (c.relkind in ('p', 'r', 'f') and ((user_only is false and tables_ is null) or tables_ is not null))
            or (c.relkind in ('r', 'f') and user_only is true and tables_ is null)
        and coalesce(si.sch, s.sch) = n.nspname
        and (case when tables_ is not null then c.relname = any(tables_) else true end)
    order by
        t.rn

    loop
        if triggers_ is not null then
            qry := format('alter table %I.%I %s trigger %I', r.nspname, r.relname, act, r.trig);
            execute qry;
        else
            qry := format('alter table %I.%I %s trigger %s', r.nspname, r.relname, act, trigs);
            execute qry;
        end if;

        raise notice 'Executed - %', qry;
    end loop;

end;
$$;

comment on procedure switch_triggers(enable_ boolean, schemas_ text[], tables_ text[], triggers_ text[], user_only boolean) is
     'enable_ boolean (def true), schemas_ text[] (def search_path), tables_ (def null - all tables with triggers in chosen schemas), triggers_ (def null), user_only (def false - ''user'', if false - ''all'')';


-- List of operators

create or replace view pg_operators as
(
    select
        o.oid as obj_oid,
        quote_ident(o.oprnamespace::regnamespace::text) || '.' || o.oprname as oper_name,
        quote_ident(t_l.typnamespace::regnamespace::text) || '.' || quote_ident(t_l.typname) as oper_left,
        quote_ident(t_r.typnamespace::regnamespace::text) || '.' || quote_ident(t_r.typname) as oper_right,
        quote_ident(t_res.typnamespace::regnamespace::text) || '.' || quote_ident(t_res.typname) as oper_result,
        o.oprcode as oper_code,
        d."description"
    from pg_catalog.pg_operator o
    join pg_catalog.pg_type t_l on o.oprleft = t_l.oid
    join pg_catalog.pg_type t_r on o.oprright = t_r.oid
    join pg_catalog.pg_type t_res on o.oprresult = t_res.oid
    left join pg_catalog.pg_description d on
        o."oid" = d.objoid
        and d.classoid = o.tableoid
    order by
        oper_name,
        oper_code
);
comment on view pg_operators is 'List of operators in the database';


-- List of comments

create or replace view pg_comments as
(
    select
        d.objoid as "oid",
        io."type" as obj_type,
        coalesce(io."identity", io."name") as obj_ident,
        d."description" as obj_comment
    from pg_catalog.pg_description d
    cross join lateral pg_catalog.pg_identify_object(d.classoid, d.objoid, d.objsubid) io ("type", "schema", "name", "identity")
    order by
        obj_type,
        obj_ident
);
comment on view pg_comments is 'List of comments in the database';


-- Tree of role dependencies

create or replace view pg_role_tree as
(
    with recursive role_tree as
    (
        select
            row_number() over(order by t.roleid) as branch,
            t.*
        from
        (
            select distinct
                1 as lvl,
                a.roleid::regrole as roleid,
                null::regrole as parent,
                null::bool as admin_option
            from pg_catalog.pg_auth_members a
            where not exists
                (
                    select
                    from pg_catalog.pg_auth_members b
                    where a.roleid = b."member"
                )
        ) t

        union all

        select
            t.branch,
            t.lvl + 1 as lvl,
            a."member"::regrole as roleid,
            a.roleid::regrole as parent,
            a.admin_option
        from pg_catalog.pg_auth_members a
        join role_tree t on t.roleid = a.roleid::regrole
    )

    select *
    from role_tree
    order by
        branch,
        lvl
);
comment on view pg_role_tree is 'Hierarchical tree of role dependencies';


-- Locks

create or replace view pg_lock_monitor as
(
    select
        coalesce(bgl.relation::regclass::text, bgl.locktype) as locked_item,
        now() - bda.query_start as waiting_duration, bda.pid as blocked_pid,
        bda.query as blocked_query,
        bdl.mode as blocked_mode,
        bda.usename as blocked_user,
        bda.client_addr as blocked_addr,
        bga.pid as blocking_pid,
        bga.query as blocking_query,
        bgl.mode as blocking_mode,
        bga.usename as blocking_user,
        bga.client_addr as blocking_addr
    from pg_catalog.pg_locks bdl
    join pg_stat_activity bda on bda.pid = bdl.pid
    join pg_catalog.pg_locks bgl on
        (
            bgl.transactionid = bdl.transactionid
            or bgl.relation = bdl.relation
            and bgl.locktype = bdl.locktype
        )
        and bgl.pid != bdl.pid
    join pg_stat_activity bga on bga.pid = bgl.pid and bga.datid = bda.datid
    where not bdl.granted
    order by
        waiting_duration desc,
        locked_item
);
comment on view pg_lock_monitor is 'Current locks in database';


-- Killing idle transactions

create or replace procedure terminate_idle()
language sql
as
$$
    select pg_terminate_backend(pid)
    from pg_catalog.pg_stat_activity
    where "state" like 'idle%';
$$;
comment on procedure terminate_idle() is 'pg_terminate_backend for idle transactions';


-- Objesct sizes

select
    relnamespace::regnamespace::text,
    relname,
    pg_size_pretty(pg_total_relation_size("oid")) as relation_size,
    pg_size_pretty(sum(pg_total_relation_size("oid")) over(partition by relnamespace)) as schema_size,
    pg_size_pretty(pg_database_size('raw_1')) as database_size
from pg_catalog.pg_class
where
    relkind in ('r', 'm')
order by
    pg_total_relation_size("oid") desc