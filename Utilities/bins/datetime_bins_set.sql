-- Grouping datetime array's values to calculated bins
    -- Returns input values back (dttm_in/dt_in/tm_in = dttm_out/dt_out/tm_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input values

-- See examples for each function below


-- Timestamp without time zone

create or replace function timestamp_bins
(
    dttm_in timestamp[],
    intvl_type text,
    intvl_shift numeric,
    start_type text default null
)
returns table (dttm_out timestamp, grp timestamp)
language plpgsql
immutable
as
$$

-- Returns unnested input timeseries array back (dttm_in = dttm_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input values

-- Input arguments:

-- dttm_in timestamp[] - input array
-- intvl_type text - interval type, bucket's unit of measurement
-- intvl_shift numeric - bucket width  in units intvl_type: < 0 - past, > 0 - future
-- start_type text default null - level of rounding for starting point; by default = intvl_type
--                                example: 'day' means that bins' intervals calculation will start from 00:00:00 at the same day as input value

-- Example: distributing timestamp values to 5 hours-wide bins rounding to the future and starting from the start of the day (00:00:00)

/*
    with base as
    (
        select s.n as id,
               localtimestamp + s.n * random() * interval '1' day as dttm_in -- input timestamps
        from generate_series(1, 20) s(n)
    )

    select a.id,
           a.dttm_in, -- input timestamps
           t.grp -- grouping bins
    from base a
    cross join (select array_agg(dttm_in) as arr_in from base) b
    join timestamp_bins(b.arr_in, 'h', 5, 'd') t on t.dttm_out = a.dttm_in
    order by t.grp, a.dttm_in;
*/

    declare

        min_dttm timestamp;
        max_dttm timestamp;
        group_intvl interval;
        start_type_ text;
        intvl_types text[] := '{millennium, millenniums, mil, century, centuries, c, decade, decades, dec, year, years, y, month, months, mon, mons, week, weeks, w, day, days, d, hour, hours, h, minute, minutes, min, mins, m, second, seconds, sec, secs, s, millisecond, milliseconds, ms, microsecond, microseconds, us}';

    begin

        -- Checking input interval types
        if lower(intvl_type) != all(intvl_types) then
            raise exception 'Invalid interval type';
        end if;

        select coalesce(start_type, intvl_type)
        into start_type_;

        if format('%s %L', intvl_shift, intvl_type)::interval > format('''1'' %L', start_type_)::interval then
            raise exception 'Invalid logic - rounding step must be less or equal to the level of rounding for starting point (interval ''%'' % > interval ''1'' %)',
                intvl_shift,
                intvl_type,
                start_type_;
        end if;

        -- Setting boundaries
        select date_trunc(coalesce(start_type_, intvl_type), min(t.n)),
               max(t.n)
        into min_dttm,
             max_dttm
        from unnest(dttm_in) t(n);


        -- Calculating bins

            -- Rounding to the past
        if intvl_shift < 0 then

            select format('%s %L', abs(intvl_shift), intvl_type)::interval
            into group_intvl;

            return query
                select distinct
                    a.dttm_val,
                    t.grp
                from unnest(dttm_in) a(dttm_val)
                join generate_series(min_dttm - group_intvl, max_dttm + group_intvl, group_intvl) as t(grp)
                     on a.dttm_val >= t.grp
                     and
                     (
                        age(a.dttm_val, t.grp) < group_intvl
                        or
                        (
                           age(a.dttm_val, t.grp) >= group_intvl
                           and date_trunc(intvl_type, a.dttm_val) - date_trunc(intvl_type, t.grp) > '0'::interval
                           and date_trunc(intvl_type, a.dttm_val) - date_trunc(intvl_type, t.grp) < group_intvl
                        )
                     );

        else

            -- Rounding to the future
            select format('%s %L', intvl_shift, intvl_type)::interval
            into group_intvl;

            return query
                select distinct
                    a.dttm_val,
                    t.grp
                from unnest(dttm_in) a(dttm_val)
                join generate_series(min_dttm - group_intvl, max_dttm + group_intvl, group_intvl) as t(grp)
                     on t.grp >= a.dttm_val
                     and
                     (
                        age(t.grp, a.dttm_val) < group_intvl
                        or
                        (
                           age(t.grp, a.dttm_val) >= group_intvl
                           and date_trunc(intvl_type, t.grp) - date_trunc(intvl_type, a.dttm_val) > '0'::interval
                           and age(date_trunc(intvl_type, t.grp), date_trunc(intvl_type, a.dttm_val)) <= group_intvl
                        )
                     );

        end if;

    end;

$$;

comment on function timestamp_bins(dttm_in timestamp[], intvl_type text, intvl_shift numeric, start_type text)
    is 'in - dttm_in, intvl_type, intvl_shift, start_type; out - dttm_out, grp';


-- Timestamp with time zone

create or replace function timestamptz_bins
(
    dttm_in timestamptz[],
    intvl_type text,
    intvl_shift numeric,
    start_type text default null
)
returns table (dttm_out timestamptz, grp timestamptz)
language plpgsql
immutable
as
$$

-- Returns unnested input timeseries array back (dttm_in = dttm_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input values

-- Input arguments:

-- dttm_in timestamptz[] - input array
-- intvl_type text - interval type, bucket's unit of measurement
-- intvl_shift numeric - bucket width  in units intvl_type: < 0 - past, > 0 - future
-- start_type text default null - level of rounding for starting point; by default = intvl_type
--                                example: 'day' means that bins' intervals calculation will start from 00:00:00 at the same day as input value

-- Example: distributing timestamptz values to 5 hours-wide bins rounding to the future and starting from the start of the day (00:00:00)

/*
    with base as
    (
        select s.n as id,
               current_timestamp + s.n * random() * interval '1' day as dttm_in -- input timestamps
        from generate_series(1, 20) s(n)
    )

    select a.id,
           a.dttm_in, -- input timestamps
           t.grp -- grouping bins
    from base a
    cross join (select array_agg(dttm_in) as arr_in from base) b
    join timestamptz_bins(b.arr_in, 'h', 5, 'd') t on t.dttm_out = a.dttm_in
    order by t.grp, a.dttm_in;
*/

    declare

        min_dttm timestamptz;
        max_dttm timestamptz;
        group_intvl interval;
        start_type_ text;
        intvl_types text[] := '{millennium, millenniums, mil, century, centuries, c, decade, decades, dec, year, years, y, month, months, mon, mons, week, weeks, w, day, days, d, hour, hours, h, minute, minutes, min, mins, m, second, seconds, sec, secs, s, millisecond, milliseconds, ms, microsecond, microseconds, us}';

    begin

        -- Checking input interval types
        if lower(intvl_type) != all(intvl_types) then
            raise exception 'Invalid interval type';
        end if;

        select coalesce(start_type, intvl_type)
        into start_type_;

        if format('%s %L', intvl_shift, intvl_type)::interval > format('''1'' %L', start_type_)::interval then
            raise exception 'Invalid logic - rounding step must be less or equal to the level of rounding for starting point (interval ''%'' % > interval ''1'' %)',
                intvl_shift,
                intvl_type,
                start_type_;
        end if;

        -- Setting boundaries
        select date_trunc(coalesce(start_type_, intvl_type), min(t.n)),
               max(t.n)
        into min_dttm,
             max_dttm
        from unnest(dttm_in) t(n);

        -- Calculating bins

            -- Rounding to the past
        if intvl_shift < 0 then

            select format('%s %L', abs(intvl_shift), intvl_type)::interval
            into group_intvl;

            return query
                select distinct
                    a.dttm_val,
                    t.grp
                from unnest(dttm_in) a(dttm_val)
                join generate_series(min_dttm - group_intvl, max_dttm + group_intvl, group_intvl) as t(grp)
                     on a.dttm_val >= t.grp
                     and
                     (
                        age(a.dttm_val, t.grp) < group_intvl
                        or
                        (
                           age(a.dttm_val, t.grp) >= group_intvl
                           and date_trunc(intvl_type, a.dttm_val) - date_trunc(intvl_type, t.grp) > '0'::interval
                           and date_trunc(intvl_type, a.dttm_val) - date_trunc(intvl_type, t.grp) < group_intvl
                        )
                     );

        else

            -- Rounding to the future
            select format('%s %L', intvl_shift, intvl_type)::interval
            into group_intvl;

            return query
                select distinct
                    a.dttm_val,
                    t.grp
                from unnest(dttm_in) a(dttm_val)
                join generate_series(min_dttm - group_intvl, max_dttm + group_intvl, group_intvl) as t(grp)
                     on t.grp >= a.dttm_val
                     and
                     (
                        age(t.grp, a.dttm_val) < group_intvl
                        or
                        (
                           age(t.grp, a.dttm_val) >= group_intvl
                           and date_trunc(intvl_type, t.grp) - date_trunc(intvl_type, a.dttm_val) > '0'::interval
                           and age(date_trunc(intvl_type, t.grp), date_trunc(intvl_type, a.dttm_val)) <= group_intvl
                        )
                     );

        end if;

    end;

$$;

comment on function timestamptz_bins(dttm_in timestamptz[], intvl_type text, intvl_shift numeric, start_type text)
    is 'in - dttm_in, intvl_type, intvl_shift, start_type; out - dttm_out, grp';


-- Date

create or replace function date_bins
(
    dt_in date[],
    intvl_type text,
    intvl_shift numeric,
    start_type text default null
)
returns table (dt_out date, grp date)
language plpgsql
immutable
as
$$

-- Returns input values back (dt_in = dt_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input values

-- Input arguments:

-- dt_in date[] - input array
-- intvl_type text - interval type, bucket's unit of measurement
-- intvl_shift numeric - bucket width  in units intvl_type: < 0 - past, > 0 - future
-- start_type text default null - level of rounding for starting point; by default = intvl_type
--                                example: 'month' means that bins' intervals calculation will start from first day at the same month as input value

-- Example: distributing date values to year quarters starting from the start of the year (January, 1)

/*
    with base as
    (
        select s.n as id,
               (current_date + s.n * random() * interval '1' day)::date as dt_in -- input dates
        from generate_series(1, 365) s(n)
    )

    select a.id,
           a.dt_in, -- input dates
           t.grp -- grouping bins
    from base a
    cross join (select array_agg(dt_in) as arr_in from base) b
    join date_bins(b.arr_in, 'month', -3, 'year') t on t.dt_out = a.dt_in
    order by t.grp, a.dt_in;
*/

    declare

        min_dt date;
        max_dt date;
        group_intvl interval;
        start_type_ text;
        intvl_types text[] := '{millennium, millenniums, mil, century, centuries, c, decade, decades, dec, year, years, y, month, months, mon, mons, week, weeks, w, day, days, d}';

    begin

        -- Checking input interval types
        if lower(intvl_type) != all(intvl_types) then
            raise exception 'Invalid interval type';
        end if;

        select coalesce(start_type, intvl_type)
        into start_type_;

        if format('%s %L', intvl_shift, intvl_type)::interval > format('''1'' %L', start_type_)::interval then
            raise exception 'Invalid logic - rounding step must be less or equal to the level of rounding for starting point (interval ''%'' % > interval ''1'' %)',
                intvl_shift,
                intvl_type,
                start_type_;
        end if;

        -- Setting boundaries
        select date_trunc(coalesce(start_type_, intvl_type), min(t.n)),
               max(t.n)
        into min_dt,
             max_dt
        from unnest(dt_in) t(n);

        -- Calculating bins

            -- Rounding to the past
        if intvl_shift < 0 then

            select format('%s %L', abs(intvl_shift), intvl_type)::interval
            into group_intvl;

            return query
                select distinct
                    a.dt_val,
                    t.grp::date
                from unnest(dt_in) a(dt_val)
                join generate_series(min_dt - group_intvl, max_dt + group_intvl, group_intvl) as t(grp)
                     on a.dt_val >= t.grp::date
                     and
                     (
                        age(a.dt_val, t.grp::date) < group_intvl
                        or
                        (
                           age(a.dt_val, t.grp::date) >= group_intvl
                           and date_trunc(intvl_type, a.dt_val) - date_trunc(intvl_type, t.grp::date) > '0'::interval
                           and date_trunc(intvl_type, a.dt_val) - date_trunc(intvl_type, t.grp::date) < group_intvl
                        )
                     );

        else

            -- Rounding to the future
            select format('%s %L', intvl_shift, intvl_type)::interval
            into group_intvl;

            return query
                select distinct
                    a.dt_val,
                    t.grp::date
                from unnest(dt_in) a(dt_val)
                join generate_series(min_dt - group_intvl, max_dt + group_intvl, group_intvl) as t(grp)
                     on t.grp::date >= a.dt_val
                     and
                     (
                        age(t.grp::date, a.dt_val) < group_intvl
                        or
                        (
                           age(t.grp::date, a.dt_val) = group_intvl
                           and a.dt_val + group_intvl != t.grp::date
                        )
                     );

        end if;

    end;

$$;

comment on function date_bins(dt_in date[], intvl_type text, intvl_shift numeric, start_type text)
    is 'in - dt_in, intvl_type, intvl_shift, start_type; out - dt_out, grp';


-- Time without time zone

create or replace function time_bins
(
    tm_in time[],
    intvl_type text,
    intvl_shift numeric
)
returns table (tm_out time, grp time)
language plpgsql
immutable
as
$$

-- Returns input values back (tm_in = tm_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input values

-- Input arguments:

-- tm_in time[] - input array
-- intvl_type text - interval type, bucket's unit of measurement
-- intvl_shift numeric - bucket width  in units intvl_type: < 0 - past, > 0 - future

-- Calculation range is always the same - from 00:00:00.000000 to 23:59:59.999999

-- Example: distributing time values to 5 hours-wide bins rounding to the future

/*
    with base as
    (
        select s.n as id,
               (localtimestamp::time(6) + s.n * random() * interval '24' hour)::time(6) as tm_in -- input time values
        from generate_series(1, 20) s(n)
    )

    select a.id,
           a.tm_in, -- input time values
           t.grp -- grouping bins
    from base a
    cross join (select array_agg(tm_in) as arr_in from base) b
    join time_bins(b.arr_in, 'h', 5) t on t.tm_out = a.tm_in
    order by t.grp, a.tm_in;
*/

    declare

        group_intvl interval;
        intvl_types text[] := '{hour, hours, h, minute, minutes, min, mins, m, second, seconds, sec, secs, s, millisecond, milliseconds, ms, microsecond, microseconds, us}';

    begin

        -- Checking input interval type

        if lower(intvl_type) != all(intvl_types) then
            raise exception 'Invalid interval type';
        end if;

        -- Calculating bins

            -- Rounding to the past
        if intvl_shift < 0 then

            select format('%s %L', abs(intvl_shift), intvl_type)::interval
            into group_intvl;

            return query
                select distinct
                    a.tm_val,
                    t.grp
                from unnest(tm_in) a(tm_val)
                join (
                        select grp_::time as grp
                        from generate_series(
                                             to_timestamp('01.01.2000 00:00:00.000000', 'dd.mm.yyyy hh24:mi:ss.us'),
                                             to_timestamp('01.01.2000 23:59:59.999999', 'dd.mm.yyyy hh24:mi:ss.us'),
                                             group_intvl
                                            ) as g(grp_)
                        union
                        select '23:59:59.999999'::time as grp
                     ) t
                     on a.tm_val - t.grp < group_intvl
                        and a.tm_val >= t.grp;

        else

            -- Rounding to the future
            select format('%s %L', intvl_shift, intvl_type)::interval
            into group_intvl;

            return query
                select distinct
                    tm_val,
                    grp_
                from
                (
                    select a.tm_val,
                           t.grp_,
                           row_number() over(partition by a.tm_val order by t.grp_) as rn
                    from unnest(tm_in) a(tm_val)
                    join (
                            select grp_::time as grp_
                            from generate_series(
                                                 to_timestamp('01.01.2000 00:00:00.000000', 'dd.mm.yyyy hh24:mi:ss.us'),
                                                 to_timestamp('01.01.2000 23:59:59.999999', 'dd.mm.yyyy hh24:mi:ss.us'),
                                                 group_intvl
                                                ) as g(grp_)
                            union
                            select '23:59:59.999999'::time as grp
                         ) t
                         on t.grp_ - a.tm_val < group_intvl
                            and t.grp_ >= a.tm_val
                ) f
                where rn = 1;

        end if;

    end;

$$;

comment on function time_bins(tm_in time[], intvl_type text, intvl_shift numeric)
    is 'in - tm_in, intvl_type, intvl_shift; out - tm_out, grp';


-- Time with time zone

create or replace function timetz_bins
(
    tm_in timetz[],
    intvl_type text,
    intvl_shift numeric
)
returns table (tm_out timetz, grp timetz)
language plpgsql
immutable
as
$$

-- Returns input values back (tm_in = tm_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input values

-- Input arguments:

-- tm_in timetz[] - input array
-- intvl_type text - interval type, bucket's unit of measurement
-- intvl_shift numeric - bucket width  in units intvl_type: < 0 - past, > 0 - future

-- Calculation range is always the same - from 00:00:00.000000 to 23:59:59.999999

-- Example: distributing time values to 5 hours-wide bins rounding to the future

/*
    with base as
    (
        select s.n as id,
               (current_timestamp::timetz(6) + s.n * random() * interval '24' hour)::timetz(6) as tm_in -- input time values
        from generate_series(1, 20) s(n)
    )

    select a.id,
           a.tm_in, -- input time values
           t.grp -- grouping bins
    from base a
    cross join (select array_agg(tm_in) as arr_in from base) b
    join timetz_bins(b.arr_in, 'h', 5) t on t.tm_out = a.tm_in
    order by t.grp, a.tm_in;
*/

    declare

        group_intvl interval;
        intvl_types text[] := '{hour, hours, h, minute, minutes, min, mins, m, second, seconds, sec, secs, s, millisecond, milliseconds, ms, microsecond, microseconds, us}';

    begin

        -- Checking input interval type

        if lower(intvl_type) != all(intvl_types) then
            raise exception 'Invalid interval type';
        end if;

        -- Calculating bins

            -- Rounding to the past
        if intvl_shift < 0 then

            select format('%s %L', abs(intvl_shift), intvl_type)::interval
            into group_intvl;

            return query
                select distinct
                    a.tm_val,
                    t.grp::timetz
                from unnest(tm_in) a(tm_val)
                join (
                        select grp_::time as grp
                        from generate_series(
                                             to_timestamp('01.01.2000 00:00:00.000000', 'dd.mm.yyyy hh24:mi:ss.us'),
                                             to_timestamp('01.01.2000 23:59:59.999999', 'dd.mm.yyyy hh24:mi:ss.us'),
                                             group_intvl
                                            ) as g(grp_)
                        union
                        select '23:59:59.999999'::time as grp
                     ) t
                     on a.tm_val::time - t.grp < group_intvl
                        and a.tm_val::time >= t.grp;

        else

            -- Rounding to the future
            select format('%s %L', intvl_shift, intvl_type)::interval
            into group_intvl;

            return query
                select distinct
                    tm_val,
                    grp_::timetz
                from
                (
                    select a.tm_val,
                           t.grp_,
                           row_number() over(partition by a.tm_val order by t.grp_) as rn
                    from unnest(tm_in) a(tm_val)
                    join (
                            select grp_::time as grp_
                            from generate_series(
                                                 to_timestamp('01.01.2000 00:00:00.000000', 'dd.mm.yyyy hh24:mi:ss.us'),
                                                 to_timestamp('01.01.2000 23:59:59.999999', 'dd.mm.yyyy hh24:mi:ss.us'),
                                                 group_intvl
                                                ) as g(grp_)
                            union
                            select '23:59:59.999999'::time as grp
                         ) t
                         on t.grp_ - a.tm_val::time < group_intvl
                            and t.grp_ >= a.tm_val::time
                ) f
                where rn = 1;

        end if;

    end;

$$;

comment on function timetz_bins(tm_in timetz[], intvl_type text, intvl_shift numeric)
    is 'in - tm_in, intvl_type, intvl_shift; out - tm_out, grp';