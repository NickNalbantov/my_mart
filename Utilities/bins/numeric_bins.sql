-- Grouping numeric values to calculated bins
    -- Returns input values back (num_in = num_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input value

-- See examples for each function below


-- Numeric

create or replace function numeric_bins
(
    num_in anycompatiblenonarray,
    num_shift anycompatiblenonarray,
    start_num anycompatiblenonarray default 0
)
returns table (num_out numeric, grp numeric)
language plpgsql
immutable
strict
as
$$

-- Returns input values back (num_in = num_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input value

-- Input arguments:

-- num_in anycompatiblenonarray - input value
-- num_shift anycompatiblenonarray - bucket width
-- start_num anycompatiblenonarray default 0 - starting point (beginning of the first bucket)

-- Example: distributing numeric values from -100 to 100 into 10-wide buckets with rounding down

/*
        with base as
        (
            select s.n as id,
                   round(s.n * random())::numeric as num_in -- input values
            from generate_series(-100::numeric, 100::numeric) s(n)
        )

        select a.id,
               a.num_in, -- input values
               t.grp -- grouping bins
        from base a
        join numeric_bins(a.num_in, -10, -100) t on t.num_out = a.num_in
        order by t.grp, a.num_in;
*/

    begin

        -- Checking inputs
        if start_num > num_in then
            raise exception 'Invalid logic - starting point can''t be bigger than input value (% > %)',
                start_num,
                num_in;
        end if;

        -- Calculating bins

            -- Rounding down
        if num_shift < 0 then

            return query
                select num_in::numeric,
                       t.grp
                from generate_series(start_num::numeric, (num_in + abs(num_shift))::numeric, abs(num_shift)::numeric) as t(grp)
                where num_in >= t.grp
                      and num_in - t.grp < abs(num_shift);

        else

            -- Rounding up
            return query
                select num_in::numeric,
                       t.grp
                from generate_series(start_num::numeric, (num_in + num_shift)::numeric, num_shift::numeric) as t(grp)
                where t.grp >= num_in
                      and abs(t.grp - num_in) < num_shift;

        end if;

    end;

$$;

comment on function numeric_bins(num_in anycompatiblenonarray, num_shift anycompatiblenonarray, start_num anycompatiblenonarray)
    is 'in - num_in, num_shift, start_num; out - num_out, grp';


-- Bigint

create or replace function int8_bins
(
    num_in anycompatiblenonarray,
    num_shift anycompatiblenonarray,
    start_num anycompatiblenonarray default 0
)
returns table (num_out bigint, grp bigint)
language plpgsql
immutable
strict
as
$$

-- Returns input values back (num_in = num_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input value

-- Input arguments:

-- num_in anycompatiblenonarray - input value
-- num_shift anycompatiblenonarray - bucket width
-- start_num anycompatiblenonarray default 0 - starting point (beginning of the first bucket)

-- Example: distributing int8 values from -100 to 100 into 10-wide buckets with rounding down

/*
        with base as
        (
            select s.n as id,
                   round(s.n * random())::bigint as num_in -- input values
            from generate_series(-100::bigint, 100::bigint) s(n)
        )

        select a.id,
               a.num_in, -- input values
               t.grp -- grouping bins
        from base a
        join int8_bins(a.num_in, -10, -100) t on t.num_out = a.num_in
        order by t.grp, a.num_in;
*/

    declare

        max_num bigint;

    begin

        -- Checking inputs
        if start_num > num_in then
            raise exception 'Invalid logic - starting point can''t be bigger than input value (% > %)',
                start_num,
                num_in;
        end if;

        if num_in not between -9223372036854775807 and 9223372036854775807 or num_in - round(num_in) != 0 then
            raise exception 'Argument num_in must be an integer from the range of int8 type';
        end if;

        if num_shift not between -9223372036854775807 and 9223372036854775807 or num_shift - round(num_shift) != 0 then
            raise exception 'Argument num_shift must be an integer from the range of int8 type';
        end if;

        if start_num not between -9223372036854775807 and 9223372036854775807 or start_num - round(start_num) != 0 then
            raise exception 'Argument start_num must be an integer from the range of int8 type';
        end if;

        -- Setting boundaries
        max_num := (case when num_in + abs(num_shift) > 9223372036854775807 then 9223372036854775807
                         when num_in + abs(num_shift) < -9223372036854775807 then -9223372036854775807
                    else num_in + abs(num_shift) end)::bigint;

        -- Calculating bins

            -- Rounding down
        if num_shift < 0 then

            return query
                select num_in::bigint,
                       t.grp
                from generate_series(start_num::bigint, max_num, abs(num_shift)::bigint) as t(grp)
                where num_in >= t.grp
                      and num_in - t.grp < abs(num_shift);

        else

            -- Rounding up
            return query
                select num_in::bigint,
                       t.grp
                from generate_series(start_num::bigint, max_num, num_shift::bigint) as t(grp)
                where t.grp >= num_in
                      and abs(t.grp - num_in) < num_shift;

        end if;

    end;

$$;

comment on function int8_bins(num_in anycompatiblenonarray, num_shift anycompatiblenonarray, start_num anycompatiblenonarray)
    is 'in - num_in, num_shift, start_num; out - num_out, grp';


-- Int

create or replace function int_bins
(
    num_in anycompatiblenonarray,
    num_shift anycompatiblenonarray,
    start_num anycompatiblenonarray default 0
)
returns table (num_out int, grp int)
language plpgsql
immutable
strict
as
$$

-- Returns input values back (num_in = num_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input value

-- Input arguments:

-- num_in anycompatiblenonarray - input value
-- num_shift anycompatiblenonarray - bucket width
-- start_num anycompatiblenonarray default 0 - starting point (beginning of the first bucket)

-- Example: distributing int4 values from -100 to 100 into 10-wide buckets with rounding down

/*
        with base as
        (
            select s.n as id,
                   round(s.n * random())::int as num_in -- input values
            from generate_series(-100::int, 100::int) s(n)
        )

        select a.id,
               a.num_in, -- input values
               t.grp -- grouping bins
        from base a
        join int_bins(a.num_in, -10, -100) t on t.num_out = a.num_in
        order by t.grp, a.num_in;
*/

    declare

        max_num int;

    begin

        -- Checking inputs

        if start_num > num_in then
            raise exception 'Invalid logic - starting point can''t be bigger than input value (% > %)',
                start_num,
                num_in;
        end if;

        if num_in not between -2147483647 and 2147483647 or num_in - round(num_in) != 0 then
            raise exception 'Argument num_in must be an integer from the range of int4 type';
        end if;

        if num_shift not between -2147483647 and 2147483647 or num_shift - round(num_shift) != 0 then
            raise exception 'Argument num_shift must be an integer from the range of int4 type';
        end if;

        if start_num not between -2147483647 and 2147483647 or start_num - round(start_num) != 0 then
            raise exception 'Argument start_num must be an integer from the range of int4 type';
        end if;

        -- Setting boundaries
        max_num := (case when num_in + abs(num_shift) > 2147483647 then 2147483647
                         when num_in + abs(num_shift) < -2147483647 then -2147483647
                    else num_in + abs(num_shift) end)::int;

        -- Calculating bins

            -- Rounding down
        if num_shift < 0 then

            return query
                select num_in::int,
                       t.grp
                from generate_series(start_num::int, max_num, abs(num_shift)::int) as t(grp)
                where num_in >= t.grp
                      and num_in - t.grp < abs(num_shift);

        else

            -- Rounding up
            return query
                select num_in::int,
                       t.grp
                from generate_series(start_num::int, max_num, num_shift::int) as t(grp)
                where t.grp >= num_in
                      and abs(t.grp - num_in) < num_shift;

        end if;

    end;

$$;

comment on function int_bins(num_in anycompatiblenonarray, num_shift anycompatiblenonarray, start_num anycompatiblenonarray)
    is 'in - num_in, num_shift, start_num; out - num_out, grp';


-- Smallint

create or replace function int2_bins
(
    num_in anycompatiblenonarray,
    num_shift anycompatiblenonarray,
    start_num anycompatiblenonarray default 0
)
returns table (num_out smallint, grp smallint)
language plpgsql
immutable
strict
as
$$

-- Returns input values back (num_in = num_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input value

-- Input arguments:

-- num_in anycompatiblenonarray - input value
-- num_shift anycompatiblenonarray - bucket width
-- start_num anycompatiblenonarray default 0 - starting point (beginning of the first bucket)

-- Example: distributing int2 values from -100 to 100 into 10-wide buckets with rounding down

/*
        with base as
        (
            select s.n as id,
                   round(s.n * random())::smallint as num_in -- input values
            from generate_series(-100::smallint, 100::smallint) s(n)
        )

        select a.id,
               a.num_in, -- input values
               t.grp -- grouping bins
        from base a
        join int2_bins(a.num_in, -10, -100) t on t.num_out = a.num_in
        order by t.grp, a.num_in;
*/

    declare

        max_num int;

    begin

        -- Checking inputs
        if start_num > num_in then
            raise exception 'Invalid logic - starting point can''t be bigger than input value (% > %)',
                start_num,
                num_in;
        end if;

        if num_in not between -32767 and 32767 or num_in - round(num_in) != 0 then
            raise exception 'Argument num_in must be an integer from the range of int2 type';
        end if;

        if num_shift not between -32767 and 32767 or num_shift - round(num_shift) != 0 then
            raise exception 'Argument num_shift must be an integer from the range of int2 type';
        end if;

        if start_num not between -32767 and 32767 or start_num - round(start_num) != 0 then
            raise exception 'Argument start_num must be an integer from the range of int2 type';
        end if;

        -- Setting boundaries
        max_num := (case when num_in + abs(num_shift) > 32767 then 32767
                         when num_in + abs(num_shift) < -32767 then -32767
                    else num_in + abs(num_shift) end)::int;

        -- Calculating bins

            -- Rounding down
        if num_shift < 0 then

            return query
                select num_in::smallint,
                       t.grp::smallint
                from generate_series(start_num::int, max_num, abs(num_shift)::int) as t(grp)
                where num_in >= t.grp
                      and num_in - t.grp < abs(num_shift);

        else

            -- Rounding up
            return query
                select num_in::smallint,
                       t.grp::smallint
                from generate_series(start_num::int, max_num, num_shift::int) as t(grp)
                where t.grp >= num_in
                      and abs(t.grp - num_in) < num_shift;

        end if;

    end;

$$;

comment on function int2_bins(num_in anycompatiblenonarray, num_shift anycompatiblenonarray, start_num anycompatiblenonarray)
    is 'in - num_in, num_shift, start_num; out - num_out, grp';