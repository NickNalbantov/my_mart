-- Grouping numeric array's values to calculated bins
    -- Returns input values back (num_in = num_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input value

-- See examples for each function below


-- Numeric

create or replace function numeric_bins
(
    num_in anycompatiblearray,
    num_shift anycompatiblenonarray,
    start_num anycompatiblenonarray default 0
)
returns table (num_out numeric, grp numeric)
language plpgsql
immutable
strict
as
$$

-- Returns unnested input values back (num_in = num_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input values

-- Input arguments:

-- num_in anycompatiblearray - input values
-- num_shift anycompatiblenonarray - bucket width
-- start_num anycompatiblenonarray default 0 - starting point (beginning of the first bucket)

-- Example: distributing numeric values from -100 to 100 into 10-wide buckets with rounding down

/*
        with base as
        (
            select s.n as id,
                   round(s.n * random())::numeric as num_in -- input values
            from generate_series(-100::numeric, 100::numeric) s(n)
        )

        select a.id,
               a.num_in, -- input values
               t.grp -- grouping bins
        from base a
        cross join (select array_agg(num_in) as arr_in from base) b
        join numeric_bins(b.arr_in, -10, -100) t on t.num_out = a.num_in
        order by t.grp, a.num_in;
*/

    declare
        min_num numeric;
        max_num numeric;

    begin

        -- Setting boundaries
        select min(t.n),
               (max(t.n) + abs(num_shift))::numeric
        into min_num,
             max_num
        from unnest(num_in) t(n);

        -- Checking inputs
        if start_num > min_num then
            raise exception 'Invalid logic - starting point can''t be bigger than the least value in the input array (% > %)', start_num, min_num;
        end if;

        -- Calculating bins

            -- Rounding down
        if num_shift < 0 then

            return query
                select distinct
                    a.num_val::numeric,
                    t.grp
                from unnest(num_in) a(num_val)
                join generate_series(start_num::numeric, max_num, abs(num_shift)::numeric) as t(grp)
                     on a.num_val >= t.grp
                        and a.num_val - t.grp < abs(num_shift);

        else

            -- Rounding up
            return query
                select distinct
                    a.num_val::numeric,
                    t.grp
                from unnest(num_in) a(num_val)
                join generate_series(start_num::numeric, max_num, num_shift::numeric) as t(grp)
                     on t.grp >= a.num_val
                        and abs(t.grp - a.num_val) < num_shift;

        end if;

    end;

$$;

comment on function numeric_bins(num_in anycompatiblearray, num_shift anycompatiblenonarray, start_num anycompatiblenonarray)
    is 'in - num_in, num_shift, start_num; out - num_out, grp';


-- Bigint

create or replace function int8_bins
(
    num_in anycompatiblearray,
    num_shift anycompatiblenonarray,
    start_num anycompatiblenonarray default 0
)
returns table (num_out bigint, grp bigint)
language plpgsql
immutable
strict
as
$$

-- Returns unnested input values back (num_in = num_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input values

-- Input arguments:

-- num_in anycompatiblearray - input values
-- num_shift anycompatiblenonarray - bucket width
-- start_num anycompatiblenonarray default 0 - starting point (beginning of the first bucket)

-- Example: distributing int8 values from -100 to 100 into 10-wide buckets with rounding down

/*
        with base as
        (
            select s.n as id,
                   round(s.n * random())::bigint as num_in -- input values
            from generate_series(-100::bigint, 100::bigint) s(n)
        )

        select a.id,
               a.num_in, -- input values
               t.grp -- grouping bins
        from base a
        cross join (select array_agg(num_in) as arr_in from base) b
        join int8_bins(b.arr_in, -10, -100) t on t.num_out = a.num_in
        order by t.grp, a.num_in;
*/

    declare

        min_num bigint;
        max_num bigint;
        fraction_cnt bigint;
        overflow_cnt bigint;

    begin

        -- Setting boundaries
        select (case when min(t.n) > 9223372036854775807 then 9223372036854775807
                     when min(t.n) < -9223372036854775807 then -9223372036854775807
                else min(t.n) end)::bigint,
               (case when max(t.n) + abs(num_shift) > 9223372036854775807 then 9223372036854775807
                     when max(t.n) + abs(num_shift) < -9223372036854775807 then -9223372036854775807
                else max(t.n) + abs(num_shift) end)::bigint,
               count(*) filter(where t.n - round(t.n) != 0),
               count(*) filter(where abs(t.n) > 9223372036854775807)
        into min_num,
             max_num,
             fraction_cnt,
             overflow_cnt
        from unnest(num_in) t(n);

        -- Checking inputs
        if start_num > min_num then
            raise exception 'Invalid logic - starting point can''t be bigger than the least value in the input array (% > %)', start_num, min_num;
        end if;

        if fraction_cnt !=0 or overflow_cnt !=0 then
            raise exception 'Invalid values in the input array that can''t be cast to bigint';
        end if;

        if num_shift not between -9223372036854775807 and 9223372036854775807 or num_shift - round(num_shift) != 0 then
            raise exception 'Argument num_shift must be an integer from the range of int8 type';
        end if;

        if start_num not between -9223372036854775807 and 9223372036854775807 or start_num - round(start_num) != 0 then
            raise exception 'Argument start_num must be an integer from the range of int8 type';
        end if;

        -- Calculating bins

            -- Rounding down
        if num_shift < 0 then

            return query
                select distinct
                    a.num_val::bigint,
                    t.grp
                from unnest(num_in) a(num_val)
                join generate_series(start_num::bigint, max_num, abs(num_shift)::bigint) as t(grp)
                     on a.num_val >= t.grp
                        and a.num_val - t.grp < abs(num_shift);

        else

            -- Rounding up
            return query
                select distinct
                    a.num_val::bigint,
                    t.grp
                from unnest(num_in) a(num_val)
                join generate_series(start_num::bigint, max_num, num_shift::bigint) as t(grp)
                     on t.grp >= a.num_val
                        and abs(t.grp - a.num_val) < num_shift;

        end if;

    end;

$$;

comment on function int8_bins(num_in anycompatiblearray, num_shift anycompatiblenonarray, start_num anycompatiblenonarray)
    is 'in - num_in, num_shift, start_num; out - num_out, grp';


-- Int

create or replace function int_bins
(
    num_in anycompatiblearray,
    num_shift anycompatiblenonarray,
    start_num anycompatiblenonarray default 0
)
returns table (num_out int, grp int)
language plpgsql
immutable
strict
as
$$

-- Returns unnested input values back (num_in = num_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input values

-- Input arguments:

-- num_in anycompatiblearray - input values
-- num_shift anycompatiblenonarray - bucket width
-- start_num anycompatiblenonarray default 0 - starting point (beginning of the first bucket)

-- Example: distributing int4 values from -100 to 100 into 10-wide buckets with rounding down

/*
        with base as
        (
            select s.n as id,
                   round(s.n * random())::int as num_in -- input values
            from generate_series(-100::int, 100::int) s(n)
        )

        select a.id,
               a.num_in, -- input values
               t.grp -- grouping bins
        from base a
        cross join (select array_agg(num_in) as arr_in from base) b
        join int_bins(b.arr_in, -10, -100) t on t.num_out = a.num_in
        order by t.grp, a.num_in;
*/

    declare

        min_num int;
        max_num int;
        fraction_cnt bigint;
        overflow_cnt bigint;

    begin

        -- Setting boundaries
        select (case when min(t.n) > 2147483647 then 2147483647
                     when min(t.n) < -2147483647 then -2147483647
                else min(t.n) end)::int,
               (case when max(t.n) + abs(num_shift) > 2147483647 then 2147483647
                     when max(t.n) + abs(num_shift) < -2147483647 then -2147483647
                else max(t.n) + abs(num_shift) end)::int,
               count(*) filter(where t.n - round(t.n) != 0),
               count(*) filter(where abs(t.n) > 2147483647)
        into min_num,
             max_num,
             fraction_cnt,
             overflow_cnt
        from unnest(num_in) t(n);

        -- Checking inputs
        if start_num > min_num then
            raise exception 'Invalid logic - starting point can''t be bigger than the least value in the input array (% > %)', start_num, min_num;
        end if;

        if fraction_cnt !=0 or overflow_cnt !=0 then
            raise exception 'Invalid values in the input array that can''t be cast to int';
        end if;

        if num_shift not between -2147483647 and 2147483647 or num_shift - round(num_shift) != 0 then
            raise exception 'Argument num_shift must be an integer from the range of int4 type';
        end if;

        if start_num not between -2147483647 and 2147483647 or start_num - round(start_num) != 0 then
            raise exception 'Argument start_num must be an integer from the range of int4 type';
        end if;

        -- Calculating bins

            -- Rounding down
        if num_shift < 0 then

            return query
                select distinct
                    a.num_val::int,
                    t.grp
                from unnest(num_in) a(num_val)
                join generate_series(start_num::int, max_num, abs(num_shift)::int) as t(grp)
                     on a.num_val >= t.grp
                        and a.num_val - t.grp < abs(num_shift);

        else

            -- Rounding up
            return query
                select distinct
                    a.num_val::int,
                    t.grp
                from unnest(num_in) a(num_val)
                join generate_series(start_num::int, max_num, num_shift::int) as t(grp)
                     on t.grp >= a.num_val
                        and abs(t.grp - a.num_val) < num_shift;

        end if;

    end;

$$;

comment on function int_bins(num_in anycompatiblearray, num_shift anycompatiblenonarray, start_num anycompatiblenonarray)
    is 'in - num_in, num_shift, start_num; out - num_out, grp';


-- Smallint

create or replace function int2_bins
(
    num_in anycompatiblearray,
    num_shift anycompatiblenonarray,
    start_num anycompatiblenonarray default 0
)
returns table (num_out smallint, grp smallint)
language plpgsql
immutable
strict
as
$$

-- Returns unnested input values back (num_in = num_out) and column with corresponding grouping bin (grp)
    -- This result can be joined back to original table by input values

-- Input arguments:

-- num_in anycompatiblearray - input values
-- num_shift anycompatiblenonarray - bucket width
-- start_num anycompatiblenonarray default 0 - starting point (beginning of the first bucket)

-- Example: distributing int2 values from -100 to 100 into 10-wide buckets with rounding down

/*
        with base as
        (
            select s.n as id,
                   round(s.n * random())::smallint as num_in -- input values
            from generate_series(-100::smallint, 100::smallint) s(n)
        )

        select a.id,
               a.num_in, -- input values
               t.grp -- grouping bins
        from base a
        cross join (select array_agg(num_in) as arr_in from base) b
        join int2_bins(b.arr_in, -10, -100) t on t.num_out = a.num_in
        order by t.grp, a.num_in;
*/

    declare

        min_num int;
        max_num int;
        fraction_cnt bigint;
        overflow_cnt bigint;

    begin

        -- Setting boundaries
        select (case when min(t.n) > 32767 then 32767
                     when min(t.n) < -32767 then -32767
                else min(t.n) end)::int,
               (case when max(t.n) + abs(num_shift) > 32767 then 32767
                     when max(t.n) + abs(num_shift) < -32767 then -32767
                else max(t.n) + abs(num_shift) end)::int,
               count(*) filter(where t.n - round(t.n) != 0),
               count(*) filter(where abs(t.n) > 32767)
        into min_num,
             max_num,
             fraction_cnt,
             overflow_cnt
        from unnest(num_in) t(n);

        -- Checking inputs
        if start_num > min_num then
            raise exception 'Invalid logic - starting point can''t be bigger than the least value in the input array (% > %)', start_num, min_num;
        end if;

        if fraction_cnt !=0 or overflow_cnt !=0 then
            raise exception 'Invalid values in the input array that can''t be cast to int';
        end if;

        if num_shift not between -32767 and 32767 or num_shift - round(num_shift) != 0 then
            raise exception 'Argument num_shift must be an integer from the range of int2 type';
        end if;

        if start_num not between -32767 and 32767 or start_num - round(start_num) != 0 then
            raise exception 'Argument start_num must be an integer from the range of int2 type';
        end if;

        -- Calculating bins

            -- Rounding down
        if num_shift < 0 then

            return query
                select distinct
                    a.num_val::smallint,
                    t.grp::smallint
                from unnest(num_in) a(num_val)
                join generate_series(start_num::int, max_num, abs(num_shift)::int) as t(grp)
                     on a.num_val >= t.grp
                        and a.num_val - t.grp < abs(num_shift);

        else

            -- Rounding up
            return query
                select distinct
                    a.num_val::smallint,
                    t.grp::smallint
                from unnest(num_in) a(num_val)
                join generate_series(start_num::int, max_num, num_shift::int) as t(grp)
                     on t.grp >= a.num_val
                        and abs(t.grp - a.num_val) < num_shift;

        end if;

    end;

$$;

comment on function int2_bins(num_in anycompatiblearray, num_shift anycompatiblenonarray, start_num anycompatiblenonarray)
    is 'in - num_in, num_shift, start_num; out - num_out, grp';