-- Integer

-- Examples:

-- select random_int(), -- random int
--           random_int(100, -100), -- random int from -100 to 100 (arguments could be in any order)
--           random_int_intvl(), -- random int
--           random_int_intvl(300, 500, true) -- random int from from 300 - 500 to 300 + 500
-- from generate_series(1, 10);


-- Random value between two ints

create or replace function random_int
    (
        base anycompatiblenonarray default -2147483648, -- starting point
        fin anycompatiblenonarray default 2147483647 -- ending point
    )
returns int
language plpgsql
strict
as
$func$

-- Examples:

-- select random_int(), -- random int
--           random_int(100, -100) -- random int from -100 to 100 (arguments could be in any order)
-- from generate_series(1, 10);

    declare

        diff numeric;
        shift_direction text;
        res int;

    begin

        -- Checking starting point
        if base not between -2147483648 and 2147483647
           or fin not between -2147483648 and 2147483647
        then

            raise warning 'Starting and ending points must be inside of int4 type''s range of values';
            return null;

        end if;

        -- Diff between start and end
        select fin::numeric - base::numeric
        into strict diff;

        -- Direction
        select case when diff < 0 then '-' else '+' end
        into strict shift_direction;

        -- Calculating
        execute
            format(
                    $fmt$
                            select round(%L::numeric %s abs(%L) * random())::int
                    $fmt$,
                    base, shift_direction, diff
                  )
            into res;

        return res;

        -- Check if result is out of type's range
        exception
            when numeric_value_out_of_range then
                return base;

    end;

$func$;

comment on function random_int(base anycompatiblenonarray, fin anycompatiblenonarray)
    is 'base (def -2147483648), fin (def 2147483647)';


-- Random value from int +/- numeric interval

create or replace function random_int_intvl
    (
        base anycompatiblenonarray default -2147483648, -- starting point
        shift anycompatiblenonarray default 4294967295, -- max shift from a starting point
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns int
language plpgsql
as
$func$

-- Examples:

-- select random_int_intvl(), -- random int
--           random_int_intvl(300, 500, true) -- random int from from 300 - 500 to 300 + 500
-- from generate_series(1, 10);

    declare
        shift_direction text := '+';
        direction_types text[2] := '{-, +}';
        res int;

    begin

        -- Checking starting point
        if base not between -2147483648 and 2147483647 then
            raise warning 'Starting point must be inside of int4 type''s range of values';
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select round(%L %s %L * random())::int
                    $fmt$,
                    base, shift_direction, shift
                  )
            into res;

        return res;

        -- Check if result is out of type's range
        exception
            when numeric_value_out_of_range then
                return base;

    end;

$func$;

comment on function random_int_intvl(base anycompatiblenonarray, shift anycompatiblenonarray, twoway bool)
    is 'base (def -2147483648), shift (def 4294967295), twoway (def false)';