-- Timestamp without time zone

-- Examples:

-- select random_timestamp(), -- random ts from localtimestamp to localtimestamp + 1 year
--        random_timestamp('01.01.2025'::timestamp, '01.01.2020'::timestamp), -- random ts between '01.01.2025' and '01.01.2020' (arguments could be in any order)
--        random_timestamp_intvl('01.01.2025'::timestamp, 'week', 5, true), -- random ts in range '01.01.2025' +/- 5 weeks
--        random_timestamp_text_intvl('03-02-2025 14:21:13.546392', 'mm-dd-yyyy hh24:mi:ss.us', 'hour', -1) -- random ts from specific text-based timestamp to this timestamp - 1 hour
-- from generate_series(1, 10);


-- Random value between two timestamps


create or replace function random_timestamp
    (
        base timestamp(6) default localtimestamp, -- starting point
        fin timestamp(6) default localtimestamp + '1 year'::interval -- ending point
    )
returns timestamp(6)
language plpgsql
strict
as
$func$

-- Could return out-of-bounds results in some corner cases for timestamps before 01.01.1970 because of epoch extraction
-- Better use random_timestamp_intvl or random_timestamp_text_intvl functions in this case

-- Examples:

-- select random_timestamp(), -- random ts from localtimestamp to localtimestamp + 1 year
--        random_timestamp('01.01.2025'::timestamp, '01.01.2020'::timestamp) -- random ts between '01.01.2025' and '01.01.2020' (arguments could be in any order)
-- from generate_series(1, 10);

    declare

        diff numeric;
        shift_direction text;
        res timestamp(6);

    begin

        -- Diff between start and end in microseconds
        select extract(epoch from (fin - base)) * 1000000
        into strict diff;

        -- Direction
        select case when diff < 0 then '-' else '+' end
        into strict shift_direction;

        -- Calculating
        execute
            format(
                    $fmt$
                            select %L::timestamp(6) %s abs(%L) * random() * interval '1 us'
                    $fmt$,
                    base, shift_direction, diff
                  )
            into strict res;

        return res;

        -- Check if result is out of type's range
        exception
            when datetime_field_overflow then
                return base;

    end;

$func$;

comment on function random_timestamp(base timestamp(6), fin timestamp(6))
    is 'base (def localtimestamp), fin (def localtimestamp + 1 year)';


-- Random value from timestamp +/- interval

create or replace function random_timestamp_intvl
    (
        base timestamp(6) default localtimestamp, -- starting point
        interval_type text default 'year', -- interval's unit of measurement
        shift anycompatiblenonarray default 1, -- max shift from a starting point (in interval's unit of measurement)
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns timestamp(6)
language plpgsql
strict
as
$func$

-- Example:

-- select random_timestamp_intvl('01.01.2025'::timestamp, 'week', 5, true) -- random ts in range '01.01.2025' +/- 5 weeks
-- from generate_series(1, 10);

    declare

        shift_direction text := '+';
        int_types text[] := '{millennium, millenniums, mil, century, centuries, c, decade, decades, dec, year, years, y, month, months, mon, mons, week, weeks, w, day, days, d, hour, hours, h, minute, minutes, min, mins, m, second, seconds, sec, secs, s, millisecond, milliseconds, ms, microsecond, microseconds, us}';
        direction_types text[2] := '{-, +}';
        res timestamp(6);

    begin

        -- Checking interval's type
        if lower(interval_type) != all(int_types) then
            raise warning 'Incorrect type of interval. Available options - %', array_to_string(int_types, ', ');
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select %L::timestamp(6) %s %L * random() * interval '1 %s'
                    $fmt$,
                    base, shift_direction, shift, lower(interval_type)
                  )
            into strict res;

        return res;

        -- Check if result is out of type's range
        exception
            when datetime_field_overflow then
                return base;

    end;

$func$;

comment on function random_timestamp_intvl(base timestamp(6), interval_type text, shift anycompatiblenonarray, twoway bool)
    is 'base (def localtimestamp), interval_type (def ''year''), shift (def 1), twoway (def false)';


-- Random value from text-based timestamp +/- interval

create or replace function random_timestamp_text_intvl
    (
        base text default to_char(localtimestamp, 'dd.mm.yyyy hh24:mi:ss.us'), -- starting point
        frmt text default 'dd.mm.yyyy hh24:mi:ss.us', -- format
        interval_type text default 'year', -- interval's unit of measurement
        shift anycompatiblenonarray default 1, -- max shift from a starting point (in interval's unit of measurement)
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns timestamp(6)
language plpgsql
strict
as
$func$

-- Example:

-- select random_timestamp_text_intvl('03-02-2025 14:21:13.546392', 'mm-dd-yyyy hh24:mi:ss.us', 'hour', -1) -- random ts from specific text-based timestamp to this timestamp - 1 hour
-- from generate_series(1, 10);

    declare

        shift_direction text := '+';
        int_types text[] := '{millennium, millenniums, mil, century, centuries, c, decade, decades, dec, year, years, y, month, months, mon, mons, week, weeks, w, day, days, d, hour, hours, h, minute, minutes, min, mins, m, second, seconds, sec, secs, s, millisecond, milliseconds, ms, microsecond, microseconds, us}';
        direction_types text[2] := '{-, +}';
        res timestamp(6);

    begin

        -- Checking interval's type
        if lower(interval_type) != all(int_types) then
            raise warning 'Incorrect type of interval. Available options - %', array_to_string(int_types, ', ');
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select to_timestamp(%L, %L)::timestamp(6) %s %L * random() * interval '1 %s'
                    $fmt$,
                    base, frmt, shift_direction, shift, lower(interval_type)
                  )
            into strict res;

        return res;

        -- Check if result is out of type's range
        exception
            when datetime_field_overflow then
                return base;

    end;

$func$;

comment on function random_timestamp_text_intvl(base text, frmt text, interval_type text, shift anycompatiblenonarray, twoway bool)
    is 'base (def localtimestamp), frmt (''dd.mm.yyyy hh24:mi:ss.us''), interval_type (def ''year''), shift (def 1), twoway (def false)';