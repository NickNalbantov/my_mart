-- Timestamp with time zone

-- Examples:

-- select random_timestamptz(), -- random tstz from current_timestamp to current_timestamp + 1 year
--        random_timestamptz('01.01.2025'::timestamptz, '01.01.2020'::timestamptz), -- random tstz between '01.01.2025' and '01.01.2020' (arguments could be in any order)
--        random_timestamptz_intvl('01.01.2025'::timestamptz, 'week', 5, true), -- random tstz in range '01.01.2025' +/- 5 weeks
--        random_timestamptz_text_intvl('03-02-2025 14:21:13.546392 +08:00', 'mm-dd-yyyy hh24:mi:ss.us tzh:tzm', 'hour', -1) -- random ts from specific text-based timestamp to this timestamp - 1 hour
-- from generate_series(1, 10);



-- Random value between two timestamptz
        -- Could give not-precise results for dates before 01.01.1970 because of epoch extraction. Better use other functions below in this case.

create or replace function random_timestamptz
    (
        base timestamptz(6) default current_timestamp, -- starting point
        fin timestamptz(6) default current_timestamp + '1 year'::interval -- ending point
    )
returns timestamptz(6)
language plpgsql
strict
as
$func$

-- Could return out-of-bounds results in some corner cases for timestamps before 01.01.1970 because of epoch extraction
-- Better use random_timestamptz_intvl or random_timestamptz_text_intvl functions in this case

-- Examples:

-- select random_timestamptz(), -- random tstz from current_timestamp to current_timestamp + 1 year
--        random_timestamptz('01.01.2025'::timestamptz, '01.01.2020'::timestamptz) -- random tstz between '01.01.2025' and '01.01.2020' (arguments could be in any order)
-- from generate_series(1, 10);

    declare

        diff numeric;
        shift_direction text;
        res timestamptz(6);

    begin

        -- Diff between start and end in microseconds
        select extract(epoch from (fin - base)) * 1000000
        into strict diff;

        -- Direction
        select case when diff < 0 then '-' else '+' end
        into strict shift_direction;

        -- Calculating
        execute
            format(
                    $fmt$
                            select %L::timestamptz(6) %s abs(%L) * random() * interval '1 us'
                    $fmt$,
                    base, shift_direction, diff
                  )
            into strict res;

        return res;

        -- Check if result is out of type's range
        exception
            when datetime_field_overflow then
                return base;

    end;

$func$;

comment on function random_timestamptz(base timestamptz(6), fin timestamptz(6))
    is 'base (def current_timestamp), fin (def current_timestamp + 1 year)';


-- Random value from timestamptz +/- interval

create or replace function random_timestamptz_intvl
    (
        base timestamptz(6) default current_timestamp, -- starting point
        interval_type text default 'year', -- interval's unit of measurement
        shift anycompatiblenonarray default 1, -- max shift from a starting point (in interval's unit of measurement)
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns timestamptz(6)
language plpgsql
strict
as
$func$

-- Example:

-- select random_timestamptz_intvl('01.01.2025'::timestamptz, 'week', 5, true), -- random tstz in range '01.01.2025' +/- 5 weeks
-- from generate_series(1, 10);

    declare

        shift_direction text := '+';
        int_types text[] := '{millennium, millenniums, mil, century, centuries, c, decade, decades, dec, year, years, y, month, months, mon, mons, week, weeks, w, day, days, d, hour, hours, h, minute, minutes, min, mins, m, second, seconds, sec, secs, s, millisecond, milliseconds, ms, microsecond, microseconds, us}';
        direction_types text[2] := '{-, +}';
        res timestamptz(6);

    begin

        -- Checking interval's type
        if lower(interval_type) != all(int_types) then
            raise warning 'Incorrect type of interval. Available options - %', array_to_string(int_types, ', ');
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select %L::timestamptz(6) %s %L * random() * interval '1 %s'
                    $fmt$,
                    base, shift_direction, shift, lower(interval_type)
                  )
            into strict res;

        return res;

        -- Check if result is out of type's range
        exception
            when datetime_field_overflow then
                return base;

    end;

$func$;

comment on function random_timestamptz_intvl(base timestamptz(6), interval_type text, shift anycompatiblenonarray, twoway bool)
    is 'base (def current_timestamp), interval_type (def ''year''), shift (def 1), twoway (def false)';


-- Random value from text-based timestamptz +/- interval

create or replace function random_timestamptz_text_intvl
    (
        base text default to_char(current_timestamp, 'dd.mm.yyyy hh24:mi:ss.us tzh:tzm'), -- starting point
        frmt text default 'dd.mm.yyyy hh24:mi:ss.us tzh:tzm', -- format
        interval_type text default 'year', -- interval's unit of measurement
        shift anycompatiblenonarray default 1, -- max shift from a starting point (in interval's unit of measurement)
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns timestamptz(6)
language plpgsql
strict
as
$func$

-- Example:

-- select random_timestamptz_text_intvl('03-02-2025 14:21:13.546392 +08:00', 'mm-dd-yyyy hh24:mi:ss.us tzh:tzm', 'hour', -1) -- random ts from specific text-based timestamp to this timestamp - 1 hour
-- from generate_series(1, 10);

    declare

        shift_direction text := '+';
        int_types text[] := '{millennium, millenniums, mil, century, centuries, c, decade, decades, dec, year, years, y, month, months, mon, mons, week, weeks, w, day, days, d, hour, hours, h, minute, minutes, min, mins, m, second, seconds, sec, secs, s, millisecond, milliseconds, ms, microsecond, microseconds, us}';
        direction_types text[2] := '{-, +}';
        res timestamptz(6);

    begin

        -- Checking interval's type
        if lower(interval_type) != all(int_types) then
            raise warning 'Incorrect type of interval. Available options - %', array_to_string(int_types, ', ');
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select to_timestamp(%L, %L) %s %L * random() * interval '1 %s'
                    $fmt$,
                    base, frmt, shift_direction, shift, lower(interval_type)
                  )
            into strict res;

        return res;

        -- Check if result is out of type's range
        exception
            when datetime_field_overflow then
                return base;

    end;

$func$;

comment on function random_timestamptz_text_intvl(base text, frmt text, interval_type text, shift anycompatiblenonarray, twoway bool)
    is 'base (def current_timestamp), frmt (''dd.mm.yyyy hh24:mi:ss.us tzh:tzm''), interval_type (def ''year''), shift (def 1), twoway (def false)';