-- Varbit

-- Example:

-- select random_varbit(20)
-- from generate_series(1, 10);

create or replace function random_varbit
    (
        str_length integer default 1 -- length of result
    )
returns bit varying
language plpgsql
strict
as
$func$

-- Example:

-- select random_varbit(20)
-- from generate_series(1, 10);

    declare

        bits text[2] := '{0, 1}';
        text_res text := '';

    begin

        -- Checking result length
        if str_length < 0 then
            raise warning 'Length of resulting string can''t be less than 0 symbols';
            return null;

        elsif str_length > 83886080 then
            raise warning 'Length of a string of varbit type can''t be more than 83886080 symbols';
            return null;

        end if;

        -- Generating random string
        for i in 1..str_length loop
            text_res := text_res || bits[1 + round(random())];
        end loop;

        return text_res::varbit;

    end;

$func$;

comment on function random_varbit(str_length int)
    is 'str_length (def 1)';