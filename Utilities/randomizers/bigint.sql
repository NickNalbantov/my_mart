-- Bigint

-- Examples:

-- select random_int8(), -- random bigint
--           random_int8(100, -100), -- random bigint from -100 to 100 (arguments could be in any order)
--           random_int8_intvl(), -- random bigint
--           random_int8_intvl(300, 500, true) -- random bigint from 300 - 500 to 300 + 500
-- from generate_series(1, 10);


-- Random value between two bigints

create or replace function random_int8
    (
        base anycompatiblenonarray default -9223372036854775808, -- starting point
        fin anycompatiblenonarray default 9223372036854775807 -- ending point
    )
returns bigint
language plpgsql
strict
as
$func$

-- Examples:

-- select random_int8(), -- random bigint
--           random_int8(100, -100) -- random bigint from -100 to 100 (arguments could be in any order)
-- from generate_series(1, 10);

    declare

        diff numeric;
        shift_direction text;
        res bigint;

    begin

        -- Checking starting point
        if base not between -9223372036854775808 and 9223372036854775807
           or fin not between -9223372036854775808 and 9223372036854775807
        then

            raise warning 'Starting and ending points must be inside of int8 type''s range of values';
            return null;

        end if;

        -- Diff between start and end
        select fin::numeric - base::numeric
        into strict diff;

        -- Direction
        select case when diff < 0 then '-' else '+' end
        into strict shift_direction;

        -- Calculating
        execute
            format(
                    $fmt$
                            select round(%L::numeric %s abs(%L) * random())::bigint
                    $fmt$,
                    base, shift_direction, diff
                  )
            into res;

        return res;

        -- Check if result is out of type's range
        exception
            when numeric_value_out_of_range then
                return base;

    end;

$func$;

comment on function random_int8(base anycompatiblenonarray, fin anycompatiblenonarray)
    is 'base (def -9223372036854775808), fin (def 9223372036854775807)';


-- Random value from bigint +/- numeric interval

create or replace function random_int8_intvl
    (
        base anycompatiblenonarray default -9223372036854775808, -- starting point
        shift anycompatiblenonarray default 18446744073709551615, -- max shift from a starting point
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns bigint
language plpgsql
strict
as
$func$

-- Examples:

-- select random_int8_intvl(), -- random bigint
--           random_int8_intvl(300, 500, true) -- random bigint from 300 - 500 to 300 + 500
-- from generate_series(1, 10);

    declare

        shift_direction text := '+';
        direction_types text[2] := '{-, +}';
        res bigint;

    begin

        -- Checking starting point
        if base not between -9223372036854775808 and 9223372036854775807 then
            raise warning 'Starting point must be inside of int8 type''s range of values';
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select round(%L %s %L * random())::bigint
                    $fmt$,
                    base, shift_direction, shift
                  )
            into res;

        return res;

        -- Check if result is out of type's range
        exception
            when numeric_value_out_of_range then
                return base;

    end;

$func$
;

comment on function random_int8_intvl(base anycompatiblenonarray, shift anycompatiblenonarray, twoway bool)
    is 'base (def -9223372036854775808), shift (def 18446744073709551615), twoway (def false)';