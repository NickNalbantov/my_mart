-- Date

-- Examples:

--  select random_date(), -- random date from current_date to current_date + 1 year
--         random_date('01.01.2025'::date, '01.01.2020'::date), -- random date between '01.01.2025' and '01.01.2020' (arguments could be in any order)
--         random_date_intvl('01.01.2025'::date, 'week', 5, true), -- random date in range '01.01.2025' +/- 5 weeks
--         random_date_text_intvl('03-02-2025', 'mm-dd-yyyy', 'month', -1) -- random date from specific text-based date to this date - 1 month
--  from generate_series(1, 10);


-- Random value between two dates

create or replace function random_date
    (
        base date default current_date, -- starting point
        fin date default current_date + '1 year'::interval -- ending point
    )
returns date
language plpgsql
strict
as
$func$

-- Examples:

--  select random_date(), -- random date from current_date to current_date + 1 year
--         random_date('01.01.2025'::date, '01.01.2020'::date) -- random date between '01.01.2025' and '01.01.2020' (arguments could be in any order)
--  from generate_series(1, 10);

    declare

        diff int;
        shift_direction text;
        res date;

    begin

        -- Diff between start and end in days
        select fin - base
        into strict diff;

        -- Direction
        select case when diff < 0 then '-' else '+' end
        into strict shift_direction;

        -- Calculating
        execute
            format(
                    $fmt$
                            select %L::date %s abs(%L) * random() * interval '1 day'
                    $fmt$,
                    base, shift_direction, diff
                  )
            into strict res;

        return res;

        -- Check if result is out of type's range
        exception
            when datetime_field_overflow then
                return base;

    end;

$func$;

comment on function random_date(base date, fin date)
    is 'base (def current_date), fin (def current_date + 1 year)';


-- Random value from date +/- interval

create or replace function random_date_intvl
    (
        base date default current_date, -- starting point
        interval_type text default 'year', -- interval's unit of measurement
        shift anycompatiblenonarray default 1, -- max shift from a starting point (in interval's unit of measurement)
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns date
language plpgsql
strict
as
$func$

-- Example:

--  select random_date_intvl('01.01.2025'::date, 'week', 5, true) -- random date in range '01.01.2025' +/- 5 weeks
--  from generate_series(1, 10);

    declare

        shift_direction text := '+';
        int_types text[] := '{millennium, millenniums, mil, century, centuries, c, decade, decades, dec, year, years, y, month, months, mon, mons, week, weeks, w, day, days, d}';
        direction_types text[2] := '{-, +}';
        res date;

    begin

        -- Checking interval's type
        if lower(interval_type) != all(int_types) then
            raise warning 'Incorrect type of interval. Available options - %', array_to_string(int_types, ', ');
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select %L::date %s %L * random() * interval '1 %s'
                    $fmt$,
                    base, shift_direction, shift, lower(interval_type)
                  )
            into strict res;

        return res;

        -- Check if result is out of type's range
        exception
            when datetime_field_overflow then
                return base;

    end;

$func$;

comment on function random_date_intvl(base date, interval_type text, shift anycompatiblenonarray, twoway bool)
    is 'base (def current_date), interval_type (def ''year''), shift (def 1), twoway (def false)';


-- Random value from text-based date +/- interval

create or replace function random_date_text_intvl
    (
        base text default to_char(current_date, 'dd.mm.yyyy'), -- starting point
        frmt text default 'dd.mm.yyyy', -- format
        interval_type text default 'year', -- interval's unit of measurement
        shift anycompatiblenonarray default 1, -- max shift from a starting point (in interval's unit of measurement)
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns date
language plpgsql
strict
as
$func$

-- Example:

--  select random_date_text_intvl('03-02-2025', 'mm-dd-yyyy', 'month', -1) -- random date from specific text-based date to this date - 1 month
--  from generate_series(1, 10);

    declare

        shift_direction text := '+';
        int_types text[] := '{millennium, millenniums, mil, century, centuries, c, decade, decades, dec, year, years, y, month, months, mon, mons, week, weeks, w, day, days, d}';
        direction_types text[2] := '{-, +}';
        res date;

    begin

        -- Checking interval's type
        if lower(interval_type) != all(int_types) then
            raise warning 'Incorrect type of interval. Available options - %', array_to_string(int_types, ', ');
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select to_date(%L, %L)::date %s %L * random() * interval '1 %s'
                    $fmt$,
                    base, frmt, shift_direction, shift, lower(interval_type)
                  )
            into strict res;

        return res;

        -- Check if result is out of type's range
        exception
            when datetime_field_overflow then
                return base;

    end;

$func$;

comment on function random_date_text_intvl(base text, frmt text, interval_type text, shift anycompatiblenonarray, twoway bool)
    is 'base (def current_date),frmt (def ''dd.mm.yyyy''), interval_type (def ''year''), shift (def 1), twoway (def false)';