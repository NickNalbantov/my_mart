-- Boolean

-- Example:

-- select random_bool()
-- from generate_series(1, 10);

create or replace function random_bool ()
returns boolean
language sql
as
$func$

-- Example:

-- select random_bool()
-- from generate_series(1, 10);

    select random() < 0.5;

$func$;

comment on function random_bool()
    is 'Random boolean value';