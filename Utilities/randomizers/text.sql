-- Text

-- Examples:

-- select random_text(), -- random string (10-symbols long) from English letters (capital and small) + digits + punctuation symbols
--        random_text(20, 'ru'), -- random string (20-symbols long) from Russian letters (capital and small) + digits + punctuation symbols
--        random_text(100, null, dig := 'f'), -- random string (100-symbols long) from punctuation symbols only
--        random_text(5000, 'ru', true, true, true, true, true) -- random string (5000-symbols long) from Russian letters (capital and small) + digits + punctuation symbols + whitespaces + line breaks
-- from generate_series(1, 10);

create or replace function random_text
    (
        str_length integer default 10, -- length of result
        lang text default 'en', -- language - en/ru/null (no letters, only digits/punctuations if specified)
        caps boolean default true, -- randomly adding capital letters
        dig boolean default true, -- randomly adding digits
        punct boolean default true, -- randomly adding punctuation symbols
        space_ boolean default false, -- randomly adding spaces
        newline boolean default false -- randomly adding newline
    )
returns text
language plpgsql
as
$func$

-- Examples:

-- select random_text(), -- random string (10-symbols long) from English letters (capital and small) + digits + punctuation symbols
--        random_text(20, 'ru'), -- random string (20-symbols long) from Russian letters (capital and small) + digits + punctuation symbols
--        random_text(100, null, dig := 'f'), -- random string (100-symbols long) from punctuation symbols only
--        random_text(5000, 'ru', true, true, true, true, true) -- random string (5000-symbols long) from Russian letters (capital and small) + digits + punctuation symbols + whitespaces + line breaks
-- from generate_series(1, 10);

    declare

        chars_eng text[] := '{a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
        chars_eng_caps text[] := '{A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z}';
        chars_rus text[] := '{а,б,в,г,д,е,ё,ж,з,и,й,к,л,м,н,о,п,р,с,т,у,ф,х,ц,ч,ш,щ,ъ,ы,ь,э,ю,я}';
        chars_rus_caps text[] := '{А,Б,В,Г,Д,Е,Ё,Ж,З,И,Й,К,Л,М,Н,О,П,Р,С,Т,У,Ф,Х,Ц,Ч,Ш,Щ,Ъ,Ы,Ь,Э,Ю,Я}';
        chars_dig text[] := '{}';
        chars_punct text[] := '{}';
        chars_space text[] := '{}';
        chars_newline text[] := '{}';
        chars_final text[] := '{}';
        res text := '';

    begin

        -- Checking result length
        if str_length < 0 then
            raise warning 'Length of resulting string can''t be less than 0 symbols';
            return null;
        end if;

        -- Checking chosen sets of symbols
        if dig = true then
            chars_dig := '{0,1,2,3,4,5,6,7,8,9}';
        end if;

        if punct = true then
            chars_punct := string_to_array(E'!d"d#d$d%d&d\'d(d)d*d+d,d-d.d/d:d;d<d=d>d?d@d[d\\d]d^d_d`d{d|d}d~', 'd');
        end if;

        if space_ = true then
            chars_space := string_to_array(' ', ',');
        end if;

        if newline = true then
            chars_newline := string_to_array(E'\r\n', ',');
        end if;

        -- Checking language and addition of capital letters
        if lang = 'en' and caps = true then
            chars_final := chars_eng || chars_eng_caps || chars_dig || chars_punct || chars_space || chars_newline;

        elsif lang = 'en' and caps = false then
            chars_final := chars_eng || chars_dig || chars_punct || chars_space || chars_newline;

        elsif lang = 'ru' and caps = true then
            chars_final := chars_rus || chars_rus_caps || chars_dig || chars_punct || chars_space || chars_newline;

        elsif lang = 'ru' and caps = false then
            chars_final := chars_rus || chars_dig || chars_punct || chars_space || chars_newline;

        elsif lang is null then -- disables letters
            chars_final := chars_dig || chars_punct || chars_space || chars_newline;

        else
            raise warning 'Language ''%'' is not available. Choose one of these options: ''en'',''ru'', null', lang;
            return null;

        end if;

        -- Checking if all sets of symbols are disabled
        if lang is null and dig = false and punct = false and space_ = false and newline = false then
            raise notice 'No character sets were chosen in options, result - empty string';
        end if;

        -- Generating random string
        for i in 1..str_length loop
            res := res || chars_final[1 + round(random() * (cardinality(chars_final) - 1))];
        end loop;

        -- Trimming extra symbols that could come from \r\n
        if length(res) > str_length then
            res := left(res, str_length);
        end if;

        return res;

    end;

$func$;

comment on function random_text(str_length int, lang text, caps boolean, dig boolean, punct boolean, space_ boolean, newline boolean)
    is 'str_length (def 10), lang (def ''en'', opt ''ru''/null), caps (def true), dig (def true), punct (def true), space_ (def false), newline (def false)';