-- Smallint

-- Examples:

-- select random_int2(), -- random smallint
--           random_int2(100, -100), -- random smallint from -100 to 100 (arguments could be in any order)
--           random_int2_intvl(), -- random smallint
--           random_int2_intvl(300, 500, true) -- random smallint from 300 - 500 to 300 + 500
-- from generate_series(1, 10);

-- Random value between two smallints

create or replace function random_int2
    (
        base anycompatiblenonarray default -32768, -- starting point
        fin anycompatiblenonarray default 32767 -- ending point
    )
returns smallint
language plpgsql
strict
as
$func$

-- Examples:

-- select random_int2(), -- random smallint
--           random_int2(100, -100) -- random smallint from -100 to 100 (arguments could be in any order)
-- from generate_series(1, 10);

    declare

        diff numeric;
        shift_direction text;
        res smallint;

    begin

        -- Checking starting point
        if base not between -32768 and 32767
            or fin not between -32768 and 32767
        then

            raise warning 'Starting and ending points must be inside of int2 type''s range of values';
            return null;

        end if;

        -- Diff between start and end
        select fin::numeric - base::numeric
        into strict diff;

        -- Direction
        select case when diff < 0 then '-' else '+' end
        into strict shift_direction;

        -- Calculating
        execute
            format(
                    $fmt$
                            select round(%L::numeric %s abs(%L) * random())::smallint
                    $fmt$,
                    base, shift_direction, diff
                  )
            into res;

        return res;

        -- Check if result is out of type's range
        exception
            when numeric_value_out_of_range then
                return base;

    end;

    $func$;

comment on function random_int2(base anycompatiblenonarray, fin anycompatiblenonarray)
    is 'base (def -32768), fin (def 32767)';


-- Random value from smallint +/- numeric interval

create or replace function random_int2_intvl
    (
        base anycompatiblenonarray default -32768, -- starting point
        shift anycompatiblenonarray default 65535, -- max shift from a starting point
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns smallint
language plpgsql
strict
as
$func$

-- Examples:

-- select random_int2_intvl(), -- random smallint
--           random_int2_intvl(300, 500, true) -- random smallint from 300 - 500 to 300 + 500
-- from generate_series(1, 10);

    declare

        shift_direction text := '+';
        direction_types text[2] := '{-, +}';
        res smallint;

    begin

        -- Checking starting point
        if base not between -32768 and 32767 then
            raise warning 'Starting point must be inside of int2 type''s range of values';
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select round(%L %s %L * random())::smallint
                    $fmt$,
                    base, shift_direction, shift
                  )
            into res;

        return res;

        -- Check if result is out of type's range
        exception
            when numeric_value_out_of_range then
                return base;

    end;

$func$;

comment on function random_int2_intvl(base anycompatiblenonarray, shift anycompatiblenonarray, twoway bool)
    is 'base (def -32768), shift (def 65535), twoway (def false)';