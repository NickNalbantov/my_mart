-- Numeric / decimal

-- Examples:

-- select random_numeric(), -- random numeric from 0 to 100
--           random_numeric(200, -200, 5, 2), -- random numeric with 2 digits in fractional part from -200 to 200 (arguments could be in any order)
--           -- random_numeric(200, -200, 5, -2), -- PG15+ only - random numeric with scale < 0 (rounding last two digits) from -200 to 200
--           -- random_numeric(-0.0009, 0.0009, 5, 8), -- PG15+ only - random numeric with scale > precision from -0.0009 to 0.0009
--           random_numeric_intvl(300, 500, 8, 5, true), -- random numeric with 5 digits in fractional part from 300 - 500 to 300 + 500
--           -- random_numeric_intvl(300, 500, 5, -2, true), -- PG15+ only - random numeric with scale < 0 (rounding last two digits) from 300 - 500 to 300 + 500
--           -- random_numeric_intvl(-0.0009, 0.0018, 5, 8, true) -- PG15+ only - random numeric with scale > precision
-- from generate_series(1, 10);


-- Random value between two numerics

create or replace function random_numeric
    (
        base anycompatiblenonarray default 0, -- starting point
        fin anycompatiblenonarray default 100, -- max shift from a starting point
        _precision int default null, -- precision - count of all digits in number
        _scale int default null -- scale - count of all digits in fractional part
    )
returns numeric
language plpgsql
as
$func$
-- Examples:

-- select random_numeric(), -- random numeric from 0 to 100
--           random_numeric(200, -200, 5, 2), -- random numeric with 2 digits in fractional part from -200 to 200 (arguments could be in any order)
--           -- random_numeric(200, -200, 5, -2), -- PG15+ only - random numeric with scale < 0 (rounding last two digits) from -200 to 200
--           -- random_numeric(-0.0009, 0.0009, 5, 8) -- PG15+ only - random numeric with scale > precision from -0.0009 to 0.0009
-- from generate_series(1, 10);

    declare

        pg_version smallint;
        diff numeric;
        shift_direction text;
        num_prec text := 'numeric';
        pre_result numeric;
        res numeric;

    begin

        -- Checking PG major version
            --Since PG15 it is possible that scale < 0 (works like rounding) and scale > precision (for fractions 0 <= abs(n) < 1)
        execute $$ select substring(version(), '\d{1,2}')::smallint $$
            into pg_version;

        -- Diff between start and end
        select fin::numeric - base::numeric
        into strict diff;


        -- Direction
        select case when diff < 0 then '-' else '+' end
        into strict shift_direction;

        -- Precision and scale

        -- Level 1 - checking if input arguments _precision and _scale is null
        if _precision is not null then

            -- Level 2 - checking value of input argument _precision
            if _precision not between 1 and 1000 then
                raise warning 'Precision must be specified in range from 1 to 1000. If higher precision is needed, than _precision and _scale arguments must be null';
                return null;

            else

                -- Level 3 - checking PG major version
                if pg_version < 15 then

                    -- Level 4 - checking value of input argument _scale
                    if _scale is null then
                        num_prec := format('numeric(%s)', _precision);

                    elsif _scale is not null and _scale >= 0 and _scale <= _precision then
                        num_prec := format('numeric(%s, %s)', _precision, _scale);

                    else
                        raise warning 'Scale must be specified in range from 0 to precision';
                        return null;

                    end if;

                else

                    -- Level 4 - checking value of input argument _scale
                    if _scale is null then
                        num_prec := format('numeric(%s)', _precision);

                    elsif _scale is not null and _scale >= 0 and _scale <= _precision then
                        num_prec := format('numeric(%s, %s)', _precision, _scale);

                    elsif (_scale < 0 or _scale > _precision) and round(abs(base + diff), _scale) < pow(10, _precision - _scale) then
                        num_prec := format('numeric(%s, %s)', _precision, _scale);

                    else
                        raise warning 'Incorrect scale value. If scale > precision or scale < 0 then round(numeric, _scale) must be less than pow(10, precision - scale)';
                        return null;

                    end if;

                end if;

            end if;

        elsif _precision is null and _scale is not null then
            raise warning 'Numeric scale can''t be specified without precision';
            return null;

        end if;

        -- Calculating
        pre_result := 1 + pow(10, _precision - _scale);

        if (_scale < 0 or _scale > _precision) then

            while round(abs(pre_result), _scale) >= pow(10, _precision - _scale)
            loop
                execute
                    format(
                            $fmt$
                                    select (%L %s abs(%L) * random())
                            $fmt$,
                            base, shift_direction, diff
                          )
                    into pre_result;
            end loop;

            execute
            format(
                    $fmt$
                            select %L::%s
                    $fmt$,
                    pre_result, num_prec
                  )
            into res;

        else

        execute
            format(
                    $fmt$
                            select (%L %s abs(%L) * random())::%s
                    $fmt$,
                    base, shift_direction, diff, num_prec
                  )
            into res;

        end if;

        return res;

        -- Check if result is out of type's range
        exception
            when numeric_value_out_of_range then
                return base;

    end;

$func$;

comment on function random_numeric(base anycompatiblenonarray, fin anycompatiblenonarray, _precision int, _scale int)
    is 'base (def 0), fin (def 100), _precision (def null), _scale (def null)';


-- Random value from numeric +/- numeric interval

create or replace function random_numeric_intvl
    (
        base anycompatiblenonarray default 0, -- starting point
        shift anycompatiblenonarray default 100, -- max shift from a starting point
        _precision int default null, -- precision - count of all digits in number
        _scale int default null, -- scale - count of all digits in fractional part
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns numeric
language plpgsql
as
$func$

-- Examples:

-- select random_numeric_intvl(300, 500, 8, 5, true), -- random numeric with 5 digits in fractional part from 300 - 500 to 300 + 500
--           -- random_numeric_intvl(300, 500, 5, -2, true), -- PG15+ only - random numeric with scale < 0 (rounding last two digits) from 300 - 500 to 300 + 500
--           -- random_numeric_intvl(-0.0009, 0.0018, 5, 8, true) -- PG15+ only - random numeric with scale > precision
-- from generate_series(1, 10);

    declare

        pg_version smallint;
        shift_direction text := '+';
        direction_types text[2] := '{-, +}';
        num_prec text := 'numeric';
        pre_result numeric;
        res numeric;

    begin

        -- Checking PG major version
            --Since PG15 it is possible that scale < 0 (works like rounding) and scale > precision (for fractions 0 <= abs(n) < 1)
        execute $$ select substring(version(), '\d{1,2}')::smallint $$
            into pg_version;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Precision and scale

        -- Level 1 - checking if input arguments _precision and _scale is null
        if _precision is not null then

            -- Level 2 - checking value of input argument _precision
            if _precision not between 1 and 1000 then
                raise warning 'Precision must be specified in range from 1 to 1000. If higher precision is needed, than _precision and _scale arguments must be null';
                return null;

            else

                -- Level 3 - checking PG major version
                if pg_version < 15 then

                    -- Level 4 - checking value of input argument _scale
                    if _scale is null then
                        num_prec := format('numeric(%s)', _precision);

                    elsif _scale is not null and _scale >= 0 and _scale <= _precision then
                        num_prec := format('numeric(%s, %s)', _precision, _scale);

                    else
                        raise warning 'Scale must be specified in range from 0 to precision';
                        return null;

                    end if;

                else

                    -- Level 4 - checking value of input argument _scale
                    if _scale is null then
                        num_prec := format('numeric(%s)', _precision);

                    elsif _scale is not null and _scale >= 0 and _scale <= _precision then
                        num_prec := format('numeric(%s, %s)', _precision, _scale);

                    elsif (_scale < 0 or _scale > _precision) and round(abs(base + shift), _scale) < pow(10, _precision - _scale) then
                        num_prec := format('numeric(%s, %s)', _precision, _scale);

                    else
                        raise warning 'Incorrect scale value. If scale > precision or scale < 0 then round(numeric, _scale) must be less than pow(10, precision - scale)';
                        return null;

                    end if;

                end if;

            end if;

        elsif _precision is null and _scale is not null then
            raise warning 'Numeric scale can''t be specified without precision';
            return null;

        end if;

        -- Calculating
        pre_result := 1 + pow(10, _precision - _scale);

        if (_scale < 0 or _scale > _precision) then

            while round(abs(pre_result), _scale) >= pow(10, _precision - _scale)
            loop
                execute
                    format(
                            $fmt$
                                    select (%L %s %L * random())
                            $fmt$,
                            base, shift_direction, shift
                          )
                    into pre_result;
            end loop;

            execute
            format(
                    $fmt$
                            select %L::%s
                    $fmt$,
                    pre_result, num_prec
                  )
            into res;

        else

        execute
            format(
                    $fmt$
                            select (%L %s %L * random())::%s
                    $fmt$,
                    base, shift_direction, shift, num_prec
                  )
            into res;

        end if;

        return res;

        -- Check if result is out of type's range
        exception
            when numeric_value_out_of_range then
                return base;

    end;

$func$;

comment on function random_numeric_intvl(base anycompatiblenonarray, shift anycompatiblenonarray, _precision int, _scale int, twoway bool)
    is 'base (def 0), shift (def 100), _precision (def null), _scale (def null), twoway (def false)';

