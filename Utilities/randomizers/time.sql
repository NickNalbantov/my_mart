-- Time without time zone

-- Examples:

-- select random_time(), -- random time value
--        random_time('14:21:13.546392'::time, '01:00:02'::time), -- random time between '14:21:13.546392' and '01:00:02'
--        random_time('01:00:02'::time, '14:21:13.546392'::time), -- random time between '01:00:02' and '14:21:13.546392'
--        random_time_intvl('00:00:00'::time, 'minute', 5, true), -- random time in range '00:00:00' +/- 5 minutes
--        random_time_text_intvl('14:21:13.546392', 'hh24:mi:ss.us', 'hour', -1) -- random time from specific text-based time to this time - 1 hour
-- from generate_series(1, 10);


-- Random value between two time points

create or replace function random_time
    (
        base time(6) default localtimestamp::time(6), -- starting point
        fin time(6) default localtimestamp::time(6) + '86399.999999 second'::interval -- ending point
    )
returns time
language plpgsql
strict
as
$func$

-- Examples:

-- select random_time(), -- random time value
--        random_time('14:21:13.546392'::time, '01:00:02'::time), -- random time between '14:21:13.546392' and '01:00:02'
--        random_time('01:00:02'::time, '14:21:13.546392'::time) -- random time between '01:00:02' and '14:21:13.546392'
-- from generate_series(1, 10);

    declare

        diff bigint;
        shift_direction text;
        res time(6);

    begin

        -- Diff between start and end in microseconds
        if fin >= base then -- 01:00:00 - 23:00:00, diff 22 * 3600 * 1000000
            select extract(epoch from (fin - base)) * 1000000
            into strict diff;

        elsif fin < base then  -- 23:00:00 - 01:00:00, diff 2 * 3600 * 1000000
            select extract(epoch from ('24 hours'::interval - (base - fin))) * 1000000
            into strict diff;

        end if;

        -- Direction
        select case when diff < 0 then '-' else '+' end
        into strict shift_direction;

        -- Calculating
        execute
            format(
                    $fmt$
                            select %L::time(6) %s abs(%L) * random() * interval '1 us'
                    $fmt$,
                    base, shift_direction, diff
                  )
            into strict res;

        return res;

    end;

$func$;

comment on function random_time(base time(6), fin time(6))
    is 'base (def localtimestamp::time), fin (def localtimestamp + 1 day - 1 us ::time)';


-- Random value from time +/- interval

create or replace function random_time_intvl
    (
        base time(6) default localtimestamp::time(6), -- starting point
        interval_type text default 'hour', -- interval's unit of measurement
        shift anycompatiblenonarray default 24, -- max shift from a starting point (in interval's unit of measurement)
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns time
language plpgsql
as
$func$

-- Example:

-- select random_time_intvl('00:00:00'::time, 'minute', 5, true) -- random time in range '00:00:00' +/- 5 minutesrandom_time_text_intvl('14:21:13.546392', 'hh24:mi:ss.us', 'hour', -1) -- random time from specific text-based time to this time - 1 hour
-- from generate_series(1, 10);

    declare

        shift_direction text := '+';
        int_types text[] := '{hour, hours, h, minute, minutes, min, mins, m, second, seconds, sec, secs, s}';
        direction_types text[2] := '{-, +}';
        res time(6);

    begin

        -- Checking interval's type
        if lower(interval_type) != all(int_types) then
            raise warning 'Incorrect type of interval. Available options - %', array_to_string(int_types, ', ');
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select %L::time(6) %s %L * random() * interval '1 %s'
                    $fmt$,
                    base, shift_direction, shift, lower(interval_type)
                  )
            into res;

        return res;

    end;

$func$;

comment on function random_time_intvl(base time(6), interval_type text, shift anycompatiblenonarray, twoway bool)
    is 'base (def localtimestamp::time), interval_type (def ''hour''), shift (def 24), twoway (def false)';


-- Random value from text-based time +/- interval

create or replace function random_time_text_intvl
    (
        base text default to_char(localtimestamp, 'hh24:mi:ss.us'), -- starting point
        frmt text default 'hh24:mi:ss.us', -- format
        interval_type text default 'hour', -- interval's unit of measurement
        shift anycompatiblenonarray default 24, -- max shift from a starting point (in interval's unit of measurement)
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns time
language plpgsql
as
$func$

-- Example:

-- select random_time_text_intvl('14:21:13.546392', 'hh24:mi:ss.us', 'hour', -1) -- random time from specific text-based time to this time - 1 hour
-- from generate_series(1, 10);

    declare

        shift_direction text := '+';
        int_types text[] := '{hour, hours, h, minute, minutes, min, mins, m, second, seconds, sec, secs, s}';
        direction_types text[2] := '{-, +}';
        res time(6);

    begin

        -- Checking interval's type
        if lower(interval_type) != all(int_types) then
            raise warning 'Incorrect type of interval. Available options - %', array_to_string(int_types, ', ');
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select to_char(('01.01.2000 '|| %L)::timestamp(6), %L)::time(6) %s %L * random() * interval '1 %s'
                    $fmt$,
                    base, frmt, shift_direction, shift, lower(interval_type)
                  )
            into res;

        return res;

    end;

$func$;

comment on function random_time_text_intvl(base text, frmt text, interval_type text, shift anycompatiblenonarray, twoway bool)
    is 'base (def localtimestamp::time), frmt (''hh24:mi:ss.us''),interval_type (def ''hour''), shift (def 24), twoway (def false)';