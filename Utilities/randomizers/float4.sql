-- Real

-- Examples:

-- select random_float4(), -- random real from 0 to 100
--           random_float4(100, -100), -- random real from -100 to 100 (arguments could be in any order)
--           random_float4_intvl(300, 500, true) -- random real from 300 - 500 to 300 + 500
-- from generate_series(1, 10);


-- Random value between two reals

create or replace function random_float4
    (
        base anycompatiblenonarray default 0, -- starting point
        fin anycompatiblenonarray default 100 -- ending point
    )
returns real
language plpgsql
strict
as
$func$

-- Examples:

-- select random_float4(), -- random real from 0 to 100
--           random_float4(100, -100) -- random real from -100 to 100 (arguments could be in any order)
-- from generate_series(1, 10);

    declare

        diff numeric;
        shift_direction text;
        res real;

    begin

        -- Diff between start and end
        select fin::numeric - base::numeric
        into strict diff;

        -- Direction
        select case when diff < 0 then '-' else '+' end
        into strict shift_direction;

        -- Calculating
        execute
            format(
                    $fmt$
                            select (%L::numeric %s abs(%L) * random())::real
                    $fmt$,
                    base, shift_direction, diff
                  )
            into res;

        return res;

        -- Check if result is out of type's range
        exception
            when numeric_value_out_of_range then
                return base;

    end;

$func$;

comment on function random_float4(base anycompatiblenonarray, fin anycompatiblenonarray)
    is 'base (def 0), fin (def 100)';



-- Random value from real +/- numeric interval

create or replace function random_float4_intvl
    (
        base anycompatiblenonarray default 0, -- starting point
        shift anycompatiblenonarray default 100, -- max shift from a starting point
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns real
language plpgsql
as
$func$

-- Example:

-- select random_float4_intvl(300, 500, true) -- random real from 300 - 500 to 300 + 500
-- from generate_series(1, 10);

    declare
        shift_direction text := '+';
        direction_types text[2] := '{-, +}';
        res real;
    begin

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select (%L %s %L * random())::real
                    $fmt$,
                    base, shift_direction, shift
                  )
            into res;

        return res;

        -- Check if result is out of type's range
        exception
            when numeric_value_out_of_range then
                return base;

    end;

$func$;

comment on function random_float4_intvl(base anycompatiblenonarray, shift anycompatiblenonarray, twoway bool)
    is 'base (def 0), shift (def 100), twoway (def false)';