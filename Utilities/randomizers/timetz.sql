-- Time with time zone

-- Examples:

-- select random_timetz(), -- random timetz value
--        random_timetz('14:21:13.546392 +01:00'::timetz, '01:00:02'::timetz), -- random timetz between '14:21:13.546392' and '01:00:02'
--        random_timetz('01:00:02 +01:00'::timetz, '14:21:13.546392 +01:00'::timetz), -- random timetz between '01:00:02' and '14:21:13.546392'
--        random_timetz_intvl('00:00:00 +01:00'::timetz, 'minute', 5, true), -- random timetz in range '00:00:00' +/- 5 minutes
--        random_timetz_text_intvl('14:21:13.546392 +01:00', 'hh24:mi:ss.us tzh:tzm', 'hour', -1) -- random timetz from specific text-based time to this time - 1 hour
-- from generate_series(1, 10);


-- Random value between two timetz points

create or replace function random_timetz
    (
        base timetz(6) default current_timestamp::timetz(6), -- starting point
        fin timetz(6) default current_timestamp::timetz(6) + '86399.999999 second'::interval -- ending point
    )
returns timetz
language plpgsql
strict
as
$func$

-- There is no operator timetz - timetz because there's no sense to calculate time diff between different timezones
-- So, function finds diff between base and fin values converted to UTC, then generating random values in range from base with its original timezone to base with its original timezone + diff

-- Examples:

-- select random_timetz(), -- random timetz value
--        random_timetz('14:21:13.546392 +01:00'::timetz, '01:00:02'::timetz), -- random timetz between '14:21:13.546392' and '01:00:02'
--        random_timetz('01:00:02 +01:00'::timetz, '14:21:13.546392 +01:00'::timetz) -- random timetz between '01:00:02' and '14:21:13.546392'
-- from generate_series(1, 10);

    declare

        diff bigint;
        shift_direction text;
        res timetz(6);

    begin

        -- Diff between start and end in microseconds
        if fin >= base then -- 01:00:00 - 23:00:00, diff 22 * 3600 * 1000000
            select extract(epoch from (fin::time - base::time)) * 1000000
            into strict diff;

        elsif fin < base then  -- 23:00:00 - 01:00:00, diff 2 * 3600 * 1000000
            select extract(epoch from ('24 hours'::interval - (base::time - fin::time))) * 1000000
            into strict diff;

        end if;

        -- Direction
        select case when diff < 0 then '-' else '+' end
        into strict shift_direction;

        -- Calculating
        execute
            format(
                    $fmt$
                            select %L::timetz(6) %s abs(%L) * random() * interval '1 us'
                    $fmt$,
                    base, shift_direction, diff
                  )
            into strict res;

        return res;

    end;

$func$;

comment on function random_timetz(base timetz(6), fin timetz(6))
    is 'base (def current_timestamp::timetz), fin (def current_timestamp + 1 day - 1 us ::timetz)';


-- Random value from timetz +/- interval

create or replace function random_timetz_intvl
    (
        base timetz(6) default current_timestamp::timetz(6), -- starting point
        interval_type text default 'hour', -- interval's unit of measurement
        shift anycompatiblenonarray default 24, -- max shift from a starting point (in interval's unit of measurement)
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns timetz
language plpgsql
as
$func$

-- There is no operator timetz - timetz because there's no sense to calculate time diff between different timezones
-- So, function finds diff between base and fin values converted to UTC, then generating random values in range from base with its original timezone to base with its original timezone + diff

-- Example:

-- select random_timetz_intvl('00:00:00 +01:00'::timetz, 'minute', 5, true) -- random timetz in range '00:00:00' +/- 5 minutes
-- from generate_series(1, 10);

    declare

        shift_direction text := '+';
        int_types text[] := '{hour, hours, h, minute, minutes, min, mins, m, second, seconds, sec, secs, s}';
        direction_types text[2] := '{-, +}';
        res timetz(6);

    begin

        -- Checking interval's type
        if lower(interval_type) != all(int_types) then
            raise warning 'Incorrect type of interval. Available options - %', array_to_string(int_types, ', ');
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select %L::timetz(6) %s %L * random() * interval '1 %s'
                    $fmt$,
                    base, shift_direction, shift, lower(interval_type)
                  )
            into res;

        return res;

    end;

$func$;

comment on function random_timetz_intvl(base timetz(6), interval_type text, shift anycompatiblenonarray, twoway bool)
    is 'base (def current_timestamp::timetz), interval_type (def ''hour''), shift (def 24), twoway (def false)';


-- Random value from text-based timetz +/- interval

create or replace function random_timetz_text_intvl
    (
        base text default to_char(current_timestamp, 'hh24:mi:ss.us tzh:tzm'), -- starting point
        frmt text default 'hh24:mi:ss.us tzh:tzm', -- format
        interval_type text default 'hour', -- interval's unit of measurement
        shift anycompatiblenonarray default 24, -- max shift from a starting point (in interval's unit of measurement)
        twoway boolean default false -- enables shifting in both directions from starting point
    )
returns timetz
language plpgsql
as
$func$

-- There is no operator timetz - timetz because there's no sense to calculate time diff between different timezones
-- So, function finds diff between base and fin values converted to UTC, then generating random values in range from base with its original timezone to base with its original timezone + diff

-- Example:

-- select random_timetz_text_intvl('14:21:13.546392 +01:00', 'hh24:mi:ss.us tzh:tzm', 'hour', -1) -- random timetz from specific text-based time to this time - 1 hour
-- from generate_series(1, 10);

    declare

        shift_direction text := '+';
        int_types text[] := '{hour, hours, h, minute, minutes, min, mins, m, second, seconds, sec, secs, s}';
        direction_types text[2] := '{-, +}';
        res timetz(6);

    begin

        -- Checking interval's type
        if lower(interval_type) != all(int_types) then
            raise warning 'Incorrect type of interval. Available options - %', array_to_string(int_types, ', ');
            return null;
        end if;

        -- Direction (sign) of shift
        if twoway is true then
            shift_direction := direction_types[1 + round(random())];
        end if;

        -- Calculating
        execute
            format(
                    $fmt$
                            select to_char(('01.01.2000 '|| %L)::timestamptz(6), %L)::timetz(6) %s %L * random() * interval '1 %s'
                    $fmt$,
                    base, frmt, shift_direction, shift, lower(interval_type)
                  )
            into res;

        return res;

    end;

$func$;

comment on function random_timetz_text_intvl(base text, frmt text, interval_type text, shift anycompatiblenonarray, twoway bool)
    is 'base (def current_timestamp::timetz), frmt (''hh24:mi:ss.us tzh:tzm''), interval_type (def ''hour''), shift (def 24), twoway (def false)';