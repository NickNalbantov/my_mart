-- /Aggregates/first_last_agg.sql
drop function if exists first_agg (anyelement, anyelement) cascade;
drop aggregate if exists first (anyelement) cascade;

drop function if exists last_agg (anyelement, anyelement) cascade;
drop aggregate if exists last (anyelement) cascade;


-- /Aggregates/jsonb_value_agg.sql
drop function if exists jsonb_value_agg_sum(in pg_catalog.jsonb, in pg_catalog.jsonb) cascade;
drop aggregate if exists jsonb_value_sum(in pg_catalog.jsonb) cascade;

drop function if exists jsonb_value_agg_avg(in pg_catalog.jsonb, in pg_catalog.jsonb) cascade;
drop aggregate if exists jsonb_value_avg(in pg_catalog.jsonb) cascade;

drop function if exists jsonb_value_agg_max_num(in pg_catalog.jsonb, in pg_catalog.jsonb) cascade;
drop aggregate if exists jsonb_value_max_num(in pg_catalog.jsonb) cascade;

drop function if exists jsonb_value_agg_max_txt(in pg_catalog.jsonb, in pg_catalog.jsonb) cascade;
drop aggregate if exists jsonb_value_max_txt(in pg_catalog.jsonb) cascade;

drop function if exists jsonb_value_agg_min_num(in pg_catalog.jsonb, in pg_catalog.jsonb) cascade;
drop aggregate if exists jsonb_value_min_num(in pg_catalog.jsonb) cascade;

drop function if exists jsonb_value_agg_min_txt(in pg_catalog.jsonb, in pg_catalog.jsonb) cascade;
drop aggregate if exists jsonb_value_min_txt(in pg_catalog.jsonb) cascade;


-- /Bins
drop function if exists timestamp_bins(in pg_catalog._timestamp, in pg_catalog.text, in pg_catalog."numeric", in pg_catalog.text) cascade;
drop function if exists timestamp_bins(in pg_catalog."timestamp", in pg_catalog.text, in pg_catalog."numeric", in pg_catalog.text) cascade;

drop function if exists timestamptz_bins(in pg_catalog._timestamptz, in pg_catalog.text, in pg_catalog."numeric", in pg_catalog.text) cascade;
drop function if exists timestamptz_bins(in pg_catalog.timestamptz, in pg_catalog.text, in pg_catalog."numeric", in pg_catalog.text) cascade;

drop function if exists date_bins(in pg_catalog._date, in pg_catalog.text, in pg_catalog."numeric", in pg_catalog.text) cascade;
drop function if exists date_bins(in pg_catalog.date, in pg_catalog.text, in pg_catalog."numeric", in pg_catalog.text) cascade;

drop function if exists time_bins(in pg_catalog._time, in pg_catalog.text, in pg_catalog."numeric") cascade;
drop function if exists time_bins(in pg_catalog."time", in pg_catalog.text, in pg_catalog."numeric") cascade;

drop function if exists timetz_bins(in pg_catalog._timetz, in pg_catalog.text, in pg_catalog."numeric") cascade;
drop function if exists timetz_bins(in pg_catalog.timetz, in pg_catalog.text, in pg_catalog."numeric") cascade;

drop function if exists numeric_bins(in pg_catalog.anycompatiblearray, in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;
drop function if exists numeric_bins(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;

drop function if exists int8_bins(in pg_catalog.anycompatiblearray, in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;
drop function if exists int8_bins(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;

drop function if exists int_bins(in pg_catalog.anycompatiblearray, in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;
drop function if exists int_bins(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;

drop function if exists int2_bins(in pg_catalog.anycompatiblearray, in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;
drop function if exists int2_bins(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;


-- /Misc
drop function if exists is_sql(in pg_catalog.text, in pg_catalog.bool) cascade;

drop function if exists instr(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists instr(in pg_catalog.text, in pg_catalog.text, in pg_catalog.int4) cascade;
drop function if exists instr(in pg_catalog.text, in pg_catalog.text, in pg_catalog.int4, in pg_catalog.int4) cascade;

drop function if exists heap_page(in pg_catalog.text, in pg_catalog.int8) cascade;
drop function if exists index_page(in pg_catalog.text, in pg_catalog.int8) cascade;

drop function if exists notice_ddl_event() cascade;
drop event trigger if exists tr_ddl_event_drops cascade;
drop event trigger if exists tr_ddl_event_no_drops cascade;
drop table if exists ddl_events cascade;


-- /Randomizers
drop function if exists random_int8(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;
drop function if exists random_int8_intvl(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;

drop function if exists random_bool() cascade;

drop function if exists random_date(in pg_catalog.date, in pg_catalog.date) cascade;
drop function if exists random_date_intvl(in pg_catalog.date, in pg_catalog.text, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;
drop function if exists random_date_text_intvl(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;

drop function if exists random_float4(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;
drop function if exists random_float4_intvl(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;

drop function if exists random_float8(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;
drop function if exists random_float8_intvl(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;

drop function if exists random_int(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;
drop function if exists random_int_intvl(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;

drop function if exists random_numeric(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray, in pg_catalog.int4, in pg_catalog.int4) cascade;
drop function if exists random_numeric_intvl(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray, in pg_catalog.int4, in pg_catalog.int4, in pg_catalog.bool) cascade;

drop function if exists random_int2(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;
drop function if exists random_int2_intvl(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;

drop function if exists random_text(in pg_catalog.int4, in pg_catalog.text, in pg_catalog.bool, in pg_catalog.bool, in pg_catalog.bool, in pg_catalog.bool, in pg_catalog.bool) cascade;

drop function if exists random_time(in pg_catalog."time", in pg_catalog."time") cascade;
drop function if exists random_time_intvl(in pg_catalog."time", in pg_catalog.text, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;
drop function if exists random_time_text_intvl(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;

drop function if exists random_timestamp(in pg_catalog."timestamp", in pg_catalog."timestamp") cascade;
drop function if exists random_timestamp_intvl(in pg_catalog."timestamp", in pg_catalog.text, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;
drop function if exists random_timestamp_text_intvl(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;

drop function if exists random_timestamptz(in pg_catalog.timestamptz, in pg_catalog.timestamptz) cascade;
drop function if exists random_timestamptz_intvl(in pg_catalog.timestamptz, in pg_catalog.text, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;
drop function if exists random_timestamptz_text_intvl(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;

drop function if exists random_timetz(in pg_catalog.timetz, in pg_catalog.timetz) cascade;
drop function if exists random_timetz_intvl(in pg_catalog.timetz, in pg_catalog.text, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;
drop function if exists random_timetz_text_intvl(in pg_catalog.text, in pg_catalog.text, in pg_catalog.text, in pg_catalog.anycompatiblenonarray, in pg_catalog.bool) cascade;

drop function if exists random_varbit(in pg_catalog.int4) cascade;


-- /array_sort_distinct.sql
drop function if exists array_sort_distinct(in pg_catalog.anyarray, in pg_catalog.bool, in pg_catalog.bool, in pg_catalog.bool) cascade;
drop function if exists array_sort_distinct_pl(in pg_catalog.anyarray, in pg_catalog.bool, in pg_catalog.bool, in pg_catalog.bool) cascade;


-- /bits_to_nums.sql
drop function if exists bitstring_to_numeric(in pg_catalog.text) cascade;
drop function if exists varbit_to_numeric(in pg_catalog.varbit) cascade;
drop function if exists hex_to_numeric(in pg_catalog.text) cascade;

drop function if exists num_to_bitstring(in pg_catalog.anycompatiblenonarray, in pg_catalog.int4) cascade;
drop function if exists num_to_varbit(in pg_catalog.anycompatiblenonarray) cascade;
drop function if exists num_to_hex(in pg_catalog.anycompatiblenonarray, in pg_catalog.int4) cascade;



-- /calendar.sql
drop function if exists month_calendar(in pg_catalog.anycompatiblenonarray, in pg_catalog.anycompatiblenonarray) cascade;
drop function if exists month_calendar_text(in pg_catalog.text, in pg_catalog.text) cascade;
drop function if exists month_calendar_date(in pg_catalog.date) cascade;

drop function if exists year_calendar(in pg_catalog.anycompatiblenonarray) cascade;
drop function if exists year_calendar_text(in pg_catalog.text) cascade;
drop function if exists year_calendar_date(in pg_catalog.date) cascade;


-- /system.sql
drop procedure if exists switch_triggers(in pg_catalog.bool, in pg_catalog._text, in pg_catalog._text, in pg_catalog._text, in pg_catalog.bool) cascade;
drop procedure if exists terminate_idle() cascade;

drop view if exists pg_operators cascade;
drop view if exists pg_comments cascade;
drop view if exists pg_lock_monitor cascade;