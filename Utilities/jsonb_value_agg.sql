-- Example:

-- select jsonb_value_sum(q1 -> 'data')
-- from unnest(
            -- array[
            --       '{"data":{"-1": 22, "123": 10, "100": 5}}'::jsonb,
            --       '{"data": {"-1": 13, "123": 20, "100": 5}}'::jsonb
            --      ]
            --) q1;


-- Summing any numeric values for same keys in jsonb rows

create or replace function jsonb_value_agg_sum
(
    jsonb_1 jsonb,
    jsonb_2 jsonb
)
returns jsonb
language sql
immutable
parallel safe
as
$$
    select jsonb_object_agg("key", "sum")
    from (
            select "key", sum("value"::numeric) as "sum"
            from (
                    select *
                    from jsonb_each(jsonb_1)
                    union all
                    select *
                    from jsonb_each(jsonb_2)
                 ) q1
            group by "key"
         ) q2;
$$;

create or replace aggregate jsonb_value_sum(jsonb)
(
  sfunc = jsonb_value_agg_sum,
  stype = jsonb,
  initcond = '{}',
  parallel = safe
);
comment on aggregate jsonb_value_sum(jsonb) is 'Summing any numeric values for same keys in jsonb rows';


-- Average of any numeric values for same keys in jsonb rows

create or replace function jsonb_value_agg_avg
(
    jsonb_1 jsonb,
    jsonb_2 jsonb
)
returns jsonb
language sql
immutable
parallel safe
as
$$
    select jsonb_object_agg("key", "avg")
    from (
            select "key", avg("value"::numeric) as "avg"
            from (
                    select *
                    from jsonb_each(jsonb_1)
                    union all
                    select *
                    from jsonb_each(jsonb_2)
                 ) q1
            group by "key"
         ) q2;
$$;

create or replace aggregate jsonb_value_avg(jsonb)
(
  sfunc = jsonb_value_agg_avg,
  stype = jsonb,
  initcond = '{}',
  parallel = safe
);
comment on aggregate jsonb_value_avg(jsonb) is 'Average of any numeric values for same keys in jsonb rows';


-- Max value from any numeric values for same keys in jsonb rows

create or replace function jsonb_value_agg_max_num
(
    jsonb_1 jsonb,
    jsonb_2 jsonb
)
returns jsonb
language sql
immutable
parallel safe
as
$$
    select jsonb_object_agg("key", "max")
    from (
            select "key", max("value"::numeric) as "max"
            from (
                    select *
                    from jsonb_each(jsonb_1)
                    union all
                    select *
                    from jsonb_each(jsonb_2)
                 ) q1
            group by "key"
         ) q2;
$$;

create or replace aggregate jsonb_value_max_num(jsonb)
(
  sfunc = jsonb_value_agg_max_num,
  stype = jsonb,
  initcond = '{}',
  parallel = safe
);
comment on aggregate jsonb_value_max_num(jsonb) is 'Max value from any numeric values for same keys in jsonb rows';


-- Max value from any text values for same keys in jsonb rows

create or replace function jsonb_value_agg_max_txt
(
    jsonb_1 jsonb,
    jsonb_2 jsonb
)
returns jsonb
language sql
immutable
parallel safe
as
$$
    select jsonb_object_agg("key", "max")
    from (
            select "key", max(trim("value"::text, '"')) as "max"
            from (
                    select *
                    from jsonb_each(jsonb_1)
                    union all
                    select *
                    from jsonb_each(jsonb_2)
                 ) q1
            group by "key"
         ) q2;
$$;

create or replace aggregate jsonb_value_max_txt(jsonb)
(
  sfunc = jsonb_value_agg_max_txt,
  stype = jsonb,
  initcond = '{}',
  parallel = safe
);
comment on aggregate jsonb_value_max_txt(jsonb) is 'Max value from any text values for same keys in jsonb rows';


-- Min value from any numeric values for same keys in jsonb rows

create or replace function jsonb_value_agg_min_num
(
    jsonb_1 jsonb,
    jsonb_2 jsonb
)
returns jsonb
language sql
immutable
parallel safe
as
$$
    select jsonb_object_agg("key", "min")
    from (
            select "key", min("value"::numeric) as "min"
            from (
                    select *
                    from jsonb_each(jsonb_1)
                    union all
                    select *
                    from jsonb_each(jsonb_2)
                 ) q1
            group by "key"
         ) q2;
$$;

create or replace aggregate jsonb_value_min_num(jsonb)
(
  sfunc = jsonb_value_agg_min_num,
  stype = jsonb,
  initcond = '{}',
  parallel = safe
);
comment on aggregate jsonb_value_min_num(jsonb) is 'Min value from any numeric values for same keys in jsonb rows';


-- Min value from any text values for same keys in jsonb rows

create or replace function jsonb_value_agg_min_txt
(
    jsonb_1 jsonb,
    jsonb_2 jsonb
)
returns jsonb
language sql
immutable
parallel safe
as
$$
    select jsonb_object_agg("key", "min")
    from (
            select "key", min(trim("value"::text, '"')) as "min"
            from (
                    select *
                    from jsonb_each(jsonb_1)
                    union all
                    select *
                    from jsonb_each(jsonb_2)
                 ) q1
            group by "key"
         ) q2;
$$;

create or replace aggregate jsonb_value_min_txt(jsonb)
(
  sfunc = jsonb_value_agg_min_txt,
  stype = jsonb,
  initcond = '{}',
  parallel = safe
);
comment on aggregate jsonb_value_min_txt(jsonb) is 'Min value from any text values for same keys in jsonb rows';