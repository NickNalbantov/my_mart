create table if not exists pub.gigs_text
(
    gig_id int primary key references public.gigs (id),
    summary text,
    description text
);

create table if not exists pub.sellers_text
(
    seller_id int primary key references public.sellers (id),
    summary text,
    description text
);

create table if not exists pub.categories_text
(
    category_id int primary key,
    summary text,
    description text
);

create table if not exists pub.subcategories_text
(
    sub_category_id int primary key,
    summary text,
    description text
);

create table if not exists pub.nested_subcategories_text
(
    nested_sub_category_id int primary key,
    summary text,
    description text
);

create table if not exists pub.keywords
(
    id int primary key generated always as identity,
    category_id int,
    sub_category_id int,
    nested_sub_category_id int,
    "position" int,
    request_count int,
    sayjam_gigs int,
    free_traffic int,
    click_price numeric,
    keyword_complexity numeric,
    keyword text not null,
    keyword_group text,
    competition_level text,
    url text,
    constraint keyword_cat_ids_unique unique (category_id, sub_category_id, nested_sub_category_id, keyword),
    constraint cat_ids_not_null check (coalesce(category_id, sub_category_id, nested_sub_category_id) is not null)
);

insert into pub.keywords
(
    category_id,
    sub_category_id,
    nested_sub_category_id,
    "position",
    request_count,
    sayjam_gigs,
    free_traffic,
    click_price,
    keyword_complexity,
    keyword,
    keyword_group,
    competition_level,
    url
)
    select
        category_id,
        sub_category_id,
        null::int as nested_sub_category_id,
        case
            when pg_input_is_valid("position", 'integer') is true then "position"::integer
            else null::integer
        end as "position",
        request_count,
        sayjam_gigs,
        free_traffc as free_traffic,
        case
            when pg_input_is_valid(ltrim(click_price, '$'), 'numeric') is true then ltrim(click_price, '$')::numeric
            else null::numeric
        end as click_price,
        keyword_complexity,
        keyword,
        keyword_group,
        competition_level,
        url
    from pub.keywords_google;

create index if not exists idx_category_id on pub.keywords (category_id);
create index if not exists idx_sub_category_id on pub.keywords (sub_category_id);
create index if not exists idx_nested_sub_category_id on pub.keywords (nested_sub_category_id);
create index if not exists idx_trg_keyword on pub.keywords using gin (keyword gin_trgm_ops);

cluster pub.keywords using keywords_pkey;

alter table if exists pub.keywords
    replica identity using index keywords_pkey;

analyze pub.keywords;