create or replace function public.get_range_partitions_ddl
(
    root_table_oid oid,
    split_point text,
    split_interval text,
    merge_windows text[],
    merge_intervals text[],
    split_amount smallint default 2,
    archive_partition_flg boolean default false,
    archive_point text default null,
    ddl_partition_of_flg boolean default true,
    split_unbounded_flg boolean default false,
    default_partition_flg boolean default false,
    partitions_schema text default null,
    partitions_name_prefix text default 'rng_prt',
    trim_datetime_zeroes_flg boolean default true,
    local_partitions_options jsonb default null,
    foreign_partitions_options jsonb default null
)
returns table
(
    window_num smallint,
    partition_num smallint,
    partition_key_expr text,
    partition_key_type text,
    value_from text,
    value_to text,
    partition_name text,
    partition_table_type text,
    tablespace_name text,
    local_tab_options jsonb,
    server_name text,
    foreign_tab_options jsonb,
    partition_ddl text
)
language plpgsql
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
    declare
        not_null_clns_check_rec record;
        keys_check_qry text;

        partition_key_config_rec record;
        partition_key text;
        partition_key_type_oid oid;
        partition_key_types_rec record;
        arr_for_checks text[];
        input_error_rec record;

        is_archive_point_valid boolean;
        gap_last_value_to text;

        merge_comparison_qry text := '';
        invalid_merge_config_rec record;

        def_tablespace text;
        default_area text;
        ddl_rec record;
        ddl_qry text;
        ddl_options text;

    begin

    /*

    Prerequisites:
        - Root table exists
        - Root table already partitioned by range

    Limitations:
        - Partitioning by a single column / expression
        - Partition key type must support '-', '+' operators with itself / a corresponding interval type (e. g. int + int, date + interval, inet + bigint etc.)
            - Two valid cases are excluded for a better consistency: date +/- integer and intX +/- intY where X > Y (e. g. int8 + int4, int8 + int2, int4 + int2)
        - Up to 999 elements in "merge_windows" array
            - That's a soft limit, can be changed in the code (see window_num = 1000 for a first 'future partition')
        - Up to 999 future partitions ("split_amount" argument)
            - That's a soft limit, can be changed in the code (see window_num = 2000 for maxvalue partition)

    Combinations of arguments:
        - If "archive_partition_flg" is false then 'past' partitions' DDL will be generated for all periods from "split_point" backwards all the way down to the lower bound from "merge_windows"
        - If "archive_partition_flg" is true but "archive_point" is null or empty ('') then 'past' partitions' DDL will be generated for all periods from "split_point" backwards all the way down to MINVALUE
            - Note that there'll also be an archive partition and its upper bound (FROM (MINVALUE) to ({upper_bound})) will be dependent on "split_point" value and "merge_windows" / "merge_intervals" arrays
        - If "archive_partition_flg" is true and "archive_point" is not null and not empty ('') then 'past' partitions' DDL will be generated for all periods from "archive_point" forwards all the way up to "split_point"
            - Note that there'll also be an archive partition and its upper bound (FROM (MINVALUE) to ("archive_point")) will NOT be dependent on "split_point" value and "merge_windows" / "merge_intervals" arrays
            - In this scenario there is a possibility to create a gap between last partition in last "merge_window" and "split_point" if there wont'be enough intervals to fill the difference between "archive_point" and "split_point"
                Exception with a corresponding message will be thrown

    Examples and JSON schemas for JSONB arguments are listed below

    Function call examples:

        1. Timestamp-based window ranges + future split + archive + default

            create table public.test (id int4, created_at timestamp)
            partition by range (created_at);

            select *
            from public.get_range_partitions_ddl
                (
                    root_table_oid := 'public.test'::regclass,
                    split_point := '2025-01-01',
                    split_interval := '1 month',
                    merge_windows := array['6 month', '6 month', '3 year'],
                    merge_intervals := array['1 month', '3 month', '1 year'],
                    archive_partition_flg := true,
                    archive_point := '2021-01-01',
                    split_unbounded_flg := true,
                    default_partition_flg := true
                );

        2. Partitions for 12 months

            create table public.test2 (id int4, created_at timestamp)
            partition by range (extract(month from (created_at)));

            select *
            from public.get_range_partitions_ddl
            (
                root_table_oid := 'public.test2'::regclass,
                split_point := '12',
                split_interval := '0',
                merge_windows := array['11'],
                merge_intervals := array['1'],
                archive_partition_flg := false,
                split_unbounded_flg := false,
                default_partition_flg := false
            );

    */

        -- Input validation

            -- Checking nulls in main arguments
        if
            root_table_oid is null
            or nullif(split_point, '') is null
            or nullif(split_interval, '') is null
            or merge_windows is null
            or merge_intervals is null
            or split_amount is null
            or ddl_partition_of_flg is null
            or archive_partition_flg is null
            or split_unbounded_flg is null
            or default_partition_flg is null
            or trim_datetime_zeroes_flg is null
        then
            raise exception '%',
                'Nulls are allowed only in "archive_point", "partitions_schema", "partitions_name_prefix", '
                    || '"local_partitions_options" and "foreign_partitions_options" arguments'
                using errcode = 'invalid_parameter_value';
        end if;

            -- Checking split_amount value
        if split_amount not between 0 and 999 then
            raise exception '%',
                'Argument "split_amount" value must be >= 0 and <= 999'
                using errcode = 'invalid_parameter_value';
        end if;

            -- Root table exists and partitioning can be applied

        select
            partattrs,
            pg_catalog.pg_get_expr(partexprs, partrelid) as part_expr
        into partition_key_config_rec
        from pg_catalog.pg_partitioned_table
        where
            partrelid = root_table_oid
            and partstrat = 'r';

        if not found then
            raise exception '%',
                'Root table ' || root_table_oid::regclass::text || ' does not exist or not partitioned by range'
                using errcode = 'invalid_parameter_value';
        end if;

        if cardinality(partition_key_config_rec.partattrs) != 1 then
            raise exception '%',
                'Root table ' || root_table_oid::regclass::text || ' partitioned by multiple colums - '
                    || 'splitting into intervals is supported only for single key partitioning'
                using errcode = 'invalid_parameter_value';
        end if;

            -- Checking input arrays lengths

        if cardinality(merge_windows) != cardinality(merge_intervals) or cardinality(merge_windows) not between 1 and 999 then
            raise exception '%',
                'Input arrays "merge_windows", "merge_intervals" must be of the same length (>= 1 and <= 999)'
                using errcode = 'invalid_parameter_value';
        end if;

            -- Checking partition key data type

        if partition_key_config_rec.part_expr is not null then

            -- If partition key is valid then trying to obtain its data type - it's not straightforward because key can be a SQL expression of any type
                -- Easiest way to check the type of partition key expression - insert 1 row into copy of root table and get pg_typeof

            drop table if exists pg_temp.nullable_root cascade;

            execute format($fmt$
                    create temporary table if not exists pg_temp.nullable_root
                    (like %s excluding all)
                    on commit drop
                $fmt$,
                root_table_oid::regclass::text
            );

            <<drop_not_null_loop>>
            for not_null_clns_check_rec in
                select
                    attname
                from pg_catalog.pg_attribute
                where
                    attrelid = 'nullable_root'::regclass
                    and attnotnull is true
                    and attnum > 0
                    and attisdropped is false
            loop
                execute format($fmt$
                        alter table if exists pg_temp.nullable_root alter column %I drop not null
                    $fmt$,
                    not_null_clns_check_rec.attname
                );
            end loop drop_not_null_loop;

            insert into pg_temp.nullable_root
            default values;

            keys_check_qry := format($fmt$
                    select pg_typeof(%s)::regtype::oid
                    from pg_temp.nullable_root
                    limit 1
                $fmt$,
                partition_key_config_rec.part_expr
            );

            execute keys_check_qry
            into partition_key_type_oid;

            partition_key := partition_key_config_rec.part_expr;

        else

            -- If partition key is a simple column reference

            select
                atttypid,
                attname
            into
                partition_key_type_oid,
                partition_key
            from pg_catalog.pg_attribute
            where
                attrelid = root_table_oid
                and attnum = partition_key_config_rec.partattrs[0]
                and attnum > 0
                and attisdropped is false;

        end if;

            -- Checking if partition key has a type suitable for an interval splitting
                -- Partition key type must have btree opclass (PG requirements) and must support '+' and '-' operators (required by this function for splitting intervals)
                    -- There are some special cases like date +/- interval, inet +/- bigint etc.
                    -- Two valid cases are excluded for a better consistency: date +/- integer and intX +/- intY where X > Y (e. g. int8 + int4, int8 + int2, int4 + int2)

        select distinct
            t."oid"::regtype::text as type_name,
            dp.oprright::regtype::text as interval_name
        into partition_key_types_rec
        from pg_catalog.pg_type t
        join pg_catalog.pg_opclass o
            on t."oid" = o.opcintype
        join pg_catalog.pg_am a
            on a."oid" = o.opcmethod
        join pg_catalog.pg_operator dm
            on dm.oprleft = o.opcintype
            and dm.oprright in (o.opcintype, 'interval'::regtype, 'int8'::regtype)
            and dm.oprresult in (o.opcintype, 'timestamp'::regtype)
        join pg_catalog.pg_operator dp
            on dp.oprleft = o.opcintype
            and dp.oprright in (o.opcintype, 'interval'::regtype, 'int8'::regtype)
            and dp.oprresult in (o.opcintype, 'timestamp'::regtype)
        where
            t."oid" = partition_key_type_oid
            and t.typtype != 'p'
            and a.amname = 'btree'
            and dm.oprname = '-'
            and dp.oprname = '+';

        if not found then
            raise exception '%',
                'Partition key must have a type with btree opclass (PG requirements) and with support of ''+'' and ''-'''
                    || 'operators (required by this function for splitting intervals)'
                using errcode = 'invalid_parameter_value';
        end if;

            -- Checking input parameters real data types
                -- All subelements of input partitioning config arrays must be eligible for casting into data type of the partition key

        arr_for_checks := array_cat(array_cat(array[split_point, archive_point, split_interval], merge_windows), merge_intervals);

        <<arr_checks_loop>>
        for arr in 1..cardinality(arr_for_checks) loop

            select
                message,
                detail,
                hint,
                sql_error_code
            into input_error_rec
            from pg_input_error_info -- checking if config values could be cast to partition keys' data types
                (
                    arr_for_checks[arr],
                    case
                        when arr <= 2 -- split_point and archive_point are partition key values, not an interval of the partition key
                            then partition_key_types_rec.type_name
                        else partition_key_types_rec.interval_name -- other parameters must be castable to partition key interval's type
                    end
                );

            if input_error_rec.sql_error_code is not null then
                raise exception using
                    message = coalesce(input_error_rec.message, 'No message provided'),
                    detail = coalesce(input_error_rec.detail, 'No detail provided'),
                    hint = coalesce(input_error_rec.hint, 'No hint provided'),
                    errcode = input_error_rec.sql_error_code;
            end if;

        end loop arr_checks_loop;

            -- Checking merge_windows and merge_intervals logic - necessary for making non-overlapping partitions
                -- merge_windows and merge_intervals must be sorted in ascending order by an interval value from split_point
                -- merge_interval can't be bigger than its merge_window
                -- multicolumn partition keys must be compared as rows

        <<merge_arr_loop_2>>
            for m in 1..cardinality(merge_windows) loop

                merge_comparison_qry := merge_comparison_qry || repeat(E'\n', 2)
                    || 'union all' || repeat(E'\n', 2)
                    || 'select' || E'\n\t' || cardinality(merge_windows) - m + 1 || '::smallint as window_num,' || E'\n\t'
                    || quote_literal(merge_windows[m]) || '::' || partition_key_types_rec.interval_name || ' as m_w,' || E'\n\t'
                    || quote_literal(merge_intervals[m]) || '::' || partition_key_types_rec.interval_name || ' as m_i';

        end loop merge_arr_loop_2;

        drop table if exists pg_temp.merge_config cascade;

        execute format($fmt$
                create temporary table if not exists pg_temp.merge_config
                    on commit drop
                as
                    select
                        window_num,
                        m_w,
                        m_i,
                        sum(m_w) over (order by window_num rows between unbounded preceding and current row exclude current row) as sum_m_w,
                        lead(m_w) over w as lead_m_w,
                        lead(m_i) over w as lead_m_i
                    from
                    (
                        %s
                    ) a
                    window w as (order by window_num)
            $fmt$,
            ltrim(merge_comparison_qry, repeat(E'\n', 2) || 'union all' || repeat(E'\n', 2))
        );

        select
            count(*) filter (where lead_m_w is not null and lead_m_w > m_w) as cnt_invalid_mw_order,
            count(*) filter (where lead_m_i is not null and lead_m_i > m_i) as cnt_invalid_mi_order,
            count(*) filter (where m_w < m_i) as cnt_invalid_mi_mw_relations,
            count(*) filter (where m_w is null) as cnt_null_m_w,
            count(*) filter (where m_i is null) as cnt_null_m_i
        into invalid_merge_config_rec
        from pg_temp.merge_config;

        if invalid_merge_config_rec.cnt_invalid_mw_order > 0 then
            raise exception '%',
                'Invalid "merge_windows" argument - each value in array must be less than previous value'
                using errcode = 'invalid_parameter_value';
        end if;

        if invalid_merge_config_rec.cnt_invalid_mi_order > 0 then
            raise exception '%',
                'Invalid "merge_intervals" argument - each value in array must be less than previous value'
                using errcode = 'invalid_parameter_value';
        end if;

        if invalid_merge_config_rec.cnt_invalid_mi_mw_relations > 0 then
            raise exception '%',
                'Invalid "merge_windows" argument - each value in array must be greater or equal to corresponding value in merge_intervals array'
                using errcode = 'invalid_parameter_value';
        end if;

        if invalid_merge_config_rec.cnt_null_m_w > 0 then
            raise exception '%',
                'Invalid "merge_windows" argument - no null values allowed'
                using errcode = 'invalid_parameter_value';
        end if;

        if invalid_merge_config_rec.cnt_null_m_i > 0 then
            raise exception '%',
                'Invalid "merge_intervals" argument - no null values allowed'
                using errcode = 'invalid_parameter_value';
        end if;

        if archive_partition_flg is true and nullif(archive_point, '') is not null then
            execute format($fmt$
                    select
                        %1$L::%2$s <= (%3$L::%2$s - m_w::%4$s)::%2$s
                    from pg_temp.merge_config
                    where
                        window_num = 1
                $fmt$,
                archive_point,
                partition_key_types_rec.type_name,
                split_point,
                partition_key_types_rec.interval_name
            )
            into is_archive_point_valid;

            if is_archive_point_valid is false then
                raise exception '%',
                    'Value of "archive_point" argument must be less or equal then "split_point" value minus last value in "merge_windows" array'
                    using errcode = 'invalid_parameter_value';
            end if;
        end if;


        -- All checks completed, finally building range intervals table

        drop table if exists pg_temp.partitions_ranges cascade;

        execute format($fmt$
                create temporary table if not exists pg_temp.partitions_ranges
                    on commit drop
                as
                    with recursive

                    past_intervals as
                    (
                        select
                            window_num,
                            1::smallint as partition_num,
                            case
                                when %6$L is false or (%6$L is true and nullif(%7$L, '') is null) -- no archive partition or archive_point is not specified
                                    then (%1$L::%2$s - m_w::%3$s)::%2$s
                                when %6$L is true and nullif(%7$L, '') is not null -- archive partition is needed and archive_point is specified
                                    then
                                        case
                                            when window_num = 1
                                                then %7$L::%2$s
                                            else (%7$L::%2$s + sum_m_w)::%2$s
                                        end
                            end as value_from,
                            case
                                when %6$L is false or (%6$L is true and nullif(%7$L, '') is null) -- no archive partition or archive_point is not specified
                                    then least((%1$L::%2$s - m_w::%3$s + m_i::%3$s)::%2$s, (%1$L::%2$s - lead_m_w)::%2$s, %1$L::%2$s)
                                when %6$L is true and nullif(%7$L, '') is not null -- archive partition is needed and archive_point is specified
                                    then
                                        case
                                            when window_num = 1
                                                then (%7$L::%2$s + m_i::%3$s)::%2$s
                                            else least((%7$L::%2$s + sum_m_w + m_i::%3$s)::%2$s, (%7$L::%2$s + sum_m_w + m_w)::%2$s, %1$L::%2$s)
                                        end
                            end as value_to,
                            sum_m_w,
                            lead_m_w,
                            m_w,
                            m_i
                        from pg_temp.merge_config

                        union all

                        select
                            window_num,
                            (partition_num + 1)::smallint as partition_num,
                            (value_from + m_i::%3$s)::%2$s as value_from,
                            case
                                when %6$L is false or (%6$L is true and nullif(%7$L, '') is null) -- no archive partition or archive_point is not specified
                                    then least((value_to + m_i::%3$s)::%2$s, (%1$L::%2$s - lead_m_w)::%2$s, %1$L::%2$s)
                                when %6$L is true and nullif(%7$L, '') is not null -- archive partition is needed and archive_point is specified
                                    then least((value_to + m_i::%3$s)::%2$s, (%7$L::%2$s + sum_m_w + m_w)::%2$s, %1$L::%2$s)
                            end as value_to,
                            sum_m_w,
                            lead_m_w,
                            m_w,
                            m_i
                        from past_intervals
                        where
                            (value_from + m_i::%3$s)::%2$s
                            <
                            case
                                when %6$L is false or (%6$L is true and nullif(%7$L, '') is null) -- no archive partition or archive_point is not specified
                                    then coalesce((%1$L::%2$s - lead_m_w)::%2$s, %1$L::%2$s)
                                when %6$L is true and nullif(%7$L, '') is not null -- archive partition is needed and archive_point is specified
                                    then least(
                                            case
                                                when window_num = 1
                                                    then (%7$L::%2$s + m_w)::%2$s
                                                else (%7$L::%2$s + sum_m_w + m_w)::%2$s
                                            end,
                                            %1$L::%2$s
                                        )
                            end
                    ),

                    future_intervals_recursive as -- to prevent 42P19 error
                    (
                        select
                            1000 as window_num,
                            %4$L::smallint as partition_num,
                            greatest((%1$L::%2$s + (%4$L::smallint - 1) * %5$L::%3$s)::%2$s, %1$L::%2$s) as value_from, -- latest future partition from split_amount - 1 to split_amount
                            (%1$L::%2$s + %4$L::smallint * %5$L::%3$s)::%2$s as value_to

                        union all

                        select
                            window_num,
                            (partition_num - 1)::smallint as partition_num,
                            greatest((%1$L::%2$s + (partition_num - 2)::smallint * %5$L::%3$s)::%2$s, %1$L::%2$s) as value_from,
                            (%1$L::%2$s + (partition_num - 1)::smallint * %5$L::%3$s)::%2$s as value_to
                        from future_intervals_recursive
                        where
                            partition_num >= 1
                    ),

                    future_intervals as
                    (
                        select
                            window_num,
                            partition_num,
                            value_from,
                            value_to
                        from future_intervals_recursive

                        union all

                        select
                            2000 as window_num, -- unbounded maxvalue partition
                            %4$L::smallint as partition_num,
                            (%1$L::%2$s + %4$L::smallint * %5$L::%3$s)::%2$s as value_from, -- latest future partition from split_amount
                            null::%2$s as value_to
                    ),

                    other_intervals as
                    (
                        select
                            0::smallint as window_num, -- archive partition (unbounded minvalue)
                            1::smallint as partition_num,
                            null::%2$s as value_from,
                            case
                                when %6$L is false or (%6$L is true and nullif(%7$L, '') is null) -- no archive partition or archive_point is not specified
                                    then (%1$L::%2$s - m_w::%3$s)::%2$s
                                when %6$L is true and nullif(%7$L, '') is not null -- archive partition is needed and archive_point is specified
                                    then %7$L::%2$s
                            end as value_to
                        from pg_temp.merge_config
                        where
                            window_num = 1

                        union all

                        select
                            -1::smallint as window_num, -- default partition
                            1::smallint as partition_num,
                            null::%2$s as value_from,
                            null::%2$s as value_to
                    )

                    select
                        window_num,
                        partition_num,
                        value_from,
                        value_to
                    from
                    (
                        select
                            window_num,
                            partition_num,
                            value_from,
                            value_to,
                            max(value_to) over (order by window_num, partition_num rows between unbounded preceding and current row exclude current row) as max_lag_value_to
                        from
                        (
                            select
                                window_num,
                                partition_num,
                                value_from,
                                value_to
                            from past_intervals
                            where
                                value_from < value_to
                                or window_num in (-1, 0, 2000)

                            union all

                            select
                                window_num,
                                partition_num,
                                value_from,
                                value_to
                            from future_intervals
                            where
                                value_from < value_to
                                or window_num in (-1, 0, 2000)

                            union all

                            select
                                window_num,
                                partition_num,
                                value_from,
                                value_to
                            from other_intervals
                        ) a
                    ) b
                    where
                        value_from >= max_lag_value_to
                        or window_num <= 0
            $fmt$,
            split_point,
            partition_key_types_rec.type_name,
            partition_key_types_rec.interval_name,
            split_amount,
            split_interval,
            archive_partition_flg,
            archive_point
        );

            -- Raising warning if there's a gap before first partition after split_point

        if archive_partition_flg is true and nullif(archive_point, '') is not null then

            with past as
            (
                select
                    a.value_to
                from
                (
                    select distinct
                        r.partition_num,
                        max(r.partition_num) over () as max_partition_num,
                        r.value_to
                    from pg_temp.partitions_ranges r
                    where
                        r.window_num = cardinality(merge_windows)
                ) a
                where
                    a.partition_num = max_partition_num
            ),

            future as
            (
                select
                    r.value_from
                from pg_temp.partitions_ranges r
                where
                    r.window_num = 1000
                    and r.partition_num = 1
            )

            select
                p.value_to::text
            into gap_last_value_to
            from future f
            join past p on f.value_from != p.value_to;

            if found then
                raise exception '%',
                    'There is a gap in partitions'' bounds before "split_point": max previous upper bound = ' || gap_last_value_to
                        || ', "split_point" = ' || split_point || E'\n'
                        || 'Higher values in "merge_windows" and "merge_intervals" arrays must be set '
                        || 'in order to fill the difference between "archive_point" and "split_point"'
                    using errcode = 'invalid_parameter_value';
            end if;

        end if;


        -- Parsing local partitions config

        /*
            Argument "local_partitions_options" config example:
                - First 2 future partitions (window_num = 1000) set with fillfactor = 70
                - archive partition (window_num = 0, partition_num = 1) to tablespace "hdd"

                [
                    {
                        "window_num": 1000, "partitions":
                            [
                                {"partition_num": 1, "options": {"fillfactor": "70"}},
                                {"partition_num": 2, "options": {"fillfactor": "70"}}
                            ]
                    },
                    {
                        "window_num": 0, "partitions":
                            [
                                {"partition_num": 1, "tablespace_name": "hdd"}
                            ]
                    }
                ]

            ---

            Argument "local_partitions_options" JSON schema:

                {
                    "type": "array",
                    "minItems": 1,
                    "uniqueItems": true,
                    "unevaluatedItems": false,
                    "items":
                        {
                            "type": "object",
                            "minProperties": 2,
                            "maxProperties": 2,
                            "properties":
                                {
                                    "window_num":
                                        {
                                            "type": "integer",
                                            "minimum": -1,
                                            "maximum": 2000
                                        },
                                    "partitions":
                                        {
                                            "type": "array",
                                            "uniqueItems": true,
                                            "unevaluatedItems": false,
                                            "minItems": 1,
                                            "items":
                                            {
                                                "type": "object",
                                                "minProperties": 2,
                                                "maxProperties": 3,
                                                "properties":
                                                    {
                                                        "partition_num":
                                                            {
                                                                "type": "integer",
                                                                "minimum": 1,
                                                                "maximum": 999
                                                            }
                                                    },
                                                "additionalProperties":
                                                    {
                                                        "tablespace_name":
                                                            {
                                                                "type": "string"
                                                            },
                                                        "options":
                                                            {
                                                                "type": "object",
                                                                "minProperties": 1
                                                            }
                                                    },
                                                "required":
                                                    [
                                                        "partition_num"
                                                    ]

                                            }
                                        }
                                },
                            "required":
                                [
                                    "window_num",
                                    "partitions"
                                ]
                        }
                }
        */

        drop table if exists pg_temp.local_partitions cascade;

        create temporary table if not exists pg_temp.local_partitions
            on commit drop
        as
            select
                w.window_num,
                p.partition_num,
                quote_ident(p.tablespace_name) as tablespace_name,
                jsonb_object_agg(o.opt, o.val) as local_tab_options_jsonb, -- jsonb_object_agg from jsonb_each_text is necessary to cast all values to strings
                string_agg(o.opt || ' = ' || o.val, ', ' order by o.opt) as local_tab_options_text
            from rows from
                (
                    jsonb_to_recordset(local_partitions_options) as ("partitions" jsonb, "window_num" smallint)
                ) as w (partitions, window_num)
            left join lateral rows from
                (
                    jsonb_to_recordset(w.partitions) as ("options" jsonb, "partition_num" smallint, "tablespace_name" text)
                ) as p (opts, partition_num, tablespace_name) on true
            left join lateral
                jsonb_each_text(p.opts) as o (opt, val) on true
            where
                w.window_num between -1 and 2000
                and p.partition_num between 1 and 999
            group by
                w.window_num,
                p.partition_num,
                p.tablespace_name;


        -- Getting default tablespace

        select
            quote_ident(t.spcname)
        into def_tablespace
        from pg_catalog.pg_database d
        join pg_catalog.pg_tablespace t
            on d.dattablespace = t."oid"
        where
            d.datname = current_database();


        -- Parsing foreign partitions config

        /*
            Argument "foreign_partitions_options" config example:
                - First and second partitions of the oldest merge_window (window_num = 1) to the remote_postgresql and remote_clickhouse servers foreign tables
                - Archive partition (window_num = 0, partition_num = 1) to CSV file in S3 bucket

                [
                    {
                        "window_num": 1, "partitions":
                            [
                                {"partition_num": 1, "server_name": "remote_postgresql", "options": {"schema_name": "some_remote_schema", "table_name": "some_table"}},
                                {"partition_num": 2, "server_name": "remote_clickhouse", "options": {"dbname": "test_database", "driver": "binary"}}
                            ]
                    },
                    {
                        "window_num": 0, "partitions":
                            [
                                {"partition_num": 1, "server_name": "s3_server", "options": {"bucket": "bucket_name","format": "CSV"}}
                            ]
                    }
                ]

            ---

            Argument "foreign_partitions_options" JSON schema:

                {
                    "type": "array",
                    "minItems": 1,
                    "uniqueItems": true,
                    "unevaluatedItems": false,
                    "items":
                        {
                            "type": "object",
                            "minProperties": 2,
                            "maxProperties": 2,
                            "properties":
                                {
                                    "window_num":
                                        {
                                            "type": "integer",
                                            "minimum": -1,
                                            "maximum": 2000
                                        },
                                    "partitions":
                                        {
                                            "type": "array",
                                            "uniqueItems": true,
                                            "unevaluatedItems": false,
                                            "minItems": 1,
                                            "items":
                                            {
                                                "type": "object",
                                                "minProperties": 2,
                                                "maxProperties": 3,
                                                "properties":
                                                    {
                                                        "partition_num":
                                                            {
                                                                "type": "integer",
                                                                "minimum": 1,
                                                                "maximum": 999
                                                            },
                                                        "server_name":
                                                            {
                                                                "type": "string"
                                                            }
                                                    },
                                                "additionalProperties":
                                                    {
                                                        "options":
                                                            {
                                                                "type": "object",
                                                                "minProperties": 1
                                                            }
                                                    },
                                                "required":
                                                    [
                                                        "partition_num",
                                                        "server_name"
                                                    ]

                                            }
                                        }
                                },
                            "required":
                                [
                                    "window_num",
                                    "partitions"
                                ]
                        }
                }
        */

        drop table if exists pg_temp.foreign_partitions cascade;

        create temporary table if not exists pg_temp.foreign_partitions
            on commit drop
        as
            select
                w.window_num,
                p.partition_num,
                quote_ident(p.server_name) as server_name,
                jsonb_object_agg(o.opt, o.val) as foreign_tab_options_jsonb, -- jsonb_object_agg from jsonb_each_text is necessary to cast all values to strings
                string_agg(o.opt || ' ''' || o.val || '''', ', ' order by o.opt) as foreign_tab_options_text
            from rows from
                (
                    jsonb_to_recordset(foreign_partitions_options) as ("partitions" jsonb, "window_num" smallint)
                ) as w (partitions, window_num)
            left join lateral rows from
                (
                    jsonb_to_recordset(w.partitions) as ("options" jsonb, "partition_num" smallint, "server_name" text)
                ) as p (opts, partition_num, server_name) on true
            left join lateral
                jsonb_each_text(p.opts) as o (opt, val) on true
            where
                w.window_num between -1 and 2000
                and p.partition_num between 1 and 999
                and p.server_name is not null
            group by
                w.window_num,
                p.partition_num,
                p.server_name;


        -- Generating DDLs from partitions_ranges table

            -- Final config table

        drop table if exists pg_temp.partitions_config cascade;

        create temporary table if not exists pg_temp.partitions_config
            on commit drop
        as
            select
                r.window_num,
                r.partition_num,
                r.value_from,
                r.value_to,
                case
                    when c.relpersistence = 't' and f.server_name is null then quote_ident('pg_temp')
                    else quote_ident(coalesce(partitions_schema, c.relnamespace::regnamespace::text))
                end || '.'
                    || quote_ident
                        (
                            substring
                            (
                                c.relname || '_'
                                    || coalesce(partitions_name_prefix, '') || '_'
                                    || replace
                                        (
                                            regexp_replace
                                            (
                                                case r.window_num
                                                    when -1 then 'default' -- default partition
                                                    else
                                                    (
                                                        case r.window_num
                                                            when 0 then 'minvalue' -- archive partition
                                                            else r.value_from::text
                                                        end
                                                        || '_' ||
                                                        case r.window_num
                                                            when 2000 then 'maxvalue' -- unbounded maxvalue partition
                                                            else r.value_to::text
                                                        end
                                                    )
                                                end,
                                                '([[:punct:]]|[[:space:]])',
                                                '_',
                                                'g'
                                            ),
                                            '_00',
                                            case when trim_datetime_zeroes_flg is true then '' else '_00' end
                                        ),
                                        1,
                                        63
                            )
                        )
                as partition_name, -- "public"."test_table_rng_prt_2024_01_01_2024_02_01"
                quote_ident(c.relnamespace::regnamespace::text)
                    || '.' || quote_ident(c.relname::text) as root_table,
                case
                    when c.relpersistence = 't' and f.server_name is null then 'temporary'
                    when c.relpersistence = 'u' and f.server_name is null then 'unlogged'
                    when f.server_name is not null then 'foreign'
                    else ''
                end as partition_table_type,
                coalesce(
                    l.tablespace_name,
                    case
                        when coalesce(f.server_name, f.foreign_tab_options_text) is null
                            then def_tablespace
                        else null::text
                    end
                ) as tablespace_name,
                l.local_tab_options_jsonb,
                l.local_tab_options_text,
                f.server_name,
                f.foreign_tab_options_jsonb,
                f.foreign_tab_options_text
            from pg_temp.partitions_ranges r
            cross join pg_catalog.pg_class c
            left join pg_temp.local_partitions l
                on l.window_num = r.window_num
                and l.partition_num = r.partition_num
            left join pg_temp.foreign_partitions f
                on f.window_num = r.window_num
                and f.partition_num = r.partition_num
            where
                c."oid" = root_table_oid
                and case
                        when default_partition_flg is false
                            then r.window_num != -1
                        else true
                    end
                and case
                        when split_unbounded_flg is false
                            then r.window_num != 2000
                        else true
                    end
                and case
                        when archive_partition_flg is false
                            then r.window_num != 0
                        else true
                    end;

        if default_partition_flg is true then
            select
                case
                    when archive_partition_flg is false
                        then partition_key || ' < ''' || min(c.value_to)::text || ''''
                    else ''
                end ||
                case
                    when archive_partition_flg is false and split_unbounded_flg is false
                        then ' or '
                    else ''
                end ||
                case
                    when split_unbounded_flg is false
                        then partition_key || ' >= ''' || max(c.value_to)::text || ''''
                    else ''
                end ||
                case
                    when archive_partition_flg is false or split_unbounded_flg is false
                        then ' or '
                    else ''
                end
                || partition_key || ' is null'
            into
                default_area
            from pg_temp.partitions_config c;
        end if;

            -- Looping through the config table

        <<ddl_loop>>
        for ddl_rec in
            select
                c.window_num,
                c.partition_num,
                c.value_from,
                c.value_to,
                c.partition_name,
                c.root_table,
                c.partition_table_type,
                c.tablespace_name,
                c.local_tab_options_jsonb,
                c.local_tab_options_text,
                c.server_name,
                c.foreign_tab_options_jsonb,
                c.foreign_tab_options_text
            from pg_temp.partitions_config c
            order by
                c.window_num,
                c.partition_num
        loop

            if
                coalesce(ddl_rec.tablespace_name, ddl_rec.local_tab_options_text) is not null
                and coalesce(ddl_rec.server_name, ddl_rec.foreign_tab_options_text) is not null
            then
                raise exception '%',
                    'Invalid xxx_partitions_options parameters - same partition ' || ddl_rec.partition_name
                        || ' specified both as local partition and foreign partition'
                    using errcode = 'invalid_parameter_value';
            end if;

            if
                coalesce(ddl_rec.server_name, ddl_rec.foreign_tab_options_text) = ddl_rec.foreign_tab_options_text
                or coalesce(ddl_rec.foreign_tab_options_text, ddl_rec.server_name) = ddl_rec.server_name
            then
                raise exception '%',
                    'Invalid foreign_partitions_options parameter for partition ' || ddl_rec.partition_name
                        || ' - both "server_name" and "options" must be specified'
                    using errcode = 'invalid_parameter_value';
            end if;

            ddl_qry := '';
            ddl_options := '' ||
                case
                    when ddl_rec.local_tab_options_text is not null
                        then E'\n' || 'with options (' || ddl_rec.local_tab_options_text || ')'
                    else '' end
                || case
                    when ddl_rec.tablespace_name is not null
                        then E'\n' || 'tablespace ' || ddl_rec.tablespace_name
                    else '' end
                || case
                    when ddl_rec.server_name is not null and ddl_rec.foreign_tab_options_text is not null
                        then E'\n' || 'server' || ddl_rec.server_name || E'\n' || 'options (' || ddl_rec.foreign_tab_options_text || ')'
                    else '' end;

                -- Generating DDLs as CREATE TABLE ... PARTITION OF root_table ...

            if ddl_partition_of_flg is true then

                ddl_qry := ddl_qry
                    || 'create '
                    || case when ddl_rec.partition_table_type != 'foreign' then '' else ddl_rec.partition_table_type || ' ' end
                    || 'table if not exists ' || ddl_rec.partition_name || E'\n'
                    || 'partition of ' || ddl_rec.root_table || E'\n\t'
                    || case
                        when ddl_rec.window_num = -1 -- default partition
                            then 'default'
                        else 'for values from ('
                            || case when ddl_rec.window_num = 0 then 'minvalue' else '''' || ddl_rec.value_from::text || '''' end -- minvalue for archive partition
                            || ') to ('
                            || case when ddl_rec.window_num = 2000 then 'maxvalue' else '''' || ddl_rec.value_to::text || '''' end || ')' -- maxvalue for unbounded split partition
                        end
                    || ddl_options
                    || ';';

                -- Generating DDLs as CREATE TABLE ... (LIKE root_table) / ... ATTACH PARTITION ...;

            else

                ddl_qry := ddl_qry
                    || 'create '
                    || case when ddl_rec.partition_table_type != 'foreign' then '' else ddl_rec.partition_table_type || ' ' end
                    || 'table if not exists ' || ddl_rec.partition_name || E'\n'
                    || '(like ' || ddl_rec.root_table || E' including defaults including constraints)'
                    || ddl_options
                    || ';' || repeat(E'\n', 2);

                ddl_qry := ddl_qry -- adding CHECK constraint for partition range
                    || 'alter '
                    || case when ddl_rec.partition_table_type != 'foreign' then '' else ddl_rec.partition_table_type || ' ' end
                    || 'table if exists only ' || ddl_rec.partition_name || E'\n\t'
                    || 'add constraint ' || quote_ident(substr('check_' || ddl_rec.partition_name, 1, 63))
                    || ' check ('
                    || case
                        when ddl_rec.window_num = 0 then partition_key || ' < ''' || ddl_rec.value_to || '''' -- minvalue for archive partition
                        when ddl_rec.window_num = 2000 then partition_key || ' > ''' || ddl_rec.value_from || '''' -- maxvalue for unbounded split partition
                        when ddl_rec.window_num = -1 then default_area -- default partition
                        else partition_key || ' >= ''' || ddl_rec.value_from || ''' and ' || partition_key || ' < ''' || ddl_rec.value_to || ''''
                    end
                    || ');' || repeat(E'\n', 2)
                    || '-- Add INSERT / COPY script here' || repeat(E'\n', 2);

                ddl_qry := ddl_qry -- attaching table as partition of root_table
                    || 'alter table if exists ' || ddl_rec.root_table  || E'\n\t'
                    || 'attach partition ' || ddl_rec.partition_name || E'\n\t'
                    || case
                        when ddl_rec.window_num = -1 -- default partition
                            then 'default'
                        else 'for values from ('
                            || case when ddl_rec.window_num = 0 then 'minvalue' else '''' || ddl_rec.value_from || ''''  end -- minvalue for archive partition
                            || ') to ('
                            || case when ddl_rec.window_num = 2000 then 'maxvalue' else '''' || ddl_rec.value_to || ''''  end -- maxvalue for unbounded split partition
                        end
                    || ');' || repeat(E'\n', 2);

                ddl_qry := ddl_qry -- removing redundant CHECK constraint
                    || 'alter '
                    || case when ddl_rec.partition_table_type != 'foreign' then '' else ddl_rec.partition_table_type || ' ' end
                    || 'table if exists only ' || ddl_rec.partition_name || E'\n\t'
                    || 'drop constraint if exists ' || quote_ident(substr('check_' || ddl_rec.partition_name, 1, 63))
                    || ' cascade;';

            end if;

                -- Result

            return query
                select
                    ddl_rec.window_num::smallint as window_num,
                    ddl_rec.partition_num::smallint as partition_num,
                    partition_key as partition_key_expr,
                    partition_key_types_rec.type_name as partition_key_type,
                    case
                        when ddl_rec.window_num = 0
                            then 'minvalue'::text
                        when ddl_rec.window_num = -1
                            then 'default'::text
                        else ddl_rec.value_from::text
                    end as value_from,
                    case
                        when ddl_rec.window_num = 2000
                            then 'maxvalue'::text
                        when ddl_rec.window_num = -1
                            then 'default'::text
                        else ddl_rec.value_to::text
                    end as value_to,
                    ddl_rec.partition_name::text as partition_name,
                    case
                        when ddl_rec.partition_table_type = '' then null::text
                        else partition_table_type::text
                    end as partition_table_type,
                    ddl_rec.tablespace_name::text as tablespace_name,
                    ddl_rec.local_tab_options_jsonb::jsonb as local_tab_options,
                    ddl_rec.server_name::text as server_name,
                    ddl_rec.foreign_tab_options_jsonb::jsonb as foreign_tab_options,
                    ddl_qry::text as partition_ddl;

        end loop ddl_loop;

        return;

    end;
$func$;