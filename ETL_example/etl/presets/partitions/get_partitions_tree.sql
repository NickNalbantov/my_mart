create or replace function public.get_partitions_tree
(
    root_table_oid oid
)
returns table
(
    lvl smallint,
    table_oid oid,
    schema_name text,
    table_name text,
    relkind text
)
language sql
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

        with recursive parts as
        (
            select
                0::smallint as lvl,
                c."oid" as table_oid,
                c.relnamespace::regnamespace::text as schema_name,
                c.relname::text as table_name,
                c.relkind::text as relkind
            from pg_catalog.pg_class c
            where
                c.relkind = 'p'
                and c.relispartition is false
                and c."oid" = root_table_oid

            union all

            select
                p.lvl + 1::smallint as lvl,
                inh.inhrelid as table_oid,
                c.relnamespace::regnamespace::text as schema_name,
                c.relname::text as table_name,
                c.relkind::text as relkind
            from parts p
            join pg_catalog.pg_inherits inh
                on inh.inhparent = p.table_oid
            join pg_catalog.pg_class c
                on inh.inhrelid = c."oid"
        )

        select *
        from parts;

$func$;