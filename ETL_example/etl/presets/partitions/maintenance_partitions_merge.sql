create or replace function public.maintenance_partitions_merge
(
    root_table_oid oid,
    split_point text,
    split_interval text,
    merge_windows text[],
    merge_intervals text[],
    split_amount smallint default 2,
    archive_partition_flg boolean default false,
    archive_point text default null,
    dry_run boolean default true,
    drop_detached_flg boolean default false,
    inactive_index_fillfactor_100_flg boolean default true,
    cluster_new_partitions_flg boolean default true,
    ddl_partition_of_flg boolean default true,
    split_unbounded_flg boolean default false,
    default_partition_flg boolean default false,
    partitions_schema text default null,
    partitions_name_prefix text default 'rng_prt',
    trim_datetime_zeroes_flg boolean default true,
    local_partitions_options jsonb default null,
    copy_local_options_flg boolean default true,
    foreign_partitions_options jsonb default null,
    copy_foreign_options_flg boolean default true,
    skip_rows_count_check_flg boolean default false
)
returns table
(
    partition_merge_type smallint,
    window_num smallint,
    partition_num smallint,
    partition_key_expr text,
    partition_key_type text,
    new_value_from text,
    new_value_to text,
    new_partition_name text,
    new_partition_table_type text,
    new_tablespace_name text,
    new_local_tab_options jsonb,
    new_server_name text,
    new_foreign_tab_options jsonb,
    new_partition_ddl text,
    cur_value_from text,
    cur_value_to text,
    cur_partition_name text,
    cur_partition_table_type text,
    cur_tablespace_name text,
    cur_local_tab_options jsonb,
    cur_server_name text,
    cur_foreign_tab_options jsonb,
    existing_dependencies_drop text,
    existing_dependencies_ddl text,
    detach_partition_flg boolean
)
language plpgsql
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
    declare
        is_root_valid boolean;
        def_tablespace text;
        partition_key_type_name text;

        detach_qry text := '';
        attach_qry text := '';
        dependencies_qry text := '';
        repartitioning_rec record;

        cur_partition_schema text;
        cur_partition_relname text;
        new_partition_schema text;
        new_partition_relname text;
        cur_partition_contained_flg boolean;

        detach_partition_flg_upd boolean;
        detach_cnt smallint;
        drop_detached_qry text;

        insert_qry text;
        insert_rec record;

        root_rows_cnt_before bigint;
        root_rows_cnt_after bigint;
        repartitioning_plan_count smallint;

        maintenance_rec record;
        fillfactor_qry text;
        fillfactor_rec record;
        cluster_qry text;
        cluster_rec record;

    begin

    /*

    Prerequisites:
        - Root table exists
        - Root table already partitioned by range

    Limitations:
        - Partitioning by a single column / expression
        - Partition key type must support '-', '+' operators with itself / a corresponding interval type (e. g. int + int, date + interval, inet + bigint etc.)
            - Two valid cases are excluded for a better consistency: date +/- integer and intX +/- intY where X > Y (e. g. int8 + int4, int8 + int2, int4 + int2)
        - Up to 999 elements in "merge_windows" array
            - That's a soft limit, can be changed in the code (see window_num = 1000 for a first 'future partition')
        - Up to 999 future partitions ("split_amount" argument)
            - That's a soft limit, can be changed in the code (see window_num = 2000 for maxvalue partition)

    Possible problems:
        - Insert from old regular / foreign partition table to new foreign partition table could fail
            - some FDWs may not support inserts
        - Be aware about recreating dependencies from old partitions to new partitions when partitions' bounds are not the same
            - Function automatically transfer dependencies only when (cur_value_from >= new_value_from and cur_value_to >= new_value_to)
            - In other cases any dependencies should be manually altered and executed from "existing_dependencies_ddl" column, if needed
        - If old partitions have some individual per-column settings (like n_distinct) - it will be lost

    This function is needed for:
        - Launching public.get_range_partitions_ddl function with a new partitioning schema design in order to adjust partitions in root table
            - Refer to description of public.get_range_partitions_ddl for more details
        - Checking any dependencies on root table or any existing partitions and any non-default options for local and foreign existing partitions

    "merge_windows" argument is an array of borders for intervals with a different partition granularity.
    Steps of this granularity for each value in "merge_windows" must be correspondingly set in "merge_intervals" argument

    "dry_run" argument value is set to True by default, so data juggling will start only if this flag will be explicitly set to False
    "drop_detached_flg" argument value is set to False by default, so any existing detached partitions won't be automatically dropped unless this flag will be explicitly set to True
    "inactive_index_fillfactor_100_flg" argument value is set to True by default, so all indices for partitions before split point will be altered with setting fillfactor = 100
    "cluster_new_partitions_flg" argument value is set to True by default, so all newly created / altered partitions will be clustered with the most suitable index (1. PK; 2. unique; 3. any other non-partial index)
    "skip_rows_count_check_flg" argument allows to commit repartitioning even if rows count in root table before repartitioning is distinct from rows count in root table after repartitioning

    Combinations of arguments:
        - If "archive_partition_flg" is false then 'past' partitions' DDL will be generated for all periods from "split_point" backwards all the way down to the lower bound from "merge_windows"
        - If "archive_partition_flg" is true but "archive_point" is null or empty ('') then 'past' partitions' DDL will be generated for all periods from "split_point" backwards all the way down to MINVALUE
            - Note that there'll also be an archive partition and its upper bound (FROM (MINVALUE) to ({upper_bound})) will be dependent on "split_point" value and "merge_windows" / "merge_intervals" arrays
        - If "archive_partition_flg" is true and "archive_point" is not null and not empty ('') then 'past' partitions' DDL will be generated for all periods from "archive_point" forwards all the way up to "split_point"
            - Note that there'll also be an archive partition and its upper bound (FROM (MINVALUE) to ("archive_point")) will NOT be dependent on "split_point" value and "merge_windows" / "merge_intervals" arrays
            - In this scenario there is a possibility to create a gap between last partition in last "merge_window" and "split_point" if there wont'be enough intervals to fill the difference between "archive_point" and "split_point"
                Exception with a corresponding message will be thrown

    Function call examples:

        1. Timestamp-based window ranges + future split + archive + default - create partitions then move split point one month ahead

            create table public.test (id int4, created_at timestamp)
            partition by range (created_at);

            select *
            from public.maintenance_partitions_merge
                (
                    root_table_oid := 'public.test'::regclass,
                    split_point := '2025-01-01',
                    split_interval := '1 month',
                    merge_windows := array['6 month', '6 month', '3 year'],
                    merge_intervals := array['1 month', '3 month', '1 year'],
                    archive_partition_flg := true,
                    archive_point := '2021-01-01',
                    split_unbounded_flg := true,
                    default_partition_flg := true
                );

            select *
            from public.maintenance_partitions_merge
                (
                    root_table_oid := 'public.test'::regclass,
                    split_point := '2025-02-01', -- + 1 month
                    split_interval := '1 month',
                    merge_windows := array['6 month', '7 month', '3 year'], -- + 1 month in a second window
                    merge_intervals := array['1 month', '3 month', '1 year'],
                    archive_partition_flg := true,
                    archive_point := '2021-01-01',
                    split_unbounded_flg := true,
                    default_partition_flg := true
                );

        2. Partitions for 12 months

            create table public.test2 (id int4, created_at timestamp)
            partition by range (extract(month from (created_at)));

            select *
            from public.maintenance_partitions_merge
            (
                root_table_oid := 'public.test2'::regclass,
                split_point := '12',
                split_interval := '0',
                merge_windows := array['11'],
                merge_intervals := array['1'],
                archive_partition_flg := false,
                split_unbounded_flg := false,
                default_partition_flg := false
            );

    If "copy_local_options_flg" argument is true then function will copy all table options and tablespace name from all current partitions to a corresponding new partitions
    Else "local_partitions_options" argument will be parsed and corresponding partitions will be rewritten with new settings
    Argument "local_partitions_options" config example:
        - First 2 future partitions (window_num = 1000) set with fillfactor = 70
        - archive partition (window_num = 0, partition_num = 1) to tablespace "hdd"

        [
            {
                "window_num": 1000, "partitions":
                    [
                        {"partition_num": 1, "options": {"fillfactor": "70"}},
                        {"partition_num": 2, "options": {"fillfactor": "70"}}
                    ]
            },
            {
                "window_num": 0, "partitions":
                    [
                        {"partition_num": 1, "tablespace_name": "hdd"}
                    ]
            }
        ]

    Argument "local_partitions_options" JSON schema:

        {
            "type": "array",
            "minItems": 1,
            "uniqueItems": true,
            "unevaluatedItems": false,
            "items":
                {
                    "type": "object",
                    "minProperties": 2,
                    "maxProperties": 2,
                    "properties":
                        {
                            "window_num":
                                {
                                    "type": "integer",
                                    "minimum": -1,
                                    "maximum": 2000
                                },
                            "partitions":
                                {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "unevaluatedItems": false,
                                    "minItems": 1,
                                    "items":
                                    {
                                        "type": "object",
                                        "minProperties": 2,
                                        "maxProperties": 3,
                                        "properties":
                                            {
                                                "partition_num":
                                                    {
                                                        "type": "integer",
                                                        "minimum": 1,
                                                        "maximum": 999
                                                    }
                                            },
                                        "additionalProperties":
                                            {
                                                "tablespace_name":
                                                    {
                                                        "type": "string"
                                                    },
                                                "options":
                                                    {
                                                        "type": "object",
                                                        "minProperties": 1
                                                    }
                                            },
                                        "required":
                                            [
                                                "partition_num"
                                            ]

                                    }
                                }
                        },
                    "required":
                        [
                            "window_num",
                            "partitions"
                        ]
                }
        }

    If "copy_foreign_options_flg" argument is true then function will copy all foreign table options and foreign server name from all current partitions to a corresponding new partition
    Else "foreign_partitions_options" argument will be parsed and corresponding partitions will be rewritten with new settings
    Argument "foreign_partitions_options" config example:
        - First and second partitions of the oldest merge_window (window_num = 1) to the remote_postgresql and remote_clickhouse servers foreign tables
        - Archive partition (window_num = 0, partition_num = 1) to CSV file in S3 bucket

        [
            {
                "window_num": 1, "partitions":
                    [
                        {"partition_num": 1, "server_name": "remote_postgresql", "options": {"schema_name": "some_remote_schema", "table_name": "some_table"}},
                        {"partition_num": 2, "server_name": "remote_clickhouse", "options": {"dbname": "test_database", "driver": "binary"}}
                    ]
            },
            {
                "window_num": 0, "partitions":
                    [
                        {"partition_num": 1, "server_name": "s3_server", "options": {"bucket": "bucket_name","format": "CSV"}}
                    ]
            }
        ]

    Argument "foreign_partitions_options" JSON schema:

        {
                "type": "array",
                "minItems": 1,
                "uniqueItems": true,
                "unevaluatedItems": false,
                "items":
                    {
                        "type": "object",
                        "minProperties": 2,
                        "maxProperties": 2,
                        "properties":
                            {
                                "window_num":
                                    {
                                        "type": "integer",
                                        "minimum": -1,
                                        "maximum": 2000
                                    },
                                "partitions":
                                    {
                                        "type": "array",
                                        "uniqueItems": true,
                                        "unevaluatedItems": false,
                                        "minItems": 1,
                                        "items":
                                        {
                                            "type": "object",
                                            "minProperties": 2,
                                            "maxProperties": 3,
                                            "properties":
                                                {
                                                    "partition_num":
                                                        {
                                                            "type": "integer",
                                                            "minimum": 1,
                                                            "maximum": 999
                                                        },
                                                    "server_name":
                                                        {
                                                            "type": "string"
                                                        }
                                                },
                                            "additionalProperties":
                                                {
                                                    "options":
                                                        {
                                                            "type": "object",
                                                            "minProperties": 1
                                                        }
                                                },
                                            "required":
                                                [
                                                    "partition_num",
                                                    "server_name"
                                                ]

                                        }
                                    }
                            },
                        "required":
                            [
                                "window_num",
                                "partitions"
                            ]
                    }
            }
    */

        -- Checking if root table is partitioned by range (see prerequisites and limitations specidied above)
            -- More complicated checks for partitioning key are used in public.get_range_partitions_ddl() later

        select
            true
        into is_root_valid
        from pg_catalog.pg_partitioned_table
        where
            partrelid = root_table_oid
            and partnatts = 1
            and partstrat = 'r';

        if is_root_valid is not true then
            raise exception '%',
                'Argument "root_table_oid" must be the OID of existing range-partitioned table with exactly one partitioning key / expression'
                using errcode = 'invalid_parameter_value';
        end if;


        -- Getting default tablespace

        select
            quote_ident(t.spcname)
        into def_tablespace
        from pg_catalog.pg_database d
        join pg_catalog.pg_tablespace t
            on d.dattablespace = t."oid"
        where
            d.datname = current_database();


        -- Calling public.get_range_partitions_ddl function that runs all checks and returns table with new partitioning config and DDLs

        drop table if exists pg_temp.partitions_compare cascade;

        create temporary table if not exists pg_temp.partitions_compare
            on commit drop
        as
            with recursive

            new_ddl as -- new partitioning schema
            (
                select
                    g.window_num,
                    g.partition_num,
                    g.partition_key_expr,
                    g.partition_key_type,
                    g.value_from,
                    g.value_to,
                    g.partition_name,
                    g.partition_table_type,
                    g.tablespace_name,
                    g.local_tab_options,
                    g.server_name,
                    g.foreign_tab_options,
                    g.partition_ddl
                from public.get_range_partitions_ddl
                    (
                        "root_table_oid" := root_table_oid,
                        "split_point" := split_point,
                        "split_interval" := split_interval,
                        "merge_windows" := merge_windows,
                        "merge_intervals" := merge_intervals,
                        "split_amount" := split_amount,
                        "archive_partition_flg" := archive_partition_flg,
                        "archive_point" := archive_point,
                        "ddl_partition_of_flg" := ddl_partition_of_flg,
                        "split_unbounded_flg" := split_unbounded_flg,
                        "default_partition_flg" := default_partition_flg,
                        "partitions_schema" := partitions_schema,
                        "partitions_name_prefix" := partitions_name_prefix,
                        "trim_datetime_zeroes_flg" := trim_datetime_zeroes_flg,
                        "local_partitions_options" := local_partitions_options,
                        "foreign_partitions_options" := foreign_partitions_options
                    ) g
            ),

            current_settings as -- current partitioning schema or NULLs
            (
                select
                    a.partition_oid,
                    case
                        when a.relpartbound = 'DEFAULT' then 'default'::text
                        when regexp_substr(a.relpartbound, '(?<=FROM \()(.*)(?=\) TO)') = 'MINVALUE' then 'minvalue'::text
                        else regexp_substr(a.relpartbound, '(?<=FROM \('')(.*)(?=''\) TO)')::text
                    end as value_from,
                    case
                        when a.relpartbound = 'DEFAULT' then 'default'::text
                        when regexp_substr(a.relpartbound, '(?<=TO \()(.*)(?=\))') = 'MAXVALUE' then 'maxvalue'::text
                        else regexp_substr(a.relpartbound, '(?<=TO \('')(.*)(?=''\))')::text
                    end as value_to,
                    a.partition_name::text as partition_name,
                    a.partition_table_type::text as partition_table_type,
                    a.tablespace_name::text as tablespace_name,
                    a.local_tab_options::jsonb as local_tab_options,
                    a.server_name::text as server_name,
                    a.foreign_tab_options::jsonb as foreign_tab_options
                from
                (
                    select
                        p."oid" as partition_oid,
                        pg_catalog.pg_get_expr(p.relpartbound, p."oid", true) as relpartbound,
                        quote_ident(p.relnamespace::regnamespace::text)
                            || '.' || quote_ident(p.relname::text) as partition_name,
                        case
                            when p.relkind = 'f' then 'foreign'
                            else
                                case p.relpersistence
                                    when 't' then 'temporary'
                                    when 'u' then 'unlogged'
                                    else null
                                end
                        end as partition_table_type,
                        coalesce(quote_ident(ts.spcname), def_tablespace) as tablespace_name,
                        jsonb_object_agg(lo.option_name, lo.option_value) filter (where lo.option_name is not null) over w as local_tab_options,
                        quote_ident(fs.srvname) as server_name,
                        jsonb_object_agg(fo.option_name, fo.option_value) filter (where fo.option_name is not null) over w as foreign_tab_options
                    from pg_catalog.pg_class c
                    left join pg_catalog.pg_inherits i
                        on c."oid" = i.inhparent
                    left join pg_catalog.pg_class p
                        on i.inhrelid = p."oid"
                    left join pg_catalog.pg_tablespace ts
                        on p.reltablespace = ts."oid"
                    left join pg_catalog.pg_foreign_table ft
                        on p."oid" = ft.ftrelid
                    left join pg_catalog.pg_foreign_server fs
                        on ft.ftserver = fs."oid"
                    left join lateral pg_catalog.pg_options_to_table(p.reloptions) lo (option_name, option_value)
                        on true
                    left join lateral pg_catalog.pg_options_to_table(ft.ftoptions) fo (option_name, option_value)
                        on true
                    where
                        c."oid" = root_table_oid
                    window w as (partition by p."oid")
                ) a
            ),

            dependency_pair as
            (
                select
                    dep.objid,
                    dep.refobjid,
                    obj."identity" as obj_name,
                    obj."type" as object_type,
                    refobj."type" as refobj_type
                from pg_catalog.pg_depend dep
                join current_settings c
                    on c.partition_oid = dep.refobjid
                cross join lateral pg_catalog.pg_identify_object(dep.classid, dep.objid, 0) obj ("type", "schema", "name", "identity")
                cross join lateral pg_catalog.pg_identify_object(dep.refclassid, dep.refobjid, 0) refobj ("type", "schema", "name", "identity")
                where
                    dep.deptype in ('n', 'a', 'i')

                union

                select
                    dep.refobjid as objid,
                    dep.objid as refobjid,
                    obj."identity" as obj_name,
                    obj."type" as object_type,
                    refobj."type" as refobj_type
                from pg_catalog.pg_depend dep
                join pg_catalog.pg_rewrite r
                    on r.oid = dep.objid
                    and r.ev_class = dep.refobjid
                cross join lateral pg_catalog.pg_identify_object(dep.classid, dep.objid, 0) refobj ("type", "schema", "name", "identity")
                cross join lateral pg_catalog.pg_identify_object(dep.refclassid, dep.refobjid, 0) obj ("type", "schema", "name", "identity")
                where
                    dep.deptype in ('n', 'a', 'i')
            ),

            dependency_hierarchy as
            (
                select
                    0 as lvl,
                    root.refobjid as objid,
                    root.obj_name,
                    root.refobj_type as obj_type,
                    array[root.refobjid] as dependency_oid_chain
                from dependency_pair root
                where not exists
                    (
                        select
                        from dependency_pair branch
                        where
                            branch.objid = root.refobjid
                    )

                union

                select
                    parent.lvl + 1 as lvl,
                    child.objid as objid,
                    child.obj_name,
                    child.object_type as obj_type,
                    parent.dependency_oid_chain || child.objid as dependency_oid_chain
                from dependency_pair child
                join dependency_hierarchy parent
                    on parent.objid = child.refobjid
                where
                    child.objid != all(parent.dependency_oid_chain)
            ),

            dep_ddl as
            (
                select
                    c.partition_oid,
                    string_agg
                    (
                        case
                            when dh.obj_type = 'view'
                                then 'DROP VIEW IF EXISTS ' || dh.obj_name || ' CASCADE;'
                            when dh.obj_type = 'materialized view'
                                then 'DROP MATERIALIZED VIEW IF EXISTS ' || dh.obj_name || ' CASCADE;'
                            when dh.obj_type = 'table constraint'
                                then
                                    'ALTER '
                                    || case when c.partition_table_type is distinct from 'foreign' then '' else upper(c.partition_table_type) || ' ' end
                                    || 'TABLE IF EXISTS ' || c.partition_oid::regclass::text
                                    || ' DROP CONSTRAINT IF EXISTS ' || substring(dh.obj_name, '^.+(?=( on ))') || ' CASCADE;'
                            when dh.obj_type = 'index'
                                then 'DROP INDEX IF EXISTS ' || dh.obj_name || ' CASCADE;'
                            when dh.obj_type = 'trigger'
                                then 'DROP TRIGGER IF EXISTS ' || dh.obj_name || ' CASCADE;'
                            when dh.obj_type in ('function', 'procedure')
                                then 'DROP ROUTINE IF EXISTS ' || dh.objid::regprocedure || ' CASCADE;' -- 'begin atomic' SQL routines
                        end,
                        repeat(E'\n', 2)
                        order by
                            dh.obj_type,
                            dh.objid
                    ) as existing_dependencies_drop,
                    string_agg
                    (
                        case
                            when dh.obj_type = 'view'
                                then 'CREATE OR REPLACE VIEW ' || dh.obj_name || ' AS' || E'\n' || pg_catalog.pg_get_viewdef(dh.objid)
                            when dh.obj_type = 'materialized view'
                                then 'CREATE MATERIALIZED VIEW IF NOT EXISTS ' || dh.obj_name || ' AS' || E'\n' || pg_catalog.pg_get_viewdef(dh.objid)
                            when dh.obj_type = 'table constraint'
                                then
                                    'ALTER '
                                    || case when c.partition_table_type is distinct from 'foreign' then '' else upper(c.partition_table_type) || ' ' end
                                    || 'TABLE IF EXISTS ' || c.partition_oid::regclass::text
                                    || ' ADD CONSTRAINT ' || substring(dh.obj_name, '^.+(?=( on ))') || ' ' || pg_catalog.pg_get_constraintdef(dh.objid) || ';'
                            when dh.obj_type = 'index'
                                then regexp_replace(pg_catalog.pg_get_indexdef(dh.objid), 'CREATE (UNIQUE |)INDEX', 'CREATE \1INDEX IF NOT EXISTS') || ';'
                            when dh.obj_type = 'trigger'
                                then regexp_replace(pg_catalog.pg_get_triggerdef(dh.objid), 'CREATE (CONSTRAINT |)TRIGGER', 'CREATE OR REPLACE \1TRIGGER') || ';'
                            when dh.obj_type in ('function', 'procedure')
                                then pg_catalog.pg_get_functiondef(dh.objid) || ';' -- 'begin atomic' SQL routines
                        end,
                        repeat(E'\n', 2)
                        order by
                            dh.obj_type,
                            dh.objid
                    ) as existing_dependencies_ddl
                from dependency_hierarchy dh
                join current_settings c
                    on c.partition_oid = any(dh.dependency_oid_chain)
                where
                    dh.lvl != 0
                    and dh.obj_type not in ('type', 'rule')
                group by
                    c.partition_oid
            )

            select
                n.window_num,
                n.partition_num,
                n.partition_key_expr,
                n.partition_key_type,
                n.value_from as new_value_from,
                n.value_to as new_value_to,
                n.partition_name as new_partition_name,
                n.partition_table_type as new_partition_table_type,
                n.tablespace_name as new_tablespace_name,
                n.local_tab_options as new_local_tab_options,
                n.server_name as new_server_name,
                n.foreign_tab_options as new_foreign_tab_options,
                n.partition_ddl as new_partition_ddl,
                c.value_from as cur_value_from,
                c.value_to as cur_value_to,
                c.partition_name as cur_partition_name,
                c.partition_table_type as cur_partition_table_type,
                c.tablespace_name as cur_tablespace_name,
                c.local_tab_options as cur_local_tab_options,
                c.server_name as cur_server_name,
                c.foreign_tab_options as cur_foreign_tab_options,
                dd.existing_dependencies_drop,
                dd.existing_dependencies_ddl
            from new_ddl n
            full join current_settings c
                on c.value_from = n.value_from
                and c.value_to = n.value_to
            left join dep_ddl dd
                on c.partition_oid = dd.partition_oid;


        -- Checking differences between old and new partitions' schema

        select distinct
            c.partition_key_type
        into partition_key_type_name
        from pg_temp.partitions_compare c
        where
            c.partition_key_type is not null;


            -- Building table with partitions that needs to be altered

        drop table if exists pg_temp.repartitioning_plan_prepare cascade;

        execute format($fmt$

            create temporary table if not exists pg_temp.repartitioning_plan_prepare
                on commit drop
            as

                with sc_1_2 as
                (
                    -- Partition merge type 1 - partitions borders are the same, but some options are different
                        -- Try to ALTER partition options; if not possible then detach old -> attach new -> copy data

                    select
                        1::smallint as partition_merge_type,
                        n.window_num,
                        n.partition_num,
                        n.partition_key_expr,
                        n.partition_key_type,
                        n.new_value_from,
                        n.new_value_to,
                        n.new_partition_name,
                        n.new_partition_table_type,
                        n.new_tablespace_name,
                        n.new_local_tab_options,
                        n.new_server_name,
                        n.new_foreign_tab_options,
                        n.new_partition_ddl,
                        n.cur_value_from,
                        n.cur_value_to,
                        n.cur_partition_name,
                        n.cur_partition_table_type,
                        n.cur_tablespace_name,
                        n.cur_local_tab_options,
                        n.cur_server_name,
                        n.cur_foreign_tab_options,
                        n.existing_dependencies_drop,
                        n.existing_dependencies_ddl,
                        null::boolean as detach_partition_flg
                    from pg_temp.partitions_compare n
                    where exists
                        (
                            select
                            from pg_temp.partitions_compare c
                            where
                                n.new_value_from = c.cur_value_from -- no casts here, with minvalue / maxvalue, but without default
                                and n.new_value_to = c.cur_value_to
                                and 'default'::text != all(array[
                                        coalesce(n.new_value_from, 'N/A'),
                                        coalesce(n.new_value_to, 'N/A'),
                                        coalesce(c.cur_value_from, 'N/A'),
                                        coalesce(c.cur_value_to, 'N/A')
                                    ]::text[])
                                and
                                    (
                                        n.new_partition_name,
                                        n.new_partition_table_type,
                                        n.new_tablespace_name,
                                        n.new_local_tab_options,
                                        n.new_server_name,
                                        n.new_foreign_tab_options
                                    )
                                    is distinct from
                                    (
                                        c.cur_partition_name,
                                        c.cur_partition_table_type,
                                        c.cur_tablespace_name,
                                        c.cur_local_tab_options,
                                        c.cur_server_name,
                                        c.cur_foreign_tab_options
                                    )
                        )

                    union all

                    -- Partition merge type 2 - partitions borders are different, but overlapping
                        -- Detach old -> attach new -> copy data

                        -- 2.1 - Normal values of partitions' bounds

                    select
                        2::smallint as partition_merge_type,
                        n.window_num,
                        n.partition_num,
                        n.partition_key_expr,
                        n.partition_key_type,
                        n.new_value_from,
                        n.new_value_to,
                        n.new_partition_name,
                        n.new_partition_table_type,
                        n.new_tablespace_name,
                        n.new_local_tab_options,
                        n.new_server_name,
                        n.new_foreign_tab_options,
                        n.new_partition_ddl,
                        c.cur_value_from,
                        c.cur_value_to,
                        c.cur_partition_name,
                        c.cur_partition_table_type,
                        c.cur_tablespace_name,
                        c.cur_local_tab_options,
                        c.cur_server_name,
                        c.cur_foreign_tab_options,
                        c.existing_dependencies_drop,
                        c.existing_dependencies_ddl,
                        null::boolean as detach_partition_flg
                    from pg_temp.partitions_compare n
                    join pg_temp.partitions_compare c
                        on 'default'::text != all(array[
                                coalesce(n.new_value_from, 'N/A'),
                                coalesce(n.new_value_to, 'N/A'),
                                coalesce(c.cur_value_from, 'N/A'),
                                coalesce(c.cur_value_to, 'N/A')
                            ]::text[])
                        and 'minvalue'::text != all(array[
                                coalesce(n.new_value_from, 'N/A'),
                                coalesce(n.new_value_to, 'N/A'),
                                coalesce(c.cur_value_from, 'N/A'),
                                coalesce(c.cur_value_to, 'N/A')
                            ]::text[])
                        and 'maxvalue'::text != all(array[
                                coalesce(n.new_value_from, 'N/A'),
                                coalesce(n.new_value_to, 'N/A'),
                                coalesce(c.cur_value_from, 'N/A'),
                                coalesce(c.cur_value_to, 'N/A')
                            ]::text[])
                        and n.new_value_from::%1$s < c.cur_value_to::%1$s -- strict inequality here, because partitions' upper bound is exclusive
                        and c.cur_value_from::%1$s < n.new_value_to::%1$s
                        and not -- no casts here, without minvalue / maxvalue
                            (
                                n.new_value_from = c.cur_value_from
                                and n.new_value_to = c.cur_value_to
                            )

                    union all

                        -- 2.2 - maxvalue / minvalue bounds

                    select
                        2::smallint as partition_merge_type,
                        n.window_num,
                        n.partition_num,
                        n.partition_key_expr,
                        n.partition_key_type,
                        n.new_value_from,
                        n.new_value_to,
                        n.new_partition_name,
                        n.new_partition_table_type,
                        n.new_tablespace_name,
                        n.new_local_tab_options,
                        n.new_server_name,
                        n.new_foreign_tab_options,
                        n.new_partition_ddl,
                        c.cur_value_from,
                        c.cur_value_to,
                        c.cur_partition_name,
                        c.cur_partition_table_type,
                        c.cur_tablespace_name,
                        c.cur_local_tab_options,
                        c.cur_server_name,
                        c.cur_foreign_tab_options,
                        c.existing_dependencies_drop,
                        c.existing_dependencies_ddl,
                        null::boolean as detach_partition_flg
                    from pg_temp.partitions_compare n
                    join pg_temp.partitions_compare c
                        on 'default'::text != all(array[
                                coalesce(n.new_value_from, 'N/A'),
                                coalesce(n.new_value_to, 'N/A'),
                                coalesce(c.cur_value_from, 'N/A'),
                                coalesce(c.cur_value_to, 'N/A')
                            ]::text[])
                        and
                        (
                            'minvalue'::text = any(array[
                                coalesce(n.new_value_from, 'N/A'),
                                coalesce(c.cur_value_from, 'N/A')
                            ]::text[])
                            or
                            'maxvalue'::text = any(array[
                                coalesce(n.new_value_to, 'N/A'),
                                coalesce(c.cur_value_to, 'N/A')
                            ]::text[])
                        )
                        and
                            (
                                n.new_value_from = c.cur_value_from  -- no casts here, with minvalue / maxvalue
                                or n.new_value_to = c.cur_value_to
                            )
                        and not
                            (
                                n.new_value_from = c.cur_value_from  -- only one pair of boundaries is equal but not both
                                and n.new_value_to = c.cur_value_to
                            )

                )

                select
                    s.partition_merge_type,
                    s.window_num,
                    s.partition_num,
                    s.partition_key_expr,
                    s.partition_key_type,
                    s.new_value_from,
                    s.new_value_to,
                    s.new_partition_name,
                    s.new_partition_table_type,
                    s.new_tablespace_name,
                    s.new_local_tab_options,
                    s.new_server_name,
                    s.new_foreign_tab_options,
                    s.new_partition_ddl,
                    s.cur_value_from,
                    s.cur_value_to,
                    s.cur_partition_name,
                    s.cur_partition_table_type,
                    s.cur_tablespace_name,
                    s.cur_local_tab_options,
                    s.cur_server_name,
                    s.cur_foreign_tab_options,
                    s.existing_dependencies_drop,
                    s.existing_dependencies_ddl,
                    s.detach_partition_flg
                from sc_1_2 s
                where
                    coalesce(s.new_partition_name, s.cur_partition_name) is not null

                union all

                -- Partition merge type 3 - new partitions that are not overlapping with any current partitions
                    -- Simple ATTACH PARTITION

                select
                    3::smallint as partition_merge_type,
                    n.window_num,
                    n.partition_num,
                    n.partition_key_expr,
                    n.partition_key_type,
                    n.new_value_from,
                    n.new_value_to,
                    n.new_partition_name,
                    n.new_partition_table_type,
                    n.new_tablespace_name,
                    n.new_local_tab_options,
                    n.new_server_name,
                    n.new_foreign_tab_options,
                    n.new_partition_ddl,
                    null::text as cur_value_from,
                    null::text as cur_value_to,
                    null::text as cur_partition_name,
                    null::text as cur_partition_table_type,
                    null::text as cur_tablespace_name,
                    null::jsonb as cur_local_tab_options,
                    null::text as cur_server_name,
                    null::jsonb as cur_foreign_tab_options,
                    null::text as existing_dependencies_drop,
                    null::text as existing_dependencies_ddl,
                    false as detach_partition_flg
                from pg_temp.partitions_compare n
                where
                    n.cur_partition_name is null
                    and not exists
                        (
                            select
                            from sc_1_2 s
                            where
                                n.window_num = s.window_num
                                and n.partition_num = s.partition_num
                        )
                    and n.new_partition_name is not null

                union all

                -- Partition merge type 4 - current partitions that are not overlapping with any new partitions
                    -- Simple DETACH PARTITION

                select
                    4::smallint as partition_merge_type,
                    null::smallint as window_num,
                    null::smallint as partition_num,
                    null::text as partition_key_expr,
                    null::text as partition_key_type,
                    null::text as new_value_from,
                    null::text as new_value_to,
                    null::text as new_partition_name,
                    null::text as new_partition_table_type,
                    null::text as new_tablespace_name,
                    null::jsonb as new_local_tab_options,
                    null::text as new_server_name,
                    null::jsonb as new_foreign_tab_options,
                    null::text as new_partition_ddl,
                    c.cur_value_from,
                    c.cur_value_to,
                    c.cur_partition_name,
                    c.cur_partition_table_type,
                    c.cur_tablespace_name,
                    c.cur_local_tab_options,
                    c.cur_server_name,
                    c.cur_foreign_tab_options,
                    c.existing_dependencies_drop,
                    c.existing_dependencies_ddl,
                    true as detach_partition_flg
                from pg_temp.partitions_compare c
                where
                    c.new_partition_name is null
                    and not exists
                        (
                            select
                            from sc_1_2 s
                            where
                                c.cur_partition_name = s.cur_partition_name
                        )
                    and c.cur_partition_name is not null

                union all

                -- Partition merge type 5 - default partition
                    -- Could not exists in current design / new design only, could exists / not exists in both designs

                select
                    5::smallint as partition_merge_type,
                    coalesce(n.window_num, -1::smallint) as window_num,
                    coalesce(n.partition_num, 1::smallint) as partition_num,
                    coalesce(n.partition_key_expr, c.partition_key_expr) as partition_key_expr,
                    coalesce(n.partition_key_type, c.partition_key_type) as partition_key_type,
                    n.new_value_from,
                    n.new_value_to,
                    n.new_partition_name,
                    n.new_partition_table_type,
                    n.new_tablespace_name,
                    n.new_local_tab_options,
                    n.new_server_name,
                    n.new_foreign_tab_options,
                    n.new_partition_ddl,
                    c.cur_value_from,
                    c.cur_value_to,
                    c.cur_partition_name,
                    c.cur_partition_table_type,
                    c.cur_tablespace_name,
                    c.cur_local_tab_options,
                    c.cur_server_name,
                    c.cur_foreign_tab_options,
                    c.existing_dependencies_drop,
                    c.existing_dependencies_ddl,
                    null::boolean as detach_partition_flg
                from pg_temp.partitions_compare n
                full join pg_temp.partitions_compare c
                    on n.new_value_from = c.cur_value_from
                where
                    'default'::text = any(array[
                            coalesce(n.new_value_from, 'N/A'),
                            coalesce(n.new_value_to, 'N/A'),
                            coalesce(c.cur_value_from, 'N/A'),
                            coalesce(c.cur_value_to, 'N/A')
                        ]::text[])
                    and
                        (
                            n.new_partition_name,
                            n.new_partition_table_type,
                            n.new_tablespace_name,
                            n.new_local_tab_options,
                            n.new_server_name,
                            n.new_foreign_tab_options
                        )
                        is distinct from
                        (
                            c.cur_partition_name,
                            c.cur_partition_table_type,
                            c.cur_tablespace_name,
                            c.cur_local_tab_options,
                            c.cur_server_name,
                            c.cur_foreign_tab_options
                        )
                    and coalesce(n.new_partition_name, c.cur_partition_name) is not null

            $fmt$,
            partition_key_type_name
        );

            -- Copying options of old partitions to new partitions if needed

        if copy_local_options_flg is true then

            update pg_temp.repartitioning_plan_prepare r
            set
                new_tablespace_name = coalesce(r.cur_tablespace_name, r.new_tablespace_name),
                new_local_tab_options = coalesce(r.cur_local_tab_options, r.new_local_tab_options)
            where
                r.new_partition_table_type is distinct from 'foreign'
                and r.cur_partition_table_type is distinct from 'foreign';

        end if;

        if copy_foreign_options_flg is true then

            update pg_temp.repartitioning_plan_prepare r
            set
                new_server_name = coalesce(r.cur_server_name, r.new_server_name),
                new_foreign_tab_options = coalesce(r.cur_foreign_tab_options, r.new_foreign_tab_options)
            where
                r.new_partition_table_type = 'foreign'
                and r.cur_partition_table_type = 'foreign';

        end if;

        drop table if exists pg_temp.repartitioning_plan cascade;

        create temporary table if not exists pg_temp.repartitioning_plan
            on commit drop
        as
            select distinct
                r.*
            from pg_temp.repartitioning_plan_prepare r
            where
                (
                    r.new_value_from,
                    r.new_value_to,
                    r.new_partition_name,
                    r.new_partition_table_type,
                    r.new_tablespace_name,
                    r.new_local_tab_options,
                    r.new_server_name,
                    r.new_foreign_tab_options
                )
                is distinct from
                (
                    r.cur_value_from,
                    r.cur_value_to,
                    r.cur_partition_name,
                    r.cur_partition_table_type,
                    r.cur_tablespace_name,
                    r.cur_local_tab_options,
                    r.cur_server_name,
                    r.cur_foreign_tab_options
                );

        get diagnostics
            repartitioning_plan_count := row_count;

        raise notice '% partitions will be altered with the repartitioning plan', repartitioning_plan_count;


        -- Manipulations with data

        if dry_run is false and repartitioning_plan_count > 0 then

            -- Checking rows count in the root before manipulations with partitions

            execute format($fmt$
                    select
                        count(*)
                    from %s
                $fmt$,
                root_table_oid::regclass::text
            )
            into root_rows_cnt_before;

            raise notice 'Rows count in the root table before data manipulation - % rows', root_rows_cnt_before;

            -- Locking the root table with all partitions

            execute 'lock ' || root_table_oid::regclass::text || ' in access exclusive mode nowait';

            -- Attaching / detaching partitions

            <<repartitioning_loop>>
            for repartitioning_rec in
                select
                    row_number() over(order by r.window_num, r.partition_num, r.partition_merge_type) as rn,
                    r.partition_merge_type,
                    r.window_num,
                    r.partition_num,
                    r.partition_key_expr,
                    r.partition_key_type,
                    r.new_value_from,
                    r.new_value_to,
                    r.new_partition_name,
                    r.new_partition_table_type,
                    r.new_tablespace_name,
                    r.new_local_tab_options,
                    r.new_server_name,
                    r.new_foreign_tab_options,
                    r.new_partition_ddl,
                    r.cur_value_from,
                    r.cur_value_to,
                    r.cur_partition_name,
                    r.cur_partition_table_type,
                    r.cur_tablespace_name,
                    r.cur_local_tab_options,
                    r.cur_server_name,
                    r.cur_foreign_tab_options,
                    r.existing_dependencies_drop,
                    r.existing_dependencies_ddl,
                    r.detach_partition_flg
                from pg_temp.repartitioning_plan r
                order by
                    r.window_num,
                    r.partition_num,
                    r.partition_merge_type,
                    r.cur_partition_name
            loop

                raise notice '%',
                    'Iteration ' || repartitioning_rec.rn || E':\n\t'
                    || 'Partition merge type = ' || repartitioning_rec.partition_merge_type || E'\n\t'
                    || 'Window number = ' || repartitioning_rec.window_num || E'\n\t'
                    || 'Partition number = ' || repartitioning_rec.partition_num || E'\n\t'
                    || 'New partition name = ' || coalesce(repartitioning_rec.new_partition_name, 'null') || E'\n\t'
                    || 'Current partition name = ' || coalesce(repartitioning_rec.cur_partition_name, 'null') || E'\n\t'
                    || 'State of "detach_partition_flg" before iteration = ' || coalesce(repartitioning_rec.detach_partition_flg::text, 'null');

                select
                    "schema"::text as "schema",
                    "name"::text as "name"
                into
                    cur_partition_schema,
                    cur_partition_relname
                from pg_catalog.pg_identify_object('pg_catalog.pg_class'::regclass, repartitioning_rec.cur_partition_name::regclass, 0)
                    as obj ("type", "schema", "name", "identity");

                select
                    case
                        when repartitioning_rec.new_partition_table_type = 'temporary' then quote_ident('pg_temp')
                        else quote_ident(coalesce(partitions_schema, c.relnamespace::regnamespace::text))
                    end as "schema",
                    replace(
                        repartitioning_rec.new_partition_name,
                        case
                            when repartitioning_rec.new_partition_table_type = 'temporary' then quote_ident('pg_temp')
                            else quote_ident(coalesce(partitions_schema, c.relnamespace::regnamespace::text))
                        end || '.',
                        ''
                    ) as "name"
                into
                    new_partition_schema,
                    new_partition_relname
                from pg_catalog.pg_class c
                where
                    c."oid" = root_table_oid;

                    -- 1. - Same borders in both designs, different DDL
                if repartitioning_rec.partition_merge_type = 1 then

                        -- 1.1 - Partition exists in both designs with same settings but different names - rename
                    if
                        repartitioning_rec.new_partition_name != repartitioning_rec.cur_partition_name
                        and
                            (
                                repartitioning_rec.new_partition_table_type,
                                repartitioning_rec.new_tablespace_name,
                                repartitioning_rec.new_local_tab_options,
                                repartitioning_rec.new_server_name,
                                repartitioning_rec.new_foreign_tab_options
                            )
                            is not distinct from
                            (
                                repartitioning_rec.cur_partition_table_type,
                                repartitioning_rec.cur_tablespace_name,
                                repartitioning_rec.cur_local_tab_options,
                                repartitioning_rec.cur_server_name,
                                repartitioning_rec.cur_foreign_tab_options
                            )
                    then

                        -- 1.1.1. - Schema and name are both different
                        if
                            cur_partition_schema != new_partition_schema
                            and cur_partition_relname != new_partition_relname
                        then

                            attach_qry := attach_qry
                                || 'alter '
                                || case
                                    when repartitioning_rec.cur_partition_table_type is null
                                        then ''
                                    else
                                        repartitioning_rec.cur_partition_table_type || ' '
                                    end
                                || 'table if exists ' || repartitioning_rec.cur_partition_name || E'\n\t'
                                || 'rename to ' || new_partition_relname || ';' || repeat(E'\n', 2)

                                || 'alter '
                                || case
                                    when repartitioning_rec.cur_partition_table_type is null
                                        then ''
                                    else
                                        repartitioning_rec.cur_partition_table_type || ' '
                                    end
                                || 'table if exists '
                                || quote_ident(cur_partition_schema) || '.' || quote_ident(new_partition_relname) || E'\n\t'
                                || 'set schema ' || repartitioning_rec.new_partition_schema || ';' || repeat(E'\n', 2);

                            detach_partition_flg_upd := repartitioning_rec.detach_partition_flg;

                        -- 1.1.2. - Same name, different schema
                        elsif
                            cur_partition_schema != new_partition_schema
                            and cur_partition_relname = new_partition_relname
                        then

                            attach_qry := attach_qry
                                || 'alter '
                                || case
                                    when repartitioning_rec.cur_partition_table_type is null
                                        then ''
                                    else
                                        repartitioning_rec.cur_partition_table_type || ' '
                                    end
                                || 'table if exists '
                                || quote_ident(cur_partition_schema) || '.' || quote_ident(new_partition_relname) || E'\n\t'
                                || 'set schema ' || repartitioning_rec.new_partition_schema || ';' || repeat(E'\n', 2);

                            detach_partition_flg_upd := repartitioning_rec.detach_partition_flg;

                        -- 1.1.3. - Same schema, different name
                        elsif
                            cur_partition_schema = new_partition_schema
                            and cur_partition_relname != new_partition_relname
                        then

                            attach_qry := attach_qry
                                || 'alter '
                                || case
                                    when repartitioning_rec.cur_partition_table_type is null
                                        then ''
                                    else
                                        repartitioning_rec.cur_partition_table_type || ' '
                                    end
                                || 'table if exists ' || repartitioning_rec.cur_partition_name || E'\n\t'
                                || 'rename to ' || new_partition_relname || ';' || repeat(E'\n', 2);

                        end if;

                        update pg_temp.repartitioning_plan r
                        set
                            detach_partition_flg = false
                        where
                            r.cur_partition_name = repartitioning_rec.cur_partition_name;

                        detach_partition_flg_upd := false;

                        -- 1.2 - Partition exists in both designs with different table type - detach old -> attach new -> copy data
                            -- Notice that inserts from regular table / foreign table to other foreign table could fail - some FDWs may not support inserts
                    elsif
                        (
                            repartitioning_rec.new_partition_table_type,
                            repartitioning_rec.new_tablespace_name,
                            repartitioning_rec.new_local_tab_options,
                            repartitioning_rec.new_server_name,
                            repartitioning_rec.new_foreign_tab_options
                        )
                        is distinct from
                        (
                            repartitioning_rec.cur_partition_table_type,
                            repartitioning_rec.cur_tablespace_name,
                            repartitioning_rec.cur_local_tab_options,
                            repartitioning_rec.cur_server_name,
                            repartitioning_rec.cur_foreign_tab_options
                        )
                    then

                        if repartitioning_rec.detach_partition_flg is not true then

                            detach_qry := detach_qry
                                || 'alter table if exists ' || root_table_oid::regclass::text || E'\n\t'
                                || 'detach partition ' || repartitioning_rec.cur_partition_name || ';' || repeat(E'\n', 2);

                        end if;

                        attach_qry := attach_qry || repartitioning_rec.new_partition_ddl || repeat(E'\n', 2);

                        if repartitioning_rec.existing_dependencies_ddl is not null then

                            dependencies_qry := dependencies_qry
                                || repartitioning_rec.existing_dependencies_drop || repeat(E'\n', 2)
                                || replace -- replacing table types if switched
                                    (
                                        replace -- replacing (cur_partition_name | cur_partition_name without schema) with (new_partition_name | new_partition_relname)
                                        (
                                            replace
                                            (
                                                repartitioning_rec.existing_dependencies_ddl,
                                                repartitioning_rec.cur_partition_name,
                                                repartitioning_rec.new_partition_name
                                            ),
                                            cur_partition_relname,
                                            new_partition_relname
                                        ),
                                        case
                                            when
                                                repartitioning_rec.new_partition_table_type = 'foreign'
                                                and repartitioning_rec.cur_partition_table_type is distinct from 'foreign'
                                                    then 'ALTER TABLE'
                                            when
                                                repartitioning_rec.new_partition_table_type is distinct from 'foreign'
                                                and repartitioning_rec.cur_partition_table_type = 'foreign'
                                                    then 'ALTER FOREIGN TABLE'
                                            else ''
                                        end,
                                        case
                                            when
                                                repartitioning_rec.new_partition_table_type = 'foreign'
                                                and repartitioning_rec.cur_partition_table_type is distinct from 'foreign'
                                                    then 'ALTER FOREIGN TABLE'
                                            when
                                                repartitioning_rec.new_partition_table_type is distinct from 'foreign'
                                                and repartitioning_rec.cur_partition_table_type = 'foreign'
                                                    then 'ALTER TABLE'
                                            else ''
                                        end
                                    )
                                || repeat(E'\n', 2);

                        end if;

                        update pg_temp.repartitioning_plan r
                        set
                            detach_partition_flg = true
                        where
                            r.cur_partition_name = repartitioning_rec.cur_partition_name;

                        detach_partition_flg_upd := true;

                    end if;


                    -- 2. - Overlapping borders in both designs - detach old -> attach new -> copy data
                elsif repartitioning_rec.partition_merge_type = 2 then

                    if repartitioning_rec.detach_partition_flg is not true then

                        detach_qry := detach_qry
                            || 'alter table if exists ' || root_table_oid::regclass::text || E'\n\t'
                            || 'detach partition ' || repartitioning_rec.cur_partition_name || ';' || repeat(E'\n', 2);

                    end if;

                    attach_qry := attach_qry || repartitioning_rec.new_partition_ddl || repeat(E'\n', 2);

                        -- Checking if both old partition bounds are inside new partition bounds
                    execute format($fmt$
                            select
                                coalesce
                                (
                                    %2$L::%1$s >= %3$L::%1$s
                                    and %4$L::%1$s <= %5$L::%1$s,
                                    false
                                )
                        $fmt$,
                        repartitioning_rec.partition_key_type,
                        repartitioning_rec.cur_value_from,
                        repartitioning_rec.new_value_from,
                        repartitioning_rec.cur_value_to,
                        repartitioning_rec.new_value_to
                    )
                    into cur_partition_contained_flg;

                        -- It's only safe-ish to switch dependencies to new partition from old partition only then "cur_partition_contained_flg" is true
                            -- When "cur_partition_contained_flg" is false then there could be violations of check constraints / foreign keys
                            -- Even with "cur_partition_contained_flg" is true take it with care - views on new partition may have more loose restricitions on partition key values
                    if
                        repartitioning_rec.existing_dependencies_ddl is not null
                        and cur_partition_contained_flg is true
                    then

                        dependencies_qry := dependencies_qry
                            || repartitioning_rec.existing_dependencies_drop || repeat(E'\n', 2)
                            || replace -- replacing table types if switched
                                (
                                    replace -- replacing (cur_partition_name | cur_partition_name without schema) with (new_partition_name | new_partition_relname)
                                    (
                                        replace
                                        (
                                            repartitioning_rec.existing_dependencies_ddl,
                                            repartitioning_rec.cur_partition_name,
                                            repartitioning_rec.new_partition_name
                                        ),
                                        cur_partition_relname,
                                        new_partition_relname
                                    ),
                                    case
                                        when
                                            repartitioning_rec.new_partition_table_type = 'foreign'
                                            and repartitioning_rec.cur_partition_table_type is distinct from 'foreign'
                                                then 'ALTER TABLE'
                                        when
                                            repartitioning_rec.new_partition_table_type is distinct from 'foreign'
                                            and repartitioning_rec.cur_partition_table_type = 'foreign'
                                                then 'ALTER FOREIGN TABLE'
                                        else ''
                                    end,
                                    case
                                        when
                                            repartitioning_rec.new_partition_table_type = 'foreign'
                                            and repartitioning_rec.cur_partition_table_type is distinct from 'foreign'
                                                then 'ALTER FOREIGN TABLE'
                                        when
                                            repartitioning_rec.new_partition_table_type is distinct from 'foreign'
                                            and repartitioning_rec.cur_partition_table_type = 'foreign'
                                                then 'ALTER TABLE'
                                        else ''
                                    end
                                )
                            || repeat(E'\n', 2);

                    end if;

                    update pg_temp.repartitioning_plan r
                    set
                        detach_partition_flg = true
                    where
                        r.cur_partition_name = repartitioning_rec.cur_partition_name;

                    detach_partition_flg_upd := true;


                    -- 3. - ATTACH only
                elsif repartitioning_rec.partition_merge_type = 3 then

                    attach_qry := attach_qry || repartitioning_rec.new_partition_ddl || repeat(E'\n', 2);

                    detach_partition_flg_upd := repartitioning_rec.detach_partition_flg;


                    -- 4. - DETACH only
                elsif repartitioning_rec.partition_merge_type = 4 then

                    if repartitioning_rec.detach_partition_flg is true then -- without "not" here

                        detach_qry := detach_qry
                            || 'alter table if exists ' || root_table_oid::regclass::text || E'\n\t'
                            || 'detach partition ' || repartitioning_rec.cur_partition_name || ';' || repeat(E'\n', 2);

                    end if;

                    update pg_temp.repartitioning_plan r
                    set
                        detach_partition_flg = true
                    where
                        r.cur_partition_name = repartitioning_rec.cur_partition_name;

                    detach_partition_flg_upd := true;


                    -- 5. - Default partition
                elsif repartitioning_rec.partition_merge_type = 5 then

                        -- 5.1. - Default partition exists in both designs with same settings but different names - rename
                    if
                        repartitioning_rec.new_partition_name != repartitioning_rec.cur_partition_name
                        and
                            (
                                repartitioning_rec.new_partition_table_type,
                                repartitioning_rec.new_tablespace_name,
                                repartitioning_rec.new_local_tab_options,
                                repartitioning_rec.new_server_name,
                                repartitioning_rec.new_foreign_tab_options
                            )
                            is not distinct from
                            (
                                repartitioning_rec.cur_partition_table_type,
                                repartitioning_rec.cur_tablespace_name,
                                repartitioning_rec.cur_local_tab_options,
                                repartitioning_rec.cur_server_name,
                                repartitioning_rec.cur_foreign_tab_options
                            )
                    then

                        -- 5.1.1. - Schema and name are both different
                        if
                            cur_partition_schema != new_partition_schema
                            and cur_partition_relname != new_partition_relname
                        then

                            attach_qry := attach_qry
                                || 'alter '
                                || case
                                    when repartitioning_rec.cur_partition_table_type is null
                                        then ''
                                    else
                                        repartitioning_rec.cur_partition_table_type || ' '
                                    end
                                || 'table if exists ' || repartitioning_rec.cur_partition_name || E'\n\t'
                                || 'rename to ' || new_partition_relname || ';' || repeat(E'\n', 2)

                                || 'alter '
                                || case
                                    when repartitioning_rec.cur_partition_table_type is null
                                        then ''
                                    else
                                        repartitioning_rec.cur_partition_table_type || ' '
                                    end
                                || 'table if exists '
                                || quote_ident(cur_partition_schema) || '.' || quote_ident(new_partition_relname) || E'\n\t'
                                || 'set schema ' || repartitioning_rec.new_partition_schema || ';' || repeat(E'\n', 2);

                            detach_partition_flg_upd := repartitioning_rec.detach_partition_flg;

                        -- 5.1.2. - Same name, different schema
                        elsif
                            cur_partition_schema != new_partition_schema
                            and cur_partition_relname = new_partition_relname
                        then

                            attach_qry := attach_qry
                                || 'alter '
                                || case
                                    when repartitioning_rec.cur_partition_table_type is null
                                        then ''
                                    else
                                        repartitioning_rec.cur_partition_table_type || ' '
                                    end
                                || 'table if exists '
                                || quote_ident(cur_partition_schema) || '.' || quote_ident(new_partition_relname) || E'\n\t'
                                || 'set schema ' || repartitioning_rec.new_partition_schema || ';' || repeat(E'\n', 2);

                            detach_partition_flg_upd := repartitioning_rec.detach_partition_flg;

                        -- 5.1.3. - Same schema, different name
                        elsif
                            cur_partition_schema = new_partition_schema
                            and cur_partition_relname != new_partition_relname
                        then

                            attach_qry := attach_qry
                                || 'alter '
                                || case
                                    when repartitioning_rec.cur_partition_table_type is null
                                        then ''
                                    else
                                        repartitioning_rec.cur_partition_table_type || ' '
                                    end
                                || 'table if exists ' || repartitioning_rec.cur_partition_name || E'\n\t'
                                || 'rename to ' || new_partition_relname || ';' || repeat(E'\n', 2);

                        end if;

                        update pg_temp.repartitioning_plan r
                        set
                            detach_partition_flg = false
                        where
                            r.cur_partition_name = repartitioning_rec.cur_partition_name;

                        detach_partition_flg_upd := false;

                        -- 5.2. - Default partition exists in both designs with same names but different settings - detach old -> attach new -> copy data
                    elsif
                        (
                            repartitioning_rec.new_partition_table_type,
                            repartitioning_rec.new_tablespace_name,
                            repartitioning_rec.new_local_tab_options,
                            repartitioning_rec.new_server_name,
                            repartitioning_rec.new_foreign_tab_options
                        )
                        is distinct from
                        (
                            repartitioning_rec.cur_partition_table_type,
                            repartitioning_rec.cur_tablespace_name,
                            repartitioning_rec.cur_local_tab_options,
                            repartitioning_rec.cur_server_name,
                            repartitioning_rec.cur_foreign_tab_options
                        )
                    then

                        if repartitioning_rec.detach_partition_flg is not true then

                            detach_qry := detach_qry
                                || 'alter table if exists ' || root_table_oid::regclass::text || E'\n\t'
                                || 'detach partition ' || repartitioning_rec.cur_partition_name || ';' || repeat(E'\n', 2);

                        end if;

                        attach_qry := attach_qry || repartitioning_rec.new_partition_ddl || repeat(E'\n', 2);

                        if repartitioning_rec.existing_dependencies_ddl is not null then

                            dependencies_qry := dependencies_qry
                                || repartitioning_rec.existing_dependencies_drop || repeat(E'\n', 2)
                                || replace -- replacing table types if switched
                                    (
                                        replace -- replacing (cur_partition_name | cur_partition_name without schema) with (new_partition_name | new_partition_relname)
                                        (
                                            replace
                                            (
                                                repartitioning_rec.existing_dependencies_ddl,
                                                repartitioning_rec.cur_partition_name,
                                                repartitioning_rec.new_partition_name
                                            ),
                                            cur_partition_relname,
                                            new_partition_relname
                                        ),
                                        case
                                            when
                                                repartitioning_rec.new_partition_table_type = 'foreign'
                                                and repartitioning_rec.cur_partition_table_type is distinct from 'foreign'
                                                    then 'ALTER TABLE'
                                            when
                                                repartitioning_rec.new_partition_table_type is distinct from 'foreign'
                                                and repartitioning_rec.cur_partition_table_type = 'foreign'
                                                    then 'ALTER FOREIGN TABLE'
                                            else ''
                                        end,
                                        case
                                            when
                                                repartitioning_rec.new_partition_table_type = 'foreign'
                                                and repartitioning_rec.cur_partition_table_type is distinct from 'foreign'
                                                    then 'ALTER FOREIGN TABLE'
                                            when
                                                repartitioning_rec.new_partition_table_type is distinct from 'foreign'
                                                and repartitioning_rec.cur_partition_table_type = 'foreign'
                                                    then 'ALTER TABLE'
                                            else ''
                                        end
                                    )
                                || repeat(E'\n', 2);

                        end if;

                        update pg_temp.repartitioning_plan r
                        set
                            detach_partition_flg = true
                        where
                            r.cur_partition_name = repartitioning_rec.cur_partition_name;

                        detach_partition_flg_upd := true;

                        -- 5.3. - Default partition not exists in new design but exists in old design - DETACH only
                    elsif
                        repartitioning_rec.new_partition_name is null
                        and repartitioning_rec.cur_partition_name is not null
                    then

                        if repartitioning_rec.detach_partition_flg is not true then

                            detach_qry := detach_qry
                                || 'alter table if exists ' || root_table_oid::regclass::text || E'\n\t'
                                || 'detach partition ' || repartitioning_rec.cur_partition_name || ';' || repeat(E'\n', 2);

                        end if;

                        update pg_temp.repartitioning_plan r
                        set
                            detach_partition_flg = true
                        where
                            r.cur_partition_name = repartitioning_rec.cur_partition_name;

                        detach_partition_flg_upd := true;

                        -- 5.4. - Default partition not exists in old design but exists in new design - ATTACH only
                    elsif
                        repartitioning_rec.new_partition_name is not null
                        and repartitioning_rec.cur_partition_name is null
                    then

                        attach_qry := attach_qry || repartitioning_rec.new_partition_ddl || repeat(E'\n', 2);

                        update pg_temp.repartitioning_plan r
                        set
                            detach_partition_flg = false
                        where
                            r.cur_partition_name = repartitioning_rec.cur_partition_name;

                        detach_partition_flg_upd := false;

                    end if;

                end if;

                raise notice '%',
                    E'\t' || 'State of "detach_partition_flg" after iteration = ' || coalesce(detach_partition_flg_upd::text, 'null');

            end loop repartitioning_loop;

            -- Starting repartioning

            raise notice '% - Start of repartitioning', clock_timestamp();

                -- Executing DETACH query (detaching unnecessary old partitions)

            detach_qry = rtrim(detach_qry, E'\n');

            raise notice '%',
                'DETACH query:' || repeat(E'\n', 2) || detach_qry;

            execute detach_qry;

                -- Executing ATTACH query (creating new partitions)

            raise notice '% - Start of repartitioning', clock_timestamp();

            attach_qry = rtrim(attach_qry, E'\n');

            raise notice '%',
                'ATTACH query:' || repeat(E'\n', 2) || attach_qry;

            execute attach_qry;

                -- Executing query for restoring dependencies from old partitions to new partitions

            dependencies_qry = rtrim(dependencies_qry, E'\n');

            raise notice '%',
                'Restoring dependencies query:' || repeat(E'\n', 2) || dependencies_qry;

            execute dependencies_qry;

                -- Inserting data from detached partitions to new partitions via root

            select
                count(distinct r.cur_partition_name)
            into detach_cnt
            from pg_temp.repartitioning_plan r
            where
                r.detach_partition_flg is true
                and r.cur_partition_name is not null;

            if detach_cnt > 0 then

                insert_qry := 'insert into ' || root_table_oid::regclass || E'\n'
                    || 'overriding system value' || E'\n\t';

                <<insert_loop>>
                for insert_rec in
                    select distinct
                        r.cur_partition_name
                    from pg_temp.repartitioning_plan r
                    where
                        r.detach_partition_flg is true
                        and r.cur_partition_name is not null
                loop

                    insert_qry := insert_qry
                        || 'select *'  || E'\n\t'
                        || 'from ' || insert_rec.cur_partition_name || E'\n\t'
                        || 'union all' || repeat(E'\n', 2) || E'\t';

                end loop insert_loop;

                insert_qry := rtrim(insert_qry, E'\n\t' || 'union all' || repeat(E'\n', 2) || E'\t') || ';';

                raise notice '%',
                    'INSERT query:' || repeat(E'\n', 2) || insert_qry;

                execute insert_qry;

                -- Dropping detached partitions if needed (drop_detached_flg is true)

                if drop_detached_flg is true then

                    select
                        rtrim
                        (
                            string_agg
                            (
                                'drop '
                                || case
                                    when coalesce(r.cur_partition_table_type, '') is distinct from 'foreign'
                                        then ''
                                    else
                                        coalesce(r.cur_partition_table_type, '') || ' '
                                    end
                                || 'table if exists ' || r.cur_partition_name
                                || ' cascade;',
                                repeat(E'\n', 2)
                                order by
                                    r.cur_partition_name
                            ),
                            E'\n'
                        )
                    into drop_detached_qry
                    from pg_temp.repartitioning_plan r
                    where
                        r.detach_partition_flg is true
                        and r.cur_partition_name is not null;

                    raise notice '%',
                        'DROP detached query:' || repeat(E'\n', 2) || drop_detached_qry;

                    execute drop_detached_qry;

                end if;

            end if;

            raise notice '% - End of repartitioning', clock_timestamp();

                -- Checking rows count in the root after manipulations with partitions

            execute format($fmt$
                    select
                        count(*)
                    from %s
                $fmt$,
                root_table_oid::regclass::text
            )
            into root_rows_cnt_after;

            raise notice 'Rows count in the root table after data manipulation - % rows', root_rows_cnt_after;

            if
                root_rows_cnt_before is distinct from root_rows_cnt_after
                and skip_rows_count_check_flg is not true
            then
                raise exception '%',
                    'Rows count in the root table ' || root_table_oid::regclass::text || ' before manipulations with partitions '
                        || '(' || root_rows_cnt_before || ') is not equal to rows count after it (' || root_rows_cnt_after || ')'
                    using errcode = 'assert_failure';
            end if;


            -- New partitions maintenance

            <<maintenance_loop>>
            for maintenance_rec in

                with clust as
                (
                    -- Getting indices that are suitable for clusterization
                    select
                        p.window_num,
                        p.new_partition_name::regclass::oid as p_oid,
                        obj."schema"::text as p_schema,
                        obj."name"::text as p_name,
                        coalesce(
                                -- indices that already have been used for CLUSTER
                            max(case when i.indisclustered is true then i.indexrelid else null end),
                                -- if no CLUSTER command have been issued yet, then use primary key
                            max(case when i.indisclustered is false and i.indisprimary is true then i.indexrelid else null end),
                                -- if there is no PK for some reason, try to use latest (by OID) non-partial unique index
                            max(case when i.indisclustered is false and i.indisprimary is false and i.indisunique is true then i.indexrelid else null end),
                                -- finally, try to use latest (by OID) non-partial non-unique index
                            max(case when i.indisclustered is false and i.indisprimary is false and i.indisunique is false then i.indexrelid else null end)
                        ) as cluster_index_oid
                    from pg_temp.repartitioning_plan p
                    left join lateral pg_catalog.pg_identify_object('pg_catalog.pg_class'::regclass, p.new_partition_name::regclass, 0)
                        as obj ("type", "schema", "name", "identity")
                        on 1 = 1
                    left join pg_catalog.pg_index i
                        on i.indrelid = p.new_partition_name::regclass
                        and i.indisvalid is true -- here and below filtering only active indices
                        and i.indisready is true
                        and i.indislive is true
                        and i.indcheckxmin is false
                        and i.indpred is null -- excluding partial indices
                    where
                        p.new_partition_name is not null
                    group by
                        1, 2, 3, 4
                ),

                corr as
                (
                    -- Getting relations that are no longer clustered
                    select
                        c.p_oid
                    from clust c
                    join pg_catalog.pg_index i
                        on i.indexrelid = c.cluster_index_oid
                    left join lateral unnest(i.indkey[:i.indnkeyatts]) with ordinality k (keys, kn)
                        on 1 = 1
                    join pg_catalog.pg_attribute a
                        on c.p_oid = a.attrelid
                        and k.keys = a.attnum
                    join pg_catalog.pg_stats s
                        on quote_ident(c.p_schema) = quote_ident(s.schemaname)
                        and quote_ident(c.p_name) = quote_ident(s.tablename)
                        and quote_ident(a.attname) = quote_ident(s.attname)
                    where
                        k.kn = 1
                        and s.correlation < 1
                        and a.attisdropped is false
                        and a.attnum > 0
                ),

                dead_tup as
                (
                    -- Getting relations with dead tuples
                    select
                        c.p_oid
                    from clust c
                    join pg_catalog.pg_stat_user_tables sut
                        on c.p_oid = sut.relid
                    where
                        sut.n_dead_tup > 0
                )

                select
                    cl.window_num,
                    cl.p_oid,
                    cl.p_schema,
                    cl.p_name,
                    case
                        when coalesce(cr.p_oid, dt.p_oid) is not null
                            then quote_ident(c.relname)
                        else null::text
                    end as cluster_index_name
                from clust cl
                left join pg_catalog.pg_class c
                    on cl.cluster_index_oid = c."oid"
                left join corr cr
                    on cl.p_oid = cr.p_oid
                left join dead_tup dt
                    on cl.p_oid = dt.p_oid
                order by
                    cl.window_num,
                    cl.p_schema,
                    cl.p_name

            loop

                raise notice '% - Start of maintenance for partition %.%',
                    clock_timestamp(),
                    maintenance_rec.p_schema,
                    maintenance_rec.p_name;

                    -- Alter fillfactor of inactive partitions' indices to 100

                if
                    inactive_index_fillfactor_100_flg is true
                    and maintenance_rec.window_num >= 0 -- only archive and partitions before split_point
                    and maintenance_rec.window_num < 1000
                then

                    fillfactor_qry := '';

                    <<fillfactor_loop>>
                    for fillfactor_rec in
                        select
                            quote_ident(c.relnamespace::regnamespace::text) || '.' || quote_ident(c.relname::text) as index_name
                        from pg_catalog.pg_class c
                        join pg_catalog.pg_index i
                            on c."oid" = i.indexrelid
                        join pg_catalog.pg_am a
                            on a."oid" = c.relam
                        where
                            i.indrelid = maintenance_rec.p_oid
                            and a.amname in ('hash', 'btree', 'gist', 'spgist')
                    loop
                        fillfactor_qry := fillfactor_qry
                            || 'alter index if exists ' || fillfactor_rec.index_name
                            || E'\n\t' || 'set (fillfactor = 100);' || repeat(E'\n', 2);
                    end loop fillfactor_loop;

                    fillfactor_qry = rtrim(fillfactor_qry, E'\n');

                    raise notice '%',
                        'ALTER INDEX (fillfactor = 100) query:' || repeat(E'\n', 2) || fillfactor_qry;

                    execute fillfactor_qry;

                end if;

                -- Clustering new partitions if needed
                if cluster_new_partitions_flg is true and maintenance_rec.cluster_index_name is not null then

                    cluster_qry := format(
                            'cluster %I.%I using %I;',
                            maintenance_rec.p_schema,
                            maintenance_rec.p_name,
                            maintenance_rec.cluster_index_name
                        );

                    raise notice '%',
                        'CLUSTER query:' || repeat(E'\n', 2) || cluster_qry;

                    execute cluster_qry;

                end if;

                raise notice '% - End of maintenance for partition %.%',
                    clock_timestamp(),
                    maintenance_rec.p_schema,
                    maintenance_rec.p_name;

            end loop maintenance_loop;

            -- Analyzing root table

            raise notice '% - Start of analyzing root table', clock_timestamp();

            execute 'analyze ' || root_table_oid::regclass::text;

            raise notice '% - End of analyzing root table', clock_timestamp();

        end if;


        -- Returning results from pg_temp.repartitioning_plan
        return query
            select
                *
            from pg_temp.repartitioning_plan
            order by
                window_num,
                partition_num,
                partition_merge_type,
                cur_partition_name;

        return;

    end;

$func$;