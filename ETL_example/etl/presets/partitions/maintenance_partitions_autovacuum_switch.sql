create or replace function public.maintenance_partitions_autovacuum_switch
(
    root_table_oid oid,
    enable_vacuum boolean
)
returns text
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
    declare
        av_qry text;

    begin

        with recursive parts as
            (
                -- For partitioned tables getting all their partitions
                select
                    0::smallint as lvl,
                    c."oid",
                    c.relnamespace::regnamespace::text as nspname,
                    c.relname,
                    c.relkind
                from pg_catalog.pg_class c
                where
                    c."oid" = root_table_oid
                    and c.relkind = 'p'

                union all

                select
                    p.lvl + 1::smallint as lvl,
                    inh.inhrelid as "oid",
                    c.relnamespace::regnamespace::text as nspname,
                    c.relname,
                    c.relkind
                from parts p
                join pg_catalog.pg_inherits inh
                    on inh.inhparent = p."oid"
                join pg_catalog.pg_class c
                    on inh.inhrelid = c."oid"
            )

        select
            string_agg
            (
                'alter '
                    || case when relkind != 'f' then '' else 'foreign' || ' ' end
                    || 'table if exists ' || quote_ident(nspname) || '.' || quote_ident(relname)
                    || ' set (autovacuum_enabled = ' || quote_literal(enable_vacuum)
                    || ', toast.autovacuum_enabled = ' || quote_literal(enable_vacuum) || ');',
                repeat(E'\n', 2)
            )
        into av_qry
        from parts
        where
            lvl != 0;

        if av_qry is not null then
            execute av_qry;
            return 'Autovacuum for partitions of table '
                || root_table_oid::regclass::text || ' has been '
                || case when enable_vacuum is true then 'enabled' else 'disabled' end;
        else
            raise exception '%',
                'No partitioned table with OID = ' || root_table_oid || ' has been found'
                using errcode = 'invalid_parameter_value';
        end if;

    end;
$func$;