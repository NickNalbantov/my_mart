-- 1. Partitions for 12 months

drop table if exists public.test_month_parts cascade;

create table public.test2 (id int4, created_at timestamp)
partition by range (extract(month from (created_at)));

select *
from get_range_partitions_ddl
(
    root_table_oid := 'public.test_month_parts'::regclass,
    split_point := '12',
    split_interval := '0',
    merge_windows := array['11'],
    merge_intervals := array['1'],
    archive_partition_flg := false,
    split_unbounded_flg := false,
    default_partition_flg := false
);


-- 2. Full test

drop table if exists public.test cascade;

create table public.test (id int4, created_at timestamp)
partition by range (created_at);

select *
from get_range_partitions_ddl
    (
        root_table_oid := 'public.test'::regclass,
        split_point := '2024-09-01',
        split_interval := '1 month',
        merge_windows := array['6 month', '6 month', '3 year'],
        merge_intervals := array['1 month', '3 month', '1 year'],
        archive_partition_flg := true,
        archive_point := '2021-01-01',
        split_unbounded_flg := true,
        default_partition_flg := true
    );

create view test_view_on_part as
select *
from public.test_rng_prt_default;

create view test_view_on_part_2 as
select a.id * 2 as double_id
from public.test_rng_prt_default a
join public.test_rng_prt_minvalue_2021_01_01 b using (id);

alter table if exists public.test_rng_prt_minvalue_2021_01_01
with (autovacuum_enabled = false);

create materialized view test_mat_view_on_part as
select *
from public.test_rng_prt_default
with data;

alter table public.test_rng_prt_default add constraint test_constr check (id > 1000);

create index test_idx_on_part on public.test_rng_prt_default (id);

create function test_trigger_func()
returns trigger
language plpgsql
as
$$
    begin
        raise notice 'test';
    end;
$$;

create trigger test_trigger_on_part before update
on public.test_rng_prt_default
execute function test_trigger_func();

create procedure test_atomic_proc_on_part()
language sql
begin atomic
    select * from public.test_rng_prt_default;
end;

    --To test default more thoroughly
alter table public.test_rng_prt_default rename to test_rng_prt_default_2;


    -- Partitions comparison

drop table if exists pg_temp.repartitioning_plan cascade;

create temporary table if not exists pg_temp.repartitioning_plan
    on commit drop
as

with recursive

new_ddl as
(
    select
        window_num,
        partition_num,
        partition_key_expr,
        partition_key_type,
        value_from,
        value_to,
        partition_name,
        partition_table_type,
        tablespace_name,
        local_tab_options,
        server_name,
        foreign_tab_options,
        partition_ddl
    from public.get_range_partitions_ddl
        (
            root_table_oid := 'public.test'::regclass,
            split_point := '2024-10-01',
            split_interval := '1 month',
            merge_windows := array['6 month', '6 month', '2 year'],
            merge_intervals := array['1 month', '3 month', '1 year'],
            archive_partition_flg := true,
            archive_point := '2022-01-01',
            split_unbounded_flg := true,
            default_partition_flg := true
        )
),

current_settings as
(
    select
        partition_oid,
        case
            when relpartbound = 'DEFAULT' then 'default'::text
            when regexp_substr(relpartbound, '(?<=FROM \()(.*)(?=\) TO)') = 'MINVALUE' then 'minvalue'::text
            else regexp_substr(relpartbound, '(?<=FROM \('')(.*)(?=''\) TO)')::text
        end as value_from,
        case
            when relpartbound = 'DEFAULT' then 'default'::text
            when regexp_substr(relpartbound, '(?<=TO \()(.*)(?=\))') = 'MAXVALUE' then 'maxvalue'::text
            else regexp_substr(relpartbound, '(?<=TO \('')(.*)(?=''\))')::text
        end as value_to,
        partition_name::text as partition_name,
        partition_table_type::text as partition_table_type,
        tablespace_name::text as tablespace_name,
        local_tab_options::jsonb as local_tab_options,
        server_name::text as server_name,
        foreign_tab_options::jsonb as foreign_tab_options
    from
    (
        select
            p."oid" as partition_oid,
            pg_catalog.pg_get_expr(p.relpartbound, p."oid", true) as relpartbound,
            quote_ident(p.relnamespace::regnamespace::text)
                || '.' || quote_ident(p.relname::text) as partition_name,
            case
                when p.relkind = 'f' then 'foreign'
                else
                    case p.relpersistence
                        when 't' then 'temporary'
                        when 'u' then 'unlogged'
                        else null
                    end
            end as partition_table_type,
            coalesce(quote_ident(ts.spcname), 'pg_default') as tablespace_name,
            jsonb_object_agg(lo.option_name, lo.option_value) filter (where lo.option_name is not null) over w as local_tab_options,
            quote_ident(fs.srvname) as server_name,
            jsonb_object_agg(fo.option_name, fo.option_value) filter (where fo.option_name is not null) over w as foreign_tab_options
        from pg_catalog.pg_class c
        left join pg_catalog.pg_inherits i
            on c."oid" = i.inhparent
        left join pg_catalog.pg_class p
            on i.inhrelid = p."oid"
        left join pg_catalog.pg_tablespace ts
            on p.reltablespace = ts."oid"
        left join pg_catalog.pg_foreign_table ft
            on p."oid" = ft.ftrelid
        left join pg_catalog.pg_foreign_server fs
            on ft.ftserver = fs."oid"
        left join lateral pg_catalog.pg_options_to_table(p.reloptions) lo (option_name, option_value)
            on true
        left join lateral pg_catalog.pg_options_to_table(ft.ftoptions) fo (option_name, option_value)
            on true
        where
            c."oid" = 'public.test'::regclass
        window w as (partition by p."oid")
    ) a
),

dependency_pair as
(
    select
        dep.objid,
        dep.refobjid,
        obj."identity" as obj_name,
        obj."type" as object_type,
        refobj."type" as refobj_type
    from pg_catalog.pg_depend dep
    join current_settings c
        on c.partition_oid = dep.refobjid
    cross join lateral pg_catalog.pg_identify_object(dep.classid, dep.objid, 0) obj ("type", "schema", "name", "identity")
    cross join lateral pg_catalog.pg_identify_object(dep.refclassid, dep.refobjid, 0) refobj ("type", "schema", "name", "identity")
    where
        dep.deptype in ('n', 'a', 'i')

    union

    select
        dep.refobjid as objid,
        dep.objid as refobjid,
        obj."identity" as obj_name,
        obj."type" as object_type,
        refobj."type" as refobj_type
    from pg_catalog.pg_depend dep
    join pg_catalog.pg_rewrite r
        on r.oid = dep.objid
        and r.ev_class = dep.refobjid
    cross join lateral pg_catalog.pg_identify_object(dep.classid, dep.objid, 0) refobj ("type", "schema", "name", "identity")
    cross join lateral pg_catalog.pg_identify_object(dep.refclassid, dep.refobjid, 0) obj ("type", "schema", "name", "identity")
    where
        dep.deptype in ('n', 'a', 'i')
),

dependency_hierarchy as
(
    select
        0 as lvl,
        root.refobjid as objid,
        root.obj_name,
        root.refobj_type as obj_type,
        array[root.refobjid] as dependency_oid_chain
    from dependency_pair root
    where not exists
        (
            select
            from dependency_pair branch
            where
                branch.objid = root.refobjid
        )

    union

    select
        parent.lvl + 1 as lvl,
        child.objid as objid,
        child.obj_name,
        child.object_type as obj_type,
        parent.dependency_oid_chain || child.objid as dependency_oid_chain
    from dependency_pair child
    join dependency_hierarchy parent
        on parent.objid = child.refobjid
    where
        child.objid != all(parent.dependency_oid_chain)
),

dep_ddl as
(
    select
        c.partition_oid,
        string_agg
        (
            case
                when dh.obj_type = 'view'
                    then 'DROP VIEW IF EXISTS ' || dh.obj_name || ' CASCADE;'
                when dh.obj_type = 'materialized view'
                    then 'DROP MATERIALIZED VIEW IF EXISTS ' || dh.obj_name || ' CASCADE;'
                when dh.obj_type = 'table constraint'
                    then
                        'ALTER '
                        || case when c.partition_table_type is distinct from 'foreign' then '' else upper(c.partition_table_type) || ' ' end
                        || 'TABLE IF EXISTS ' || c.partition_oid::regclass::text
                        || ' DROP CONSTRAINT IF EXISTS ' || substring(dh.obj_name, '^.+(?=( on ))') || ' CASCADE;'
                when dh.obj_type = 'index'
                    then 'DROP INDEX IF EXISTS ' || dh.obj_name || ' CASCADE;'
                when dh.obj_type = 'trigger'
                    then 'DROP TRIGGER IF EXISTS ' || dh.obj_name || ' CASCADE;'
                when dh.obj_type in ('function', 'procedure')
                    then 'DROP ROUTINE IF EXISTS ' || dh.objid::regprocedure || ' CASCADE;' -- 'begin atomic' SQL routines
            end,
            repeat(E'\n', 2)
            order by
                dh.obj_type,
                dh.objid
        ) as existing_dependencies_drop,
        string_agg
        (
            case
                when dh.obj_type = 'view'
                    then 'CREATE OR REPLACE VIEW ' || dh.obj_name || ' AS' || E'\n' || pg_catalog.pg_get_viewdef(dh.objid)
                when dh.obj_type = 'materialized view'
                    then 'CREATE MATERIALIZED VIEW IF NOT EXISTS ' || dh.obj_name || ' AS' || E'\n' || pg_catalog.pg_get_viewdef(dh.objid)
                when dh.obj_type = 'table constraint'
                    then
                        'ALTER '
                        || case when c.partition_table_type is distinct from 'foreign' then '' else upper(c.partition_table_type) || ' ' end
                        || 'TABLE IF EXISTS ' || c.partition_oid::regclass::text
                        || ' ADD CONSTRAINT ' || substring(dh.obj_name, '^.+(?=( on ))') || ' ' || pg_catalog.pg_get_constraintdef(dh.objid) || ';'
                when dh.obj_type = 'index'
                    then regexp_replace(pg_catalog.pg_get_indexdef(dh.objid), 'CREATE (UNIQUE |)INDEX', 'CREATE \1INDEX IF NOT EXISTS') || ';'
                when dh.obj_type = 'trigger'
                    then regexp_replace(pg_catalog.pg_get_triggerdef(dh.objid), 'CREATE (CONSTRAINT |)TRIGGER', 'CREATE OR REPLACE \1TRIGGER') || ';'
                when dh.obj_type in ('function', 'procedure')
                    then pg_catalog.pg_get_functiondef(dh.objid) || ';' -- 'begin atomic' SQL routines
            end,
            repeat(E'\n', 2)
            order by
                dh.obj_type,
                dh.objid
        ) as existing_dependencies_ddl
    from dependency_hierarchy dh
    join current_settings c
        on c.partition_oid = any(dh.dependency_oid_chain)
    where
        dh.lvl != 0
        and dh.obj_type not in ('type', 'rule')
    group by
        c.partition_oid
),

partitions_compare as
(
select
    n.window_num,
    n.partition_num,
    n.partition_key_expr,
    n.partition_key_type,
    n.value_from as new_value_from,
    n.value_to as new_value_to,
    n.partition_name as new_partition_name,
    n.partition_table_type as new_partition_table_type,
    n.tablespace_name as new_tablespace_name,
    n.local_tab_options as new_local_tab_options,
    n.server_name as new_server_name,
    n.foreign_tab_options as new_foreign_tab_options,
    n.partition_ddl as new_partition_ddl,
    c.value_from as cur_value_from,
    c.value_to as cur_value_to,
    c.partition_name as cur_partition_name,
    c.partition_table_type as cur_partition_table_type,
    c.tablespace_name as cur_tablespace_name,
    c.local_tab_options as cur_local_tab_options,
    c.server_name as cur_server_name,
    c.foreign_tab_options as cur_foreign_tab_options,
    dd.existing_dependencies_drop,
    dd.existing_dependencies_ddl
from new_ddl n
full join current_settings c
    on c.value_from = n.value_from
    and c.value_to = n.value_to
left join dep_ddl dd
    on c.partition_oid = dd.partition_oid
),

sc_1_2 as
(
    -- Partition merge type 1 - partitions borders are the same, but some options are different
        -- Try to ALTER partition options; if not possible then detach old -> attach new -> copy data

    select
        1::smallint as partition_merge_type,
        n.window_num,
        n.partition_num,
        n.partition_key_expr,
        n.partition_key_type,
        n.new_value_from,
        n.new_value_to,
        n.new_partition_name,
        n.new_partition_table_type,
        n.new_tablespace_name,
        n.new_local_tab_options,
        n.new_server_name,
        n.new_foreign_tab_options,
        n.new_partition_ddl,
        n.cur_value_from,
        n.cur_value_to,
        n.cur_partition_name,
        n.cur_partition_table_type,
        n.cur_tablespace_name,
        n.cur_local_tab_options,
        n.cur_server_name,
        n.cur_foreign_tab_options,
        n.existing_dependencies_drop,
        n.existing_dependencies_ddl
    from partitions_compare n
    where exists
        (
            select
            from partitions_compare c
            where
                n.new_value_from = c.cur_value_from -- no casts here, with minvalue / maxvalue, but without default
                and n.new_value_to = c.cur_value_to
                and 'default'::text != all(array[
                        coalesce(n.new_value_from, 'N/A'),
                        coalesce(n.new_value_to, 'N/A'),
                        coalesce(c.cur_value_from, 'N/A'),
                        coalesce(c.cur_value_to, 'N/A')
                    ]::text[])
                and
                    (
                        n.new_partition_name,
                        n.new_partition_table_type,
                        n.new_tablespace_name,
                        n.new_local_tab_options,
                        n.new_server_name,
                        n.new_foreign_tab_options
                    )
                    is distinct from
                    (
                        c.cur_partition_name,
                        c.cur_partition_table_type,
                        c.cur_tablespace_name,
                        c.cur_local_tab_options,
                        c.cur_server_name,
                        c.cur_foreign_tab_options
                    )
        )

    union all

    -- Partition merge type 2 - partitions borders are different, but overlapping
        -- Detach old -> attach new -> copy data

        -- 2.1 - Normal values of partitions' bounds

    select
        2::smallint as partition_merge_type,
        n.window_num,
        n.partition_num,
        n.partition_key_expr,
        n.partition_key_type,
        n.new_value_from,
        n.new_value_to,
        n.new_partition_name,
        n.new_partition_table_type,
        n.new_tablespace_name,
        n.new_local_tab_options,
        n.new_server_name,
        n.new_foreign_tab_options,
        n.new_partition_ddl,
        c.cur_value_from,
        c.cur_value_to,
        c.cur_partition_name,
        c.cur_partition_table_type,
        c.cur_tablespace_name,
        c.cur_local_tab_options,
        c.cur_server_name,
        c.cur_foreign_tab_options,
        c.existing_dependencies_drop,
        c.existing_dependencies_ddl
    from partitions_compare n
    join partitions_compare c
        on 'default'::text != all(array[
                coalesce(n.new_value_from, 'N/A'),
                coalesce(n.new_value_to, 'N/A'),
                coalesce(c.cur_value_from, 'N/A'),
                coalesce(c.cur_value_to, 'N/A')
            ]::text[])
        and 'minvalue'::text != all(array[
                coalesce(n.new_value_from, 'N/A'),
                coalesce(n.new_value_to, 'N/A'),
                coalesce(c.cur_value_from, 'N/A'),
                coalesce(c.cur_value_to, 'N/A')
            ]::text[])
        and 'maxvalue'::text != all(array[
                coalesce(n.new_value_from, 'N/A'),
                coalesce(n.new_value_to, 'N/A'),
                coalesce(c.cur_value_from, 'N/A'),
                coalesce(c.cur_value_to, 'N/A')
            ]::text[])
        and n.new_value_from::timestamp without time zone < c.cur_value_to::timestamp without time zone -- strict inequality here, because partitions' upper bound is exclusive
        and c.cur_value_from::timestamp without time zone < n.new_value_to::timestamp without time zone
        and not -- no casts here, without minvalue / maxvalue
            (
                n.new_value_from = c.cur_value_from
                and n.new_value_to = c.cur_value_to
            )

    union all

        -- 2.2 - maxvalue / minvalue bounds

    select
        2::smallint as partition_merge_type,
        n.window_num,
        n.partition_num,
        n.partition_key_expr,
        n.partition_key_type,
        n.new_value_from,
        n.new_value_to,
        n.new_partition_name,
        n.new_partition_table_type,
        n.new_tablespace_name,
        n.new_local_tab_options,
        n.new_server_name,
        n.new_foreign_tab_options,
        n.new_partition_ddl,
        c.cur_value_from,
        c.cur_value_to,
        c.cur_partition_name,
        c.cur_partition_table_type,
        c.cur_tablespace_name,
        c.cur_local_tab_options,
        c.cur_server_name,
        c.cur_foreign_tab_options,
        c.existing_dependencies_drop,
        c.existing_dependencies_ddl
    from partitions_compare n
    join partitions_compare c
        on 'default'::text != all(array[
                coalesce(n.new_value_from, 'N/A'),
                coalesce(n.new_value_to, 'N/A'),
                coalesce(c.cur_value_from, 'N/A'),
                coalesce(c.cur_value_to, 'N/A')
            ]::text[])
        and
        (
            'minvalue'::text = any(array[
                coalesce(n.new_value_from, 'N/A'),
                coalesce(c.cur_value_from, 'N/A')
            ]::text[])
            or 'maxvalue'::text = any(array[
                coalesce(n.new_value_to, 'N/A'),
                coalesce(c.cur_value_to, 'N/A')
            ]::text[])
        )
        and
        (
            n.new_value_from = c.cur_value_from  -- no casts here, with minvalue / maxvalue
            or n.new_value_to = c.cur_value_to
        )

)

select
    s.partition_merge_type,
    s.window_num,
    s.partition_num,
    s.partition_key_expr,
    s.partition_key_type,
    s.new_value_from,
    s.new_value_to,
    s.new_partition_name,
    s.new_partition_table_type,
    s.new_tablespace_name,
    s.new_local_tab_options,
    s.new_server_name,
    s.new_foreign_tab_options,
    s.new_partition_ddl,
    s.cur_value_from,
    s.cur_value_to,
    s.cur_partition_name,
    s.cur_partition_table_type,
    s.cur_tablespace_name,
    s.cur_local_tab_options,
    s.cur_server_name,
    s.cur_foreign_tab_options,
    s.existing_dependencies_drop,
    s.existing_dependencies_ddl
from sc_1_2 s

union all

-- Partition merge type 3 - new partitions that are not overlapping with any current partitions
    -- Simple ATTACH PARTITION

select
    3::smallint as partition_merge_type,
    n.window_num,
    n.partition_num,
    n.partition_key_expr,
    n.partition_key_type,
    n.new_value_from,
    n.new_value_to,
    n.new_partition_name,
    n.new_partition_table_type,
    n.new_tablespace_name,
    n.new_local_tab_options,
    n.new_server_name,
    n.new_foreign_tab_options,
    n.new_partition_ddl,
    null::text as cur_value_from,
    null::text as cur_value_to,
    null::text as cur_partition_name,
    null::text as cur_partition_table_type,
    null::text as cur_tablespace_name,
    null::jsonb as cur_local_tab_options,
    null::text as cur_server_name,
    null::jsonb as cur_foreign_tab_options,
    null::text as existing_dependencies_drop,
    null::text as existing_dependencies_ddl
from partitions_compare n
where
    n.cur_partition_name is null
    and not exists
        (
            select
            from sc_1_2 s
            where
                n.window_num = s.window_num
                and n.partition_num = s.partition_num
        )

union all

-- Partition merge type 4 - current partitions that are not overlapping with any new partitions
    -- Simple DETACH PARTITION

select
    4::smallint as partition_merge_type,
    null::smallint as window_num,
    null::smallint as partition_num,
    null::text as partition_key_expr,
    null::text as partition_key_type,
    null::text as new_value_from,
    null::text as new_value_to,
    null::text as new_partition_name,
    null::text as new_partition_table_type,
    null::text as new_tablespace_name,
    null::jsonb as new_local_tab_options,
    null::text as new_server_name,
    null::jsonb as new_foreign_tab_options,
    null::text as new_partition_ddl,
    c.cur_value_from,
    c.cur_value_to,
    c.cur_partition_name,
    c.cur_partition_table_type,
    c.cur_tablespace_name,
    c.cur_local_tab_options,
    c.cur_server_name,
    c.cur_foreign_tab_options,
    c.existing_dependencies_drop,
    c.existing_dependencies_ddl
from partitions_compare c
where
    c.new_partition_name is null
    and not exists
        (
            select
            from sc_1_2 s
            where
                c.cur_partition_name = s.cur_partition_name
        )

union all

-- Partition merge type 5 - default partition
    -- Could not exists in current design / new design only, could exists / not exists in both designs

select
    5::smallint as partition_merge_type,
    coalesce(n.window_num, -1::smallint) as window_num,
    coalesce(n.partition_num, 1::smallint) as partition_num,
    coalesce(n.partition_key_expr, c.partition_key_expr) as partition_key_expr,
    coalesce(n.partition_key_type, c.partition_key_type) as partition_key_type,
    n.new_value_from,
    n.new_value_to,
    n.new_partition_name,
    n.new_partition_table_type,
    n.new_tablespace_name,
    n.new_local_tab_options,
    n.new_server_name,
    n.new_foreign_tab_options,
    n.new_partition_ddl,
    c.cur_value_from,
    c.cur_value_to,
    c.cur_partition_name,
    c.cur_partition_table_type,
    c.cur_tablespace_name,
    c.cur_local_tab_options,
    c.cur_server_name,
    c.cur_foreign_tab_options,
    c.existing_dependencies_drop,
    c.existing_dependencies_ddl
from partitions_compare n
full join partitions_compare c
    on n.new_value_from = c.cur_value_from
where
    'default'::text = any(array[
            coalesce(n.new_value_from, 'N/A'),
            coalesce(n.new_value_to, 'N/A'),
            coalesce(c.cur_value_from, 'N/A'),
            coalesce(c.cur_value_to, 'N/A')
        ]::text[])
    and
        (
            n.new_partition_name,
            n.new_partition_table_type,
            n.new_tablespace_name,
            n.new_local_tab_options,
            n.new_server_name,
            n.new_foreign_tab_options
        )
        is distinct from
        (
            c.cur_partition_name,
            c.cur_partition_table_type,
            c.cur_tablespace_name,
            c.cur_local_tab_options,
            c.cur_server_name,
            c.cur_foreign_tab_options
        );

-- if copy_local_options_flg is true then

    update pg_temp.repartitioning_plan
    set
        new_tablespace_name = coalesce(cur_tablespace_name, new_tablespace_name),
        new_local_tab_options = coalesce(cur_local_tab_options, new_local_tab_options)
    where
        new_partition_table_type is distinct from 'foreign'
        and cur_partition_table_type is distinct from 'foreign';

-- if copy_foreign_options_flg is true then

    update pg_temp.repartitioning_plan
    set
        new_server_name = coalesce(cur_server_name, new_server_name),
        new_foreign_tab_options = coalesce(cur_foreign_tab_options, new_foreign_tab_options)
    where
        new_partition_table_type = 'foreign'
        and cur_partition_table_type = 'foreign';

select distinct
    r.*
from pg_temp.repartitioning_plan_prepare r
where
    (
        r.new_value_from,
        r.new_value_to,
        r.new_partition_name,
        r.new_partition_table_type,
        r.new_tablespace_name,
        r.new_local_tab_options,
        r.new_server_name,
        r.new_foreign_tab_options
    )
    is distinct from
    (
        r.cur_value_from,
        r.cur_value_to,
        r.cur_partition_name,
        r.cur_partition_table_type,
        r.cur_tablespace_name,
        r.cur_local_tab_options,
        r.cur_server_name,
        r.cur_foreign_tab_options
    );