create or replace view etl.category_tree_latest_revision as
    select distinct on (category_id, sub_category_id, nested_sub_category_id)
        id,
        category_id,
        sub_category_id,
        nested_sub_category_id,
        revision,
        category_name,
        sub_category_name,
        nested_sub_category_name,
        category_url,
        sub_category_url,
        nested_sub_category_url
    from public.category_tree
    order by
        category_id,
        sub_category_id,
        nested_sub_category_id,
        revision desc;

create or replace view etl_dev.category_tree_latest_revision as
    select distinct on (category_id, sub_category_id, nested_sub_category_id)
        id,
        category_id,
        sub_category_id,
        nested_sub_category_id,
        revision,
        category_name,
        sub_category_name,
        nested_sub_category_name,
        category_url,
        sub_category_url,
        nested_sub_category_url
    from public.category_tree
    order by
        category_id,
        sub_category_id,
        nested_sub_category_id,
        revision desc;