create table etl_wrk.missing_nest_cats
(
    nested_sub_category_id int primary key,
    nested_sub_category_name text
);

insert into etl_wrk.missing_nest_cats
values
(1022,'Graphic UI'),
(1023,'Wireframe UX'),
(1072,'App Customization'),
(1073,'Bug Fix'),
(1074,'Help/Consultation'),
(2003,'Administration'),
(2040,'Custom Website'),
(2130,'Copywriting'),
(2250,'Game Sessions'),
(2314,'Writing & Translation Projects'),
(2327,'General Project Services'),
(2346,'Skip Tracing'),
(2429,'Language Tutoring'),
(2456,'Data Mining & Scraping'),
(2490,'Composing & Soundtracks'),
(2498,'Coins & Tokens'),
(2500,'Exchange Platforms'),
(2511,'Meal Plans'),
(2512,'Workout Plans'),
(2533,'Smart Contracts'),
(2564,'Model Creation'),
(2565,'Full Project'),
(2572,'Data Formatting & Cleaning'),
(2590,'CI/CD'),
(2591,'Infra as Code'),
(2645,'Financial Modeling'),
(2710,'Other Gaming Services'),
(2754,'Software Management'),
(2902,'Vtuber Avatars'),
(2904,'AI Avatars'),
(2908,'Budgeting & Forecasting'),
(2909,'Financial Modeling'),
(2911,'Stock Analysis'),
(2913,'Valuation'),
(2914,'Mergers & Acquisitions'),
(2915,'Corporate Finance Strategy'),
(2916,'Investors Sourcing'),
(2917,'Funding Pitch Presentations'),
(2918,'Fundraising Consultation'),
(2919,'Fractional CFO Services'),
(2920,'Financial Reporting'),
(2921,'Bookkeeping'),
(2922,'Payroll Management'),
(2923,'Tax Returns'),
(2924,'Tax Identification Services'),
(2925,'Tax Planning'),
(2926,'Tax Compliance'),
(2927,'Tax Exemptions'),
(2928,'Personal Budget Management'),
(2929,'Investments Advisory'),
(2930,'Online Trading Lessons');

insert into public.category_tree
(
    category_id,
    sub_category_id,
    nested_sub_category_id,
    revision,
    category_name,
    sub_category_name,
    nested_sub_category_name,
    category_url,
    sub_category_url,
    nested_sub_category_url
)
    select distinct
        l.category_id,
        l.sub_category_id,
        n.nested_sub_category_id,
        7::int as revision,
        l.category_name,
        l.sub_category_name,
        n.nested_sub_category_name,
        l.category_url,
        l.sub_category_url,
        l.sub_category_url || '/' || translate(replace(lower(
            case
                when n.nested_sub_category_name = 'Fractional CFO Services' then 'Fractional CFO'
                else n.nested_sub_category_name
            end
        ), ' & ', '-and-'), ' /', '--') as nested_sub_category_url
    from etl.metrics m
    join etl_wrk.missing_nest_cats n
        on m.nested_sub_category_id = n.nested_sub_category_id
    join etl.category_tree_latest_revision l
        on m.category_id = l.category_id
        and m.sub_category_id = l.sub_category_id
    where
        m.nested_sub_category_name is null
        and m.nested_sub_category_id is not null;

cluster public.category_tree using uidx_category_tree;

analyze public.category_tree;

drop table if exists etl_wrk.missing_nest_cats cascade;