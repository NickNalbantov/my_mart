-- category_tree

drop table if exists public.category_tree cascade;

create table if not exists public.category_tree
(
    id int generated always as identity primary key,
    category_id int not null,
    sub_category_id int not null,
    nested_sub_category_id int,
    revision int not null,
    category_name text not null,
    sub_category_name text not null,
    nested_sub_category_name text,
    category_url text not null,
    sub_category_url text not null,
    nested_sub_category_url text,
    constraint uidx_category_tree unique (category_id, sub_category_id, nested_sub_category_id, revision)
);

-- merge new revisions
with raw_cats as
(
    select
        replace(
            $${category_tree_json}
        $$, '\u0026', '&')::jsonb as js
),

top_cats as
(
    select
        (c.top ->> 'id')::int as category_id,
        (c.top ->> 'name')::text as category_name,
        c.top -> 'subcategories' as sub_categories
    from raw_cats r
    cross join lateral jsonb_array_elements(r.js -> 'categories') c (top)
),

fin_cats as
(
    select
        t.category_id,
        (s.subcat ->> 'id')::int as sub_category_id,
        (m.max_rev + 1)::int as revision,
        t.category_name,
        'https://www.fiverr.com' || regexp_replace((s.subcat ->> 'url'), '/(\w|-)+$', '') as category_url,
        (s.subcat ->> 'name')::text as sub_category_name,
        'https://www.fiverr.com' || (s.subcat ->> 'url') as sub_category_url
    from top_cats t
    cross join lateral jsonb_array_elements(t.sub_categories) s (subcat)
    cross join (select max(revision) as max_rev from public.category_tree) m
),

raw_nest as
(
    select $${nest_cat_json}$$::jsonb as js
),

nest_cats as
(
    select
        (n.nc_js ->> 'sub_category_id')::int as sub_category_id,
        (n.nc_js ->> 'nested_sub_category_id')::int as nested_sub_category_id,
        n.nc_js ->> 'nested_sub_category_cached_slug' as nested_sub_category_cached_slug,
        n.nc_js ->> 'nested_sub_category_cached_name' as nested_sub_category_name
    from raw_nest r
    cross join lateral jsonb_array_elements(r.js) n (nc_js)
),

fin as
(
    select distinct
        c.category_id,
        c.sub_category_id,
        n.nested_sub_category_id,
        c.revision,
        c.category_name,
        c.sub_category_name,
        n.nested_sub_category_name,
        c.category_url,
        c.sub_category_url,
        c.sub_category_url || '/' || n.nested_sub_category_cached_slug as nested_sub_category_url
    from fin_cats c
    left join nest_cats n using (sub_category_id)
)

merge into public.category_tree c
using fin j on
    c.category_id = j.category_id
    and c.sub_category_id = j.sub_category_id
    and c.nested_sub_category_id is not distinct from j.nested_sub_category_id
    and c.category_name = j.category_name
    and c.sub_category_name = j.sub_category_name
    and c.nested_sub_category_name is not distinct from j.nested_sub_category_name
    and c.category_url = j.category_url
    and c.sub_category_url = j.sub_category_url
    and c.nested_sub_category_url is not distinct from j.nested_sub_category_url
    and c.revision < j.revision
when matched then
    do nothing
when not matched then
    insert
    (
        category_id,
        sub_category_id,
        nested_sub_category_id,
        revision,
        category_name,
        sub_category_name,
        nested_sub_category_name,
        category_url,
        sub_category_url,
        nested_sub_category_url
    )
    values
    (
        j.category_id,
        j.sub_category_id,
        j.nested_sub_category_id,
        j.revision,
        j.category_name,
        j.sub_category_name,
        j.nested_sub_category_name,
        j.category_url,
        j.sub_category_url,
        j.nested_sub_category_url
    );

cluster public.category_tree using uidx_category_tree;

alter table if exists public.category_tree
    replica identity full;

analyze public.category_tree;


/*
-- Nested categories JSON first insert

with raw_nest as
(
    select $${nest_cat_json}$$::jsonb as js
),

nest_cats as
(
    select
        (n.nc_js ->> 'sub_category_id')::int as sub_category_id,
        (n.nc_js ->> 'nested_sub_category_id')::int as nested_sub_category_id,
        n.nc_js ->> 'nested_sub_category_cached_slug' as nested_sub_category_cached_slug,
        n.nc_js ->> 'nested_sub_category_cached_name' as nested_sub_category_name
    from raw_nest
    cross join lateral jsonb_array_elements(raw.js) n (nc_js)
)

insert into public.category_tree_new
(category_id, sub_category_id, nested_sub_category_id, revision, category_name, sub_category_name, nested_sub_category_name, category_url, sub_category_url, nested_sub_category_url)
    select distinct
        c.category_id,
        c.sub_category_id,
        nc.nested_sub_category_id,
        c.revision,
        c.category_name,
        c.sub_category_name,
        nc.nested_sub_category_name,
        regexp_replace(c.sub_category_url, '/(\w|-)+$', '') as category_url,
        c.sub_category_url,
        c.sub_category_url || '/' || nc.nested_sub_category_cached_slug as nested_sub_category_url
    from public.category_tree c
    left join nest_cats nc on c.sub_category_id = nc.sub_category_id
    order by
        category_id,
        sub_category_id,
        nested_sub_category_id,
        revision;
*/