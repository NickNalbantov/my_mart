create or replace function public.maintenance_etl_attributes_stats_cleanup
(
    clean_before_timestamp timestamptz default (current_date::timestamp at time zone 'UTC' - '1 month'::interval)::timestamptz
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        row_cnt bigint;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Deleting old rows

        execute format($exec$
            delete
            from etl_log.etl_attributes_stats
            where
                processed_dttm < %L::timestamptz;
        $exec$,
        clean_before_timestamp
        );

        get diagnostics
            row_cnt = row_count;

        cluster etl_log.etl_attributes_stats using etl_attributes_stats_pkey;

        analyze etl_log.etl_attributes_stats;

        raise notice '% - % rows deleted', clock_timestamp(), row_cnt;

        total_runtime := clock_timestamp() - transaction_timestamp();

        -- returning valid Python dictionary with runtime, row count and 'Success' status
        result := (
            '{"runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '% - %', clock_timestamp(), result;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            total_runtime := clock_timestamp() - transaction_timestamp();

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice '% - %', clock_timestamp(), result;

            return result;

    end;
$func$;