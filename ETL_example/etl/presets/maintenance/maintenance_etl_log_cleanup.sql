create or replace function public.maintenance_etl_log_cleanup
(
    clean_before_timestamp timestamptz default (current_date::timestamp at time zone 'UTC' - '1 month'::interval)::timestamptz
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        dangling_load_id bigint;

        row_cnt bigint;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Looking for load_ids that exists before and after "clean_before_timestamp"

        select distinct
            b.load_id
        into dangling_load_id
        from etl_log.etl_log b
        where
            b.processed_dttm < clean_before_timestamp
            and exists
                (
                    select
                    from etl_log.etl_log a
                    where
                        a.processed_dttm >= clean_before_timestamp
                        and a.load_id = b.load_id
                );

        -- Deleting old rows and rows with dangling load_ids

        execute format($exec$
            delete
            from etl_log.etl_log
            where
                processed_dttm < %L::timestamptz
                or load_id = %L::bigint;
        $exec$,
        clean_before_timestamp,
        dangling_load_id
        );

        get diagnostics
            row_cnt = row_count;

        cluster etl_log.etl_log using etl_log_pkey;

        analyze etl_log.etl_log;

        raise notice '% - % rows deleted', clock_timestamp(), row_cnt;

        total_runtime := clock_timestamp() - transaction_timestamp();

        -- returning valid Python dictionary with runtime, row count and 'Success' status
        result := (
            '{"runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '% - %', clock_timestamp(), result;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            total_runtime := clock_timestamp() - transaction_timestamp();

            -- returning valid Python dictionary with runtime and error status
            result := (
                '{"runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice '% - %', clock_timestamp(), result;

            return result;

    end;
$func$;