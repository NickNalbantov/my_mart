create or replace function public.maintenance_bloat_cleanup
(
    objects jsonb,
    dry_run boolean default false
)
returns table
(
    nspname text,
    relname text,
    cluster_index_name text
)
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
    declare
        obj_rec record;
        analyze_columns text;
        conn_name text := current_database() || '_maintenance_bloat_cleanup_conn';
        server_name text := 'loopback_dblink_' || current_database();

    begin
        /*

        This function has been created for the "maintenance_cleanup" DAG in Airflow.
        It is not intended to be launched manually
        But if you really want to do that, check docs for the DAG: https://gitlab.com/fiverr-dev/data-engineering/airflow/-/blob/main/README.md

        Function call example:

            select *
            from public.maintenance_bloat_cleanup
            (
                objects := $exmpl$
                    [
                        {
                            "schema": "public",
                            "include": ["alembic_version"],
                            "exclude": []
                        },
                        {
                            "schema": "etl_log",
                            "include": ["*"],
                            "exclude": ["etl_attributes_stats"]
                        },
                        {
                            "schema": "auth",
                            "include": ["*"],
                            "exclude": []
                        }
                    ]
                    $exmpl$::jsonb,
                dry_run := true
            );

        JSON schema:
            {
                "type": "array",
                "minItems": 1,
                "uniqueItems": true,
                "unevaluatedItems": false,
                "items":
                    {
                        "type": "object",
                        "minProperties": 3,
                        "maxProperties": 3,
                        "properties":
                            {
                                "schema":
                                    {
                                        "type": "string"
                                    },
                                "include":
                                    {
                                        "type": "array",
                                        "uniqueItems": true,
                                        "unevaluatedItems": false,
                                        "items":
                                            {
                                                "type": "string"
                                            }
                                    },
                                "exclude":
                                    {
                                        "type": "array",
                                        "uniqueItems": true,
                                        "unevaluatedItems": false,
                                        "items":
                                            {
                                                "type": "string"
                                            }
                                    }
                            },
                        "required":
                            [
                                "schema",
                                "include",
                                "exclude"
                            ]
                    }
            }
        */

        -- Temp table with parsing results

        create temporary table if not exists pg_temp.maintenance_bloat_cleanup
            on commit drop
        as
            with recursive obj as
            (
                -- Unnesting relations from "include" list and getting "exclude" list
                    -- If any list contains '*' then this whole list is replaced by array['*']::text[] - '*' overrides all other inputs
                select
                    t.sn,
                    r.rn,
                    t.sch,
                    r.relation,
                    case
                        when '*' = any(t.excluded) then array['*']::text[]
                        else t.excluded
                    end as excluded
                from rows from
                    (
                        jsonb_to_recordset(objects) as ("schema" text, "include" text[], "exclude" text[])
                    ) with ordinality as t (sch, included, excluded, sn)
                cross join lateral unnest
                    (
                        case
                            when '*' = any(t.included) then array['*']::text[]
                            else t.included
                        end
                    ) with ordinality as r (relation, rn)
            ),

            relnames as
            (
                -- Filtering included relations through pg_class and "exclude" list
                    -- Entries from "exclude" list overrides entries from "include"
                    -- '*' in "exclude" list would deny whole schema even if '*' is placed in "include" list
                select
                    o.sn,
                    c."oid",
                    case
                        when o.relation != '*' then o.rn
                        else row_number() over (partition by o.sn order by c.relname)
                    end as rn,
                    n.nspname,
                    c.relname,
                    c.relkind
                from obj o
                join pg_catalog.pg_namespace n
                    on o.sch = n.nspname::text
                join pg_catalog.pg_class c
                    on c.relnamespace = n."oid"
                    and o.relation in (c.relname, '*')
                    and c.relname != all(o.excluded)
                where
                    c.relkind in ('r', 'p', 'm')
                    and o.excluded != array['*']::text[]
                    and c.relispartition is false
            ),

            parts as
            (
                -- For partitioned tables getting all their partitions
                select
                    r.sn,
                    r.rn,
                    case r.relkind
                        when 'p' then 0
                        else -1
                    end as pn,
                    r."oid",
                    r.nspname,
                    r.relname
                from relnames r

                union all

                select
                    p.sn,
                    p.rn,
                    p.pn + 1 as pn,
                    inh.inhrelid as "oid",
                    c.relnamespace::regnamespace::text as nspname,
                    c.relname
                from parts p
                join pg_catalog.pg_inherits inh
                    on inh.inhparent = p."oid"
                join pg_catalog.pg_class c
                    on inh.inhrelid = c."oid"
            ),

            clust as
            (
                -- Getting indices that are suitable for clusterization
                select
                    p.sn,
                    p.rn,
                    p.pn,
                    p."oid",
                    p.nspname,
                    p.relname,
                    coalesce(
                            -- indices that already have been used for CLUSTER
                        max(case when i.indisclustered is true then i.indexrelid else null end),
                            -- if no CLUSTER command have been issued yet, then use primary key
                        max(case when i.indisclustered is false and i.indisprimary is true then i.indexrelid else null end),
                            -- if there is no PK for some reason, try to use latest (by OID) non-partial unique index
                        max(case when i.indisclustered is false and i.indisprimary is false and i.indisunique is true then i.indexrelid else null end),
                            -- finally, try to use latest (by OID) non-partial non-unique index
                        max(case when i.indisclustered is false and i.indisprimary is false and i.indisunique is false then i.indexrelid else null end)
                    ) as cluster_index_oid
                from parts p
                left join pg_catalog.pg_index i
                    on i.indrelid = p."oid"
                    and i.indisvalid is true -- here and below filtering only active indices
                    and i.indisready is true
                    and i.indislive is true
                    and i.indcheckxmin is false
                    and i.indpred is null -- excluding partial indices
                where
                    p.pn != 0 -- excluding partitioned root tables
                group by
                    p.sn,
                    p.rn,
                    p.pn,
                    p."oid",
                    p.nspname,
                    p.relname
            ),

            corr as
            (
                -- Getting relations that are no longer clustered
                select
                    c."oid"
                from clust c
                join pg_catalog.pg_index i
                    on i.indexrelid = c.cluster_index_oid
                left join lateral unnest(i.indkey[:i.indnkeyatts]) with ordinality k (keys, kn)
                    on 1 = 1
                join pg_catalog.pg_attribute a
                    on c."oid" = a.attrelid
                    and k.keys = a.attnum
                join pg_catalog.pg_stats s
                    on quote_ident(c.nspname) = quote_ident(s.schemaname)
                    and quote_ident(c.relname) = quote_ident(s.tablename)
                    and quote_ident(a.attname) = quote_ident(s.attname)
                where
                    k.kn = 1
                    and s.correlation not in (1, -1) -- asc and desc order of indices
                    and a.attisdropped is false
                    and a.attnum > 0
            ),

            dead_tup as
            (
                -- Getting relations with dead tuples
                select
                    c."oid"
                from clust c
                join pg_catalog.pg_stat_user_tables sut
                    on c."oid" = sut.relid
                where
                    sut.n_dead_tup > 0
            ),

            obj_types as
            (
                select
                    c."oid",
                    case
                        when array_agg(a.attname order by a.attnum)::text[] = array['id', 'raw']::text[]
                            then 'raw'::text
                        -- more object types can be added here
                        else
                            null::text
                    end as obj_type
                from clust c
                join pg_catalog.pg_attribute a
                    on c."oid" = a.attrelid
                where
                    a.attisdropped is false
                    and a.attnum > 0
                group by
                    c."oid"
            )

            select
                row_number() over (order by cl.sn, cl.rn, cl.pn, cl."oid") as rn,
                cl.nspname,
                cl.relname,
                c.relname as cluster_index_name,
                ot.obj_type
            from clust cl
            left join pg_catalog.pg_class c
                on cl.cluster_index_oid = c."oid"
            left join corr cr
                on cl."oid" = cr."oid"
            left join dead_tup dt
                on cl."oid" = dt."oid"
            left join obj_types ot
                on cl."oid" = ot."oid"
            where
                coalesce(cr."oid", dt."oid") is not null;

        -- Executing the maintenance

            -- dblink connection is needed to autonomously commit every CLUSTER / ANALYZE command from SECURITY DEFINER routine
        if conn_name = any(public.dblink_get_connections()) then
            perform public.dblink_disconnect(conn_name);
            perform public.dblink_connect(conn_name, server_name);
        else
            perform public.dblink_connect(conn_name, server_name);
        end if;

        for obj_rec in
            select
                m.nspname,
                m.relname,
                m.cluster_index_name,
                m.obj_type
            from pg_temp.maintenance_bloat_cleanup m
            order by
                m.rn

            loop

                -- cluster

                if obj_rec.cluster_index_name is not null then

                    if dry_run is false then
                        perform public.dblink_exec(
                            conn_name,
                            format(
                                'cluster %I.%I using %I;',
                                obj_rec.nspname,
                                obj_rec.relname,
                                obj_rec.cluster_index_name
                            )
                        );
                    end if;

                    raise notice '% - %.% has been clustered', clock_timestamp(), obj_rec.nspname, obj_rec.relname;

                else
                    raise warning 'Relation %.% cannot be clustered because it doesn''t have any non-partial valid indices', obj_rec.nspname, obj_rec.relname;
                end if;

                -- analyze

                if obj_rec.obj_type = 'raw' then
                    analyze_columns = ' (id);';
                else
                    analyze_columns := ';';
                end if;

                if dry_run is false then
                    perform public.dblink_exec(
                        conn_name,
                        format(
                            'analyze %I.%I%s',
                            obj_rec.nspname,
                            obj_rec.relname,
                            analyze_columns
                        )
                    );
                end if;

                raise notice '% - %.% has been analyzed', clock_timestamp(), obj_rec.nspname, obj_rec.relname;

            end loop;

        if conn_name = any(public.dblink_get_connections()) then
            perform public.dblink_disconnect(conn_name);
        end if;

        -- Returning the temp table

        return query
            select
                m.nspname::text as nspname,
                m.relname::text as relname,
                m.cluster_index_name::text as cluster_index_name
            from pg_temp.maintenance_bloat_cleanup m
            order by
                m.rn;

        return;

    end;
$func$;