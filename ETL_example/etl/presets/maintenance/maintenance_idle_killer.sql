create or replace function public.maintenance_idle_killer()
returns table
(
    rolename text,
    cnt_idle smallint
)
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$$

    declare
        etl_meta_rec record;

    begin

        if current_database() = 'raw_1' then
            select
                load_id,
                case
                    when
                        src_name = 'af_mj_fin_etl_statistics'
                        and log_type = 'NOTICE'
                        and op_type = 'end'
                    then
                        true
                    else
                        false
                end as etl_finished_flg
            into etl_meta_rec
            from etl_log.last_run_log
            order by
                processed_dttm desc
            limit 1;
        end if;

        return query
            select
                usename::text as rolename,
                count(*)::smallint as cnt_idle
            from
            (
                select
                    a.usename,
                    pg_terminate_backend(a.pid) as res
                from pg_catalog.pg_stat_activity a
                where
                    a.state = any(array[
                            'idle',
                            'idle in transaction',
                            'idle in transaction (aborted)'
                        ])
                    and a.usename != all(array -- don't try to kill any admin processes
                        [
                            'rdsadmin',
                            'rdsproxyadmin',
                            'airflow_admin'
                        ])
                    and a.usename is not null
                    and a.pid != pg_backend_pid()
                    and
                        (
                            (
                                a.application_name != all(array
                                    [
                                        'Maintenance idle killer',
                                        'Maintenance cleanup',
                                        'loopback_dblink_' || current_database()
                                    ])
                                -- killing only idle connects older than pool_recycle period in backend's SQL Alchemy
                                and clock_timestamp() - a.state_change > interval '5 minutes'
                            )
                            or clock_timestamp() - a.state_change > interval '30 minutes'
                        )
                    and -- excluding last unfinished ETL process if it exists
                        (
                            substring(a.application_name, '\d{14}$')::bigint is null
                            or
                            (
                                etl_meta_rec.load_id = substring(a.application_name, '\d{14}$')::bigint
                                and etl_meta_rec.etl_finished_flg is true
                            )
                        )
            )
            where
                res is true
            group by
                usename;

        return;

    end;
$$;