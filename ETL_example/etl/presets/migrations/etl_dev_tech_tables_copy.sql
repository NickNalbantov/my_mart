-- gig_status_history

begin;

    drop table if exists etl_dev.gig_status_history cascade;

    create table if not exists etl_dev.gig_status_history
        (like etl.gig_status_history including all excluding indexes excluding statistics);

    insert into etl_dev.gig_status_history
        select *
        from etl.gig_status_history;

    create unique index if not exists uidx_gig_status_history_valid_from
        on etl_dev.gig_status_history (gig_id, valid_from_dttm);
    create index if not exists idx_gig_status_history_gig_id_valid_to
        on etl_dev.gig_status_history (gig_id, valid_to_dttm);
    create index if not exists idx_gig_status_history_valid_from
        on etl_dev.gig_status_history (valid_from_dttm desc);
    create index if not exists idx_gig_status_history_valid_to
        on etl_dev.gig_status_history (valid_to_dttm desc);
    create index if not exists idx_gig_status_history_load_id
        on etl_dev.gig_status_history (load_id desc);

    cluster etl_dev.gig_status_history using uidx_gig_status_history_valid_from;

    create statistics if not exists etl_dev.gig_status_history_id_dttm
    on gig_id, valid_from_dttm, valid_to_dttm
    from etl_dev.gig_status_history;

    analyze etl_dev.gig_status_history;

commit;


-- seller_status_history

begin;

    drop table if exists etl_dev.seller_status_history cascade;

    create table if not exists etl_dev.seller_status_history
        (like etl.seller_status_history including all excluding indexes excluding statistics);

    insert into etl_dev.seller_status_history
        select *
        from etl.seller_status_history;

    create unique index if not exists uidx_seller_status_history_valid_from
        on etl_dev.seller_status_history (seller_id, valid_from_dttm);
    create index if not exists idx_seller_status_history_seller_id_valid_to
        on etl_dev.seller_status_history (seller_id, valid_to_dttm);
    create index if not exists idx_seller_status_history_valid_from
        on etl_dev.seller_status_history (valid_from_dttm desc);
    create index if not exists idx_seller_status_history_valid_to
        on etl_dev.seller_status_history (valid_to_dttm desc);
    create index if not exists idx_seller_status_history_load_id
        on etl_dev.seller_status_history (load_id desc);

    cluster etl_dev.seller_status_history using uidx_seller_status_history_valid_from;

    create statistics if not exists etl_dev.seller_status_history_id_dttm
    on seller_id, valid_from_dttm, valid_to_dttm
    from etl_dev.seller_status_history;

    analyze etl_dev.seller_status_history;

commit;


-- gig_reviews_agg

begin;

    drop table if exists etl_dev.gig_reviews_agg cascade;

    create table if not exists etl_dev.gig_reviews_agg
        (like etl.gig_reviews_agg including all excluding indexes excluding statistics);

    insert into etl_dev.gig_reviews_agg
    select *
    from etl.gig_reviews_agg;

    alter table if exists etl_dev.gig_reviews_agg add primary key (gig_id);
    create unique index if not exists uidx_gig_reviews_agg_gig_id_last_fiverr_created_at
        on etl_dev.gig_reviews_agg (gig_id, last_fiverr_created_at desc);
    create index if not exists idx_gig_reviews_agg_last_fiverr_created_at
        on etl_dev.gig_reviews_agg (last_fiverr_created_at desc);
    create index if not exists idx_gig_reviews_agg_load_id
        on etl_dev.gig_reviews_agg (load_id desc);

    cluster etl_dev.gig_reviews_agg using gig_reviews_agg_pkey;

    create statistics if not exists etl_dev.gig_reviews_agg_all_ids
    on gig_id, last_review_id, last_fiverr_review_id, last_fiverr_created_at
    from etl_dev.gig_reviews_agg;

    analyze etl_dev.gig_reviews_agg;

commit;


-- gig_reviews_prices

do
$func$
    declare
        end_dttm timestamptz := (current_date + '1 day'::interval)::timestamptz;
        split_point text;
        split_interval text := '1 month';
        split_amount smallint := 2;
        monthly_window_period interval := '1 year'::interval;
        quarterly_window_period interval;
        archive_lag interval := '2 year'::interval;
        archive_point timestamptz;
        maintenance_rec record;
        maintenance_qry text;
        stats_columns_list text[];

    begin

        split_point := date_trunc('month', end_dttm)::text;
        archive_point := date_trunc('year', end_dttm - archive_lag);
        quarterly_window_period := age(date_trunc('month', end_dttm), archive_point) - monthly_window_period;

        create table if not exists etl_dev.gig_reviews_prices
            (like etl.gig_reviews_prices including all excluding indexes excluding statistics);
        partition by range (fiverr_created_at);

        create unique index if not exists uidx_gig_reviews_prices_id
            on etl_dev.gig_reviews_prices (id, fiverr_created_at desc);
        create index if not exists idx_gig_reviews_prices_gig_id
            on etl_dev.gig_reviews_prices (gig_id, fiverr_created_at desc);
        create index if not exists idx_gig_reviews_prices_fiverr_created_at
            on etl_dev.gig_reviews_prices (fiverr_created_at desc);
        create index if not exists idx_gig_reviews_prices_load_id
            on etl_dev.gig_reviews_prices ("load_id" desc, fiverr_created_at desc);

        drop table if exists pg_temp.gig_reviews_prices_repartition cascade;

        create temporary table if not exists pg_temp.gig_reviews_prices_repartition
            on commit drop
        as
            select *
            from public.maintenance_partitions_merge
                (
                    "root_table_oid" := 'etl_dev.gig_reviews_prices'::regclass,
                    "split_point" := split_point,
                    "split_interval" := split_interval,
                    "merge_windows" := array[monthly_window_period::text, quarterly_window_period::text],
                    "merge_intervals" := array['1 month', '3 month'],
                    "split_amount" := split_amount,
                    "archive_partition_flg" := true,
                    "archive_point" := archive_point::text,
                    "split_unbounded_flg" := false,
                    "default_partition_flg" := false,
                    "dry_run" := false,
                    "drop_detached_flg" := not false,
                    "inactive_index_fillfactor_100_flg" := true,
                    "cluster_new_partitions_flg" := true,
                    "copy_local_options_flg" := true,
                    "copy_foreign_options_flg" :=  true
                );

        insert into etl_dev.gig_reviews_prices
            select *
            from etl.gig_reviews_prices;

        stats_columns_list := array[
                'id', 'gig_id', 'price_range_start', 'price_range_end'
            ];

        <<maintenance_loop>>
        for maintenance_rec in
            select
                quote_ident(t.schema_name) as partition_schema,
                quote_ident(t.table_name) as partition_name,
                quote_ident(ci.relname) as cluster_index_name
            from public.get_partitions_tree('etl_dev.gigs_pages_pre_metrics'::regclass) t
            join pg_catalog.pg_attribute a
                on a.attrelid = t.table_oid
            join pg_catalog.pg_index ix
                on ix.indrelid = a.attrelid
                and ix.indkey[0] = a.attnum
            join pg_catalog.pg_class ci
                on ix.indexrelid = ci."oid"
            where
                t.relkind in ('r', 'f')
                and a.attname = 'scraped_at' -- clustering by scraped_at
                and cardinality(ix.indkey) = 1
        loop

            raise notice '% - Start of maintenance for partition %.%',
                clock_timestamp(),
                maintenance_rec.partition_schema,
                maintenance_rec.partition_name;

            -- CLUSTER + CREATE STATISTICS
            maintenance_qry := 'cluster '
                || maintenance_rec.partition_schema || '.' || maintenance_rec.partition_name
                || ' using ' || maintenance_rec.cluster_index_name || ';' || repeat(E'\n', 2)

                || 'create statistics if not exists ' || maintenance_rec.partition_schema || '.'
                || substring('stats_' || maintenance_rec.partition_name, 1, 63) || E'\n'
                || 'on ' || array_to_string(stats_columns_list, ', ') || E'\n'
                || 'from ' || maintenance_rec.partition_schema || '.' || maintenance_rec.partition_name || ';';

            execute maintenance_qry;

            raise notice '% - End of maintenance for partition %.%',
                clock_timestamp(),
                maintenance_rec.partition_schema,
                maintenance_rec.partition_name;

        end loop maintenance_loop;

        analyze etl_dev.gig_reviews_prices;

    end;
$func$;


-- gigs_pages_pre_metrics

do
$func$
    declare
        end_dttm timestamptz := (current_date + '1 day'::interval)::timestamptz;
        split_point text;
        split_interval text := '1 month';
        split_amount smallint := 2;
        monthly_window_period interval := '1 year'::interval;
        quarterly_window_period interval;
        archive_lag interval := '2 year'::interval;
        archive_point timestamptz;
        maintenance_rec record;
        maintenance_qry text;
        stats_columns_list text[];

    begin

        split_point := date_trunc('month', end_dttm)::text;
        archive_point := date_trunc('year', end_dttm - archive_lag);
        quarterly_window_period := age(date_trunc('month', end_dttm), archive_point) - monthly_window_period;

        create table if not exists etl.gigs_pages_pre_metrics
            (like etl.gigs_pages_pre_metrics including all excluding indexes excluding statistics);
        partition by range (scraped_at);

        create unique index if not exists uidx_gigs_pages_pre_metrics_id
            on etl_dev.gigs_pages_pre_metrics using btree (id, scraped_at desc);
        create index if not exists idx_gigs_pages_pre_metrics_gig_id
            on etl_dev.gigs_pages_pre_metrics using btree (gig_id, scraped_at desc);
        create index if not exists idx_gigs_pages_pre_metrics_fiverr_seller_id
            on etl_dev.gigs_pages_pre_metrics using btree (fiverr_seller_id, scraped_at desc);
        create index if not exists idx_gigs_pages_pre_metrics_scraped_at
            on etl_dev.gigs_pages_pre_metrics (scraped_at desc);
        create index if not exists idx_gigs_pages_pre_metrics_load_id
            on etl_dev.gigs_pages_pre_metrics using btree ("load_id" desc, scraped_at desc);

        drop table if exists pg_temp.gigs_pages_pre_metrics_repartition cascade;

        create temporary table if not exists pg_temp.gigs_pages_pre_metrics_repartition
            on commit drop
        as
            select *
            from public.maintenance_partitions_merge
                (
                    "root_table_oid" := 'etl_dev.gigs_pages_pre_metrics'::regclass,
                    "split_point" := split_point,
                    "split_interval" := split_interval,
                    "merge_windows" := array[monthly_window_period::text, quarterly_window_period::text],
                    "merge_intervals" := array['1 month', '3 month'],
                    "split_amount" := split_amount,
                    "archive_partition_flg" := true,
                    "archive_point" := archive_point::text,
                    "split_unbounded_flg" := false,
                    "default_partition_flg" := false,
                    "dry_run" := false,
                    "drop_detached_flg" := not false,
                    "inactive_index_fillfactor_100_flg" := true,
                    "cluster_new_partitions_flg" := true,
                    "copy_local_options_flg" := true,
                    "copy_foreign_options_flg" :=  true
                );

        insert into etl_dev.gigs_pages_pre_metrics
            select *
            from etl.gigs_pages_pre_metrics;

        stats_columns_list := array[
                'id', 'gig_id', 'fiverr_gig_id', 'fiverr_seller_id'
            ];

        <<maintenance_loop>>
        for maintenance_rec in
            select
                quote_ident(t.schema_name) as partition_schema,
                quote_ident(t.table_name) as partition_name,
                quote_ident(ci.relname) as cluster_index_name
            from public.get_partitions_tree('etl_dev.gigs_pages_pre_metrics'::regclass) t
            join pg_catalog.pg_attribute a
                on a.attrelid = t.table_oid
            join pg_catalog.pg_index ix
                on ix.indrelid = a.attrelid
                and ix.indkey[0] = a.attnum
            join pg_catalog.pg_class ci
                on ix.indexrelid = ci."oid"
            where
                t.relkind in ('r', 'f')
                and a.attname = 'scraped_at' -- clustering by scraped_at
                and cardinality(ix.indkey) = 1
        loop

            raise notice '% - Start of maintenance for partition %.%',
                clock_timestamp(),
                maintenance_rec.partition_schema,
                maintenance_rec.partition_name;

            -- CLUSTER + CREATE STATISTICS
            maintenance_qry := 'cluster '
                || maintenance_rec.partition_schema || '.' || maintenance_rec.partition_name
                || ' using ' || maintenance_rec.cluster_index_name || ';' || repeat(E'\n', 2)

                || 'create statistics if not exists ' || maintenance_rec.partition_schema || '.'
                || substring('stats_' || maintenance_rec.partition_name, 1, 63) || E'\n'
                || 'on ' || array_to_string(stats_columns_list, ', ') || E'\n'
                || 'from ' || maintenance_rec.partition_schema || '.' || maintenance_rec.partition_name || ';';

            execute maintenance_qry;

            raise notice '% - End of maintenance for partition %.%',
                clock_timestamp(),
                maintenance_rec.partition_schema,
                maintenance_rec.partition_name;

        end loop maintenance_loop;

        analyze etl_dev.gigs_pages_pre_metrics;

    end;
$func$;


-- gigs_pages_pre_metrics_invalid

begin;

    drop table if exists etl_dev.gigs_pages_pre_metrics_invalid cascade;

    create table if not exists etl_dev.gigs_pages_pre_metrics_invalid
        (like etl.gigs_pages_pre_metrics_invalid including all excluding indexes excluding statistics);

    insert into etl_dev.gigs_pages_pre_metrics_invalid
        select *
        from etl.gigs_pages_pre_metrics_invalid;

    create unique index if not exists uidx_gigs_pages_pre_metrics_invalid_id
        on etl_dev.gigs_pages_pre_metrics_invalid (id);
    create index if not exists idx_gigs_pages_pre_metrics_invalid
        on etl_dev.gigs_pages_pre_metrics_invalid (gig_id, scraped_at desc);

    cluster etl_dev.gigs_pages_pre_metrics_invalid using uidx_gigs_pages_pre_metrics_invalid_id;

    analyze etl_dev.gigs_pages_pre_metrics_invalid;

commit;


-- gigs_revisions_pre_metrics

do
$func$
    declare
        end_dttm timestamptz := (current_date + '1 day'::interval)::timestamptz;
        split_point text;
        split_interval text := '1 month';
        split_amount smallint := 2;
        monthly_window_period interval := '1 year'::interval;
        quarterly_window_period interval;
        archive_lag interval := '2 year'::interval;
        archive_point timestamptz;
        maintenance_rec record;
        maintenance_qry text;
        stats_columns_list text[];

    begin

        split_point := date_trunc('month', end_dttm)::text;
        archive_point := date_trunc('year', end_dttm - archive_lag);
        quarterly_window_period := age(date_trunc('month', end_dttm), archive_point) - monthly_window_period;

        create table if not exists etl_dev.gigs_revisions_pre_metrics
            (like etl.gigs_revisions_pre_metrics including all excluding indexes excluding statistics);
        partition by range (scraped_at);

        create unique index if not exists uidx_gigs_revisions_pre_metrics_id
            on etl_dev.gigs_revisions_pre_metrics (id, scraped_at desc);
        create index if not exists idx_gigs_revisions_pre_metrics_gig_id
            on etl_dev.gigs_revisions_pre_metrics (gig_id, scraped_at desc);
        create index if not exists idx_gigs_revisions_pre_metrics_fiverr_seller_id
            on etl_dev.gigs_revisions_pre_metrics (fiverr_seller_id, scraped_at desc);
        create index if not exists idx_gigs_revisions_pre_metrics_scraped_at
            on etl_dev.gigs_revisions_pre_metrics (scraped_at desc);
        create index if not exists idx_gigs_revisions_pre_metrics_load_id
            on etl_dev.gigs_revisions_pre_metrics ("load_id" desc, scraped_at desc);

        drop table if exists pg_temp.gigs_revisions_pre_metrics_repartition cascade;

        create temporary table if not exists pg_temp.gigs_revisions_pre_metrics_repartition
            on commit drop
        as
            select *
            from public.maintenance_partitions_merge
                (
                    "root_table_oid" := 'etl_dev.gigs_revisions_pre_metrics'::regclass,
                    "split_point" := split_point,
                    "split_interval" := split_interval,
                    "merge_windows" := array[monthly_window_period::text, quarterly_window_period::text],
                    "merge_intervals" := array['1 month', '3 month'],
                    "split_amount" := split_amount,
                    "archive_partition_flg" := true,
                    "archive_point" := archive_point::text,
                    "split_unbounded_flg" := false,
                    "default_partition_flg" := false,
                    "dry_run" := false,
                    "drop_detached_flg" := not false,
                    "inactive_index_fillfactor_100_flg" := true,
                    "cluster_new_partitions_flg" := true,
                    "copy_local_options_flg" := true,
                    "copy_foreign_options_flg" :=  true
                );

        insert into etl_dev.gigs_revisions_pre_metrics
            select *
            from etl.gigs_revisions_pre_metrics;

        stats_columns_list := array[
                'id', 'gig_id', 'fiverr_gig_id', 'fiverr_seller_id',
                'category_id', 'sub_category_id', 'nested_sub_category_id'
            ];

        <<maintenance_loop>>
        for maintenance_rec in
            select
                quote_ident(t.schema_name) as partition_schema,
                quote_ident(t.table_name) as partition_name,
                quote_ident(ci.relname) as cluster_index_name
            from public.get_partitions_tree('etl_dev.gigs_revisions_pre_metrics'::regclass) t
            join pg_catalog.pg_attribute a
                on a.attrelid = t.table_oid
            join pg_catalog.pg_index ix
                on ix.indrelid = a.attrelid
                and ix.indkey[0] = a.attnum
            join pg_catalog.pg_class ci
                on ix.indexrelid = ci."oid"
            where
                t.relkind in ('r', 'f')
                and a.attname = 'scraped_at' -- clustering by scraped_at
                and cardinality(ix.indkey) = 1
        loop

            raise notice '% - Start of maintenance for partition %.%',
                clock_timestamp(),
                maintenance_rec.partition_schema,
                maintenance_rec.partition_name;

            -- CLUSTER + CREATE STATISTICS
            maintenance_qry := 'cluster '
                || maintenance_rec.partition_schema || '.' || maintenance_rec.partition_name
                || ' using ' || maintenance_rec.cluster_index_name || ';' || repeat(E'\n', 2)

                || 'create statistics if not exists ' || maintenance_rec.partition_schema || '.'
                || substring('stats_' || maintenance_rec.partition_name, 1, 63) || E'\n'
                || 'on ' || array_to_string(stats_columns_list, ', ') || E'\n'
                || 'from ' || maintenance_rec.partition_schema || '.' || maintenance_rec.partition_name || ';';

            execute maintenance_qry;

            raise notice '% - End of maintenance for partition %.%',
                clock_timestamp(),
                maintenance_rec.partition_schema,
                maintenance_rec.partition_name;

        end loop maintenance_loop;

        analyze etl_dev.gigs_revisions_pre_metrics;

    end;
$func$;


-- gigs_revisions_pre_metrics_invalid

begin;

    drop table if exists etl_dev.gigs_revisions_pre_metrics_invalid cascade;

    create table if not exists etl_dev.gigs_revisions_pre_metrics_invalid
        (like etl.gigs_revisions_pre_metrics_invalid including all excluding indexes excluding statistics);

    insert into etl_dev.gigs_revisions_pre_metrics_invalid
        select *
        from etl.gigs_revisions_pre_metrics_invalid;

    create unique index if not exists uidx_gigs_revisions_pre_metrics_invalid_id
        on etl_dev.gigs_revisions_pre_metrics_invalid (id);
    create index if not exists idx_gigs_revisions_pre_metrics_invalid
        on etl_dev.gigs_revisions_pre_metrics_invalid (gig_id, scraped_at desc);

    cluster etl_dev.gigs_revisions_pre_metrics_invalid using uidx_gigs_revisions_pre_metrics_invalid_id;

    analyze etl_dev.gigs_revisions_pre_metrics_invalid;

commit;


-- sellers_revisions_pre_metrics

do
$func$
    declare
        end_dttm timestamptz := (current_date + '1 day'::interval)::timestamptz;
        split_point text;
        split_interval text := '1 month';
        split_amount smallint := 2;
        monthly_window_period interval := '1 year'::interval;
        quarterly_window_period interval;
        archive_lag interval := '2 year'::interval;
        archive_point timestamptz;
        maintenance_rec record;
        maintenance_qry text;
        stats_columns_list text[];

    begin

        split_point := date_trunc('month', end_dttm)::text;
        archive_point := date_trunc('year', end_dttm - archive_lag);
        quarterly_window_period := age(date_trunc('month', end_dttm), archive_point) - monthly_window_period;

        create table if not exists etl_dev.sellers_revisions_pre_metrics
            (like etl.sellers_revisions_pre_metrics including all excluding indexes excluding statistics);
        partition by range (scraped_at);

        create unique index if not exists uidx_sellers_revisions_pre_metrics_id
                on etl_dev.sellers_revisions_pre_metrics (id, scraped_at desc);
            create index if not exists idx_sellers_revisions_pre_metrics_seller_id
                on etl_dev.sellers_revisions_pre_metrics (seller_id, scraped_at desc);
            create index if not exists idx_sellers_revisions_pre_metrics_fiverr_seller_id
                on etl_dev.sellers_revisions_pre_metrics (fiverr_seller_id, scraped_at desc);
            create index if not exists idx_sellers_revisions_pre_metrics_scraped_at
                on etl_dev.sellers_revisions_pre_metrics (scraped_at desc);
            create index if not exists idx_sellers_revisions_pre_metrics_load_id
                on etl_dev.sellers_revisions_pre_metrics ("load_id" desc, scraped_at desc);

        drop table if exists pg_temp.sellers_revisions_pre_metrics_repartition cascade;

        create temporary table if not exists pg_temp.sellers_revisions_pre_metrics_repartition
            on commit drop
        as
            select *
            from public.maintenance_partitions_merge
                (
                    "root_table_oid" := 'etl_dev.sellers_revisions_pre_metrics'::regclass,
                    "split_point" := split_point,
                    "split_interval" := split_interval,
                    "merge_windows" := array[monthly_window_period::text, quarterly_window_period::text],
                    "merge_intervals" := array['1 month', '3 month'],
                    "split_amount" := split_amount,
                    "archive_partition_flg" := true,
                    "archive_point" := archive_point::text,
                    "split_unbounded_flg" := false,
                    "default_partition_flg" := false,
                    "dry_run" := false,
                    "drop_detached_flg" := not false,
                    "inactive_index_fillfactor_100_flg" := true,
                    "cluster_new_partitions_flg" := true,
                    "copy_local_options_flg" := true,
                    "copy_foreign_options_flg" :=  true
                );

        insert into etl_dev.sellers_revisions_pre_metrics
            select *
            from etl.sellers_revisions_pre_metrics;

        stats_columns_list := array[
                'id', 'seller_id', 'fiverr_seller_id'
            ];

        <<maintenance_loop>>
        for maintenance_rec in
            select
                quote_ident(t.schema_name) as partition_schema,
                quote_ident(t.table_name) as partition_name,
                quote_ident(ci.relname) as cluster_index_name
            from public.get_partitions_tree('etl_dev.sellers_revisions_pre_metrics'::regclass) t
            join pg_catalog.pg_attribute a
                on a.attrelid = t.table_oid
            join pg_catalog.pg_index ix
                on ix.indrelid = a.attrelid
                and ix.indkey[0] = a.attnum
            join pg_catalog.pg_class ci
                on ix.indexrelid = ci."oid"
            where
                t.relkind in ('r', 'f')
                and a.attname = 'scraped_at' -- clustering by scraped_at
                and cardinality(ix.indkey) = 1
        loop

            raise notice '% - Start of maintenance for partition %.%',
                clock_timestamp(),
                maintenance_rec.partition_schema,
                maintenance_rec.partition_name;

            -- CLUSTER + CREATE STATISTICS
            maintenance_qry := 'cluster '
                || maintenance_rec.partition_schema || '.' || maintenance_rec.partition_name
                || ' using ' || maintenance_rec.cluster_index_name || ';' || repeat(E'\n', 2)

                || 'create statistics if not exists ' || maintenance_rec.partition_schema || '.'
                || substring('stats_' || maintenance_rec.partition_name, 1, 63) || E'\n'
                || 'on ' || array_to_string(stats_columns_list, ', ') || E'\n'
                || 'from ' || maintenance_rec.partition_schema || '.' || maintenance_rec.partition_name || ';';

            execute maintenance_qry;

            raise notice '% - End of maintenance for partition %.%',
                clock_timestamp(),
                maintenance_rec.partition_schema,
                maintenance_rec.partition_name;

        end loop maintenance_loop;

        analyze etl_dev.sellers_revisions_pre_metrics;

    end;
$func$;


-- sellers_revisions_pre_metrics_invalid

begin;

    drop table if exists etl_dev.sellers_revisions_pre_metrics_invalid cascade;

    create table if not exists etl_dev.sellers_revisions_pre_metrics_invalid
        (like etl.sellers_revisions_pre_metrics_invalid including all excluding indexes excluding statistics);

    insert into etl_dev.sellers_revisions_pre_metrics_invalid
        select *
        from etl.sellers_revisions_pre_metrics_invalid;

    create unique index if not exists uidx_sellers_revisions_pre_metrics_invalid_id
        on etl_dev.sellers_revisions_pre_metrics_invalid (id);
    create index if not exists idx_sellers_revisions_pre_metrics_invalid
        on etl_dev.sellers_revisions_pre_metrics_invalid (seller_id, scraped_at desc);

    cluster etl_dev.sellers_revisions_pre_metrics_invalid using uidx_sellers_revisions_pre_metrics_invalid_id;

    analyze etl_dev.sellers_revisions_pre_metrics_invalid;

commit;