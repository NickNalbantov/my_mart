create schema if not exists public_dev;


-- DDL

create table public_dev.alembic_version
(like public.alembic_version including all);

create table public_dev.category_tree
(like public.category_tree including all);

create table public_dev.gig_reviews
(like public.gig_reviews including all);

create table public_dev.gig_reviews_meta
(like public.gig_reviews_meta including all);

create table public_dev.gigs
(like public.gigs including all);

create table public_dev.gigs_revisions
(like public.gigs_revisions including all);

create table public_dev.gigs_revisions_meta
(like public.gigs_revisions_meta including all);

create table public_dev.landing1_users
(like public.landing1_users including all);

create table public_dev.seller_sheets
(like public.seller_sheets including all);

create table public_dev.seller_sheets_revisions
(like public.seller_sheets_revisions including all);

create table public_dev.seller_sheets_revisions_meta
(like public.seller_sheets_revisions_meta including all);

create table public_dev.sellers
(like public.sellers including all);

create table public_dev.sellers_revisions
(like public.sellers_revisions including all);

create table public_dev.sellers_revisions_meta
(like public.sellers_revisions_meta including all);


-- inserts

    -- public_dev.alembic_version

insert into public_dev.alembic_version
overriding system value
    select *
    from public.alembic_version;

cluster public_dev.alembic_version using alembic_version_pkey;
analyze public_dev.alembic_version;

    -- public_dev.category_tree

insert into public_dev.category_tree
overriding system value
    select *
    from public.category_tree;

cluster public_dev.category_tree using uidx_category_tree;
analyze public_dev.category_tree;

select setval
    (
        pg_get_serial_sequence('public_dev.category_tree', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public_dev.category_tree;

    -- public_dev.landing1_users

insert into public_dev.landing1_users
overriding system value
    select *
    from public.landing1_users;

cluster public_dev.landing1_users using landing1_users_pkey;
analyze public_dev.landing1_users;

select setval
    (
        pg_get_serial_sequence('public_dev.landing1_users', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public_dev.landing1_users;

    -- public_dev.gigs

insert into public_dev.gigs
overriding system value
    select g.*
    from public.gigs g
    where exists
        (
            select
            from public.gigs_revisions_meta r
            where g.id = r.gig_id
        )
        and exists
        (
            select
            from public.sellers s
            where g.seller_id = s.id
        )
        and exists
        (
            select
            from public.gig_reviews_meta w
            where g.id = w.gig_id
        )
    order by g.created_at desc
    limit 5000;

cluster public_dev.gigs using gigs_pkey;
analyze public_dev.gigs;

select setval
    (
        pg_get_serial_sequence('public_dev.gigs', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public_dev.gigs;

    -- public_dev.gigs_revisions_meta

insert into public_dev.gigs_revisions_meta
overriding system value
    select r.*
    from public.gigs_revisions_meta r
    where exists
        (
            select
            from public_dev.gigs s
            where s.id = r.gig_id
        );

cluster public_dev.gigs_revisions_meta using gigs_revisions_meta_pkey;
analyze public_dev.gigs_revisions_meta;

select setval
    (
        pg_get_serial_sequence('public_dev.gigs_revisions_meta', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public_dev.gigs_revisions_meta;

    -- public_dev.gigs_revisions

insert into public_dev.gigs_revisions
overriding system value
    select r.*
    from public.gigs_revisions r
    where exists
        (
            select
            from public_dev.gigs_revisions_meta s
            where s.id = r.id
        );

cluster public_dev.gigs_revisions using gigs_revisions_pkey;
analyze public_dev.gigs_revisions;

    -- public_dev.gig_reviews_meta

insert into public_dev.gig_reviews_meta
overriding system value
    select r.*
    from public.gig_reviews_meta r
    where exists
        (
            select
            from public_dev.gigs s
            where s.id = r.gig_id
        );

cluster public_dev.gig_reviews_meta using gig_reviews_meta_pkey;
analyze public_dev.gig_reviews_meta;

select setval
    (
        pg_get_serial_sequence('public_dev.gig_reviews_meta', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public_dev.gig_reviews_meta;

    -- public_dev.gig_reviews

insert into public_dev.gig_reviews
overriding system value
    select r.*
    from public.gig_reviews r
    where exists
        (
            select
            from public_dev.gig_reviews_meta s
            where s.id = r.id
        );

cluster public_dev.gig_reviews using gig_reviews_pkey;
analyze public_dev.gig_reviews;

    -- public_dev.sellers

insert into public_dev.sellers
overriding system value
    select s.*
    from public.sellers s
    where
        exists
        (
            select
            from public.sellers_revisions_meta r
            where s.id = r.seller_id
        )
        and exists
        (
            select
            from public_dev.gigs g
            where s.id = g.seller_id
        );

cluster public_dev.sellers using sellers_pkey;
analyze public_dev.sellers;

select setval
    (
        pg_get_serial_sequence('public_dev.sellers', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public_dev.sellers;

    -- public_dev.sellers_revisions_meta

insert into public_dev.sellers_revisions_meta
overriding system value
    select r.*
    from public.sellers_revisions_meta r
    where exists
        (
            select
            from public_dev.sellers s
            where s.id = r.seller_id
        );

cluster public_dev.sellers_revisions_meta using sellers_revisions_meta_pkey;
analyze public_dev.sellers_revisions_meta;

select setval
    (
        pg_get_serial_sequence('public_dev.sellers_revisions_meta', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public_dev.sellers_revisions_meta;

    -- public_dev.seller_sheets

insert into public_dev.seller_sheets
overriding system value
    select s.*
    from public.seller_sheets s
    where exists
        (
            select
            from public.seller_sheets_revisions_meta r
            where s.id = r.seller_sheet_id
        );

cluster public_dev.seller_sheets using seller_sheets_pkey;
analyze public_dev.seller_sheets;

select setval
    (
        pg_get_serial_sequence('public_dev.seller_sheets', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public_dev.seller_sheets;

    -- public_dev.seller_sheets_revisions_meta

insert into public_dev.seller_sheets_revisions_meta
overriding system value
    select r.*
    from public.seller_sheets_revisions_meta r
    where exists
        (
            select
            from public_dev.seller_sheets s
            where s.id = r.seller_sheet_id
        );

cluster public_dev.seller_sheets_revisions_meta using seller_sheets_revisions_meta_pkey;
analyze public_dev.seller_sheets_revisions_meta;

select setval
    (
        pg_get_serial_sequence('public_dev.seller_sheets_revisions_meta', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public_dev.seller_sheets_revisions_meta;

    -- public_dev.seller_sheets_revisions

insert into public_dev.seller_sheets_revisions
overriding system value
    select r.*
    from public.seller_sheets_revisions r
    where exists
        (
            select
            from public_dev.seller_sheets_revisions_meta s
            where s.id = r.id
        );

cluster public_dev.seller_sheets_revisions using seller_sheets_revisions_pkey;
analyze public_dev.seller_sheets_revisions;