set session_replication_role = 'replica';

-- execute results to drop indices and constarints

--select string_agg('alter table if exists public.' || r.relname || ' drop constraint if exists ' || c.conname || ' cascade', E';\n' order by c.oid)
--from pg_constraint c
--join pg_class r on r.oid = c.conrelid
--where
--    r.relnamespace = 'public'::regnamespace
--    and substring(r.relname, '(s3_export|alembic_version|landing1_users)') is null
--    and c.contype in ('p', 'u');

--select string_agg('drop index if exists public.' || r.relname || ' cascade', E';\n' order by r.oid)
--from pg_class r
--where
--    r.relnamespace = 'public'::regnamespace
--    and relkind in ('i', 'I')
--    and substring(r.relname, '(s3_export|alembic_version|landing1_users)') is null



-- sellers

merge into public.sellers p
using public_dev.sellers pd
    on p.fiverr_seller_id = pd.fiverr_seller_id
when not matched then
    insert (scraped_at, heuristic_created_at, joined_at, fiverr_seller_id, username)
        values (pd.scraped_at, pd.heuristic_created_at, pd.joined_at, pd.fiverr_seller_id, pd.username)
when matched and p.username is distinct from pd.username then
    update set username = pd.username;

alter table if exists public.sellers add constraint sellers_pkey primary key (id);
alter table if exists public.sellers add constraint uidx_sellers_fiverr_seller_id unique (fiverr_seller_id);

create index if not exists idx_sellers_created_at on public.sellers using btree (created_at desc);
create index if not exists idx_sellers_scraped_at on public.sellers using btree (scraped_at desc);

cluster public.sellers using sellers_pkey;
analyze public.sellers;


-- gigs

with sel as
(
    select
        dm.scraped_at,
        dm.heuristic_created_at,
        dm.first_review_created_at,
        dm.fiverr_gig_id,
        s.id as seller_id
    from public_dev.gigs_revisions_meta dm
    join public_dev.gigs dg on dm.gig_id = dg.id
    join public.gigs g on g.fiverr_gig_id = dg.fiverr_gig_id
)

merge into public.gigs p
using sel pd
    on p.fiverr_gig_id = pd.fiverr_gig_id
when not matched then
    insert (scraped_at, heuristic_created_at, first_review_created_at, fiverr_gig_id, seller_id)
        values (pd.scraped_at, pd.heuristic_created_at, pd.first_review_created_at, pd.fiverr_gig_id, pd.seller_id)
when matched then
    do nothing;

alter table if exists public.gigs add constraint gigs_pkey primary key (id);
alter table if exists public.gigs add constraint uidx_gigs_fiverr_gig_id unique (fiverr_gig_id);

create index if not exists idx_gigs_created_at on public.gigs using btree (created_at desc);
create index if not exists idx_gigs_scraped_at on public.gigs using btree (scraped_at desc);
create index if not exists idx_gigs_seller_id on public.gigs using btree (seller_id);

cluster public.gigs using gigs_pkey;
analyze public.gigs;


-- gigs_revisions_meta

alter table public.gigs_revisions_meta add column if not exists dev_id int;

insert into public.gigs_revisions_meta
(created_at, scraped_at, gig_id, schema_version, fiverr_source, fiverr_sort_type, fiverr_status, "origin", s3_file_name, dev_id)
    select
        dm.created_at,
        dm.scraped_at,
        g.id as gig_id,
        dm.schema_version,
        dm.fiverr_source,
        dm.fiverr_sort_type,
        dm.fiverr_status,
        dm."origin",
        dm.s3_file_name,
        dm.id as dev_id
    from public_dev.gigs_revisions_meta dm
    join public_dev.gigs dg on dm.gig_id = dg.id
    join public.gigs g on g.fiverr_gig_id = dg.fiverr_gig_id;

alter table if exists public.gigs_revisions_meta add constraint gigs_revisions_meta_pkey primary key (id);
alter table if exists public.gigs_revisions_meta add constraint uidx_gigs_revisions_meta_gig_id_source_sort_origin_created_at unique (gig_id, fiverr_source, fiverr_sort_type, origin, created_at);

create index if not exists idx_gigs_revisions_meta_created_at on public.gigs_revisions_meta using btree (created_at desc);
create index if not exists idx_gigs_revisions_meta_gig_id_created_at on public.gigs_revisions_meta using btree (gig_id, created_at desc);
create index if not exists idx_gigs_revisions_meta_gig_id_scraped_at on public.gigs_revisions_meta using btree (gig_id, scraped_at desc);
create index if not exists idx_gigs_revisions_meta_scraped_at on public.gigs_revisions_meta using btree (scraped_at desc);
create index if not exists dev_id_idx_gig_rev on public.gigs_revisions_meta using btree (dev_id);

cluster public.gigs_revisions_meta using gigs_revisions_meta_pkey;
analyze public.gigs_revisions_meta;


-- gig_reviews_meta

alter table public.gig_reviews_meta add column if not exists dev_id int;

insert into public.gig_reviews_meta
(created_at, scraped_at, fiverr_created_at, gig_id, schema_version, fiverr_review_id, "origin", s3_file_name, dev_id)
    select
        dm.created_at,
        dm.scraped_at,
        dm.fiverr_created_at,
        g.id as gig_id,
        dm.schema_version,
        dm.fiverr_review_id,
        dm."origin",
        dm.s3_file_name,
        dm.id as dev_id
    from public_dev.gig_reviews_meta dm
    join public_dev.gigs dg on dm.gig_id = dg.id
    join public.gigs g on g.fiverr_gig_id = dg.fiverr_gig_id
    where not exists
    (
        select
        from public.gig_reviews_meta m
        where m.fiverr_review_id = dm.fiverr_review_id
    );


alter table if exists public.gig_reviews_meta add constraint gig_reviews_meta_pkey primary key (id);
alter table if exists public.gig_reviews_meta add constraint uidx_gig_reviews_meta_fiverr_review_id unique (fiverr_review_id);

create index if not exists idx_gig_reviews_meta_created_at on public.gig_reviews_meta using btree (created_at desc);
create index if not exists idx_gig_reviews_meta_gig_id_created_at on public.gig_reviews_meta using btree (gig_id, created_at desc);
create index if not exists idx_gig_reviews_meta_gig_id_scraped_at on public.gig_reviews_meta using btree (gig_id, scraped_at desc);
create index if not exists idx_gig_reviews_meta_scraped_at on public.gig_reviews_meta using btree (scraped_at desc);
create index if not exists idx_gig_reviews_meta_gig_id_fiverr_created_at on public.gig_reviews_meta using btree (gig_id, fiverr_created_at desc);
create index if not exists idx_gig_reviews_meta_fiverr_created_at on public.gig_reviews_meta using btree (fiverr_created_at desc);
create index if not exists dev_id_idx_review on public.gig_reviews_meta using btree (dev_id);

cluster public.gig_reviews_meta using gig_reviews_meta_pkey;
analyze public.gig_reviews_meta;


-- sellers_revisions_meta

alter table public.sellers_revisions_meta add column if not exists dev_id int;

insert into public.sellers_revisions_meta
(created_at, scraped_at, seller_id, schema_version, "origin", s3_file_name, dev_id)
    select
        dm.created_at,
        dm.scraped_at,
        s.id as seller_id,
        dm.schema_version,
        dm."origin",
        dm.s3_file_name,
        dm.id as dev_id
    from public_dev.sellers_revisions_meta dm
    join public_dev.sellers ds on dm.seller_id = ds.id
    join public.sellers s on s.fiverr_seller_id = ds.fiverr_seller_id;

alter table if exists public.sellers_revisions_meta add constraint sellers_revisions_meta_pkey primary key (id);
alter table if exists public.sellers_revisions_meta add constraint uidx_sellers_revisions_meta_seller_id_created_at_origin unique (seller_id, created_at, origin);

create index if not exists idx_sellers_revisions_meta_created_at on public.sellers_revisions_meta using btree (created_at desc);
create index if not exists idx_sellers_revisions_meta_scraped_at on public.sellers_revisions_meta using btree (scraped_at desc);
create index if not exists idx_sellers_revisions_meta_seller_id_scraped_at on public.sellers_revisions_meta using btree (seller_id, scraped_at desc);
create index if not exists dev_id_idx_sel_rev on public.gig_reviews_meta using btree (dev_id);

cluster public.sellers_revisions_meta using sellers_revisions_meta_pkey;
analyze public.sellers_revisions_meta;


-- gig_pages_meta

alter table if exists public_dev.gig_pages_meta set schema public;
alter table if exists public_dev.gig_pages set schema public;

with ids as
(
    select
        dm.gig_id as dev_id,
        g.id as gig_id
    from public.gigs_pages_meta dm
    join public_dev.gigs dg on dm.gig_id = dg.id
    join public.gigs g on g.fiverr_gig_id = dg.fiverr_gig_id
)

update public.gigs_pages_meta m
set gig_id = i.gig_id
from ids i
where m.gig_id = i.dev_id;

alter table if exists public.gigs_pages_meta add constraint gigs_pages_meta_pkey primary key (id);
alter table if exists public.gigs_pages_meta add constraint uidx_gigs_pages_meta_gig_id_source_sort_origin_created_at unique (gig_id, origin, created_at);

create index if not exists idx_gigs_pages_meta_created_at on public.gigs_pages_meta using btree (created_at desc);
create index if not exists idx_gigs_pages_meta_gig_id_created_at on public.gigs_pages_meta using btree (gig_id, created_at desc);
create index if not exists idx_gigs_pages_meta_gig_id_scraped_at on public.gigs_pages_meta using btree (gig_id, scraped_at desc);
create index if not exists idx_gigs_pages_meta_scraped_at on public.gigs_pages_meta using btree (scraped_at desc);

cluster public.gigs_pages_meta using gigs_pages_meta_pkey;
analyze public.gigs_pages_meta;


-- gigs_pages

alter table if exists public.gigs_pages add constraint gigs_pages_pkey primary key (id);

cluster public.gigs_pages using gigs_pages_pkey;
analyze public.gigs_pages;


-- sellers_revisions

insert into public.sellers_revisions
(id, raw)
    select
        p.id,
        pd.raw
    from public.sellers_revisions_meta p
    join public_dev.sellers_revisions pd on p.dev_id = pd.id;

alter table if exists public.sellers_revisions add constraint sellers_revisions_pkey primary key (id);

cluster public.sellers_revisions using sellers_revisions_pkey;
analyze public.sellers_revisions;


-- gigs_revisions

insert into public.gigs_revisions
(id, raw)
    select
        p.id,
        pd.raw
    from public.gigs_revisions_meta p
    join public_dev.gigs_revisions pd on p.dev_id = pd.id;

alter table if exists public.gigs_revisions add constraint gigs_revisions_pkey primary key (id);

cluster public.gigs_revisions using gigs_revisions_pkey;
analyze public.gigs_revisions;


-- gig_reviews

insert into public.gig_reviews
(id, raw)
    select
        p.id,
        pd.raw
    from public.gig_reviews_meta p
    join public_dev.gig_reviews pd on p.dev_id = pd.id;

alter table if exists public.gig_reviews add constraint gig_reviews_pkey primary key (id);

cluster public.gig_reviews using gig_reviews_pkey;
analyze public.gig_reviews;


-- dropping dev_ids

alter table if exists public.gig_reviews_meta drop column if exists dev_id cascade;
alter table if exists public.gigs_revisions_meta drop column if exists dev_id cascade;
alter table if exists public.sellers_revisions_meta drop column if exists dev_id cascade;

cluster public.gig_reviews_meta using gig_reviews_meta_pkey;
cluster public.gigs_revisions_meta using gigs_revisions_meta_pkey;
cluster public.sellers_revisions_meta using sellers_revisions_meta_pkey;

analyze public.gig_reviews_meta;
analyze public.gigs_revisions_meta;
analyze public.sellers_revisions_meta;


SET session_replication_role = 'origin';