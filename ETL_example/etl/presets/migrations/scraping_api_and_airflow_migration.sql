create database api;
create database airflow;

-- CREATE ROLE commands + DCL here

-- api DDL

CREATE TABLE public.alembic_version (
    version_num varchar(32) NOT NULL,
    CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num)
);

CREATE TABLE public.db_gig_pages (
    url text NOT NULL,
    id bigserial NOT NULL,
    status varchar NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL,
    foreign_id int4 NOT NULL,
    CONSTRAINT "_db_gig_pages_foreign_id_uc" UNIQUE (foreign_id),
    CONSTRAINT db_gig_pages_pkey PRIMARY KEY (id)
);
CREATE INDEX part_idx_id_taken_db_gig_pages ON public.db_gig_pages USING btree (id) INCLUDE (created_at) WHERE ((status)::text = ('taken'::character varying)::text);

CREATE TABLE public.db_gigs (
    id int8 GENERATED ALWAYS AS IDENTITY( INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1 NO CYCLE) NOT NULL,
    gig_id int8 NOT NULL,
    created_at timestamptz NOT NULL,
    updated_at timestamptz NOT NULL,
    foreign_id int4 NOT NULL,
    status varchar NOT NULL,
    CONSTRAINT "_db_gigs_foreign_id_uc" UNIQUE (foreign_id),
    CONSTRAINT db_gigs_pkey PRIMARY KEY (id)
);
CREATE INDEX idx_gig_id_status_db_gigs ON public.db_gigs USING btree (gig_id, status);
CREATE INDEX part_idx_id_taken_db_gigs ON public.db_gigs USING btree (id) INCLUDE (created_at) WHERE ((status)::text = ANY (ARRAY[('taken'::character varying)::text, ('not_taken'::character varying)::text]));

CREATE TABLE public.db_sellers (
    id int8 GENERATED ALWAYS AS IDENTITY( INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1 NO CYCLE) NOT NULL,
    fiverr_gig_id int8 NULL,
    created_at timestamptz NOT NULL,
    updated_at timestamptz NOT NULL,
    foreign_id int4 NOT NULL,
    status varchar NOT NULL,
    seller_name varchar NOT NULL,
    fiverr_gig_ids _int4 NULL,
    CONSTRAINT "_sellers_foreign_id_uc" UNIQUE (foreign_id),
    CONSTRAINT db_sellers_pkey PRIMARY KEY (id)
);
CREATE INDEX part_idx_id_taken_db_sellers ON public.db_sellers USING btree (id) INCLUDE (created_at) WHERE ((status)::text = ('taken'::character varying)::text);

CREATE TABLE public.db_sellers_without_created_at (
    id int8 GENERATED ALWAYS AS IDENTITY( INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1 NO CYCLE) NOT NULL,
    fiverr_gig_id int8 NULL,
    created_at timestamptz NOT NULL,
    updated_at timestamptz NOT NULL,
    foreign_id int4 NOT NULL,
    status varchar NOT NULL,
    seller_name varchar NOT NULL,
    CONSTRAINT "_sellers_without_created_at_foreign_id_uc" UNIQUE (foreign_id),
    CONSTRAINT db_sellers_without_created_at_pkey PRIMARY KEY (id)
);

CREATE TABLE public.new_catalog_gigs (
    id int8 GENERATED ALWAYS AS IDENTITY( INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1 NO CYCLE) NOT NULL,
    created_at timestamptz NOT NULL,
    updated_at timestamptz NOT NULL,
    foreign_id int4 NOT NULL,
    sorting varchar NOT NULL,
    status varchar NOT NULL,
    url text NOT NULL,
    CONSTRAINT "_new_gigs_foreign_id_url_uc" UNIQUE (foreign_id, url),
    CONSTRAINT new_catalog_gigs_pkey PRIMARY KEY (id)
);

CREATE TABLE public.new_gig_reviews (
    id int8 GENERATED ALWAYS AS IDENTITY( INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1 NO CYCLE) NOT NULL,
    created_at timestamptz NOT NULL,
    updated_at timestamptz NOT NULL,
    foreign_id int4 NOT NULL,
    status varchar NOT NULL,
    url text NOT NULL,
    CONSTRAINT "_gig_reviews_foreign_id_uc" UNIQUE (foreign_id),
    CONSTRAINT new_gig_reviews_pkey PRIMARY KEY (id)
);

-- api connect

create extension if not exists postgres_fdw with schema public cascade;

create server api
foreign data wrapper postgres_fdw
options (host 'scraping-api.cr82xjq3sz3n.eu-central-1.rds.amazonaws.com', port '5432', dbname 'api');

create user mapping for sayjam
server api
options (user 'sayjam', password 'insert_password_here');

create schema public_remote;

import foreign schema public
from server api
into public_remote
options (
    import_collate 'true',
    import_default 'true',
    import_generated 'true',
    import_not_null 'true'
);

-- moving data

insert into public.alembic_version
overriding system value
    select *
     from public_remote.alembic_version;

cluster public.alembic_version using alembic_version_pkc;
analyze public.alembic_version;

insert into public.db_gig_pages
overriding system value
    select *
     from public_remote.db_gig_pages;

cluster public.db_gig_pages using db_gig_pages_pkey;
analyze public.db_gig_pages;

select setval
    (
        pg_get_serial_sequence('public.db_gig_pages', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public.db_gig_pages;

insert into public.db_gigs
overriding system value
    select *
     from public_remote.db_gigs;

cluster public.db_gigs using db_gigs_pkey;
analyze public.db_gigs;

select setval
    (
        pg_get_serial_sequence('public.db_gigs', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public.db_gigs;

insert into public.db_sellers
overriding system value
    select *
     from public_remote.db_sellers;

cluster public.db_sellers using db_sellers_pkey;
analyze public.db_sellers;

select setval
    (
        pg_get_serial_sequence('public.db_sellers', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public.db_sellers;

insert into public.db_sellers_without_created_at
overriding system value
    select *
     from public_remote.db_sellers_without_created_at;

cluster public.db_sellers_without_created_at using db_sellers_without_created_at_pkey;
analyze public.db_sellers_without_created_at;

select setval
    (
        pg_get_serial_sequence('public.db_sellers_without_created_at', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public.db_sellers_without_created_at;

insert into public.new_catalog_gigs
overriding system value
    select *
     from public_remote.new_catalog_gigs;

cluster public.new_catalog_gigs using new_catalog_gigs_pkey;
analyze public.new_catalog_gigs;

select setval
    (
        pg_get_serial_sequence('public.new_catalog_gigs', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public.new_catalog_gigs;

insert into public.new_gig_reviews
overriding system value
    select *
     from public_remote.new_gig_reviews;

cluster public.new_gig_reviews using new_gig_reviews_pkey;
analyze public.new_gig_reviews;

select setval
    (
        pg_get_serial_sequence('public.new_gig_reviews', 'id')::regclass,
        coalesce(max(id), 1)::bigint,
        case when max(id) is not null then true else false end
    )
from public.new_gig_reviews;