-- Create a function that always returns the first non-NULL value:
create or replace function public.first_agg (anyelement, anyelement)
    returns anyelement
    language sql
    immutable
    strict
    parallel safe
    as
        'select $1'
;

create or replace aggregate public.first (anyelement)
(
    sfunc = first_agg,
    stype = anyelement,
    parallel = safe
);
comment on aggregate public.first(anyelement) is 'First non-null value';


-- Create a function that always returns the last non-NULL value:
create or replace function public.last_agg (anyelement, anyelement)
    returns anyelement
    language sql
    immutable
    strict
    parallel safe
    as
        'select $2'
;

create or replace aggregate public.last (anyelement)
(
    sfunc = last_agg,
    stype = anyelement,
    parallel = safe
);
comment on aggregate public.last(anyelement) is 'Last non-null value';