-- public.gigs

drop statistics if exists public.gigs_raw_all_ids;

create statistics if not exists public.gigs_raw_all_ids
on id, seller_id, fiverr_gig_id
from public.gigs;

analyze public.gigs;

-- public.sellers

drop statistics if exists public.sellers_raw_all_ids;

create statistics if not exists public.sellers_raw_all_ids
on id, fiverr_seller_id
from public.sellers;

analyze public.sellers;

-- public.gigs_revisions_meta;

drop statistics if exists public.gigs_revisions_meta_status;

create statistics if not exists public.gigs_revisions_meta_status
on gig_id, fiverr_status, scraped_at
from public.gigs_revisions_meta;

analyze public.gigs_revisions_meta;