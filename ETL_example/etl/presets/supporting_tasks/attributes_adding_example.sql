-- Regular ETL

begin;

    drop table if exists etl.sellers cascade;

    create table if not exists etl.sellers
    (
        "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
        processed_dttm timestamptz not null default transaction_timestamp(),
        created_at timestamptz not null,
        fiverr_created_at timestamptz null,
        scraped_at timestamptz not null,
        min_total_historical_revenue bigint not null,
        avg_total_historical_revenue bigint not null,
        max_total_historical_revenue bigint not null,
        seller_id int not null,
        fiverr_seller_id int not null,
        best_selling_gig_id int null,
        best_selling_fiverr_gig_id int null,
        global_rank int not null,
        gig_count_by_seller int not null,
        ratings_count int not null,
        completed_orders_count int not null,
        active_gigs_ratings_count_by_seller int not null,
        sales_volume int not null,
        total_historical_sales_volume int not null,
        best_selling_gig_total_historical_volume int null,
        weighted_avg_gig_price int not null,
        min_active_revenue int not null,
        min_inactive_revenue int not null,
        min_revenue int not null,
        avg_active_revenue int not null,
        avg_inactive_revenue int not null,
        avg_revenue int not null,
        max_active_revenue int not null,
        max_inactive_revenue int not null,
        max_revenue int not null,
        best_selling_gig_revenue int not null,
        is_pro bool not null,
        is_active bool not null,
        is_trend_valid bool not null,
        sales_volume_growth_percent numeric not null,
        rating numeric not null,
        revenue_growth_percent numeric not null,
        revenue_trend numeric not null,
        best_selling_gig_revenue_share numeric not null,
        best_selling_gig_title text null,
        best_selling_gig_cached_slug text null,
        "level" text not null,
        seller_name text not null,
        agency_slug text null,
        agency_status text null,
        country text null,
        country_code text null,
        languages text[] null,
        profile_image text null,
        best_selling_gig_image_preview text null
    );

    drop table if exists etl_wrk.sellers_new_attributes cascade;

    create unlogged table if not exists etl_wrk.sellers_new_attributes
        with
        (
            autovacuum_enabled = false,
            toast.autovacuum_enabled = false
        )
        as

        select distinct
            s.seller_id,
            first_value(s.total_historical_volume_by_gig) over w_s as best_selling_gig_total_historical_volume
        from etl.metrics s
        window
            w_s as
                (
                    partition by s.seller_id
                    order by
                        s.gig_is_active desc,
                        s.min_total_historical_revenue_by_gig desc,
                        s.gig_scraped_at desc,
                        s.gig_id desc
                );

    create index if not exists idx_seller_id on etl_wrk.sellers_new_attributes (seller_id) with (fillfactor = 100);

    cluster etl_wrk.sellers_new_attributes using idx_seller_id;

    analyze etl_wrk.sellers_new_attributes;

    insert into etl.sellers
    (
        "load_id",
        processed_dttm,
        created_at,
        fiverr_created_at,
        scraped_at,
        min_total_historical_revenue,
        avg_total_historical_revenue,
        max_total_historical_revenue,
        seller_id,
        fiverr_seller_id,
        best_selling_gig_id,
        best_selling_fiverr_gig_id,
        global_rank,
        gig_count_by_seller,
        ratings_count,
        completed_orders_count,
        active_gigs_ratings_count_by_seller,
        sales_volume,
        total_historical_sales_volume,
        best_selling_gig_total_historical_volume,
        weighted_avg_gig_price,
        min_active_revenue,
        min_inactive_revenue,
        min_revenue,
        avg_active_revenue,
        avg_inactive_revenue,
        avg_revenue,
        max_active_revenue,
        max_inactive_revenue,
        max_revenue,
        best_selling_gig_revenue,
        is_pro,
        is_active,
        is_trend_valid,
        sales_volume_growth_percent,
        rating,
        revenue_growth_percent,
        revenue_trend,
        best_selling_gig_revenue_share,
        best_selling_gig_title,
        best_selling_gig_cached_slug,
        "level",
        seller_name,
        agency_slug,
        agency_status,
        country,
        country_code,
        languages,
        profile_image,
        best_selling_gig_image_preview
    )

        select
            b."load_id",
            b.processed_dttm,
            b.created_at,
            b.fiverr_created_at,
            b.scraped_at,
            b.min_total_historical_revenue,
            b.avg_total_historical_revenue,
            b.max_total_historical_revenue,
            b.seller_id,
            b.fiverr_seller_id,
            b.best_selling_gig_id,
            b.best_selling_fiverr_gig_id,
            b.global_rank,
            b.gig_count_by_seller,
            b.ratings_count,
            b.completed_orders_count,
            b.active_gigs_ratings_count_by_seller,
            b.sales_volume,
            b.total_historical_sales_volume,
            n.best_selling_gig_total_historical_volume,
            b.weighted_avg_gig_price,
            b.min_active_revenue,
            b.min_inactive_revenue,
            b.min_revenue,
            b.avg_active_revenue,
            b.avg_inactive_revenue,
            b.avg_revenue,
            b.max_active_revenue,
            b.max_inactive_revenue,
            b.max_revenue,
            b.best_selling_gig_revenue,
            b.is_pro,
            b.is_active,
            b.is_trend_valid,
            b.sales_volume_growth_percent,
            b.rating,
            b.revenue_growth_percent,
            b.revenue_trend,
            b.best_selling_gig_revenue_share,
            b.best_selling_gig_title,
            b.best_selling_gig_cached_slug,
            b."level",
            b.seller_name,
            b.agency_slug,
            b.agency_status,
            b.country,
            b.country_code,
            b.languages,
            b.profile_image,
            b.best_selling_gig_image_preview
        from etl_wrk.sellers_backup_20241224030002 b
        join etl_wrk.sellers_new_attributes n on b.seller_id = n.seller_id;

        alter table if exists etl.sellers
            add primary key (seller_id) include (is_active) with (fillfactor = 100);
        create index if not exists idx_sellers_best_selling_gig_id
            on etl.sellers (best_selling_gig_id) with (fillfactor = 100);
        create unique index if not exists uidx_sellers_ranks
            on etl.sellers (global_rank desc) with (fillfactor = 100);
        create index if not exists idx_trg_seller_name_sellers
            on etl.sellers using gin (seller_name gin_trgm_ops);

        cluster etl.sellers using sellers_pkey;

        alter table if exists etl.sellers
            replica identity using index sellers_pkey;

        analyze etl.sellers;

        drop table if exists etl_wrk.sellers_new_attributes cascade;

commit;


-- Aggregations

drop table if exists etl_wrk.yearly_sellers_old cascade;

create unlogged table if not exists etl_wrk.yearly_sellers_old as
    select *
    from etl_agg.yearly_sellers;

alter table if exists etl_wrk.yearly_sellers_old
    add primary key (agg_period_start, seller_id) with (fillfactor = 100);

cluster etl_wrk.yearly_sellers_old using yearly_sellers_old_pkey;

analyze etl_wrk.yearly_sellers_old;

drop procedure if exists etl_agg.attributes_reload() cascade;

create or replace procedure etl_agg.attributes_reload()
language plpgsql
as
$proc$

    declare
        rec record;
        part_ddl text;

    begin

    raise notice 'Job start - %', clock_timestamp();

    drop table if exists etl_agg.yearly_sellers cascade;

    create table if not exists etl_agg.yearly_sellers
    (
        "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
        processed_dttm timestamptz not null default transaction_timestamp(),
        created_at timestamptz not null,
        fiverr_created_at timestamptz null,
        scraped_at timestamptz not null,
        min_active_revenue bigint not null,
        min_inactive_revenue bigint not null,
        min_revenue bigint not null,
        avg_active_revenue bigint not null,
        avg_inactive_revenue bigint not null,
        avg_revenue bigint not null,
        max_active_revenue bigint not null,
        max_inactive_revenue bigint not null,
        max_revenue bigint not null,
        min_total_historical_revenue bigint not null,
        avg_total_historical_revenue bigint not null,
        max_total_historical_revenue bigint not null,
        best_selling_gig_revenue bigint not null,
        agg_period_start date not null,
        seller_id int not null,
        fiverr_seller_id int not null,
        best_selling_gig_id int null,
        best_selling_fiverr_gig_id int null,
        global_rank int not null,
        gig_count_by_seller int not null,
        ratings_count int not null,
        completed_orders_count int not null,
        active_gigs_ratings_count_by_seller int not null,
        sales_volume int not null,
        total_historical_sales_volume int not null,
        best_selling_gig_total_historical_volume int null,
        weighted_avg_gig_price int not null,
        is_pro bool not null,
        is_active bool not null,
        is_trend_valid bool not null,
        sales_volume_growth_percent numeric not null,
        rating numeric not null,
        revenue_growth_percent numeric not null,
        revenue_trend numeric not null,
        best_selling_gig_revenue_share numeric not null,
        best_selling_gig_title text null,
        best_selling_gig_cached_slug text null,
        "level" text not null,
        seller_name text not null,
        agency_slug text null,
        agency_status text null,
        country text null,
        country_code text null,
        languages text[] null,
        profile_image text null,
        best_selling_gig_image_preview text null
    )
    partition by range (agg_period_start);

    select
        string_agg(partition_ddl, repeat(E'\n', 2))
    into part_ddl
    from public.get_range_partitions_ddl
        (
            root_table_oid := ('etl_agg.yearly_sellers')::regclass,
            split_point := '2025-01-01', -- '2024-10-01' / '2024-12-01' etc.
            split_interval := '1 year', -- '3 months' / '1 month'  etc.
            merge_windows := array['2 year']::text[], -- '15 months' / '15 months'  etc.
            merge_intervals := array['1 year']::text[], -- '3 months' / '1 month'  etc.
            split_amount := 2::smallint,
            archive_partition_flg := false,
            split_unbounded_flg := false,
            default_partition_flg := false
        );

    execute part_ddl;

    raise notice 'Preparations completed - %', clock_timestamp();

    for
        rec in
            select
                dt::date as dt
            from generate_series('2023-01-01'::date, '2025-01-01'::date, '1 year'::interval) g (dt)
            -- from generate_series('2023-10-01'::date, '2025-01-01'::date, '3 month'::interval) g (dt)
            -- from generate_series('2023-10-01'::date, '2025-01-01'::date, '1 month'::interval) g (dt)
    loop

        raise notice 'Iteration start for agg_period_start = % - %', rec.dt, clock_timestamp();

        drop table if exists etl_wrk.sellers_new_attributes cascade;

        create unlogged table if not exists etl_wrk.sellers_new_attributes
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                s.seller_id,
                rec.dt as agg_period_start,
                first_value(s.total_historical_volume_by_gig) over w_s as best_selling_gig_total_historical_volume
            from etl_agg.yearly_metrics s
            where
                s.agg_period_start = rec.dt
            window
                w_s as
                    (
                        partition by s.seller_id
                        order by
                            s.gig_is_active desc,
                            s.min_total_historical_revenue_by_gig desc,
                            s.gig_scraped_at desc,
                            s.gig_id desc
                    );

        create index if not exists idx_seller_id on etl_wrk.sellers_new_attributes (agg_period_start, seller_id) with (fillfactor = 100);

        cluster etl_wrk.sellers_new_attributes using idx_seller_id;

        analyze etl_wrk.sellers_new_attributes;

        insert into etl_agg.yearly_sellers
        (
            "load_id",
            processed_dttm,
            created_at,
            fiverr_created_at,
            scraped_at,
            min_active_revenue,
            min_inactive_revenue,
            min_revenue,
            avg_active_revenue,
            avg_inactive_revenue,
            avg_revenue,
            max_active_revenue,
            max_inactive_revenue,
            max_revenue,
            min_total_historical_revenue,
            avg_total_historical_revenue,
            max_total_historical_revenue,
            best_selling_gig_revenue,
            agg_period_start,
            seller_id,
            fiverr_seller_id,
            best_selling_gig_id,
            best_selling_fiverr_gig_id,
            global_rank,
            gig_count_by_seller,
            ratings_count,
            completed_orders_count,
            active_gigs_ratings_count_by_seller,
            sales_volume,
            total_historical_sales_volume,
            best_selling_gig_total_historical_volume,
            weighted_avg_gig_price,
            is_pro,
            is_active,
            is_trend_valid,
            sales_volume_growth_percent,
            rating,
            revenue_growth_percent,
            revenue_trend,
            best_selling_gig_revenue_share,
            best_selling_gig_title,
            best_selling_gig_cached_slug,
            "level",
            seller_name,
            agency_slug,
            agency_status,
            country,
            country_code,
            languages,
            profile_image,
            best_selling_gig_image_preview
        )

            select
                b."load_id",
                b.processed_dttm,
                b.created_at,
                b.fiverr_created_at,
                b.scraped_at,
                b.min_active_revenue,
                b.min_inactive_revenue,
                b.min_revenue,
                b.avg_active_revenue,
                b.avg_inactive_revenue,
                b.avg_revenue,
                b.max_active_revenue,
                b.max_inactive_revenue,
                b.max_revenue,
                b.min_total_historical_revenue,
                b.avg_total_historical_revenue,
                b.max_total_historical_revenue,
                b.best_selling_gig_revenue,
                b.agg_period_start,
                b.seller_id,
                b.fiverr_seller_id,
                b.best_selling_gig_id,
                b.best_selling_fiverr_gig_id,
                b.global_rank,
                b.gig_count_by_seller,
                b.ratings_count,
                b.completed_orders_count,
                b.active_gigs_ratings_count_by_seller,
                b.sales_volume,
                b.total_historical_sales_volume,
                n.best_selling_gig_total_historical_volume,
                b.weighted_avg_gig_price,
                b.is_pro,
                b.is_active,
                b.is_trend_valid,
                b.sales_volume_growth_percent,
                b.rating,
                b.revenue_growth_percent,
                b.revenue_trend,
                b.best_selling_gig_revenue_share,
                b.best_selling_gig_title,
                b.best_selling_gig_cached_slug,
                b."level",
                b.seller_name,
                b.agency_slug,
                b.agency_status,
                b.country,
                b.country_code,
                b.languages,
                b.profile_image,
                b.best_selling_gig_image_preview
            from etl_wrk.yearly_sellers_old b
            join etl_wrk.sellers_new_attributes n
                on b.agg_period_start = n.agg_period_start
                and b.seller_id = n.seller_id;

            raise notice 'Iteration end for agg_period_start = % - %', rec.dt, clock_timestamp();

            commit;

        end loop;

        raise notice 'Target indexing started - %', clock_timestamp();

        drop table if exists etl_wrk.sellers_new_attributes cascade;

        alter table if exists etl_agg.yearly_sellers
            add primary key (agg_period_start, seller_id) include (is_active) with (fillfactor = 100);
        create index if not exists idx_yearly_sellers_best_selling_gig_id
            on etl_agg.yearly_sellers (agg_period_start, best_selling_gig_id) with (fillfactor = 100);
        create unique index if not exists uidx_yearly_sellers_ranks
            on etl_agg.yearly_sellers (agg_period_start, global_rank desc) with (fillfactor = 100);
        create index if not exists idx_trg_seller_name_yearly_sellers
            on etl_agg.yearly_sellers using gin (seller_name gin_trgm_ops);

        alter table if exists etl_agg.yearly_sellers
            replica identity using index yearly_sellers_pkey;

        raise notice 'Target indexing completed - %', clock_timestamp();

    end;

$proc$;

call etl_agg.attributes_reload();

drop procedure if exists etl_agg.attributes_reload() cascade;

    -- CLUSTER on partitioned table can't be executed in a function
cluster etl_agg.yearly_sellers using yearly_sellers_pkey;

analyze etl_agg.yearly_sellers;

    -- Checks

select agg_period_start, count(*) as cnt
from etl_wrk.monthly_sellers_old
group by agg_period_start;

select agg_period_start, count(best_selling_gig_total_historical_volume) as cnt
from etl_agg.monthly_sellers
group by agg_period_start;


drop table if exists etl_wrk.yearly_sellers_old cascade;