-- t3.xlarge - 2 vCPU, 16 GB RAM, SSD

alter system set effective_cache_size = '12GB'; -- in AWS unit is 8kb, not 1 kb. so numeric value should be divided by 8 (1572864)
alter system set maintenance_work_mem = '1GB'; -- '1048576kb'
alter system set autovacuum_work_mem = '1GB'; -- '1048576kb'
alter system set default_statistics_target = '256';
alter system set random_page_cost = '1.0';

alter system set max_connections = '250'; -- '200'
alter system set maintenance_io_concurrency = '200';
alter system set effective_io_concurrency = '200';
alter system set work_mem = '32768kb'; -- '20971kB'
alter system set shared_buffers = '4GB'; -- in AWS unit is 8kb, not 1 kb. so numeric value should be divided by 8 (524288)

alter system set max_worker_processes = '4';
alter system set max_parallel_workers_per_gather = '2';
alter system set max_parallel_workers = '4';
alter system set max_parallel_maintenance_workers = '2';

alter system set vacuum_cost_delay = 0;
alter system set autovacuum_vacuum_cost_delay = -1;

-- alter system set tcp_keepalives_idle = '7200';
-- alter system set tcp_keepalives_count = '10';
-- alter system set tcp_keepalives_interval = '60';


-- t3.large - 2 vCPU, 8 GB RAM, SSD

alter system set effective_cache_size = '6GB'; -- in AWS unit is 8kb, not 1 kb. so numeric value should be divided by 8 (786432)
alter system set maintenance_work_mem = '327680kb';
alter system set autovacuum_work_mem = '327680kb';
alter system set default_statistics_target = '256';
alter system set random_page_cost = '1.0';

alter system set max_connections = '100';
alter system set maintenance_io_concurrency = '200';
alter system set effective_io_concurrency = '200';
alter system set work_mem = '32768kb'; -- '10485kb'
alter system set shared_buffers = '2147488kb'; -- in AWS unit is 8kb, not 1 kb. so numeric value should be divided by 8 (268436)

alter system set vacuum_cost_delay = 0;
alter system set autovacuum_vacuum_cost_delay = -1;

-- alter system set tcp_keepalives_idle = '7200';
-- alter system set tcp_keepalives_count = '10';
-- alter system set tcp_keepalives_interval = '60';


-- t3.medium - 2 vCPU, 4 GB RAM, SSD

alter system set effective_cache_size = '3GB'; -- in AWS unit is 8kb, not 1 kb. so numeric value should be divided by 8 (393216)
alter system set maintenance_work_mem = '327680kb';
alter system set autovacuum_work_mem = '327680kb';
alter system set default_statistics_target = '256';
alter system set random_page_cost = '1.0';

alter system set max_connections = '100';
alter system set maintenance_io_concurrency = '200';
alter system set effective_io_concurrency = '200';
alter system set work_mem = '32768kb'; -- '10485kb'
alter system set shared_buffers = '1073742kb'; -- in AWS unit is 8kb, not 1 kb. so numeric value should be divided by 8 (134218)

alter system set vacuum_cost_delay = 0;
alter system set autovacuum_vacuum_cost_delay = -1;

-- alter system set tcp_keepalives_idle = '7200';
-- alter system set tcp_keepalives_count = '10';
-- alter system set tcp_keepalives_interval = '60';



-- t3.small - 2 vCPU, 2 GB RAM, SSD

alter system set effective_cache_size = '1536MB'; -- in AWS unit is 8kb, not 1 kb. so numeric value should be divided by 8 (196608)
alter system set maintenance_work_mem = '163840kb';
alter system set autovacuum_work_mem = '163840kb';
alter system set default_statistics_target = '256';
alter system set random_page_cost = '1.0';

alter system set max_connections = '100';
alter system set maintenance_io_concurrency = '200';
alter system set effective_io_concurrency = '200';
alter system set work_mem = '16384kb'; -- '5243kb'
alter system set shared_buffers = '536871kb'; -- in AWS unit is 8kb, not 1 kb. so numeric value should be divided by 8 (67109)

alter system set vacuum_cost_delay = 0;
alter system set autovacuum_vacuum_cost_delay = -1;

-- alter system set tcp_keepalives_idle = '7200';
-- alter system set tcp_keepalives_count = '10';
-- alter system set tcp_keepalives_interval = '60';