-- AWS RDS specific trick to drop user

-- -U sayjam

create role own login password '123';
grant api_dev to own;
grant sayjam to own;

-- -U own

reassign owned by api_dev to sayjam;
drop owned by api_dev;

drop role api_dev;

-- -U sayjam

drop role own;