-- /c raw_1

create schema if not exists etl;
create schema if not exists etl_dev;
create schema if not exists etl_agg;
create schema if not exists etl_agg_dev;
create schema if not exists etl_wrk;
create schema if not exists etl_wrk_dev;
create schema if not exists etl_log;
create schema if not exists public;
create schema if not exists auth;
create schema if not exists payment;
create schema if not exists utils;
create schema if not exists pub;
create schema if not exists aws_s3;
create schema if not exists aws_commons;

    -- common

revoke all on database postgres, template1, api, airflow
    from "read+write", datalore, grafana, airflow, airflow_admin, scraper, api_public_owner, etl_read_only, web, public;
grant connect on database raw_1
    to "read+write", datalore, grafana, airflow, airflow_admin, scraper, api_public_owner, etl_read_only, web;
revoke create on database raw_1
    from datalore, grafana, airflow, airflow_admin, scraper, api_public_owner, etl_read_only, public;
revoke create on schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    from "read+write", datalore, web, public;

revoke all on all tables in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    from "read+write", datalore, grafana, airflow, airflow_admin, scraper, api_public_owner, etl_read_only, web, public;
revoke all on all sequences in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    from "read+write", datalore, grafana, airflow, airflow_admin, scraper, api_public_owner, etl_read_only, web, public;
revoke all on all routines in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    from "read+write", datalore, grafana, airflow, airflow_admin, scraper, api_public_owner, etl_read_only, web, public;

alter default privileges in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    revoke all on tables from "read+write", datalore, grafana, airflow, airflow_admin, scraper, api_public_owner, etl_read_only, web, public;
alter default privileges in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    revoke all on sequences from "read+write", datalore, grafana, airflow, airflow_admin, scraper, api_public_owner, etl_read_only, web, public;
alter default privileges in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    revoke all on routines from "read+write", datalore, grafana, airflow, airflow_admin, scraper, api_public_owner, etl_read_only, web, public;

alter default privileges in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    grant select on tables to "read+write", datalore;
alter default privileges in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    grant select on sequences to "read+write", datalore;

revoke all on foreign server loopback_dblink_raw_1
    from "read+write", datalore, grafana, airflow, airflow_admin, scraper, api_public_owner, etl_read_only, web, public;

    -- read+write

grant create on database raw_1
    to "read+write";

grant usage on schema etl, etl_dev, etl_agg, etl_agg_dev, public, pub, auth, payment, utils
    to "read+write"; -- remove etl/etl_agg/etl_dev/etl_agg_dev when read replica will be ready
grant create on schema public, pub, auth, payment, utils
    to "read+write";

grant select, insert, update, delete, references on all tables in schema public, pub, auth, payment, utils
    to "read+write";
grant all on all sequences in schema public, pub, auth, payment, utils
    to "read+write";

grant all on function public.s3_export_function(text[])
    to "read+write";

grant select on all tables in schema etl, etl_dev, etl_agg, etl_agg_dev
    to "read+write"; -- remove when read replica will be ready
grant select on all sequences in schema etl, etl_dev, etl_agg, etl_agg_dev
    to "read+write"; -- remove when read replica will be ready

alter default privileges in schema public, pub, auth, payment, utils
    grant select, insert, update, delete, references on tables to "read+write";
alter default privileges in schema public, pub, auth, payment, utils
    grant all on sequences to "read+write";

alter default privileges in schema etl, etl_dev, etl_agg, etl_agg_dev
    grant select on tables to "read+write"; -- remove when read replica will be ready
alter default privileges in schema etl, etl_dev, etl_agg, etl_agg_dev
    grant select on sequences to "read+write"; -- remove when read replica will be ready

do
$func$
    declare
        tab_rec record;

    begin

        for tab_rec in
            select 'alter '
                || case
                    when relkind in ('r', 'p') then 'table'
                    when relkind = 'v' then 'view'
                    when relkind = 'm' then 'materialized view'
                end
                || ' if exists public.' || relname || ' owner to "read+write";' as qry
            from pg_catalog.pg_class
            where
              relkind in ('r', 'p', 'm', 'v')
              and relnamespace in ('auth'::regnamespace, 'payment'::regnamespace, 'utils'::regnamespace)
        loop
            execute tab_rec.qry;
            raise notice '%', tab_rec.qry;
        end loop;
    end;
$func$;

    -- datalore

revoke all on schema auth, payment, utils
    from datalore;
grant usage on schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, public
    to datalore;

grant select on all tables in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, public
    to datalore;
grant select on all sequences in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, public
    to datalore;
grant execute on all routines in schema etl_log
    to datalore;

alter default privileges in schema etl_log
    grant execute on routines to datalore;

    -- etl_read_only

grant usage on schema etl, etl_agg
    to etl_read_only;

grant select on all tables in schema etl, etl_agg
    to etl_read_only;
grant select on all sequences in schema etl, etl_agg
    to etl_read_only;

alter default privileges in schema etl, etl_agg
    grant select on tables to etl_read_only;
alter default privileges in schema etl, etl_agg
    grant select on sequences to etl_read_only;

    -- web

grant create on database raw_1
    to web;
grant usage on schema etl, etl_agg, etl_dev, etl_agg_dev, public
    to web;
grant all on schema auth, payment, utils
    to web;

grant select on all tables in schema etl, etl_agg, etl_dev, etl_agg_dev, public
    to web;
grant select on all sequences in schema etl, etl_agg, etl_dev, etl_agg_dev, public
    to web;

grant all on all tables in schema auth, payment, utils
    to web;
grant all on all sequences in schema auth, payment, utils
    to web;
grant all on all routines in schema auth, payment, utils
    to web;

alter default privileges in schema etl, etl_agg, etl_dev, etl_agg_dev, public
    grant select on tables to web;
alter default privileges in schema etl, etl_agg, etl_dev, etl_agg_dev, public
    grant select on sequences to web;

alter default privileges in schema auth, payment, utils
    grant all on tables to web;
alter default privileges in schema auth, payment, utils
    grant all on sequences to web;
alter default privileges in schema auth, payment, utils
    grant all on routines to web;

    -- grafana

revoke all on schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, auth, payment, utils, pub, public
    from grafana;
grant usage on schema public, etl_log
    to grafana;

grant select on all tables in schema etl_log, public
    to grafana;
grant select on all sequences in schema etl_log, public
    to grafana;
grant execute on all routines in schema etl_log
    to grafana;

alter default privileges in schema etl_log, public
    grant select on tables to grafana;
alter default privileges in schema etl_log, public
    grant select on sequences to grafana;
alter default privileges in schema etl_log
    grant execute on routines to grafana;

    -- airflow

revoke all on schema etl, etl_dev, etl_agg, etl_agg_dev, etl_log, etl_wrk, etl_wrk_dev, auth, payment, utils, pub, public
    from airflow;
grant usage on schema public, etl, etl_dev, etl_agg, etl_agg_dev, etl_log
    to airflow;

grant select on all tables in schema etl_log
    to airflow;
grant select on all sequences in schema etl_log
    to airflow;

grant execute on all routines in schema public, etl, etl_dev, etl_agg, etl_agg_dev, etl_log
    to airflow;

revoke all on function public.s3_export_function(text[])
    from airflow;

alter default privileges in schema etl_log
    grant select on tables to airflow;
alter default privileges in schema etl_log
    grant select on sequences to airflow;
alter default privileges in schema public, etl, etl_dev, etl_agg, etl_agg_dev, etl_log
    grant execute on routines to airflow;

    -- sayjam

grant all on database postgres, template1, raw_1
    to sayjam;
grant all on schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    to sayjam;
grant all on foreign server loopback_dblink_raw_1
    to sayjam;
grant all on all tables in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    to sayjam;
grant all on all sequences in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    to sayjam;
grant all on all routines in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    to sayjam;

alter default privileges in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    grant all on tables to sayjam;
alter default privileges in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    grant all on routines to sayjam;
alter default privileges in schema etl, etl_dev, etl_agg, etl_agg_dev, etl_wrk, etl_wrk_dev, etl_log, auth, payment, utils, pub, public, aws_s3, aws_commons
    grant all on sequences to sayjam;


-- /c api

    -- api_public_owner
create role api_public_owner
    nologin
    admin sayjam
    role scraper;

    -- common

grant connect on database api
    to scraper, airflow, grafana;
revoke create on database api
    from scraper, airflow, grafana, public;
revoke create on schema public
    from scraper, airflow, grafana, public;
grant usage on schema public
    to scraper, airflow, grafana;

revoke all on all tables in schema public
    from scraper, airflow, grafana, public;
revoke all on all sequences in schema public
    from scraper, airflow, grafana, public;
revoke all on all routines in schema public
    from scraper, airflow, grafana, public;

grant select on all tables in schema public
    to airflow, grafana;
grant select on all sequences in schema public
    to airflow, grafana;

alter default privileges in schema public
    revoke all on tables from scraper, airflow, grafana, public;
alter default privileges in schema public
    revoke all on routines from scraper, airflow, grafana, public;
alter default privileges in schema public
    revoke all on sequences from scraper, airflow, grafana, public;

alter default privileges in schema public
    grant select on tables to airflow, grafana;
alter default privileges in schema public
    grant select on sequences to airflow, grafana;

revoke all on foreign server loopback_dblink_api
    from scraper, airflow, grafana, api_public_owner, public;

    -- api_public_owner ownership

do
$func$
    declare
        alt_rec record;

    begin

        for alt_rec in
            select
              'alter table if exists public.' || relname || ' owner to api_public_owner' as com
            from pg_class
            where
                relnamespace = 'public'::regnamespace
                and relkind in ('r', 'p')
        loop
            execute alt_rec.com;
        end loop;

    end;
$func$;

    -- scraper

grant select, insert, update, delete on all tables in schema public
    to scraper;
grant all on all sequences in schema public
    to scraper;

alter default privileges in schema public
    grant select, insert, update, delete on tables to scraper;
alter default privileges in schema public
    grant all on sequences to scraper;

    -- airflow

grant execute on all routines in schema public
    to airflow;
alter default privileges in schema public
    grant execute on routines to airflow;

    -- sayjam

grant all on schema public
    to sayjam;
grant all on foreign server loopback_dblink_api
    to sayjam;
grant all on all tables in schema public
    to sayjam;
grant all on all sequences in schema public
    to sayjam;
grant all on all routines in schema public
    to sayjam;

alter default privileges in schema public
    grant all on tables to sayjam;
alter default privileges in schema public
    grant all on routines to sayjam;
alter default privileges in schema public
    grant all on sequences to sayjam;


-- /c airflow

    -- airflow_admin

create role airflow_admin with login password 'insert_password_here';

revoke create on database airflow
    from public;
revoke create on schema public
    from public;
revoke all on foreign server loopback_dblink_airflow
    from public;

grant all privileges on database airflow
    to airflow_admin;
grant all on schema public
    to airflow_admin;

alter role airflow_admin set search_path = public;

grant all on all tables in schema public
    to airflow_admin;
grant all on all sequences in schema public
    to airflow_admin;
grant all on all routines in schema public
    to airflow_admin;

alter default privileges in schema public
    grant all on tables to airflow_admin;
alter default privileges in schema public
    grant all on sequences to airflow_admin;
alter default privileges in schema public
    grant all on routines to airflow_admin;

do
$func$
    declare
        tab_rec record;

    begin

        for tab_rec in
            select 'alter table if exists public.' || relname || ' owner to airflow_admin;' as qry
            from pg_catalog.pg_class
            where
              relkind in ('r', 'p')
              and relnamespace = 'public'::regnamespace
        loop
            execute tab_rec.qry;
            raise notice '%', tab_rec.qry;
        end loop;
    end;
$func$;

    -- sayjam

grant all on schema public
    to sayjam;
grant all on all tables in schema public
    to sayjam;
grant all on all sequences in schema public
    to sayjam;
grant all on all routines in schema public
    to sayjam;
grant all on foreign server loopback_dblink_airflow
    to sayjam;

alter default privileges in schema public
    grant all on tables to sayjam;
alter default privileges in schema public
    grant all on routines to sayjam;
alter default privileges in schema public
    grant all on sequences to sayjam;