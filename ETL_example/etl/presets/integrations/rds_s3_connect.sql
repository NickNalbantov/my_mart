-- aws iam create-policy \
--    --policy-name etl-rds-s3-export-policy \
--    --policy-document '{
--      "Version": "2012-10-17",
--      "Statement": [
--        {
--          "Sid": "s3export",
--          "Action": [
--             "s3:PutObject",
--             "s3:AbortMultipartUpload"
--          ],
--          "Effect": "Allow",
--          "Resource": [
--            "arn:aws:s3:::m5ia882hy752r7hf-fiverr-scraping-api/*"
--          ]
--        }
--      ]
--    }'

-- aws iam create-role \
--    --role-name etl-rds-s3-export-role \
--    --assume-role-policy-document '{
--      "Version": "2012-10-17",
--      "Statement": [
--        {
--          "Effect": "Allow",
--          "Principal": {
--             "Service": "rds.amazonaws.com"
--           },
--          "Action": "sts:AssumeRole",
--          "Condition": {
--              "StringEquals": {
--                 "aws:SourceAccount": "715942603434",
--                 "aws:SourceArn": "arn:aws:rds:eu-central-1:715942603434:db:db-raw-1"
--                 }
--              }
--        }
--      ]
--    }'

-- aws iam attach-role-policy \
--    --policy-arn arn:aws:iam::715942603434:policy/etl-rds-s3-export-policy \
--    --role-name etl-rds-s3-export-role



create extension if not exists aws_s3 cascade;


-- public

drop table if exists public.s3_export_queries cascade;

create table if not exists public.s3_export_queries
(
    export_id smallint generated always as identity primary key,
    file_name text,
    db_query text
);

create or replace function public.s3_export_function
(
    file_names text[] default null::text[]
)
returns table
(
    export_duration interval,
    export_start timestamptz,
    export_end timestamptz,
    rows_uploaded bigint,
    files_uploaded bigint,
    bytes_uploaded bigint,
    export_id smallint,
    file_name text,
    db_query text
)
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
    declare
        result_rec record;
        export_rec record;
        export_iteration_start timestamptz;

    begin

        for result_rec in
            select
                m.export_id,
                m.file_name,
                m.db_query
            from public.s3_export_queries m
            where
                m.file_name = any(file_names)
                or file_names is null
            order by
                m.export_id
        loop
            export_iteration_start := clock_timestamp();

            raise notice '% - Starting export of /tasks/%.csv',
                export_iteration_start,
                result_rec.file_name;

            select
                clock_timestamp() - export_iteration_start as export_duration,
                export_iteration_start as export_start,
                clock_timestamp() as export_end,
                e.rows_uploaded,
                e.files_uploaded,
                e.bytes_uploaded
            into export_rec
            from aws_s3.query_export_to_s3
                (
                    query := result_rec.db_query,
                    s3_info := aws_commons.create_s3_uri
                    (
                        bucket := 'm5ia882hy752r7hf-fiverr-scraping-api',
                        file_path := 'tasks/' || result_rec.file_name || '.csv',
                        region := 'eu-central-1'
                    ),
                    options := 'format csv, header true'
                ) e;

            return query
                select
                    export_rec.export_duration,
                    export_rec.export_start,
                    export_rec.export_end,
                    export_rec.rows_uploaded,
                    export_rec.files_uploaded,
                    export_rec.bytes_uploaded,
                    result_rec.export_id,
                    result_rec.file_name,
                    result_rec.db_query;

            raise notice E'\nexport_duration: %,\nexport_start: %,\nexport_end: %,\nrows_uploaded: %,\nfiles_uploaded: %,\nbytes_uploaded: %,\nexport_id: %,\nfile_name: %\n\n',
                export_rec.export_duration,
                export_rec.export_start,
                export_rec.export_end,
                export_rec.rows_uploaded,
                export_rec.files_uploaded,
                export_rec.bytes_uploaded,
                result_rec.export_id,
                result_rec.file_name;

        end loop;

        return;

    end;
$func$;


insert into public.s3_export_queries (file_name, db_query)
    select
        'db_gig_pages'::text as file_name,
        $qry$
            with url as
            (
                select distinct on (g.gig_id)
                    g.gig_id,
                    concat(s.username, '/', g.gig_cached_slug) as gig_url
                from
                (
                    select
                        id,
                        gig_id,
                        fiverr_seller_id,
                        gig_cached_slug,
                        created_at
                    from etl.gigs_revisions_pre_metrics

                    union all

                    select
                        id,
                        gig_id,
                        fiverr_seller_id,
                        gig_cached_slug,
                        created_at
                    from etl.gigs_revisions_pre_metrics_invalid
                ) as g
                join public.sellers s on
                    g.fiverr_seller_id = s.fiverr_seller_id
                order by
                    g.gig_id,
                    g.created_at desc,
                    g.id desc
            ),

            cur as
            (
                select
                    gig_id
                from etl.gig_current_status
                where
                    fiverr_status in ('blocked', 'deleted', 'denied', 'suspended')
            )

            select
                g.id,
                u.gig_url
            from public.gigs g
            join url u on
                g.id = u.gig_id
            where not exists
                (
                    select
                    from etl.gig_current_status s
                    where
                        g.id = s.gig_id
                )
            union all

            select
                g.id,
                u.gig_url
            from public.gigs g
            join url u on
                g.id = u.gig_id
            where not exists
                (
                    select
                    from cur c
                    where
                        g.id = c.gig_id
                )
        $qry$::text as db_query

    union all

    select
        'db_gigs'::text as file_name,
        $qry$
            with cur as
            (
                select
                    gig_id
                from etl.gig_current_status
                where
                    fiverr_status in ('blocked', 'deleted', 'denied', 'suspended')
            )

            select
                g.id,
                g.fiverr_gig_id
            from public.gigs g
            where not exists
                (
                    select
                    from etl.gig_current_status s
                    where
                        g.id = s.gig_id
                )

            union all

            select
                g.id,
                g.fiverr_gig_id
            from public.gigs g
            where not exists
                (
                    select
                    from cur c
                    where
                        g.id = c.gig_id
                )
        $qry$::text as db_query

    union all

    select
        'new_catalog_gigs'::text as file_name,
        $qry$
            select
                sub_category_url as url
            from etl.category_tree_latest_revision

            union

            select
                nested_sub_category_url as url
            from etl.category_tree_latest_revision
            where
                nested_sub_category_url is not null
        $qry$::text as db_query

    union all

    select
        'db_sellers'::text as file_name,
        $qry$
            with sel as
            (
                select
                    s.joined_at,
                    s.id as seller_id,
                    s.username as seller_name
                from public.sellers s
                where not exists
                    (
                        select
                        from etl.seller_current_status h
                        where s.id = h.seller_id
                    )

                union all

                select
                    s.joined_at,
                    s.id as seller_id,
                    s.username as seller_name
                from public.sellers s
                where exists
                    (
                        select
                        from etl.seller_current_status h
                        where
                            s.id = h.seller_id
                            and h.status = 'available'
                    )
            ),

            last_rev as
            (
                select distinct on (s.seller_id)
                    s.joined_at,
                    s.seller_id,
                    s.seller_name,
                    g.fiverr_gig_id,
                    m.id as gig_revision_id,
                    m.schema_version
                from sel s
                join public.gigs g
                    on s.seller_id = g.seller_id
                join public.gigs_revisions_meta m
                    on g.id = m.gig_id
                order by
                    s.seller_id,
                    m.created_at desc,
                    m.id desc
            ),

            gig_arr as
            (
                select
                    seller_id,
                    array_agg(fiverr_gig_id) as fiverr_gig_id_arr
                from public.gigs
                group by
                    seller_id
            )

            select
                l.joined_at,
                l.seller_id,
                l.fiverr_gig_id,
                'https://www.fiverr.com/'
                    || coalesce
                    (
                        case
                            when l.schema_version = 1 then r.raw['data']['gig'] ->> 'seller_name'
                            when l.schema_version in (2, 3, 4) then r.raw ->> 'seller_name'
                        end,
                        l.seller_name
                    ) as seller_url,
                a.fiverr_gig_id_arr
            from last_rev l
            join public.gigs_revisions r
                on r.id = l.gig_revision_id
            join gig_arr a
                on l.seller_id = a.seller_id
        $qry$::text as db_query

    union all

    select
        'gig_reviews'::text as file_name,
        $qry$
            select distinct
                g.fiverr_gig_id,
                r.last_fiverr_review_id as fiverr_review_id
            from etl.gigs_revisions_pre_metrics g
            join etl.gig_reviews_agg r
                on g.gig_id = r.gig_id
            where
                g.gig_ratings_count > r.reviews_count
        $qry$::text as db_query

    union all

    select
        'gig_reviews_with_buying_rating_count'::text as file_name,
        $qry$
            select distinct
                g.fiverr_gig_id
            from etl.gigs_revisions_pre_metrics g
            where
                g.gig_ratings_count > 0
                and not exists
                    (
                        select
                        from etl.gig_reviews_agg rm
                        where g.gig_id = rm.gig_id
                    )
        $qry$::text as db_query;

cluster public.s3_export_queries using s3_export_queries_pkey;

analyze public.s3_export_queries;

grant execute on function public.s3_export_function to "read+write";