# ETL repo README

Check more details about ETL and maintenance DAGs in [Airflow repo README](https://gitlab.com/fiverr-dev/data-engineering/airflow/-/blob/main/README.md)

---

## /etl

Set of functions for data processing

### regular_etl_sequential

Default call to launch ETL job:

```sql
select etl.regular_etl_sequential();
```

#### Optional arguments

- *metrics_start_dttm* - default (current_date::timestamp at time zone 'UTC' - '1 month'::interval)::timestamptz; lower bound for general metrics calculation time period
- *trends_start_dttm* - default (current_date::timestamp at time zone 'UTC' - '3 month'::interval)::timestamptz; lower bound for trends calculation time period
- *end_dttm* - default (current_date::timestamp at time zone 'UTC' + '1 day'::interval)::timestamptz; upper bound for calculation time period
- *trends_intervals* - default 3; amount of intervals on x-axis for trend line ({trends_intervals} + 1 = amount of points on x-axis)
- *trends_students_coeff* - default 1.96; critical value of correlation coefficient for Student's coefficient (by default - confidence interval = 0.95, t-statistics = 1.96)
- *ratings_to_volume_coeff* - default 0.8; arbitrary coefficient representing ratio of reviews conversion to sales
- *overpriced_coeff* - default 10; crutch to filter gigs with suspiciously high prices (higher than weighted average price in subcategory multiplied by this coefficient)
- *competition_score_params* - set of parameters for *etl_dev.competition_score* function (competition_score.sql) for calculating competition_score_xxx attributes; default:

```json
        {
            "volume_exp_scale_coeff": 100,
            "sigmoid_scale_coeff": -5,
            "sigmoid_shift_coeff": 0.5,
            "competition_score_scale_coeff": 0.8,
            "competition_score_add_coeff": 0.1,
            "volume_scale_coeff": 0.5
        }
```

- *rank_scale_factor* - default 0.0001; scaling coefficient for *etl.relevance_sorting* function
- *debug_mode* - default false; if true - all intermediate unlogged tables won't be dropped after the end of function execution; if false - only table + view with final results will be preserved
- *dag_name* - default 'manual_launch'; name of the process that triggered this function
- *cleanup_preserve* - default 40; defines how many old calculation results will be preserved after initial cleanup; by default - there'll stay last 40 [xxx_]metrics_{some old *load_id*} tables
- *cluster_threshold* - default 0.2; threshold value of the ratio between modified / inserted rows and n_live_tup or the ratio of uncorrelated keys of the index that used for clusterization for *pre_materialization* targets'. CLUSTER + ANALYZE commands are being launched if threshold is reached
- *ddl_change_mode* - default false; if true then targets (non-technical tables only) will be dropped and recreated instead of truncation (if false)

#### Calculations

- picking up all gig_id's in timerange {*metrics_start_dttm*} and {*end_dttm*}
- picking up all gig_id's that are not present in timeframe for regular metrics - timerange bounds are ({*metrics_start_dttm*} - ({*end_dttm*} - {*metrics_start_dttm*})) and {*metrics_start_dttm*}
- union both types of gig_id's, marked with "heuristic" column
- calculation of all metrics (except trends)
- calculation of all trends in timerange between {*trends_start_dttm*} and {*end_dttm*}
- calculation of heuristical trends for gig_id's that are not present in timeframe for regular metrics - timerange bounds are ({*metrics_start_dttm*} - ({end_dttm} - {*metrics_start_dttm*})) and {*metrics_start_dttm*}
- union trends for both types of gig_id's
- joining metrics and trends together
- calculating additional aggregating metrics (min / max / avg / rank etc.)

#### Job flow

##### Initial steps

- Validating input arguments' values
- Cleaning up old backups (preserving only last {*cleanup_preserve*} calculations) and target tables (prepare_targets.sql)

##### Subsequent subroutine calls

- *etl.prepare_targets* (prepare_targets.sql)
- *etl.pre_materialization* (pre_materialization.sql) - see description below
- *etl.metrics_func* (metrics.sql)
- *etl.trends_func* (trends.sql)
  - iterates over {trends_intervals} number of calls of *etl.trend_period_func* (trend_period.sql) - i.e. calculates trends over {trends_intervals} time periods between {trends_start_dttm} and {end_dttm}
- *etl.etl_statistics* (etl_statistics.sql)

##### Final steps

- joining results of metrics_func and trends_func
- gathering results of two previous steps into final table (*etl.metrics_{load_id}*)
- calculating global ranks for each entity (using *etl.relevance_sorting* function - relevance_sorting.sql)
- renaming columns
- inserting data into target tables, restoring indexes
- making backup of current calculation (with _{*load_id*} in the names)
- gathering statistics about last run (calling *etl.etl_statistics*, generating query, and inserting into *etl_log.etl_attributes_stats* table)

#### Results

*etl.regular_etl_sequential* automatically rebuilds these tables:

- *etl.metrics* (all metrics combined)
- *etl.gigs*
- *etl.sellers*
- *etl.categories*
- *etl.subcategories*
- *etl.nested_subcategories*
- *etl.all_categories*
- *etl.category_x_seller_levels*
- *etl.subcategory_x_seller_levels*
- *etl.nested_subcategory_x_seller_levels*

Function returns JSONB with function's *load_id*, total runtime of the function and status message: either "Success" or full information about the error (exception text, code, context, details, hint)

#### Testing

All subroutines can be called separately for the purpose of testing - preferably in debug mode, because otherwise they won't materialize any tables.

### pre_materialization

This function parses raw JSON data from *public.gigs_revisions*, *public.gigs_pages*, *public.sellers_revisions* and *public.gig_reviews* tables, and places all necessary values for metrics and trends calculations into plain tables.

Every consequent call of *etl.pre_materialization* will make an incremental insert / update / merge into resulting tables

Subsequent steps from *etl.regular_etl_sequential* function will load all data from *etl.pre_materialization* target tables rather than from source tables with raw JSONs.

Results of parsing are stored as tables:

- *etl.gigs_revisions_pre_metrics*
- *etl.gigs_pages_pre_metrics*
- *etl.sellers_revisions_pre_metrics*
- *etl.gig_status_history*
- *etl.seller_status_history*
- *etl.gig_reviews_agg*
- *etl.gig_reviews_prices*

*etl.gig_status_history* table is used to evaluate gig contribution to seller / category revenue during period of time that was chosen for metrics' calculations
*etl.seller_status_history* table is used for filtering only sellers active at the calculation period
*etl.gig_reviews_prices* is used for getting possible values of gigs' prices
There are also active slices for *etl.gig_status_history* and *etl.seller_status_history* tables - views *etl.gig_current_status* and *etl.seller_current_status*

*etl.gig_reviews_agg* table is used by backend for gig reviews incremental scraping.

There are several filters applied to parsed JSON data:

#### etl.gigs_revisions_pre_metrics

- fiverr_status = 'approved'
- fiverr_gig_id is not null
- fiverr_seller_id is not null
- category_id is not null
- sub_category_id is not null
- gig_ratings_count is not null
- prices is not null
- gig_is_pro is not null
- gig_cached_slug is not null
- scraped_at = max(scraped_at) over (partition by gig_id, revision)
- gig_preview_url is a first media URL with type 'image'
  - gig_preview_url is not null

#### etl.gigs_pages_pre_metrics

- prices is not null
- seller_completed_orders_count is not null
- fiverr_gig_id is not null
- fiverr_seller_id is not null
- scraped_at = max(scraped_at) over (partition by seller_id, revision)

#### etl.sellers_revisions_pre_metrics

- fiverr_seller_id is not null
- seller_name is not null
- seller_is_pro is not null
- scraped_at = max(scraped_at) over (partition by seller_id, revision)

All rows that does not match these filtering conditions are inserting into separate tables with *_invalid* postfix.
These tables are not used in metrics / trends calculations, but could be useful for analysis of invalid data.

#### Arguments

- *load_id*, *debug_mode*, *dag_name* - inherited from *etl.regular_etl_sequential*
- *high_level_step* - default '3'; prefix for function's steps numbers (used for logging)

### /etl/parallel_etl

This folder contains decomposed version of ETL functions described above (each step as a separate PL/pgSQL function) for parallelized ETL - launching from Airflow *etl_regular* DAG with ```"parallel_mode": True``` setting

---

## /etl_agg

Another version of ETL flow made saving for aggregated metrics from previous periods

### agg_etl_sequential

#### Optional arguments

A few differences from *etl.regular_etl_sequential* function:

- *cleanup_preserve* is not present because there are no backups for the aggregated ETL
- *ddl_change_mode* is not present because function can't recalculate all partitions in one run, so this mode is useless
- new *tables_prefix* argument - string that represents a period for the metrics aggregation. Valid formats:
  - 'mX_YYYY' - for monthly aggregations, where X - month number, YYYY - year
  - 'qX_YYYY' - for quarterly aggregations, where X - quarter number, YYYY - year
  - 'yYYYY' - for yearly aggregations, where YYYY - year

*tables_prefix* and all timestamptz parameters are set by default for the data aggregation for the last month from current date

Detailed description of default parameters for different types of aggregation can be found in [Airflow repo README](https://gitlab.com/fiverr-dev/data-engineering/airflow/-/blob/main/README.md)

#### Job flow

##### Initial steps

- Validating input arguments' values
- Entirely skips backup cleaning and prematerialization steps in contrast with regular ETL

##### Subsequent subroutine calls

- *etl_agg.agg_etl_prepare_targets* (agg_etl_prepare_targets.sql) - version of *etl.prepare_targets* function but without tech and history tables and with dynamically generated mart tables names
- reusing *etl.metrics_func*, *etl.trends_func* and *etl.etl_statistics* without any changes

##### Final steps

Same steps as in regular ETL except skipping backup creation - all metrics are being inserted directly into target tables

#### Results

Function builds the same set of tables as *etl.regular_etl_sequential* function does, but inside *etl_agg* schema and with names starting with *tables_prefix* value for a corresponding aggregation period
Example: *etl_agg.m6_2024_all_categories*

### /etl_agg/parallel_etl

This folder contains decomposed version of ETL functions described above (each step as a separate PL/pgSQL function) for parallelized ETL - launching from Airflow *etl_agg_monthly* / *etl_agg_quarterly* / *etl_agg_yearly* DAG with ```"parallel_mode": True``` setting

---

## /etl_log

Every major step in call stack reports to SQL console via RAISE NOTICE / RAISE EXCEPTION and inserts data to table *etl_log.etl_log* via function *etl_log.etl_log_writer* (etl_log_writer.sql) in autonomous transaction (requires dblink extension, foreign server and user mapping - see dblink.sql)

Last step of *etl.regular_etl_sequential* also inserts various statistics about results of last run into *etl_log.etl_attributes_stats* table - see DDL in etl_attributes_stats.sql

There are also several views and functions built upon these tables (runs_metrics.sql and stats_metrics.sql):

- *etl_log.last_run_log* - view, log of a latest launch of any ETL-function
- *etl_log.last_run_rows_metrics* - view, compares number of rows inserted and updated in the last run of *regular_etl_sequential* and *pre_materialization* functions with min / avg / max number of inserted and updated rows by previous runs of the same function (within 30 days)
- *etl_log.last_run_runtime_metrics* - view, compares duration of the last run of every available ETL-function with min / avg / max duration of the run from previous runs of the same function (within 30 days)
- *etl_log.last_run_attributes_stats* - view, compares attributes statistics from the last run of ETL with min / avg / max of corresponding statistics from previous runs (within 30 days)
- *etl_log.run_rows_metrics* - function that works like *last_run_rows_metrics* view but for a chosen *load_id* and chosen depth of history lookup (instead of the latest *load_id* and 30 days)
- *etl_log.run_runtime_metrics* - function that works like *last_run_runtime_metrics* view but for a chosen *load_id* and chosen depth of history lookup (instead of the latest *load_id* and 30 days)
- *etl_log.run_attributes_stats* - function that works like *last_run_attributes_stats* view but for a chosen *load_id* and chosen depth of history lookup (instead of the latest *load_id* and 30 days)

- grafana_panels.sql - contains queries for a corresponding panels in [ETL dashboard](https://grafana-kube1.sayjam.com/d/adiwaz8s188w0e/etl?orgId=1&refresh=12h) in Grafana

---

## /presets

Set of supporting scripts that has to be executed in a database before initial launch of ETL job

- settings.sql - script for changing system configs. For info only, as the real configuration is being changed via AWS parameter groups

### /category_tree

- category_tree.sql - DDL and MERGE scripts for *public.category_tree*
- category_tree_latest_revision.sql - DDL for views that pick latest revisions of categories from *public.category_tree*
- JSONS with raw data for *public.category_tree*

### /data_processing

- first_last_agg.sql - custom aggregates *public.first* and *public.last*
- array_sort_distinct.sql - custom function *public.array_distinct_sort* for sorting and grouping values in arrays

### /dcl

- dcl_drop_template.sql - template script with AWS-specific way to drop role
- dcl_main.sql - template script for granting / revoking privileges to objects in ETL DB (raw_1), Scraping API DB (api) and Airflow DB (airflow)

### /integrations

- rds_s3_connect.sql - script that setting up objects used for exporting data from specific DB queries to S3

### /maintenance

- maintenance_bloat_cleanup.sql - technical function *public.maintenance_bloat_cleanup* for the [maintenance_cleanup](https://airflow.fiverr-dev.org/dags/maintenance_cleanup/grid) DAG
- maintenance_etl_attributes_stats_cleanup.sql - technical function *public.maintenance_etl_attributes_stats_cleanup* for the [maintenance_cleanup](https://airflow.fiverr-dev.org/dags/maintenance_cleanup/grid) DAG, removes old rows from *etl_log.etl_attributes_stats* table
- maintenance_etl_log_cleanup.sql - technical function *public.maintenance_etl_log_cleanup* for the [maintenance_cleanup](https://airflow.fiverr-dev.org/dags/maintenance_cleanup/grid) DAG, removes old rows from *etl_log.etl_log* table
- maintenance_idle_killer.sql - technical function *public.maintenance_idle_killer* for the [maintenance_idle_killer](https://airflow.fiverr-dev.org/dags/maintenance_idle_killer/grid) DAG

### /migrations

- Historical scripts for creation development schema *public_dev* and merging it back into *public* schema
- etl_dev_tech_tables_copy.sql - script for copying targets of *etl.pre_materialization* function to *etl_dev* schema without full ETL recalculation
- scraping_api_and_airflow_migration.sql - script for moving data from other RDS instances to the main ETL cluster

### /partitions

- get_partitions_tree.sql - technical function *public.get_partitions_tree* that returns a table with hierarchical tree of all partiions and subpartitions for the given root table (used in regular ETL)
- get_range_partitions_ddl.sql - technical function *public.get_range_partitions_ddl* that returns a table with the metadata of a new partitioning design for a given settings applied to an existing partitioned table (used in regular ETL and aggregated ETL)
- maintenance_partitions_autovacuum_switch.sql - technical function *public.maintenance_partitions_autovacuum_switch* that helps to enable / disable autovacuum for each partition of the partitioned table (used in regular ETL)
- maintenance_partitions_merge.sql - technical function *public.maintenance_partitions_merge* that rearranges patitions and data in them according to the given settings for an existing partitioned table, based on results of *public.get_range_partitions_ddl* function (used in regular ETL)
- partitioning_test.sql - test case for *public.maintenance_partitions_merge* function

### /pub

DDL and raw data for public API schema *pub*

### /supporting_tasks

Various additional scripts, such as:

- attributes_adding_example.sql - example of a script for manual addition of attributes to some target table of regular and aggregated ETL
- public_sources_extended_statistics.sql - DDL of extended statistics objects for some sources tables in schema *public*

---

## /replication

Scripts for creating publication and subscription for different combinations of publication / subscription:

- *etl* - all tables from *etl* schema
- *etl_agg* - all tables from *etl_agg* schema
- *etl_history* - only *gig_status_history*, *seller_status_history*, *gig_review_agg* history tables from schema *etl*
- *etl_marts* - only main targets of ETL procedures with business data from schema *etl*
- *etl_tech* - only technical tables (*pre_metrics* / *pre_metrics_invalid* and *metrics*) from schema *etl*

---

## /etl_dev

Development versions of all functions from **/etl** folder (and *etl* schema)
All these functions are writing data in the same targets as their counterparts from *etl* schema but in *etl_dev* schema

---

## /etl_agg_dev

Development versions of all functions from **/etl_agg** folder (and *etl_agg* schema)
All these functions are writing data in the same targets as their counterparts from *etl_agg* schema but in *etl_agg_dev* schema
