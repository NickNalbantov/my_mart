create or replace function etl_dev.af_mj_prep_start
(
    metrics_start_dttm timestamptz default (current_date::timestamp at time zone 'UTC' - '1 month'::interval)::timestamptz,
    trends_start_dttm timestamptz default (current_date::timestamp at time zone 'UTC' - '3 month'::interval)::timestamptz,
    end_dttm timestamptz default (current_date::timestamp at time zone 'UTC' + '1 day'::interval)::timestamptz,
    trends_intervals smallint default 3,
    trends_students_coeff numeric default 1.96, -- critical value of correlation coefficient for Student's coefficient (by default - confidence interval = 0.95, t-statistics = 1.96)
    ratings_to_volume_coeff numeric default 0.8, -- arbitrary coefficient representing ratio of reviews conversion to sales
    competition_score_percentile numeric default 0.95, -- percentile of inversed sales ratio metrics which used for calculations of gigs' competition scores
    competition_score_params jsonb default -- set of parameters for etl_dev.competition_score function for calculating competition_score_xxx attributes
        $js$
            {
                "volume_exp_scale_coeff": 100,
                "sigmoid_scale_coeff": -5,
                "sigmoid_shift_coeff": 0.5,
                "competition_score_scale_coeff": 0.8,
                "competition_score_add_coeff": 0.1,
                "volume_scale_coeff": 0.5
            }
        $js$::jsonb,
    cleanup_preserve smallint default 40, -- 10 metrics tables * 4 last job runs
    cluster_threshold numeric default 0.2, -- threshold value of the ratio between modified / inserted rows and n_live_tup or the ratio of uncorrelated keys of the index that used for clusterization for *pre_materialization* targets'. CLUSTER + ANALYZE commands are being launched if threshold is reached
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '1.1'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.1' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            metrics_start_dttm,
            trends_start_dttm,
            end_dttm,
            trends_intervals,
            trends_students_coeff,
            ratings_to_volume_coeff,
            competition_score_percentile,
            competition_score_params,
            cleanup_preserve,
            cluster_threshold,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Custom exceptions

        if metrics_start_dttm >= end_dttm then
            raise exception 'Value of argument "metrics_start_dttm" should be less than value of argument "end_dttm"'
                using errcode = 'invalid_parameter_value';

        elsif trends_start_dttm >= end_dttm then
            raise exception 'Value of argument "trends_start_dttm" should be less than value of argument "end_dttm"'
                using errcode = 'invalid_parameter_value';

        elsif trends_intervals < 2 then
            raise exception 'Value of argument "trends_intervals" should not be less than 2'
                using errcode = 'invalid_parameter_value';

        elsif trends_intervals > (end_dttm::date - trends_start_dttm::date) then
            raise exception 'Value of argument "trends_intervals" should not be greater than the difference between "end_dttm" and "trends_start_dttm" arguments'' values in days'
                using errcode = 'invalid_parameter_value';

        elsif trends_intervals > 12 then
            raise exception 'No more than 12 trend periods iterations ("trends_intervals") are allowed'
                using errcode = 'invalid_parameter_value';

        elsif trends_students_coeff <= 0 then
            raise exception 't-distribution coefficient ("trends_students_coeff") can''t be <= 0'
                using errcode = 'invalid_parameter_value';

        elsif not ratings_to_volume_coeff <@ '(0, 1]'::numrange then
            raise exception 'Value of argument "ratings_to_volume_coeff" must be in range (0, 1]'
                using errcode = 'invalid_parameter_value';

        elsif not competition_score_percentile <@ '(0, 1)'::numrange then
            raise exception 'Value of argument "competition_score_percentile" must be in range (0, 1)'
                using errcode = 'invalid_parameter_value';

        elsif not (competition_score_params ->> 'sigmoid_shift_coeff')::numeric <@ '[0, 1]'::numrange then
            raise exception 'Value of argument "competition_score_params"[''sigmoid_shift_coeff''] must be in range [0, 1]'
                using errcode = 'invalid_parameter_value';

        elsif not (competition_score_params ->> 'competition_score_scale_coeff')::numeric <@ '[0, 1]'::numrange then
            raise exception 'Value of argument "competition_score_params"[''competition_score_scale_coeff''] must be in range [0, 1]'
                using errcode = 'invalid_parameter_value';

        elsif not (competition_score_params ->> 'competition_score_add_coeff')::numeric <@ '[0, 1]'::numrange then
            raise exception 'Value of argument "competition_score_params"[''competition_score_add_coeff''] must be in range [0, 1]'
                using errcode = 'invalid_parameter_value';

        elsif not (competition_score_params ->> 'volume_scale_coeff')::numeric <@ '[0, 1]'::numrange then
            raise exception 'Value of argument "competition_score_params"[''volume_scale_coeff''] must be in range [0, 1]'
                using errcode = 'invalid_parameter_value';

        elsif not cleanup_preserve::integer <@ '[0, 90]'::int4range or mod(cleanup_preserve, 10) != 0 then
            raise exception 'Value of argument "cleanup_preserve" must be in range [0, 90] and be the multiple of 10'
                using errcode = 'invalid_parameter_value';

        elsif not cluster_threshold <@ '[0, 1]'::numrange then
            raise exception 'Value of argument "cluster_threshold" must be in range [0, 1]'
                using errcode = 'invalid_parameter_value';

        end if;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            metrics_start_dttm,
            trends_start_dttm,
            end_dttm,
            trends_intervals,
            trends_students_coeff,
            ratings_to_volume_coeff,
            competition_score_percentile,
            competition_score_params,
            cleanup_preserve,
            cluster_threshold,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;