create or replace function etl_dev.af_premat_gigs_revisions_parsed_valid
(
    cluster_threshold numeric default 0.2, -- threshold value of the ratio between modified / inserted rows and n_live_tup or the ratio of uncorrelated keys of the index that used for clusterization for *pre_materialization* targets'. CLUSTER + ANALYZE commands are being launched if threshold is reached
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '4.1'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
#variable_conflict use_variable

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

        empty_incr_flg boolean;

        updated_partitions_arr oid[];
        partitions_maintenance_rec record;
        stats_columns_list text[];
        maintenance_qry text;
        analyze_root_flg boolean;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            cluster_threshold,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - inserting valid data into pre-metrics table

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.1' as src_step,
            'etl_dev' as tgt_schema,
            'gigs_revisions_pre_metrics' as tgt_name,
            'insert' as op_type
        into log_rec;

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.tgt_name = 'gigs_revisions_id_increment';

        if empty_incr_flg is false then

            perform public.maintenance_partitions_autovacuum_switch('etl_dev.gigs_revisions_pre_metrics'::regclass, false);

            insert into etl_dev.gigs_revisions_pre_metrics
                (
                    "load_id",
                    processed_dttm,
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    id,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    gig_ratings_count,
                    gig_is_pro,
                    gig_rating,
                    prices,
                    gig_title,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name,
                    gig_cached_slug,
                    gig_preview_url
                )
                select
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    id,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    gig_ratings_count,
                    gig_is_pro,
                    gig_rating,
                    prices,
                    gig_title,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name,
                    gig_cached_slug,
                    gig_preview_url
                from etl_wrk_dev.gigs_revisions_parsed
                    -- Data-quality criteria
                where
                    prices != array[]::numeric[]
                    and array_positions(prices, null) = array[]::int[]
                    and gig_is_pro is not null
                    and gig_cached_slug is not null
                    and gig_preview_url is not null
                    and fiverr_gig_id is not null
                    and fiverr_seller_id is not null
                    and category_id is not null
                    and sub_category_id is not null;

            get diagnostics
                row_cnt := row_count;

            create unique index if not exists uidx_gigs_revisions_pre_metrics_id
                on etl_dev.gigs_revisions_pre_metrics (id, scraped_at desc);
            create index if not exists idx_gigs_revisions_pre_metrics_gig_id
                on etl_dev.gigs_revisions_pre_metrics (gig_id, scraped_at desc);
            create index if not exists idx_gigs_revisions_pre_metrics_fiverr_seller_id
                on etl_dev.gigs_revisions_pre_metrics (fiverr_seller_id, scraped_at desc);
            create index if not exists idx_gigs_revisions_pre_metrics_scraped_at
                on etl_dev.gigs_revisions_pre_metrics (scraped_at desc);
            create index if not exists idx_gigs_revisions_pre_metrics_load_id
                on etl_dev.gigs_revisions_pre_metrics ("load_id" desc, scraped_at desc);

            alter table if exists etl_dev.gigs_revisions_pre_metrics
                replica identity using index uidx_gigs_revisions_pre_metrics_id;

                -- Clustering

            analyze_root_flg := false;

            stats_columns_list := array[
                    'id', 'gig_id', 'fiverr_gig_id', 'fiverr_seller_id',
                    'category_id', 'sub_category_id', 'nested_sub_category_id'
                ];

                -- Finding partitions where new rows were inserted

            with part_key_values as
            (
                select
                    min(scraped_at) as min_part_key_value,
                    max(scraped_at) as max_part_key_value
                from etl_wrk_dev.gigs_revisions_parsed
            )

            select
                array_agg(b.table_oid)
            into updated_partitions_arr
            from
            (
                select
                    a.table_oid,
                    case
                        when a.relpartbound = 'DEFAULT' then '2000-01-01 00:00:00 UTC'::timestamptz
                        when regexp_substr(a.relpartbound, '(?<=FROM \()(.*)(?=\) TO)') = 'MINVALUE' then '2000-01-01 00:00:00 UTC'::timestamptz
                        else regexp_substr(a.relpartbound, '(?<=FROM \('')(.*)(?=''\) TO)')::timestamptz
                    end as value_from,
                    case
                        when a.relpartbound = 'DEFAULT' then '2099-01-01 00:00:00 UTC'::timestamptz
                        when regexp_substr(a.relpartbound, '(?<=TO \()(.*)(?=\))') = 'MAXVALUE' then '2099-01-01 00:00:00 UTC'::timestamptz
                        else regexp_substr(a.relpartbound, '(?<=TO \('')(.*)(?=''\))')::timestamptz
                    end as value_to
                from
                (
                    select
                        t.table_oid,
                        pg_catalog.pg_get_expr(c.relpartbound, t.table_oid, true) as relpartbound
                    from public.get_partitions_tree
                        (
                            (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass
                        ) t
                    join pg_catalog.pg_class c
                        on t.table_oid = c."oid"
                    where
                        t.relkind in ('r', 'f')
                ) a
            ) b
            cross join part_key_values p
            where -- overlapping ranges
                tstzrange(p.min_part_key_value, p.max_part_key_value) && tstzrange(b.value_from, b.value_to);

                -- Clustering loop

            for partitions_maintenance_rec in
                select
                    quote_ident(t.schema_name) as partition_schema,
                    quote_ident(t.table_name) as partition_name,
                    quote_ident(ci.relname) as cluster_index_name,
                    (
                        coalesce
                            (
                                greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric,
                                0
                            )::numeric >= cluster_threshold
                        or 1::numeric - coalesce(abs(st.correlation), 0)::numeric >= cluster_threshold
                    ) as do_cluster_flg_partition
                from public.get_partitions_tree
                    (
                        (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass
                    ) t
                join pg_catalog.pg_attribute a
                    on a.attrelid = t.table_oid
                join pg_catalog.pg_index ix
                    on ix.indrelid = a.attrelid
                    and ix.indkey[0] = a.attnum
                join pg_catalog.pg_class ci
                    on ix.indexrelid = ci."oid"
                left join pg_catalog.pg_stat_user_tables s
                    on t.table_oid = s.relid
                left join pg_catalog.pg_stats st
                    on a.attrelid = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                    and a.attname = st.attname
                where
                    t.table_oid = any(updated_partitions_arr) -- clustering only paritions with new rows
                    and a.attname = 'scraped_at' -- clustering by scraped_at
                    and cardinality(ix.indkey) = 1
            loop

                if partitions_maintenance_rec.do_cluster_flg_partition is true then

                    -- CLUSTER + CREATE STATISTICS
                    maintenance_qry := 'cluster '
                        || partitions_maintenance_rec.partition_schema || '.' || partitions_maintenance_rec.partition_name
                        || ' using ' || partitions_maintenance_rec.cluster_index_name || ';' || repeat(E'\n', 2)

                        || 'create statistics if not exists ' || partitions_maintenance_rec.partition_schema || '.'
                        || substring('stats_' || partitions_maintenance_rec.partition_name, 1, 63) || E'\n'
                        || 'on ' || array_to_string(stats_columns_list, ', ') || E'\n'
                        || 'from ' || partitions_maintenance_rec.partition_schema || '.' || partitions_maintenance_rec.partition_name || ';';

                    execute maintenance_qry;

                    analyze_root_flg := true;

                end if;

            end loop;

            if analyze_root_flg is true then

                analyze etl_dev.gigs_revisions_pre_metrics;

            end if;

        else

            row_cnt := 0;

        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            cluster_threshold,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;