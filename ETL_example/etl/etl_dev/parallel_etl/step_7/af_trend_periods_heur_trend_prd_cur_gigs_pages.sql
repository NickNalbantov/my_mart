create or replace function etl_dev.af_trend_periods_heur_trend_prd_cur_gigs_pages
(
    start_dttm timestamptz,
    period_num smallint,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '7.2.1.2'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.' || period_num || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            start_dttm,
            period_num,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Gigs pages from current assessed period (heuristic trends)

        task_start_dttm := clock_timestamp();

        execute format($exec$
            drop table if exists etl_wrk_dev.heur_trend_prd_cur_gigs_pages_%1$s cascade;

            create unlogged table if not exists etl_wrk_dev.heur_trend_prd_cur_gigs_pages_%1$s
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select distinct
                    p.gig_id,
                    p.fiverr_seller_id,
                    min(p.seller_completed_orders_count) over (partition by p.fiverr_seller_id) as min_seller_completed_orders_count,
                    max(p.seller_completed_orders_count) over (partition by p.fiverr_seller_id) as max_seller_completed_orders_count,
                    first_value(p.prices) over (partition by p.gig_id order by p.scraped_at desc, p.id desc) as prices
                from etl_dev.gigs_pages_pre_metrics p
                where
                    p.scraped_at < %2$L::timestamptz
                    and exists
                        (
                            select
                            from etl_wrk_dev.trends_gigs_hist_active_%1$s h
                            where
                                p.gig_id = h.gig_id
                        )
                    and not exists
                        (
                            select
                            from etl_wrk_dev.reg_trend_prd_cur_metrics_%1$s r
                            where
                                p.gig_id = r.gig_id
                        );

        $exec$,
        period_num,
        start_dttm
        );

        get diagnostics
            row_cnt = row_count;

        execute format($exec$
            create unique index if not exists uidx_heur_trend_prd_cur_gigs_pages_%1$s
                on etl_wrk_dev.heur_trend_prd_cur_gigs_pages_%1$s (gig_id) with (fillfactor = 100);

            analyze etl_wrk_dev.heur_trend_prd_cur_gigs_pages_%1$s;
        $exec$,
        period_num
        );

        select
            high_level_step || '.' || period_num || '.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_trend_prd_cur_gigs_pages_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.' || period_num || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            start_dttm,
            period_num,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;