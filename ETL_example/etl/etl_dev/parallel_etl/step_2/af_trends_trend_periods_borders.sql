create or replace function etl_dev.af_trends_trend_periods_borders
(
    start_dttm timestamptz default (current_date::timestamp at time zone 'UTC' - '3 month'::interval)::timestamptz,
    end_dttm timestamptz default (current_date::timestamp at time zone 'UTC' + '1 day'::interval)::timestamptz,
    intervals smallint default 3,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '2.4'
)
returns table
(
    period_num smallint,
    period_start timestamptz,
    period_end timestamptz
)
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
#variable_conflict use_column

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            start_dttm,
            end_dttm,
            intervals,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - calculating data for each period

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.trends_periods_borders cascade;

        create unlogged table if not exists etl_wrk_dev.trends_periods_borders
            with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
            as

            select
                t.rn::smallint as period_num,
                start_dttm as period_start,
                coalesce(lead(t.pit) over (order by t.pit), end_dttm) as period_end
            from generate_series(start_dttm, end_dttm, (end_dttm - start_dttm) / intervals) with ordinality t (pit, rn)
            order by t.pit
            limit (intervals);

        return query
            select
                period_num,
                period_start,
                period_end
            from etl_wrk_dev.trends_periods_borders;

        select
            high_level_step || '.1' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'calculations' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (borders of intervals for trend evaluation have been calculated)';

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            start_dttm,
            end_dttm,
            intervals,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

    end;
$func$;