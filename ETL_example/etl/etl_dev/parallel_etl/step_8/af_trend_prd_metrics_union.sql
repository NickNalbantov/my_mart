create or replace function etl_dev.af_trend_prd_metrics_union
(
    period_num smallint,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '7.2'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.' || period_num || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            period_num,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Union heuristic and non-heuristic gigs

        task_start_dttm := clock_timestamp();

        execute format($exec$
            drop table if exists etl_wrk_dev.trend_prd_metrics_union_%1$s cascade;

            create unlogged table if not exists etl_wrk_dev.trend_prd_metrics_union_%1$s
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    gig_id,
                    fiverr_seller_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    greatest(gig_volume, 0)::int as gig_volume,
                    gig_is_active,
                    (
                        case
                            when -- no data from gig_reviews_prices
                                weighted_start_price_by_gig = 0::numeric
                                and weighted_end_price_by_gig in (0::numeric, 'infinity'::numeric)
                            then
                                avg_price_by_gig
                            when -- invalid weighted_end_price_by_gig
                                weighted_start_price_by_gig > 0::numeric
                                and weighted_end_price_by_gig = 0::numeric
                            then
                                (
                                    case
                                        when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig <= avg_price_by_gig
                                            weighted_start_price_by_gig <= avg_price_by_gig
                                        then
                                            avg_price_by_gig
                                        when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig > avg_price_by_gig
                                            weighted_start_price_by_gig > avg_price_by_gig
                                        then
                                            weighted_start_price_by_gig
                                    end
                                )
                            when -- valid data from gig_reviews_prices
                                weighted_start_price_by_gig >= 0::numeric
                                and weighted_end_price_by_gig > 0::numeric
                            then
                                (
                                    case
                                        when -- valid data from gig_reviews_prices but weighted_end_price_by_gig >= avg_price_by_gig
                                            weighted_end_price_by_gig >= avg_price_by_gig
                                        then
                                            avg_price_by_gig
                                        when -- valid data from gig_reviews_prices but weighted_end_price_by_gig < avg_price_by_gig
                                            weighted_end_price_by_gig < avg_price_by_gig
                                        then
                                            (weighted_start_price_by_gig + weighted_end_price_by_gig) / 2::numeric
                                    end
                                )
                            else
                                avg_price_by_gig
                        end
                    ) as avg_price_by_gig
                from
                (
                    select *
                    from etl_wrk_dev.reg_trend_prd_metrics_%1$s

                    union all

                    select *
                    from etl_wrk_dev.heur_trend_prd_metrics_%1$s
                ) u;

        $exec$,
        period_num
        );

        get diagnostics
            row_cnt = row_count;

        execute format($exec$
            create index if not exists idx_trend_prd_metrics_union_nest_subcat_%1$s
                on etl_wrk_dev.trend_prd_metrics_union_%1$s (nested_sub_category_id)
                with (fillfactor = 100);

            analyze etl_wrk_dev.trend_prd_metrics_union_%1$s;
        $exec$,
        period_num
        );

        if debug_mode is false then
            execute format ($exec$
                drop table if exists etl_wrk_dev.reg_trend_prd_cur_metrics_%1$s cascade;
                drop table if exists etl_wrk_dev.reg_trend_prd_metrics_%1$s cascade;
                drop table if exists etl_wrk_dev.heur_trend_prd_metrics_%1$s cascade;
                drop table if exists etl_wrk_dev.trends_gigs_hist_active_%1$s cascade;
                drop table if exists etl_wrk_dev.trends_sellers_hist_active_%1$s cascade;
            $exec$, period_num);
        end if;

        select
            high_level_step || '.' || period_num || '.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'trend_prd_metrics_union_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.' || period_num || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            period_num,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;