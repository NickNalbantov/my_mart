create or replace function etl_dev.af_metrics_union
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '7.1'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Union heuristic and non-heuristic gigs

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_union cascade;

        create unlogged table if not exists etl_wrk_dev.metrics_union
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                created_at,
                fiverr_created_at,
                scraped_at,
                gig_id,
                fiverr_gig_id,
                fiverr_seller_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                gig_ratings_count,
                seller_completed_orders_count,
                gig_volume,
                total_historical_volume,
                false as heuristic,
                gig_is_pro,
                gig_is_active,
                gig_rating,
                -- Fixing anomalies with prices
                least(min_price_by_gig, avg_price_by_gig, max_price_by_gig) as min_price_by_gig,
                (
                    case
                        when
                            avg_price_by_gig not between min_price_by_gig and max_price_by_gig
                        then
                            (min_price_by_gig + max_price_by_gig) / 2::numeric
                        else
                            avg_price_by_gig
                    end
                ) as avg_price_by_gig,
                greatest(min_price_by_gig, avg_price_by_gig, max_price_by_gig) as max_price_by_gig,
                gig_title,
                gig_cached_slug,
                category_name,
                sub_category_name,
                nested_sub_category_name,
                gig_preview_url
            from
            (
                select
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    gig_ratings_count,
                    seller_completed_orders_count,
                    greatest(least(gig_volume_pre, total_historical_volume), 0)::int as gig_volume,
                    total_historical_volume,
                    false as heuristic,
                    gig_is_pro,
                    gig_is_active,
                    gig_rating,
                    (
                        case
                            when -- no data from gig_reviews_prices
                                weighted_start_price_by_gig = 0::numeric
                                and weighted_end_price_by_gig in (0::numeric, 'infinity'::numeric)
                            then
                                min_price_by_gig
                            when -- invalid weighted_end_price_by_gig
                                weighted_start_price_by_gig > 0::numeric
                                and weighted_end_price_by_gig = 0::numeric
                            then
                                weighted_start_price_by_gig
                            when -- valid data from gig_reviews_prices
                                weighted_start_price_by_gig >= 0::numeric
                                and weighted_end_price_by_gig > 0::numeric
                            then
                                (
                                    case
                                        when -- valid data from gig_reviews_prices but weighted_end_price_by_gig >= min_price_by_gig
                                            weighted_end_price_by_gig >= min_price_by_gig
                                        then
                                            min_price_by_gig
                                        when -- valid data from gig_reviews_prices and weighted_end_price_by_gig < min_price_by_gig
                                            weighted_end_price_by_gig < min_price_by_gig
                                        then
                                            weighted_end_price_by_gig
                                    end
                                )
                            else
                                min_price_by_gig
                        end
                    ) as min_price_by_gig,
                    (
                        case
                            when -- no data from gig_reviews_prices
                                weighted_start_price_by_gig = 0::numeric
                                and weighted_end_price_by_gig in (0::numeric, 'infinity'::numeric)
                            then
                                avg_price_by_gig
                            when -- invalid weighted_end_price_by_gig
                                weighted_start_price_by_gig > 0::numeric
                                and weighted_end_price_by_gig = 0::numeric
                            then
                                (
                                    case
                                        when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig <= avg_price_by_gig
                                            weighted_start_price_by_gig <= avg_price_by_gig
                                        then
                                            avg_price_by_gig
                                        when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig > avg_price_by_gig
                                            weighted_start_price_by_gig > avg_price_by_gig
                                        then
                                            weighted_start_price_by_gig
                                    end
                                )
                            when -- valid data from gig_reviews_prices
                                weighted_start_price_by_gig >= 0::numeric
                                and weighted_end_price_by_gig > 0::numeric
                            then
                                (
                                    case
                                        when -- valid data from gig_reviews_prices but weighted_end_price_by_gig >= avg_price_by_gig
                                            weighted_end_price_by_gig >= avg_price_by_gig
                                        then
                                            avg_price_by_gig
                                        when -- valid data from gig_reviews_prices but weighted_end_price_by_gig < avg_price_by_gig
                                            weighted_end_price_by_gig < avg_price_by_gig
                                        then
                                            (weighted_start_price_by_gig + weighted_end_price_by_gig) / 2::numeric
                                    end
                                )
                            else
                                avg_price_by_gig
                        end
                    ) as avg_price_by_gig,
                    (
                        case
                            when -- no data from gig_reviews_prices
                                weighted_start_price_by_gig = 0::numeric
                                and weighted_end_price_by_gig in (0::numeric, 'infinity'::numeric)
                            then
                                max_price_by_gig

                            when -- invalid weighted_end_price_by_gig
                                weighted_start_price_by_gig > 0::numeric
                                and weighted_end_price_by_gig = 0::numeric
                            then
                                (
                                    case
                                        when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig <= max_price_by_gig
                                            weighted_start_price_by_gig <= max_price_by_gig
                                        then
                                            max_price_by_gig
                                        when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig > max_price_by_gig
                                            weighted_start_price_by_gig > max_price_by_gig
                                        then
                                            (
                                                case
                                                    when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig > max_price_by_gig and weighted_end_price_by_gig != 'infinity'
                                                        weighted_end_price_by_gig != 'infinity'::numeric
                                                    then
                                                        (weighted_start_price_by_gig + weighted_end_price_by_gig) / 2::numeric
                                                    when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig > max_price_by_gig and weighted_end_price_by_gig != 'infinity'
                                                        weighted_end_price_by_gig = 'infinity'::numeric
                                                    then
                                                        weighted_start_price_by_gig
                                                end
                                            )
                                    end
                                )
                            when -- valid data from gig_reviews_prices
                                weighted_start_price_by_gig >= 0::numeric
                                and weighted_end_price_by_gig > 0::numeric
                            then
                                (
                                    case
                                        when -- valid data from gig_reviews_prices but weighted_end_price_by_gig = 'infinity'
                                            weighted_end_price_by_gig = 'infinity'::numeric
                                        then
                                            max_price_by_gig
                                        when -- valid data from gig_reviews_prices and weighted_end_price_by_gig != 'infinity'
                                            weighted_end_price_by_gig != 'infinity'::numeric
                                        then
                                            weighted_end_price_by_gig
                                    end
                                )
                            else
                                max_price_by_gig
                        end
                    ) as max_price_by_gig,
                    gig_title,
                    gig_cached_slug,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name,
                    gig_preview_url
                from
                (
                    select *
                    from etl_wrk_dev.reg_metrics

                    union all

                    select *
                    from etl_wrk_dev.heur_metrics
                ) u
            ) p;

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_metrics_union_fiverr_seller_id
            on etl_wrk_dev.metrics_union (fiverr_seller_id) with (fillfactor = 100);
        create index if not exists idx_metrics_union_cat
            on etl_wrk_dev.metrics_union (category_id) with (fillfactor = 100);
        create index if not exists idx_metrics_union_subcat
            on etl_wrk_dev.metrics_union (sub_category_id) with (fillfactor = 100);
        create index if not exists idx_metrics_union_nest_subcat
            on etl_wrk_dev.metrics_union (nested_sub_category_id) with (fillfactor = 100);
        create index if not exists idx_metrics_union_all_cats
            on etl_wrk_dev.metrics_union (category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk_dev.metrics_union;

        if debug_mode is false then
            drop table if exists etl_wrk.gigs_revisions_parsed cascade;
            drop table if exists etl_wrk.gigs_pages_parsed cascade;
            drop table if exists etl_wrk.sellers_revisions_parsed cascade;
            drop table if exists etl_wrk.trends_periods_borders cascade;
            drop table if exists etl_wrk.reg_metrics_cur cascade;
            drop table if exists etl_wrk.reg_metrics cascade;
            drop table if exists etl_wrk.heur_metrics cascade;
            drop table if exists etl_wrk_dev.metrics_gigs_hist_active cascade;
            drop table if exists etl_wrk_dev.metrics_sellers_hist_active cascade;
        end if;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_union' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;