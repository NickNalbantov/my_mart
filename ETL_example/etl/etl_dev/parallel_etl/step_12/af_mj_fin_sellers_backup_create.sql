create or replace function etl_dev.af_mj_fin_sellers_backup_create
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '12.3.2'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Materializing sellers metrics

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.sellers_backup cascade;

        create table if not exists etl_wrk_dev.sellers_backup
        (like etl_dev.sellers including all excluding indexes)
        with
        (
            autovacuum_enabled = false,
            toast.autovacuum_enabled = false
        );

        insert into etl_wrk_dev.sellers_backup
        (
            "load_id",
            processed_dttm,
            created_at,
            fiverr_created_at,
            scraped_at,
            min_total_historical_revenue,
            avg_total_historical_revenue,
            max_total_historical_revenue,
            seller_id,
            fiverr_seller_id,
            best_selling_gig_id,
            best_selling_fiverr_gig_id,
            global_rank,
            gig_count_by_seller,
            ratings_count,
            completed_orders_count,
            active_gigs_ratings_count_by_seller,
            sales_volume,
            total_historical_sales_volume,
            best_selling_gig_total_historical_volume,
            weighted_avg_gig_price,
            min_active_revenue,
            min_inactive_revenue,
            min_revenue,
            avg_active_revenue,
            avg_inactive_revenue,
            avg_revenue,
            max_active_revenue,
            max_inactive_revenue,
            max_revenue,
            best_selling_gig_revenue,
            is_pro,
            is_active,
            is_trend_valid,
            sales_volume_growth_percent,
            rating,
            revenue_growth_percent,
            revenue_trend,
            best_selling_gig_revenue_share,
            best_selling_gig_title,
            best_selling_gig_cached_slug,
            "level",
            seller_name,
            agency_slug,
            agency_status,
            country,
            country_code,
            languages,
            profile_image,
            best_selling_gig_image_preview
        )

            select
                load_id as "load_id",
                transaction_timestamp() as processed_dttm,
                created_at,
                fiverr_created_at,
                scraped_at,
                round(min_total_historical_revenue)::bigint as min_total_historical_revenue,
                round(avg_total_historical_revenue)::bigint as avg_total_historical_revenue,
                round(max_total_historical_revenue)::bigint as max_total_historical_revenue,
                seller_id,
                fiverr_seller_id,
                best_selling_gig_id,
                best_selling_fiverr_gig_id,
                (dense_rank() over (order by
                    is_active desc nulls last, -- true first
                    coeff_revenue desc nulls last, -- bigger revenue first
                    (
                        case "level"
                            when 'bad_actor' then -3
                            when 'low_quality' then -2
                            when 'no_level' then -1
                            when 'new_seller' then 0
                            when 'level_one' then 1
                            when 'level_two' then 2
                            when 'level_trs' then 3
                        else -4 end
                    ) desc nulls last, -- higher level first
                    coeff_trend desc nulls last, -- bigger trend growth first
                    coeff_rating desc nulls last, -- bigger rating first
                    coalesce(fiverr_created_at, scraped_at) asc nulls last, -- oldest first
                    seller_id asc nulls last -- oldest first
                ))::int as global_rank,
                gig_count_by_seller,
                ratings_count,
                completed_orders_count,
                active_gigs_ratings_count,
                sales_volume,
                total_historical_sales_volume,
                best_selling_gig_total_historical_volume,
                round(weighted_avg_gig_price)::int as weighted_avg_gig_price,
                round(min_active_revenue)::int as min_active_revenue,
                round(min_inactive_revenue)::int as min_inactive_revenue,
                round(min_total_revenue)::int as min_revenue,
                round(avg_active_revenue)::int as avg_active_revenue,
                round(avg_inactive_revenue)::int as avg_inactive_revenue,
                round(avg_total_revenue)::int as avg_revenue,
                round(max_active_revenue)::int as max_active_revenue,
                round(max_inactive_revenue)::int as max_inactive_revenue,
                round(max_total_revenue)::int as max_revenue,
                round(best_selling_gig_revenue)::int as best_selling_gig_revenue,
                is_pro,
                is_active,
                is_trend_valid,
                round(sales_volume_growth_percent, 2) as sales_volume_growth_percent,
                round(rating, 2) as rating,
                round(revenue_growth_percent, 2) as revenue_growth_percent,
                round(revenue_trend, 2) as revenue_trend,
                round(coalesce(
                        best_selling_gig_revenue::numeric / nullif(min_total_historical_revenue, 0::numeric),
                        0::numeric
                    ), 2) as best_selling_gig_revenue_share,
                best_selling_gig_title,
                best_selling_gig_cached_slug,
                "level",
                seller_name,
                agency_slug,
                agency_status,
                country,
                country_code,
                languages,
                profile_image,
                best_selling_gig_image_preview
            from etl_wrk_dev.sellers_attributes;

        get diagnostics
            row_cnt := row_count;

        alter table if exists etl_wrk_dev.sellers_backup
            add primary key (seller_id) include (is_active) with (fillfactor = 100);
        create index if not exists idx_fiverr_seller_id_sellers_backup
            on etl_wrk_dev.sellers_backup (fiverr_seller_id) with (fillfactor = 100);
        create index if not exists idx_sellers_best_selling_gig_id_backup
            on etl_wrk_dev.sellers_backup (best_selling_gig_id) with (fillfactor = 100);
        create unique index if not exists uidx_sellers_ranks_backup
            on etl_wrk_dev.sellers_backup (global_rank desc) with (fillfactor = 100);
        create index if not exists idx_trg_seller_name_sellers_backup
            on etl_wrk_dev.sellers_backup using gin (seller_name gin_trgm_ops);

        cluster etl_wrk_dev.sellers_backup using sellers_backup_pkey;

        alter table if exists etl_wrk_dev.sellers_backup
            replica identity using index sellers_backup_pkey;

        create statistics if not exists etl_wrk_dev.sellers_all_ids_backup
        on seller_id, fiverr_seller_id, best_selling_gig_id, best_selling_fiverr_gig_id
        from etl_wrk_dev.sellers_backup;

        analyze etl_wrk_dev.sellers_backup;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.sellers_attributes cascade;
        end if;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'sellers_backup' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;