create or replace function etl_dev.af_mj_fin_nested_subcategories_backup_create
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '12.6'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Final metrics for nested subcategories

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.nested_subcategories_backup cascade;

        create table if not exists etl_wrk_dev.nested_subcategories_backup
        (like etl_dev.nested_subcategories including all excluding indexes)
        with
        (
            autovacuum_enabled = false,
            toast.autovacuum_enabled = false
        );

        insert into etl_wrk_dev.nested_subcategories_backup
        (
            "load_id",
            processed_dttm,
            min_total_historical_revenue,
            avg_total_historical_revenue,
            max_total_historical_revenue,
            nested_sub_category_id,
            global_rank,
            all_gigs_count,
            regular_gigs_count,
            pro_gigs_count,
            seller_count,
            sales_volume,
            total_historical_sales_volume,
            weighted_avg_gig_price,
            min_active_revenue,
            min_inactive_revenue,
            min_revenue,
            avg_active_revenue,
            avg_inactive_revenue,
            avg_revenue,
            max_active_revenue,
            max_inactive_revenue,
            max_revenue,
            is_trend_valid,
            sales_volume_growth_percent,
            competition_score,
            market_share,
            revenue_growth_percent,
            revenue_trend,
            nested_sub_category_name
        )

            select
                load_id as "load_id",
                transaction_timestamp() as processed_dttm,
                round(min_total_historical_revenue)::bigint as min_total_historical_revenue,
                round(avg_total_historical_revenue)::bigint as avg_total_historical_revenue,
                round(max_total_historical_revenue)::bigint as max_total_historical_revenue,
                nested_sub_category_id,
                (dense_rank() over (order by
                    min_revenue desc nulls last,
                    revenue_trend desc nulls last,
                    nested_sub_category_id desc nulls last
                ))::int as global_rank,
                all_gigs_count,
                regular_gigs_count,
                pro_gigs_count,
                seller_count,
                sales_volume,
                total_historical_sales_volume,
                round(weighted_avg_gig_price)::int as weighted_avg_gig_price,
                round(min_active_revenue)::int as min_active_revenue,
                round(min_inactive_revenue)::int as min_inactive_revenue,
                round(min_revenue)::int as min_revenue,
                round(avg_active_revenue)::int as avg_active_revenue,
                round(avg_inactive_revenue)::int as avg_inactive_revenue,
                round(avg_revenue)::int as avg_revenue,
                round(max_active_revenue)::int as max_active_revenue,
                round(max_inactive_revenue)::int as max_inactive_revenue,
                round(max_revenue)::int as max_revenue,
                is_trend_valid,
                round(sales_volume_growth_percent, 2) as sales_volume_growth_percent,
                round(competition_score, 2) as competition_score,
                round(market_share, 2) as market_share,
                round(revenue_growth_percent, 2) as revenue_growth_percent,
                round(revenue_trend, 2) as revenue_trend,
                nested_sub_category_name
            from
            (
                select distinct
                    c.nested_sub_category_id,
                    c.all_gigs_count_by_nested_subcategory as all_gigs_count,
                    c.regular_gigs_count_by_nested_subcategory as regular_gigs_count,
                    c.pro_gigs_count_by_nested_subcategory as pro_gigs_count,
                    s.cnt as seller_count,
                    c.sales_volume_by_nested_subcategory as sales_volume,
                    c.total_historical_sales_volume_by_nested_subcategory as total_historical_sales_volume,
                    c.is_trend_valid_for_nested_subcategory as is_trend_valid,
                    c.sales_volume_growth_percent_by_nested_subcategory as sales_volume_growth_percent,
                    c.competition_score_for_nested_subcategory as competition_score,
                    c.market_share_for_nested_subcategory as market_share,
                    c.weighted_avg_gig_price_by_nested_subcategory as weighted_avg_gig_price,
                    c.min_active_revenue_by_nested_subcategory as min_active_revenue,
                    c.min_inactive_revenue_by_nested_subcategory as min_inactive_revenue,
                    c.min_total_revenue_by_nested_subcategory as min_revenue,
                    c.avg_active_revenue_by_nested_subcategory as avg_active_revenue,
                    c.avg_inactive_revenue_by_nested_subcategory as avg_inactive_revenue,
                    c.avg_total_revenue_by_nested_subcategory as avg_revenue,
                    c.max_active_revenue_by_nested_subcategory as max_active_revenue,
                    c.max_inactive_revenue_by_nested_subcategory as max_inactive_revenue,
                    c.max_total_revenue_by_nested_subcategory as max_revenue,
                    c.min_total_historical_revenue_by_nested_subcategory as min_total_historical_revenue,
                    c.avg_total_historical_revenue_by_nested_subcategory as avg_total_historical_revenue,
                    c.max_total_historical_revenue_by_nested_subcategory as max_total_historical_revenue,
                    c.revenue_growth_percent_by_nested_subcategory as revenue_growth_percent,
                    c.revenue_trend_by_nested_subcategory as revenue_trend,
                    c.nested_sub_category_name
                from etl_wrk_dev.categories_prefin c
                join etl_wrk_dev.seller_counts s
                    on c.nested_sub_category_id = s.nested_sub_category_id
                    and s.group_type = 'nested_sub_category_id'
                where
                    c.nested_sub_category_id is not null
            ) a;

        get diagnostics
            row_cnt := row_count;

        alter table if exists etl_wrk_dev.nested_subcategories_backup
            add primary key (nested_sub_category_id) with (fillfactor = 100);
        create unique index if not exists uidx_nested_subcategories_ranks_backup
            on etl_wrk_dev.nested_subcategories_backup (global_rank desc) with (fillfactor = 100);
        create index if not exists idx_trg_nest_subcat_name_nest_subcat_backup
            on etl_wrk_dev.nested_subcategories_backup using gin (nested_sub_category_name gin_trgm_ops);

        cluster etl_wrk_dev.nested_subcategories_backup using nested_subcategories_backup_pkey;

        alter table if exists etl_wrk_dev.nested_subcategories_backup
            replica identity using index nested_subcategories_backup_pkey;

        analyze etl_wrk_dev.nested_subcategories_backup;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'nested_subcategories_backup' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;