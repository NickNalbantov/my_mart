create or replace function etl_dev.af_mj_fin_sellers_attributes
(
    end_dttm timestamptz default (current_date::timestamp at time zone 'UTC' + '1 day'::interval)::timestamptz,
    rank_scale_factor numeric default 0.0001, -- scaling coefficient for relevance_sorting function
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '12.3.1'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            end_dttm,
            rank_scale_factor,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Gathering sellers' attributes

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.sellers_attributes cascade;

        create unlogged table if not exists etl_wrk_dev.sellers_attributes
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                s.seller_created_at as created_at,
                s.seller_fiverr_created_at as fiverr_created_at,
                s.seller_scraped_at as scraped_at,
                s.seller_id,
                s.fiverr_seller_id,
                first_value(s.gig_id) over w_s as best_selling_gig_id,
                first_value(s.fiverr_gig_id) over w_s as best_selling_fiverr_gig_id,
                s.gig_count_by_seller,
                s.seller_ratings_count as ratings_count,
                s.seller_completed_orders_count as completed_orders_count,
                s.active_gigs_ratings_count_by_seller as active_gigs_ratings_count,
                s.volume_by_seller as sales_volume,
                s.total_historical_volume_by_seller as total_historical_sales_volume,
                first_value(s.total_historical_volume_by_gig) over w_s as best_selling_gig_total_historical_volume,
                s.seller_is_pro as is_pro,
                s.seller_is_active as is_active,
                s.is_trend_valid_for_seller as is_trend_valid,
                s.sales_volume_growth_percent_by_seller as sales_volume_growth_percent,
                s.seller_rating as rating,
                s.weighted_avg_price_by_seller as weighted_avg_gig_price,
                s.min_active_revenue_by_seller as min_active_revenue,
                s.min_inactive_revenue_by_seller as min_inactive_revenue,
                s.min_total_revenue_by_seller as min_total_revenue,
                s.avg_active_revenue_by_seller as avg_active_revenue,
                s.avg_inactive_revenue_by_seller as avg_inactive_revenue,
                s.avg_total_revenue_by_seller as avg_total_revenue,
                s.max_active_revenue_by_seller as max_active_revenue,
                s.max_inactive_revenue_by_seller as max_inactive_revenue,
                s.max_total_revenue_by_seller as max_total_revenue,
                s.min_total_historical_revenue_by_seller as min_total_historical_revenue,
                s.avg_total_historical_revenue_by_seller as avg_total_historical_revenue,
                s.max_total_historical_revenue_by_seller as max_total_historical_revenue,
                coalesce
                    (
                        first_value(s.min_total_historical_revenue_by_gig) over w_s,
                        0::numeric
                    ) as best_selling_gig_revenue,
                s.revenue_growth_percent_by_seller as revenue_growth_percent,
                s.trend_by_seller as revenue_trend,
                etl_dev.relevance_sorting(
                        s.min_active_revenue_by_seller,
                        coalesce(s.seller_fiverr_created_at, s.seller_scraped_at),
                        end_dttm,
                        rank_scale_factor
                    ) as coeff_revenue,
                etl_dev.relevance_sorting(
                        s.trend_by_seller,
                        coalesce(s.seller_fiverr_created_at, s.seller_scraped_at),
                        end_dttm,
                        rank_scale_factor
                    ) as coeff_trend,
                etl_dev.relevance_sorting(
                        100 * coalesce((s.seller_rating * s.active_gigs_ratings_count_by_seller + r.avg_seller_rating * r.stddev_seller_rating)::numeric
                            / nullif((s.active_gigs_ratings_count_by_seller + r.stddev_seller_rating), 0)::numeric, 0::numeric),
                        coalesce(s.seller_fiverr_created_at, s.seller_scraped_at),
                        end_dttm,
                        rank_scale_factor
                    ) as coeff_rating,
                s.seller_level as "level",
                s.seller_name,
                s.agency_slug,
                s.agency_status,
                s.seller_country as country,
                s.seller_country_code as country_code,
                s.seller_languages as languages,
                s.seller_profile_image as profile_image,
                first_value(s.gig_title) over w_s as best_selling_gig_title,
                first_value(s.gig_cached_slug) over w_s as best_selling_gig_cached_slug,
                first_value(s.gig_preview_url) over w_s as best_selling_gig_image_preview
            from etl_wrk_dev.metrics_backup s
            cross join etl_wrk_dev.metrics_ratings_stats r
            window
                w_s as
                    (
                        partition by s.seller_id
                        order by
                            s.gig_is_active desc,
                            s.min_total_historical_revenue_by_gig desc,
                            s.gig_scraped_at desc,
                            s.gig_id desc
                    );

        get diagnostics
            row_cnt := row_count;

        analyze etl_wrk_dev.sellers_attributes;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'sellers_attributes' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            end_dttm,
            rank_scale_factor,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;