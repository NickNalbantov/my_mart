create or replace function etl_dev.af_mj_fin_nested_subcategory_x_seller_levels_backup_create
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '12.9'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Materializing subcategory_x_seller_levels_backup

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.nested_subcategory_x_seller_levels_backup cascade;

        create table if not exists etl_wrk_dev.nested_subcategory_x_seller_levels_backup
        (like etl_dev.nested_subcategory_x_seller_levels including all excluding indexes)
        with
        (
            autovacuum_enabled = false,
            toast.autovacuum_enabled = false
        );

        insert into etl_wrk_dev.nested_subcategory_x_seller_levels_backup
        (
            "load_id",
            processed_dttm,
            id,
            nested_sub_category_id,
            seller_count,
            seller_level,
            nested_sub_category_name
        )

            select
                m.load_id as "load_id",
                transaction_timestamp() as processed_dttm,
                (row_number() over (
                    order by
                        m.nested_sub_category_id,
                        (
                            case c.seller_level
                                when 'bad_actor' then -3
                                when 'low_quality' then -2
                                when 'no_level' then -1
                                when 'new_seller' then 0
                                when 'level_one' then 1
                                when 'level_two' then 2
                                when 'level_trs' then 3
                            else -4 end
                        ) desc
                    ))::int as id,
                m.nested_sub_category_id,
                c.cnt as seller_count,
                c.seller_level,
                m.nested_sub_category_name
            from etl_wrk_dev.seller_counts c
            join etl_wrk_dev.nested_subcategories_backup m
                on c.nested_sub_category_id = m.nested_sub_category_id
            where
                c.group_type = 'nested_sub_category_id_x_seller_level';

        get diagnostics
            row_cnt := row_count;

        alter table if exists etl_wrk_dev.nested_subcategory_x_seller_levels_backup
            add primary key (id) with (fillfactor = 100);
        create unique index if not exists uidx_nest_subcat_sel_lvl
            on etl_wrk_dev.nested_subcategory_x_seller_levels_backup (nested_sub_category_id, seller_level) with (fillfactor = 100);
        create index if not exists idx_trg_nest_subcat_name_nest_subcat_sel_lvl
            on etl_wrk_dev.nested_subcategory_x_seller_levels_backup using gin (nested_sub_category_name gin_trgm_ops);

        cluster etl_wrk_dev.nested_subcategory_x_seller_levels_backup using nested_subcategory_x_seller_levels_backup_pkey;

        alter table if exists etl_wrk_dev.nested_subcategory_x_seller_levels_backup
            replica identity using index nested_subcategory_x_seller_levels_backup_pkey;

        analyze etl_wrk_dev.nested_subcategory_x_seller_levels_backup;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'nested_subcategory_x_seller_levels_backup' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;