create or replace function etl_dev.af_mj_fin_all_categories_backup_create
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '12.6'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Final combined metrics for all category types

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.all_categories_backup cascade;

        create table if not exists etl_wrk_dev.all_categories_backup
        (like etl_dev.all_categories including all excluding indexes)
        with
        (
            autovacuum_enabled = false,
            toast.autovacuum_enabled = false
        );

        insert into etl_wrk_dev.all_categories_backup
        (
            "load_id",
            processed_dttm,
            min_total_historical_revenue_by_category,
            min_total_historical_revenue_by_subcategory,
            min_total_historical_revenue_by_nested_subcategory,
            avg_total_historical_revenue_by_category,
            avg_total_historical_revenue_by_subcategory,
            avg_total_historical_revenue_by_nested_subcategory,
            max_total_historical_revenue_by_category,
            max_total_historical_revenue_by_subcategory,
            max_total_historical_revenue_by_nested_subcategory,
            id,
            category_id,
            sub_category_id,
            nested_sub_category_id,
            global_rank_for_category,
            global_rank_for_subcategory,
            global_rank_for_nested_subcategory,
            all_gigs_count_by_category,
            all_gigs_count_by_subcategory,
            all_gigs_count_by_nested_subcategory,
            regular_gigs_count_by_category,
            regular_gigs_count_by_subcategory,
            regular_gigs_count_by_nested_subcategory,
            pro_gigs_count_by_category,
            pro_gigs_count_by_subcategory,
            pro_gigs_count_by_nested_subcategory,
            seller_count_by_category,
            seller_count_by_subcategory,
            seller_count_by_nested_subcategory,
            sales_volume_by_category,
            sales_volume_by_subcategory,
            sales_volume_by_nested_subcategory,
            total_historical_sales_volume_by_category,
            total_historical_sales_volume_by_subcategory,
            total_historical_sales_volume_by_nested_subcategory,
            weighted_avg_gig_price_by_category,
            weighted_avg_gig_price_by_subcategory,
            weighted_avg_gig_price_by_nested_subcategory,
            min_active_revenue_by_category,
            min_inactive_revenue_by_category,
            min_revenue_by_category,
            min_active_revenue_by_subcategory,
            min_inactive_revenue_by_subcategory,
            min_revenue_by_subcategory,
            min_active_revenue_by_nested_subcategory,
            min_inactive_revenue_by_nested_subcategory,
            min_revenue_by_nested_subcategory,
            avg_active_revenue_by_category,
            avg_inactive_revenue_by_category,
            avg_revenue_by_category,
            avg_active_revenue_by_subcategory,
            avg_inactive_revenue_by_subcategory,
            avg_revenue_by_subcategory,
            avg_active_revenue_by_nested_subcategory,
            avg_inactive_revenue_by_nested_subcategory,
            avg_revenue_by_nested_subcategory,
            max_active_revenue_by_category,
            max_inactive_revenue_by_category,
            max_revenue_by_category,
            max_active_revenue_by_subcategory,
            max_inactive_revenue_by_subcategory,
            max_revenue_by_subcategory,
            max_active_revenue_by_nested_subcategory,
            max_inactive_revenue_by_nested_subcategory,
            max_revenue_by_nested_subcategory,
            is_trend_valid_for_category,
            is_trend_valid_for_subcategory,
            is_trend_valid_for_nested_subcategory,
            sales_volume_growth_percent_by_category,
            sales_volume_growth_percent_by_subcategory,
            sales_volume_growth_percent_by_nested_subcategory,
            competition_score_for_category,
            competition_score_for_subcategory,
            competition_score_for_nested_subcategory,
            market_share_for_category,
            market_share_for_subcategory,
            market_share_for_nested_subcategory,
            revenue_growth_percent_by_category,
            revenue_growth_percent_by_subcategory,
            revenue_growth_percent_by_nested_subcategory,
            revenue_trend_by_category,
            revenue_trend_by_subcategory,
            revenue_trend_by_nested_subcategory,
            category_name,
            sub_category_name,
            nested_sub_category_name
        )

            select
                c.load_id as "load_id",
                transaction_timestamp() as processed_dttm,
                c.min_total_historical_revenue as min_total_historical_revenue_by_category,
                s.min_total_historical_revenue as min_total_historical_revenue_by_subcategory,
                n.min_total_historical_revenue as min_total_historical_revenue_by_nested_subcategory,
                c.avg_total_historical_revenue as avg_total_historical_revenue_by_category,
                s.avg_total_historical_revenue as avg_total_historical_revenue_by_subcategory,
                n.avg_total_historical_revenue as avg_total_historical_revenue,
                c.max_total_historical_revenue as max_total_historical_revenue_by_category,
                s.max_total_historical_revenue as max_total_historical_revenue_by_subcategory,
                n.max_total_historical_revenue as max_total_historical_revenue_by_nested_subcategory,
                (row_number() over (order by c.category_id, s.sub_category_id, n.nested_sub_category_id))::int as id,
                c.category_id,
                s.sub_category_id,
                n.nested_sub_category_id,
                c.global_rank as global_rank_for_category,
                s.global_rank as global_rank_for_subcategory,
                n.global_rank as global_rank_for_nested_subcategory,
                c.all_gigs_count as all_gigs_count_by_category,
                s.all_gigs_count as all_gigs_count_by_subcategory,
                n.all_gigs_count as all_gigs_count_by_nested_subcategory,
                c.regular_gigs_count as regular_gigs_count_by_category,
                s.regular_gigs_count as regular_gigs_count_by_subcategory,
                n.regular_gigs_count as regular_gigs_count_by_nested_subcategory,
                c.pro_gigs_count as pro_gigs_count_by_category,
                s.pro_gigs_count as pro_gigs_count_by_subcategory,
                n.pro_gigs_count as pro_gigs_count_by_nested_subcategory,
                c.seller_count as seller_count_by_category,
                s.seller_count as seller_count_by_subcategory,
                n.seller_count as seller_count_by_nested_subcategory,
                c.sales_volume as sales_volume_by_category,
                s.sales_volume as sales_volume_by_subcategory,
                n.sales_volume as sales_volume_by_nested_subcategory,
                c.total_historical_sales_volume as total_historical_sales_volume_by_category,
                s.total_historical_sales_volume as total_historical_sales_volume_by_subcategory,
                n.total_historical_sales_volume as total_historical_sales_volume_by_nested_subcategory,
                c.weighted_avg_gig_price as weighted_avg_gig_price_by_category,
                s.weighted_avg_gig_price as weighted_avg_gig_price_by_subcategory,
                n.weighted_avg_gig_price as weighted_avg_gig_price_by_nested_subcategory,
                c.min_active_revenue as min_active_revenue_by_category,
                c.min_inactive_revenue as min_inactive_revenue_by_category,
                c.min_revenue as min_revenue_by_category,
                s.min_active_revenue as min_active_revenue_by_subcategory,
                s.min_inactive_revenue as min_inactive_revenue_by_subcategory,
                s.min_revenue as min_revenue_by_subcategory,
                n.min_active_revenue as min_active_revenue_by_nested_subcategory,
                n.min_inactive_revenue as min_inactive_revenue_by_nested_subcategory,
                n.min_revenue as min_revenue_by_nested_subcategory,
                c.avg_active_revenue as avg_active_revenue_by_category,
                c.avg_inactive_revenue as avg_inactive_revenue_by_category,
                c.avg_revenue as avg_revenue_by_category,
                s.avg_active_revenue as avg_active_revenue_by_subcategory,
                s.avg_inactive_revenue as avg_inactive_revenue_by_subcategory,
                s.avg_revenue as avg_revenue_by_subcategory,
                n.avg_active_revenue as avg_active_revenue_by_nested_subcategory,
                n.avg_inactive_revenue as avg_inactive_revenue_by_nested_subcategory,
                n.avg_revenue as avg_revenue_by_nested_subcategory,
                c.max_active_revenue as max_active_revenue_by_category,
                c.max_inactive_revenue as max_inactive_revenue_by_category,
                c.max_revenue as max_revenue_by_category,
                s.max_active_revenue as max_active_revenue_by_subcategory,
                s.max_inactive_revenue as max_inactive_revenue_by_subcategory,
                s.max_revenue as max_revenue_by_subcategory,
                n.max_active_revenue as max_active_revenue_by_nested_subcategory,
                n.max_inactive_revenue as max_inactive_revenue_by_nested_subcategory,
                n.max_revenue as max_revenue_by_nested_subcategory,
                c.is_trend_valid as is_trend_valid_for_category,
                s.is_trend_valid as is_trend_valid_for_subcategory,
                n.is_trend_valid as is_trend_valid_for_nested_subcategory,
                c.sales_volume_growth_percent as sales_volume_growth_percent_by_category,
                s.sales_volume_growth_percent as sales_volume_growth_percent_by_subcategory,
                n.sales_volume_growth_percent as sales_volume_growth_percent_by_nested_subcategory,
                c.competition_score as competition_score_for_category,
                s.competition_score as competition_score_for_subcategory,
                n.competition_score as competition_score_for_nested_subcategory,
                c.market_share as market_share_for_category,
                s.market_share as market_share_for_subcategory,
                n.market_share as market_share_for_nested_subcategory,
                c.revenue_growth_percent as revenue_growth_percent_by_category,
                s.revenue_growth_percent as revenue_growth_percent_by_subcategory,
                n.revenue_growth_percent as revenue_growth_percent_by_nested_subcategory,
                c.revenue_trend as revenue_trend_by_category,
                s.revenue_trend as revenue_trend_by_subcategory,
                n.revenue_trend as revenue_trend_by_nested_subcategory,
                c.category_name,
                s.sub_category_name,
                n.nested_sub_category_name
            from etl_wrk_dev.categories_prefin p
            join etl_wrk_dev.categories_backup c
                on p.category_id = c.category_id
            join etl_wrk_dev.subcategories_backup s
                on p.sub_category_id = s.sub_category_id
            left join etl_wrk_dev.nested_subcategories_backup n
                on p.nested_sub_category_id = n.nested_sub_category_id;

        get diagnostics
            row_cnt := row_count;

        alter table if exists etl_wrk_dev.all_categories_backup
            add primary key (id) with (fillfactor = 100);
        create unique index if not exists uidx_all_id_all_cats_backup
            on etl_wrk_dev.all_categories_backup (category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);
        create index if not exists idx_cat_id_all_cats_backup
            on etl_wrk_dev.all_categories_backup (category_id) with (fillfactor = 100);
        create index if not exists idx_sub_cat_id_all_cats_backup
            on etl_wrk_dev.all_categories_backup (sub_category_id) with (fillfactor = 100);
        create index if not exists idx_nest_sub_cat_id_all_cats_backup
            on etl_wrk_dev.all_categories_backup (nested_sub_category_id) with (fillfactor = 100);
        create index if not exists idx_trg_cat_name_all_cats
            on etl_wrk_dev.all_categories_backup using gin (category_name gin_trgm_ops);
        create index if not exists idx_trg_subcat_name_all_cats
            on etl_wrk_dev.all_categories_backup using gin (sub_category_name gin_trgm_ops);
        create index if not exists idx_trg_nest_subcat_name_all_cats
            on etl_wrk_dev.all_categories_backup using gin (nested_sub_category_name gin_trgm_ops);

        cluster etl_wrk_dev.all_categories_backup using all_categories_backup_pkey;

        alter table if exists etl_wrk_dev.all_categories_backup
            replica identity using index all_categories_backup_pkey;

        analyze etl_wrk_dev.all_categories_backup;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'all_categories_backup' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;