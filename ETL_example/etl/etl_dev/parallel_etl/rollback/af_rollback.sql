create or replace function etl_dev.af_rollback
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '-1'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
#variable_conflict use_variable

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        killed_queries text[];
        cleanup_tbls_rec record;
        rollback_tbls_rec record;
        has_etl_finished boolean;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Killing parallel sessions

        task_start_dttm := clock_timestamp();

        select
            array_agg(query)::text[] as killed_queries
        into killed_queries
        from
        (
            select
                pg_terminate_backend(pid),
                query
            from pg_catalog.pg_stat_activity
            where
                usename = 'airflow'
                and
                (
                    (
                        dag_name = 'etl_regular'
                        and application_name like 'Regular ETL%'
                    )
                    or dag_name = 'manual_launch'
                )
                and pid != pg_backend_pid()
        ) a;

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'maintenance' as op_type
        into log_rec;

        if cardinality(killed_queries) > 0 then
            log_msg := E'Sessions with following queries has been killed:\n\n - ' || array_to_string(killed_queries, E'\n\n - ');
        else
            log_msg := 'No sessions has been killed';
        end if;

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 2 - Cleaning up temporary tables

        task_start_dttm := clock_timestamp();

        if debug_mode is false then

            log_msg := '';

            for cleanup_tbls_rec in
                select
                    'etl_wrk_dev' as relnamespace,
                    relname
                from pg_catalog.pg_class
                where
                    relkind = 'r'
                    and relnamespace = 'etl_wrk_dev'::regnamespace
                    and substring(relname, '(^agg_backup_|backup_\d+$|_backup$)') is null
            loop

                execute format
                (
                    $exec$
                        drop table if exists %1$I.%2$I cascade
                    $exec$,
                    cleanup_tbls_rec.relnamespace, cleanup_tbls_rec.relname
                );

                log_msg := log_msg
                    || 'Working table ' || quote_ident(cleanup_tbls_rec.relnamespace) || '.' || quote_ident(cleanup_tbls_rec.relname)
                    || E' has been dropped\n';

            end loop;

            if log_msg = '' then
                log_msg := 'No working tables in schema etl_wrk_dev to clean up';
            else
                log_msg := rtrim(log_msg, E'\n');
            end if;

        else

            log_msg := 'Working tables cleanup has been skipped because debug_mode set to True';

        end if;

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'ddl' as op_type
        into log_rec;

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 3 - Rolling back target tables
            -- We don't have to rollback successful upserts to pre_materialization targets

        task_start_dttm := clock_timestamp();

        select
            count(*) > 0
        into has_etl_finished
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l.src_name = 'af_mj_fin_etl_statistics'
            and l.op_type = 'start';

            -- Nothing to rollback

        if has_etl_finished is true then

            log_msg := 'ETL successfully finished, targets rollback is not needed';

            -- For regular ETL inserting data from last backups to targets

        elsif has_etl_finished is false and dag_name = 'etl_regular' then

            log_msg := '';

            for rollback_tbls_rec in

                with targets as
                (
                    select
                        l.tgt_name as target_name
                    from etl_log.etl_log l
                    where
                        l."load_id" = load_id
                        and l.op_type = any(array['insert', 'update', 'merge', 'delete']::text[])
                        and l.tgt_schema = 'etl_dev'
                        and l.tgt_name != all(array
                            [
                                'gig_reviews_agg',
                                'gig_reviews_prices',
                                'gig_status_history',
                                'seller_status_history',
                                'gigs_revisions_pre_metrics',
                                'gigs_revisions_pre_metrics_invalid',
                                'gigs_pages_pre_metrics',
                                'gigs_pages_pre_metrics_invalid',
                                'sellers_revisions_pre_metrics',
                                'sellers_revisions_pre_metrics_invalid'
                            ]::text[])
                )

                select distinct on (target_name)
                    'etl_dev' as target_schema,
                    t.target_name,
                    'etl_wrk_dev' as backup_schema,
                    c.relname::text as backup_name,
                    substring(c.relname, 'backup_*(.+$)')::bigint as backup_load_id
                from targets t
                left join pg_catalog.pg_class c
                    on t.target_name = substring(c.relname, '^.+?(?=_backup)')
                where
                    c.relkind = 'r'
                    and c.relnamespace = 'etl_wrk_dev'::regnamespace
                order by
                    target_name,
                    backup_load_id desc

            loop

                if rollback_tbls_rec.backup_name is not null then

                    execute format
                    (
                        $exec$

                            truncate table %1$I.%2$I restart identity cascade;

                            insert into %1$I.%2$I
                                select *
                                from %3$I.%4$I;

                            cluster %1$I.%2$I using %2$s_pkey;

                            analyze %1$I.%2$I;

                        $exec$,
                        rollback_tbls_rec.target_schema,
                        rollback_tbls_rec.target_name,
                        rollback_tbls_rec.backup_schema,
                        rollback_tbls_rec.backup_name
                    );

                    log_msg := log_msg
                        || 'Table ' || quote_ident(rollback_tbls_rec.target_schema) || '.' || quote_ident(rollback_tbls_rec.target_name)
                        || ' has been rolled back to backup ' || quote_ident(rollback_tbls_rec.backup_schema) || '.' || quote_ident(rollback_tbls_rec.backup_name)
                        || E'\n';

                else

                    log_msg := log_msg
                        || 'Table ' || quote_ident(rollback_tbls_rec.target_schema) || '.' || quote_ident(rollback_tbls_rec.target_name)
                        || E' rollback has been skipped' || E'\n';

                end if;

            end loop;

            if log_msg = '' then
                log_msg := 'No target tables have been rolled back to backups';
            else
                log_msg := rtrim(log_msg, E'\n');
            end if;

        end if;

        select
            high_level_step || '.3' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'ddl' as op_type
        into log_rec;

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 4 - Maintaining the log

        if
            public.dblink_get_connections() is not null
            and load_id || '_conn' = any(public.dblink_get_connections()) then

                perform public.dblink_disconnect(load_id || '_conn');
        end if;

        cluster etl_log.etl_log using etl_log_pkey;

        analyze etl_log.etl_log;

        log_msg := 'Rollback has been finished';

        total_runtime := clock_timestamp() - transaction_timestamp();

        -- returning valid Python dictionary with load_id, runtime and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', high_level_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            if
                public.dblink_get_connections() is not null
                and load_id || '_conn' = any(public.dblink_get_connections()) then

                    perform public.dblink_disconnect(load_id || '_conn');
            end if;

            total_runtime := clock_timestamp() - transaction_timestamp();

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;