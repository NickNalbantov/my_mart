create or replace function etl_dev.relevance_sorting
(
    metric numeric,
    dttm timestamptz,
    base_dttm timestamptz default (current_date::timestamp at time zone 'UTC')::timestamptz,
    scale_factor numeric default 0.0001
)
returns numeric
language plpgsql
immutable
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$$
    declare
        sgn smallint;
        metric_log numeric;
        res numeric;

    begin

        sgn := sign(metric);

        if metric = 0 then
            metric_log := 0;
        else
            metric_log := log10(abs(metric));
        end if;

        res := (sgn * metric_log + sgn * scale_factor * extract(epoch from (base_dttm - dttm)) / 86400::numeric)::numeric;

        return res;

    end;
$$;