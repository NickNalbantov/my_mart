create or replace function etl_dev.competition_score
(
    cnt int,
    volume int,
    percentile numeric,
    competition_score_params jsonb default
        $js$
            {
                "volume_exp_scale_coeff": 100,
                "sigmoid_scale_coeff": -5,
                "sigmoid_shift_coeff": 0.5,
                "competition_score_scale_coeff": 0.8,
                "competition_score_add_coeff": 0.1,
                "volume_scale_coeff": 0.5
            }
        $js$::jsonb
)
returns numeric
language plpgsql
immutable
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$$
    declare
        params_rec record;

        ratio numeric;
        normalized_ratio numeric;
        volume_factor numeric;
        sigmoid numeric;

        res numeric;

    begin

        /*
            Details of the algorithm are described here:
            https://gitlab.com/fiverr-dev/data-engineering/analytics-notebooks/-/blob/main/competition_score/gigs_competition.py
        */

        if cnt <= 0 then
            res := 0;
            return res;

        elsif volume <= 0 and percentile > 0 then
            res := 100;
            return res;

        elsif volume <= 0 and percentile = 0 then
            res := 0;
            return res;

        end if;

        select
            coalesce(nullif((competition_score_params ->> 'volume_exp_scale_coeff')::numeric, 0::numeric), 1::numeric) as volume_exp_scale_coeff,
            (competition_score_params ->> 'sigmoid_scale_coeff')::numeric as sigmoid_scale_coeff,
            (competition_score_params ->> 'sigmoid_shift_coeff')::numeric as sigmoid_shift_coeff,
            (competition_score_params ->> 'competition_score_scale_coeff')::numeric as competition_score_scale_coeff,
            (competition_score_params ->> 'competition_score_add_coeff')::numeric as competition_score_add_coeff,
            (competition_score_params ->> 'volume_scale_coeff')::numeric as volume_scale_coeff
        into params_rec;

        ratio = cnt::numeric / volume::numeric;
        normalized_ratio := ln(1 + ratio)::numeric / ln(1 + percentile)::numeric;
        volume_factor := 1 - exp(-volume::numeric / params_rec.volume_exp_scale_coeff);
        sigmoid = 1 / (1 + exp(params_rec.sigmoid_scale_coeff * (normalized_ratio - params_rec.sigmoid_shift_coeff)));

        res :=
            (sigmoid * params_rec.competition_score_scale_coeff + params_rec.competition_score_add_coeff)
            * 99 * (1 - params_rec.volume_scale_coeff * volume_factor) + 1;

        return res;

    end;
$$;