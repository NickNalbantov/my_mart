create or replace function etl_dev.metrics_func
(
    start_dttm timestamptz default (current_date::timestamp at time zone 'UTC' - '1 month'::interval)::timestamptz,
    end_dttm timestamptz default (current_date::timestamp at time zone 'UTC' + '1 day'::interval)::timestamptz,
    ratings_to_volume_coeff numeric default 0.8, -- arbitrary coefficient representing ratio of reviews conversion to sales
    overpriced_coeff numeric default 10, -- crutch to filter gigs with suspiciously high prices (higher than weighted average price in subcategory multiplied by this coefficient)
    competition_score_percentile numeric default 0.95, -- percentile of inversed sales ratio metrics which used for calculations of gigs' competition scores
    competition_score_params jsonb default -- set of parameters for etl_dev.competition_score function for calculating competition_score_xxx attributes
        $js$
            {
                "volume_exp_scale_coeff": 100,
                "sigmoid_scale_coeff": -5,
                "sigmoid_shift_coeff": 0.5,
                "competition_score_scale_coeff": 0.8,
                "competition_score_add_coeff": 0.1,
                "volume_scale_coeff": 0.5
            }
        $js$::jsonb,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '4'
)
returns void
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        run_start_dttm timestamptz;
        total_runtime interval;

        period_width interval;

    begin

        -- Starting

        run_start_dttm := clock_timestamp();
        period_width := end_dttm - start_dttm;

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            start_dttm,
            end_dttm,
            ratings_to_volume_coeff,
            overpriced_coeff,
            competition_score_percentile,
            competition_score_params,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Preparing slices with active gigs and sellers

            -- Step 1.1 - Active gigs

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_gigs_hist_active cascade;

        create unlogged table if not exists etl_wrk_dev.metrics_gigs_hist_active
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (gig_id)
                valid_to_dttm,
                gig_id
            from etl_dev.gig_status_history
            where
                valid_to_dttm >= start_dttm
                and valid_from_dttm < end_dttm
                and fiverr_status in ('approved', 'active')
            order by
                gig_id,
                valid_to_dttm desc;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_metrics_gigs_hist_active
            on etl_wrk_dev.metrics_gigs_hist_active (gig_id) include (valid_to_dttm) with (fillfactor = 100);

        analyze etl_wrk_dev.metrics_gigs_hist_active;

        select
            high_level_step || '.1.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_gigs_hist_active' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


            -- Step 1.2 - Active sellers

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_sellers_hist_active cascade;

        create unlogged table if not exists etl_wrk_dev.metrics_sellers_hist_active
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (seller_id)
                valid_to_dttm,
                seller_id
            from etl_dev.seller_status_history
            where
                valid_to_dttm >= start_dttm
                and valid_from_dttm < end_dttm
                and status = 'available'
            order by
                seller_id,
                valid_to_dttm desc;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_metrics_sellers_hist_active
            on etl_wrk_dev.metrics_sellers_hist_active (seller_id) include (valid_to_dttm) with (fillfactor = 100);

        analyze etl_wrk_dev.metrics_sellers_hist_active;

        select
            high_level_step || '.1.2' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_sellers_hist_active' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 2 - Regular metrics

            -- Step 2.1 - Data from current assessed period (regular metrics)

                -- Step 2.1.1 - Sellers revisions from current assessed period (regular metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.reg_metrics_cur_sellers_revisions cascade;

        create unlogged table if not exists etl_wrk_dev.reg_metrics_cur_sellers_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                s.fiverr_seller_id,
                min(s.seller_ratings_count) as min_seller_ratings_count,
                max(s.seller_ratings_count) as max_seller_ratings_count
            from etl_dev.sellers_revisions_pre_metrics s
            where
                s.scraped_at >= start_dttm
                and s.scraped_at < end_dttm
                and exists
                    (
                        select
                        from etl_wrk_dev.metrics_sellers_hist_active h
                        where
                            s.seller_id = h.seller_id
                    )
            group by
                s.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_metrics_cur_sellers_revisions
            on etl_wrk_dev.reg_metrics_cur_sellers_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.reg_metrics_cur_sellers_revisions;

        select
            high_level_step || '.2.1.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'reg_metrics_cur_sellers_revisions' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.1.2 - Gigs pages from current assessed period (regular metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.reg_metrics_cur_gigs_pages cascade;

        create unlogged table if not exists etl_wrk_dev.reg_metrics_cur_gigs_pages
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                p.gig_id,
                p.fiverr_seller_id,
                min(p.seller_completed_orders_count) over (partition by p.fiverr_seller_id) as min_seller_completed_orders_count,
                max(p.seller_completed_orders_count) over (partition by p.fiverr_seller_id) as max_seller_completed_orders_count,
                first_value(p.prices) over (partition by p.gig_id order by p.scraped_at desc, p.id desc) as prices
            from etl_dev.gigs_pages_pre_metrics p
            where
                p.scraped_at >= start_dttm
                and p.scraped_at < end_dttm
                and exists
                    (
                        select
                        from etl_wrk_dev.metrics_gigs_hist_active h
                        where
                            p.gig_id = h.gig_id
                    );

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_metrics_cur_gigs_pages
            on etl_wrk_dev.reg_metrics_cur_gigs_pages (gig_id) with (fillfactor = 100);

        analyze etl_wrk_dev.reg_metrics_cur_gigs_pages;

        select
            high_level_step || '.2.1.2' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'reg_metrics_cur_gigs_pages' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.1.3 - Gigs revisions from current assessed period (regular metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.reg_metrics_cur_gigs_revisions cascade;

        create unlogged table if not exists etl_wrk_dev.reg_metrics_cur_gigs_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (gig_id)
                g.created_at,
                g.fiverr_created_at,
                g.scraped_at,
                g.gig_id,
                g.fiverr_gig_id,
                g.fiverr_seller_id,
                g.category_id,
                g.sub_category_id,
                g.nested_sub_category_id,
                coalesce(min(g.gig_ratings_count) over (partition by g.gig_id), 0)::int as min_gig_ratings_count,
                coalesce(max(g.gig_ratings_count) over (partition by g.gig_id), 0)::int as max_gig_ratings_count,
                g.gig_is_pro,
                case when h.valid_to_dttm >= end_dttm then true else false end as gig_is_active,
                g.gig_rating,
                g.prices,
                g.gig_title,
                g.category_name,
                g.sub_category_name,
                g.nested_sub_category_name,
                g.gig_cached_slug,
                g.gig_preview_url
            from etl_dev.gigs_revisions_pre_metrics g
            join etl_wrk_dev.metrics_gigs_hist_active h
                on g.gig_id = h.gig_id
            where
                g.scraped_at >= start_dttm
                and g.scraped_at < end_dttm
            order by
                g.gig_id,
                g.scraped_at desc,
                g.id desc;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_metrics_cur_gigs_revisions
            on etl_wrk_dev.reg_metrics_cur_gigs_revisions (gig_id) with (fillfactor = 100);
        create index if not exists idx_reg_metrics_cur_gigs_revisions_fiverr_seller_id
            on etl_wrk_dev.reg_metrics_cur_gigs_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.reg_metrics_cur_gigs_revisions;

        select
            high_level_step || '.2.1.3' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'reg_metrics_cur_gigs_revisions' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.1.4 - Metrics from current assessed period (regular metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.reg_metrics_cur cascade;

        create unlogged table if not exists etl_wrk_dev.reg_metrics_cur
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.created_at,
                g.fiverr_created_at,
                g.scraped_at,
                g.gig_id,
                g.fiverr_gig_id,
                g.fiverr_seller_id,
                g.category_id,
                g.sub_category_id,
                g.nested_sub_category_id,
                case
                    when g.max_gig_ratings_count = g.min_gig_ratings_count
                        then 0::int
                    else g.max_gig_ratings_count
                end as gig_ratings_count,
                g.max_gig_ratings_count,
                case
                    when
                        greatest(
                            max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                            max(g.max_gig_ratings_count) over w_s
                        )::int =
                        least(
                            min(coalesce(s.min_seller_ratings_count, 0)) over w_s,
                            min(g.min_gig_ratings_count) over w_s
                        )::int
                    then 0::int
                    else
                        greatest(
                            max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                            max(g.max_gig_ratings_count) over w_s
                        )::int
                end as seller_ratings_count,
                greatest(
                    max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                    max(g.max_gig_ratings_count) over w_s
                )::int as max_seller_ratings_count,
                case
                    when
                        (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int =
                        (min(coalesce(p.min_seller_completed_orders_count, 0)) over w_s)::int
                    then 0::int
                    else
                        (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int
                end as seller_completed_orders_count,
                (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int as max_seller_completed_orders_count,
                g.gig_is_pro,
                g.gig_is_active,
                g.gig_rating,
                coalesce(p.prices, g.prices, array[0]::numeric[]) as prices,
                g.gig_title,
                g.category_name,
                g.sub_category_name,
                g.nested_sub_category_name,
                g.gig_cached_slug,
                g.gig_preview_url
            from etl_wrk_dev.reg_metrics_cur_gigs_revisions g
            left join etl_wrk_dev.reg_metrics_cur_gigs_pages p
                on g.gig_id = p.gig_id
            left join etl_wrk_dev.reg_metrics_cur_sellers_revisions s
                on g.fiverr_seller_id = s.fiverr_seller_id
            window
                w_s as (partition by g.fiverr_seller_id);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_metrics_cur
            on etl_wrk_dev.reg_metrics_cur (gig_id) with (fillfactor = 100);
        create index if not exists idx_reg_metrics_cur_fiverr_seller_id
            on etl_wrk_dev.reg_metrics_cur (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.reg_metrics_cur;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.reg_metrics_cur_gigs_revisions cascade;
            drop table if exists etl_wrk_dev.reg_metrics_cur_gigs_pages cascade;
            drop table if exists etl_wrk_dev.reg_metrics_cur_sellers_revisions cascade;
        end if;

        select
            high_level_step || '.2.1.4' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'reg_metrics_cur' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 2.2 - Data from previous assessed period (regular metrics)

                -- Step 2.2.1 - Active sellers in current period (regular metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.reg_metrics_prev_sellers_revisions_filter cascade;

        create unlogged table if not exists etl_wrk_dev.reg_metrics_prev_sellers_revisions_filter
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                fiverr_seller_id
            from etl_wrk_dev.reg_metrics_cur;

        get diagnostics
            row_cnt = row_count;

        alter table if exists etl_wrk_dev.reg_metrics_prev_sellers_revisions_filter
            add primary key (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.reg_metrics_prev_sellers_revisions_filter;

        select
            high_level_step || '.2.2.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'reg_metrics_prev_sellers_revisions_filter' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.2.2 - Sellers revisions from previous assessed period (regular metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.reg_metrics_prev_sellers_revisions cascade;

            -- Planner CRUTCH ALERT!!! Have to disable nested loop for this one query (bad estimates with nested loop). Works fast with hash join
        set enable_nestloop = off;

        create unlogged table if not exists etl_wrk_dev.reg_metrics_prev_sellers_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                s.fiverr_seller_id,
                max(s.seller_ratings_count) as max_seller_ratings_count
            from etl_dev.sellers_revisions_pre_metrics s
            where
                s.scraped_at < start_dttm
                and exists
                    (
                        select
                        from etl_wrk_dev.reg_metrics_prev_sellers_revisions_filter f
                        where
                            s.fiverr_seller_id = f.fiverr_seller_id
                    )
            group by
                s.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        set enable_nestloop = on;

        create unique index if not exists uidx_reg_metrics_prev_sellers_revisions
            on etl_wrk_dev.reg_metrics_prev_sellers_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.reg_metrics_prev_sellers_revisions;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.reg_metrics_prev_sellers_revisions_filter cascade;
        end if;

        select
            high_level_step || '.2.2.2' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'reg_metrics_prev_sellers_revisions' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.2.3 - Gigs pages from previous assessed period (regular metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.reg_metrics_prev_gigs_pages cascade;

        create unlogged table if not exists etl_wrk_dev.reg_metrics_prev_gigs_pages
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                p.fiverr_seller_id,
                max(p.seller_completed_orders_count) as max_seller_completed_orders_count
            from etl_dev.gigs_pages_pre_metrics p
            where
                p.scraped_at < start_dttm
                and exists
                    (
                        select
                        from etl_wrk_dev.reg_metrics_cur cur
                        where
                            p.gig_id = cur.gig_id
                    )
            group by
                p.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_metrics_prev_gigs_pages
            on etl_wrk_dev.reg_metrics_prev_gigs_pages (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.reg_metrics_prev_gigs_pages;

        select
            high_level_step || '.2.2.3' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'reg_metrics_prev_gigs_pages' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.2.4 - Gigs revisions from previous assessed period (regular metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.reg_metrics_prev_gigs_revisions cascade;

        create unlogged table if not exists etl_wrk_dev.reg_metrics_prev_gigs_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.gig_id,
                g.fiverr_seller_id,
                coalesce(max(g.gig_ratings_count), 0)::int as max_gig_ratings_count
            from etl_dev.gigs_revisions_pre_metrics g
            where
                g.scraped_at < start_dttm
                and exists
                    (
                        select
                        from etl_wrk_dev.reg_metrics_cur cur
                        where
                            g.gig_id = cur.gig_id
                    )
            group by
                g.gig_id,
                g.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_metrics_prev_gigs_revisions
            on etl_wrk_dev.reg_metrics_prev_gigs_revisions (gig_id) with (fillfactor = 100);
        create index if not exists idx_reg_metrics_prev_gigs_revisions_fiverr_seller_id
            on etl_wrk_dev.reg_metrics_prev_gigs_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.reg_metrics_prev_gigs_revisions;

        select
            high_level_step || '.2.2.4' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'reg_metrics_prev_gigs_revisions' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.2.5 - Metrics from previous assessed period (regular metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.reg_metrics_prev cascade;

        create unlogged table if not exists etl_wrk_dev.reg_metrics_prev
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.gig_id,
                g.max_gig_ratings_count as gig_ratings_count,
                greatest(
                    max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                    max(g.max_gig_ratings_count) over w_s
                )::int as seller_ratings_count,
                (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int as seller_completed_orders_count
            from etl_wrk_dev.reg_metrics_prev_gigs_revisions g
            left join etl_wrk_dev.reg_metrics_prev_gigs_pages p
                on g.fiverr_seller_id = p.fiverr_seller_id
            left join etl_wrk_dev.reg_metrics_prev_sellers_revisions s
                on s.fiverr_seller_id = g.fiverr_seller_id
            window
                w_s as (partition by g.fiverr_seller_id);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_metrics_prev
            on etl_wrk_dev.reg_metrics_prev (gig_id) with (fillfactor = 100);

        analyze etl_wrk_dev.reg_metrics_prev;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.reg_metrics_prev_gigs_revisions cascade;
            drop table if exists etl_wrk_dev.reg_metrics_prev_gigs_pages cascade;
            drop table if exists etl_wrk_dev.reg_metrics_prev_sellers_revisions cascade;
        end if;

        select
            high_level_step || '.2.2.5' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'reg_metrics_prev' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 2.3.1 - Gig reviews prices counts (regular metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.reg_metrics_gig_reviews_prices_counts cascade;

        create unlogged table if not exists etl_wrk_dev.reg_metrics_gig_reviews_prices_counts
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                g.gig_id,
                g.price_range_start,
                g.price_range_end,
                count(*) over(partition by g.gig_id) as gig_reviews_cnt,
                count(*) over(partition by g.gig_id, g.price_range_start) as gig_reviews_cnt_by_start_price,
                count(*) over(partition by g.gig_id, g.price_range_end) as gig_reviews_cnt_by_end_price
            from etl_dev.gig_reviews_prices g
            where
                g.fiverr_created_at >= start_dttm
                and g.fiverr_created_at < end_dttm
                and exists
                    (
                        select
                        from etl_wrk_dev.metrics_gigs_hist_active h
                        where
                            g.gig_id = h.gig_id
                    );

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_reg_metrics_gig_reviews_prices_counts
            on etl_wrk_dev.reg_metrics_gig_reviews_prices_counts (gig_id) with (fillfactor = 100);

        analyze etl_wrk_dev.reg_metrics_gig_reviews_prices_counts;

        select
            high_level_step || '.2.3.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'reg_metrics_gig_reviews_prices_counts' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 2.3.2 - Gig reviews weighted prices (regular metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.reg_metrics_gig_reviews_weighted_prices cascade;

        create unlogged table if not exists etl_wrk_dev.reg_metrics_gig_reviews_weighted_prices
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                gig_id,
                sum(price_range_start * gig_reviews_cnt_by_start_price)::numeric / gig_reviews_cnt::numeric as weighted_start_price_by_gig,
                sum(price_range_end * gig_reviews_cnt_by_end_price)::numeric / gig_reviews_cnt::numeric as weighted_end_price_by_gig
            from etl_wrk_dev.reg_metrics_gig_reviews_prices_counts
            group by
                gig_id,
                gig_reviews_cnt;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_metrics_gig_reviews_weighted_prices
            on etl_wrk_dev.reg_metrics_gig_reviews_weighted_prices (gig_id) with (fillfactor = 100);

        analyze etl_wrk_dev.reg_metrics_gig_reviews_weighted_prices;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.reg_metrics_gig_reviews_prices_counts cascade;
        end if;

        select
            high_level_step || '.2.3.2' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'reg_metrics_gig_reviews_weighted_prices' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 2.4 - Joining data from previous and current periods (regular metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.reg_metrics cascade;

        create unlogged table if not exists etl_wrk_dev.reg_metrics
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                cur.created_at,
                cur.fiverr_created_at,
                cur.scraped_at,
                cur.gig_id,
                cur.fiverr_gig_id,
                cur.fiverr_seller_id,
                cur.category_id,
                cur.sub_category_id,
                cur.nested_sub_category_id,
                cur.max_gig_ratings_count as gig_ratings_count,
                cur.max_seller_completed_orders_count as seller_completed_orders_count,
                (
                    case
                        when
                            nullif(cur.seller_ratings_count, 0::int) is null
                            or nullif(prev.seller_ratings_count, 0::int) is null
                            or (nullif(cur.seller_completed_orders_count, 0::int) is null and cur.gig_ratings_count > 0)
                            or (nullif(prev.seller_completed_orders_count, 0::int) is null and prev.gig_ratings_count > 0)
                        then
                            round((cur.gig_ratings_count - coalesce(prev.gig_ratings_count, 0))::numeric / ratings_to_volume_coeff) -- old version
                        else
                            round(
                                (cur.gig_ratings_count - coalesce(prev.gig_ratings_count, 0))::numeric *
                                cur.seller_completed_orders_count::numeric / cur.seller_ratings_count::numeric
                            )
                    end
                )::int as gig_volume_pre,
                (
                    case
                        when
                            nullif(cur.max_seller_ratings_count, 0::int) is null
                            or (nullif(cur.max_seller_completed_orders_count, 0::int) is null and cur.max_gig_ratings_count > 0)
                        then
                            round(cur.max_gig_ratings_count::numeric / ratings_to_volume_coeff) -- old version
                        else
                            round(
                                coalesce(cur.max_gig_ratings_count::numeric * cur.max_seller_completed_orders_count::numeric / cur.max_seller_ratings_count::numeric, 0)
                            )
                    end
                )::int as total_historical_volume,
                cur.gig_is_pro,
                cur.gig_is_active,
                cur.gig_rating,
                coalesce(rew.weighted_start_price_by_gig, 0::numeric) as weighted_start_price_by_gig,
                coalesce(rew.weighted_end_price_by_gig, 'infinity'::numeric) as weighted_end_price_by_gig,
                (select min(price) from unnest(cur.prices) p (price)) as min_price_by_gig,
                (select avg(price) from unnest(cur.prices) p (price)) as avg_price_by_gig,
                (select max(price) from unnest(cur.prices) p (price)) as max_price_by_gig,
                cur.gig_title,
                cur.gig_cached_slug,
                cur.category_name,
                cur.sub_category_name,
                cur.nested_sub_category_name,
                cur.gig_preview_url
            from etl_wrk_dev.reg_metrics_cur cur
            left join etl_wrk_dev.reg_metrics_prev prev
                on cur.gig_id = prev.gig_id
            left join etl_wrk_dev.reg_metrics_gig_reviews_weighted_prices rew
                on cur.gig_id = rew.gig_id;

        get diagnostics
            row_cnt = row_count;

        analyze etl_wrk_dev.reg_metrics;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.reg_metrics_prev cascade;
            drop table if exists etl_wrk_dev.reg_metrics_gig_reviews_weighted_prices cascade;
        end if;

        select
            high_level_step || '.2.4' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'reg_metrics' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 3 - Materializing sellers_revisions

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_sellers_revisions cascade;

            -- Planner CRUTCH ALERT!!! Have to disable merge join for this one query (expensive sorts). Works faster with hash join
        set enable_mergejoin = off;

        create unlogged table if not exists etl_wrk_dev.metrics_sellers_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (fiverr_seller_id)
                r.created_at,
                coalesce(s.joined_at, r.fiverr_created_at) as fiverr_created_at,
                r.scraped_at,
                r.seller_id,
                r.fiverr_seller_id,
                coalesce(max(r.seller_ratings_count) over (partition by r.seller_id), 0)::int as seller_ratings_count,
                r.seller_is_pro,
                case when h.valid_to_dttm >= end_dttm then true else false end as seller_is_active,
                r.seller_rating,
                r.seller_level,
                coalesce(s.username, r.seller_name) as seller_name,
                r.agency_slug,
                r.agency_status,
                r.seller_profile_image,
                r.seller_country,
                r.seller_country_code,
                r.seller_languages
            from etl_dev.sellers_revisions_pre_metrics r
            join public.sellers s
                on s.id = r.seller_id
            join etl_wrk_dev.metrics_sellers_hist_active h
                on r.seller_id = h.seller_id
            where
                r.scraped_at < end_dttm
            order by
                r.fiverr_seller_id,
                r.scraped_at desc,
                r.id desc;

        get diagnostics
            row_cnt = row_count;

        set enable_mergejoin = on;

        create unique index if not exists uidx_metrics_sellers_revisions
            on etl_wrk_dev.metrics_sellers_revisions (fiverr_seller_id) with (fillfactor = 100);

        cluster etl_wrk_dev.metrics_sellers_revisions using uidx_metrics_sellers_revisions;

        analyze etl_wrk_dev.metrics_sellers_revisions;

        select
            high_level_step || '.3' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_sellers_revisions' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 4 - Heuristic metrics

            -- Step 4.1 - Data before timeframe for regular metrics (heuristic metrics)

                -- Step 4.1.1 - Sellers revisions from current assessed period (heuristic metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.heur_metrics_cur_sellers_revisions cascade;

        create unlogged table if not exists etl_wrk_dev.heur_metrics_cur_sellers_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                s.fiverr_seller_id,
                min(s.seller_ratings_count) as min_seller_ratings_count,
                max(s.seller_ratings_count) as max_seller_ratings_count
            from etl_dev.sellers_revisions_pre_metrics s
            where
                s.scraped_at < start_dttm
                and exists
                    (
                        select
                        from etl_wrk_dev.metrics_sellers_hist_active h
                        where
                            s.seller_id = h.seller_id
                    )
            group by
                s.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_metrics_cur_sellers_revisions
            on etl_wrk_dev.heur_metrics_cur_sellers_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.heur_metrics_cur_sellers_revisions;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.metrics_sellers_hist_active cascade;
        end if;

        select
            high_level_step || '.4.1.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_metrics_cur_sellers_revisions' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 4.1.2 - Gigs pages from current assessed period (heuristic metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.heur_metrics_cur_gigs_pages cascade;

        create unlogged table if not exists etl_wrk_dev.heur_metrics_cur_gigs_pages
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                p.gig_id,
                p.fiverr_seller_id,
                min(p.seller_completed_orders_count) over (partition by p.fiverr_seller_id) as min_seller_completed_orders_count,
                max(p.seller_completed_orders_count) over (partition by p.fiverr_seller_id) as max_seller_completed_orders_count,
                first_value(p.prices) over (partition by p.gig_id order by p.scraped_at desc, p.id desc) as prices
            from etl_dev.gigs_pages_pre_metrics p
            where
                p.scraped_at < start_dttm
                and exists
                    (
                        select
                        from etl_wrk_dev.metrics_gigs_hist_active h
                        where
                            p.gig_id = h.gig_id
                    )
                and not exists
                    (
                        select
                        from etl_wrk_dev.reg_metrics_cur r
                        where
                            p.gig_id = r.gig_id
                    );

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_metrics_cur_gigs_pages
            on etl_wrk_dev.heur_metrics_cur_gigs_pages (gig_id) with (fillfactor = 100);

        analyze etl_wrk_dev.heur_metrics_cur_gigs_pages;

        select
            high_level_step || '.4.1.2' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_metrics_cur_gigs_pages' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 4.1.3 - Gigs revisions from current assessed period (heuristic metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.heur_metrics_cur_gigs_revisions cascade;

        create unlogged table if not exists etl_wrk_dev.heur_metrics_cur_gigs_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (gig_id)
                g.created_at,
                g.fiverr_created_at,
                g.scraped_at,
                g.gig_id,
                g.fiverr_gig_id,
                g.fiverr_seller_id,
                g.category_id,
                g.sub_category_id,
                g.nested_sub_category_id,
                coalesce(min(g.gig_ratings_count) over (partition by g.gig_id), 0)::int as min_gig_ratings_count,
                coalesce(max(g.gig_ratings_count) over (partition by g.gig_id), 0)::int as max_gig_ratings_count,
                g.gig_is_pro,
                case when h.valid_to_dttm >= end_dttm then true else false end as gig_is_active,
                g.gig_rating,
                g.prices,
                g.gig_title,
                g.category_name,
                g.sub_category_name,
                g.nested_sub_category_name,
                g.gig_cached_slug,
                g.gig_preview_url
            from etl_dev.gigs_revisions_pre_metrics g
            join etl_wrk_dev.metrics_gigs_hist_active h
                on g.gig_id = h.gig_id
            where
                g.scraped_at < start_dttm
                and not exists
                    (
                        select
                        from etl_wrk_dev.reg_metrics_cur r
                        where
                            r.gig_id = g.gig_id
                    )
            order by
                g.gig_id,
                g.scraped_at desc,
                g.id desc;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_metrics_cur_gigs_revisions
            on etl_wrk_dev.heur_metrics_cur_gigs_revisions (gig_id) with (fillfactor = 100);
        create index if not exists idx_heur_metrics_cur_gigs_revisions_fiverr_seller_id
            on etl_wrk_dev.heur_metrics_cur_gigs_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.heur_metrics_cur_gigs_revisions;

        select
            high_level_step || '.4.1.3' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_metrics_cur_gigs_revisions' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 4.1.4 - Metrics from current assessed period (heuristic metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.heur_metrics_cur cascade;

        create unlogged table if not exists etl_wrk_dev.heur_metrics_cur
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.created_at,
                g.fiverr_created_at,
                g.scraped_at,
                g.gig_id,
                g.fiverr_gig_id,
                g.fiverr_seller_id,
                g.category_id,
                g.sub_category_id,
                g.nested_sub_category_id,
                case
                    when g.max_gig_ratings_count = g.min_gig_ratings_count
                        then 0::int
                    else g.max_gig_ratings_count
                end as gig_ratings_count,
                g.max_gig_ratings_count,
                case
                    when
                        greatest(
                            max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                            max(g.max_gig_ratings_count) over w_s
                        )::int =
                        least(
                            min(coalesce(s.min_seller_ratings_count, 0)) over w_s,
                            min(g.min_gig_ratings_count) over w_s
                        )::int
                    then 0::int
                    else
                        greatest(
                            max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                            max(g.max_gig_ratings_count) over w_s
                        )::int
                end as seller_ratings_count,
                greatest(
                    max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                    max(g.max_gig_ratings_count) over w_s
                )::int as max_seller_ratings_count,
                case
                    when
                        (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int =
                        (min(coalesce(p.min_seller_completed_orders_count, 0)) over w_s)::int
                    then 0::int
                    else
                        (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int
                end as seller_completed_orders_count,
                (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int as max_seller_completed_orders_count,
                g.gig_is_pro,
                g.gig_is_active,
                g.gig_rating,
                coalesce(p.prices, g.prices, array[0]::numeric[]) as prices,
                g.gig_title,
                g.category_name,
                g.sub_category_name,
                g.nested_sub_category_name,
                g.gig_cached_slug,
                g.gig_preview_url
            from etl_wrk_dev.heur_metrics_cur_gigs_revisions g
            left join etl_wrk_dev.heur_metrics_cur_gigs_pages p
                on g.gig_id = p.gig_id
            left join etl_wrk_dev.heur_metrics_cur_sellers_revisions s
                on g.fiverr_seller_id = s.fiverr_seller_id
            window
                w_s as (partition by g.fiverr_seller_id);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_metrics_cur
            on etl_wrk_dev.heur_metrics_cur (gig_id, scraped_at desc) with (fillfactor = 100);
        create index if not exists idx_heur_metrics_cur_fiverr_seller_id
            on etl_wrk_dev.heur_metrics_cur (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.heur_metrics_cur;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.heur_metrics_cur_gigs_revisions cascade;
            drop table if exists etl_wrk_dev.heur_metrics_cur_gigs_pages cascade;
            drop table if exists etl_wrk_dev.heur_metrics_cur_sellers_revisions cascade;
        end if;

        select
            high_level_step || '.4.1.4' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_metrics_cur' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 4.2 - Data from previous assessed period relative to dataset from Step 4.1 (heuristic metrics)

                -- Step 4.2.1 - Active sellers in current period (heuristic metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.heur_metrics_prev_sellers_revisions_filter cascade;

        create unlogged table if not exists etl_wrk_dev.heur_metrics_prev_sellers_revisions_filter
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                fiverr_seller_id
            from etl_wrk_dev.heur_metrics_cur;

        get diagnostics
            row_cnt = row_count;

        alter table if exists etl_wrk_dev.heur_metrics_prev_sellers_revisions_filter
            add primary key (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.heur_metrics_prev_sellers_revisions_filter;

        select
            high_level_step || '.4.2.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_metrics_prev_sellers_revisions_filter' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 4.2.2 - Sellers revisions from previous assessed period (heuristic metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.heur_metrics_prev_sellers_revisions cascade;

        create unlogged table if not exists etl_wrk_dev.heur_metrics_prev_sellers_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                s.fiverr_seller_id,
                max(s.seller_ratings_count) as max_seller_ratings_count
            from etl_dev.sellers_revisions_pre_metrics s
            where
                s.scraped_at < start_dttm - period_width
                and exists
                    (
                        select
                        from etl_wrk_dev.heur_metrics_prev_sellers_revisions_filter f
                        where
                            s.fiverr_seller_id = f.fiverr_seller_id
                    )
            group by
                s.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_metrics_prev_sellers_revisions
            on etl_wrk_dev.heur_metrics_prev_sellers_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.heur_metrics_prev_sellers_revisions;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.heur_metrics_prev_sellers_revisions_filter cascade;
        end if;

        select
            high_level_step || '.4.2.2' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_metrics_prev_sellers_revisions' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 4.2.3 - Gigs pages from previous assessed period (heuristic metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.heur_metrics_prev_gigs_pages cascade;

        create unlogged table if not exists etl_wrk_dev.heur_metrics_prev_gigs_pages
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                p.fiverr_seller_id,
                max(p.seller_completed_orders_count) as max_seller_completed_orders_count
            from etl_dev.gigs_pages_pre_metrics p
            where
                p.scraped_at < start_dttm - period_width
                and exists
                    (
                        select
                        from etl_wrk_dev.heur_metrics_cur cur
                        where
                            p.gig_id = cur.gig_id
                            and p.scraped_at < cur.scraped_at - period_width
                    )
            group by
                p.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_metrics_prev_gigs_pages
            on etl_wrk_dev.heur_metrics_prev_gigs_pages (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.heur_metrics_prev_gigs_pages;

        select
            high_level_step || '.4.2.3' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_metrics_prev_gigs_pages' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 4.2.4 - Gigs revisions from previous assessed period (heuristic metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.heur_metrics_prev_gigs_revisions cascade;

        create unlogged table if not exists etl_wrk_dev.heur_metrics_prev_gigs_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.gig_id,
                g.fiverr_seller_id,
                coalesce(max(g.gig_ratings_count), 0)::int as max_gig_ratings_count
            from etl_dev.gigs_revisions_pre_metrics g
            where
                g.scraped_at < start_dttm - period_width
                and exists
                    (
                        select
                        from etl_wrk_dev.heur_metrics_cur cur
                        where
                            g.gig_id = cur.gig_id
                            and g.scraped_at < cur.scraped_at - period_width
                    )
            group by
                g.gig_id,
                g.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_metrics_prev_gigs_revisions
            on etl_wrk_dev.heur_metrics_prev_gigs_revisions (gig_id) with (fillfactor = 100);
        create index if not exists idx_heur_metrics_prev_gigs_revisions_fiverr_seller_id
            on etl_wrk_dev.heur_metrics_prev_gigs_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk_dev.heur_metrics_prev_gigs_revisions;

        select
            high_level_step || '.4.2.4' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_metrics_prev_gigs_revisions' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 4.2.5 - Metrics from previous assessed period (heuristic metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.heur_metrics_prev cascade;

        create unlogged table if not exists etl_wrk_dev.heur_metrics_prev
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.gig_id,
                g.max_gig_ratings_count as gig_ratings_count,
                greatest(
                    max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                    max(g.max_gig_ratings_count) over w_s
                )::int as seller_ratings_count,
                (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int as seller_completed_orders_count
            from etl_wrk_dev.heur_metrics_prev_gigs_revisions g
            left join etl_wrk_dev.heur_metrics_prev_gigs_pages p
                on g.fiverr_seller_id = p.fiverr_seller_id
            left join etl_wrk_dev.heur_metrics_prev_sellers_revisions s
                on s.fiverr_seller_id = g.fiverr_seller_id
            window
                w_s as (partition by g.fiverr_seller_id);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_metrics_prev
            on etl_wrk_dev.heur_metrics_prev (gig_id) with (fillfactor = 100);

        analyze etl_wrk_dev.heur_metrics_prev;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.heur_metrics_prev_gigs_revisions cascade;
            drop table if exists etl_wrk_dev.heur_metrics_prev_gigs_pages cascade;
            drop table if exists etl_wrk_dev.heur_metrics_prev_sellers_revisions cascade;
        end if;

        select
            high_level_step || '.4.2.5' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_metrics_prev' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 4.3.1 - Gig reviews prices counts (heuristic metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.heur_metrics_gig_reviews_prices_counts cascade;

        create unlogged table if not exists etl_wrk_dev.heur_metrics_gig_reviews_prices_counts
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                g.gig_id,
                g.price_range_start,
                g.price_range_end,
                count(*) over(partition by g.gig_id) as gig_reviews_cnt,
                count(*) over(partition by g.gig_id, g.price_range_start) as gig_reviews_cnt_by_start_price,
                count(*) over(partition by g.gig_id, g.price_range_end) as gig_reviews_cnt_by_end_price
            from etl_dev.gig_reviews_prices g
            where
                g.fiverr_created_at < start_dttm
                and exists
                    (
                        select
                        from etl_wrk_dev.metrics_gigs_hist_active h
                        where
                            g.gig_id = h.gig_id
                    )
                and not exists
                    (
                        select
                        from etl_wrk_dev.reg_metrics_cur r
                        where
                            g.gig_id = r.gig_id
                    );

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_heur_metrics_gig_reviews_prices_counts
            on etl_wrk_dev.heur_metrics_gig_reviews_prices_counts (gig_id) with (fillfactor = 100);

        analyze etl_wrk_dev.heur_metrics_gig_reviews_prices_counts;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.metrics_gigs_hist_active cascade;
        end if;

        select
            high_level_step || '.4.3.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_metrics_gig_reviews_prices_counts' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 4.3.2 - Gig reviews weighted prices (heuristic metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.heur_metrics_gig_reviews_weighted_prices cascade;

        create unlogged table if not exists etl_wrk_dev.heur_metrics_gig_reviews_weighted_prices
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                gig_id,
                sum(price_range_start * gig_reviews_cnt_by_start_price)::numeric / gig_reviews_cnt::numeric as weighted_start_price_by_gig,
                sum(price_range_end * gig_reviews_cnt_by_end_price)::numeric / gig_reviews_cnt::numeric as weighted_end_price_by_gig
            from etl_wrk_dev.heur_metrics_gig_reviews_prices_counts
            group by
                gig_id,
                gig_reviews_cnt;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_metrics_gig_reviews_weighted_prices
            on etl_wrk_dev.heur_metrics_gig_reviews_weighted_prices (gig_id) with (fillfactor = 100);

        analyze etl_wrk_dev.heur_metrics_gig_reviews_weighted_prices;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.heur_metrics_gig_reviews_prices_counts cascade;
        end if;

        select
            high_level_step || '.4.3.2' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_metrics_gig_reviews_weighted_prices' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 4.4 - Joining data from previous and current periods (heuristic metrics)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.heur_metrics cascade;

        create unlogged table if not exists etl_wrk_dev.heur_metrics
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                cur.created_at,
                cur.fiverr_created_at,
                cur.scraped_at,
                cur.gig_id,
                cur.fiverr_gig_id,
                cur.fiverr_seller_id,
                cur.category_id,
                cur.sub_category_id,
                cur.nested_sub_category_id,
                cur.max_gig_ratings_count as gig_ratings_count,
                cur.max_seller_completed_orders_count as seller_completed_orders_count,
                (
                    case
                        when
                            nullif(cur.seller_ratings_count, 0::int) is null
                            or nullif(prev.seller_ratings_count, 0::int) is null
                            or (nullif(cur.seller_completed_orders_count, 0::int) is null and cur.gig_ratings_count > 0)
                            or (nullif(prev.seller_completed_orders_count, 0::int) is null and prev.gig_ratings_count > 0)
                        then
                            round((cur.gig_ratings_count - coalesce(prev.gig_ratings_count, 0))::numeric / ratings_to_volume_coeff) -- old version
                        else
                            round(
                                (cur.gig_ratings_count - coalesce(prev.gig_ratings_count, 0))::numeric *
                                cur.seller_completed_orders_count::numeric / cur.seller_ratings_count::numeric
                            )
                    end
                )::int as gig_volume_pre,
                (
                    case
                        when
                            nullif(cur.max_seller_ratings_count, 0::int) is null
                            or (nullif(cur.max_seller_completed_orders_count, 0::int) is null and cur.max_gig_ratings_count > 0)
                        then
                            round(cur.max_gig_ratings_count::numeric / ratings_to_volume_coeff) -- old version
                        else
                            round(
                                coalesce(cur.max_gig_ratings_count::numeric * cur.max_seller_completed_orders_count::numeric / cur.max_seller_ratings_count::numeric, 0)
                            )
                    end
                )::int as total_historical_volume,
                cur.gig_is_pro,
                cur.gig_is_active,
                cur.gig_rating,
                coalesce(rew.weighted_start_price_by_gig, 0::numeric) as weighted_start_price_by_gig,
                coalesce(rew.weighted_end_price_by_gig, 'infinity'::numeric) as weighted_end_price_by_gig,
                (select min(price) from unnest(cur.prices) p (price)) as min_price_by_gig,
                (select avg(price) from unnest(cur.prices) p (price)) as avg_price_by_gig,
                (select max(price) from unnest(cur.prices) p (price)) as max_price_by_gig,
                cur.gig_title,
                cur.gig_cached_slug,
                cur.category_name,
                cur.sub_category_name,
                cur.nested_sub_category_name,
                cur.gig_preview_url
            from etl_wrk_dev.heur_metrics_cur cur
            left join etl_wrk_dev.heur_metrics_prev prev
                on cur.gig_id = prev.gig_id
            left join etl_wrk_dev.heur_metrics_gig_reviews_weighted_prices rew
                on cur.gig_id = rew.gig_id;

        get diagnostics
            row_cnt = row_count;

        analyze etl_wrk_dev.heur_metrics;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.heur_metrics_cur cascade;
            drop table if exists etl_wrk_dev.heur_metrics_prev cascade;
            drop table if exists etl_wrk_dev.heur_metrics_gig_reviews_weighted_prices cascade;
        end if;

        select
            high_level_step || '.4.4' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'heur_metrics' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 5 - Union heuristic and non-heuristic gigs

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_union cascade;

        create unlogged table if not exists etl_wrk_dev.metrics_union
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                created_at,
                fiverr_created_at,
                scraped_at,
                gig_id,
                fiverr_gig_id,
                fiverr_seller_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                gig_ratings_count,
                seller_completed_orders_count,
                gig_volume,
                total_historical_volume,
                false as heuristic,
                gig_is_pro,
                gig_is_active,
                gig_rating,
                -- Fixing anomalies with prices
                least(min_price_by_gig, avg_price_by_gig, max_price_by_gig) as min_price_by_gig,
                (
                    case
                        when
                            avg_price_by_gig not between min_price_by_gig and max_price_by_gig
                        then
                            (min_price_by_gig + max_price_by_gig) / 2::numeric
                        else
                            avg_price_by_gig
                    end
                ) as avg_price_by_gig,
                greatest(min_price_by_gig, avg_price_by_gig, max_price_by_gig) as max_price_by_gig,
                gig_title,
                gig_cached_slug,
                category_name,
                sub_category_name,
                nested_sub_category_name,
                gig_preview_url
            from
            (
                select
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    gig_ratings_count,
                    seller_completed_orders_count,
                    greatest(least(gig_volume_pre, total_historical_volume), 0)::int as gig_volume,
                    total_historical_volume,
                    false as heuristic,
                    gig_is_pro,
                    gig_is_active,
                    gig_rating,
                    (
                        case
                            when -- no data from gig_reviews_prices
                                weighted_start_price_by_gig = 0::numeric
                                and weighted_end_price_by_gig in (0::numeric, 'infinity'::numeric)
                            then
                                min_price_by_gig
                            when -- invalid weighted_end_price_by_gig
                                weighted_start_price_by_gig > 0::numeric
                                and weighted_end_price_by_gig = 0::numeric
                            then
                                weighted_start_price_by_gig
                            when -- valid data from gig_reviews_prices
                                weighted_start_price_by_gig >= 0::numeric
                                and weighted_end_price_by_gig > 0::numeric
                            then
                                (
                                    case
                                        when -- valid data from gig_reviews_prices but weighted_end_price_by_gig >= min_price_by_gig
                                            weighted_end_price_by_gig >= min_price_by_gig
                                        then
                                            min_price_by_gig
                                        when -- valid data from gig_reviews_prices and weighted_end_price_by_gig < min_price_by_gig
                                            weighted_end_price_by_gig < min_price_by_gig
                                        then
                                            weighted_end_price_by_gig
                                    end
                                )
                            else
                                min_price_by_gig
                        end
                    ) as min_price_by_gig,
                    (
                        case
                            when -- no data from gig_reviews_prices
                                weighted_start_price_by_gig = 0::numeric
                                and weighted_end_price_by_gig in (0::numeric, 'infinity'::numeric)
                            then
                                avg_price_by_gig
                            when -- invalid weighted_end_price_by_gig
                                weighted_start_price_by_gig > 0::numeric
                                and weighted_end_price_by_gig = 0::numeric
                            then
                                (
                                    case
                                        when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig <= avg_price_by_gig
                                            weighted_start_price_by_gig <= avg_price_by_gig
                                        then
                                            avg_price_by_gig
                                        when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig > avg_price_by_gig
                                            weighted_start_price_by_gig > avg_price_by_gig
                                        then
                                            weighted_start_price_by_gig
                                    end
                                )
                            when -- valid data from gig_reviews_prices
                                weighted_start_price_by_gig >= 0::numeric
                                and weighted_end_price_by_gig > 0::numeric
                            then
                                (
                                    case
                                        when -- valid data from gig_reviews_prices but weighted_end_price_by_gig >= avg_price_by_gig
                                            weighted_end_price_by_gig >= avg_price_by_gig
                                        then
                                            avg_price_by_gig
                                        when -- valid data from gig_reviews_prices but weighted_end_price_by_gig < avg_price_by_gig
                                            weighted_end_price_by_gig < avg_price_by_gig
                                        then
                                            (weighted_start_price_by_gig + weighted_end_price_by_gig) / 2::numeric
                                    end
                                )
                            else
                                avg_price_by_gig
                        end
                    ) as avg_price_by_gig,
                    (
                        case
                            when -- no data from gig_reviews_prices
                                weighted_start_price_by_gig = 0::numeric
                                and weighted_end_price_by_gig in (0::numeric, 'infinity'::numeric)
                            then
                                max_price_by_gig

                            when -- invalid weighted_end_price_by_gig
                                weighted_start_price_by_gig > 0::numeric
                                and weighted_end_price_by_gig = 0::numeric
                            then
                                (
                                    case
                                        when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig <= max_price_by_gig
                                            weighted_start_price_by_gig <= max_price_by_gig
                                        then
                                            max_price_by_gig
                                        when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig > max_price_by_gig
                                            weighted_start_price_by_gig > max_price_by_gig
                                        then
                                            (
                                                case
                                                    when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig > max_price_by_gig and weighted_end_price_by_gig != 'infinity'
                                                        weighted_end_price_by_gig != 'infinity'::numeric
                                                    then
                                                        (weighted_start_price_by_gig + weighted_end_price_by_gig) / 2::numeric
                                                    when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig > max_price_by_gig and weighted_end_price_by_gig != 'infinity'
                                                        weighted_end_price_by_gig = 'infinity'::numeric
                                                    then
                                                        weighted_start_price_by_gig
                                                end
                                            )
                                    end
                                )
                            when -- valid data from gig_reviews_prices
                                weighted_start_price_by_gig >= 0::numeric
                                and weighted_end_price_by_gig > 0::numeric
                            then
                                (
                                    case
                                        when -- valid data from gig_reviews_prices but weighted_end_price_by_gig = 'infinity'
                                            weighted_end_price_by_gig = 'infinity'::numeric
                                        then
                                            max_price_by_gig
                                        when -- valid data from gig_reviews_prices and weighted_end_price_by_gig != 'infinity'
                                            weighted_end_price_by_gig != 'infinity'::numeric
                                        then
                                            weighted_end_price_by_gig
                                    end
                                )
                            else
                                max_price_by_gig
                        end
                    ) as max_price_by_gig,
                    gig_title,
                    gig_cached_slug,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name,
                    gig_preview_url
                from
                (
                    select *
                    from etl_wrk_dev.reg_metrics

                    union all

                    select *
                    from etl_wrk_dev.heur_metrics
                ) u
            ) p;

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_metrics_union_fiverr_seller_id
            on etl_wrk_dev.metrics_union (fiverr_seller_id) with (fillfactor = 100);
        create index if not exists idx_metrics_union_cat
            on etl_wrk_dev.metrics_union (category_id) with (fillfactor = 100);
        create index if not exists idx_metrics_union_subcat
            on etl_wrk_dev.metrics_union (sub_category_id) with (fillfactor = 100);
        create index if not exists idx_metrics_union_nest_subcat
            on etl_wrk_dev.metrics_union (nested_sub_category_id) with (fillfactor = 100);
        create index if not exists idx_metrics_union_all_cats
            on etl_wrk_dev.metrics_union (category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk_dev.metrics_union;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.reg_metrics_cur cascade;
            drop table if exists etl_wrk_dev.reg_metrics cascade;
            drop table if exists etl_wrk_dev.heur_metrics cascade;
        end if;

        select
            high_level_step || '.5' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_union' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 6 - Heuristics to fill categories names

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_categories_names cascade;

        create unlogged table if not exists etl_wrk_dev.metrics_categories_names
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (category_id, sub_category_id, nested_sub_category_id)
                m.category_id,
                m.sub_category_id,
                coalesce(m.nested_sub_category_id, -1)::int as nested_sub_category_id,
                public.last(coalesce(c.category_name, cd.category_name, m.category_name))
                    over(partition by m.category_id order by m.scraped_at, m.gig_id) as category_name,
                public.last(coalesce(c.sub_category_name, sd.sub_category_name, m.sub_category_name))
                    over(partition by m.sub_category_id order by m.scraped_at, m.gig_id) as sub_category_name,
                public.last(coalesce(c.nested_sub_category_name, nd.nested_sub_category_name, m.nested_sub_category_name))
                    over(partition by m.nested_sub_category_id order by m.scraped_at, m.gig_id) as nested_sub_category_name
            from etl_wrk_dev.metrics_union m
            left join etl_dev.category_tree_latest_revision c on -- if cat_name / sub_cat_name in revisions is empty then seek in category_tree by ids
                m.category_id = c.category_id
                and m.sub_category_id = c.sub_category_id
                and coalesce(m.nested_sub_category_id, -1)::int = coalesce(c.nested_sub_category_id, -1)::int
            left join -- if first LJ haven't found cat_name then pick latest available cat_name
                (
                    select distinct
                        category_id,
                        category_name
                    from etl_dev.category_tree_latest_revision
                ) cd on m.category_id = cd.category_id
            left join -- if first LJ haven't found sub_cat_name then pick latest available sub_cat_name
                (
                    select distinct
                        sub_category_id,
                        sub_category_name
                    from etl_dev.category_tree_latest_revision
                ) sd on m.sub_category_id = sd.sub_category_id
            left join -- if first LJ haven't found sub_cat_name then pick latest available nest_sub_cat_name
                (
                    select
                        nested_sub_category_id,
                        nested_sub_category_name
                    from etl_dev.category_tree_latest_revision
                    where
                        nested_sub_category_id is not null
                ) nd on m.nested_sub_category_id = nd.nested_sub_category_id
            order by
                category_id,
                sub_category_id,
                nested_sub_category_id,
                m.scraped_at desc,
                m.gig_id desc;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists idx_categories_names
            on etl_wrk_dev.metrics_categories_names (category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk_dev.metrics_categories_names;

        select
            high_level_step || '.6' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'categories_names' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 7 - Categories' metrics

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_categories cascade;

        create unlogged table if not exists etl_wrk_dev.metrics_categories
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                c.category_id,
                c.sub_category_id,
                c.nested_sub_category_id,
                (count(g.gig_id) filter (where g.gig_is_active is true and g.gig_is_pro is false) over w_cat)::int as regular_gigs_by_category,
                (count(g.gig_id) filter (where g.gig_is_active is true and g.gig_is_pro is false) over w_subcat)::int as regular_gigs_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else (count(g.gig_id) filter (where g.gig_is_active is true and g.gig_is_pro is false) over w_nest_subcat)::int
                end as regular_gigs_by_nested_subcategory,
                (count(g.gig_id) filter (where g.gig_is_active is true and g.gig_is_pro is true) over w_cat)::int as pro_gigs_by_category,
                (count(g.gig_id) filter (where g.gig_is_active is true and g.gig_is_pro is true) over w_subcat)::int as pro_gigs_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else (count(g.gig_id) filter (where g.gig_is_active is true and g.gig_is_pro is true) over w_nest_subcat)::int
                end as pro_gigs_by_nested_subcategory,
                coalesce(sum(g.gig_volume) over w_cat, 0)::int as volume_by_category,
                coalesce(sum(g.gig_volume) over w_subcat, 0)::int as volume_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.gig_volume) over w_nest_subcat, 0)::int
                end as volume_by_nested_subcategory,
                sum(g.total_historical_volume) over (w_cat) as total_historical_volume_by_category,
                sum(g.total_historical_volume) over (w_subcat) as total_historical_volume_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else sum(g.total_historical_volume) over (w_nest_subcat)
                end as total_historical_volume_by_nested_subcategory,
                coalesce(min(g.min_price_by_gig) over w_cat, 0)::numeric as min_price_by_category,
                coalesce(min(g.min_price_by_gig) over w_subcat, 0)::numeric as min_price_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(min(g.min_price_by_gig) over w_nest_subcat, 0)::numeric
                end as min_price_by_nested_subcategory,
                coalesce(avg(g.avg_price_by_gig) over w_cat, 0)::numeric as avg_price_by_category,
                coalesce(avg(g.avg_price_by_gig) over w_subcat, 0)::numeric as avg_price_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(avg(g.avg_price_by_gig) over w_nest_subcat, 0)::numeric
                end as avg_price_by_nested_subcategory,
                coalesce(max(g.max_price_by_gig) over w_cat, 0)::numeric as max_price_by_category,
                coalesce(max(g.max_price_by_gig) over w_subcat, 0)::numeric as max_price_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(max(g.max_price_by_gig) over w_nest_subcat, 0)::numeric
                end as max_price_by_nested_subcategory,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.min_price_by_gig) over w_cat)::numeric /
                            nullif(sum(g.gig_volume) over w_cat, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.min_price_by_gig) over w_cat
                    )::numeric as weighted_min_price_by_category,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.min_price_by_gig) over w_subcat)::numeric /
                            nullif(sum(g.gig_volume) over w_subcat, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.min_price_by_gig) over w_subcat
                    )::numeric as weighted_min_price_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(
                            nullif
                            (
                                (sum(g.gig_volume * g.min_price_by_gig) over w_nest_subcat)::numeric /
                                nullif(sum(g.gig_volume) over w_nest_subcat, 0)::numeric,
                                0::numeric
                            ),
                            avg(g.min_price_by_gig) over w_nest_subcat
                        )::numeric
                end as weighted_min_price_by_nested_subcategory,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.avg_price_by_gig) over w_cat)::numeric /
                            nullif(sum(g.gig_volume) over w_cat, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.avg_price_by_gig) over w_cat
                    )::numeric as weighted_avg_price_by_category,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.avg_price_by_gig) over w_subcat)::numeric /
                            nullif(sum(g.gig_volume) over w_subcat, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.avg_price_by_gig) over w_subcat
                    )::numeric as weighted_avg_price_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(
                            nullif
                            (
                                (sum(g.gig_volume * g.avg_price_by_gig) over w_nest_subcat)::numeric /
                                nullif(sum(g.gig_volume) over w_nest_subcat, 0)::numeric,
                                0::numeric
                            ),
                            avg(g.avg_price_by_gig) over w_nest_subcat
                        )::numeric
                end as weighted_avg_price_by_nested_subcategory,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.max_price_by_gig) over w_cat)::numeric /
                            nullif(sum(g.gig_volume) over w_cat, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.max_price_by_gig) over w_cat
                    )::numeric as weighted_max_price_by_category,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.max_price_by_gig) over w_subcat)::numeric /
                            nullif(sum(g.gig_volume) over w_subcat, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.max_price_by_gig) over w_subcat
                    )::numeric as weighted_max_price_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(
                            nullif
                            (
                                (sum(g.gig_volume * g.max_price_by_gig) over w_nest_subcat)::numeric /
                                nullif(sum(g.gig_volume) over w_nest_subcat, 0)::numeric,
                                0::numeric
                            ),
                            avg(g.max_price_by_gig) over w_nest_subcat
                        )::numeric
                end as weighted_max_price_by_nested_subcategory,
                first_value(c.category_name) over (
                        partition by g.category_id
                        order by g.scraped_at desc, g.gig_id desc
                    ) as category_name,
                first_value(c.sub_category_name) over (
                        partition by g.sub_category_id
                        order by g.scraped_at desc, g.gig_id desc
                    ) as sub_category_name,
                first_value(c.nested_sub_category_name) over (
                        partition by g.nested_sub_category_id
                        order by g.scraped_at desc, g.gig_id desc
                    ) as nested_sub_category_name
            from etl_wrk_dev.metrics_union g
            join etl_wrk_dev.metrics_categories_names c
                on g.category_id = c.category_id
                and g.sub_category_id = c.sub_category_id
                and coalesce(g.nested_sub_category_id, -1)::int = c.nested_sub_category_id
            window
                w_cat as (partition by g.category_id),
                w_subcat as (partition by g.sub_category_id),
                w_nest_subcat as (partition by g.nested_sub_category_id);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists idx_metrics_categories_ids
            on etl_wrk_dev.metrics_categories (category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk_dev.metrics_categories;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.metrics_categories_names cascade;
        end if;

        select
            high_level_step || '.7' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_categories' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 8 - Joining gigs, sellers and categories data

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_join cascade;

        create unlogged table if not exists etl_wrk_dev.metrics_join
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                s.created_at as seller_created_at,
                s.fiverr_created_at as seller_fiverr_created_at,
                s.scraped_at as seller_scraped_at,
                g.created_at as gig_created_at,
                g.fiverr_created_at as gig_fiverr_created_at,
                g.scraped_at as gig_scraped_at,
                s.seller_id,
                g.fiverr_seller_id,
                g.gig_id,
                g.fiverr_gig_id,
                g.category_id,
                g.sub_category_id,
                g.nested_sub_category_id,
                c.regular_gigs_by_category + c.pro_gigs_by_category as all_gigs_count_by_category,
                c.regular_gigs_by_subcategory + c.pro_gigs_by_subcategory as all_gigs_count_by_subcategory,
                c.regular_gigs_by_nested_subcategory + c.pro_gigs_by_nested_subcategory as all_gigs_count_by_nested_subcategory,
                c.regular_gigs_by_category,
                c.regular_gigs_by_subcategory,
                c.regular_gigs_by_nested_subcategory,
                c.pro_gigs_by_category,
                c.pro_gigs_by_subcategory,
                c.pro_gigs_by_nested_subcategory,
                (count(g.fiverr_seller_id) over (partition by g.category_id, s.seller_level))::int as seller_count_by_level_and_category,
                (count(g.fiverr_seller_id) over (partition by g.sub_category_id, s.seller_level))::int as seller_count_by_level_and_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else (count(g.fiverr_seller_id) over (partition by g.nested_sub_category_id, s.seller_level))::int
                end as seller_count_by_level_and_nested_subcategory,
                (count(g.gig_id) filter (where g.gig_is_active is true) over w_s)::int as gig_count_by_seller,
                s.seller_ratings_count,
                max(g.seller_completed_orders_count) over w_s as seller_completed_orders_count,
                coalesce(sum(g.gig_ratings_count) filter (where g.gig_is_active is true) over w_s, 0)::int as active_gigs_ratings_count_by_seller,
                g.gig_ratings_count,
                coalesce(sum(g.gig_volume) over w_s, 0)::int as volume_by_seller,
                g.gig_volume as volume_by_gig,
                c.volume_by_category,
                c.volume_by_subcategory,
                c.volume_by_nested_subcategory,
                sum(g.total_historical_volume) over (w_s) as total_historical_volume_by_seller,
                g.total_historical_volume as total_historical_volume_by_gig,
                c.total_historical_volume_by_category,
                c.total_historical_volume_by_subcategory,
                c.total_historical_volume_by_nested_subcategory,
                g.heuristic,
                s.seller_is_pro,
                s.seller_is_active,
                g.gig_is_pro,
                g.gig_is_active,
                coalesce(s.seller_rating, 0)::numeric as seller_rating,
                coalesce(g.gig_rating, 0)::numeric as gig_rating,
                coalesce(min(g.min_price_by_gig) over w_s, 0)::numeric as min_price_by_seller,
                g.min_price_by_gig,
                c.min_price_by_category,
                c.min_price_by_subcategory,
                c.min_price_by_nested_subcategory,
                coalesce(avg(g.avg_price_by_gig) over w_s, 0)::numeric as avg_price_by_seller,
                avg_price_by_gig,
                c.avg_price_by_category,
                c.avg_price_by_subcategory,
                c.avg_price_by_nested_subcategory,
                coalesce(max(g.max_price_by_gig) over w_s, 0)::numeric as max_price_by_seller,
                max_price_by_gig,
                c.max_price_by_category,
                c.max_price_by_subcategory,
                c.max_price_by_nested_subcategory,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.min_price_by_gig) over w_s)::numeric /
                            nullif(sum(g.gig_volume) over w_s, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.min_price_by_gig) over w_s
                    )::numeric as weighted_min_price_by_seller,
                c.weighted_min_price_by_category,
                c.weighted_min_price_by_subcategory,
                c.weighted_min_price_by_nested_subcategory,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.avg_price_by_gig) over w_s)::numeric /
                            nullif(sum(g.gig_volume) over w_s, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.avg_price_by_gig) over w_s
                    )::numeric as weighted_avg_price_by_seller,
                c.weighted_avg_price_by_category,
                c.weighted_avg_price_by_subcategory,
                c.weighted_avg_price_by_nested_subcategory,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.max_price_by_gig) over w_s)::numeric /
                            nullif(sum(g.gig_volume) over w_s, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.max_price_by_gig) over w_s
                    )::numeric as weighted_max_price_by_seller,
                c.weighted_max_price_by_category,
                c.weighted_max_price_by_subcategory,
                c.weighted_max_price_by_nested_subcategory,
                g.gig_volume * g.min_price_by_gig as min_revenue_by_gig,
                g.gig_volume * g.avg_price_by_gig as avg_revenue_by_gig,
                g.gig_volume * g.max_price_by_gig as max_revenue_by_gig,
                coalesce(
                        (c.regular_gigs_by_category + c.pro_gigs_by_category)::numeric /
                        nullif(g.gig_volume, 0)::numeric,
                    0)::numeric as inversed_sales_ratio_by_category,
                coalesce(
                        (c.regular_gigs_by_subcategory + c.pro_gigs_by_subcategory)::numeric /
                        nullif(g.gig_volume, 0)::numeric,
                    0)::numeric as inversed_sales_ratio_by_subcategory,
                coalesce(
                        (c.regular_gigs_by_nested_subcategory + c.pro_gigs_by_nested_subcategory)::numeric /
                        nullif(g.gig_volume, 0)::numeric,
                    0)::numeric as inversed_sales_ratio_by_nested_subcategory,
                s.seller_level,
                s.seller_name,
                s.agency_slug,
                s.agency_status,
                g.gig_title,
                g.gig_cached_slug,
                coalesce(c.category_name, 'Miscellaneous') as category_name,
                coalesce(c.sub_category_name, 'Miscellaneous') as sub_category_name,
                coalesce(c.nested_sub_category_name, 'Miscellaneous') as nested_sub_category_name,
                s.seller_country,
                s.seller_country_code,
                s.seller_languages,
                s.seller_profile_image,
                g.gig_preview_url
            from etl_wrk_dev.metrics_union g
            join etl_wrk_dev.metrics_sellers_revisions s
                on s.fiverr_seller_id = g.fiverr_seller_id
            join etl_wrk_dev.metrics_categories c
                on g.category_id = c.category_id
                and g.sub_category_id = c.sub_category_id
                and coalesce(g.nested_sub_category_id, -1)::int = c.nested_sub_category_id
            where
                g.avg_price_by_gig < overpriced_coeff * coalesce(c.weighted_avg_price_by_nested_subcategory, c.weighted_avg_price_by_subcategory)
            window
                w_s as (partition by g.fiverr_seller_id);

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_metrics_join_fiverr_seller_id
            on etl_wrk_dev.metrics_join (fiverr_seller_id) with (fillfactor = 100);
        create index if not exists idx_metrics_join_cat
            on etl_wrk_dev.metrics_join (category_id) with (fillfactor = 100);
        create index if not exists idx_metrics_join_subcat
            on etl_wrk_dev.metrics_join (sub_category_id) with (fillfactor = 100);
        create index if not exists idx_metrics_join_nest_subcat
            on etl_wrk_dev.metrics_join (nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk_dev.metrics_join;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.metrics_union cascade;
            drop table if exists etl_wrk_dev.metrics_sellers_revisions cascade;
            drop table if exists etl_wrk_dev.metrics_categories cascade;
        end if;

        select
            high_level_step || '.8' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_join' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 9 - Revenue

            -- Step 9.1 - Preparing dataset for categories' competition score calculations

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_competition_score_prepare cascade;

        create unlogged table if not exists etl_wrk_dev.metrics_competition_score_prepare
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select -- no group by / distinct here, we need all values for percentile
                category_id,
                sub_category_id,
                nested_sub_category_id,
                inversed_sales_ratio_by_category,
                inversed_sales_ratio_by_subcategory,
                inversed_sales_ratio_by_nested_subcategory
            from etl_wrk_dev.metrics_join
            where
                volume_by_gig != 0;

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_metrics_competition_score_prepare_cat
            on etl_wrk_dev.metrics_competition_score_prepare (category_id) with (fillfactor = 100);
        create index if not exists idx_metrics_competition_score_prepare_subcat
            on etl_wrk_dev.metrics_competition_score_prepare (sub_category_id) with (fillfactor = 100);
        create index if not exists idx_metrics_competition_score_prepare_nest_subcat
            on etl_wrk_dev.metrics_competition_score_prepare (nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk_dev.metrics_competition_score_prepare;

        select
            high_level_step || '.9.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_competition_score_prepare' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 9.2 - Sales percentile for categories' competition score

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_categories_percentile cascade;

        create unlogged table if not exists etl_wrk_dev.metrics_categories_percentile
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                category_id,
                coalesce(
                    percentile_disc(competition_score_percentile::double precision)
                        within group (order by inversed_sales_ratio_by_category::double precision)
                    , 0
                )::numeric as percentile
            from etl_wrk_dev.metrics_competition_score_prepare
            group by
                category_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_metrics_categories_percentile
            on etl_wrk_dev.metrics_categories_percentile (category_id) with (fillfactor = 100);

        analyze etl_wrk_dev.metrics_categories_percentile;

        select
            high_level_step || '.9.2' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_categories_percentile' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 9.3 - Sales percentile for subcategories' competition score

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_subcategories_percentile cascade;

        create unlogged table if not exists etl_wrk_dev.metrics_subcategories_percentile
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                sub_category_id,
                coalesce(
                    percentile_disc(competition_score_percentile::double precision)
                        within group (order by inversed_sales_ratio_by_subcategory::double precision)
                    , 0
                )::numeric as percentile
            from etl_wrk_dev.metrics_competition_score_prepare
            group by
                sub_category_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_metrics_subcategories_percentile
            on etl_wrk_dev.metrics_subcategories_percentile (sub_category_id) with (fillfactor = 100);

        analyze etl_wrk_dev.metrics_subcategories_percentile;

        select
            high_level_step || '.9.3' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_subcategories_percentile' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 9.4 - Sales percentile for nested subcategories' competition score

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_nested_subcategories_percentile cascade;

        create unlogged table if not exists etl_wrk_dev.metrics_nested_subcategories_percentile
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                nested_sub_category_id,
                coalesce(
                    percentile_disc(competition_score_percentile::double precision)
                        within group (order by inversed_sales_ratio_by_nested_subcategory::double precision)
                    , 0
                )::numeric as percentile
            from etl_wrk_dev.metrics_competition_score_prepare
            where
                nested_sub_category_id is not null
            group by
                nested_sub_category_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_metrics_nested_subcategories_percentile
            on etl_wrk_dev.metrics_nested_subcategories_percentile (nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk_dev.metrics_nested_subcategories_percentile;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.metrics_competition_score_prepare cascade;
        end if;

        select
            high_level_step || '.9.4' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_nested_subcategories_percentile' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 9.5 - Pre-final metrics table

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk_dev.metrics_prefin cascade;

        create unlogged table if not exists etl_wrk_dev.metrics_prefin
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.seller_created_at,
                g.seller_fiverr_created_at,
                g.seller_scraped_at,
                g.gig_created_at,
                g.gig_fiverr_created_at,
                g.gig_scraped_at,
                g.seller_id,
                g.fiverr_seller_id,
                g.gig_id,
                g.fiverr_gig_id,
                g.category_id,
                g.sub_category_id,
                g.nested_sub_category_id,
                g.all_gigs_count_by_category,
                g.all_gigs_count_by_subcategory,
                g.all_gigs_count_by_nested_subcategory,
                g.regular_gigs_by_category,
                g.regular_gigs_by_subcategory,
                g.regular_gigs_by_nested_subcategory,
                g.pro_gigs_by_category,
                g.pro_gigs_by_subcategory,
                g.pro_gigs_by_nested_subcategory,
                g.seller_count_by_level_and_category,
                g.seller_count_by_level_and_subcategory,
                g.seller_count_by_level_and_nested_subcategory,
                g.gig_count_by_seller,
                g.seller_ratings_count,
                g.seller_completed_orders_count,
                g.active_gigs_ratings_count_by_seller,
                g.gig_ratings_count,
                g.volume_by_seller,
                g.volume_by_gig,
                g.volume_by_category,
                g.volume_by_subcategory,
                g.volume_by_nested_subcategory,
                g.total_historical_volume_by_seller,
                g.total_historical_volume_by_gig,
                g.total_historical_volume_by_category,
                g.total_historical_volume_by_subcategory,
                g.total_historical_volume_by_nested_subcategory,
                g.heuristic,
                g.seller_is_pro,
                g.seller_is_active,
                g.gig_is_pro,
                g.gig_is_active,
                coalesce
                    (
                        round(g.volume_by_seller)::numeric
                        / nullif
                            (
                                round(g.total_historical_volume_by_seller) - round(least(g.volume_by_seller, g.total_historical_volume_by_seller)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as sales_volume_growth_percent_by_seller,
                coalesce
                    (
                        round(g.volume_by_gig)::numeric
                        / nullif
                            (
                                round(g.total_historical_volume_by_gig) - round(least(g.volume_by_gig, g.total_historical_volume_by_gig)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as sales_volume_growth_percent_by_gig,
                coalesce
                    (
                        round(g.volume_by_category)::numeric
                        / nullif
                            (
                                round(g.total_historical_volume_by_category) - round(least(g.volume_by_category, g.total_historical_volume_by_category)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as sales_volume_growth_percent_by_category,
                coalesce
                    (
                        round(g.volume_by_subcategory)::numeric
                        / nullif
                            (
                                round(g.total_historical_volume_by_subcategory) - round(least(g.volume_by_subcategory, g.total_historical_volume_by_subcategory)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as sales_volume_growth_percent_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce
                        (
                            round(g.volume_by_nested_subcategory)::numeric
                            / nullif
                                (
                                    round(g.total_historical_volume_by_nested_subcategory) - round(least(g.volume_by_nested_subcategory, g.total_historical_volume_by_nested_subcategory)),
                                    0
                                )::numeric,
                            0::numeric
                        ) * 100
                end as sales_volume_growth_percent_by_nested_subcategory,
                case
                    when c.category_id is null then 0
                    else
                        etl_dev.competition_score(g.all_gigs_count_by_category, g.volume_by_gig, c.percentile, competition_score_params)
                    end as competition_score_for_gig_by_category,
                case
                    when s.sub_category_id is null then 0
                    else
                        etl_dev.competition_score(g.all_gigs_count_by_subcategory, g.volume_by_gig, s.percentile, competition_score_params)
                    end as competition_score_for_gig_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    when g.nested_sub_category_id is not null and n.nested_sub_category_id is null then 0
                    else
                        etl_dev.competition_score(g.all_gigs_count_by_nested_subcategory, g.volume_by_gig, n.percentile, competition_score_params)
                end as competition_score_for_gig_by_nested_subcategory,
                coalesce
                    (
                        (1 - 1::numeric / (1 + g.all_gigs_count_by_category::numeric / nullif(g.volume_by_category, 0)::numeric)) * 99::numeric + 1,
                        0
                    )::numeric as competition_score_for_category,
                coalesce
                    (
                        (1 - 1::numeric / (1 + g.all_gigs_count_by_subcategory::numeric / nullif(g.volume_by_subcategory, 0)::numeric)) * 99::numeric + 1,
                        0
                    )::numeric as competition_score_for_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else
                        coalesce
                            (
                                (1 - 1::numeric / (1 + g.all_gigs_count_by_nested_subcategory::numeric / nullif(g.volume_by_nested_subcategory, 0)::numeric)) * 99::numeric + 1,
                                0
                            )::numeric
                end as competition_score_for_nested_subcategory,
                case
                    when g.volume_by_category != 0
                        then least(g.volume_by_gig::numeric / g.volume_by_category::numeric, 1)::numeric
                    else 0::numeric
                end as market_share_for_gig_by_category,
                case
                    when g.volume_by_subcategory != 0
                        then least(g.volume_by_gig::numeric / g.volume_by_subcategory::numeric, 1)::numeric
                    else 0::numeric
                end as market_share_for_gig_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else
                        case
                            when g.volume_by_nested_subcategory != 0
                                then least(g.volume_by_gig::numeric / g.volume_by_nested_subcategory::numeric, 1)::numeric
                            else 0::numeric
                        end
                end as market_share_for_gig_by_nested_subcategory,
                coalesce
                    (
                        g.volume_by_category::numeric / nullif(sum(g.volume_by_gig) over (), 0)::numeric,
                        0::numeric
                    ) as market_share_for_category,
                coalesce
                    (
                        g.volume_by_subcategory::numeric / nullif(max(g.volume_by_category) over w_subcat, 0)::numeric,
                        0::numeric
                    ) as market_share_for_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else
                        coalesce
                        (
                            g.volume_by_nested_subcategory::numeric / nullif(max(g.volume_by_subcategory) over w_nest_subcat, 0)::numeric,
                            0::numeric
                        )
                end as market_share_for_nested_subcategory,
                g.seller_rating,
                g.gig_rating,
                g.min_price_by_seller,
                g.min_price_by_gig,
                g.min_price_by_category,
                g.min_price_by_subcategory,
                g.min_price_by_nested_subcategory,
                g.avg_price_by_seller,
                g.avg_price_by_gig,
                g.avg_price_by_category,
                g.avg_price_by_subcategory,
                g.avg_price_by_nested_subcategory,
                g.max_price_by_seller,
                g.max_price_by_gig,
                g.max_price_by_category,
                g.max_price_by_subcategory,
                g.max_price_by_nested_subcategory,
                g.weighted_min_price_by_seller,
                g.weighted_min_price_by_category,
                g.weighted_min_price_by_subcategory,
                g.weighted_min_price_by_nested_subcategory,
                g.weighted_avg_price_by_seller,
                g.weighted_avg_price_by_category,
                g.weighted_avg_price_by_subcategory,
                g.weighted_avg_price_by_nested_subcategory,
                g.weighted_max_price_by_seller,
                g.weighted_max_price_by_category,
                g.weighted_max_price_by_subcategory,
                g.weighted_max_price_by_nested_subcategory,
                coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is true) over w_s, 0)::numeric as min_active_revenue_by_seller,
                coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is false) over w_s, 0)::numeric as min_inactive_revenue_by_seller,
                g.min_revenue_by_gig,
                coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is true) over w_cat, 0)::numeric as min_active_revenue_by_category,
                coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is false) over w_cat, 0)::numeric as min_inactive_revenue_by_category,
                coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is true) over w_subcat, 0)::numeric as min_active_revenue_by_subcategory,
                coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is false) over w_subcat, 0)::numeric as min_inactive_revenue_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is true) over w_nest_subcat, 0)::numeric
                end as min_active_revenue_by_nested_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is false) over w_nest_subcat, 0)::numeric
                end as min_inactive_revenue_by_nested_subcategory,
                coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is true) over w_s, 0)::numeric as avg_active_revenue_by_seller,
                coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is false) over w_s, 0)::numeric as avg_inactive_revenue_by_seller,
                g.avg_revenue_by_gig,
                coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is true) over w_cat, 0)::numeric as avg_active_revenue_by_category,
                coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is false) over w_cat, 0)::numeric as avg_inactive_revenue_by_category,
                coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is true) over w_subcat, 0)::numeric as avg_active_revenue_by_subcategory,
                coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is false) over w_subcat, 0)::numeric as avg_inactive_revenue_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is true) over w_nest_subcat, 0)::numeric
                end as avg_active_revenue_by_nested_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is false) over w_nest_subcat, 0)::numeric
                end as avg_inactive_revenue_by_nested_subcategory,
                coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is true) over w_s, 0)::numeric as max_active_revenue_by_seller,
                coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is false) over w_s, 0)::numeric as max_inactive_revenue_by_seller,
                g.max_revenue_by_gig,
                coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is true) over w_cat, 0)::numeric as max_active_revenue_by_category,
                coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is false) over w_cat, 0)::numeric as max_inactive_revenue_by_category,
                coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is true) over w_subcat, 0)::numeric as max_active_revenue_by_subcategory,
                coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is false) over w_subcat, 0)::numeric as max_inactive_revenue_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is true) over w_nest_subcat, 0)::numeric
                end as max_active_revenue_by_nested_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is false) over w_nest_subcat, 0)::numeric
                end as max_inactive_revenue_by_nested_subcategory,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.min_price_by_gig) over w_s,
                        greatest(g.seller_completed_orders_count, g.total_historical_volume_by_seller) * g.weighted_min_price_by_seller
                    ) as min_total_historical_revenue_by_seller,
                g.total_historical_volume_by_gig * g.min_price_by_gig as min_total_historical_revenue_by_gig,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.min_price_by_gig) over w_cat,
                        g.total_historical_volume_by_category * g.weighted_min_price_by_category
                    ) as min_total_historical_revenue_by_category,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.min_price_by_gig) over w_subcat,
                        g.total_historical_volume_by_subcategory * g.weighted_min_price_by_subcategory
                    ) as min_total_historical_revenue_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else greatest(
                            sum(g.total_historical_volume_by_gig * g.min_price_by_gig) over w_nest_subcat,
                            g.total_historical_volume_by_nested_subcategory * g.weighted_min_price_by_nested_subcategory
                        )
                end as min_total_historical_revenue_by_nested_subcategory,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.avg_price_by_gig) over w_s,
                        greatest(g.seller_completed_orders_count, g.total_historical_volume_by_seller) * g.weighted_avg_price_by_seller
                    ) as avg_total_historical_revenue_by_seller,
                g.total_historical_volume_by_gig * g.avg_price_by_gig as avg_total_historical_revenue_by_gig,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.avg_price_by_gig) over w_cat,
                        g.total_historical_volume_by_category * g.weighted_avg_price_by_category
                    ) as avg_total_historical_revenue_by_category,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.avg_price_by_gig) over w_subcat,
                        g.total_historical_volume_by_subcategory * g.weighted_avg_price_by_subcategory
                    ) as avg_total_historical_revenue_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else greatest(
                            sum(g.total_historical_volume_by_gig * g.avg_price_by_gig) over w_nest_subcat,
                            g.total_historical_volume_by_nested_subcategory * g.weighted_avg_price_by_nested_subcategory
                        )
                end as avg_total_historical_revenue_by_nested_subcategory,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.max_price_by_gig) over w_s,
                        greatest(g.seller_completed_orders_count, g.total_historical_volume_by_seller) * g.weighted_max_price_by_seller
                    ) as max_total_historical_revenue_by_seller,
                g.total_historical_volume_by_gig * g.max_price_by_gig as max_total_historical_revenue_by_gig,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.max_price_by_gig) over w_cat,
                        g.total_historical_volume_by_category * g.weighted_max_price_by_category
                    ) as max_total_historical_revenue_by_category,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.max_price_by_gig) over w_subcat,
                        g.total_historical_volume_by_subcategory * g.weighted_max_price_by_subcategory
                    ) as max_total_historical_revenue_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else greatest(
                            sum(g.total_historical_volume_by_gig * g.max_price_by_gig) over w_nest_subcat,
                            g.total_historical_volume_by_nested_subcategory * g.weighted_max_price_by_nested_subcategory
                        )
                end as max_total_historical_revenue_by_nested_subcategory,
                g.seller_level,
                g.seller_name,
                g.agency_slug,
                g.agency_status,
                g.gig_title,
                g.gig_cached_slug,
                g.category_name,
                g.sub_category_name,
                g.nested_sub_category_name,
                g.seller_country,
                g.seller_country_code,
                g.seller_languages,
                g.seller_profile_image,
                g.gig_preview_url
            from etl_wrk_dev.metrics_join g
            left join etl_wrk_dev.metrics_categories_percentile c
                on c.category_id = g.category_id
                and c.percentile != 0
            left join etl_wrk_dev.metrics_subcategories_percentile s
                on s.sub_category_id = g.sub_category_id
                and s.percentile != 0
            left join etl_wrk_dev.metrics_nested_subcategories_percentile n
                on n.nested_sub_category_id = g.nested_sub_category_id
                and n.percentile != 0 -- percentile could be = 0 for categories without any active gigs
            window
                w_s as (partition by g.seller_id),
                w_cat as (partition by g.category_id),
                w_subcat as (partition by g.sub_category_id),
                w_nest_subcat as (partition by g.nested_sub_category_id);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_metrics_prefin
            on etl_wrk_dev.metrics_prefin (gig_id) with (fillfactor = 100);

        cluster etl_wrk_dev.metrics_prefin using uidx_metrics_prefin;

        analyze etl_wrk_dev.metrics_prefin;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.metrics_join cascade;
            drop table if exists etl_wrk_dev.metrics_categories_percentile cascade;
            drop table if exists etl_wrk_dev.metrics_subcategories_percentile cascade;
            drop table if exists etl_wrk_dev.metrics_nested_subcategories_percentile cascade;
        end if;

        select
            high_level_step || '.9.5' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_prefin' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Exiting

        select
            high_level_step || '.10' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - run_start_dttm;

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            start_dttm,
            end_dttm,
            ratings_to_volume_coeff,
            overpriced_coeff,
            competition_score_percentile,
            competition_score_params,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

    end;
$func$;