create or replace function etl_agg.af_agg_rollback
(
    tables_prefix text,
    metrics_start_dttm timestamptz,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '-1'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
#variable_conflict use_variable

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        partitioning_step text;
        killed_queries text[];
        cleanup_tbls_rec record;
        rollback_tbls_rec record;
        partition_pkey text;
        has_etl_finished boolean;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Killing parallel sessions

        task_start_dttm := clock_timestamp();

        select
            array_agg(query)::text[] as killed_queries
        into killed_queries
        from
        (
            select
                pg_terminate_backend(pid),
                query
            from pg_catalog.pg_stat_activity
            where
                usename = 'airflow'
                and
                (
                    (
                        dag_name = 'etl_agg_' || tables_prefix
                        and application_name like initcap(tables_prefix) || ' aggregation ETL%'
                    )
                    or dag_name = 'manual_launch'
                )
                and pid != pg_backend_pid()
        ) a;

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'maintenance' as op_type
        into log_rec;

        if cardinality(killed_queries) > 0 then
            log_msg := E'Sessions with following queries has been killed:\n\n - ' || array_to_string(killed_queries, E'\n\n - ');
        else
            log_msg := 'No sessions has been killed';
        end if;

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 2 - Cleaning up temporary tables

        task_start_dttm := clock_timestamp();

        if debug_mode is false then

            log_msg := '';

            for cleanup_tbls_rec in
                select
                    'etl_wrk' as relnamespace,
                    relname
                from pg_catalog.pg_class
                where
                    relkind = 'r'
                    and relnamespace = 'etl_wrk'::regnamespace
                    and substring(relname, '(^agg_backup_|backup_\d+$|_backup$)') is null
            loop

                execute format
                (
                    $exec$
                        drop table if exists %1$I.%2$I cascade
                    $exec$,
                    cleanup_tbls_rec.relnamespace, cleanup_tbls_rec.relname
                );

                log_msg := log_msg
                    || 'Working table ' || quote_ident(cleanup_tbls_rec.relnamespace) || '.' || quote_ident(cleanup_tbls_rec.relname)
                    || ' has been dropped' || E'\n';

            end loop;

            if log_msg = '' then
                log_msg := 'No working tables in schema etl_wrk to clean up';
            else
                log_msg := rtrim(log_msg, E'\n');
            end if;

        else
            log_msg := 'Working tables cleanup has been skipped because debug_mode set to True';

        end if;

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'ddl' as op_type
        into log_rec;

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 3 - Rolling back target tables

        task_start_dttm := clock_timestamp();

        select
            count(*) > 0
        into has_etl_finished
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l.src_name = 'af_mj_fin_etl_statistics'
            and l.op_type = 'start';

        select
            case
                when tables_prefix = 'quarterly' then '3 month'
                else '1 ' || replace(tables_prefix, 'ly', '')
            end
        into partitioning_step;

            -- Nothing to rollback

        if has_etl_finished is true then

            log_msg := 'ETL successfully finished, targets rollback is not needed';

            -- For aggregated ETL truncating current period partitions and restoring them from backups if any

        elsif has_etl_finished is false then

            log_msg := '';

            for rollback_tbls_rec in

                with targets_roots as
                (
                    select
                        l.tgt_schema as root_schema,
                        l.tgt_name as root_name
                    from etl_log.etl_log l
                    where
                        l."load_id" = load_id
                        and l.op_type = any(array['insert', 'update', 'merge', 'delete']::text[])
                        and l.tgt_schema = 'etl_agg'
                ),

                targets_partitions as
                (
                    select
                        r.root_schema,
                        r.root_name,
                        c.relnamespace::regnamespace::text as partition_schema,
                        c.relname::text as partition_name
                    from pg_catalog.pg_class c
                    join pg_catalog.pg_inherits p
                        on c."oid" = p.inhrelid
                    join targets_roots r
                        on p.inhparent = (r.root_schema || '.' || r.root_name)::regclass
                    where
                        c.relkind = 'r'
                        and c.relispartition is true
                        and metrics_start_dttm::date
                            = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
                        and (metrics_start_dttm::date + partitioning_step::interval)::date
                            = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date
                )

                select
                    p.root_schema,
                    p.root_name,
                    p.partition_schema,
                    p.partition_name,
                    'etl_wrk' as backup_schema,
                    c.relname::text as backup_name
                from targets_partitions p
                left join pg_catalog.pg_class c
                    on c.relname = substring('agg_backup_' || p.partition_name, 1, 63)
                where
                    c.relkind = 'r'
                    and c.relnamespace = 'etl_wrk'::regnamespace

            loop

                if rollback_tbls_rec.backup_name is not null then

                    execute format -- swapping updated partition to backup
                    (
                        $exec$

                            alter table if exists %1$I.%2$I
                                detach partition %3$I.%4$I;

                            drop table if exists %3$I.%4$I cascade;

                            alter table if exists %5$I.%6$I
                                set schema %3$I;

                            alter table if exists %3$I.%6$I
                                rename to %4$I;

                            alter table if exists %1$I.%2$I
                                attach partition %3$I.%4$I
                                for values
                                    from (%7$L)
                                    to (%8$L);

                        $exec$,
                        rollback_tbls_rec.root_schema,
                        rollback_tbls_rec.root_name,
                        rollback_tbls_rec.partition_schema,
                        rollback_tbls_rec.partition_name,
                        rollback_tbls_rec.backup_schema,
                        rollback_tbls_rec.backup_name,
                        metrics_start_dttm::date,
                        (metrics_start_dttm::date + partitioning_step::interval)::date
                    );

                    select
                        ci.relname::text
                    into partition_pkey
                    from pg_catalog.pg_index i
                    join pg_catalog.pg_class ci
                        on i.indexrelid = ci."oid"
                    where
                        i.indrelid = (rollback_tbls_rec.partition_schema || '.' || rollback_tbls_rec.partition_name)::regclass
                        and i.indisprimary is true
                        and i.indisvalid is true
                        and i.indisready is true
                        and i.indislive is true
                        and i.indcheckxmin is false;

                    execute format -- CLUSTER + ANALYZE recovered data
                    (
                        $exec$

                            cluster %1$I.%2$I using %3$I;

                            analyze %4$I.%5$I;

                        $exec$,
                        rollback_tbls_rec.partition_schema,
                        rollback_tbls_rec.partition_name,
                        partition_pkey,
                        rollback_tbls_rec.root_schema,
                        rollback_tbls_rec.root_name
                    );

                    log_msg := log_msg
                        || initcap(tables_prefix) || ' partition for values from ''' || metrics_start_dttm::date || ''' for table '
                        || quote_ident(rollback_tbls_rec.root_schema) || '.' || quote_ident(rollback_tbls_rec.root_name)
                        || ' has been rolled back to backup ' || quote_ident(rollback_tbls_rec.backup_schema) || '.' || quote_ident(rollback_tbls_rec.backup_name)
                        || E'\n';

                else -- if there's no backup then just truncate partition

                    execute format
                    (
                        $exec$

                            truncate table %1$I.%2$I restart identity cascade;

                            analyze %3$I.%4$I;

                        $exec$,
                        rollback_tbls_rec.partition_schema,
                        rollback_tbls_rec.partition_name,
                        rollback_tbls_rec.root_schema,
                        rollback_tbls_rec.root_name
                    );

                    log_msg := log_msg
                        || initcap(tables_prefix) || ' partition for values from ''' || metrics_start_dttm::date || ''' for table '
                        || quote_ident(rollback_tbls_rec.root_schema) || '.' || quote_ident(rollback_tbls_rec.root_name)
                        || ' has been truncated' || E'\n';

                end if;

            end loop;

            if log_msg = '' then
                log_msg := 'No target tables have been rolled back to backups';
            else
                log_msg := rtrim(log_msg, E'\n');
            end if;

        end if;

        select
            high_level_step || '.3' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'ddl' as op_type
        into log_rec;

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 4 - Maintaining the log

        if
            public.dblink_get_connections() is not null
            and load_id || '_conn' = any(public.dblink_get_connections()) then

                perform public.dblink_disconnect(load_id || '_conn');
        end if;

        cluster etl_log.etl_log using etl_log_pkey;

        analyze etl_log.etl_log;

        log_msg := 'Rollback has been finished';

        total_runtime := clock_timestamp() - transaction_timestamp();

        -- returning valid Python dictionary with load_id, runtime and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', high_level_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            if
                public.dblink_get_connections() is not null
                and load_id || '_conn' = any(public.dblink_get_connections()) then

                    perform public.dblink_disconnect(load_id || '_conn');
            end if;

            total_runtime := clock_timestamp() - transaction_timestamp();

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;