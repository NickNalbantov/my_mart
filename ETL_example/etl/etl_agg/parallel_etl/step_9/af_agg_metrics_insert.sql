create or replace function etl_agg.af_agg_metrics_insert
(
    tables_prefix text,
    metrics_start_dttm timestamptz,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '9.1'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

        partitioning_step text;
        period_partition_rec record;
        empty_partition_flg boolean;
        root_pkey text;
        partition_pkey text;

        stats_columns_list text[];

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            tables_prefix,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        -- Step 1 - Preparations

        task_start_dttm := clock_timestamp();

            -- Getting name of current period partition

        select
            case
                when tables_prefix = 'quarterly' then '3 month'
                else '1 ' || replace(tables_prefix, 'ly', '')
            end
        into partitioning_step;

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg.' || tables_prefix || '_metrics')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

            -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk.agg_backup_%2$s cascade;

                -- This backup won't be cleaned up automatically
                    -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            high_level_step || '.1' as src_step,
            'etl_agg' as tgt_schema,
            tables_prefix || '_metrics' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 2 - Joining trends into metrics

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                seller_created_at,
                seller_fiverr_created_at,
                seller_scraped_at,
                gig_created_at,
                gig_fiverr_created_at,
                gig_scraped_at,
                agg_period_start,
                seller_id,
                fiverr_seller_id,
                gig_id,
                fiverr_gig_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                all_gigs_count_by_category,
                all_gigs_count_by_subcategory,
                all_gigs_count_by_nested_subcategory,
                regular_gigs_by_category,
                regular_gigs_by_subcategory,
                regular_gigs_by_nested_subcategory,
                pro_gigs_by_category,
                pro_gigs_by_subcategory,
                pro_gigs_by_nested_subcategory,
                seller_count_by_level_and_category,
                seller_count_by_level_and_subcategory,
                seller_count_by_level_and_nested_subcategory,
                gig_count_by_seller,
                seller_ratings_count,
                seller_completed_orders_count,
                active_gigs_ratings_count_by_seller,
                gig_ratings_count,
                volume_by_seller,
                volume_by_gig,
                volume_by_category,
                volume_by_subcategory,
                volume_by_nested_subcategory,
                total_historical_volume_by_seller,
                total_historical_volume_by_gig,
                total_historical_volume_by_category,
                total_historical_volume_by_subcategory,
                total_historical_volume_by_nested_subcategory,
                heuristic,
                seller_is_pro,
                seller_is_active,
                gig_is_pro,
                gig_is_active,
                is_trend_valid_for_seller,
                is_trend_valid_for_gig,
                is_trend_valid_for_category,
                is_trend_valid_for_subcategory,
                is_trend_valid_for_nested_subcategory,
                sales_volume_growth_percent_by_seller,
                sales_volume_growth_percent_by_gig,
                sales_volume_growth_percent_by_category,
                sales_volume_growth_percent_by_subcategory,
                sales_volume_growth_percent_by_nested_subcategory,
                competition_score_for_gig_by_category,
                competition_score_for_gig_by_subcategory,
                competition_score_for_gig_by_nested_subcategory,
                competition_score_for_category,
                competition_score_for_subcategory,
                competition_score_for_nested_subcategory,
                market_share_for_gig_by_category,
                market_share_for_gig_by_subcategory,
                market_share_for_gig_by_nested_subcategory,
                market_share_for_category,
                market_share_for_subcategory,
                market_share_for_nested_subcategory,
                seller_rating,
                gig_rating,
                min_price_by_seller,
                min_price_by_gig,
                min_price_by_category,
                min_price_by_subcategory,
                min_price_by_nested_subcategory,
                avg_price_by_seller,
                avg_price_by_gig,
                avg_price_by_category,
                avg_price_by_subcategory,
                avg_price_by_nested_subcategory,
                max_price_by_seller,
                max_price_by_gig,
                max_price_by_category,
                max_price_by_subcategory,
                max_price_by_nested_subcategory,
                weighted_min_price_by_seller,
                weighted_min_price_by_category,
                weighted_min_price_by_subcategory,
                weighted_min_price_by_nested_subcategory,
                weighted_avg_price_by_seller,
                weighted_avg_price_by_category,
                weighted_avg_price_by_subcategory,
                weighted_avg_price_by_nested_subcategory,
                weighted_max_price_by_seller,
                weighted_max_price_by_category,
                weighted_max_price_by_subcategory,
                weighted_max_price_by_nested_subcategory,
                min_active_revenue_by_seller,
                min_inactive_revenue_by_seller,
                min_total_revenue_by_seller,
                min_revenue_by_gig,
                min_active_revenue_by_category,
                min_inactive_revenue_by_category,
                min_total_revenue_by_category,
                min_active_revenue_by_subcategory,
                min_inactive_revenue_by_subcategory,
                min_total_revenue_by_subcategory,
                min_active_revenue_by_nested_subcategory,
                min_inactive_revenue_by_nested_subcategory,
                min_total_revenue_by_nested_subcategory,
                avg_active_revenue_by_seller,
                avg_inactive_revenue_by_seller,
                avg_total_revenue_by_seller,
                avg_revenue_by_gig,
                avg_active_revenue_by_category,
                avg_inactive_revenue_by_category,
                avg_total_revenue_by_category,
                avg_active_revenue_by_subcategory,
                avg_inactive_revenue_by_subcategory,
                avg_total_revenue_by_subcategory,
                avg_active_revenue_by_nested_subcategory,
                avg_inactive_revenue_by_nested_subcategory,
                avg_total_revenue_by_nested_subcategory,
                max_active_revenue_by_seller,
                max_inactive_revenue_by_seller,
                max_total_revenue_by_seller,
                max_revenue_by_gig,
                max_active_revenue_by_category,
                max_inactive_revenue_by_category,
                max_total_revenue_by_category,
                max_active_revenue_by_subcategory,
                max_inactive_revenue_by_subcategory,
                max_total_revenue_by_subcategory,
                max_active_revenue_by_nested_subcategory,
                max_inactive_revenue_by_nested_subcategory,
                max_total_revenue_by_nested_subcategory,
                min_total_historical_revenue_by_seller,
                min_total_historical_revenue_by_gig,
                min_total_historical_revenue_by_category,
                min_total_historical_revenue_by_subcategory,
                min_total_historical_revenue_by_nested_subcategory,
                avg_total_historical_revenue_by_seller,
                avg_total_historical_revenue_by_gig,
                avg_total_historical_revenue_by_category,
                avg_total_historical_revenue_by_subcategory,
                avg_total_historical_revenue_by_nested_subcategory,
                max_total_historical_revenue_by_seller,
                max_total_historical_revenue_by_gig,
                max_total_historical_revenue_by_category,
                max_total_historical_revenue_by_subcategory,
                max_total_historical_revenue_by_nested_subcategory,
                revenue_growth_percent_by_seller,
                revenue_growth_percent_by_gig,
                revenue_growth_percent_by_category,
                revenue_growth_percent_by_subcategory,
                revenue_growth_percent_by_nested_subcategory,
                trend_by_seller,
                r2_by_seller,
                trends_avg_revenue_by_seller,
                trend_by_gig,
                r2_by_gig,
                trends_avg_revenue_by_gig,
                trend_by_category,
                r2_by_category,
                trends_avg_revenue_by_category,
                trend_by_subcategory,
                r2_by_subcategory,
                trends_avg_revenue_by_subcategory,
                trend_by_nested_subcategory,
                r2_by_nested_subcategory,
                trends_avg_revenue_by_nested_subcategory,
                rcrit,
                seller_level,
                seller_name,
                agency_slug,
                agency_status,
                gig_title,
                gig_cached_slug,
                category_name,
                sub_category_name,
                nested_sub_category_name,
                seller_country,
                seller_country_code,
                seller_languages,
                seller_profile_image,
                gig_preview_url
            )

            select
                %3$L::bigint as "load_id",
                transaction_timestamp() as processed_dttm,
                m.seller_created_at,
                m.seller_fiverr_created_at,
                m.seller_scraped_at,
                m.gig_created_at,
                m.gig_fiverr_created_at,
                m.gig_scraped_at,
                %4$L::date as agg_period_start,
                m.seller_id,
                m.fiverr_seller_id,
                m.gig_id,
                m.fiverr_gig_id,
                m.category_id,
                m.sub_category_id,
                m.nested_sub_category_id,
                m.all_gigs_count_by_category,
                m.all_gigs_count_by_subcategory,
                m.all_gigs_count_by_nested_subcategory,
                m.regular_gigs_by_category,
                m.regular_gigs_by_subcategory,
                m.regular_gigs_by_nested_subcategory,
                m.pro_gigs_by_category,
                m.pro_gigs_by_subcategory,
                m.pro_gigs_by_nested_subcategory,
                m.seller_count_by_level_and_category,
                m.seller_count_by_level_and_subcategory,
                m.seller_count_by_level_and_nested_subcategory,
                m.gig_count_by_seller,
                m.seller_ratings_count,
                m.seller_completed_orders_count,
                m.active_gigs_ratings_count_by_seller,
                m.gig_ratings_count,
                m.volume_by_seller,
                m.volume_by_gig,
                m.volume_by_category,
                m.volume_by_subcategory,
                m.volume_by_nested_subcategory,
                m.total_historical_volume_by_seller,
                m.total_historical_volume_by_gig,
                m.total_historical_volume_by_category,
                m.total_historical_volume_by_subcategory,
                m.total_historical_volume_by_nested_subcategory,
                m.heuristic,
                m.seller_is_pro,
                m.seller_is_active,
                m.gig_is_pro,
                m.gig_is_active,
                case
                    when m.fiverr_seller_id = t.fiverr_seller_id
                        then sqrt(t.r2_by_seller) > t.rcrit
                    else
                        (max((sqrt(t.r2_by_seller) > t.rcrit)::int) filter (where m.fiverr_seller_id = t.fiverr_seller_id) over w_s)::boolean
                end as is_trend_valid_for_seller,
                sqrt(t.r2_by_gig) > t.rcrit as is_trend_valid_for_gig,
                case
                    when m.category_id = t.category_id
                        then sqrt(t.r2_by_category) > t.rcrit
                    else
                        (max((sqrt(t.r2_by_category) > t.rcrit)::int) filter (where m.category_id = t.category_id) over w_cat)::boolean
                end as is_trend_valid_for_category,
                case
                    when m.sub_category_id = t.sub_category_id
                        then sqrt(t.r2_by_subcategory) > t.rcrit
                    else
                        (max((sqrt(t.r2_by_subcategory) > t.rcrit)::int) filter (where m.sub_category_id = t.sub_category_id) over w_subcat)::boolean
                end as is_trend_valid_for_subcategory,
                case
                    when m.nested_sub_category_id is null then null
                    else
                        case
                            when m.nested_sub_category_id = t.nested_sub_category_id
                                then sqrt(t.r2_by_nested_subcategory) > t.rcrit
                            else
                                (max((sqrt(t.r2_by_nested_subcategory) > t.rcrit)::int) filter (where m.nested_sub_category_id = t.nested_sub_category_id) over w_nest_subcat)::boolean
                        end
                end as is_trend_valid_for_nested_subcategory,
                m.sales_volume_growth_percent_by_seller,
                m.sales_volume_growth_percent_by_gig,
                m.sales_volume_growth_percent_by_category,
                m.sales_volume_growth_percent_by_subcategory,
                m.sales_volume_growth_percent_by_nested_subcategory,
                m.competition_score_for_gig_by_category,
                m.competition_score_for_gig_by_subcategory,
                m.competition_score_for_gig_by_nested_subcategory,
                m.competition_score_for_category,
                m.competition_score_for_subcategory,
                m.competition_score_for_nested_subcategory,
                m.market_share_for_gig_by_category,
                m.market_share_for_gig_by_subcategory,
                m.market_share_for_gig_by_nested_subcategory,
                m.market_share_for_category,
                m.market_share_for_subcategory,
                m.market_share_for_nested_subcategory,
                m.seller_rating,
                m.gig_rating,
                m.min_price_by_seller,
                m.min_price_by_gig,
                m.min_price_by_category,
                m.min_price_by_subcategory,
                m.min_price_by_nested_subcategory,
                m.avg_price_by_seller,
                m.avg_price_by_gig,
                m.avg_price_by_category,
                m.avg_price_by_subcategory,
                m.avg_price_by_nested_subcategory,
                m.max_price_by_seller,
                m.max_price_by_gig,
                m.max_price_by_category,
                m.max_price_by_subcategory,
                m.max_price_by_nested_subcategory,
                m.weighted_min_price_by_seller,
                m.weighted_min_price_by_category,
                m.weighted_min_price_by_subcategory,
                m.weighted_min_price_by_nested_subcategory,
                m.weighted_avg_price_by_seller,
                m.weighted_avg_price_by_category,
                m.weighted_avg_price_by_subcategory,
                m.weighted_avg_price_by_nested_subcategory,
                m.weighted_max_price_by_seller,
                m.weighted_max_price_by_category,
                m.weighted_max_price_by_subcategory,
                m.weighted_max_price_by_nested_subcategory,
                m.min_active_revenue_by_seller,
                m.min_inactive_revenue_by_seller,
                m.min_active_revenue_by_seller + m.min_inactive_revenue_by_seller as min_total_revenue_by_seller,
                m.min_revenue_by_gig,
                m.min_active_revenue_by_category,
                m.min_inactive_revenue_by_category,
                m.min_active_revenue_by_category + m.min_inactive_revenue_by_category as min_total_revenue_by_category,
                m.min_active_revenue_by_subcategory,
                m.min_inactive_revenue_by_subcategory,
                m.min_active_revenue_by_subcategory + m.min_inactive_revenue_by_subcategory as min_total_revenue_by_subcategory,
                m.min_active_revenue_by_nested_subcategory,
                m.min_inactive_revenue_by_nested_subcategory,
                m.min_active_revenue_by_nested_subcategory + m.min_inactive_revenue_by_nested_subcategory as min_total_revenue_by_nested_subcategory,
                m.avg_active_revenue_by_seller,
                m.avg_inactive_revenue_by_seller,
                m.avg_active_revenue_by_seller + m.avg_inactive_revenue_by_seller as avg_total_revenue_by_seller,
                m.avg_revenue_by_gig,
                m.avg_active_revenue_by_category,
                m.avg_inactive_revenue_by_category,
                m.avg_active_revenue_by_category + m.avg_inactive_revenue_by_category as avg_total_revenue_by_category,
                m.avg_active_revenue_by_subcategory,
                m.avg_inactive_revenue_by_subcategory,
                m.avg_active_revenue_by_subcategory + m.avg_inactive_revenue_by_subcategory as avg_total_revenue_by_subcategory,
                m.avg_active_revenue_by_nested_subcategory,
                m.avg_inactive_revenue_by_nested_subcategory,
                m.avg_active_revenue_by_nested_subcategory + m.avg_inactive_revenue_by_nested_subcategory as avg_total_revenue_by_nested_subcategory,
                m.max_active_revenue_by_seller,
                m.max_inactive_revenue_by_seller,
                m.max_active_revenue_by_seller + m.max_inactive_revenue_by_seller as max_total_revenue_by_seller,
                m.max_revenue_by_gig,
                m.max_active_revenue_by_category,
                m.max_inactive_revenue_by_category,
                m.max_active_revenue_by_category + m.max_inactive_revenue_by_category as max_total_revenue_by_category,
                m.max_active_revenue_by_subcategory,
                m.max_inactive_revenue_by_subcategory,
                m.max_active_revenue_by_subcategory + m.max_inactive_revenue_by_subcategory as max_total_revenue_by_subcategory,
                m.max_active_revenue_by_nested_subcategory,
                m.max_inactive_revenue_by_nested_subcategory,
                m.max_active_revenue_by_nested_subcategory + m.max_inactive_revenue_by_nested_subcategory as max_total_revenue_by_nested_subcategory,
                m.min_total_historical_revenue_by_seller,
                m.min_total_historical_revenue_by_gig,
                m.min_total_historical_revenue_by_category,
                m.min_total_historical_revenue_by_subcategory,
                m.min_total_historical_revenue_by_nested_subcategory,
                m.avg_total_historical_revenue_by_seller,
                m.avg_total_historical_revenue_by_gig,
                m.avg_total_historical_revenue_by_category,
                m.avg_total_historical_revenue_by_subcategory,
                m.avg_total_historical_revenue_by_nested_subcategory,
                m.max_total_historical_revenue_by_seller,
                m.max_total_historical_revenue_by_gig,
                m.max_total_historical_revenue_by_category,
                m.max_total_historical_revenue_by_subcategory,
                m.max_total_historical_revenue_by_nested_subcategory,
                coalesce
                    (
                        round(m.min_active_revenue_by_seller + m.min_inactive_revenue_by_seller)::numeric
                        / nullif
                            (
                                round(m.min_total_historical_revenue_by_seller) - round(least((m.min_active_revenue_by_seller + m.min_inactive_revenue_by_seller), m.min_total_historical_revenue_by_seller)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as revenue_growth_percent_by_seller,
                coalesce
                    (
                        round(m.min_revenue_by_gig)::numeric
                        / nullif
                            (
                                round(m.min_total_historical_revenue_by_gig) - round(least(m.min_revenue_by_gig, m.min_total_historical_revenue_by_gig)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as revenue_growth_percent_by_gig,
                coalesce
                    (
                        round(m.min_active_revenue_by_category + m.min_inactive_revenue_by_category)::numeric
                        / nullif
                            (
                               round(m.min_total_historical_revenue_by_category) - round(least((m.min_active_revenue_by_category + m.min_inactive_revenue_by_category), m.min_total_historical_revenue_by_category)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as revenue_growth_percent_by_category,
                coalesce
                    (
                        round(m.min_active_revenue_by_subcategory + m.min_inactive_revenue_by_subcategory)::numeric
                        / nullif
                            (
                                round(m.min_total_historical_revenue_by_subcategory) - round(least((m.min_active_revenue_by_subcategory + m.min_inactive_revenue_by_subcategory), m.min_total_historical_revenue_by_subcategory)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as revenue_growth_percent_by_subcategory,
                case
                    when m.nested_sub_category_id is null then null
                    else coalesce
                        (
                            round(m.min_active_revenue_by_nested_subcategory + m.min_inactive_revenue_by_nested_subcategory)::numeric
                            / nullif
                                (
                                    round(m.min_total_historical_revenue_by_nested_subcategory) - round(least((m.min_active_revenue_by_nested_subcategory + m.min_inactive_revenue_by_nested_subcategory), m.min_total_historical_revenue_by_nested_subcategory)),
                                    0
                                )::numeric,
                            0::numeric
                        ) * 100
                end as revenue_growth_percent_by_nested_subcategory,
                case
                    when m.fiverr_seller_id = t.fiverr_seller_id
                        then t.trend_by_seller
                    else
                        max(t.trend_by_seller) filter (where m.fiverr_seller_id = t.fiverr_seller_id) over w_s
                end as trend_by_seller,
                case
                    when m.fiverr_seller_id = t.fiverr_seller_id
                        then t.r2_by_seller
                    else
                        max(t.r2_by_seller) filter (where m.fiverr_seller_id = t.fiverr_seller_id) over w_s
                end as r2_by_seller,
                case
                    when m.fiverr_seller_id = t.fiverr_seller_id
                        then t.full_avg_revenue_by_seller
                    else
                        max(t.full_avg_revenue_by_seller) filter (where m.fiverr_seller_id = t.fiverr_seller_id) over w_s
                end as trends_avg_revenue_by_seller,
                t.trend_by_gig,
                t.r2_by_gig,
                t.full_avg_revenue_by_gig as trends_avg_revenue_by_gig,
                case
                    when m.category_id = t.category_id
                        then t.trend_by_category
                    else
                        max(t.trend_by_category) filter (where m.category_id = t.category_id) over w_cat
                end as trend_by_category,
                case
                    when m.category_id = t.category_id
                        then t.r2_by_category
                    else
                        max(t.r2_by_category) filter (where m.category_id = t.category_id) over w_cat
                end as r2_by_category,
                case
                    when m.category_id = t.category_id
                        then t.full_avg_revenue_by_category
                    else
                        max(t.full_avg_revenue_by_category) filter (where m.category_id = t.category_id) over w_cat
                end as trends_avg_revenue_by_category,
                case
                    when m.sub_category_id = t.sub_category_id
                        then t.trend_by_subcategory
                    else
                        max(t.trend_by_subcategory) filter (where m.sub_category_id = t.sub_category_id) over w_subcat
                end as trend_by_subcategory,
                case
                    when m.sub_category_id = t.sub_category_id
                        then t.r2_by_subcategory
                    else
                        max(t.r2_by_subcategory) filter (where m.sub_category_id = t.sub_category_id) over w_subcat
                end as r2_by_subcategory,
                case
                    when m.sub_category_id = t.sub_category_id
                        then t.full_avg_revenue_by_subcategory
                    else
                        max(t.full_avg_revenue_by_subcategory) filter (where m.sub_category_id = t.sub_category_id) over w_subcat
                end as trends_avg_revenue_by_subcategory,
                case
                    when m.nested_sub_category_id is null then null
                    else
                        case
                            when m.nested_sub_category_id = t.nested_sub_category_id
                                then t.trend_by_nested_subcategory
                            else
                                max(t.trend_by_nested_subcategory) filter (where m.nested_sub_category_id = t.nested_sub_category_id) over w_nest_subcat
                        end
                end as trend_by_nested_subcategory,
                case
                    when m.nested_sub_category_id is null then null
                    else
                        case
                            when m.nested_sub_category_id = t.nested_sub_category_id
                                then t.r2_by_nested_subcategory
                            else
                                max(t.r2_by_nested_subcategory) filter (where m.nested_sub_category_id = t.nested_sub_category_id) over w_nest_subcat
                        end
                end as r2_by_nested_subcategory,
                case
                    when m.nested_sub_category_id is null then null
                    else
                        case
                            when m.nested_sub_category_id = t.nested_sub_category_id
                                then t.full_avg_revenue_by_nested_subcategory
                            else
                                max(t.full_avg_revenue_by_nested_subcategory) filter (where m.nested_sub_category_id = t.nested_sub_category_id) over w_nest_subcat
                        end
                end as trends_avg_revenue_by_nested_subcategory,
                t.rcrit,
                m.seller_level,
                m.seller_name,
                m.agency_slug,
                m.agency_status,
                m.gig_title,
                m.gig_cached_slug,
                m.category_name,
                m.sub_category_name,
                m.nested_sub_category_name,
                m.seller_country,
                m.seller_country_code,
                m.seller_languages,
                m.seller_profile_image,
                m.gig_preview_url
            from etl_wrk.metrics_prefin m
            join etl_wrk.trends t
                on m.gig_id = t.gig_id
            window
                w_s as (partition by m.fiverr_seller_id),
                w_cat as (partition by m.category_id),
                w_subcat as (partition by m.sub_category_id),
                w_nest_subcat as (partition by m.nested_sub_category_id);

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt = row_count;

            -- Maintenance

                -- Indices

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg.' || tables_prefix || '_metrics')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg.%1$s_metrics
                    add primary key (agg_period_start, gig_id) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create index if not exists idx_seller_id_%1$s_metrics
                on etl_agg.%1$s_metrics (agg_period_start, seller_id) with (fillfactor = 100);
            create index if not exists idx_category_id_%1$s_metrics
                on etl_agg.%1$s_metrics (agg_period_start, category_id) with (fillfactor = 100);
            create index if not exists idx_sub_category_id_%1$s_metrics
                on etl_agg.%1$s_metrics (agg_period_start, sub_category_id) with (fillfactor = 100);
            create index if not exists idx_nested_sub_category_id_%1$s_metrics
                on etl_agg.%1$s_metrics (agg_period_start, nested_sub_category_id) with (fillfactor = 100);
            create index if not exists idx_categories_%1$s_metrics
                on etl_agg.%1$s_metrics (agg_period_start, category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);

            alter table if exists etl_agg.%1$s_metrics
                replica identity using index %1$s_metrics_pkey;

        $exec$,
        tables_prefix
        );

                -- CLUSTER

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

                -- CREATE STATISTICS and ANALYZE

        stats_columns_list := array[
                'gig_id', 'seller_id', 'fiverr_gig_id', 'fiverr_seller_id',
                'category_id', 'sub_category_id', 'nested_sub_category_id'
            ];

        execute format($exec$
            create statistics if not exists %1$I.%2$s
            on %3$s
            from %1$I.%4$I;
        $exec$,
        period_partition_rec.part_schema,
        substring('stats_' || period_partition_rec.part_name, 1, 63),
        array_to_string(stats_columns_list, ', '),
        period_partition_rec.part_name
        );

        execute format($exec$
            analyze etl_agg.%1$s_metrics;
        $exec$,
        tables_prefix
        );

        if debug_mode is false then
            drop table if exists etl_wrk.metrics_prefin cascade;
            drop table if exists etl_wrk.trends cascade;
        end if;

        select
            high_level_step || '.2' as src_step,
            'etl_agg' as tgt_schema,
            tables_prefix || '_metrics' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.3' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            tables_prefix,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;