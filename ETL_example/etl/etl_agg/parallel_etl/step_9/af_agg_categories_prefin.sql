create or replace function etl_agg.af_agg_categories_prefin
(
    tables_prefix text,
    metrics_start_dttm timestamptz,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '9.2.3'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            tables_prefix,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Preparing dataset for categories' metrics

        task_start_dttm := clock_timestamp();

        execute format($exec$

            drop table if exists etl_wrk.categories_prefin cascade;

            create unlogged table if not exists etl_wrk.categories_prefin
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select distinct
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    all_gigs_count_by_category,
                    all_gigs_count_by_subcategory,
                    all_gigs_count_by_nested_subcategory,
                    regular_gigs_by_category as regular_gigs_count_by_category,
                    regular_gigs_by_subcategory as regular_gigs_count_by_subcategory,
                    regular_gigs_by_nested_subcategory as regular_gigs_count_by_nested_subcategory,
                    pro_gigs_by_category as pro_gigs_count_by_category,
                    pro_gigs_by_subcategory as pro_gigs_count_by_subcategory,
                    pro_gigs_by_nested_subcategory as pro_gigs_count_by_nested_subcategory,
                    volume_by_category as sales_volume_by_category,
                    volume_by_subcategory as sales_volume_by_subcategory,
                    volume_by_nested_subcategory as sales_volume_by_nested_subcategory,
                    total_historical_volume_by_category as total_historical_sales_volume_by_category,
                    total_historical_volume_by_subcategory as total_historical_sales_volume_by_subcategory,
                    total_historical_volume_by_nested_subcategory as total_historical_sales_volume_by_nested_subcategory,
                    is_trend_valid_for_category,
                    is_trend_valid_for_subcategory,
                    is_trend_valid_for_nested_subcategory,
                    sales_volume_growth_percent_by_category,
                    sales_volume_growth_percent_by_subcategory,
                    sales_volume_growth_percent_by_nested_subcategory,
                    competition_score_for_category,
                    competition_score_for_subcategory,
                    competition_score_for_nested_subcategory,
                    market_share_for_category,
                    market_share_for_subcategory,
                    market_share_for_nested_subcategory,
                    weighted_avg_price_by_category as weighted_avg_gig_price_by_category,
                    weighted_avg_price_by_subcategory as weighted_avg_gig_price_by_subcategory,
                    weighted_avg_price_by_nested_subcategory as weighted_avg_gig_price_by_nested_subcategory,
                    min_active_revenue_by_category,
                    min_inactive_revenue_by_category,
                    min_total_revenue_by_category,
                    min_active_revenue_by_subcategory,
                    min_inactive_revenue_by_subcategory,
                    min_total_revenue_by_subcategory,
                    min_active_revenue_by_nested_subcategory,
                    min_inactive_revenue_by_nested_subcategory,
                    min_total_revenue_by_nested_subcategory,
                    avg_active_revenue_by_category,
                    avg_inactive_revenue_by_category,
                    avg_total_revenue_by_category,
                    avg_active_revenue_by_subcategory,
                    avg_inactive_revenue_by_subcategory,
                    avg_total_revenue_by_subcategory,
                    avg_active_revenue_by_nested_subcategory,
                    avg_inactive_revenue_by_nested_subcategory,
                    avg_total_revenue_by_nested_subcategory,
                    max_active_revenue_by_category,
                    max_inactive_revenue_by_category,
                    max_total_revenue_by_category,
                    max_active_revenue_by_subcategory,
                    max_inactive_revenue_by_subcategory,
                    max_total_revenue_by_subcategory,
                    max_active_revenue_by_nested_subcategory,
                    max_inactive_revenue_by_nested_subcategory,
                    max_total_revenue_by_nested_subcategory,
                    min_total_historical_revenue_by_category,
                    min_total_historical_revenue_by_subcategory,
                    min_total_historical_revenue_by_nested_subcategory,
                    avg_total_historical_revenue_by_category,
                    avg_total_historical_revenue_by_subcategory,
                    avg_total_historical_revenue_by_nested_subcategory,
                    max_total_historical_revenue_by_category,
                    max_total_historical_revenue_by_subcategory,
                    max_total_historical_revenue_by_nested_subcategory,
                    revenue_growth_percent_by_category,
                    revenue_growth_percent_by_subcategory,
                    revenue_growth_percent_by_nested_subcategory,
                    trend_by_category as revenue_trend_by_category,
                    trend_by_subcategory as revenue_trend_by_subcategory,
                    trend_by_nested_subcategory as revenue_trend_by_nested_subcategory,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name
                from etl_agg.%1$s_metrics
                where
                    agg_period_start = %2$L::date;

        $exec$,
        tables_prefix,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_cat_id_cat_prefin
            on etl_wrk.categories_prefin (category_id) with (fillfactor = 100);
        create index if not exists idx_subcat_id_cat_prefin
            on etl_wrk.categories_prefin (sub_category_id) with (fillfactor = 100);
        create index if not exists idx_nested_subcat_id_cat_prefin
            on etl_wrk.categories_prefin (nested_sub_category_id) with (fillfactor = 100);
        create unique index if not exists uidx_id_cat_prefin
            on etl_wrk.categories_prefin (category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk.categories_prefin;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk' as tgt_schema,
            'categories_prefin' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            tables_prefix,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;