create or replace function etl_agg.agg_prepare_targets
(
    tables_prefix text,
    end_dttm timestamptz,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '1'
)
returns void
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        task_start_dttm timestamptz;
        run_start_dttm timestamptz;
        total_runtime interval;

        ddl_qry text;
        generating_partitions_qry text := '';
        partitions_qry text;
        partitioning_step text;
        root_tables_rec record;

    begin

        -- Starting

        run_start_dttm := clock_timestamp();

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            tables_prefix,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Executing targets' DDL

        task_start_dttm := clock_timestamp();

            -- Root tables

        ddl_qry := format($ddl$

                -- metrics

            create table if not exists etl_agg.%1$s_metrics
            (
                "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                processed_dttm timestamptz not null default transaction_timestamp(),
                seller_created_at timestamptz not null,
                seller_fiverr_created_at timestamptz null,
                seller_scraped_at timestamptz not null,
                gig_created_at timestamptz not null,
                gig_fiverr_created_at timestamptz null,
                gig_scraped_at timestamptz not null,
                agg_period_start date not null,
                seller_id int not null,
                fiverr_seller_id int not null,
                gig_id int not null,
                fiverr_gig_id int not null,
                category_id int not null,
                sub_category_id int not null,
                nested_sub_category_id int null,
                all_gigs_count_by_category int not null,
                all_gigs_count_by_subcategory int not null,
                all_gigs_count_by_nested_subcategory int null,
                regular_gigs_by_category int not null,
                regular_gigs_by_subcategory int not null,
                regular_gigs_by_nested_subcategory int null,
                pro_gigs_by_category int not null,
                pro_gigs_by_subcategory int not null,
                pro_gigs_by_nested_subcategory int null,
                seller_count_by_level_and_category int not null,
                seller_count_by_level_and_subcategory int not null,
                seller_count_by_level_and_nested_subcategory int null,
                gig_count_by_seller int not null,
                seller_ratings_count int not null,
                seller_completed_orders_count int not null,
                active_gigs_ratings_count_by_seller int not null,
                gig_ratings_count int not null,
                volume_by_seller int not null,
                volume_by_gig int not null,
                volume_by_category int not null,
                volume_by_subcategory int not null,
                volume_by_nested_subcategory int null,
                total_historical_volume_by_seller int not null,
                total_historical_volume_by_gig int not null,
                total_historical_volume_by_category int not null,
                total_historical_volume_by_subcategory int not null,
                total_historical_volume_by_nested_subcategory int null,
                heuristic bool not null,
                seller_is_pro bool not null,
                seller_is_active bool not null,
                gig_is_pro bool not null,
                gig_is_active bool not null,
                is_trend_valid_for_seller bool not null,
                is_trend_valid_for_gig bool not null,
                is_trend_valid_for_category bool not null,
                is_trend_valid_for_subcategory bool not null,
                is_trend_valid_for_nested_subcategory bool null,
                sales_volume_growth_percent_by_seller numeric not null,
                sales_volume_growth_percent_by_gig numeric not null,
                sales_volume_growth_percent_by_category numeric not null,
                sales_volume_growth_percent_by_subcategory numeric not null,
                sales_volume_growth_percent_by_nested_subcategory numeric null,
                competition_score_for_gig_by_category numeric not null,
                competition_score_for_gig_by_subcategory numeric not null,
                competition_score_for_gig_by_nested_subcategory numeric null,
                competition_score_for_category numeric not null,
                competition_score_for_subcategory numeric not null,
                competition_score_for_nested_subcategory numeric null,
                market_share_for_gig_by_category numeric not null,
                market_share_for_gig_by_subcategory numeric not null,
                market_share_for_gig_by_nested_subcategory numeric null,
                market_share_for_category numeric not null,
                market_share_for_subcategory numeric not null,
                market_share_for_nested_subcategory numeric null,
                seller_rating numeric not null,
                gig_rating numeric not null,
                min_price_by_seller numeric not null,
                min_price_by_gig numeric not null,
                min_price_by_category numeric not null,
                min_price_by_subcategory numeric not null,
                min_price_by_nested_subcategory numeric null,
                avg_price_by_seller numeric not null,
                avg_price_by_gig numeric not null,
                avg_price_by_category numeric not null,
                avg_price_by_subcategory numeric not null,
                avg_price_by_nested_subcategory numeric null,
                max_price_by_seller numeric not null,
                max_price_by_gig numeric not null,
                max_price_by_category numeric not null,
                max_price_by_subcategory numeric not null,
                max_price_by_nested_subcategory numeric null,
                weighted_min_price_by_seller numeric not null,
                weighted_min_price_by_category numeric not null,
                weighted_min_price_by_subcategory numeric not null,
                weighted_min_price_by_nested_subcategory numeric null,
                weighted_avg_price_by_seller numeric not null,
                weighted_avg_price_by_category numeric not null,
                weighted_avg_price_by_subcategory numeric not null,
                weighted_avg_price_by_nested_subcategory numeric null,
                weighted_max_price_by_seller numeric not null,
                weighted_max_price_by_category numeric not null,
                weighted_max_price_by_subcategory numeric not null,
                weighted_max_price_by_nested_subcategory numeric null,
                min_active_revenue_by_seller numeric not null,
                min_inactive_revenue_by_seller numeric not null,
                min_total_revenue_by_seller numeric not null,
                min_revenue_by_gig numeric not null,
                min_active_revenue_by_category numeric not null,
                min_inactive_revenue_by_category numeric not null,
                min_total_revenue_by_category numeric not null,
                min_active_revenue_by_subcategory numeric not null,
                min_inactive_revenue_by_subcategory numeric not null,
                min_total_revenue_by_subcategory numeric not null,
                min_active_revenue_by_nested_subcategory numeric null,
                min_inactive_revenue_by_nested_subcategory numeric null,
                min_total_revenue_by_nested_subcategory numeric null,
                avg_active_revenue_by_seller numeric not null,
                avg_inactive_revenue_by_seller numeric not null,
                avg_total_revenue_by_seller numeric not null,
                avg_revenue_by_gig numeric not null,
                avg_active_revenue_by_category numeric not null,
                avg_inactive_revenue_by_category numeric not null,
                avg_total_revenue_by_category numeric not null,
                avg_active_revenue_by_subcategory numeric not null,
                avg_inactive_revenue_by_subcategory numeric not null,
                avg_total_revenue_by_subcategory numeric not null,
                avg_active_revenue_by_nested_subcategory numeric null,
                avg_inactive_revenue_by_nested_subcategory numeric null,
                avg_total_revenue_by_nested_subcategory numeric null,
                max_active_revenue_by_seller numeric not null,
                max_inactive_revenue_by_seller numeric not null,
                max_total_revenue_by_seller numeric not null,
                max_revenue_by_gig numeric not null,
                max_active_revenue_by_category numeric not null,
                max_inactive_revenue_by_category numeric not null,
                max_total_revenue_by_category numeric not null,
                max_active_revenue_by_subcategory numeric not null,
                max_inactive_revenue_by_subcategory numeric not null,
                max_total_revenue_by_subcategory numeric not null,
                max_active_revenue_by_nested_subcategory numeric null,
                max_inactive_revenue_by_nested_subcategory numeric null,
                max_total_revenue_by_nested_subcategory numeric null,
                min_total_historical_revenue_by_seller numeric not null,
                min_total_historical_revenue_by_gig numeric not null,
                min_total_historical_revenue_by_category numeric not null,
                min_total_historical_revenue_by_subcategory numeric not null,
                min_total_historical_revenue_by_nested_subcategory numeric null,
                avg_total_historical_revenue_by_seller numeric not null,
                avg_total_historical_revenue_by_gig numeric not null,
                avg_total_historical_revenue_by_category numeric not null,
                avg_total_historical_revenue_by_subcategory numeric not null,
                avg_total_historical_revenue_by_nested_subcategory numeric null,
                max_total_historical_revenue_by_seller numeric not null,
                max_total_historical_revenue_by_gig numeric not null,
                max_total_historical_revenue_by_category numeric not null,
                max_total_historical_revenue_by_subcategory numeric not null,
                max_total_historical_revenue_by_nested_subcategory numeric null,
                revenue_growth_percent_by_seller numeric not null,
                revenue_growth_percent_by_gig numeric not null,
                revenue_growth_percent_by_category numeric not null,
                revenue_growth_percent_by_subcategory numeric not null,
                revenue_growth_percent_by_nested_subcategory numeric null,
                trend_by_seller numeric not null,
                r2_by_seller numeric not null,
                trends_avg_revenue_by_seller numeric not null,
                trend_by_gig numeric not null,
                r2_by_gig numeric not null,
                trends_avg_revenue_by_gig numeric not null,
                trend_by_category numeric not null,
                r2_by_category numeric not null,
                trends_avg_revenue_by_category numeric not null,
                trend_by_subcategory numeric not null,
                r2_by_subcategory numeric not null,
                trends_avg_revenue_by_subcategory numeric not null,
                trend_by_nested_subcategory numeric null,
                r2_by_nested_subcategory numeric null,
                trends_avg_revenue_by_nested_subcategory numeric null,
                rcrit numeric not null,
                seller_level text not null,
                seller_name text not null,
                agency_slug text null,
                agency_status text null,
                gig_title text null,
                gig_cached_slug text not null,
                category_name text not null,
                sub_category_name text not null,
                nested_sub_category_name text null,
                seller_country text null,
                seller_country_code text null,
                seller_languages text[] null,
                seller_profile_image text null,
                gig_preview_url text not null
            )
            partition by range (agg_period_start);

                -- gigs

            create table if not exists etl_agg.%1$s_gigs
            (
                "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                processed_dttm timestamptz not null default transaction_timestamp(),
                seller_created_at timestamptz not null,
                seller_fiverr_created_at timestamptz null,
                seller_scraped_at timestamptz not null,
                gig_created_at timestamptz not null,
                gig_fiverr_created_at timestamptz null,
                gig_scraped_at timestamptz not null,
                min_revenue bigint not null,
                avg_revenue bigint not null,
                max_revenue bigint not null,
                min_total_historical_revenue bigint not null,
                avg_total_historical_revenue bigint not null,
                max_total_historical_revenue bigint not null,
                agg_period_start date not null,
                seller_id int not null,
                fiverr_seller_id int not null,
                gig_id int not null,
                fiverr_gig_id int not null,
                category_id int not null,
                sub_category_id int not null,
                nested_sub_category_id int null,
                global_rank int not null,
                seller_ratings_count int not null,
                ratings_count int not null,
                sales_volume int not null,
                total_historical_sales_volume int not null,
                min_price int not null,
                avg_price int not null,
                max_price int not null,
                heuristic bool not null,
                is_pro bool not null,
                is_active bool not null,
                is_trend_valid bool not null,
                sales_volume_growth_percent numeric not null,
                competition_score_for_gig_by_category numeric not null,
                competition_score_for_gig_by_subcategory numeric not null,
                competition_score_for_gig_by_nested_subcategory numeric null,
                market_share_for_gig_by_category numeric not null,
                market_share_for_gig_by_subcategory numeric not null,
                market_share_for_gig_by_nested_subcategory numeric null,
                seller_rating numeric not null,
                rating numeric not null,
                revenue_growth_percent numeric not null,
                revenue_trend numeric not null,
                seller_level text not null,
                seller_name text not null,
                title text null,
                cached_slug text not null,
                category_name text not null,
                sub_category_name text not null,
                nested_sub_category_name text null,
                seller_country text null,
                seller_country_code text null,
                image_preview text not null,
                seller_profile_image text null
            )
            partition by range (agg_period_start);

                -- sellers

            create table if not exists etl_agg.%1$s_sellers
            (
                "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                processed_dttm timestamptz not null default transaction_timestamp(),
                created_at timestamptz not null,
                fiverr_created_at timestamptz null,
                scraped_at timestamptz not null,
                min_active_revenue bigint not null,
                min_inactive_revenue bigint not null,
                min_revenue bigint not null,
                avg_active_revenue bigint not null,
                avg_inactive_revenue bigint not null,
                avg_revenue bigint not null,
                max_active_revenue bigint not null,
                max_inactive_revenue bigint not null,
                max_revenue bigint not null,
                min_total_historical_revenue bigint not null,
                avg_total_historical_revenue bigint not null,
                max_total_historical_revenue bigint not null,
                best_selling_gig_revenue bigint not null,
                agg_period_start date not null,
                seller_id int not null,
                fiverr_seller_id int not null,
                best_selling_gig_id int null,
                best_selling_fiverr_gig_id int null,
                global_rank int not null,
                gig_count_by_seller int not null,
                ratings_count int not null,
                completed_orders_count int not null,
                active_gigs_ratings_count_by_seller int not null,
                sales_volume int not null,
                total_historical_sales_volume int not null,
                best_selling_gig_total_historical_volume int null,
                weighted_avg_gig_price int not null,
                is_pro bool not null,
                is_active bool not null,
                is_trend_valid bool not null,
                sales_volume_growth_percent numeric not null,
                rating numeric not null,
                revenue_growth_percent numeric not null,
                revenue_trend numeric not null,
                best_selling_gig_revenue_share numeric not null,
                best_selling_gig_title text null,
                best_selling_gig_cached_slug text null,
                "level" text not null,
                seller_name text not null,
                agency_slug text null,
                agency_status text null,
                country text null,
                country_code text null,
                languages text[] null,
                profile_image text null,
                best_selling_gig_image_preview text null
            )
            partition by range (agg_period_start);

                -- all_categories

            create table if not exists etl_agg.%1$s_all_categories
            (
                "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                processed_dttm timestamptz not null default transaction_timestamp(),
                min_active_revenue_by_category bigint not null,
                min_inactive_revenue_by_category bigint not null,
                min_revenue_by_category bigint not null,
                min_active_revenue_by_subcategory bigint not null,
                min_inactive_revenue_by_subcategory bigint not null,
                min_revenue_by_subcategory bigint not null,
                min_active_revenue_by_nested_subcategory bigint null,
                min_inactive_revenue_by_nested_subcategory bigint null,
                min_revenue_by_nested_subcategory bigint null,
                avg_active_revenue_by_category bigint not null,
                avg_inactive_revenue_by_category bigint not null,
                avg_revenue_by_category bigint not null,
                avg_active_revenue_by_subcategory bigint not null,
                avg_inactive_revenue_by_subcategory bigint not null,
                avg_revenue_by_subcategory bigint not null,
                avg_active_revenue_by_nested_subcategory bigint null,
                avg_inactive_revenue_by_nested_subcategory bigint null,
                avg_revenue_by_nested_subcategory bigint null,
                max_active_revenue_by_category bigint not null,
                max_inactive_revenue_by_category bigint not null,
                max_revenue_by_category bigint not null,
                max_active_revenue_by_subcategory bigint not null,
                max_inactive_revenue_by_subcategory bigint not null,
                max_revenue_by_subcategory bigint not null,
                max_active_revenue_by_nested_subcategory bigint null,
                max_inactive_revenue_by_nested_subcategory bigint null,
                max_revenue_by_nested_subcategory bigint null,
                min_total_historical_revenue_by_category bigint not null,
                min_total_historical_revenue_by_subcategory bigint not null,
                min_total_historical_revenue_by_nested_subcategory bigint null,
                avg_total_historical_revenue_by_category bigint not null,
                avg_total_historical_revenue_by_subcategory bigint not null,
                avg_total_historical_revenue_by_nested_subcategory bigint null,
                max_total_historical_revenue_by_category bigint not null,
                max_total_historical_revenue_by_subcategory bigint not null,
                max_total_historical_revenue_by_nested_subcategory bigint null,
                agg_period_start date not null,
                id int not null,
                category_id int not null,
                sub_category_id int not null,
                nested_sub_category_id int null,
                global_rank_for_category int not null,
                global_rank_for_subcategory int not null,
                global_rank_for_nested_subcategory int null,
                all_gigs_count_by_category int not null,
                all_gigs_count_by_subcategory int not null,
                all_gigs_count_by_nested_subcategory int null,
                regular_gigs_count_by_category int not null,
                regular_gigs_count_by_subcategory int not null,
                regular_gigs_count_by_nested_subcategory int null,
                pro_gigs_count_by_category int not null,
                pro_gigs_count_by_subcategory int not null,
                pro_gigs_count_by_nested_subcategory int null,
                seller_count_by_category int not null,
                seller_count_by_subcategory int not null,
                seller_count_by_nested_subcategory int null,
                sales_volume_by_category int not null,
                sales_volume_by_subcategory int not null,
                sales_volume_by_nested_subcategory int null,
                total_historical_sales_volume_by_category int not null,
                total_historical_sales_volume_by_subcategory int not null,
                total_historical_sales_volume_by_nested_subcategory int null,
                weighted_avg_gig_price_by_category int not null,
                weighted_avg_gig_price_by_subcategory int not null,
                weighted_avg_gig_price_by_nested_subcategory int null,
                is_trend_valid_for_category bool not null,
                is_trend_valid_for_subcategory bool not null,
                is_trend_valid_for_nested_subcategory bool null,
                sales_volume_growth_percent_by_category numeric not null,
                sales_volume_growth_percent_by_subcategory numeric not null,
                sales_volume_growth_percent_by_nested_subcategory numeric null,
                competition_score_for_category numeric not null,
                competition_score_for_subcategory numeric not null,
                competition_score_for_nested_subcategory numeric null,
                market_share_for_category numeric not null,
                market_share_for_subcategory numeric not null,
                market_share_for_nested_subcategory numeric null,
                revenue_growth_percent_by_category numeric not null,
                revenue_growth_percent_by_subcategory numeric not null,
                revenue_growth_percent_by_nested_subcategory numeric null,
                revenue_trend_by_category numeric not null,
                revenue_trend_by_subcategory numeric not null,
                revenue_trend_by_nested_subcategory numeric null,
                category_name text not null,
                sub_category_name text not null,
                nested_sub_category_name text null
            )
            partition by range (agg_period_start);

                -- categories

            create table if not exists etl_agg.%1$s_categories
            (
                "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                processed_dttm timestamptz not null default transaction_timestamp(),
                min_active_revenue bigint not null,
                min_inactive_revenue bigint not null,
                min_revenue bigint not null,
                avg_active_revenue bigint not null,
                avg_inactive_revenue bigint not null,
                avg_revenue bigint not null,
                max_active_revenue bigint not null,
                max_inactive_revenue bigint not null,
                max_revenue bigint not null,
                min_total_historical_revenue bigint not null,
                avg_total_historical_revenue bigint not null,
                max_total_historical_revenue bigint not null,
                agg_period_start date not null,
                category_id int not null,
                global_rank int not null,
                all_gigs_count int not null,
                regular_gigs_count int not null,
                pro_gigs_count int not null,
                seller_count int not null,
                sales_volume int not null,
                total_historical_sales_volume int not null,
                weighted_avg_gig_price int not null,
                is_trend_valid bool not null,
                sales_volume_growth_percent numeric not null,
                competition_score numeric not null,
                market_share numeric not null,
                revenue_growth_percent numeric not null,
                revenue_trend numeric not null,
                category_name text not null
            )
            partition by range (agg_period_start);

                -- subcategories

            create table if not exists etl_agg.%1$s_subcategories
            (
                "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                processed_dttm timestamptz not null default transaction_timestamp(),
                min_active_revenue bigint not null,
                min_inactive_revenue bigint not null,
                min_revenue bigint not null,
                avg_active_revenue bigint not null,
                avg_inactive_revenue bigint not null,
                avg_revenue bigint not null,
                max_active_revenue bigint not null,
                max_inactive_revenue bigint not null,
                max_revenue bigint not null,
                min_total_historical_revenue bigint not null,
                avg_total_historical_revenue bigint not null,
                max_total_historical_revenue bigint not null,
                agg_period_start date not null,
                sub_category_id int not null,
                global_rank int not null,
                all_gigs_count int not null,
                regular_gigs_count int not null,
                pro_gigs_count int not null,
                seller_count int not null,
                sales_volume int not null,
                total_historical_sales_volume int not null,
                weighted_avg_gig_price int not null,
                is_trend_valid bool not null,
                sales_volume_growth_percent numeric not null,
                competition_score numeric not null,
                market_share numeric not null,
                revenue_growth_percent numeric not null,
                revenue_trend numeric not null,
                sub_category_name text not null
            )
            partition by range (agg_period_start);

                -- nested_subcategories

            create table if not exists etl_agg.%1$s_nested_subcategories
            (
                "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                processed_dttm timestamptz not null default transaction_timestamp(),
                min_active_revenue bigint not null,
                min_inactive_revenue bigint not null,
                min_revenue bigint not null,
                avg_active_revenue bigint not null,
                avg_inactive_revenue bigint not null,
                avg_revenue bigint not null,
                max_active_revenue bigint not null,
                max_inactive_revenue bigint not null,
                max_revenue bigint not null,
                min_total_historical_revenue bigint not null,
                avg_total_historical_revenue bigint not null,
                max_total_historical_revenue bigint not null,
                agg_period_start date not null,
                nested_sub_category_id int not null,
                global_rank int not null,
                all_gigs_count int not null,
                regular_gigs_count int not null,
                pro_gigs_count int not null,
                seller_count int not null,
                sales_volume int not null,
                total_historical_sales_volume int not null,
                weighted_avg_gig_price int not null,
                is_trend_valid bool not null,
                sales_volume_growth_percent numeric not null,
                competition_score numeric not null,
                market_share numeric not null,
                revenue_growth_percent numeric not null,
                revenue_trend numeric not null,
                nested_sub_category_name text not null
            )
            partition by range (agg_period_start);

                -- category_x_seller_levels

            create table if not exists etl_agg.%1$s_category_x_seller_levels
            (
                "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                processed_dttm timestamptz not null default transaction_timestamp(),
                agg_period_start date not null,
                id int not null,
                category_id int not null,
                seller_count int not null,
                seller_level text not null,
                category_name text not null
            )
            partition by range (agg_period_start);

                -- subcategory_x_seller_levels

            create table if not exists etl_agg.%1$s_subcategory_x_seller_levels
            (
                "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                processed_dttm timestamptz not null default transaction_timestamp(),
                agg_period_start date not null,
                id int not null,
                sub_category_id int not null,
                seller_count int not null,
                seller_level text not null,
                sub_category_name text not null
            )
            partition by range (agg_period_start);

                -- nested_subcategory_x_seller_levels

            create table if not exists etl_agg.%1$s_nested_subcategory_x_seller_levels
            (
                "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                processed_dttm timestamptz not null default transaction_timestamp(),
                agg_period_start date not null,
                id int not null,
                nested_sub_category_id int not null,
                seller_count int not null,
                seller_level text not null,
                nested_sub_category_name text not null
            )
            partition by range (agg_period_start);

        $ddl$,
        tables_prefix
        );

            -- Partitions

        select
            case
                when tables_prefix = 'quarterly' then '3 month'
                else '1 ' || replace(tables_prefix, 'ly', '')
            end
        into partitioning_step;

        <<partitioning_loop>>
        for root_tables_rec in
            select
                root_table_name
            from unnest
                (
                    array[
                        'metrics',
                        'gigs',
                        'sellers',
                        'all_categories',
                        'categories',
                        'subcategories',
                        'nested_subcategories',
                        'category_x_seller_levels',
                        'subcategory_x_seller_levels',
                        'nested_subcategory_x_seller_levels'
                    ]::text[]
                ) u (root_table_name)
        loop

            generating_partitions_qry := generating_partitions_qry
                || 'select' || E'\n' || repeat(E'\t', 2) || 'partition_ddl' || E'\n\t'
                || 'from public.get_range_partitions_ddl' || E'\n' || repeat(E'\t', 2) || '(' || E'\n' || repeat(E'\t', 3)
                || 'root_table_oid := (''etl_agg.' || tables_prefix || '_' || root_tables_rec.root_table_name || ''')::regclass,' || E'\n' || repeat(E'\t', 3)
                || 'split_point := date_trunc(replace(''' || tables_prefix || ''', ''ly'', ''''), ''' || end_dttm || '''::date)::text,' || E'\n' || repeat(E'\t', 3)
                || 'split_interval := ''' || partitioning_step || '''::text,' || E'\n' || repeat(E'\t', 3)
                || 'merge_windows := array[''' || partitioning_step || ''']::text[],' || E'\n' || repeat(E'\t', 3)
                || 'merge_intervals := array[''' || partitioning_step || ''']::text[],' || E'\n' || repeat(E'\t', 3)
                || 'split_amount := 2::smallint,' || E'\n' || repeat(E'\t', 3)
                || 'archive_partition_flg := false,' || E'\n' || repeat(E'\t', 3)
                || 'split_unbounded_flg := false,' || E'\n' || repeat(E'\t', 3)
                || 'default_partition_flg := false' || E'\n' || repeat(E'\t', 2)
                || ')' || repeat(E'\n', 2) || E'\t'
                || 'union all' || repeat(E'\n', 2) || E'\t';

        end loop partitioning_loop;

        generating_partitions_qry := 'select' || E'\n\t' || 'string_agg(partition_ddl, E''\n\n'') as part_ddl' || E'\n'
            || 'from' || E'\n' || '(' || E'\n\t'
            || rtrim(generating_partitions_qry, repeat(E'\n', 2) || E'\t' || 'union all' || repeat(E'\n', 2) || E'\t')
            || E'\n' || ') a' || E'\n' || ';';

            -- Executing generated queries

        execute ddl_qry;

        execute generating_partitions_qry
           into partitions_qry;

        execute partitions_qry;

            -- Logging

        select
            high_level_step || '.1' as src_step,
            'etl_agg' as tgt_schema,
            null as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (target tables have been created in schema ' || log_rec.tgt_schema || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Exiting

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - run_start_dttm;

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            tables_prefix,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

    end;

$func$;