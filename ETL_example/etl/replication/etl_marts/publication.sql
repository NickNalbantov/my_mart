-- publisher

grant rds_replication to etl_read_only;

    -- Main target marts only

drop publication if exists etl_marts cascade;

create publication etl_marts for table
    etl.gigs,
    etl.sellers,
    etl.all_categories,
    etl.categories,
    etl.subcategories,
    etl.nested_subcategories,
    etl.category_x_seller_levels,
    etl.subcategory_x_seller_levels,
    etl.nested_subcategory_x_seller_levels
with
(
    publish_via_partition_root = true
);