-- DDL for etl_marts subscription

create schema if not exists etl;
create extension if not exists pg_trgm;

drop table if exists etl.gigs cascade;
drop table if exists etl.sellers cascade;
drop table if exists etl.all_categories cascade;
drop table if exists etl.categories cascade;
drop table if exists etl.subcategories cascade;
drop table if exists etl.nested_subcategories cascade;
drop table if exists etl.category_x_seller_levels cascade;
drop table if exists etl.subcategory_x_seller_levels cascade;
drop table if exists etl.nested_subcategory_x_seller_levels cascade;

    -- gigs

create table if not exists etl.gigs
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    seller_created_at timestamptz not null,
    seller_fiverr_created_at timestamptz null,
    seller_scraped_at timestamptz not null,
    gig_created_at timestamptz not null,
    gig_fiverr_created_at timestamptz null,
    gig_scraped_at timestamptz not null,
    min_total_historical_revenue bigint not null,
    avg_total_historical_revenue bigint not null,
    max_total_historical_revenue bigint not null,
    seller_id int not null,
    fiverr_seller_id int not null,
    gig_id int not null,
    fiverr_gig_id int not null,
    category_id int not null,
    sub_category_id int not null,
    nested_sub_category_id int null,
    global_rank int not null,
    seller_ratings_count int not null,
    ratings_count int not null,
    sales_volume int not null,
    total_historical_sales_volume int not null,
    min_price int not null,
    avg_price int not null,
    max_price int not null,
    min_revenue int not null,
    avg_revenue int not null,
    max_revenue int not null,
    heuristic bool not null,
    is_pro bool not null,
    is_active bool not null,
    is_trend_valid bool not null,
    sales_volume_growth_percent numeric not null,
    competition_score_for_gig_by_category numeric not null,
    competition_score_for_gig_by_subcategory numeric not null,
    competition_score_for_gig_by_nested_subcategory numeric null,
    market_share_for_gig_by_category numeric not null,
    market_share_for_gig_by_subcategory numeric not null,
    market_share_for_gig_by_nested_subcategory numeric null,
    seller_rating numeric not null,
    rating numeric not null,
    revenue_growth_percent numeric not null,
    revenue_trend numeric not null,
    seller_level text not null,
    seller_name text not null,
    title text null,
    cached_slug text not null,
    category_name text not null,
    sub_category_name text not null,
    nested_sub_category_name text null,
    seller_country text null,
    seller_country_code text null,
    image_preview text not null,
    seller_profile_image text null
);

alter table if exists etl.gigs add primary key (gig_id);
create unique index if not exists uidx_fiverr_gig_id_gigs on etl.gigs (fiverr_gig_id);
create index if not exists idx_seller_id_gigs on etl.gigs (seller_id);
create index if not exists idx_fiverr_seller_id_gigs on etl.gigs (fiverr_seller_id);
create index if not exists idx_category_id_gigs on etl.gigs (category_id);
create index if not exists idx_sub_category_id_gigs on etl.gigs (sub_category_id);
create index if not exists idx_nested_sub_category_id_gigs on etl.gigs (nested_sub_category_id);
create index if not exists idx_categories_gigs on etl.gigs (category_id, sub_category_id, nested_sub_category_id);
create unique index if not exists uidx_gigs_ranks on etl.gigs (global_rank desc);
create index if not exists idx_cached_slug_gigs on etl.gigs using hash (cached_slug);
create index if not exists idx_trg_title_gigs on etl.gigs using gin (title gin_trgm_ops);
create index if not exists idx_trg_seller_name_gigs on etl.gigs using gin (seller_name gin_trgm_ops);
create index if not exists idx_trg_cat_name_gigs on etl.gigs using gin (category_name gin_trgm_ops);
create index if not exists idx_trg_subcat_name_gigs on etl.gigs using gin (sub_category_name gin_trgm_ops);
create index if not exists idx_trg_nest_subcat_name_gigs on etl.gigs using gin (nested_sub_category_name gin_trgm_ops);

    -- sellers

create table if not exists etl.sellers
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    created_at timestamptz not null,
    fiverr_created_at timestamptz null,
    scraped_at timestamptz not null,
    min_total_historical_revenue bigint not null,
    avg_total_historical_revenue bigint not null,
    max_total_historical_revenue bigint not null,
    seller_id int not null,
    fiverr_seller_id int not null,
    best_selling_gig_id int null,
    best_selling_fiverr_gig_id int null,
    global_rank int not null,
    gig_count_by_seller int not null,
    ratings_count int not null,
    completed_orders_count int not null,
    active_gigs_ratings_count_by_seller int not null,
    sales_volume int not null,
    total_historical_sales_volume int not null,
    best_selling_gig_total_historical_volume int null,
    weighted_avg_gig_price int not null,
    min_active_revenue int not null,
    min_inactive_revenue int not null,
    min_revenue int not null,
    avg_active_revenue int not null,
    avg_inactive_revenue int not null,
    avg_revenue int not null,
    max_active_revenue int not null,
    max_inactive_revenue int not null,
    max_revenue int not null,
    best_selling_gig_revenue int not null,
    is_pro bool not null,
    is_active bool not null,
    is_trend_valid bool not null,
    sales_volume_growth_percent numeric not null,
    rating numeric not null,
    revenue_growth_percent numeric not null,
    revenue_trend numeric not null,
    best_selling_gig_revenue_share numeric not null,
    best_selling_gig_title text null,
    best_selling_gig_cached_slug text null,
    "level" text not null,
    seller_name text not null,
    agency_slug text null,
    agency_status text null,
    country text null,
    country_code text null,
    languages text[] null,
    profile_image text null,
    best_selling_gig_image_preview text null
);

alter table if exists etl.sellers add primary key (seller_id) include (is_active);
create unique index if not exists uidx_fiverr_seller_id_sellers on etl.sellers (fiverr_seller_id);
create index if not exists idx_sellers_best_selling_gig_id on etl.sellers (best_selling_gig_id);
create unique index if not exists uidx_sellers_ranks on etl.sellers (global_rank desc);
create index if not exists idx_trg_seller_name_sellers on etl.sellers using gin (seller_name gin_trgm_ops);

    -- all_categories

create table if not exists etl.all_categories
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    min_total_historical_revenue_by_category bigint not null,
    min_total_historical_revenue_by_subcategory bigint not null,
    min_total_historical_revenue_by_nested_subcategory bigint null,
    avg_total_historical_revenue_by_category bigint not null,
    avg_total_historical_revenue_by_subcategory bigint not null,
    avg_total_historical_revenue_by_nested_subcategory bigint null,
    max_total_historical_revenue_by_category bigint not null,
    max_total_historical_revenue_by_subcategory bigint not null,
    max_total_historical_revenue_by_nested_subcategory bigint null,
    id int not null,
    category_id int not null,
    sub_category_id int not null,
    nested_sub_category_id int null,
    global_rank_for_category int not null,
    global_rank_for_subcategory int not null,
    global_rank_for_nested_subcategory int null,
    all_gigs_count_by_category int not null,
    all_gigs_count_by_subcategory int not null,
    all_gigs_count_by_nested_subcategory int null,
    regular_gigs_count_by_category int not null,
    regular_gigs_count_by_subcategory int not null,
    regular_gigs_count_by_nested_subcategory int null,
    pro_gigs_count_by_category int not null,
    pro_gigs_count_by_subcategory int not null,
    pro_gigs_count_by_nested_subcategory int null,
    seller_count_by_category int not null,
    seller_count_by_subcategory int not null,
    seller_count_by_nested_subcategory int null,
    sales_volume_by_category int not null,
    sales_volume_by_subcategory int not null,
    sales_volume_by_nested_subcategory int null,
    total_historical_sales_volume_by_category int not null,
    total_historical_sales_volume_by_subcategory int not null,
    total_historical_sales_volume_by_nested_subcategory int null,
    weighted_avg_gig_price_by_category int not null,
    weighted_avg_gig_price_by_subcategory int not null,
    weighted_avg_gig_price_by_nested_subcategory int null,
    min_active_revenue_by_category int not null,
    min_inactive_revenue_by_category int not null,
    min_revenue_by_category int not null,
    min_active_revenue_by_subcategory int not null,
    min_inactive_revenue_by_subcategory int not null,
    min_revenue_by_subcategory int not null,
    min_active_revenue_by_nested_subcategory int null,
    min_inactive_revenue_by_nested_subcategory int null,
    min_revenue_by_nested_subcategory int null,
    avg_active_revenue_by_category int not null,
    avg_inactive_revenue_by_category int not null,
    avg_revenue_by_category int not null,
    avg_active_revenue_by_subcategory int not null,
    avg_inactive_revenue_by_subcategory int not null,
    avg_revenue_by_subcategory int not null,
    avg_active_revenue_by_nested_subcategory int null,
    avg_inactive_revenue_by_nested_subcategory int null,
    avg_revenue_by_nested_subcategory int null,
    max_active_revenue_by_category int not null,
    max_inactive_revenue_by_category int not null,
    max_revenue_by_category int not null,
    max_active_revenue_by_subcategory int not null,
    max_inactive_revenue_by_subcategory int not null,
    max_revenue_by_subcategory int not null,
    max_active_revenue_by_nested_subcategory int null,
    max_inactive_revenue_by_nested_subcategory int null,
    max_revenue_by_nested_subcategory int null,
    is_trend_valid_for_category bool not null,
    is_trend_valid_for_subcategory bool not null,
    is_trend_valid_for_nested_subcategory bool null,
    sales_volume_growth_percent_by_category numeric not null,
    sales_volume_growth_percent_by_subcategory numeric not null,
    sales_volume_growth_percent_by_nested_subcategory numeric null,
    competition_score_for_category numeric not null,
    competition_score_for_subcategory numeric not null,
    competition_score_for_nested_subcategory numeric null,
    market_share_for_category numeric not null,
    market_share_for_subcategory numeric not null,
    market_share_for_nested_subcategory numeric null,
    revenue_growth_percent_by_category numeric not null,
    revenue_growth_percent_by_subcategory numeric not null,
    revenue_growth_percent_by_nested_subcategory numeric null,
    revenue_trend_by_category numeric not null,
    revenue_trend_by_subcategory numeric not null,
    revenue_trend_by_nested_subcategory numeric null,
    category_name text not null,
    sub_category_name text not null,
    nested_sub_category_name text null
);

alter table if exists etl.all_categories add primary key (id);
create unique index if not exists uidx_all_id_all_cats on etl.all_categories (category_id, sub_category_id, nested_sub_category_id);
create index if not exists idx_cat_id_all_cats on etl.all_categories (category_id);
create index if not exists idx_sub_cat_id_all_cats on etl.all_categories (sub_category_id);
create index if not exists idx_nest_sub_cat_id_all_cats on etl.all_categories (nested_sub_category_id);
create index if not exists idx_trg_cat_name_all_cats on etl.all_categories using gin (category_name gin_trgm_ops);
create index if not exists idx_trg_subcat_name_all_cats on etl.all_categories using gin (sub_category_name gin_trgm_ops);
create index if not exists idx_trg_nest_subcat_name_all_cats on etl.all_categories using gin (nested_sub_category_name gin_trgm_ops);

    -- categories

create table if not exists etl.categories
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    min_total_historical_revenue bigint not null,
    avg_total_historical_revenue bigint not null,
    max_total_historical_revenue bigint not null,
    category_id int not null,
    global_rank int not null,
    all_gigs_count int not null,
    regular_gigs_count int not null,
    pro_gigs_count int not null,
    seller_count int not null,
    sales_volume int not null,
    total_historical_sales_volume int not null,
    weighted_avg_gig_price int not null,
    min_active_revenue int not null,
    min_inactive_revenue int not null,
    min_revenue int not null,
    avg_active_revenue int not null,
    avg_inactive_revenue int not null,
    avg_revenue int not null,
    max_active_revenue int not null,
    max_inactive_revenue int not null,
    max_revenue int not null,
    is_trend_valid bool not null,
    sales_volume_growth_percent numeric not null,
    competition_score numeric not null,
    market_share numeric not null,
    revenue_growth_percent numeric not null,
    revenue_trend numeric not null,
    category_name text not null
);

alter table if exists etl.categories add primary key (category_id);
create unique index if not exists uidx_categories_ranks on etl.categories (global_rank desc);
create index if not exists idx_trg_cat_name_cat on etl.categories using gin (category_name gin_trgm_ops);

    -- subcategories

create table if not exists etl.subcategories
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    min_total_historical_revenue bigint not null,
    avg_total_historical_revenue bigint not null,
    max_total_historical_revenue bigint not null,
    sub_category_id int not null,
    global_rank int not null,
    all_gigs_count int not null,
    regular_gigs_count int not null,
    pro_gigs_count int not null,
    seller_count int not null,
    sales_volume int not null,
    total_historical_sales_volume int not null,
    weighted_avg_gig_price int not null,
    min_active_revenue int not null,
    min_inactive_revenue int not null,
    min_revenue int not null,
    avg_active_revenue int not null,
    avg_inactive_revenue int not null,
    avg_revenue int not null,
    max_active_revenue int not null,
    max_inactive_revenue int not null,
    max_revenue int not null,
    is_trend_valid bool not null,
    sales_volume_growth_percent numeric not null,
    competition_score numeric not null,
    market_share numeric not null,
    revenue_growth_percent numeric not null,
    revenue_trend numeric not null,
    sub_category_name text not null
);

alter table if exists etl.subcategories add primary key (sub_category_id);
create unique index if not exists uidx_subcategories_ranks on etl.subcategories (global_rank desc);
create index if not exists idx_trg_subcat_name_subcat on etl.subcategories using gin (sub_category_name gin_trgm_ops);

    -- nested_subcategories

create table if not exists etl.nested_subcategories
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    min_total_historical_revenue bigint not null,
    avg_total_historical_revenue bigint not null,
    max_total_historical_revenue bigint not null,
    nested_sub_category_id int not null,
    global_rank int not null,
    all_gigs_count int not null,
    regular_gigs_count int not null,
    pro_gigs_count int not null,
    seller_count int not null,
    sales_volume int not null,
    total_historical_sales_volume int not null,
    weighted_avg_gig_price int not null,
    min_active_revenue int not null,
    min_inactive_revenue int not null,
    min_revenue int not null,
    avg_active_revenue int not null,
    avg_inactive_revenue int not null,
    avg_revenue int not null,
    max_active_revenue int not null,
    max_inactive_revenue int not null,
    max_revenue int not null,
    is_trend_valid bool not null,
    sales_volume_growth_percent numeric not null,
    competition_score numeric not null,
    market_share numeric not null,
    revenue_growth_percent numeric not null,
    revenue_trend numeric not null,
    nested_sub_category_name text not null
);

alter table if exists etl.nested_subcategories add primary key (nested_sub_category_id);
create unique index if not exists uidx_nested_subcategories_ranks on etl.nested_subcategories (global_rank desc);
create index if not exists idx_trg_nest_subcat_name_nest_subcat on etl.nested_subcategories using gin (nested_sub_category_name gin_trgm_ops);

    -- category_x_seller_levels

create table if not exists etl.category_x_seller_levels
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    id int not null,
    category_id int not null,
    seller_count int not null,
    seller_level text not null,
    category_name text not null
);

alter table if exists etl.category_x_seller_levels add primary key (id);
create unique index if not exists uidx_cat_sel_lvl on etl.category_x_seller_levels (category_id, seller_level);
create index if not exists idx_trg_cat_name_cat_sel_lvl on etl.category_x_seller_levels using gin (category_name gin_trgm_ops);

    -- subcategory_x_seller_levels

create table if not exists etl.subcategory_x_seller_levels
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    id int not null,
    sub_category_id int not null,
    seller_count int not null,
    seller_level text not null,
    sub_category_name text not null
);

alter table if exists etl.subcategory_x_seller_levels add primary key (id);
create unique index if not exists uidx_subcat_sel_lvl on etl.subcategory_x_seller_levels (sub_category_id, seller_level);
create index if not exists idx_trg_subcat_name_subcat_sel_lvl on etl.subcategory_x_seller_levels using gin (sub_category_name gin_trgm_ops);

    -- nested_subcategory_x_seller_levels

create table if not exists etl.nested_subcategory_x_seller_levels
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    id int not null,
    nested_sub_category_id int not null,
    seller_count int not null,
    seller_level text not null,
    nested_sub_category_name text not null
);

alter table if exists etl.nested_subcategory_x_seller_levels add primary key (id);
create unique index if not exists uidx_nest_subcat_sel_lvl on etl.nested_subcategory_x_seller_levels (nested_sub_category_id, seller_level);
create index if not exists idx_trg_nest_subcat_name_nest_subcat_sel_lvl on etl.nested_subcategory_x_seller_levels using gin (nested_sub_category_name gin_trgm_ops);


-- Direct subscription
    -- Replace PWD with role password and SSLROOTCERTPATH with a full local path to certificate file

    -- Link to download AWS RDS SSL certificate bundle - https://truststore.pki.rds.amazonaws.com/eu-central-1/eu-central-1-bundle.pem
    -- Root certificate must be placed in a local folder accessible by PostgreSQL
        -- For example, use local PG data directory (can be found with SQL command on replica ```show data_directory;```)

drop subscription if exists etl_marts cascade;

create subscription etl_marts
    connection 'host=db-raw-1.cr82xjq3sz3n.eu-central-1.rds.amazonaws.com port=5432 user=etl_read_only dbname=raw_1 password=PWD sslrootcert=SSLROOTCERTPATH sslmode=verify-full'
    publication etl_marts;


-- Proxy subscription
    -- Replace PWD with role password

drop subscription if exists etl_marts cascade;

create subscription etl_marts
    connection 'host=etl-proxy.proxy-cr82xjq3sz3n.eu-central-1.rds.amazonaws.com port=5432 user=etl_read_only dbname=raw_1 password=PWD sslmode=require'
    publication etl_marts;