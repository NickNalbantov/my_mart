-- publisher

grant rds_replication to etl_read_only;

    -- All tables from schema etl

drop publication if exists etl cascade;

create publication etl for tables in schema etl
    with
    (
        publish_via_partition_root = true
    );