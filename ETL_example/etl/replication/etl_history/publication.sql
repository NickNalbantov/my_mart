-- publisher

grant rds_replication to etl_read_only;

    -- Status history and latest review tables

drop publication if exists etl_history cascade;

create publication etl_history for table
    etl.gig_status_history,
    etl.seller_status_history,
    etl.gig_reviews_agg
with
(
    publish_via_partition_root = true
);