-- DDL for etl_history subscription

create schema if not exists etl;

drop table if exists etl.gig_status_history cascade;
drop table if exists etl.seller_status_history cascade;
drop table if exists etl.gig_reviews_agg cascade;
drop table if exists etl.gig_reviews_prices cascade;

drop view if exists etl.gig_current_status cascade;
drop view if exists etl.seller_current_status cascade;

    -- gig status history

create table if not exists etl.gig_status_history
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    valid_from_dttm timestamptz not null,
    valid_to_dttm timestamptz not null,
    gig_id int not null,
    fiverr_status text not null
);

create unique index if not exists uidx_gig_status_history_valid_from on etl.gig_status_history (gig_id, valid_from_dttm);
create index if not exists idx_gig_status_history_gig_id_valid_to on etl.gig_status_history (gig_id, valid_to_dttm);
create index if not exists idx_gig_status_history_valid_from on etl.gig_status_history (valid_from_dttm desc);
create index if not exists idx_gig_status_history_valid_to on etl.gig_status_history (valid_to_dttm desc);
create index if not exists idx_gig_status_history_load_id on etl.gig_status_history ("load_id" desc);

    -- gig current status view

create or replace view etl.gig_current_status as
    select
        gig_id,
        fiverr_status,
        valid_from_dttm
    from etl.gig_status_history
    where
        valid_to_dttm = '2999-12-31 00:00:00 UTC'::timestamptz;

    -- seller status history

create table if not exists etl.seller_status_history
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    valid_from_dttm timestamptz not null,
    valid_to_dttm timestamptz not null,
    seller_id int not null,
    status text not null
);

create unique index if not exists uidx_seller_status_history_valid_from on etl.seller_status_history (seller_id, valid_from_dttm);
create index if not exists idx_seller_status_history_seller_id_valid_to on etl.seller_status_history (seller_id, valid_to_dttm);
create index if not exists idx_seller_status_history_valid_from on etl.seller_status_history (valid_from_dttm desc);
create index if not exists idx_seller_status_history_valid_to on etl.seller_status_history (valid_to_dttm desc);
create index if not exists idx_seller_status_history_load_id on etl.seller_status_history ("load_id" desc);

    -- seller current status view

create or replace view etl.seller_current_status as
    select
        seller_id,
        status,
        valid_from_dttm
    from etl.seller_status_history
    where
        valid_to_dttm = '2999-12-31 00:00:00 UTC'::timestamptz;

    -- gig_reviews_agg

create table if not exists etl.gig_reviews_agg
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    last_fiverr_created_at timestamptz,
    gig_id int primary key,
    last_review_id int not null,
    reviews_count int not null,
    last_fiverr_review_id text not null
);

create unique index if not exists uidx_gig_reviews_agg_gig_id_last_fiverr_created_at on etl.gig_reviews_agg (gig_id, last_fiverr_created_at desc);
create index if not exists idx_gig_reviews_agg_last_fiverr_created_at on etl.gig_reviews_agg (last_fiverr_created_at desc);
create index if not exists idx_gig_reviews_agg_load_id on etl.gig_reviews_agg ("load_id" desc);

    -- gig_reviews_prices

create table if not exists etl.gig_reviews_prices
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    fiverr_created_at timestamptz not null,
    id int not null,
    gig_id int not null,
    price_range_start numeric null default 0::numeric,
    price_range_end numeric not null default 'infinity'::numeric
);

create unique index if not exists uidx_gig_reviews_prices_id on etl.gig_reviews_prices (id);
create index if not exists idx_gig_reviews_prices_fiverr_created_at on etl.gig_reviews_prices (fiverr_created_at desc);
create index if not exists idx_gig_reviews_prices_gig_id on etl.gig_reviews_prices (gig_id);
create index if not exists idx_gig_reviews_prices_load_id on etl.gig_reviews_prices ("load_id" desc);


-- Direct subscription
    -- Replace PWD with role password and SSLROOTCERTPATH with a full local path to certificate file

    -- Link to download AWS RDS SSL certificate bundle - https://truststore.pki.rds.amazonaws.com/eu-central-1/eu-central-1-bundle.pem
    -- Root certificate must be placed in a local folder accessible by PostgreSQL
        -- For example, use local PG data directory (can be found with SQL command on replica ```show data_directory;```)

drop subscription if exists etl_history cascade;

create subscription etl_history
    connection 'host=db-raw-1.cr82xjq3sz3n.eu-central-1.rds.amazonaws.com port=5432 user=etl_read_only dbname=raw_1 password=PWD sslrootcert=SSLROOTCERTPATH sslmode=verify-full'
    publication etl_history;


-- Proxy subscription
    -- Replace PWD with role password

drop subscription if exists etl_history cascade;

create subscription etl_history
    connection 'host=etl-proxy.proxy-cr82xjq3sz3n.eu-central-1.rds.amazonaws.com port=5432 user=etl_read_only dbname=raw_1 password=PWD sslmode=require'
    publication etl_history;