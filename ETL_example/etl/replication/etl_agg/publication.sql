-- publisher

grant rds_replication to etl_read_only;

    -- ETL aggregations

drop publication if exists etl_agg cascade;

create publication etl_agg for tables in schema etl_agg
    with
    (
        publish_via_partition_root = true
    );