-- DDL for etl_agg subscription

create schema if not exists etl_agg;
create extension if not exists pg_trgm;


-- Function for dynamic generation of tables DDL for aggregated ETL

create or replace function etl_agg.generate_agg_ddl
(
    agg_period_json jsonb,
    dry_run boolean default false
)
returns text
language plpgsql
strict
as
$func$
    declare
        src_schema text;
        ddl_qry text := '';
        rec record;

    /*
        Example of input JSONB for the function:

        [
            {
                "year": 2023,
                "quarters": [3, 4],
                "start_month": null,
                "end_month": null
            },
            {
                "year": 2024,
                "quarters": [1, 2],
                "start_month": 1,
                "end_month": 6
            }
        ]

        This would yield tables y2023, y2024, q[3-4]_2023, q[1-2]_2024, m[1-6]_2024

        If dry_run := true then function will only return generated DDL text without executing it
    */

    begin

        select
            pronamespace::regnamespace::text
        into
            src_schema
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        for rec in

            with yrs as
            (
                select
                    (js ->> 'year')::int as yr,
                    array(select jsonb_array_elements_text(js ->'quarters'))::int[] as qts,
                    array(select generate_series((js ->> 'start_month')::int, (js ->> 'end_month')::int)) as mns
                from lateral jsonb_array_elements(agg_period_json) j (js)
            )

            select
                'y' || yr as tables_prefix
            from yrs y

            union all

            select
                'q' || q.qt || '_' || y.yr as tables_prefix
            from yrs y
            left join unnest(y.qts) q (qt) on true
            where
                cardinality(y.qts) > 0

            union all

            select
                'm' || m.mn || '_' || y.yr as tables_prefix
            from yrs y
            left join unnest(y.mns) m (mn) on true
            where
                cardinality(y.mns) > 0

        loop

                -- Preparing queries

            ddl_qry := ddl_qry || repeat(E'\n', 2) || format($ddl$

                drop table if exists %1$I.%2$s_gigs cascade;
                drop table if exists %1$I.%2$s_sellers cascade;
                drop table if exists %1$I.%2$s_all_categories cascade;
                drop table if exists %1$I.%2$s_categories cascade;
                drop table if exists %1$I.%2$s_subcategories cascade;
                drop table if exists %1$I.%2$s_nested_subcategories cascade;
                drop table if exists %1$I.%2$s_category_x_seller_levels cascade;
                drop table if exists %1$I.%2$s_subcategory_x_seller_levels cascade;
                drop table if exists %1$I.%2$s_nested_subcategory_x_seller_levels cascade;

                    -- metrics

                create table if not exists %1$I.%2$s_metrics
                (
                    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                    processed_dttm timestamptz not null default transaction_timestamp(),
                    seller_created_at timestamptz not null,
                    seller_fiverr_created_at timestamptz null,
                    seller_scraped_at timestamptz not null,
                    gig_created_at timestamptz not null,
                    gig_fiverr_created_at timestamptz null,
                    gig_scraped_at timestamptz not null,
                    agg_period_start date not null,
                    seller_id int not null,
                    fiverr_seller_id int not null,
                    gig_id int not null,
                    fiverr_gig_id int not null,
                    category_id int not null,
                    sub_category_id int not null,
                    nested_sub_category_id int null,
                    all_gigs_count_by_category int not null,
                    all_gigs_count_by_subcategory int not null,
                    all_gigs_count_by_nested_subcategory int null,
                    regular_gigs_by_category int not null,
                    regular_gigs_by_subcategory int not null,
                    regular_gigs_by_nested_subcategory int null,
                    pro_gigs_by_category int not null,
                    pro_gigs_by_subcategory int not null,
                    pro_gigs_by_nested_subcategory int null,
                    seller_count_by_level_and_category int not null,
                    seller_count_by_level_and_subcategory int not null,
                    seller_count_by_level_and_nested_subcategory int null,
                    gig_count_by_seller int not null,
                    seller_ratings_count int not null,
                    seller_completed_orders_count int not null,
                    active_gigs_ratings_count_by_seller int not null,
                    gig_ratings_count int not null,
                    volume_by_seller int not null,
                    volume_by_gig int not null,
                    volume_by_category int not null,
                    volume_by_subcategory int not null,
                    volume_by_nested_subcategory int null,
                    total_historical_volume_by_seller int not null,
                    total_historical_volume_by_gig int not null,
                    total_historical_volume_by_category int not null,
                    total_historical_volume_by_subcategory int not null,
                    total_historical_volume_by_nested_subcategory int null,
                    heuristic bool not null,
                    seller_is_pro bool not null,
                    seller_is_active bool not null,
                    gig_is_pro bool not null,
                    gig_is_active bool not null,
                    is_trend_valid_for_seller bool not null,
                    is_trend_valid_for_gig bool not null,
                    is_trend_valid_for_category bool not null,
                    is_trend_valid_for_subcategory bool not null,
                    is_trend_valid_for_nested_subcategory bool null,
                    sales_volume_growth_percent_by_seller numeric not null,
                    sales_volume_growth_percent_by_gig numeric not null,
                    sales_volume_growth_percent_by_category numeric not null,
                    sales_volume_growth_percent_by_subcategory numeric not null,
                    sales_volume_growth_percent_by_nested_subcategory numeric null,
                    competition_score_for_gig_by_category numeric not null,
                    competition_score_for_gig_by_subcategory numeric not null,
                    competition_score_for_gig_by_nested_subcategory numeric null,
                    competition_score_for_category numeric not null,
                    competition_score_for_subcategory numeric not null,
                    competition_score_for_nested_subcategory numeric null,
                    market_share_for_gig_by_category numeric not null,
                    market_share_for_gig_by_subcategory numeric not null,
                    market_share_for_gig_by_nested_subcategory numeric null,
                    market_share_for_category numeric not null,
                    market_share_for_subcategory numeric not null,
                    market_share_for_nested_subcategory numeric null,
                    seller_rating numeric not null,
                    gig_rating numeric not null,
                    min_price_by_seller numeric not null,
                    min_price_by_gig numeric not null,
                    min_price_by_category numeric not null,
                    min_price_by_subcategory numeric not null,
                    min_price_by_nested_subcategory numeric null,
                    avg_price_by_seller numeric not null,
                    avg_price_by_gig numeric not null,
                    avg_price_by_category numeric not null,
                    avg_price_by_subcategory numeric not null,
                    avg_price_by_nested_subcategory numeric null,
                    max_price_by_seller numeric not null,
                    max_price_by_gig numeric not null,
                    max_price_by_category numeric not null,
                    max_price_by_subcategory numeric not null,
                    max_price_by_nested_subcategory numeric null,
                    weighted_min_price_by_seller numeric not null,
                    weighted_min_price_by_category numeric not null,
                    weighted_min_price_by_subcategory numeric not null,
                    weighted_min_price_by_nested_subcategory numeric null,
                    weighted_avg_price_by_seller numeric not null,
                    weighted_avg_price_by_category numeric not null,
                    weighted_avg_price_by_subcategory numeric not null,
                    weighted_avg_price_by_nested_subcategory numeric null,
                    weighted_max_price_by_seller numeric not null,
                    weighted_max_price_by_category numeric not null,
                    weighted_max_price_by_subcategory numeric not null,
                    weighted_max_price_by_nested_subcategory numeric null,
                    min_active_revenue_by_seller numeric not null,
                    min_inactive_revenue_by_seller numeric not null,
                    min_total_revenue_by_seller numeric not null,
                    min_revenue_by_gig numeric not null,
                    min_active_revenue_by_category numeric not null,
                    min_inactive_revenue_by_category numeric not null,
                    min_total_revenue_by_category numeric not null,
                    min_active_revenue_by_subcategory numeric not null,
                    min_inactive_revenue_by_subcategory numeric not null,
                    min_total_revenue_by_subcategory numeric not null,
                    min_active_revenue_by_nested_subcategory numeric null,
                    min_inactive_revenue_by_nested_subcategory numeric null,
                    min_total_revenue_by_nested_subcategory numeric null,
                    avg_active_revenue_by_seller numeric not null,
                    avg_inactive_revenue_by_seller numeric not null,
                    avg_total_revenue_by_seller numeric not null,
                    avg_revenue_by_gig numeric not null,
                    avg_active_revenue_by_category numeric not null,
                    avg_inactive_revenue_by_category numeric not null,
                    avg_total_revenue_by_category numeric not null,
                    avg_active_revenue_by_subcategory numeric not null,
                    avg_inactive_revenue_by_subcategory numeric not null,
                    avg_total_revenue_by_subcategory numeric not null,
                    avg_active_revenue_by_nested_subcategory numeric null,
                    avg_inactive_revenue_by_nested_subcategory numeric null,
                    avg_total_revenue_by_nested_subcategory numeric null,
                    max_active_revenue_by_seller numeric not null,
                    max_inactive_revenue_by_seller numeric not null,
                    max_total_revenue_by_seller numeric not null,
                    max_revenue_by_gig numeric not null,
                    max_active_revenue_by_category numeric not null,
                    max_inactive_revenue_by_category numeric not null,
                    max_total_revenue_by_category numeric not null,
                    max_active_revenue_by_subcategory numeric not null,
                    max_inactive_revenue_by_subcategory numeric not null,
                    max_total_revenue_by_subcategory numeric not null,
                    max_active_revenue_by_nested_subcategory numeric null,
                    max_inactive_revenue_by_nested_subcategory numeric null,
                    max_total_revenue_by_nested_subcategory numeric null,
                    min_total_historical_revenue_by_seller numeric not null,
                    min_total_historical_revenue_by_gig numeric not null,
                    min_total_historical_revenue_by_category numeric not null,
                    min_total_historical_revenue_by_subcategory numeric not null,
                    min_total_historical_revenue_by_nested_subcategory numeric null,
                    avg_total_historical_revenue_by_seller numeric not null,
                    avg_total_historical_revenue_by_gig numeric not null,
                    avg_total_historical_revenue_by_category numeric not null,
                    avg_total_historical_revenue_by_subcategory numeric not null,
                    avg_total_historical_revenue_by_nested_subcategory numeric null,
                    max_total_historical_revenue_by_seller numeric not null,
                    max_total_historical_revenue_by_gig numeric not null,
                    max_total_historical_revenue_by_category numeric not null,
                    max_total_historical_revenue_by_subcategory numeric not null,
                    max_total_historical_revenue_by_nested_subcategory numeric null,
                    revenue_growth_percent_by_seller numeric not null,
                    revenue_growth_percent_by_gig numeric not null,
                    revenue_growth_percent_by_category numeric not null,
                    revenue_growth_percent_by_subcategory numeric not null,
                    revenue_growth_percent_by_nested_subcategory numeric null,
                    trend_by_seller numeric not null,
                    r2_by_seller numeric not null,
                    trends_avg_revenue_by_seller numeric not null,
                    trend_by_gig numeric not null,
                    r2_by_gig numeric not null,
                    trends_avg_revenue_by_gig numeric not null,
                    trend_by_category numeric not null,
                    r2_by_category numeric not null,
                    trends_avg_revenue_by_category numeric not null,
                    trend_by_subcategory numeric not null,
                    r2_by_subcategory numeric not null,
                    trends_avg_revenue_by_subcategory numeric not null,
                    trend_by_nested_subcategory numeric null,
                    r2_by_nested_subcategory numeric null,
                    trends_avg_revenue_by_nested_subcategory numeric null,
                    rcrit numeric not null,
                    seller_level text not null,
                    seller_name text not null,
                    agency_slug text null,
                    agency_status text null,
                    gig_title text null,
                    gig_cached_slug text not null,
                    category_name text not null,
                    sub_category_name text not null,
                    nested_sub_category_name text null,
                    seller_country text null,
                    seller_country_code text null,
                    seller_languages text[] null,
                    seller_profile_image text null,
                    gig_preview_url text not null
                );

                alter table if exists %1$I.%2$s_metrics
                    add primary key (agg_period_start, gig_id);
                create index if not exists idx_seller_id_%2$s_metrics
                    on %1$I.%2$s_metrics (agg_period_start, seller_id);
                create index if not exists idx_category_id_%2$s_metrics
                    on %1$I.%2$s_metrics (agg_period_start, category_id);
                create index if not exists idx_sub_category_id_%2$s_metrics
                    on %1$I.%2$s_metrics (agg_period_start, sub_category_id);
                create index if not exists idx_nested_sub_category_id_%2$s_metrics
                    on %1$I.%2$s_metrics (agg_period_start, nested_sub_category_id);
                create index if not exists idx_categories_%2$s_metrics
                    on %1$I.%2$s_metrics (agg_period_start, category_id, sub_category_id, nested_sub_category_id);

                    -- gigs

                create table if not exists %1$I.%2$s_gigs
                (
                    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                    processed_dttm timestamptz not null default transaction_timestamp(),
                    seller_created_at timestamptz not null,
                    seller_fiverr_created_at timestamptz null,
                    seller_scraped_at timestamptz not null,
                    gig_created_at timestamptz not null,
                    gig_fiverr_created_at timestamptz null,
                    gig_scraped_at timestamptz not null,
                    min_revenue bigint not null,
                    avg_revenue bigint not null,
                    max_revenue bigint not null,
                    min_total_historical_revenue bigint not null,
                    avg_total_historical_revenue bigint not null,
                    max_total_historical_revenue bigint not null,
                    agg_period_start date not null,
                    seller_id int not null,
                    fiverr_seller_id int not null,
                    gig_id int not null,
                    fiverr_gig_id int not null,
                    category_id int not null,
                    sub_category_id int not null,
                    nested_sub_category_id int null,
                    global_rank int not null,
                    seller_ratings_count int not null,
                    ratings_count int not null,
                    sales_volume int not null,
                    total_historical_sales_volume int not null,
                    min_price int not null,
                    avg_price int not null,
                    max_price int not null,
                    heuristic bool not null,
                    is_pro bool not null,
                    is_active bool not null,
                    is_trend_valid bool not null,
                    sales_volume_growth_percent numeric not null,
                    competition_score_for_gig_by_category numeric not null,
                    competition_score_for_gig_by_subcategory numeric not null,
                    competition_score_for_gig_by_nested_subcategory numeric null,
                    market_share_for_gig_by_category numeric not null,
                    market_share_for_gig_by_subcategory numeric not null,
                    market_share_for_gig_by_nested_subcategory numeric null,
                    seller_rating numeric not null,
                    rating numeric not null,
                    revenue_growth_percent numeric not null,
                    revenue_trend numeric not null,
                    seller_level text not null,
                    seller_name text not null,
                    title text null,
                    cached_slug text not null,
                    category_name text not null,
                    sub_category_name text not null,
                    nested_sub_category_name text null,
                    seller_country text null,
                    seller_country_code text null,
                    image_preview text not null,
                    seller_profile_image text null
                );

                alter table if exists %1$I.%2$s_gigs
                    add primary key (agg_period_start, gig_id);
                create unique index if not exists uidx_fiverr_gig_id_%2$s_gigs
                    on %1$I.%2$s_gigs (agg_period_start, fiverr_gig_id);
                create index if not exists idx_seller_id_%2$s_gigs
                    on %1$I.%2$s_gigs (agg_period_start, seller_id);
                create index if not exists idx_fiverr_seller_id_%2$s_gigs
                    on %1$I.%2$s_gigs (agg_period_start, fiverr_seller_id);
                create index if not exists idx_category_id_%2$s_gigs
                    on %1$I.%2$s_gigs (agg_period_start, category_id);
                create index if not exists idx_sub_category_id_%2$s_gigs
                    on %1$I.%2$s_gigs (agg_period_start, sub_category_id);
                create index if not exists idx_nested_sub_category_id_%2$s_gigs
                    on %1$I.%2$s_gigs (agg_period_start, nested_sub_category_id);
                create index if not exists idx_categories_%2$s_gigs
                    on %1$I.%2$s_gigs (agg_period_start, category_id, sub_category_id, nested_sub_category_id);
                create unique index if not exists uidx_%2$s_gigs_ranks
                    on %1$I.%2$s_gigs (agg_period_start, global_rank desc);
                create index if not exists idx_cached_slug_gigs
                    on %1$I.%2$s_gigs (agg_period_start, cached_slug);
                create index if not exists idx_trg_title_%2$s_gigs
                    on %1$I.%2$s_gigs using gin (title gin_trgm_ops);
                create index if not exists idx_trg_seller_name_%2$s_gigs
                    on %1$I.%2$s_gigs using gin (seller_name gin_trgm_ops);
                create index if not exists idx_trg_cat_name_%2$s_gigs
                    on %1$I.%2$s_gigs using gin (category_name gin_trgm_ops);
                create index if not exists idx_trg_subcat_name_%2$s_gigs
                    on %1$I.%2$s_gigs using gin (sub_category_name gin_trgm_ops);
                create index if not exists idx_trg_nest_subcat_name_%2$s_gigs
                    on %1$I.%2$s_gigs using gin (nested_sub_category_name gin_trgm_ops);

                    -- sellers

                create table if not exists %1$I.%2$s_sellers
                (
                    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                    processed_dttm timestamptz not null default transaction_timestamp(),
                    created_at timestamptz not null,
                    fiverr_created_at timestamptz null,
                    scraped_at timestamptz not null,
                    min_active_revenue bigint not null,
                    min_inactive_revenue bigint not null,
                    min_revenue bigint not null,
                    avg_active_revenue bigint not null,
                    avg_inactive_revenue bigint not null,
                    avg_revenue bigint not null,
                    max_active_revenue bigint not null,
                    max_inactive_revenue bigint not null,
                    max_revenue bigint not null,
                    min_total_historical_revenue bigint not null,
                    avg_total_historical_revenue bigint not null,
                    max_total_historical_revenue bigint not null,
                    best_selling_gig_revenue bigint not null,
                    agg_period_start date not null,
                    seller_id int not null,
                    fiverr_seller_id int not null,
                    best_selling_gig_id int null,
                    best_selling_fiverr_gig_id int null,
                    global_rank int not null,
                    gig_count_by_seller int not null,
                    ratings_count int not null,
                    completed_orders_count int not null,
                    active_gigs_ratings_count_by_seller int not null,
                    sales_volume int not null,
                    total_historical_sales_volume int not null,
                    best_selling_gig_total_historical_volume int null,
                    weighted_avg_gig_price int not null,
                    is_pro bool not null,
                    is_active bool not null,
                    is_trend_valid bool not null,
                    sales_volume_growth_percent numeric not null,
                    rating numeric not null,
                    revenue_growth_percent numeric not null,
                    revenue_trend numeric not null,
                    best_selling_gig_revenue_share numeric not null,
                    best_selling_gig_title text null,
                    best_selling_gig_cached_slug text null,
                    "level" text not null,
                    seller_name text not null,
                    agency_slug text null,
                    agency_status text null,
                    country text null,
                    country_code text null,
                    languages text[] null,
                    profile_image text null,
                    best_selling_gig_image_preview text null
                );

                alter table if exists %1$I.%2$s_sellers
                    add primary key (agg_period_start, seller_id) include (is_active);
                create unique index if not exists uidx_fiverr_seller_id_%2$s_sellers
                    on %1$I.%2$s_sellers (agg_period_start, fiverr_seller_id);
                create index if not exists idx_%2$s_sellers_best_selling_gig_id
                    on %1$I.%2$s_sellers (agg_period_start, best_selling_gig_id);
                create unique index if not exists uidx_%2$s_sellers_ranks
                    on %1$I.%2$s_sellers (agg_period_start, global_rank desc);
                create index if not exists idx_trg_seller_name_%2$s_sellers
                    on %1$I.%2$s_sellers using gin (seller_name gin_trgm_ops);

                    -- all_categories

                create table if not exists %1$I.%2$s_all_categories
                (
                    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                    processed_dttm timestamptz not null default transaction_timestamp(),
                    min_active_revenue_by_category bigint not null,
                    min_inactive_revenue_by_category bigint not null,
                    min_revenue_by_category bigint not null,
                    min_active_revenue_by_subcategory bigint not null,
                    min_inactive_revenue_by_subcategory bigint not null,
                    min_revenue_by_subcategory bigint not null,
                    min_active_revenue_by_nested_subcategory bigint null,
                    min_inactive_revenue_by_nested_subcategory bigint null,
                    min_revenue_by_nested_subcategory bigint null,
                    avg_active_revenue_by_category bigint not null,
                    avg_inactive_revenue_by_category bigint not null,
                    avg_revenue_by_category bigint not null,
                    avg_active_revenue_by_subcategory bigint not null,
                    avg_inactive_revenue_by_subcategory bigint not null,
                    avg_revenue_by_subcategory bigint not null,
                    avg_active_revenue_by_nested_subcategory bigint null,
                    avg_inactive_revenue_by_nested_subcategory bigint null,
                    avg_revenue_by_nested_subcategory bigint null,
                    max_active_revenue_by_category bigint not null,
                    max_inactive_revenue_by_category bigint not null,
                    max_revenue_by_category bigint not null,
                    max_active_revenue_by_subcategory bigint not null,
                    max_inactive_revenue_by_subcategory bigint not null,
                    max_revenue_by_subcategory bigint not null,
                    max_active_revenue_by_nested_subcategory bigint null,
                    max_inactive_revenue_by_nested_subcategory bigint null,
                    max_revenue_by_nested_subcategory bigint null,
                    min_total_historical_revenue_by_category bigint not null,
                    min_total_historical_revenue_by_subcategory bigint not null,
                    min_total_historical_revenue_by_nested_subcategory bigint null,
                    avg_total_historical_revenue_by_category bigint not null,
                    avg_total_historical_revenue_by_subcategory bigint not null,
                    avg_total_historical_revenue_by_nested_subcategory bigint null,
                    max_total_historical_revenue_by_category bigint not null,
                    max_total_historical_revenue_by_subcategory bigint not null,
                    max_total_historical_revenue_by_nested_subcategory bigint null,
                    agg_period_start date not null,
                    id int not null,
                    category_id int not null,
                    sub_category_id int not null,
                    nested_sub_category_id int null,
                    global_rank_for_category int not null,
                    global_rank_for_subcategory int not null,
                    global_rank_for_nested_subcategory int null,
                    all_gigs_count_by_category int not null,
                    all_gigs_count_by_subcategory int not null,
                    all_gigs_count_by_nested_subcategory int null,
                    regular_gigs_count_by_category int not null,
                    regular_gigs_count_by_subcategory int not null,
                    regular_gigs_count_by_nested_subcategory int null,
                    pro_gigs_count_by_category int not null,
                    pro_gigs_count_by_subcategory int not null,
                    pro_gigs_count_by_nested_subcategory int null,
                    seller_count_by_category int not null,
                    seller_count_by_subcategory int not null,
                    seller_count_by_nested_subcategory int null,
                    sales_volume_by_category int not null,
                    sales_volume_by_subcategory int not null,
                    sales_volume_by_nested_subcategory int null,
                    total_historical_sales_volume_by_category int not null,
                    total_historical_sales_volume_by_subcategory int not null,
                    total_historical_sales_volume_by_nested_subcategory int null,
                    weighted_avg_gig_price_by_category int not null,
                    weighted_avg_gig_price_by_subcategory int not null,
                    weighted_avg_gig_price_by_nested_subcategory int null,
                    is_trend_valid_for_category bool not null,
                    is_trend_valid_for_subcategory bool not null,
                    is_trend_valid_for_nested_subcategory bool null,
                    sales_volume_growth_percent_by_category numeric not null,
                    sales_volume_growth_percent_by_subcategory numeric not null,
                    sales_volume_growth_percent_by_nested_subcategory numeric null,
                    competition_score_for_category numeric not null,
                    competition_score_for_subcategory numeric not null,
                    competition_score_for_nested_subcategory numeric null,
                    market_share_for_category numeric not null,
                    market_share_for_subcategory numeric not null,
                    market_share_for_nested_subcategory numeric null,
                    revenue_growth_percent_by_category numeric not null,
                    revenue_growth_percent_by_subcategory numeric not null,
                    revenue_growth_percent_by_nested_subcategory numeric null,
                    revenue_trend_by_category numeric not null,
                    revenue_trend_by_subcategory numeric not null,
                    revenue_trend_by_nested_subcategory numeric null,
                    category_name text not null,
                    sub_category_name text not null,
                    nested_sub_category_name text null
                );

                alter table if exists %1$I.%2$s_all_categories
                    add primary key (agg_period_start, id);
                create unique index if not exists uidx_all_id_%2$s_all_cats
                    on %1$I.%2$s_all_categories (agg_period_start, category_id, sub_category_id, nested_sub_category_id);
                create index if not exists idx_cat_id_%2$s_all_cats
                    on %1$I.%2$s_all_categories (agg_period_start, category_id);
                create index if not exists idx_sub_cat_id_%2$s_all_cats
                    on %1$I.%2$s_all_categories (agg_period_start, sub_category_id);
                create index if not exists idx_nest_sub_cat_id_%2$s_all_cats
                    on %1$I.%2$s_all_categories (agg_period_start, nested_sub_category_id);
                create index if not exists idx_trg_cat_name_%2$s_all_cats
                    on %1$I.%2$s_all_categories using gin (category_name gin_trgm_ops);
                create index if not exists idx_trg_subcat_name_%2$s_all_cats
                    on %1$I.%2$s_all_categories using gin (sub_category_name gin_trgm_ops);
                create index if not exists idx_trg_nest_subcat_name_%2$s_all_cats
                    on %1$I.%2$s_all_categories using gin (nested_sub_category_name gin_trgm_ops);

                    -- categories

                create table if not exists %1$I.%2$s_categories
                (
                    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                    processed_dttm timestamptz not null default transaction_timestamp(),
                    min_active_revenue bigint not null,
                    min_inactive_revenue bigint not null,
                    min_revenue bigint not null,
                    avg_active_revenue bigint not null,
                    avg_inactive_revenue bigint not null,
                    avg_revenue bigint not null,
                    max_active_revenue bigint not null,
                    max_inactive_revenue bigint not null,
                    max_revenue bigint not null,
                    min_total_historical_revenue bigint not null,
                    avg_total_historical_revenue bigint not null,
                    max_total_historical_revenue bigint not null,
                    agg_period_start date not null,
                    category_id int not null,
                    global_rank int not null,
                    all_gigs_count int not null,
                    regular_gigs_count int not null,
                    pro_gigs_count int not null,
                    seller_count int not null,
                    sales_volume int not null,
                    total_historical_sales_volume int not null,
                    weighted_avg_gig_price int not null,
                    is_trend_valid bool not null,
                    sales_volume_growth_percent numeric not null,
                    competition_score numeric not null,
                    market_share numeric not null,
                    revenue_growth_percent numeric not null,
                    revenue_trend numeric not null,
                    category_name text not null
                );

                alter table if exists %1$I.%2$s_categories
                    add primary key (agg_period_start, category_id);
                create unique index if not exists uidx_%2$s_categories_ranks
                    on %1$I.%2$s_categories (agg_period_start, global_rank desc);
                create index if not exists idx_trg_cat_name_%2$s_cat
                    on %1$I.%2$s_categories using gin (category_name gin_trgm_ops);

                    -- subcategories

                create table if not exists %1$I.%2$s_subcategories
                (
                    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                    processed_dttm timestamptz not null default transaction_timestamp(),
                    min_active_revenue bigint not null,
                    min_inactive_revenue bigint not null,
                    min_revenue bigint not null,
                    avg_active_revenue bigint not null,
                    avg_inactive_revenue bigint not null,
                    avg_revenue bigint not null,
                    max_active_revenue bigint not null,
                    max_inactive_revenue bigint not null,
                    max_revenue bigint not null,
                    min_total_historical_revenue bigint not null,
                    avg_total_historical_revenue bigint not null,
                    max_total_historical_revenue bigint not null,
                    agg_period_start date not null,
                    sub_category_id int not null,
                    global_rank int not null,
                    all_gigs_count int not null,
                    regular_gigs_count int not null,
                    pro_gigs_count int not null,
                    seller_count int not null,
                    sales_volume int not null,
                    total_historical_sales_volume int not null,
                    weighted_avg_gig_price int not null,
                    is_trend_valid bool not null,
                    sales_volume_growth_percent numeric not null,
                    competition_score numeric not null,
                    market_share numeric not null,
                    revenue_growth_percent numeric not null,
                    revenue_trend numeric not null,
                    sub_category_name text not null
                );

                alter table if exists %1$I.%2$s_subcategories
                    add primary key (agg_period_start, sub_category_id);
                create unique index if not exists uidx_%2$s_subcategories_ranks
                    on %1$I.%2$s_subcategories (agg_period_start, global_rank desc);
                create index if not exists idx_trg_subcat_name_%2$s_subcat
                    on %1$I.%2$s_subcategories using gin (sub_category_name gin_trgm_ops);

                    -- nested_subcategories

                create table if not exists %1$I.%2$s_nested_subcategories
                (
                    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                    processed_dttm timestamptz not null default transaction_timestamp(),
                    min_active_revenue bigint not null,
                    min_inactive_revenue bigint not null,
                    min_revenue bigint not null,
                    avg_active_revenue bigint not null,
                    avg_inactive_revenue bigint not null,
                    avg_revenue bigint not null,
                    max_active_revenue bigint not null,
                    max_inactive_revenue bigint not null,
                    max_revenue bigint not null,
                    min_total_historical_revenue bigint not null,
                    avg_total_historical_revenue bigint not null,
                    max_total_historical_revenue bigint not null,
                    agg_period_start date not null,
                    nested_sub_category_id int not null,
                    global_rank int not null,
                    all_gigs_count int not null,
                    regular_gigs_count int not null,
                    pro_gigs_count int not null,
                    seller_count int not null,
                    sales_volume int not null,
                    total_historical_sales_volume int not null,
                    weighted_avg_gig_price int not null,
                    is_trend_valid bool not null,
                    sales_volume_growth_percent numeric not null,
                    competition_score numeric not null,
                    market_share numeric not null,
                    revenue_growth_percent numeric not null,
                    revenue_trend numeric not null,
                    nested_sub_category_name text not null
                );

                alter table if exists %1$I.%2$s_nested_subcategories
                    add primary key (agg_period_start, nested_sub_category_id);
                create unique index if not exists uidx_%2$s_nested_subcategories_ranks
                    on %1$I.%2$s_nested_subcategories (agg_period_start, global_rank desc);
                create index if not exists idx_trg_nest_subcat_name_%2$s_nest_subcat
                    on %1$I.%2$s_nested_subcategories using gin (nested_sub_category_name gin_trgm_ops);

                    -- category_x_seller_levels

                create table if not exists %1$I.%2$s_category_x_seller_levels
                (
                    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                    processed_dttm timestamptz not null default transaction_timestamp(),
                    agg_period_start date not null,
                    id int not null,
                    category_id int not null,
                    seller_count int not null,
                    seller_level text not null,
                    category_name text not null
                );

                alter table if exists %1$I.%2$s_category_x_seller_levels
                    add primary key (agg_period_start, id);
                create unique index if not exists uidx_%2$s_cat_sel_lvl
                    on %1$I.%2$s_category_x_seller_levels (agg_period_start, category_id, seller_level);
                create index if not exists idx_trg_cat_name_%2$s_cat_sel_lvl
                    on %1$I.%2$s_category_x_seller_levels using gin (category_name gin_trgm_ops);

                    -- subcategory_x_seller_levels

                create table if not exists %1$I.%2$s_subcategory_x_seller_levels
                (
                    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                    processed_dttm timestamptz not null default transaction_timestamp(),
                    agg_period_start date not null,
                    id int not null,
                    sub_category_id int not null,
                    seller_count int not null,
                    seller_level text not null,
                    sub_category_name text not null
                );

                alter table if exists %1$I.%2$s_subcategory_x_seller_levels
                    add primary key (agg_period_start, id);
                create unique index if not exists uidx_%2$s_subcat_sel_lvl
                    on %1$I.%2$s_subcategory_x_seller_levels (agg_period_start, sub_category_id, seller_level);
                create index if not exists idx_trg_subcat_name_%2$s_subcat_sel_lvl
                    on %1$I.%2$s_subcategory_x_seller_levels using gin (sub_category_name gin_trgm_ops);

                    -- nested_subcategory_x_seller_levels

                create table if not exists %1$I.%2$s_nested_subcategory_x_seller_levels
                (
                    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
                    processed_dttm timestamptz not null default transaction_timestamp(),
                    agg_period_start date not null,
                    id int not null,
                    nested_sub_category_id int not null,
                    seller_count int not null,
                    seller_level text not null,
                    nested_sub_category_name text not null
                );

                alter table if exists %1$I.%2$s_nested_subcategory_x_seller_levels
                    add primary key (agg_period_start, id);
                create unique index if not exists uidx_%2$s_nest_subcat_sel_lvl
                    on %1$I.%2$s_nested_subcategory_x_seller_levels (agg_period_start, nested_sub_category_id, seller_level);
                create index if not exists idx_trg_nest_subcat_name_%2$s_nest_subcat_sel_lvl
                    on %1$I.%2$s_nested_subcategory_x_seller_levels using gin (nested_sub_category_name gin_trgm_ops);

            $ddl$,
            src_schema,
            rec.tables_prefix
            );

            raise notice 'DDL has been generated for % period', rec.tables_prefix;

        end loop;

        if dry_run is false then
            execute ddl_qry;
        end if;

        return ddl_qry;

    end;
$func$;


-- Function call

select etl_agg.generate_agg_ddl
    (
        agg_period_json := $$
            [
                {
                    "year": 2023,
                    "quarters": [],
                    "start_month": null,
                    "end_month": null
                },
                {
                    "year": 2024,
                    "quarters": [1, 2, 3, 4],
                    "start_month": 1,
                    "end_month": 12
                }
            ]$$::jsonb,
        dry_run := false
    );



-- Direct subscription
    -- Replace PWD with role password and SSLROOTCERTPATH with a full local path to certificate file

    -- Link to download AWS RDS SSL certificate bundle - https://truststore.pki.rds.amazonaws.com/eu-central-1/eu-central-1-bundle.pem
    -- Root certificate must be placed in a local folder accessible by PostgreSQL
        -- For example, use local PG data directory (can be found with SQL command on replica ```show data_directory;```)

drop subscription if exists etl_agg cascade;

create subscription etl_agg
    connection 'host=db-raw-1.cr82xjq3sz3n.eu-central-1.rds.amazonaws.com port=5432 user=etl_read_only dbname=raw_1 password=PWD sslrootcert=SSLROOTCERTPATH sslmode=verify-full'
    publication etl_agg;


-- Proxy subscription
    -- Replace PWD with role password

drop subscription if exists etl_agg cascade;

create subscription etl_agg
    connection 'host=etl-proxy.proxy-cr82xjq3sz3n.eu-central-1.rds.amazonaws.com port=5432 user=etl_read_only dbname=raw_1 password=PWD sslmode=require'
    publication etl_agg;