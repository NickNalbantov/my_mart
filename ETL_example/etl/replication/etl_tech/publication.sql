-- publisher

grant rds_replication to etl_read_only;

    -- Prematerialization and etl.metrics tech tables

drop publication if exists etl_tech cascade;

create publication etl_tech for table
    etl.metrics,
    etl.gigs_revisions_pre_metrics,
    etl.gigs_pages_pre_metrics,
    etl.sellers_revisions_pre_metrics,
    etl.gigs_revisions_pre_metrics_invalid,
    etl.gigs_pages_pre_metrics_invalid,
    etl.sellers_revisions_pre_metrics_invalid
with
(
    publish_via_partition_root = true
);