-- DDL for etl_tech subscription

create schema if not exists etl;

drop table if exists etl.gigs_revisions_pre_metrics cascade;
drop table if exists etl.gigs_pages_pre_metrics cascade;
drop table if exists etl.sellers_revisions_pre_metrics cascade;
drop table if exists etl.gigs_revisions_pre_metrics_invalid cascade;
drop table if exists etl.gigs_pages_pre_metrics_invalid cascade;
drop table if exists etl.sellers_revisions_pre_metrics_invalid cascade;

drop table if exists etl.metrics cascade;

    -- gigs revisions prematerialization

create table if not exists etl.gigs_revisions_pre_metrics
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    created_at timestamptz not null,
    fiverr_created_at timestamptz null,
    scraped_at timestamptz not null,
    id int not null,
    gig_id int not null,
    fiverr_gig_id int not null,
    fiverr_seller_id int not null,
    category_id int not null,
    sub_category_id int not null,
    nested_sub_category_id int null,
    gig_ratings_count int null,
    gig_is_pro boolean not null,
    gig_rating numeric null,
    prices numeric[] not null,
    gig_title text null,
    category_name text null,
    sub_category_name text null,
    nested_sub_category_name text null,
    gig_cached_slug text not null,
    gig_preview_url text not null
);

create unique index if not exists uidx_gigs_revisions_pre_metrics_id on etl.gigs_revisions_pre_metrics (id);
create index if not exists idx_gigs_revisions_pre_metrics_gig_id_scraped_at on etl.gigs_revisions_pre_metrics (gig_id, scraped_at desc);
create index if not exists idx_gigs_revisions_pre_metrics_fiverr_seller_id on etl.gigs_revisions_pre_metrics (fiverr_seller_id);
create index if not exists idx_gigs_revisions_pre_metrics_scraped_at on etl.gigs_revisions_pre_metrics (scraped_at desc);
create index if not exists idx_gigs_revisions_pre_metrics_load_id on etl.gigs_revisions_pre_metrics ("load_id" desc);

    -- invalid gigs revisions prematerialization

create table if not exists etl.gigs_revisions_pre_metrics_invalid
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    created_at timestamptz not null,
    fiverr_created_at timestamptz null,
    scraped_at timestamptz not null,
    id int not null,
    gig_id int not null,
    fiverr_gig_id int null,
    fiverr_seller_id int null,
    category_id int null,
    sub_category_id int null,
    nested_sub_category_id int null,
    gig_ratings_count int null,
    gig_is_pro boolean null,
    gig_rating numeric null,
    prices numeric[] null,
    gig_title text null,
    category_name text null,
    sub_category_name text null,
    nested_sub_category_name text null,
    gig_cached_slug text null,
    gig_preview_url text null
);

create unique index if not exists uidx_gigs_revisions_pre_metrics_invalid_id on etl.gigs_revisions_pre_metrics_invalid (id);
create index if not exists idx_gigs_revisions_pre_metrics_invalid on etl.gigs_revisions_pre_metrics_invalid (gig_id, scraped_at desc);

    -- gigs pages prematerialization

create table if not exists etl.gigs_pages_pre_metrics
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    created_at timestamptz not null,
    scraped_at timestamptz not null,
    id int not null,
    gig_id int not null,
    fiverr_gig_id int not null,
    fiverr_seller_id int not null,
    seller_completed_orders_count int not null,
    prices numeric[] not null
);

create unique index if not exists uidx_gigs_pages_pre_metrics_id on etl.gigs_pages_pre_metrics (id);
create index if not exists idx_gigs_pages_pre_metrics_gig_id_scraped_at on etl.gigs_pages_pre_metrics (gig_id, scraped_at desc);
create index if not exists idx_gigs_pages_pre_metrics_fiverr_seller_id on etl.gigs_pages_pre_metrics (fiverr_seller_id);
create index if not exists idx_gigs_pages_pre_metrics_scraped_at on etl.gigs_pages_pre_metrics (scraped_at desc);
create index if not exists idx_gigs_pages_pre_metrics_load_id on etl.gigs_pages_pre_metrics ("load_id" desc);

    -- invalid gigs pages prematerialization

create table if not exists etl.gigs_pages_pre_metrics_invalid
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    created_at timestamptz not null,
    scraped_at timestamptz not null,
    id int not null,
    gig_id int not null,
    fiverr_gig_id int null,
    fiverr_seller_id int null,
    seller_completed_orders_count int null,
    prices numeric[] null
);

create unique index if not exists uidx_gigs_pages_pre_metrics_invalid_id on etl.gigs_pages_pre_metrics_invalid (id);
create index if not exists idx_gigs_pages_pre_metrics_invalid on etl.gigs_pages_pre_metrics_invalid (gig_id, scraped_at desc);

    -- sellers revisions prematerialization

create table if not exists etl.sellers_revisions_pre_metrics
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    created_at timestamptz not null,
    fiverr_created_at timestamptz null,
    scraped_at timestamptz not null,
    id int not null,
    seller_id int not null,
    fiverr_seller_id int not null,
    seller_ratings_count int null,
    seller_is_pro boolean not null,
    seller_rating numeric null,
    seller_level text not null,
    seller_name text not null,
    agency_slug text null,
    agency_status text null,
    seller_profile_image text null,
    seller_country text null,
    seller_country_code text null,
    seller_languages text[] null
);

create unique index if not exists uidx_sellers_revisions_pre_metrics_id on etl.sellers_revisions_pre_metrics (id);
create index if not exists idx_sellers_revisions_pre_metrics_seller_id_scraped_at on etl.sellers_revisions_pre_metrics (seller_id, scraped_at desc);
create index if not exists idx_sellers_revisions_pre_metrics_fiverr_seller_id on etl.sellers_revisions_pre_metrics (fiverr_seller_id);
create index if not exists idx_sellers_revisions_pre_metrics_scraped_at on etl.sellers_revisions_pre_metrics (scraped_at desc);
create index if not exists idx_sellers_revisions_pre_metrics_load_id on etl.sellers_revisions_pre_metrics ("load_id" desc);

    -- invalid sellers revisions prematerialization

create table if not exists etl.sellers_revisions_pre_metrics_invalid
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    created_at timestamptz not null,
    fiverr_created_at timestamptz null,
    scraped_at timestamptz not null,
    id int not null,
    seller_id int not null,
    fiverr_seller_id int null,
    seller_ratings_count int null,
    seller_is_pro boolean null,
    seller_rating numeric null,
    seller_level text not null,
    seller_name text null,
    agency_slug text null,
    agency_status text null,
    seller_profile_image text null,
    seller_country text null,
    seller_country_code text null,
    seller_languages text[] null
);

create unique index if not exists uidx_sellers_revisions_pre_metrics_invalid_id on etl.sellers_revisions_pre_metrics_invalid (id);
create index if not exists idx_sellers_revisions_pre_metrics_invalid on etl.sellers_revisions_pre_metrics_invalid (seller_id, scraped_at desc);

    -- metrics

create table if not exists etl.metrics
(
    "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
    processed_dttm timestamptz not null default transaction_timestamp(),
    seller_created_at timestamptz not null,
    seller_fiverr_created_at timestamptz null,
    seller_scraped_at timestamptz not null,
    gig_created_at timestamptz not null,
    gig_fiverr_created_at timestamptz null,
    gig_scraped_at timestamptz not null,
    seller_id int not null,
    fiverr_seller_id int not null,
    gig_id int not null,
    fiverr_gig_id int not null,
    category_id int not null,
    sub_category_id int not null,
    nested_sub_category_id int null,
    all_gigs_count_by_category int not null,
    all_gigs_count_by_subcategory int not null,
    all_gigs_count_by_nested_subcategory int null,
    regular_gigs_by_category int not null,
    regular_gigs_by_subcategory int not null,
    regular_gigs_by_nested_subcategory int null,
    pro_gigs_by_category int not null,
    pro_gigs_by_subcategory int not null,
    pro_gigs_by_nested_subcategory int null,
    seller_count_by_level_and_category int not null,
    seller_count_by_level_and_subcategory int not null,
    seller_count_by_level_and_nested_subcategory int null,
    gig_count_by_seller int not null,
    seller_ratings_count int not null,
    seller_completed_orders_count int not null,
    active_gigs_ratings_count_by_seller int not null,
    gig_ratings_count int not null,
    volume_by_seller int not null,
    volume_by_gig int not null,
    volume_by_category int not null,
    volume_by_subcategory int not null,
    volume_by_nested_subcategory int null,
    total_historical_volume_by_seller int not null,
    total_historical_volume_by_gig int not null,
    total_historical_volume_by_category int not null,
    total_historical_volume_by_subcategory int not null,
    total_historical_volume_by_nested_subcategory int null,
    heuristic bool not null,
    seller_is_pro bool not null,
    seller_is_active bool not null,
    gig_is_pro bool not null,
    gig_is_active bool not null,
    is_trend_valid_for_seller bool not null,
    is_trend_valid_for_gig bool not null,
    is_trend_valid_for_category bool not null,
    is_trend_valid_for_subcategory bool not null,
    is_trend_valid_for_nested_subcategory bool null,
    sales_volume_growth_percent_by_seller numeric not null,
    sales_volume_growth_percent_by_gig numeric not null,
    sales_volume_growth_percent_by_category numeric not null,
    sales_volume_growth_percent_by_subcategory numeric not null,
    sales_volume_growth_percent_by_nested_subcategory numeric null,
    competition_score_for_gig_by_category numeric not null,
    competition_score_for_gig_by_subcategory numeric not null,
    competition_score_for_gig_by_nested_subcategory numeric null,
    competition_score_for_category numeric not null,
    competition_score_for_subcategory numeric not null,
    competition_score_for_nested_subcategory numeric null,
    market_share_for_gig_by_category numeric not null,
    market_share_for_gig_by_subcategory numeric not null,
    market_share_for_gig_by_nested_subcategory numeric null,
    market_share_for_category numeric not null,
    market_share_for_subcategory numeric not null,
    market_share_for_nested_subcategory numeric null,
    seller_rating numeric not null,
    gig_rating numeric not null,
    min_price_by_seller numeric not null,
    min_price_by_gig numeric not null,
    min_price_by_category numeric not null,
    min_price_by_subcategory numeric not null,
    min_price_by_nested_subcategory numeric null,
    avg_price_by_seller numeric not null,
    avg_price_by_gig numeric not null,
    avg_price_by_category numeric not null,
    avg_price_by_subcategory numeric not null,
    avg_price_by_nested_subcategory numeric null,
    max_price_by_seller numeric not null,
    max_price_by_gig numeric not null,
    max_price_by_category numeric not null,
    max_price_by_subcategory numeric not null,
    max_price_by_nested_subcategory numeric null,
    weighted_min_price_by_seller numeric not null,
    weighted_min_price_by_category numeric not null,
    weighted_min_price_by_subcategory numeric not null,
    weighted_min_price_by_nested_subcategory numeric null,
    weighted_avg_price_by_seller numeric not null,
    weighted_avg_price_by_category numeric not null,
    weighted_avg_price_by_subcategory numeric not null,
    weighted_avg_price_by_nested_subcategory numeric null,
    weighted_max_price_by_seller numeric not null,
    weighted_max_price_by_category numeric not null,
    weighted_max_price_by_subcategory numeric not null,
    weighted_max_price_by_nested_subcategory numeric null,
    min_active_revenue_by_seller numeric not null,
    min_inactive_revenue_by_seller numeric not null,
    min_total_revenue_by_seller numeric not null,
    min_revenue_by_gig numeric not null,
    min_active_revenue_by_category numeric not null,
    min_inactive_revenue_by_category numeric not null,
    min_total_revenue_by_category numeric not null,
    min_active_revenue_by_subcategory numeric not null,
    min_inactive_revenue_by_subcategory numeric not null,
    min_total_revenue_by_subcategory numeric not null,
    min_active_revenue_by_nested_subcategory numeric null,
    min_inactive_revenue_by_nested_subcategory numeric null,
    min_total_revenue_by_nested_subcategory numeric null,
    avg_active_revenue_by_seller numeric not null,
    avg_inactive_revenue_by_seller numeric not null,
    avg_total_revenue_by_seller numeric not null,
    avg_revenue_by_gig numeric not null,
    avg_active_revenue_by_category numeric not null,
    avg_inactive_revenue_by_category numeric not null,
    avg_total_revenue_by_category numeric not null,
    avg_active_revenue_by_subcategory numeric not null,
    avg_inactive_revenue_by_subcategory numeric not null,
    avg_total_revenue_by_subcategory numeric not null,
    avg_active_revenue_by_nested_subcategory numeric null,
    avg_inactive_revenue_by_nested_subcategory numeric null,
    avg_total_revenue_by_nested_subcategory numeric null,
    max_active_revenue_by_seller numeric not null,
    max_inactive_revenue_by_seller numeric not null,
    max_total_revenue_by_seller numeric not null,
    max_revenue_by_gig numeric not null,
    max_active_revenue_by_category numeric not null,
    max_inactive_revenue_by_category numeric not null,
    max_total_revenue_by_category numeric not null,
    max_active_revenue_by_subcategory numeric not null,
    max_inactive_revenue_by_subcategory numeric not null,
    max_total_revenue_by_subcategory numeric not null,
    max_active_revenue_by_nested_subcategory numeric null,
    max_inactive_revenue_by_nested_subcategory numeric null,
    max_total_revenue_by_nested_subcategory numeric null,
    min_total_historical_revenue_by_seller numeric not null,
    min_total_historical_revenue_by_gig numeric not null,
    min_total_historical_revenue_by_category numeric not null,
    min_total_historical_revenue_by_subcategory numeric not null,
    min_total_historical_revenue_by_nested_subcategory numeric null,
    avg_total_historical_revenue_by_seller numeric not null,
    avg_total_historical_revenue_by_gig numeric not null,
    avg_total_historical_revenue_by_category numeric not null,
    avg_total_historical_revenue_by_subcategory numeric not null,
    avg_total_historical_revenue_by_nested_subcategory numeric null,
    max_total_historical_revenue_by_seller numeric not null,
    max_total_historical_revenue_by_gig numeric not null,
    max_total_historical_revenue_by_category numeric not null,
    max_total_historical_revenue_by_subcategory numeric not null,
    max_total_historical_revenue_by_nested_subcategory numeric null,
    revenue_growth_percent_by_seller numeric not null,
    revenue_growth_percent_by_gig numeric not null,
    revenue_growth_percent_by_category numeric not null,
    revenue_growth_percent_by_subcategory numeric not null,
    revenue_growth_percent_by_nested_subcategory numeric null,
    trend_by_seller numeric not null,
    r2_by_seller numeric not null,
    trends_avg_revenue_by_seller numeric not null,
    trend_by_gig numeric not null,
    r2_by_gig numeric not null,
    trends_avg_revenue_by_gig numeric not null,
    trend_by_category numeric not null,
    r2_by_category numeric not null,
    trends_avg_revenue_by_category numeric not null,
    trend_by_subcategory numeric not null,
    r2_by_subcategory numeric not null,
    trends_avg_revenue_by_subcategory numeric not null,
    trend_by_nested_subcategory numeric null,
    r2_by_nested_subcategory numeric null,
    trends_avg_revenue_by_nested_subcategory numeric null,
    rcrit numeric not null,
    seller_level text not null,
    seller_name text not null,
    agency_slug text null,
    agency_status text null,
    gig_title text null,
    gig_cached_slug text not null,
    category_name text not null,
    sub_category_name text not null,
    nested_sub_category_name text null,
    seller_country text null,
    seller_country_code text null,
    seller_languages text[] null,
    seller_profile_image text null,
    gig_preview_url text not null
);

alter table if exists etl.metrics add primary key (gig_id);
create index if not exists idx_seller_id on etl.metrics (seller_id);
create index if not exists idx_category_id on etl.metrics (category_id);
create index if not exists idx_sub_category_id on etl.metrics (sub_category_id);
create index if not exists idx_nested_sub_category_id on etl.metrics (nested_sub_category_id);
create index if not exists idx_categories on etl.metrics (category_id, sub_category_id, nested_sub_category_id);


-- Direct subscription
    -- Replace PWD with role password and SSLROOTCERTPATH with a full local path to certificate file

    -- Link to download AWS RDS SSL certificate bundle - https://truststore.pki.rds.amazonaws.com/eu-central-1/eu-central-1-bundle.pem
    -- Root certificate must be placed in a local folder accessible by PostgreSQL
        -- For example, use local PG data directory (can be found with SQL command on replica ```show data_directory;```)

drop subscription if exists etl_tech cascade;

create subscription etl_tech
    connection 'host=db-raw-1.cr82xjq3sz3n.eu-central-1.rds.amazonaws.com port=5432 user=etl_read_only dbname=raw_1 password=PWD sslrootcert=SSLROOTCERTPATH sslmode=verify-full'
    publication etl_tech;


-- Proxy subscription
    -- Replace PWD with role password

drop subscription if exists etl_tech cascade;

create subscription etl_tech
    connection 'host=etl-proxy.proxy-cr82xjq3sz3n.eu-central-1.rds.amazonaws.com port=5432 user=etl_read_only dbname=raw_1 password=PWD sslmode=require'
    publication etl_tech;