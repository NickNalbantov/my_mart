------------------------------------------------------------------------
-- Views
------------------------------------------------------------------------

-- Last log
create or replace view etl_log.last_run_attributes_stats as
    select l.*
    from etl_log.etl_attributes_stats l
    join
    (
        select max(load_id) as load_id
        from etl_log.etl_attributes_stats
    ) m
        on l.load_id = m.load_id;


-- last run attributes stats vs average and extremums

create or replace view etl_log.last_run_attributes_stats_metrics as

    with last_run as
    (
        select
            processed_dttm as last_processed_dttm,
            load_id as last_load_id,
            cnt_null as last_cnt_null,
            cnt_not_null as last_cnt_not_null,
            cnt_distinct as last_cnt_distinct,
            cnt_zero as last_cnt_zero,
            cnt_below_zero as last_cnt_below_zero,
            cnt_true as last_cnt_true,
            cnt_false as last_cnt_false,
            attribute_number,
            min_value as last_min_value,
            avg_value as last_avg_value,
            max_value as last_max_value,
            attribute_name,
            attribute_type,
            tgt_schema,
            tgt_name,
            src_schema,
            src_name
        from etl_log.last_run_attributes_stats
    ),

    agg as
    (
        select
            min(l.cnt_null) as min_cnt_null,
            avg(l.cnt_null) as avg_cnt_null,
            max(l.cnt_null) as max_cnt_null,
            min(l.cnt_not_null) as min_cnt_not_null,
            avg(l.cnt_not_null) as avg_cnt_not_null,
            max(l.cnt_not_null) as max_cnt_not_null,
            min(l.cnt_distinct) as min_cnt_distinct,
            avg(l.cnt_distinct) as avg_cnt_distinct,
            max(l.cnt_distinct) as max_cnt_distinct,
            min(l.cnt_zero) as min_cnt_zero,
            avg(l.cnt_zero) as avg_cnt_zero,
            max(l.cnt_zero) as max_cnt_zero,
            min(l.cnt_below_zero) as min_cnt_below_zero,
            avg(l.cnt_below_zero) as avg_cnt_below_zero,
            max(l.cnt_below_zero) as max_cnt_below_zero,
            min(l.cnt_true) as min_cnt_true,
            avg(l.cnt_true) as avg_cnt_true,
            max(l.cnt_true) as max_cnt_true,
            min(l.cnt_false) as min_cnt_false,
            avg(l.cnt_false) as avg_cnt_false,
            max(l.cnt_false) as max_cnt_false,
            min(l.min_value) as min_min_value,
            avg(l.min_value) as avg_min_value,
            max(l.min_value) as max_min_value,
            min(l.avg_value) as min_avg_value,
            avg(l.avg_value) as avg_avg_value,
            max(l.avg_value) as max_avg_value,
            min(l.max_value) as min_max_value,
            avg(l.max_value) as avg_max_value,
            max(l.max_value) as max_max_value,
            l.attribute_name,
            l.tgt_schema,
            l.tgt_name,
            l.src_schema,
            l.src_name
        from etl_log.etl_attributes_stats l
        join last_run r
            on l.src_schema = r.src_schema
            and l.src_name = r.src_name
            and l.tgt_schema = r.tgt_schema
            and l.tgt_name = r.tgt_name
            and l.attribute_name = r.attribute_name
            and l.load_id < r.last_load_id -- excluding last run from aggregated history stats
            and r.last_processed_dttm - l.processed_dttm <= '30 days'::interval -- excluding old runs from aggregated history stats
        group by
            l.attribute_name,
            l.tgt_schema,
            l.tgt_name,
            l.src_schema,
            l.src_name
    )

    select
        l.last_processed_dttm,
        l.last_load_id,
        l.last_cnt_null,
        a.min_cnt_null,
        a.avg_cnt_null,
        a.max_cnt_null,
        l.last_cnt_not_null,
        a.min_cnt_not_null,
        a.avg_cnt_not_null,
        a.max_cnt_not_null,
        l.last_cnt_distinct,
        a.min_cnt_distinct,
        a.avg_cnt_distinct,
        a.max_cnt_distinct,
        l.last_cnt_zero,
        a.min_cnt_zero,
        a.avg_cnt_zero,
        a.max_cnt_zero,
        l.last_cnt_below_zero,
        a.min_cnt_below_zero,
        a.avg_cnt_below_zero,
        a.max_cnt_below_zero,
        l.last_cnt_true,
        a.min_cnt_true,
        a.avg_cnt_true,
        a.max_cnt_true,
        l.last_cnt_false,
        a.min_cnt_false,
        a.avg_cnt_false,
        a.max_cnt_false,
        l.last_min_value,
        a.min_min_value,
        a.avg_min_value,
        a.max_min_value,
        l.last_avg_value,
        a.min_avg_value,
        a.avg_avg_value,
        a.max_avg_value,
        l.last_max_value,
        a.min_max_value,
        a.avg_max_value,
        a.max_max_value,
        l.attribute_number,
        l.attribute_name,
        l.attribute_type,
        l.tgt_schema,
        l.tgt_name,
        l.src_schema,
        l.src_name
    from last_run l
    left join agg a
        using (src_schema, src_name, tgt_schema, tgt_name, attribute_name);


------------------------------------------------------------------------
-- Functions
------------------------------------------------------------------------

-- run attributes stats vs average and extremums for a chosen load_id and history depth lookup
    -- view etl_log.last_run_attributes_stats_metrics is an analogue of this function for the latest load_id in etl_log.etl_attributes_stats and hist_depth_days = 30

create or replace function etl_log.run_attributes_stats_metrics
(
    load_id bigint,
    hist_depth_days smallint default 30
)
returns table
(
    run_processed_dttm timestamptz,
    run_load_id bigint,
    run_cnt_null bigint,
    min_cnt_null bigint,
    avg_cnt_null numeric,
    max_cnt_null bigint,
    run_cnt_not_null bigint,
    min_cnt_not_null bigint,
    avg_cnt_not_null numeric,
    max_cnt_not_null bigint,
    run_cnt_distinct bigint,
    min_cnt_distinct bigint,
    avg_cnt_distinct numeric,
    max_cnt_distinct bigint,
    run_cnt_zero bigint,
    min_cnt_zero bigint,
    avg_cnt_zero numeric,
    max_cnt_zero bigint,
    run_cnt_below_zero bigint,
    min_cnt_below_zero bigint,
    avg_cnt_below_zero numeric,
    max_cnt_below_zero bigint,
    run_cnt_true bigint,
    min_cnt_true bigint,
    avg_cnt_true numeric,
    max_cnt_true bigint,
    run_cnt_false bigint,
    min_cnt_false bigint,
    avg_cnt_false numeric,
    max_cnt_false bigint,
    run_min_value numeric,
    min_min_value numeric,
    avg_min_value numeric,
    max_min_value numeric,
    run_avg_value numeric,
    min_avg_value numeric,
    avg_avg_value numeric,
    max_avg_value numeric,
    run_max_value numeric,
    min_max_value numeric,
    avg_max_value numeric,
    max_max_value numeric,
    attribute_number smallint,
    attribute_name text,
    attribute_type text,
    tgt_schema text,
    tgt_name text,
    src_schema text,
    src_name text
)
language sql
strict
rows 263

    begin atomic

        with run as
        (
            select
                processed_dttm as run_processed_dttm,
                $1 as run_load_id,
                cnt_null as run_cnt_null,
                cnt_not_null as run_cnt_not_null,
                cnt_distinct as run_cnt_distinct,
                cnt_zero as run_cnt_zero,
                cnt_below_zero as run_cnt_below_zero,
                cnt_true as run_cnt_true,
                cnt_false as run_cnt_false,
                attribute_number,
                min_value as run_min_value,
                avg_value as run_avg_value,
                max_value as run_max_value,
                attribute_name,
                attribute_type,
                tgt_schema,
                tgt_name,
                src_schema,
                src_name
            from etl_log.etl_attributes_stats
            where
                load_id = $1
        ),

        agg as
        (
            select
                min(l.cnt_null) as min_cnt_null,
                avg(l.cnt_null) as avg_cnt_null,
                max(l.cnt_null) as max_cnt_null,
                min(l.cnt_not_null) as min_cnt_not_null,
                avg(l.cnt_not_null) as avg_cnt_not_null,
                max(l.cnt_not_null) as max_cnt_not_null,
                min(l.cnt_distinct) as min_cnt_distinct,
                avg(l.cnt_distinct) as avg_cnt_distinct,
                max(l.cnt_distinct) as max_cnt_distinct,
                min(l.cnt_zero) as min_cnt_zero,
                avg(l.cnt_zero) as avg_cnt_zero,
                max(l.cnt_zero) as max_cnt_zero,
                min(l.cnt_below_zero) as min_cnt_below_zero,
                avg(l.cnt_below_zero) as avg_cnt_below_zero,
                max(l.cnt_below_zero) as max_cnt_below_zero,
                min(l.cnt_true) as min_cnt_true,
                avg(l.cnt_true) as avg_cnt_true,
                max(l.cnt_true) as max_cnt_true,
                min(l.cnt_false) as min_cnt_false,
                avg(l.cnt_false) as avg_cnt_false,
                max(l.cnt_false) as max_cnt_false,
                min(l.min_value) as min_min_value,
                avg(l.min_value) as avg_min_value,
                max(l.min_value) as max_min_value,
                min(l.avg_value) as min_avg_value,
                avg(l.avg_value) as avg_avg_value,
                max(l.avg_value) as max_avg_value,
                min(l.max_value) as min_max_value,
                avg(l.max_value) as avg_max_value,
                max(l.max_value) as max_max_value,
                l.attribute_name,
                l.tgt_schema,
                l.tgt_name,
                l.src_schema,
                l.src_name
            from etl_log.etl_attributes_stats l
            join run r
                on l.src_schema = r.src_schema
                and l.src_name = r.src_name
                and l.tgt_schema = r.tgt_schema
                and l.tgt_name = r.tgt_name
                and l.attribute_name = r.attribute_name
                and r.run_processed_dttm - l.processed_dttm <= ($2 || ' days')::interval -- excluding old runs from aggregated history stats
            where
                l.load_id < $1 -- excluding last run from aggregated history stats
            group by
                l.attribute_name,
                l.tgt_schema,
                l.tgt_name,
                l.src_schema,
                l.src_name
        )

        select
            r.run_processed_dttm,
            r.run_load_id,
            r.run_cnt_null,
            a.min_cnt_null,
            a.avg_cnt_null,
            a.max_cnt_null,
            r.run_cnt_not_null,
            a.min_cnt_not_null,
            a.avg_cnt_not_null,
            a.max_cnt_not_null,
            r.run_cnt_distinct,
            a.min_cnt_distinct,
            a.avg_cnt_distinct,
            a.max_cnt_distinct,
            r.run_cnt_zero,
            a.min_cnt_zero,
            a.avg_cnt_zero,
            a.max_cnt_zero,
            r.run_cnt_below_zero,
            a.min_cnt_below_zero,
            a.avg_cnt_below_zero,
            a.max_cnt_below_zero,
            r.run_cnt_true,
            a.min_cnt_true,
            a.avg_cnt_true,
            a.max_cnt_true,
            r.run_cnt_false,
            a.min_cnt_false,
            a.avg_cnt_false,
            a.max_cnt_false,
            r.run_min_value,
            a.min_min_value,
            a.avg_min_value,
            a.max_min_value,
            r.run_avg_value,
            a.min_avg_value,
            a.avg_avg_value,
            a.max_avg_value,
            r.run_max_value,
            a.min_max_value,
            a.avg_max_value,
            a.max_max_value,
            r.attribute_number,
            r.attribute_name,
            r.attribute_type,
            r.tgt_schema,
            r.tgt_name,
            r.src_schema,
            r.src_name
        from run r
        left join agg a
            using (src_schema, src_name, tgt_schema, tgt_name, attribute_name);

    end;