-- Referenced load_id, by default - latest load_id from runs

select distinct
    load_id
from etl_log.etl_log
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
    and processed_dttm <= $__timeTo()
    and $__timeTo() - processed_dttm <= '${hist_depth_days} days'::interval;
-- order by load_id desc;

-- DB rows queries

select
    count(id) as "value",
    date(scraped_at) as "time"
from public.sellers
where
    scraped_at is not null
    and scraped_at <= $__timeTo()
group by
    date(scraped_at)
order by
    date(scraped_at) desc
limit ${hist_depth_days};

select
    count(id) as "value",
    date(scraped_at) as "time"
from public.gigs
where
    scraped_at is not null
    and scraped_at <= $__timeTo()
group by
    date(scraped_at)
order by
    date(scraped_at) desc
limit ${hist_depth_days};

select
    count(id) as "value",
    date(created_at) as "time"
from public.gig_reviews_meta
where
    created_at is not null
    and created_at <= $__timeTo()
group by
    date(created_at)
order by
    date(created_at) desc
limit ${hist_depth_days};

select
    count(id) as "value",
    date(created_at) as "time"
from public.gigs_revisions_meta
where
    created_at is not null
    and created_at <= $__timeTo()
group by
    date(created_at)
order by
    date(created_at) desc
limit ${hist_depth_days};

select
    count(id) as "value",
    date(created_at) as "time"
from public.gigs_pages_meta
where
    created_at is not null
    and created_at <= $__timeTo()
group by
    date(created_at)
order by
    date(created_at) desc
limit ${hist_depth_days};

select
    count(id) as "value",
    date(created_at) as "time"
from public.sellers_revisions_meta
where
    created_at is not null
    and created_at <= $__timeTo()
group by
    date(created_at)
order by
    date(created_at) desc
limit ${hist_depth_days};

-- Targets' (history tables) processed row counts for ${load_id} run compared to ${hist_depth_days}-days history
-- Comparison between amount of processed rows in history tables for run with load_id=$load_id and their corresponding $hist_depth_days-days average values ana extremums

select
    tgt_schema || '.' || tgt_name || ' | ' || op_type as target,
    run_row_count,
    min_row_count,
    avg_row_count,
    max_row_count
from etl_log.run_rows_metrics(${load_id}::bigint, ${hist_depth_days}::smallint)
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
    and tgt_name = any(array[
            'gig_status_history', 'seller_status_history', 'gig_reviews_agg', 'gig_reviews_prices'
        ])
order by run_processed_dttm;


-- Targets' (valid revisions tables) processed row counts for ${load_id} run compared to ${hist_depth_days}-days history
-- Comparison between amount of processed rows in valid revisions tables for run with load_id=$load_id and their corresponding $hist_depth_days-days average values ana extremums

select
    tgt_schema || '.' || tgt_name || ' | ' || op_type as target,
    run_row_count,
    min_row_count,
    avg_row_count,
    max_row_count
from etl_log.run_rows_metrics(${load_id}::bigint, ${hist_depth_days}::smallint)
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
    and tgt_name = any(array[
            'gigs_revisions_pre_metrics', 'gigs_pages_pre_metrics', 'sellers_revisions_pre_metrics'
        ])
order by
    run_processed_dttm;


-- Targets' (invalid revisions tables) processed row counts for ${load_id} run compared to ${hist_depth_days}-days history
-- Comparison between amount of processed rows in invalid revisions tables for run with load_id=$load_id and their corresponding $hist_depth_days-days average values ana extremums

select
    tgt_schema || '.' || tgt_name || ' | ' || op_type as target,
    run_row_count,
    min_row_count,
    avg_row_count,
    max_row_count
from etl_log.run_rows_metrics(${load_id}::bigint, ${hist_depth_days}::smallint)
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
    and tgt_name = any(array[
            'gigs_revisions_pre_metrics_invalid', 'gigs_pages_pre_metrics_invalid', 'sellers_revisions_pre_metrics_invalid'
        ])
order by
    run_processed_dttm;


-- Targets' (gigs & sellers) processed row counts for ${load_id} run compared to ${hist_depth_days}-days history
-- Comparison between amount of processed rows in ETL's targets (gigs and sellers data) for run with load_id=$load_id and their corresponding $hist_depth_days-days average values ana extremums

select
    tgt_schema || '.' || tgt_name as target,
    run_row_count,
    min_row_count,
    avg_row_count,
    max_row_count
from etl_log.run_rows_metrics(${load_id}::bigint, ${hist_depth_days}::smallint)
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
    and tgt_name = any(array['metrics', 'gigs', 'sellers'])
order by
    run_processed_dttm;


-- targets' (categories) last run rows metrics
-- Comparison between amount of processed rows in ETL's targets (categories data) for run with load_id=$load_id and their corresponding $hist_depth_days-days average values ana extremums

select
    tgt_schema || '.' || tgt_name as target,
    run_row_count,
    min_row_count,
    avg_row_count,
    max_row_count
from etl_log.run_rows_metrics(${load_id}::bigint, ${hist_depth_days}::smallint)
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
    and tgt_name = any(array[
            'all_categories', 'categories', 'subcategories', 'nested_subcategories',
            'category_x_seller_levels', 'subcategory_x_seller_levels', 'nested_subcategory_x_seller_levels'
        ])
order by
    run_processed_dttm;


-- Functions' runtime for ${load_id} run compared to ${hist_depth_days}-days history
-- Comparison between duration (in seconds) of ETL-functions' runtimes for run with load_id=$load_id and their corresponding $hist_depth_days-days average values and extremums

select
    src_schema || '.' || src_name as "function",
    extract(epoch from run_time) as run_time,
    extract(epoch from min_run_time) as min_run_time,
    extract(epoch from avg_run_time) as avg_run_time,
    extract(epoch from max_run_time) as max_run_time
from etl_log.run_runtime_metrics(${load_id}::bigint, ${hist_depth_days}::smallint) r
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
order by
    run_end_dttm;


-- Functions' runtime duration values in seconds in last ${hist_depth_days} days
-- Time series for ETL-functions' runtime statistics in last $hist_depth_days

select
    extract(epoch from avg(run_time)) as run_time,
    src_schema || '.' || src_name as "function",
    to_timestamp(load_id::text || '+00:00', 'yyyymmddhh24miss+tzh:tzm') as run_dttm
from etl_log.etl_log
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
    and op_type = 'end'
    and $__timeTo() - processed_dttm <= '${hist_depth_days} days'::interval
group by
    load_id,
    src_schema,
    src_name,
    src_step
order by
    src_step,
    load_id;


-- Targets' (history tables) processed row counts in last ${hist_depth_days} days
-- Time series for history tables' processed row counts in last $hist_depth_days

select
    row_count,
    tgt_schema || '.' || tgt_name || ' | ' || op_type as target,
    to_timestamp(load_id::text || '+00:00', 'yyyymmddhh24miss+tzh:tzm') as run_dttm
from etl_log.etl_log
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
    and tgt_name = any(array[
            'gig_status_history', 'seller_status_history', 'gig_reviews_agg', 'gig_reviews_prices'
        ])
    and op_type = any(array['insert', 'update', 'merge', 'delete'])
    and $__timeTo() - processed_dttm <= '${hist_depth_days} days'::interval
order by
    load_id,
    processed_dttm;


-- Targets' (valid revisions tables) processed row counts in last ${hist_depth_days} days
-- Time series for valid revisions tables' processed row counts in last $hist_depth_days

select
    row_count,
    tgt_schema || '.' || tgt_name || ' | ' || op_type as target,
    to_timestamp(load_id::text || '+00:00', 'yyyymmddhh24miss+tzh:tzm') as run_dttm
from etl_log.etl_log
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
    and tgt_name = any(array[
            'gigs_revisions_pre_metrics', 'gigs_pages_pre_metrics', 'sellers_revisions_pre_metrics'
        ])
    and op_type = any(array['insert', 'update', 'merge', 'delete'])
    and $__timeTo() - processed_dttm <= '${hist_depth_days} days'::interval
order by
    load_id,
    processed_dttm;


-- Targets' (invalid revisions tables) processed row counts in last ${hist_depth_days} days
-- Time series for invalid revisions tables' processed row counts in last $hist_depth_days

select
    row_count,
    tgt_schema || '.' || tgt_name || ' | ' || op_type as target,
    to_timestamp(load_id::text || '+00:00', 'yyyymmddhh24miss+tzh:tzm') as run_dttm
from etl_log.etl_log
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
    and tgt_name = any(array[
            'gigs_revisions_pre_metrics_invalid', 'gigs_pages_pre_metrics_invalid', 'sellers_revisions_pre_metrics_invalid'
        ])
    and op_type = any(array['insert', 'update', 'merge', 'delete'])
    and $__timeTo() - processed_dttm <= '${hist_depth_days} days'::interval
order by
    load_id,
    processed_dttm;


-- Targets' (gigs & sellers) processed row counts in last ${hist_depth_days} days
-- Time series for ETL targets' (gigs & sellers) processed row counts in last $hist_depth_days

select
    row_count,
    tgt_schema || '.' || tgt_name as target,
    to_timestamp(load_id::text || '+00:00', 'yyyymmddhh24miss+tzh:tzm') as run_dttm
from etl_log.etl_log
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
    and op_type = any(array['insert', 'update', 'merge', 'delete'])
    and tgt_name = any(array['metrics', 'gigs', 'sellers'])
    and $__timeTo() - processed_dttm <= '${hist_depth_days} days'::interval
order by
    load_id,
    processed_dttm;


-- Targets' (categories) processed row counts in last ${hist_depth_days} days
-- Time series for ETL targets' (categories) processed row counts in last $hist_depth_days

select
    row_count,
    tgt_schema || '.' || tgt_name as target,
    to_timestamp(load_id::text || '+00:00', 'yyyymmddhh24miss+tzh:tzm') as run_dttm
from etl_log.etl_log
where
    src_schema = 'etl'
    and dag_name = 'etl_regular'
    and op_type = any(array['insert', 'update', 'merge', 'delete'])
    and tgt_name = any(array[
            'all_categories', 'categories', 'subcategories', 'nested_subcategories',
            'category_x_seller_levels', 'subcategory_x_seller_levels', 'nested_subcategory_x_seller_levels'
        ])
    and $__timeTo() - processed_dttm <= '${hist_depth_days} days'::interval
order by
    load_id,
    processed_dttm;