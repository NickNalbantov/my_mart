------------------------------------------------------------------------
-- Views
------------------------------------------------------------------------

-- Last log

drop view if exists etl_log.last_run_log cascade;

create or replace view etl_log.last_run_log as
    select
        l.*
    from etl_log.etl_log l
    join
    (
        select max(load_id) as load_id
        from etl_log.etl_log
    ) m
        on l.load_id = m.load_id;


-- regular_etl_sequential / pre_materialization last run inserts/updates row counts upon targets vs average and extremums

drop view if exists etl_log.last_run_rows_metrics cascade;

create or replace view etl_log.last_run_rows_metrics as

    with last_run as
    (
        select
            processed_dttm as last_processed_dttm,
            load_id as last_load_id,
            row_count as last_row_count,
            dag_name,
            src_schema,
            src_name,
            src_step,
            tgt_schema,
            tgt_name,
            op_type
        from etl_log.last_run_log
        where
            op_type in ('insert', 'update', 'merge', 'delete')
            and substring(tgt_name, '(etl_attributes_stats|(dev_|)trends_prefin|_backup)$') is null
    ),

    agg as
    (
        select
            min(l.row_count) as min_row_count,
            avg(l.row_count) as avg_row_count,
            max(l.row_count) as max_row_count,
            l.dag_name,
            l.src_schema,
            l.src_name,
            l.src_step,
            l.tgt_schema,
            l.tgt_name,
            l.op_type
        from etl_log.etl_log l
        join last_run r
            on l.dag_name = r.dag_name
            and l.src_schema = r.src_schema
            and l.src_name = r.src_name
            and l.tgt_schema = r.tgt_schema
            and l.tgt_name = r.tgt_name
            and l.op_type = r.op_type
            and l.load_id < r.last_load_id -- excluding last run from aggregated history stats
            and r.last_processed_dttm - l.processed_dttm <= '30 days'::interval -- excluding old runs from aggregated history stats
        where
            l.op_type in ('insert', 'update', 'merge', 'delete')
            and substring(l.tgt_name, '(etl_attributes_stats|(dev_|)trends_prefin|_backup)$') is null
        group by
            l.dag_name,
            l.src_schema,
            l.src_name,
            l.src_step,
            l.tgt_schema,
            l.tgt_name,
            l.op_type
    )

    select
        l.last_processed_dttm,
        l.last_load_id,
        l.last_row_count,
        a.min_row_count,
        a.avg_row_count,
        a.max_row_count,
        l.dag_name,
        l.src_schema,
        l.src_name,
        l.src_step,
        l.tgt_schema,
        l.tgt_name,
        l.op_type
    from last_run l
    left join agg a using (dag_name, src_schema, src_name, src_step, tgt_schema, tgt_name, op_type);


-- All functions last run runtimes vs average and extremums

drop view if exists etl_log.last_run_runtime_metrics cascade;

create or replace view etl_log.last_run_runtime_metrics as

    with last_run as
    (
        select
            avg(run_time) as last_run_time,
            max(processed_dttm) as last_run_end_dttm,
            load_id as last_load_id,
            dag_name,
            src_schema,
            src_name
        from etl_log.last_run_log
        where
            op_type = 'end'
        group by
            load_id,
            dag_name,
            src_schema,
            src_name
    ),

    agg as
    (
        select
            min(l.run_time) as min_run_time,
            avg(l.run_time) as avg_run_time,
            max(l.run_time) as max_run_time,
            l.dag_name,
            l.src_schema,
            l.src_name
        from etl_log.etl_log l
        join last_run r
            on l.dag_name = r.dag_name
            and l.src_schema = r.src_schema
            and l.src_name = r.src_name
            and l.load_id < r.last_load_id -- excluding last run from aggregated history stats
            and r.last_run_end_dttm - l.processed_dttm <= '30 days'::interval -- excluding old runs from aggregated history stats
        where
            l.op_type = 'end'
        group by
            l.dag_name,
            l.src_schema,
            l.src_name
    )

    select
        l.last_run_time,
        a.min_run_time,
        a.avg_run_time,
        a.max_run_time,
        l.last_run_end_dttm,
        l.last_load_id,
        l.dag_name,
        l.src_schema,
        l.src_name
    from last_run l
    left join agg a
        using (dag_name, src_schema, src_name);


------------------------------------------------------------------------
-- Functions
------------------------------------------------------------------------

-- regular_etl_sequential / pre_materialization run inserts/updates row counts upon targets vs average and extremums for a chosen load_id and history depth lookup
    -- view etl_log.last_run_rows_metrics is an analogue of this function for the latest load_id in etl_log.etl_log and hist_depth_days = 30

drop function if exists etl_log.run_rows_metrics cascade;

create or replace function etl_log.run_rows_metrics
(
    load_id bigint,
    hist_depth_days smallint default 30
)
returns table
(
    run_processed_dttm timestamptz,
    run_load_id bigint,
    run_row_count bigint,
    min_row_count bigint,
    avg_row_count bigint,
    max_row_count bigint,
    dag_name text,
    src_schema text,
    src_name text,
    src_step text,
    tgt_schema text,
    tgt_name text,
    op_type text
)
language sql
strict
rows 22

    begin atomic

        with run as
        (
            select
                processed_dttm as run_processed_dttm,
                $1 as run_load_id,
                row_count as run_row_count,
                dag_name,
                src_schema,
                src_name,
                src_step,
                tgt_schema,
                tgt_name,
                op_type
            from etl_log.etl_log
            where
                load_id = $1
                and op_type in ('insert', 'update', 'merge', 'delete')
                and substring(tgt_name, '(etl_attributes_stats|(dev_|)trends_prefin|_backup)$') is null
        ),

        agg as
        (
            select
                min(l.row_count) as min_row_count,
                avg(l.row_count) as avg_row_count,
                max(l.row_count) as max_row_count,
                l.dag_name,
                l.src_schema,
                l.src_name,
                l.src_step,
                l.tgt_schema,
                l.tgt_name,
                l.op_type
            from etl_log.etl_log l
            join run r
                on l.dag_name = r.dag_name
                and l.src_schema = r.src_schema
                and l.src_name = r.src_name
                and l.tgt_schema = r.tgt_schema
                and l.tgt_name = r.tgt_name
                and l.op_type = r.op_type
                and r.run_processed_dttm - l.processed_dttm <= ($2 || ' days')::interval -- excluding old runs from aggregated history stats
            where
                l.op_type in ('insert', 'update', 'merge', 'delete')
                and substring(l.tgt_name, '(etl_attributes_stats|(dev_|)trends_prefin|_backup)$') is null
                and l.load_id < $1 -- excluding last run from aggregated history stats
            group by
                l.dag_name,
                l.src_schema,
                l.src_name,
                l.src_step,
                l.tgt_schema,
                l.tgt_name,
                l.op_type
        )

        select
            r.run_processed_dttm,
            r.run_load_id,
            r.run_row_count,
            a.min_row_count,
            a.avg_row_count,
            a.max_row_count,
            r.dag_name,
            r.src_schema,
            r.src_name,
            r.src_step,
            r.tgt_schema,
            r.tgt_name,
            r.op_type
        from run r
        left join agg a
            using (dag_name, src_schema, src_name, src_step, tgt_schema, tgt_name, op_type);

    end;


-- All functions last run runtimes vs average and extremums for a chosen load_id and history depth lookup
    -- view etl_log.last_run_runtime_metrics is an analogue of this function for the latest load_id in etl_log.etl_log and hist_depth_days = 30

drop function if exists etl_log.run_runtime_metrics cascade;

create or replace function etl_log.run_runtime_metrics
(
    load_id bigint,
    hist_depth_days smallint default 30
)
returns table
(
    run_time interval,
    min_run_time interval,
    avg_run_time interval,
    max_run_time interval,
    run_end_dttm timestamptz,
    run_load_id bigint,
    dag_name text,
    src_schema text,
    src_name text
)
language sql
strict
rows 128

    begin atomic

        with run as
        (
            select
                avg(run_time) as run_time,
                max(processed_dttm) as run_end_dttm,
                $1 as run_load_id,
                dag_name,
                src_schema,
                src_name
            from etl_log.etl_log
            where
                load_id = $1
                and op_type = 'end'
            group by
                dag_name,
                src_schema,
                src_name
        ),

        agg as
        (
            select
                min(l.run_time) as min_run_time,
                avg(l.run_time) as avg_run_time,
                max(l.run_time) as max_run_time,
                l.dag_name,
                l.src_schema,
                l.src_name
            from etl_log.etl_log l
            join run r
                on l.dag_name = r.dag_name
                and l.src_schema = r.src_schema
                and l.src_name = r.src_name
                and r.run_end_dttm - l.processed_dttm <= ($2 || ' days')::interval -- excluding old runs from aggregated history stats
            where
                l.op_type = 'end'
                and l.load_id < $1 -- excluding last run from aggregated history stats
            group by
                l.dag_name,
                l.src_schema,
                l.src_name
        )

        select
            r.run_time,
            a.min_run_time,
            a.avg_run_time,
            a.max_run_time,
            r.run_end_dttm,
            r.run_load_id,
            r.dag_name,
            r.src_schema,
            r.src_name
        from run r
        left join agg a
            using (dag_name, src_schema, src_name);

    end;