create schema if not exists etl_log;

-- Log table

-- drop table if exists etl_log.etl_log cascade;
create table if not exists etl_log.etl_log
(
    run_time interval,
    processed_dttm timestamptz not null default clock_timestamp(),
    id bigint generated always as identity primary key,
    load_id bigint not null,
    row_count bigint,
    debug_flg boolean,
    log_type text not null,
    dag_name text not null,
    src_schema text,
    src_name text,
    src_step text,
    tgt_schema text,
    tgt_name text,
    op_type text,
    call_place text,
    log_message text
);

create index if not exists idx_etl_log_processed_dttm_desc on etl_log.etl_log (processed_dttm desc);
create index if not exists idx_etl_log_load_id on etl_log.etl_log (load_id);
create index if not exists idx_etl_log_targets on etl_log.etl_log (dag_name, tgt_schema, tgt_name, op_type, load_id desc);
create index if not exists idx_etl_log_sources on etl_log.etl_log (dag_name, src_schema, src_name, op_type, load_id desc);
create index if not exists idx_partial_etl_log_errors on etl_log.etl_log (processed_dttm desc) where (log_type = 'ERROR');

alter table if exists etl_log.etl_log
    replica identity using index etl_log_pkey;


-- Logger
create or replace function etl_log.etl_log_writer
(
    runtime interval default null,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    row_cnt bigint default null,
    debug_flg boolean default null,
    log_type text default 'NOTICE',
    dag_name text default 'manual_launch',
    src_schema text default null,
    src_name text default null,
    src_step text default null,
    tgt_schema text default null,
    tgt_name text default null,
    op_type text default null,
    call_place text default null,
    msg text default null
)
returns void
language plpgsql
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        qry text;
        conn_name text;

    begin

        -- Run autonomous transaction for logging

        conn_name = load_id || '_'
            || substring
                (
                    regexp_replace
                        (
                            src_name,
                            '^af_(agg_|)(mj_prep_|trends_|premat_|metrics_|trend_periods_|mj_fin_)',
                            ''
                        ),
                    1,
                    45
                )
            || '_' || random(0, 99);

        qry := format($$
            insert into etl_log.etl_log
            (
                run_time,
                load_id,
                row_count,
                debug_flg,
                log_type,
                dag_name,
                src_schema,
                src_name,
                src_step,
                tgt_schema,
                tgt_name,
                op_type,
                call_place,
                log_message
            )
                values (%L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L)$$,
            runtime,
            load_id,
            row_cnt,
            debug_flg,
            log_type,
            dag_name,
            src_schema,
            src_name,
            src_step,
            tgt_schema,
            tgt_name,
            op_type,
            call_place,
            msg
        );

        if conn_name = any(public.dblink_get_connections()) then -- for sequential ETL
            perform public.dblink_exec(conn_name, qry);
        else -- for parallel ETL
            raise warning 'Connection % was not found', conn_name;
            perform public.dblink_connect(conn_name, 'loopback_dblink_' || current_database());
            perform public.dblink_exec(conn_name, qry);
            perform public.dblink_disconnect(conn_name);
        end if;

    end;

$func$;