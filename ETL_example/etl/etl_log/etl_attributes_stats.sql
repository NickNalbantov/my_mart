create schema if not exists etl_log;

-- Target table

-- drop table if exists etl_log.etl_attributes_stats cascade;

create table if not exists etl_log.etl_attributes_stats
(
    processed_dttm timestamptz not null default transaction_timestamp(),
    load_id int8 not null,
    cnt_null int8 not null,
    cnt_not_null int8 not null,
    cnt_distinct int8 not null,
    cnt_zero int8,
    cnt_below_zero int8,
    cnt_true int8,
    cnt_false int8,
    id int generated always as identity primary key,
    attribute_number int2 not null,
    min_value numeric,
    avg_value numeric,
    max_value numeric,
    attribute_name text not null,
    attribute_type text not null,
    tgt_schema text not null,
    tgt_name text not null,
    src_schema text not null,
    src_name text not null
);

create unique index if not exists uidx_etl_attributes_data on etl_log.etl_attributes_stats (load_id, tgt_schema, tgt_name, attribute_number);
create index if not exists idx_etl_att_targets on etl_log.etl_attributes_stats (tgt_schema, tgt_name, load_id desc);
create index if not exists idx_etl_att_sources on etl_log.etl_attributes_stats (src_schema, src_name, load_id desc);
create index if not exists idx_etl_att_attributes on etl_log.etl_attributes_stats (attribute_name, load_id desc);

alter table if exists etl_log.etl_attributes_stats
    replica identity using index etl_attributes_stats_pkey;