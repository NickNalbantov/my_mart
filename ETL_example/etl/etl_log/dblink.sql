create extension if not exists dblink with schema public cascade;

/*
Server name / application name created like 'loopback_dblink_' || current_database() for:
    - raw_1
    - api
    - airflow
*/

do
$func$
    begin
        execute format($fmt$
            create server if not exists loopback_dblink_%1$s
            foreign data wrapper dblink_fdw
            options (host 'db-raw-1.cr82xjq3sz3n.eu-central-1.rds.amazonaws.com', dbname '%1$s', application_name 'loopback_dblink_%1$s');

            create user mapping if not exists for sayjam
            server loopback_dblink_%1$s
            options (user 'sayjam', password 'insert_password_here');
        $fmt$,
        current_database()
        )
    end;
$func$;