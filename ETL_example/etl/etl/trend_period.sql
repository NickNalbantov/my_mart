create or replace function etl.trend_period_func
(
    start_dttm timestamptz,
    end_dttm timestamptz,
    period_num smallint,
    ratings_to_volume_coeff numeric default 0.8, -- arbitrary coefficient representing ratio of reviews conversion to sales
    overpriced_coeff numeric default 10, -- crutch to filter gigs with suspiciously high prices (higher than weighted average price in subcategory multiplied by this coefficient)
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '5.2'
)
returns void
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        run_start_dttm timestamptz;
        total_runtime interval;

        period_width interval;

    begin

        -- Starting

        run_start_dttm := clock_timestamp();
        period_width := end_dttm - start_dttm;

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.' || period_num || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            start_dttm,
            end_dttm,
            period_num,
            ratings_to_volume_coeff,
            overpriced_coeff,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Preparing slices with active gigs and sellers

            -- Step 1.1 - Active gigs

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.trends_gigs_hist_active cascade;

        create unlogged table if not exists etl_wrk.trends_gigs_hist_active
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (gig_id)
                gig_id,
                valid_to_dttm
            from etl.gig_status_history
            where
                valid_to_dttm >= start_dttm
                and valid_from_dttm < end_dttm
                and fiverr_status in ('approved', 'active')
            order by
                gig_id,
                valid_to_dttm desc;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_trends_gigs_hist_active
            on etl_wrk.trends_gigs_hist_active (gig_id) include (valid_to_dttm) with (fillfactor = 100);

        analyze etl_wrk.trends_gigs_hist_active;

        select
            high_level_step || '.' || period_num || '.1' as src_step,
            'etl_wrk' as tgt_schema,
            'trends_gigs_hist_active_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 1.2 - Active sellers

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.trends_sellers_hist_active cascade;

        create unlogged table if not exists etl_wrk.trends_sellers_hist_active
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (seller_id)
                valid_to_dttm,
                seller_id
            from etl.seller_status_history
            where
                valid_to_dttm >= start_dttm
                and valid_from_dttm < end_dttm
                and status = 'available'
            order by
                seller_id,
                valid_to_dttm desc;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_trends_sellers_hist_active
            on etl_wrk.trends_sellers_hist_active (seller_id) include (valid_to_dttm) with (fillfactor = 100);

        analyze etl_wrk.trends_sellers_hist_active;

        select
            high_level_step || '.1.2' as src_step,
            'etl_wrk' as tgt_schema,
            'trends_sellers_hist_active' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 2 - Materializing data for regular gigs

            -- Step 2.1 - Data from current assessed period (regular trends)

                -- Step 2.1.1 - Sellers revisions from current assessed period (regular trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.reg_trend_prd_cur_sellers_revisions cascade;

        create unlogged table if not exists etl_wrk.reg_trend_prd_cur_sellers_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                s.fiverr_seller_id,
                min(s.seller_ratings_count) as min_seller_ratings_count,
                max(s.seller_ratings_count) as max_seller_ratings_count
            from etl.sellers_revisions_pre_metrics s
            where
                s.scraped_at >= start_dttm
                and s.scraped_at < end_dttm
                and exists
                    (
                        select
                        from etl_wrk.trends_sellers_hist_active h
                        where
                            s.seller_id = h.seller_id
                    )
            group by
                s.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_trend_prd_cur_sellers_revisions
            on etl_wrk.reg_trend_prd_cur_sellers_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.reg_trend_prd_cur_sellers_revisions;

        select
            high_level_step || '.' || period_num || '.2.1.1' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_cur_sellers_revisions_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.1.2 - Gigs pages from current assessed period (regular trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.reg_trend_prd_cur_gigs_pages cascade;

        create unlogged table if not exists etl_wrk.reg_trend_prd_cur_gigs_pages
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                p.gig_id,
                p.fiverr_seller_id,
                min(p.seller_completed_orders_count) over (partition by p.fiverr_seller_id) as min_seller_completed_orders_count,
                max(p.seller_completed_orders_count) over (partition by p.fiverr_seller_id) as max_seller_completed_orders_count,
                first_value(p.prices) over (partition by p.gig_id order by p.scraped_at desc, p.id desc) as prices
            from etl.gigs_pages_pre_metrics p
            where
                p.scraped_at >= start_dttm
                and p.scraped_at < end_dttm
                and exists
                    (
                        select
                        from etl_wrk.trends_gigs_hist_active h
                        where
                            p.gig_id = h.gig_id
                    );

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_trend_prd_cur_gigs_pages
            on etl_wrk.reg_trend_prd_cur_gigs_pages (gig_id) with (fillfactor = 100);

        analyze etl_wrk.reg_trend_prd_cur_gigs_pages;

        select
            high_level_step || '.' || period_num || '.2.1.2' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_cur_gigs_pages_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.1.3 - Gigs revisions from current assessed period (regular trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.reg_trend_prd_cur_gigs_revisions cascade;

        create unlogged table if not exists etl_wrk.reg_trend_prd_cur_gigs_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (gig_id)
                g.scraped_at,
                g.gig_id,
                g.fiverr_seller_id,
                g.category_id,
                g.sub_category_id,
                g.nested_sub_category_id,
                coalesce(min(g.gig_ratings_count) over (partition by g.gig_id), 0)::int as min_gig_ratings_count,
                coalesce(max(g.gig_ratings_count) over (partition by g.gig_id), 0)::int as max_gig_ratings_count,
                case when h.valid_to_dttm >= end_dttm then true else false end as gig_is_active,
                g.prices
            from etl.gigs_revisions_pre_metrics g
            join etl_wrk.trends_gigs_hist_active h
                on g.gig_id = h.gig_id
            where
                g.scraped_at >= start_dttm
                and g.scraped_at < end_dttm
            order by
                g.gig_id,
                g.scraped_at desc,
                g.id desc;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_trend_prd_cur_gigs_revisions
            on etl_wrk.reg_trend_prd_cur_gigs_revisions (gig_id) with (fillfactor = 100);
        create index if not exists idx_reg_trend_prd_cur_gigs_revisions_fiverr_seller_id
            on etl_wrk.reg_trend_prd_cur_gigs_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.reg_trend_prd_cur_gigs_revisions;

        select
            high_level_step || '.' || period_num || '.2.1.3' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_cur_gigs_revisions_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.1.4 - Metrics from current assessed period (regular trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.reg_trend_prd_cur_metrics cascade;

        create unlogged table if not exists etl_wrk.reg_trend_prd_cur_metrics
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.scraped_at,
                g.gig_id,
                g.fiverr_seller_id,
                g.category_id,
                g.sub_category_id,
                g.nested_sub_category_id,
                case
                    when g.max_gig_ratings_count = g.min_gig_ratings_count
                        then 0::int
                    else g.max_gig_ratings_count
                end as gig_ratings_count,
                case
                    when
                        greatest(
                            max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                            max(g.max_gig_ratings_count) over w_s
                        )::int =
                        least(
                            min(coalesce(s.min_seller_ratings_count, 0)) over w_s,
                            min(g.min_gig_ratings_count) over w_s
                        )::int
                    then 0::int
                    else
                        greatest(
                            max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                            max(g.max_gig_ratings_count) over w_s
                        )::int
                end as seller_ratings_count,
                case
                    when
                        (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int =
                        (min(coalesce(p.min_seller_completed_orders_count, 0)) over w_s)::int
                    then 0::int
                    else
                        (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int
                end as seller_completed_orders_count,
                g.gig_is_active,
                coalesce(p.prices, g.prices, array[0]::numeric[]) as prices
            from etl_wrk.reg_trend_prd_cur_gigs_revisions g
            left join etl_wrk.reg_trend_prd_cur_gigs_pages p
                on g.gig_id = p.gig_id
            left join etl_wrk.reg_trend_prd_cur_sellers_revisions s
                on g.fiverr_seller_id = s.fiverr_seller_id
            window
                w_s as (partition by g.fiverr_seller_id);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_trend_prd_cur_metrics
            on etl_wrk.reg_trend_prd_cur_metrics (gig_id) with (fillfactor = 100);
        create index if not exists idx_reg_trend_prd_cur_metrics_fiverr_seller_id
            on etl_wrk.reg_trend_prd_cur_metrics (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.reg_trend_prd_cur_metrics;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.reg_trend_prd_cur_sellers_revisions_%1$s cascade;
                alter table if exists etl_wrk.reg_trend_prd_cur_sellers_revisions rename to reg_trend_prd_cur_sellers_revisions_%1$s;
                alter index if exists etl_wrk.uidx_reg_trend_prd_cur_sellers_revisions rename to uidx_reg_trend_prd_cur_sellers_revisions_%1$s;
                drop table if exists etl_wrk.reg_trend_prd_cur_gigs_pages_%1$s cascade;
                alter table if exists etl_wrk.reg_trend_prd_cur_gigs_pages rename to reg_trend_prd_cur_gigs_pages_%1$s;
                alter index if exists etl_wrk.uidx_reg_trend_prd_cur_gigs_pages rename to uidx_reg_trend_prd_cur_gigs_pages_%1$s;
                drop table if exists etl_wrk.reg_trend_prd_cur_gigs_revisions_%1$s cascade;
                alter table if exists etl_wrk.reg_trend_prd_cur_gigs_revisions rename to reg_trend_prd_cur_gigs_revisions_%1$s;
                alter index if exists etl_wrk.uidx_reg_trend_prd_cur_gigs_revisions rename to uidx_reg_trend_prd_cur_gigs_revisions_%1$s;
                alter index if exists etl_wrk.idx_reg_trend_prd_cur_gigs_revisions_fiverr_seller_id rename to idx_reg_trend_prd_cur_gigs_revisions_fiverr_seller_id_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.reg_trend_prd_cur_gigs_revisions cascade;
            drop table if exists etl_wrk.reg_trend_prd_cur_gigs_pages cascade;
            drop table if exists etl_wrk.reg_trend_prd_cur_sellers_revisions cascade;
        end if;

        select
            high_level_step || '.2.1.4' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_cur_metrics_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 2.2 - Data from previous assessed period (regular trends)

                -- Step 2.2.1 - Active sellers in current period (regular trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.reg_trend_prd_prev_sellers_revisions_filter cascade;

        create unlogged table if not exists etl_wrk.reg_trend_prd_prev_sellers_revisions_filter
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                fiverr_seller_id
            from etl_wrk.reg_trend_prd_cur_metrics;

        get diagnostics
            row_cnt = row_count;

        alter table if exists etl_wrk.reg_trend_prd_prev_sellers_revisions_filter
            add primary key (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.reg_trend_prd_prev_sellers_revisions_filter;

        select
            high_level_step || '.' || period_num || '.2.2.1' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_prev_sellers_revisions_filter_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.2.2 - Sellers revisions from previous assessed period (regular trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.reg_trend_prd_prev_sellers_revisions cascade;

        create unlogged table if not exists etl_wrk.reg_trend_prd_prev_sellers_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                s.fiverr_seller_id,
                max(s.seller_ratings_count) as max_seller_ratings_count
            from etl.sellers_revisions_pre_metrics s
            where
                s.scraped_at < start_dttm
                and exists
                    (
                        select
                        from etl_wrk.reg_trend_prd_prev_sellers_revisions_filter f
                        where
                            s.fiverr_seller_id = f.fiverr_seller_id
                    )
            group by
                s.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_trend_prd_prev_sellers_revisions
            on etl_wrk.reg_trend_prd_prev_sellers_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.reg_trend_prd_prev_sellers_revisions;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.reg_trend_prd_prev_sellers_revisions_filter_%1$s cascade;
                alter table if exists etl_wrk.reg_trend_prd_prev_sellers_revisions_filter rename to reg_trend_prd_prev_sellers_revisions_filter_%1$s;
                alter table if exists etl_wrk.reg_trend_prd_prev_sellers_revisions_filter_%1s
                    rename constraint reg_trend_prd_prev_sellers_revisions_filter_pkey to reg_trend_prd_prev_sellers_revisions_filter_%1$s_pkey;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.reg_trend_prd_prev_sellers_revisions_filter cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.2.2.2' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_prev_sellers_revisions_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.2.3 - Gigs pages from previous assessed period (regular trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.reg_trend_prd_prev_gigs_pages cascade;

        create unlogged table if not exists etl_wrk.reg_trend_prd_prev_gigs_pages
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                p.fiverr_seller_id,
                max(p.seller_completed_orders_count) as max_seller_completed_orders_count
            from etl.gigs_pages_pre_metrics p
            where
                p.scraped_at < start_dttm
                and exists
                    (
                        select
                        from etl_wrk.reg_trend_prd_cur_metrics cur
                        where
                            p.gig_id = cur.gig_id
                    )
            group by
                p.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_trend_prd_prev_gigs_pages
            on etl_wrk.reg_trend_prd_prev_gigs_pages (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.reg_trend_prd_prev_gigs_pages;

        select
            high_level_step || '.' || period_num || '.2.2.3' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_prev_gigs_pages_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.2.4 - Gigs revisions from previous assessed period (regular trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.reg_trend_prd_prev_gigs_revisions cascade;

        create unlogged table if not exists etl_wrk.reg_trend_prd_prev_gigs_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.gig_id,
                g.fiverr_seller_id,
                coalesce(max(g.gig_ratings_count), 0)::int as max_gig_ratings_count
            from etl.gigs_revisions_pre_metrics g
            where
                g.scraped_at < start_dttm
                and exists
                    (
                        select
                        from etl_wrk.reg_trend_prd_cur_metrics cur
                        where
                            g.gig_id = cur.gig_id
                    )
            group by
                g.gig_id,
                g.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_trend_prd_prev_gigs_revisions
            on etl_wrk.reg_trend_prd_prev_gigs_revisions (gig_id) with (fillfactor = 100);
        create index if not exists uidx_reg_trend_prd_prev_gigs_revisions_fiverr_seller_id
            on etl_wrk.reg_trend_prd_prev_gigs_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.reg_trend_prd_prev_gigs_revisions;

        select
            high_level_step || '.' || period_num || '.2.2.4' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_prev_gigs_revisions_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.2.5 - Metrics from previous assessed period (regular trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.reg_trend_prd_prev_metrics cascade;

        create unlogged table if not exists etl_wrk.reg_trend_prd_prev_metrics
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.gig_id,
                g.max_gig_ratings_count as gig_ratings_count,
                greatest(
                    max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                    max(g.max_gig_ratings_count) over w_s
                )::int as seller_ratings_count,
                (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int as seller_completed_orders_count
            from etl_wrk.reg_trend_prd_prev_gigs_revisions g
            left join etl_wrk.reg_trend_prd_prev_gigs_pages p
                on g.fiverr_seller_id = p.fiverr_seller_id
            left join etl_wrk.reg_trend_prd_prev_sellers_revisions s
                on s.fiverr_seller_id = g.fiverr_seller_id
            window
                w_s as (partition by g.fiverr_seller_id);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_trend_prd_prev_metrics
            on etl_wrk.reg_trend_prd_prev_metrics (gig_id) with (fillfactor = 100);

        analyze etl_wrk.reg_trend_prd_prev_metrics;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.reg_trend_prd_prev_sellers_revisions_%1$s cascade;
                alter table if exists etl_wrk.reg_trend_prd_prev_sellers_revisions rename to reg_trend_prd_prev_sellers_revisions_%1$s;
                alter index if exists etl_wrk.uidx_reg_trend_prd_prev_sellers_revisions rename to uidx_reg_trend_prd_prev_sellers_revisions_%1$s;
                drop table if exists etl_wrk.reg_trend_prd_prev_gigs_pages_%1$s cascade;
                alter table if exists etl_wrk.reg_trend_prd_prev_gigs_pages rename to reg_trend_prd_prev_gigs_pages_%1$s;
                alter index if exists etl_wrk.uidx_reg_trend_prd_prev_gigs_pages rename to uidx_reg_trend_prd_prev_gigs_pages_%1$s;
                drop table if exists etl_wrk.reg_trend_prd_prev_gigs_revisions_%1$s cascade;
                alter table if exists etl_wrk.reg_trend_prd_prev_gigs_revisions rename to reg_trend_prd_prev_gigs_revisions_%1$s;
                alter index if exists etl_wrk.uidx_reg_trend_prd_prev_gigs_revisions rename to uidx_reg_trend_prd_prev_gigs_revisions_%1$s;
                alter index if exists etl_wrk.idx_reg_trend_prd_prev_gigs_revisions_fiverr_seller_id rename to idx_reg_trend_prd_prev_gigs_revisions_fiverr_seller_id_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.reg_trend_prd_prev_gigs_revisions cascade;
            drop table if exists etl_wrk.reg_trend_prd_prev_gigs_pages cascade;
            drop table if exists etl_wrk.reg_trend_prd_prev_sellers_revisions cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.2.2.5' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_prev_metrics_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 2.3.1 - Gig reviews prices counts (regular trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.reg_trend_prd_gig_reviews_prices_counts cascade;

        create unlogged table if not exists etl_wrk.reg_trend_prd_gig_reviews_prices_counts
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                g.gig_id,
                g.price_range_start,
                g.price_range_end,
                count(*) over(partition by g.gig_id) as gig_reviews_cnt,
                count(*) over(partition by g.gig_id, g.price_range_start) as gig_reviews_cnt_by_start_price,
                count(*) over(partition by g.gig_id, g.price_range_end) as gig_reviews_cnt_by_end_price
            from etl.gig_reviews_prices g
            where
                g.fiverr_created_at >= start_dttm
                and g.fiverr_created_at < end_dttm
                and exists
                    (
                        select
                        from etl_wrk.trends_gigs_hist_active h
                        where
                            g.gig_id = h.gig_id
                    );

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_reg_trend_prd_gig_reviews_prices_counts
            on etl_wrk.reg_trend_prd_gig_reviews_prices_counts (gig_id) with (fillfactor = 100);

        analyze etl_wrk.reg_trend_prd_gig_reviews_prices_counts;

        select
            high_level_step || '.' || period_num || '.2.3.1' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_gig_reviews_prices_counts_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 2.3.2 - Gig reviews weighted prices (regular trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.reg_trend_prd_gig_reviews_weighted_prices cascade;

        create unlogged table if not exists etl_wrk.reg_trend_prd_gig_reviews_weighted_prices
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                gig_id,
                sum(price_range_start * gig_reviews_cnt_by_start_price)::numeric / gig_reviews_cnt::numeric as weighted_start_price_by_gig,
                sum(price_range_end * gig_reviews_cnt_by_end_price)::numeric / gig_reviews_cnt::numeric as weighted_end_price_by_gig
            from etl_wrk.reg_trend_prd_gig_reviews_prices_counts
            group by
                gig_id,
                gig_reviews_cnt;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_reg_trend_prd_gig_reviews_weighted_prices
            on etl_wrk.reg_trend_prd_gig_reviews_weighted_prices (gig_id) with (fillfactor = 100);

        analyze etl_wrk.reg_trend_prd_gig_reviews_weighted_prices;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.reg_trend_prd_gig_reviews_prices_counts_%1$s cascade;
                alter table if exists etl_wrk.reg_trend_prd_gig_reviews_prices_counts rename to reg_trend_prd_gig_reviews_prices_counts_%1$s;
                alter index if exists etl_wrk.idx_reg_trend_prd_gig_reviews_prices_counts rename to idx_reg_trend_prd_gig_reviews_prices_counts_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.reg_trend_prd_gig_reviews_prices_counts cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.2.3.2' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_gig_reviews_weighted_prices_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 2.4 - Joining data from previous and current periods (regular trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.reg_trend_prd_metrics cascade;

        create unlogged table if not exists etl_wrk.reg_trend_prd_metrics
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                cur.gig_id,
                cur.fiverr_seller_id,
                cur.category_id,
                cur.sub_category_id,
                cur.nested_sub_category_id,
                (
                    case
                        when period_num = 1 then
                            case
                                when
                                    nullif(cur.seller_ratings_count, 0::int) is null
                                    or nullif(prev.seller_ratings_count, 0::int) is null
                                    or (nullif(cur.seller_completed_orders_count, 0::int) is null and cur.gig_ratings_count > 0)
                                    or (nullif(prev.seller_completed_orders_count, 0::int) is null and prev.gig_ratings_count > 0)
                                then
                                    round(cur.gig_ratings_count::numeric / ratings_to_volume_coeff) -- old version
                                else
                                    round(coalesce(cur.gig_ratings_count::numeric * cur.seller_completed_orders_count::numeric / cur.seller_ratings_count::numeric, 0))
                            end
                        else
                            case
                                when
                                    nullif(cur.seller_ratings_count, 0::int) is null
                                    or nullif(prev.seller_ratings_count, 0::int) is null
                                    or (nullif(cur.seller_completed_orders_count, 0::int) is null and cur.gig_ratings_count > 0)
                                    or (nullif(prev.seller_completed_orders_count, 0::int) is null and prev.gig_ratings_count > 0)
                                then
                                    round((cur.gig_ratings_count + (cur.gig_ratings_count - coalesce(prev.gig_ratings_count, 0)))::numeric / ratings_to_volume_coeff) -- old version
                                else
                                    round(
                                        (cur.gig_ratings_count - coalesce(prev.gig_ratings_count, 0))::numeric *
                                        cur.seller_completed_orders_count::numeric / cur.seller_ratings_count::numeric
                                    )
                            end
                    end
                )::int as gig_volume,
                cur.gig_is_active,
                coalesce(rew.weighted_start_price_by_gig, 0::numeric) as weighted_start_price_by_gig,
                coalesce(rew.weighted_end_price_by_gig, 'infinity'::numeric) as weighted_end_price_by_gig,
                (select avg(price) from unnest(cur.prices) p (price)) as avg_price_by_gig
            from etl_wrk.reg_trend_prd_cur_metrics cur
            left join etl_wrk.reg_trend_prd_prev_metrics prev
                on cur.gig_id = prev.gig_id
            left join etl_wrk.reg_trend_prd_gig_reviews_weighted_prices rew
                on cur.gig_id = rew.gig_id;

        get diagnostics
            row_cnt = row_count;

        analyze etl_wrk.reg_trend_prd_metrics;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.reg_trend_prd_prev_metrics_%1$s cascade;
                alter table if exists etl_wrk.reg_trend_prd_prev_metrics rename to reg_trend_prd_prev_metrics_%1$s;
                alter index if exists etl_wrk.uidx_reg_trend_prd_prev_metrics rename to uidx_reg_trend_prd_prev_metrics_%1$s;
                drop table if exists etl_wrk.reg_trend_prd_gig_reviews_weighted_prices_%1$s cascade;
                alter table if exists etl_wrk.reg_trend_prd_gig_reviews_weighted_prices rename to reg_trend_prd_gig_reviews_weighted_prices_%1$s;
                alter index if exists etl_wrk.uidx_reg_trend_prd_gig_reviews_weighted_prices rename to uidx_reg_trend_prd_gig_reviews_weighted_prices_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.reg_trend_prd_prev_metrics cascade;
            drop table if exists etl_wrk.reg_trend_prd_gig_reviews_weighted_prices cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.2.4' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_metrics_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 3 - Materializing data for heuristic gigs

            -- Step 3.1 - Data from current assessed period (heuristic trends)

                -- Step 3.1.1 - Sellers revisions from current assessed period (heuristic trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.heur_trend_prd_cur_sellers_revisions cascade;

        create unlogged table if not exists etl_wrk.heur_trend_prd_cur_sellers_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                s.fiverr_seller_id,
                min(s.seller_ratings_count) as min_seller_ratings_count,
                max(s.seller_ratings_count) as max_seller_ratings_count
            from etl.sellers_revisions_pre_metrics s
            where
                s.scraped_at < start_dttm
                and exists
                    (
                        select
                        from etl_wrk.trends_sellers_hist_active h
                        where
                            s.seller_id = h.seller_id
                    )
            group by
                s.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_trend_prd_cur_sellers_revisions
            on etl_wrk.heur_trend_prd_cur_sellers_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.heur_trend_prd_cur_sellers_revisions;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.trends_sellers_hist_active_%1$s cascade;
                alter table if exists etl_wrk.trends_sellers_hist_active rename to trends_sellers_hist_active_%1$s;
                alter index if exists etl_wrk.uidx_trends_sellers_hist_active rename to uidx_trends_sellers_hist_active_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.trends_sellers_hist_active cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.3.1.1' as src_step,
            'etl_wrk' as tgt_schema,
            'heur_trend_prd_cur_sellers_revisions_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 3.1.2 - Gigs pages from current assessed period (heuristic trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.heur_trend_prd_cur_gigs_pages cascade;

        create unlogged table if not exists etl_wrk.heur_trend_prd_cur_gigs_pages
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                p.gig_id,
                p.fiverr_seller_id,
                min(p.seller_completed_orders_count) over (partition by p.fiverr_seller_id) as min_seller_completed_orders_count,
                max(p.seller_completed_orders_count) over (partition by p.fiverr_seller_id) as max_seller_completed_orders_count,
                first_value(p.prices) over (partition by p.gig_id order by p.scraped_at desc, p.id desc) as prices
            from etl.gigs_pages_pre_metrics p
            where
                p.scraped_at < start_dttm
                and exists
                    (
                        select
                        from etl_wrk.trends_gigs_hist_active h
                        where
                            p.gig_id = h.gig_id
                    )
                and not exists
                    (
                        select
                        from etl_wrk.reg_trend_prd_cur_metrics r
                        where
                            p.gig_id = r.gig_id
                    );

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_trend_prd_cur_gigs_pages
            on etl_wrk.heur_trend_prd_cur_gigs_pages (gig_id) with (fillfactor = 100);

        analyze etl_wrk.heur_trend_prd_cur_gigs_pages;

        select
            high_level_step || '.' || period_num || '.3.1.2' as src_step,
            'etl_wrk' as tgt_schema,
            'heur_trend_prd_cur_gigs_pages_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 3.1.3 - Gigs revisions from current assessed period (heuristic trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.heur_trend_prd_cur_gigs_revisions cascade;

        create unlogged table if not exists etl_wrk.heur_trend_prd_cur_gigs_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (gig_id)
                g.scraped_at,
                g.gig_id,
                g.fiverr_seller_id,
                g.category_id,
                g.sub_category_id,
                g.nested_sub_category_id,
                coalesce(min(g.gig_ratings_count) over (partition by g.gig_id), 0)::int as min_gig_ratings_count,
                coalesce(max(g.gig_ratings_count) over (partition by g.gig_id), 0)::int as max_gig_ratings_count,
                case when h.valid_to_dttm >= end_dttm then true else false end as gig_is_active,
                g.prices
            from etl.gigs_revisions_pre_metrics g
            join etl_wrk.trends_gigs_hist_active h
                on g.gig_id = h.gig_id
            where
                g.scraped_at < start_dttm
                and not exists
                    (
                        select
                        from etl_wrk.reg_trend_prd_cur_metrics r
                        where
                            g.gig_id = r.gig_id
                    )
            order by
                g.gig_id,
                g.scraped_at desc,
                g.id desc;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_trend_prd_cur_gigs_revisions
            on etl_wrk.heur_trend_prd_cur_gigs_revisions (gig_id) with (fillfactor = 100);
        create index if not exists idx_heur_trend_prd_cur_gigs_revisions_fiverr_seller_id
            on etl_wrk.heur_trend_prd_cur_gigs_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.heur_trend_prd_cur_gigs_revisions;

        select
            high_level_step || '.' || period_num || '.3.1.3' as src_step,
            'etl_wrk' as tgt_schema,
            'heur_trend_prd_cur_gigs_revisions_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 3.1.4 - Metrics from current assessed period (heuristic trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.heur_trend_prd_cur_metrics cascade;

        create unlogged table if not exists etl_wrk.heur_trend_prd_cur_metrics
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.scraped_at,
                g.gig_id,
                g.fiverr_seller_id,
                g.category_id,
                g.sub_category_id,
                g.nested_sub_category_id,
                case
                    when g.max_gig_ratings_count = g.min_gig_ratings_count
                        then 0::int
                    else g.max_gig_ratings_count
                end as gig_ratings_count,
                case
                    when
                        greatest(
                            max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                            max(g.max_gig_ratings_count) over w_s
                        )::int =
                        least(
                            min(coalesce(s.min_seller_ratings_count, 0)) over w_s,
                            min(g.min_gig_ratings_count) over w_s
                        )::int
                    then 0::int
                    else
                        greatest(
                            max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                            max(g.max_gig_ratings_count) over w_s
                        )::int
                end as seller_ratings_count,
                case
                    when
                        (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int =
                        (min(coalesce(p.min_seller_completed_orders_count, 0)) over w_s)::int
                    then 0::int
                    else
                        (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int
                end as seller_completed_orders_count,
                g.gig_is_active,
                coalesce(p.prices, g.prices, array[0]::numeric[]) as prices
            from etl_wrk.heur_trend_prd_cur_gigs_revisions g
            left join etl_wrk.heur_trend_prd_cur_gigs_pages p
                on g.gig_id = p.gig_id
            left join etl_wrk.heur_trend_prd_cur_sellers_revisions s
                on g.fiverr_seller_id = s.fiverr_seller_id
            window
                w_s as (partition by g.fiverr_seller_id);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_trend_prd_cur_metrics
            on etl_wrk.heur_trend_prd_cur_metrics (gig_id, scraped_at desc) with (fillfactor = 100);
        create index if not exists idx_heur_trend_prd_cur_metrics_fiverr_seller_id
            on etl_wrk.heur_trend_prd_cur_metrics (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.heur_trend_prd_cur_metrics;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.heur_trend_prd_cur_sellers_revisions_%1$s cascade;
                alter table if exists etl_wrk.heur_trend_prd_cur_sellers_revisions rename to heur_trend_prd_cur_sellers_revisions_%1$s;
                alter index if exists etl_wrk.uidx_heur_trend_prd_cur_sellers_revisions rename to uidx_heur_trend_prd_cur_sellers_revisions_%1$s;
                drop table if exists etl_wrk.heur_trend_prd_cur_gigs_pages_%1$s cascade;
                alter table if exists etl_wrk.heur_trend_prd_cur_gigs_pages rename to heur_trend_prd_cur_gigs_pages_%1$s;
                alter index if exists etl_wrk.uidx_heur_trend_prd_cur_gigs_pages rename to uidx_heur_trend_prd_cur_gigs_pages_%1$s;
                drop table if exists etl_wrk.heur_trend_prd_cur_gigs_revisions_%1$s cascade;
                alter table if exists etl_wrk.heur_trend_prd_cur_gigs_revisions rename to heur_trend_prd_cur_gigs_revisions_%1$s;
                alter index if exists etl_wrk.uidx_heur_trend_prd_cur_gigs_revisions rename to uidx_heur_trend_prd_cur_gigs_revisions_%1$s;
                alter index if exists etl_wrk.idx_heur_trend_prd_cur_gigs_revisions_fiverr_seller_id rename to idx_heur_trend_prd_cur_gigs_revisions_fiverr_seller_id_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.heur_trend_prd_cur_gigs_revisions cascade;
            drop table if exists etl_wrk.heur_trend_prd_cur_gigs_pages cascade;
            drop table if exists etl_wrk.heur_trend_prd_cur_sellers_revisions cascade;
        end if;

        select
            high_level_step || '.3.1.4' as src_step,
            'etl_wrk' as tgt_schema,
            'heur_trend_prd_cur_metrics_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 3.2 - Data from previous assessed period (heuristic trends)

                -- Step 3.2.1 - Active sellers in current period (heuristic trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.heur_trend_prd_prev_sellers_revisions_filter cascade;

        create unlogged table if not exists etl_wrk.heur_trend_prd_prev_sellers_revisions_filter
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                fiverr_seller_id
            from etl_wrk.heur_trend_prd_cur_metrics;

        get diagnostics
            row_cnt = row_count;

        alter table if exists etl_wrk.heur_trend_prd_prev_sellers_revisions_filter
            add primary key (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.heur_trend_prd_prev_sellers_revisions_filter;

        select
            high_level_step || '.' || period_num || '.3.2.1' as src_step,
            'etl_wrk' as tgt_schema,
            'heur_trend_prd_prev_sellers_revisions_filter_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 3.2.2 - Sellers revisions from previous assessed period (heuristic trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.heur_trend_prd_prev_sellers_revisions cascade;

        create unlogged table if not exists etl_wrk.heur_trend_prd_prev_sellers_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                s.fiverr_seller_id,
                max(s.seller_ratings_count) as max_seller_ratings_count
            from etl.sellers_revisions_pre_metrics s
            where
                s.scraped_at < start_dttm - period_width
                and exists
                    (
                        select
                        from etl_wrk.heur_trend_prd_prev_sellers_revisions_filter f
                        where
                            s.fiverr_seller_id = f.fiverr_seller_id
                    )
            group by
                s.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_trend_prd_prev_sellers_revisions
            on etl_wrk.heur_trend_prd_prev_sellers_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.heur_trend_prd_prev_sellers_revisions;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.heur_trend_prd_prev_sellers_revisions_filter_%1$s cascade;
                alter table if exists etl_wrk.heur_trend_prd_prev_sellers_revisions_filter rename to heur_trend_prd_prev_sellers_revisions_filter_%1$s;
                alter table if exists etl_wrk.heur_trend_prd_prev_sellers_revisions_filter_%1s
                    rename constraint heur_trend_prd_prev_sellers_revisions_filter_pkey to heur_trend_prd_prev_sellers_revisions_filter_%1$s_pkey;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.heur_trend_prd_prev_sellers_revisions_filter cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.3.2.2' as src_step,
            'etl_wrk' as tgt_schema,
            'heur_trend_prd_prev_sellers_revisions_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 2.2.3 - Gigs pages from previous assessed period (heuristic trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.heur_trend_prd_prev_gigs_pages cascade;

        create unlogged table if not exists etl_wrk.heur_trend_prd_prev_gigs_pages
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                p.fiverr_seller_id,
                max(p.seller_completed_orders_count) as max_seller_completed_orders_count
            from etl.gigs_pages_pre_metrics p
            where
                p.scraped_at < start_dttm - period_width
                and exists
                    (
                        select
                        from etl_wrk.heur_trend_prd_cur_metrics cur
                        where
                            p.gig_id = cur.gig_id
                            and p.scraped_at < cur.scraped_at - period_width
                    )
            group by
                p.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_trend_prd_prev_gigs_pages
            on etl_wrk.heur_trend_prd_prev_gigs_pages (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.heur_trend_prd_prev_gigs_pages;

        select
            high_level_step || '.' || period_num || '.3.2.3' as src_step,
            'etl_wrk' as tgt_schema,
            'heur_trend_prd_prev_gigs_pages_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 3.2.4 - Gigs revisions from previous assessed period (heuristic trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.heur_trend_prd_prev_gigs_revisions cascade;

        create unlogged table if not exists etl_wrk.heur_trend_prd_prev_gigs_revisions
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.gig_id,
                g.fiverr_seller_id,
                coalesce(max(g.gig_ratings_count), 0)::int as max_gig_ratings_count
            from etl.gigs_revisions_pre_metrics g
            where
                g.scraped_at < start_dttm - period_width
                and exists
                    (
                        select
                        from etl_wrk.heur_trend_prd_cur_metrics cur
                        where
                            g.gig_id = cur.gig_id
                            and g.scraped_at < cur.scraped_at - period_width
                    )
            group by
                g.gig_id,
                g.fiverr_seller_id;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_trend_prd_prev_gigs_revisions
            on etl_wrk.heur_trend_prd_prev_gigs_revisions (gig_id) with (fillfactor = 100);
        create index if not exists idx_heur_trend_prd_prev_gigs_revisions_fiverr_seller_id
            on etl_wrk.heur_trend_prd_prev_gigs_revisions (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.heur_trend_prd_prev_gigs_revisions;

        select
            high_level_step || '.' || period_num || '.3.2.4' as src_step,
            'etl_wrk' as tgt_schema,
            'heur_trend_prd_prev_gigs_revisions_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

                -- Step 3.2.5 - Metrics from previous assessed period (heuristic trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.heur_trend_prd_prev_metrics cascade;

        create unlogged table if not exists etl_wrk.heur_trend_prd_prev_metrics
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.gig_id,
                g.max_gig_ratings_count as gig_ratings_count,
                greatest(
                    max(coalesce(s.max_seller_ratings_count, 0)) over w_s,
                    max(g.max_gig_ratings_count) over w_s
                )::int as seller_ratings_count,
                (max(coalesce(p.max_seller_completed_orders_count, 0)) over w_s)::int as seller_completed_orders_count
            from etl_wrk.heur_trend_prd_prev_gigs_revisions g
            left join etl_wrk.heur_trend_prd_prev_gigs_pages p
                on g.fiverr_seller_id = p.fiverr_seller_id
            left join etl_wrk.heur_trend_prd_prev_sellers_revisions s
                on s.fiverr_seller_id = g.fiverr_seller_id
            window
                w_s as (partition by g.fiverr_seller_id);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_trend_prd_prev_metrics
            on etl_wrk.heur_trend_prd_prev_metrics (gig_id) with (fillfactor = 100);

        analyze etl_wrk.heur_trend_prd_prev_metrics;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.heur_trend_prd_prev_sellers_revisions_%1$s cascade;
                alter table if exists etl_wrk.heur_trend_prd_prev_sellers_revisions rename to heur_trend_prd_prev_sellers_revisions_%1$s;
                alter index if exists etl_wrk.uidx_heur_trend_prd_prev_sellers_revisions rename to uidx_heur_trend_prd_prev_sellers_revisions_%1$s;
                drop table if exists etl_wrk.heur_trend_prd_prev_gigs_pages_%1$s cascade;
                alter table if exists etl_wrk.heur_trend_prd_prev_gigs_pages rename to heur_trend_prd_prev_gigs_pages_%1$s;
                alter index if exists etl_wrk.uidx_heur_trend_prd_prev_gigs_pages rename to uidx_heur_trend_prd_prev_gigs_pages_%1$s;
                drop table if exists etl_wrk.heur_trend_prd_prev_gigs_revisions_%1$s cascade;
                alter table if exists etl_wrk.heur_trend_prd_prev_gigs_revisions rename to heur_trend_prd_prev_gigs_revisions_%1$s;
                alter index if exists etl_wrk.uidx_heur_trend_prd_prev_gigs_revisions rename to uidx_heur_trend_prd_prev_gigs_revisions_%1$s;
                alter index if exists etl_wrk.idx_heur_trend_prd_prev_gigs_revisions_fiverr_seller_id rename to idx_heur_trend_prd_prev_gigs_revisions_fiverr_seller_id_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.heur_trend_prd_prev_gigs_revisions cascade;
            drop table if exists etl_wrk.heur_trend_prd_prev_gigs_pages cascade;
            drop table if exists etl_wrk.heur_trend_prd_prev_sellers_revisions cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.3.2.5' as src_step,
            'etl_wrk' as tgt_schema,
            'heur_trend_prd_prev_metrics_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 3.3.1 - Gig reviews prices counts (heuristic trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.heur_trend_prd_gig_reviews_prices_counts cascade;

        create unlogged table if not exists etl_wrk.heur_trend_prd_gig_reviews_prices_counts
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                g.gig_id,
                g.price_range_start,
                g.price_range_end,
                count(*) over(partition by g.gig_id) as gig_reviews_cnt,
                count(*) over(partition by g.gig_id, g.price_range_start) as gig_reviews_cnt_by_start_price,
                count(*) over(partition by g.gig_id, g.price_range_end) as gig_reviews_cnt_by_end_price
            from etl.gig_reviews_prices g
            where
                g.fiverr_created_at < start_dttm
                and exists
                    (
                        select
                        from etl_wrk.trends_gigs_hist_active h
                        where
                            g.gig_id = h.gig_id
                    )
                and not exists
                    (
                        select
                        from etl_wrk.reg_trend_prd_cur_metrics r
                        where
                            g.gig_id = r.gig_id
                    );

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_heur_trend_prd_gig_reviews_prices_counts
            on etl_wrk.heur_trend_prd_gig_reviews_prices_counts (gig_id) with (fillfactor = 100);

        analyze etl_wrk.heur_trend_prd_gig_reviews_prices_counts;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.trends_gigs_hist_active_%1$s cascade;
                alter table if exists etl_wrk.trends_gigs_hist_active rename to trends_gigs_hist_active_%1$s;
                alter index if exists etl_wrk.uidx_trends_gigs_hist_active rename to uidx_trends_gigs_hist_active_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.trends_gigs_hist_active cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.3.3.1' as src_step,
            'etl_wrk' as tgt_schema,
            'heur_trend_prd_gig_reviews_prices_counts_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 3.3.2 - Gig reviews weighted prices (heuristic trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.heur_trend_prd_gig_reviews_weighted_prices cascade;

        create unlogged table if not exists etl_wrk.heur_trend_prd_gig_reviews_weighted_prices
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                gig_id,
                sum(price_range_start * gig_reviews_cnt_by_start_price)::numeric / gig_reviews_cnt::numeric as weighted_start_price_by_gig,
                sum(price_range_end * gig_reviews_cnt_by_end_price)::numeric / gig_reviews_cnt::numeric as weighted_end_price_by_gig
            from etl_wrk.heur_trend_prd_gig_reviews_prices_counts
            group by
                gig_id,
                gig_reviews_cnt;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_heur_trend_prd_gig_reviews_weighted_prices
            on etl_wrk.heur_trend_prd_gig_reviews_weighted_prices (gig_id) with (fillfactor = 100);

        analyze etl_wrk.heur_trend_prd_gig_reviews_weighted_prices;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.heur_trend_prd_gig_reviews_prices_counts_%1$s cascade;
                alter table if exists etl_wrk.heur_trend_prd_gig_reviews_prices_counts rename to heur_trend_prd_gig_reviews_prices_counts_%1$s;
                alter index if exists etl_wrk.idx_heur_trend_prd_gig_reviews_prices_counts rename to idx_heur_trend_prd_gig_reviews_prices_counts_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.heur_trend_prd_gig_reviews_prices_counts cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.3.3.2' as src_step,
            'etl_wrk' as tgt_schema,
            'heur_trend_prd_gig_reviews_weighted_prices_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 3.4 - Joining data from previous and current periods (heuristic trends)

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.heur_trend_prd_metrics cascade;

        create unlogged table if not exists etl_wrk.heur_trend_prd_metrics
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                cur.gig_id,
                cur.fiverr_seller_id,
                cur.category_id,
                cur.sub_category_id,
                cur.nested_sub_category_id,
                (
                    case
                        when period_num = 1 then
                            case
                                when
                                    nullif(cur.seller_ratings_count, 0::int) is null
                                    or nullif(prev.seller_ratings_count, 0::int) is null
                                    or (nullif(cur.seller_completed_orders_count, 0::int) is null and cur.gig_ratings_count > 0)
                                    or (nullif(prev.seller_completed_orders_count, 0::int) is null and prev.gig_ratings_count > 0)
                                then
                                    round(cur.gig_ratings_count::numeric / ratings_to_volume_coeff) -- old version
                                else
                                    round(coalesce(cur.gig_ratings_count::numeric * cur.seller_completed_orders_count::numeric / cur.seller_ratings_count::numeric, 0))
                            end
                        else
                            case
                                when
                                    nullif(cur.seller_ratings_count, 0::int) is null
                                    or nullif(prev.seller_ratings_count, 0::int) is null
                                    or (nullif(cur.seller_completed_orders_count, 0::int) is null and cur.gig_ratings_count > 0)
                                    or (nullif(prev.seller_completed_orders_count, 0::int) is null and prev.gig_ratings_count > 0)
                                then
                                    round((cur.gig_ratings_count + (cur.gig_ratings_count - coalesce(prev.gig_ratings_count, 0)))::numeric / ratings_to_volume_coeff) -- old version
                                else
                                    round(
                                        (cur.gig_ratings_count - coalesce(prev.gig_ratings_count, 0))::numeric *
                                        cur.seller_completed_orders_count::numeric / cur.seller_ratings_count::numeric
                                    )
                            end
                    end
                )::int as gig_volume,
                cur.gig_is_active,
                coalesce(rew.weighted_start_price_by_gig, 0::numeric) as weighted_start_price_by_gig,
                coalesce(rew.weighted_end_price_by_gig, 'infinity'::numeric) as weighted_end_price_by_gig,
                (select avg(price) from unnest(cur.prices) p (price)) as avg_price_by_gig
            from etl_wrk.heur_trend_prd_cur_metrics cur
            left join etl_wrk.heur_trend_prd_prev_metrics prev
                on cur.gig_id = prev.gig_id
            left join etl_wrk.heur_trend_prd_gig_reviews_weighted_prices rew
                on cur.gig_id = rew.gig_id;

        get diagnostics
            row_cnt = row_count;

        analyze etl_wrk.heur_trend_prd_metrics;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.heur_trend_prd_cur_metrics_%1$s cascade;
                alter table if exists etl_wrk.heur_trend_prd_cur_metrics rename to heur_trend_prd_cur_metrics_%1$s;
                alter index if exists etl_wrk.uidx_heur_trend_prd_cur_metrics rename to uidx_heur_trend_prd_cur_metrics_%1$s;
                drop table if exists etl_wrk.heur_trend_prd_prev_metrics_%1$s cascade;
                alter table if exists etl_wrk.heur_trend_prd_prev_metrics rename to heur_trend_prd_prev_metrics_%1$s;
                alter index if exists etl_wrk.uidx_heur_trend_prd_prev_metrics rename to uidx_heur_trend_prd_prev_metrics_%1$s;
                drop table if exists etl_wrk.heur_trend_prd_gig_reviews_weighted_prices_%1$s cascade;
                alter table if exists etl_wrk.heur_trend_prd_gig_reviews_weighted_prices rename to heur_trend_prd_gig_reviews_weighted_prices_%1$s;
                alter index if exists etl_wrk.uidx_heur_trend_prd_gig_reviews_weighted_prices rename to uidx_heur_trend_prd_gig_reviews_weighted_prices_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.heur_trend_prd_cur_metrics cascade;
            drop table if exists etl_wrk.heur_trend_prd_prev_metrics cascade;
            drop table if exists etl_wrk.heur_trend_prd_gig_reviews_weighted_prices cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.3.4' as src_step,
            'etl_wrk' as tgt_schema,
            'heur_trend_prd_metrics_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 4 - Union heuristic and non-heuristic gigs

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.trend_prd_metrics_union cascade;

        create unlogged table if not exists etl_wrk.trend_prd_metrics_union
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                gig_id,
                fiverr_seller_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                greatest(gig_volume, 0)::int as gig_volume,
                gig_is_active,
                (
                    case
                        when -- no data from gig_reviews_prices
                            weighted_start_price_by_gig = 0::numeric
                            and weighted_end_price_by_gig in (0::numeric, 'infinity'::numeric)
                        then
                            avg_price_by_gig
                        when -- invalid weighted_end_price_by_gig
                            weighted_start_price_by_gig > 0::numeric
                            and weighted_end_price_by_gig = 0::numeric
                        then
                            (
                                case
                                    when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig <= avg_price_by_gig
                                        weighted_start_price_by_gig <= avg_price_by_gig
                                    then
                                        avg_price_by_gig
                                    when -- invalid weighted_end_price_by_gig and weighted_start_price_by_gig > avg_price_by_gig
                                        weighted_start_price_by_gig > avg_price_by_gig
                                    then
                                        weighted_start_price_by_gig
                                end
                            )
                        when -- valid data from gig_reviews_prices
                            weighted_start_price_by_gig >= 0::numeric
                            and weighted_end_price_by_gig > 0::numeric
                        then
                            (
                                case
                                    when -- valid data from gig_reviews_prices but weighted_end_price_by_gig >= avg_price_by_gig
                                        weighted_end_price_by_gig >= avg_price_by_gig
                                    then
                                        avg_price_by_gig
                                    when -- valid data from gig_reviews_prices but weighted_end_price_by_gig < avg_price_by_gig
                                        weighted_end_price_by_gig < avg_price_by_gig
                                    then
                                        (weighted_start_price_by_gig + weighted_end_price_by_gig) / 2::numeric
                                end
                            )
                        else
                            avg_price_by_gig
                    end
                ) as avg_price_by_gig
            from
            (
                select *
                from etl_wrk.reg_trend_prd_metrics

                union all

                select *
                from etl_wrk.heur_trend_prd_metrics
            ) u;

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_trend_prd_metrics_union_nest_subcat
            on etl_wrk.trend_prd_metrics_union (nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk.trend_prd_metrics_union;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.reg_trend_prd_cur_metrics_%1$s cascade;
                alter table if exists etl_wrk.reg_trend_prd_cur_metrics rename to reg_trend_prd_cur_metrics_%1$s;
                alter index if exists etl_wrk.uidx_reg_trend_prd_cur_metrics rename to uidx_reg_trend_prd_cur_metrics_%1$s;
                drop table if exists etl_wrk.reg_trend_prd_metrics_%1$s cascade;
                alter table if exists etl_wrk.reg_trend_prd_metrics rename to reg_trend_prd_metrics_%1$s;
                alter index if exists etl_wrk.uidx_reg_trend_prd_metrics rename to uidx_reg_trend_prd_metrics_%1$s;
                drop table if exists etl_wrk.heur_trend_prd_metrics_%1$s cascade;
                alter table if exists etl_wrk.heur_trend_prd_metrics rename to heur_trend_prd_metrics_%1$s;
                alter index if exists etl_wrk.uidx_heur_trend_prd_metrics rename to uidx_heur_trend_prd_metrics_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.reg_trend_prd_cur_metrics cascade;
            drop table if exists etl_wrk.reg_trend_prd_metrics cascade;
            drop table if exists etl_wrk.heur_trend_prd_metrics cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.4' as src_step,
            'etl_wrk' as tgt_schema,
            'trend_prd_metrics_union_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 5 - Calculating weighted prices in subcategories

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.trend_prd_metrics_weighted_prices cascade;

        create unlogged table if not exists etl_wrk.trend_prd_metrics_weighted_prices
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                gig_id,
                fiverr_seller_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                gig_volume,
                gig_is_active,
                avg_price_by_gig,
                coalesce(
                        nullif
                        (
                            (sum(gig_volume * avg_price_by_gig) over w_subcat)::numeric /
                            nullif(sum(gig_volume) over w_subcat, 0)::numeric,
                            0::numeric
                        ),
                        avg(avg_price_by_gig) over w_subcat
                    )::numeric as weighted_avg_price_by_subcategory,
                case
                    when nested_sub_category_id is null then null
                    else coalesce(
                            nullif
                            (
                                (sum(gig_volume * avg_price_by_gig) over w_nest_subcat)::numeric /
                                nullif(sum(gig_volume) over w_nest_subcat, 0)::numeric,
                                0::numeric
                            ),
                            avg(avg_price_by_gig) over w_nest_subcat
                        )::numeric
                end as weighted_avg_price_by_nested_subcategory
            from etl_wrk.trend_prd_metrics_union
            window
                w_subcat as (partition by sub_category_id),
                w_nest_subcat as (partition by nested_sub_category_id);

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_trend_prd_metrics_weighted_prices_fiverr_seller_id
            on etl_wrk.trend_prd_metrics_weighted_prices (fiverr_seller_id) with (fillfactor = 100);

        analyze etl_wrk.trend_prd_metrics_weighted_prices;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.trend_prd_metrics_union_%1$s cascade;
                alter table if exists etl_wrk.trend_prd_metrics_union rename to trend_prd_metrics_union_%1$s;
                alter index if exists etl_wrk.idx_trend_prd_metrics_union_nest_subcat rename to idx_trend_prd_metrics_union_nest_subcat_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.trend_prd_metrics_union cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.5' as src_step,
            'etl_wrk' as tgt_schema,
            'trend_prd_metrics_weighted_prices_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 6 - Revenue

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.trend_period cascade;

        create unlogged table if not exists etl_wrk.trend_period
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                gig_id,
                fiverr_seller_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                period_num as period_num,
                coalesce(sum(gig_volume * avg_price_by_gig) filter (where gig_is_active is true) over w_s, 0)::numeric as avg_revenue_by_seller,
                gig_volume * avg_price_by_gig as avg_revenue_by_gig,
                coalesce(sum(gig_volume * avg_price_by_gig) filter (where gig_is_active is true) over w_cat, 0)::numeric as avg_revenue_by_category,
                coalesce(sum(gig_volume * avg_price_by_gig) filter (where gig_is_active is true) over w_subcat, 0)::numeric as avg_revenue_by_subcategory,
                case
                    when nested_sub_category_id is null then null
                    else
                        coalesce(sum(gig_volume * avg_price_by_gig) filter (where gig_is_active is true) over w_nest_subcat, 0)::numeric
                end as avg_revenue_by_nested_subcategory
            from etl_wrk.trend_prd_metrics_weighted_prices
            where
                avg_price_by_gig < overpriced_coeff * coalesce(weighted_avg_price_by_nested_subcategory, weighted_avg_price_by_subcategory)
            window
                w_s as (partition by fiverr_seller_id),
                w_cat as (partition by category_id),
                w_subcat as (partition by sub_category_id),
                w_nest_subcat as (partition by nested_sub_category_id);

        get diagnostics
            row_cnt = row_count;

        analyze etl_wrk.trend_period;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.trend_prd_metrics_weighted_prices_%1$s cascade;
                alter table if exists etl_wrk.trend_prd_metrics_weighted_prices rename to trend_prd_metrics_weighted_prices_%1$s;
                alter index if exists etl_wrk.uidx_trend_prd_metrics_weighted_prices rename to uidx_trend_prd_metrics_weighted_prices_union_%1$s;
                alter index if exists etl_wrk.idx_trend_prd_metrics_weighted_prices_cats rename to idx_trend_prd_metrics_weighted_prices_cats_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.trend_prd_metrics_weighted_prices cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.6' as src_step,
            'etl_wrk' as tgt_schema,
            'trend_period_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 7 - Inserting into pre-result table

        task_start_dttm := clock_timestamp();

        insert into etl_wrk.trends_prefin
        select *
        from etl_wrk.trend_period;

        get diagnostics
            row_cnt = row_count;

        if debug_mode is true then
            execute format ($exec$
                drop table if exists etl_wrk.trend_period_%1$s cascade;
                alter table if exists etl_wrk.trend_period rename to trend_period_%1$s;
            $exec$, period_num);
        else
            drop table if exists etl_wrk.trend_period cascade;
        end if;

        select
            high_level_step || '.' || period_num || '.7' as src_step,
            'etl_wrk' as tgt_schema,
            'trends_prefin' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name ||
            ' from ' || log_rec.tgt_schema || '.trend_period_' || period_num || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Exiting

        select
            high_level_step || '.' || period_num || '.8' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - run_start_dttm;

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            start_dttm,
            end_dttm,
            period_num,
            ratings_to_volume_coeff,
            overpriced_coeff,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

    end;
$func$;