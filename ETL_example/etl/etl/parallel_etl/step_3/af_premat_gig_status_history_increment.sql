create or replace function etl.af_premat_gig_status_history_increment
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '3.4.1'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
#variable_conflict use_variable

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

        empty_incr_flg boolean;
        incr_start_dttm timestamptz;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Tracking fiverr_status changes and inserting new history

        task_start_dttm := clock_timestamp();

        select
            coalesce(max(valid_from_dttm), '2000-01-01 00:00:00 UTC'::timestamptz)
        into incr_start_dttm
        from etl.gig_status_history;

        select
            false
        into empty_incr_flg
        from public.gigs_revisions_meta
        where
            scraped_at > incr_start_dttm
        limit 1;

                -- merging data from new revisions

        if empty_incr_flg is false then

            alter table if exists etl.gig_status_history
                set
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                );

            drop index if exists etl.uidx_gig_status_history_valid_to cascade; -- this index blocks merge, has to be recreated

            with new_revs as
            (
                select
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    scraped_at as valid_from_dttm,
                    coalesce(
                                lead(scraped_at) over (partition by gig_id order by scraped_at) - '1 us'::interval,
                                '2999-12-31 00:00:00 UTC'::timestamptz
                            ) as valid_to_dttm,
                    gig_id,
                    fiverr_status
                from
                (
                    select
                        scraped_at,
                        gig_id,
                        case
                            when lag(fiverr_status, 1, 'empty') over (partition by gig_id order by scraped_at) != coalesce(fiverr_status, 'empty')
                                then true
                            else false
                        end as is_group_start,
                        fiverr_status
                    from
                    (
                        select
                            scraped_at,
                            gig_id,
                            fiverr_status
                        from public.gigs_revisions_meta
                        where
                            scraped_at > incr_start_dttm

                        union all

                        select
                            valid_from_dttm as scraped_at,
                            gig_id,
                            fiverr_status
                        from etl.gig_status_history
                        where
                            valid_to_dttm = '2999-12-31 00:00:00 UTC'::timestamptz -- current slice
                    ) u
                ) a
                where
                    is_group_start is true -- history rollup
            )

            merge into etl.gig_status_history as g
            using new_revs n
                on n.gig_id = g.gig_id
                and n.valid_from_dttm = g.valid_from_dttm
            when matched then
                do nothing -- to avoid inserting current slice again
            when not matched then
                insert
                    (
                        "load_id",
                        processed_dttm,
                        valid_from_dttm,
                        valid_to_dttm,
                        gig_id,
                        fiverr_status
                    )
                values
                    (
                        n."load_id",
                        n.processed_dttm,
                        n.valid_from_dttm,
                        n.valid_to_dttm,
                        n.gig_id,
                        n.fiverr_status
                    );

            get diagnostics
                row_cnt := row_count;

        else

            row_cnt := 0;

        end if;

        select
            high_level_step || '.1' as src_step,
            'etl' as tgt_schema,
            'gig_status_history' as tgt_name,
            'merge' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;