create or replace function etl.af_premat_sellers_revisions_parsing
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '3.3.3'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
#variable_conflict use_variable

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

        empty_incr_flg boolean;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Parsing JSONs

        task_start_dttm := clock_timestamp();

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.tgt_name = 'sellers_revisions_id_increment';

        if empty_incr_flg is false then

            drop table if exists etl_wrk.sellers_revisions_parsed cascade;

            create unlogged table if not exists etl_wrk.sellers_revisions_parsed
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    a.created_at,
                    case
                        when a.schema_version = 1 then to_timestamp((a.raw ->> 'created_at')::bigint)::timestamptz
                        when a.schema_version in (3, 4) then to_timestamp((a.raw ->> 'joined_at')::bigint)::timestamptz
                    end as fiverr_created_at,
                    a.scraped_at,
                    a.id,
                    a.seller_id,
                    nullif(case
                        when a.schema_version = 1 then (a.raw ->> 'id')::int
                        when a.schema_version = 2 then (a.raw ->> 'seller_id')::int
                        when a.schema_version in (3, 4) then (a.raw -> 'user' ->> 'id')::int
                    end, 0::int) as fiverr_seller_id,
                    case
                        when a.schema_version = 1 then (a.raw ->> 'ratings_count')::int
                        when a.schema_version in (3, 4) then (a.raw -> 'rating' ->> 'count')::int
                    end as seller_ratings_count,
                    case
                        when a.schema_version in (1, 2) then (a.raw ->> 'is_pro')::boolean
                        when a.schema_version in (3, 4) then (a.raw ->> 'isPro')::boolean
                    end as seller_is_pro,
                    case
                        when a.schema_version = 1 then (a.raw ->> 'rating')::numeric
                        when a.schema_version in (3, 4) then (a.raw -> 'rating' ->> 'score')::numeric
                    end as seller_rating,
                    coalesce
                        (
                            case
                                when a.schema_version = 1 then
                                    (
                                        case
                                            when (a.raw ->> 'level')::int = 0 then 'no_level'
                                            when (a.raw ->> 'level')::int = 1 then 'level_one'
                                            when (a.raw ->> 'level')::int = 2 then 'level_two'
                                            when (a.raw ->> 'level')::int = 5 then 'level_trs'
                                            else null
                                        end
                                    )
                                when a.schema_version = 2 then
                                    (
                                        case
                                            when lower(a.raw ->> 'seller_level') = 'no_level' then 'no_level'
                                            when lower(a.raw ->> 'seller_level') = 'level_one_seller' then 'level_one'
                                            when lower(a.raw ->> 'seller_level') = 'level_two_seller' then 'level_two'
                                            when lower(a.raw ->> 'seller_level') = 'top_rated_seller' then 'level_trs'
                                            else null
                                        end
                                    )
                                when a.schema_version in (3, 4)
                                    then lower(a.raw ->> 'sellerLevel')
                                else null
                            end,
                            'new_seller'
                        )::text as seller_level,
                    case
                        when a.schema_version = 1 then a.raw ->> 'username'
                        when a.schema_version = 2 then a.raw ->> 'seller_name'
                        when a.schema_version in (3, 4) then a.raw -> 'user' ->> 'name'
                    end as seller_name,
                    case
                        when a.schema_version in (3, 4) then a.raw -> 'agency' ->> 'slug'
                        else null::text
                    end as agency_slug,
                    case
                        when a.schema_version in (3, 4) then lower(a.raw -> 'agency' ->> 'status')
                        else null::text
                    end as agency_status,
                    case
                        when a.schema_version = 1 then a.raw ->> 'profile_image'
                        when a.schema_version = 2 then a.raw ->> 'seller_img'
                        when a.schema_version in (3, 4) then a.raw -> 'user' ->> 'profileImageUrl'
                    end as seller_profile_image,
                    case
                        when a.schema_version = 1 then a.raw -> 'address' ->> 'country'
                        when a.schema_version in (3, 4) then a.raw['user']['address'] ->> 'countryName'
                    end as seller_country,
                    case
                        when a.schema_version = 1 then a.raw -> 'address' ->> 'country_code'
                        when a.schema_version = 2 then a.raw ->> 'seller_country'
                        when a.schema_version in (3, 4) then a.raw['user']['address'] ->> 'countryCode'
                    end as seller_country_code,
                    case
                        when a.schema_version = 1 then array(select lower(jsonb_array_elements_text(jsonb_path_query_array(a.raw -> 'languages', '$.code'))))
                        when a.schema_version = 2 then array(select lower(jsonb_array_elements_text(jsonb_path_query_array(a.raw -> 'seller_languages', '$.code'))))
                        when a.schema_version in (3, 4) then array(select lower(jsonb_array_elements_text(jsonb_path_query_array(a.raw['user']['languages'], '$.code'))))
                    end as seller_languages
                from etl_wrk.sellers_revisions_raw_increment a;

            get diagnostics
                row_cnt := row_count;

            analyze etl_wrk.sellers_revisions_parsed;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.sellers_revisions_raw_increment cascade;
        end if;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk' as tgt_schema,
            'sellers_revisions_parsed' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;