create or replace function etl.af_premat_gig_status_history_rollup
(
    cluster_threshold numeric default 0.2, -- threshold value of the ratio between modified / inserted rows and n_live_tup or the ratio of uncorrelated keys of the index that used for clusterization for *pre_materialization* targets'. CLUSTER + ANALYZE commands are being launched if threshold is reached
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '3.4.2'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
#variable_conflict use_variable

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

        empty_incr_flg boolean;
        do_cluster_flg boolean;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            cluster_threshold,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Updating valid_to_dttm

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.1' as src_step,
            'etl' as tgt_schema,
            'gig_status_history' as tgt_name,
            'update' as op_type
        into log_rec;

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.op_type = 'merge'
            and l.tgt_name = 'gig_status_history';

        if empty_incr_flg is false then

            create unique index if not exists uidx_gig_status_history_valid_from
                on etl.gig_status_history (gig_id, valid_from_dttm); -- this index has to be set up for replication

            alter table if exists etl.gig_status_history
                replica identity using index uidx_gig_status_history_valid_from;

            with history_to_fix as
            (
                select
                    valid_from_dttm,
                    lead_valid_from,
                    gig_id
                from
                (
                    select
                        valid_to_dttm,
                        valid_from_dttm,
                        lead(valid_from_dttm) over (partition by gig_id order by valid_from_dttm) as lead_valid_from,
                        gig_id
                    from etl.gig_status_history
                ) a
                where
                    valid_to_dttm > lead_valid_from -- normal valid_to_dttm update after new history arrives
                    or lead_valid_from - valid_to_dttm > '1 us'::interval -- finding holes in history
            )

            update etl.gig_status_history as h
            set
                valid_to_dttm = f.lead_valid_from - '1 us'::interval,
                processed_dttm = transaction_timestamp(),
                "load_id" = load_id
            from history_to_fix f
            where
                h.gig_id = f.gig_id
                and h.valid_from_dttm = f.valid_from_dttm;

            get diagnostics
                row_cnt := row_count;

            create unique index if not exists uidx_gig_status_history_valid_to
                on etl.gig_status_history (gig_id, valid_to_dttm) with (fillfactor = 100);
            create index if not exists idx_gig_status_history_valid_from
                on etl.gig_status_history (valid_from_dttm desc);
            create index if not exists idx_gig_status_history_valid_to
                on etl.gig_status_history (valid_to_dttm desc);
            create index if not exists idx_gig_status_history_load_id
                on etl.gig_status_history ("load_id" desc);

                -- Clustering

            select
                coalesce
                (
                    (
                        coalesce(greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric, 0)::numeric >= cluster_threshold
                        or 1 - abs(st.correlation)::numeric >= cluster_threshold
                    ),
                    false
                )
            into do_cluster_flg
            from pg_catalog.pg_class c
            left join pg_catalog.pg_stat_user_tables s
                on c."oid" = s.relid
            left join pg_catalog.pg_stats st
                on c."oid" = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                and st.attname = 'gig_id' -- clustering by gig_id
            where
                c."oid" = (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass;

            if do_cluster_flg is true then

                cluster etl.gig_status_history using uidx_gig_status_history_valid_from;

                create statistics if not exists etl.gig_status_history_id_dttm
                on gig_id, valid_from_dttm, valid_to_dttm
                from etl.gig_status_history;

                analyze etl.gig_status_history;

            end if;

        else

            row_cnt := 0;

        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (update ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows updated', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            cluster_threshold,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;