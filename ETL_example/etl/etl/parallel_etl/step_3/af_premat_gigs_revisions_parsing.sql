create or replace function etl.af_premat_gigs_revisions_parsing
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '3.1.3'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
#variable_conflict use_variable

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

        empty_incr_flg boolean;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Parsing JSONs

        task_start_dttm := clock_timestamp();

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.tgt_name = 'gigs_revisions_id_increment';

        if empty_incr_flg is false then

            drop table if exists etl_wrk.gigs_revisions_parsed cascade;

            create unlogged table if not exists etl_wrk.gigs_revisions_parsed
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    id,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    gig_is_pro,
                    gig_rating,
                    gig_ratings_count,
                    prices,
                    gig_title,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name,
                    gig_cached_slug,
                    gig_preview_url
                from
                (
                    select
                        a.created_at,
                        case when a.schema_version = 1 then to_timestamp((a.raw ->> 'created_at')::bigint)::timestamptz end as fiverr_created_at,
                        a.scraped_at,
                        row_number() over (partition by a.id order by a.scraped_at desc, coalesce(t1.tp_rn, t2.tp_rn) asc) as image_url_rn,
                        a.id,
                        a.gig_id,
                        nullif(case
                            when a.schema_version = 1 then (a.raw ->> 'id')::int
                            when a.schema_version in (2, 3, 4) then (a.raw ->> 'gig_id')::int
                        end, 0::int) as fiverr_gig_id,
                        nullif((a.raw ->> 'seller_id')::int, 0::int) as fiverr_seller_id,
                        nullif((a.raw ->> 'category_id')::int, 0::int) as category_id,
                        nullif((a.raw ->> 'sub_category_id')::int, 0::int) as sub_category_id,
                        nullif((a.raw ->> 'nested_sub_category_id')::int, 0::int) as nested_sub_category_id,
                        case
                            when a.schema_version = 1 then (a.raw ->> 'ratings_count')::int
                            when a.schema_version in (2, 3, 4) then (a.raw ->> 'buying_review_rating_count')::int
                        end as gig_ratings_count,
                        (a.raw ->> 'is_pro')::boolean as gig_is_pro,
                        case
                            when a.schema_version = 1 then (a.raw ->> 'rating')::numeric
                            when a.schema_version in (2, 3, 4) then (a.raw ->> 'buying_review_rating')::numeric
                        end as gig_rating,
                        case
                            when a.schema_version = 1 then
                                array(select jsonb_array_elements_text(jsonb_path_query_array(a.raw -> 'packages', '$.price'))::numeric)
                            when a.schema_version in (2, 3, 4) then array[(a.raw ->> 'price_i')::numeric]
                        end as prices,
                        a.raw ->> 'title' as gig_title,
                        case when a.schema_version = 1 then a.raw ->> 'category_name' end as category_name,
                        case when a.schema_version = 1 then a.raw ->> 'sub_category_name' end as sub_category_name,
                        case when a.schema_version = 1 then a.raw ->> 'nested_sub_category_name' end as nested_sub_category_name,
                        a.raw ->> 'cached_slug' as gig_cached_slug,
                        coalesce(u1.url, u2.url) as gig_preview_url
                    from etl_wrk.gigs_revisions_raw_increment a
                        left join lateral unnest(array(select jsonb_array_elements(a.raw -> 'media') ->> 'type')) with ordinality t1 (tp, tp_rn)
                            on a.schema_version = 1 and t1.tp = 'image'
                        left join lateral unnest(array(select jsonb_array_elements(a.raw -> 'media') ->> 'preview_url')) with ordinality u1 (url, url_rn)
                            on a.schema_version = 1 and t1.tp_rn = u1.url_rn
                        left join lateral unnest(array(select jsonb_array_elements(a.raw -> 'assets') ->> 'type')) with ordinality t2 (tp, tp_rn)
                            on a.schema_version in (2, 3, 4) and t2.tp = 'ImageAsset'
                        left join lateral unnest(array(select jsonb_array_elements(a.raw -> 'assets') ->> 'cloud_img_main_gig')) with ordinality u2 (url, url_rn)
                            on a.schema_version in (2, 3, 4) and t2.tp_rn = u2.url_rn
                ) b
                where
                    image_url_rn = 1; -- performs better than distinct on for this query

            get diagnostics
                row_cnt := row_count;

            analyze etl_wrk.gigs_revisions_parsed;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.gigs_revisions_raw_increment cascade;
        end if;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk' as tgt_schema,
            'gigs_revisions_parsed' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;