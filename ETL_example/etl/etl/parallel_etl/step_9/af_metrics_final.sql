create or replace function etl.af_metrics_final
(
    competition_score_params jsonb default -- set of parameters for etl.competition_score function for calculating competition_score_xxx attributes
        $js$
            {
                "volume_exp_scale_coeff": 100,
                "sigmoid_scale_coeff": -5,
                "sigmoid_shift_coeff": 0.5,
                "competition_score_scale_coeff": 0.8,
                "competition_score_add_coeff": 0.1,
                "volume_scale_coeff": 0.5
            }
        $js$::jsonb,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '9.1.6'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            competition_score_params,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Revenue

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.metrics_prefin cascade;

        create unlogged table if not exists etl_wrk.metrics_prefin
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                g.seller_created_at,
                g.seller_fiverr_created_at,
                g.seller_scraped_at,
                g.gig_created_at,
                g.gig_fiverr_created_at,
                g.gig_scraped_at,
                g.seller_id,
                g.fiverr_seller_id,
                g.gig_id,
                g.fiverr_gig_id,
                g.category_id,
                g.sub_category_id,
                g.nested_sub_category_id,
                g.all_gigs_count_by_category,
                g.all_gigs_count_by_subcategory,
                g.all_gigs_count_by_nested_subcategory,
                g.regular_gigs_by_category,
                g.regular_gigs_by_subcategory,
                g.regular_gigs_by_nested_subcategory,
                g.pro_gigs_by_category,
                g.pro_gigs_by_subcategory,
                g.pro_gigs_by_nested_subcategory,
                g.seller_count_by_level_and_category,
                g.seller_count_by_level_and_subcategory,
                g.seller_count_by_level_and_nested_subcategory,
                g.gig_count_by_seller,
                g.seller_ratings_count,
                g.seller_completed_orders_count,
                g.active_gigs_ratings_count_by_seller,
                g.gig_ratings_count,
                g.volume_by_seller,
                g.volume_by_gig,
                g.volume_by_category,
                g.volume_by_subcategory,
                g.volume_by_nested_subcategory,
                g.total_historical_volume_by_seller,
                g.total_historical_volume_by_gig,
                g.total_historical_volume_by_category,
                g.total_historical_volume_by_subcategory,
                g.total_historical_volume_by_nested_subcategory,
                g.heuristic,
                g.seller_is_pro,
                g.seller_is_active,
                g.gig_is_pro,
                g.gig_is_active,
                coalesce
                    (
                        round(g.volume_by_seller)::numeric
                        / nullif
                            (
                                round(g.total_historical_volume_by_seller) - round(least(g.volume_by_seller, g.total_historical_volume_by_seller)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as sales_volume_growth_percent_by_seller,
                coalesce
                    (
                        round(g.volume_by_gig)::numeric
                        / nullif
                            (
                                round(g.total_historical_volume_by_gig) - round(least(g.volume_by_gig, g.total_historical_volume_by_gig)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as sales_volume_growth_percent_by_gig,
                coalesce
                    (
                        round(g.volume_by_category)::numeric
                        / nullif
                            (
                                round(g.total_historical_volume_by_category) - round(least(g.volume_by_category, g.total_historical_volume_by_category)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as sales_volume_growth_percent_by_category,
                coalesce
                    (
                        round(g.volume_by_subcategory)::numeric
                        / nullif
                            (
                                round(g.total_historical_volume_by_subcategory) - round(least(g.volume_by_subcategory, g.total_historical_volume_by_subcategory)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as sales_volume_growth_percent_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce
                        (
                            round(g.volume_by_nested_subcategory)::numeric
                            / nullif
                                (
                                    round(g.total_historical_volume_by_nested_subcategory) - round(least(g.volume_by_nested_subcategory, g.total_historical_volume_by_nested_subcategory)),
                                    0
                                )::numeric,
                            0::numeric
                        ) * 100
                end as sales_volume_growth_percent_by_nested_subcategory,
                case
                    when c.category_id is null then 0
                    else
                        etl.competition_score(g.all_gigs_count_by_category, g.volume_by_gig, c.percentile, competition_score_params)
                    end as competition_score_for_gig_by_category,
                case
                    when s.sub_category_id is null then 0
                    else
                        etl.competition_score(g.all_gigs_count_by_subcategory, g.volume_by_gig, s.percentile, competition_score_params)
                    end as competition_score_for_gig_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    when g.nested_sub_category_id is not null and n.nested_sub_category_id is null then 0
                    else
                        etl.competition_score(g.all_gigs_count_by_nested_subcategory, g.volume_by_gig, n.percentile, competition_score_params)
                end as competition_score_for_gig_by_nested_subcategory,
                coalesce
                    (
                        (1 - 1::numeric / (1 + g.all_gigs_count_by_category::numeric / nullif(g.volume_by_category, 0)::numeric)) * 99::numeric + 1,
                        0
                    )::numeric as competition_score_for_category,
                coalesce
                    (
                        (1 - 1::numeric / (1 + g.all_gigs_count_by_subcategory::numeric / nullif(g.volume_by_subcategory, 0)::numeric)) * 99::numeric + 1,
                        0
                    )::numeric as competition_score_for_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else
                        coalesce
                            (
                                (1 - 1::numeric / (1 + g.all_gigs_count_by_nested_subcategory::numeric / nullif(g.volume_by_nested_subcategory, 0)::numeric)) * 99::numeric + 1,
                                0
                            )::numeric
                end as competition_score_for_nested_subcategory,
                case
                    when g.volume_by_category != 0
                        then least(g.volume_by_gig::numeric / g.volume_by_category::numeric, 1)::numeric
                    else 0::numeric
                end as market_share_for_gig_by_category,
                case
                    when g.volume_by_subcategory != 0
                        then least(g.volume_by_gig::numeric / g.volume_by_subcategory::numeric, 1)::numeric
                    else 0::numeric
                end as market_share_for_gig_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else
                        case
                            when g.volume_by_nested_subcategory != 0
                                then least(g.volume_by_gig::numeric / g.volume_by_nested_subcategory::numeric, 1)::numeric
                            else 0::numeric
                        end
                end as market_share_for_gig_by_nested_subcategory,
                coalesce
                    (
                        g.volume_by_category::numeric / nullif(sum(g.volume_by_gig) over (), 0)::numeric,
                        0::numeric
                    ) as market_share_for_category,
                coalesce
                    (
                        g.volume_by_subcategory::numeric / nullif(max(g.volume_by_category) over w_subcat, 0)::numeric,
                        0::numeric
                    ) as market_share_for_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else
                        coalesce
                        (
                            g.volume_by_nested_subcategory::numeric / nullif(max(g.volume_by_subcategory) over w_nest_subcat, 0)::numeric,
                            0::numeric
                        )
                end as market_share_for_nested_subcategory,
                g.seller_rating,
                g.gig_rating,
                g.min_price_by_seller,
                g.min_price_by_gig,
                g.min_price_by_category,
                g.min_price_by_subcategory,
                g.min_price_by_nested_subcategory,
                g.avg_price_by_seller,
                g.avg_price_by_gig,
                g.avg_price_by_category,
                g.avg_price_by_subcategory,
                g.avg_price_by_nested_subcategory,
                g.max_price_by_seller,
                g.max_price_by_gig,
                g.max_price_by_category,
                g.max_price_by_subcategory,
                g.max_price_by_nested_subcategory,
                g.weighted_min_price_by_seller,
                g.weighted_min_price_by_category,
                g.weighted_min_price_by_subcategory,
                g.weighted_min_price_by_nested_subcategory,
                g.weighted_avg_price_by_seller,
                g.weighted_avg_price_by_category,
                g.weighted_avg_price_by_subcategory,
                g.weighted_avg_price_by_nested_subcategory,
                g.weighted_max_price_by_seller,
                g.weighted_max_price_by_category,
                g.weighted_max_price_by_subcategory,
                g.weighted_max_price_by_nested_subcategory,
                coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is true) over w_s, 0)::numeric as min_active_revenue_by_seller,
                coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is false) over w_s, 0)::numeric as min_inactive_revenue_by_seller,
                g.min_revenue_by_gig,
                coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is true) over w_cat, 0)::numeric as min_active_revenue_by_category,
                coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is false) over w_cat, 0)::numeric as min_inactive_revenue_by_category,
                coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is true) over w_subcat, 0)::numeric as min_active_revenue_by_subcategory,
                coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is false) over w_subcat, 0)::numeric as min_inactive_revenue_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is true) over w_nest_subcat, 0)::numeric
                end as min_active_revenue_by_nested_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.min_revenue_by_gig) filter (where g.gig_is_active is false) over w_nest_subcat, 0)::numeric
                end as min_inactive_revenue_by_nested_subcategory,
                coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is true) over w_s, 0)::numeric as avg_active_revenue_by_seller,
                coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is false) over w_s, 0)::numeric as avg_inactive_revenue_by_seller,
                g.avg_revenue_by_gig,
                coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is true) over w_cat, 0)::numeric as avg_active_revenue_by_category,
                coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is false) over w_cat, 0)::numeric as avg_inactive_revenue_by_category,
                coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is true) over w_subcat, 0)::numeric as avg_active_revenue_by_subcategory,
                coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is false) over w_subcat, 0)::numeric as avg_inactive_revenue_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is true) over w_nest_subcat, 0)::numeric
                end as avg_active_revenue_by_nested_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.avg_revenue_by_gig) filter (where g.gig_is_active is false) over w_nest_subcat, 0)::numeric
                end as avg_inactive_revenue_by_nested_subcategory,
                coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is true) over w_s, 0)::numeric as max_active_revenue_by_seller,
                coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is false) over w_s, 0)::numeric as max_inactive_revenue_by_seller,
                g.max_revenue_by_gig,
                coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is true) over w_cat, 0)::numeric as max_active_revenue_by_category,
                coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is false) over w_cat, 0)::numeric as max_inactive_revenue_by_category,
                coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is true) over w_subcat, 0)::numeric as max_active_revenue_by_subcategory,
                coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is false) over w_subcat, 0)::numeric as max_inactive_revenue_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is true) over w_nest_subcat, 0)::numeric
                end as max_active_revenue_by_nested_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.max_revenue_by_gig) filter (where g.gig_is_active is false) over w_nest_subcat, 0)::numeric
                end as max_inactive_revenue_by_nested_subcategory,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.min_price_by_gig) over w_s,
                        greatest(g.seller_completed_orders_count, g.total_historical_volume_by_seller) * g.weighted_min_price_by_seller
                    ) as min_total_historical_revenue_by_seller,
                g.total_historical_volume_by_gig * g.min_price_by_gig as min_total_historical_revenue_by_gig,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.min_price_by_gig) over w_cat,
                        g.total_historical_volume_by_category * g.weighted_min_price_by_category
                    ) as min_total_historical_revenue_by_category,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.min_price_by_gig) over w_subcat,
                        g.total_historical_volume_by_subcategory * g.weighted_min_price_by_subcategory
                    ) as min_total_historical_revenue_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else greatest(
                            sum(g.total_historical_volume_by_gig * g.min_price_by_gig) over w_nest_subcat,
                            g.total_historical_volume_by_nested_subcategory * g.weighted_min_price_by_nested_subcategory
                        )
                end as min_total_historical_revenue_by_nested_subcategory,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.avg_price_by_gig) over w_s,
                        greatest(g.seller_completed_orders_count, g.total_historical_volume_by_seller) * g.weighted_avg_price_by_seller
                    ) as avg_total_historical_revenue_by_seller,
                g.total_historical_volume_by_gig * g.avg_price_by_gig as avg_total_historical_revenue_by_gig,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.avg_price_by_gig) over w_cat,
                        g.total_historical_volume_by_category * g.weighted_avg_price_by_category
                    ) as avg_total_historical_revenue_by_category,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.avg_price_by_gig) over w_subcat,
                        g.total_historical_volume_by_subcategory * g.weighted_avg_price_by_subcategory
                    ) as avg_total_historical_revenue_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else greatest(
                            sum(g.total_historical_volume_by_gig * g.avg_price_by_gig) over w_nest_subcat,
                            g.total_historical_volume_by_nested_subcategory * g.weighted_avg_price_by_nested_subcategory
                        )
                end as avg_total_historical_revenue_by_nested_subcategory,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.max_price_by_gig) over w_s,
                        greatest(g.seller_completed_orders_count, g.total_historical_volume_by_seller) * g.weighted_max_price_by_seller
                    ) as max_total_historical_revenue_by_seller,
                g.total_historical_volume_by_gig * g.max_price_by_gig as max_total_historical_revenue_by_gig,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.max_price_by_gig) over w_cat,
                        g.total_historical_volume_by_category * g.weighted_max_price_by_category
                    ) as max_total_historical_revenue_by_category,
                greatest(
                        sum(g.total_historical_volume_by_gig * g.max_price_by_gig) over w_subcat,
                        g.total_historical_volume_by_subcategory * g.weighted_max_price_by_subcategory
                    ) as max_total_historical_revenue_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else greatest(
                            sum(g.total_historical_volume_by_gig * g.max_price_by_gig) over w_nest_subcat,
                            g.total_historical_volume_by_nested_subcategory * g.weighted_max_price_by_nested_subcategory
                        )
                end as max_total_historical_revenue_by_nested_subcategory,
                g.seller_level,
                g.seller_name,
                g.agency_slug,
                g.agency_status,
                g.gig_title,
                g.gig_cached_slug,
                g.category_name,
                g.sub_category_name,
                g.nested_sub_category_name,
                g.seller_country,
                g.seller_country_code,
                g.seller_languages,
                g.seller_profile_image,
                g.gig_preview_url
            from etl_wrk.metrics_join g
            left join etl_wrk.metrics_categories_percentile c
                on c.category_id = g.category_id
                and c.percentile != 0
            left join etl_wrk.metrics_subcategories_percentile s
                on s.sub_category_id = g.sub_category_id
                and s.percentile != 0
            left join etl_wrk.metrics_nested_subcategories_percentile n
                on n.nested_sub_category_id = g.nested_sub_category_id
                and n.percentile != 0 -- percentile could be = 0 for categories without any active gigs
            window
                w_s as (partition by g.seller_id),
                w_cat as (partition by g.category_id),
                w_subcat as (partition by g.sub_category_id),
                w_nest_subcat as (partition by g.nested_sub_category_id);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_metrics_prefin
            on etl_wrk.metrics_prefin (gig_id) with (fillfactor = 100);

        cluster etl_wrk.metrics_prefin using uidx_metrics_prefin;

        analyze etl_wrk.metrics_prefin;

        if debug_mode is false then
            drop table if exists etl_wrk.metrics_join cascade;
            drop table if exists etl_wrk.metrics_competition_score_prepare cascade;
            drop table if exists etl_wrk.metrics_categories_percentile cascade;
            drop table if exists etl_wrk.metrics_subcategories_percentile cascade;
            drop table if exists etl_wrk.metrics_nested_subcategories_percentile cascade;
        end if;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk' as tgt_schema,
            'metrics_prefin' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            competition_score_params,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;