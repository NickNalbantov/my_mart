create or replace function etl.af_metrics_categories_names
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '9.1.1'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Heuristics to fill categories names

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.metrics_categories_names cascade;

        create unlogged table if not exists etl_wrk.metrics_categories_names
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (category_id, sub_category_id, nested_sub_category_id)
                m.category_id,
                m.sub_category_id,
                coalesce(m.nested_sub_category_id, -1)::int as nested_sub_category_id,
                public.last(coalesce(c.category_name, cd.category_name, m.category_name))
                    over(partition by m.category_id order by m.scraped_at, m.gig_id) as category_name,
                public.last(coalesce(c.sub_category_name, sd.sub_category_name, m.sub_category_name))
                    over(partition by m.sub_category_id order by m.scraped_at, m.gig_id) as sub_category_name,
                public.last(coalesce(c.nested_sub_category_name, nd.nested_sub_category_name, m.nested_sub_category_name))
                    over(partition by m.nested_sub_category_id order by m.scraped_at, m.gig_id) as nested_sub_category_name
            from etl_wrk.metrics_union m
            left join etl.category_tree_latest_revision c on -- if cat_name / sub_cat_name in revisions is empty then seek in category_tree by ids
                m.category_id = c.category_id
                and m.sub_category_id = c.sub_category_id
                and coalesce(m.nested_sub_category_id, -1)::int = coalesce(c.nested_sub_category_id, -1)::int
            left join -- if first LJ haven't found cat_name then pick latest available cat_name
                (
                    select distinct
                        category_id,
                        category_name
                    from etl.category_tree_latest_revision
                ) cd on m.category_id = cd.category_id
            left join -- if first LJ haven't found sub_cat_name then pick latest available sub_cat_name
                (
                    select distinct
                        sub_category_id,
                        sub_category_name
                    from etl.category_tree_latest_revision
                ) sd on m.sub_category_id = sd.sub_category_id
            left join -- if first LJ haven't found sub_cat_name then pick latest available nest_sub_cat_name
                (
                    select
                        nested_sub_category_id,
                        nested_sub_category_name
                    from etl.category_tree_latest_revision
                    where
                        nested_sub_category_id is not null
                ) nd on m.nested_sub_category_id = nd.nested_sub_category_id
            order by
                category_id,
                sub_category_id,
                nested_sub_category_id,
                m.scraped_at desc,
                m.gig_id desc;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists idx_categories_names
            on etl_wrk.metrics_categories_names (category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk.metrics_categories_names;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk' as tgt_schema,
            'categories_names' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;