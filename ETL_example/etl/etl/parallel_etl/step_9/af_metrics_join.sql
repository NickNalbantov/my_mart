create or replace function etl.af_metrics_join
(
    overpriced_coeff numeric default 10, -- crutch to filter gigs with suspiciously high prices (higher than weighted average price in subcategory multiplied by this coefficient)
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '9.1.3'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            overpriced_coeff,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Joining gigs, sellers and categories data

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.metrics_join cascade;

        create unlogged table if not exists etl_wrk.metrics_join
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                s.created_at as seller_created_at,
                s.fiverr_created_at as seller_fiverr_created_at,
                s.scraped_at as seller_scraped_at,
                g.created_at as gig_created_at,
                g.fiverr_created_at as gig_fiverr_created_at,
                g.scraped_at as gig_scraped_at,
                s.seller_id,
                g.fiverr_seller_id,
                g.gig_id,
                g.fiverr_gig_id,
                g.category_id,
                g.sub_category_id,
                g.nested_sub_category_id,
                c.regular_gigs_by_category + c.pro_gigs_by_category as all_gigs_count_by_category,
                c.regular_gigs_by_subcategory + c.pro_gigs_by_subcategory as all_gigs_count_by_subcategory,
                c.regular_gigs_by_nested_subcategory + c.pro_gigs_by_nested_subcategory as all_gigs_count_by_nested_subcategory,
                c.regular_gigs_by_category,
                c.regular_gigs_by_subcategory,
                c.regular_gigs_by_nested_subcategory,
                c.pro_gigs_by_category,
                c.pro_gigs_by_subcategory,
                c.pro_gigs_by_nested_subcategory,
                (count(g.fiverr_seller_id) over (partition by g.category_id, s.seller_level))::int as seller_count_by_level_and_category,
                (count(g.fiverr_seller_id) over (partition by g.sub_category_id, s.seller_level))::int as seller_count_by_level_and_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else (count(g.fiverr_seller_id) over (partition by g.nested_sub_category_id, s.seller_level))::int
                end as seller_count_by_level_and_nested_subcategory,
                (count(g.gig_id) filter (where g.gig_is_active is true) over w_s)::int as gig_count_by_seller,
                s.seller_ratings_count,
                max(g.seller_completed_orders_count) over w_s as seller_completed_orders_count,
                coalesce(sum(g.gig_ratings_count) filter (where g.gig_is_active is true) over w_s, 0)::int as active_gigs_ratings_count_by_seller,
                g.gig_ratings_count,
                coalesce(sum(g.gig_volume) over w_s, 0)::int as volume_by_seller,
                g.gig_volume as volume_by_gig,
                c.volume_by_category,
                c.volume_by_subcategory,
                c.volume_by_nested_subcategory,
                sum(g.total_historical_volume) over (w_s) as total_historical_volume_by_seller,
                g.total_historical_volume as total_historical_volume_by_gig,
                c.total_historical_volume_by_category,
                c.total_historical_volume_by_subcategory,
                c.total_historical_volume_by_nested_subcategory,
                g.heuristic,
                s.seller_is_pro,
                s.seller_is_active,
                g.gig_is_pro,
                g.gig_is_active,
                coalesce(s.seller_rating, 0)::numeric as seller_rating,
                coalesce(g.gig_rating, 0)::numeric as gig_rating,
                coalesce(min(g.min_price_by_gig) over w_s, 0)::numeric as min_price_by_seller,
                g.min_price_by_gig,
                c.min_price_by_category,
                c.min_price_by_subcategory,
                c.min_price_by_nested_subcategory,
                coalesce(avg(g.avg_price_by_gig) over w_s, 0)::numeric as avg_price_by_seller,
                avg_price_by_gig,
                c.avg_price_by_category,
                c.avg_price_by_subcategory,
                c.avg_price_by_nested_subcategory,
                coalesce(max(g.max_price_by_gig) over w_s, 0)::numeric as max_price_by_seller,
                max_price_by_gig,
                c.max_price_by_category,
                c.max_price_by_subcategory,
                c.max_price_by_nested_subcategory,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.min_price_by_gig) over w_s)::numeric /
                            nullif(sum(g.gig_volume) over w_s, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.min_price_by_gig) over w_s
                    )::numeric as weighted_min_price_by_seller,
                c.weighted_min_price_by_category,
                c.weighted_min_price_by_subcategory,
                c.weighted_min_price_by_nested_subcategory,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.avg_price_by_gig) over w_s)::numeric /
                            nullif(sum(g.gig_volume) over w_s, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.avg_price_by_gig) over w_s
                    )::numeric as weighted_avg_price_by_seller,
                c.weighted_avg_price_by_category,
                c.weighted_avg_price_by_subcategory,
                c.weighted_avg_price_by_nested_subcategory,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.max_price_by_gig) over w_s)::numeric /
                            nullif(sum(g.gig_volume) over w_s, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.max_price_by_gig) over w_s
                    )::numeric as weighted_max_price_by_seller,
                c.weighted_max_price_by_category,
                c.weighted_max_price_by_subcategory,
                c.weighted_max_price_by_nested_subcategory,
                g.gig_volume * g.min_price_by_gig as min_revenue_by_gig,
                g.gig_volume * g.avg_price_by_gig as avg_revenue_by_gig,
                g.gig_volume * g.max_price_by_gig as max_revenue_by_gig,
                coalesce(
                        (c.regular_gigs_by_category + c.pro_gigs_by_category)::numeric /
                        nullif(g.gig_volume, 0)::numeric,
                    0)::numeric as inversed_sales_ratio_by_category,
                coalesce(
                        (c.regular_gigs_by_subcategory + c.pro_gigs_by_subcategory)::numeric /
                        nullif(g.gig_volume, 0)::numeric,
                    0)::numeric as inversed_sales_ratio_by_subcategory,
                coalesce(
                        (c.regular_gigs_by_nested_subcategory + c.pro_gigs_by_nested_subcategory)::numeric /
                        nullif(g.gig_volume, 0)::numeric,
                    0)::numeric as inversed_sales_ratio_by_nested_subcategory,
                s.seller_level,
                s.seller_name,
                s.agency_slug,
                s.agency_status,
                g.gig_title,
                g.gig_cached_slug,
                coalesce(c.category_name, 'Miscellaneous') as category_name,
                coalesce(c.sub_category_name, 'Miscellaneous') as sub_category_name,
                coalesce(c.nested_sub_category_name, 'Miscellaneous') as nested_sub_category_name,
                s.seller_country,
                s.seller_country_code,
                s.seller_languages,
                s.seller_profile_image,
                g.gig_preview_url
            from etl_wrk.metrics_union g
            join etl_wrk.metrics_sellers_revisions s
                on s.fiverr_seller_id = g.fiverr_seller_id
            join etl_wrk.metrics_categories c
                on g.category_id = c.category_id
                and g.sub_category_id = c.sub_category_id
                and coalesce(g.nested_sub_category_id, -1)::int = c.nested_sub_category_id
            where
                g.avg_price_by_gig < overpriced_coeff * coalesce(c.weighted_avg_price_by_nested_subcategory, c.weighted_avg_price_by_subcategory)
            window
                w_s as (partition by g.fiverr_seller_id);

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_metrics_join_fiverr_seller_id
            on etl_wrk.metrics_join (fiverr_seller_id) with (fillfactor = 100);
        create index if not exists idx_metrics_join_cat
            on etl_wrk.metrics_join (category_id) with (fillfactor = 100);
        create index if not exists idx_metrics_join_subcat
            on etl_wrk.metrics_join (sub_category_id) with (fillfactor = 100);
        create index if not exists idx_metrics_join_nest_subcat
            on etl_wrk.metrics_join (nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk.metrics_join;

        if debug_mode is false then
            drop table if exists etl_wrk.metrics_union cascade;
            drop table if exists etl_wrk.metrics_sellers_revisions cascade;
            drop table if exists etl_wrk.metrics_categories cascade;
        end if;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk' as tgt_schema,
            'metrics_join' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            overpriced_coeff,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;