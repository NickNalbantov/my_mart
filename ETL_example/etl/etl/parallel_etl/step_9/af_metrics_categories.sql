create or replace function etl.af_metrics_categories
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '9.1.2'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Categories' metrics

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.metrics_categories cascade;

        create unlogged table if not exists etl_wrk.metrics_categories
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct
                c.category_id,
                c.sub_category_id,
                c.nested_sub_category_id,
                (count(g.gig_id) filter (where g.gig_is_active is true and g.gig_is_pro is false) over w_cat)::int as regular_gigs_by_category,
                (count(g.gig_id) filter (where g.gig_is_active is true and g.gig_is_pro is false) over w_subcat)::int as regular_gigs_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else (count(g.gig_id) filter (where g.gig_is_active is true and g.gig_is_pro is false) over w_nest_subcat)::int
                end as regular_gigs_by_nested_subcategory,
                (count(g.gig_id) filter (where g.gig_is_active is true and g.gig_is_pro is true) over w_cat)::int as pro_gigs_by_category,
                (count(g.gig_id) filter (where g.gig_is_active is true and g.gig_is_pro is true) over w_subcat)::int as pro_gigs_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else (count(g.gig_id) filter (where g.gig_is_active is true and g.gig_is_pro is true) over w_nest_subcat)::int
                end as pro_gigs_by_nested_subcategory,
                coalesce(sum(g.gig_volume) over w_cat, 0)::int as volume_by_category,
                coalesce(sum(g.gig_volume) over w_subcat, 0)::int as volume_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(sum(g.gig_volume) over w_nest_subcat, 0)::int
                end as volume_by_nested_subcategory,
                sum(g.total_historical_volume) over (w_cat) as total_historical_volume_by_category,
                sum(g.total_historical_volume) over (w_subcat) as total_historical_volume_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else sum(g.total_historical_volume) over (w_nest_subcat)
                end as total_historical_volume_by_nested_subcategory,
                coalesce(min(g.min_price_by_gig) over w_cat, 0)::numeric as min_price_by_category,
                coalesce(min(g.min_price_by_gig) over w_subcat, 0)::numeric as min_price_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(min(g.min_price_by_gig) over w_nest_subcat, 0)::numeric
                end as min_price_by_nested_subcategory,
                coalesce(avg(g.avg_price_by_gig) over w_cat, 0)::numeric as avg_price_by_category,
                coalesce(avg(g.avg_price_by_gig) over w_subcat, 0)::numeric as avg_price_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(avg(g.avg_price_by_gig) over w_nest_subcat, 0)::numeric
                end as avg_price_by_nested_subcategory,
                coalesce(max(g.max_price_by_gig) over w_cat, 0)::numeric as max_price_by_category,
                coalesce(max(g.max_price_by_gig) over w_subcat, 0)::numeric as max_price_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(max(g.max_price_by_gig) over w_nest_subcat, 0)::numeric
                end as max_price_by_nested_subcategory,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.min_price_by_gig) over w_cat)::numeric /
                            nullif(sum(g.gig_volume) over w_cat, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.min_price_by_gig) over w_cat
                    )::numeric as weighted_min_price_by_category,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.min_price_by_gig) over w_subcat)::numeric /
                            nullif(sum(g.gig_volume) over w_subcat, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.min_price_by_gig) over w_subcat
                    )::numeric as weighted_min_price_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(
                            nullif
                            (
                                (sum(g.gig_volume * g.min_price_by_gig) over w_nest_subcat)::numeric /
                                nullif(sum(g.gig_volume) over w_nest_subcat, 0)::numeric,
                                0::numeric
                            ),
                            avg(g.min_price_by_gig) over w_nest_subcat
                        )::numeric
                end as weighted_min_price_by_nested_subcategory,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.avg_price_by_gig) over w_cat)::numeric /
                            nullif(sum(g.gig_volume) over w_cat, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.avg_price_by_gig) over w_cat
                    )::numeric as weighted_avg_price_by_category,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.avg_price_by_gig) over w_subcat)::numeric /
                            nullif(sum(g.gig_volume) over w_subcat, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.avg_price_by_gig) over w_subcat
                    )::numeric as weighted_avg_price_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(
                            nullif
                            (
                                (sum(g.gig_volume * g.avg_price_by_gig) over w_nest_subcat)::numeric /
                                nullif(sum(g.gig_volume) over w_nest_subcat, 0)::numeric,
                                0::numeric
                            ),
                            avg(g.avg_price_by_gig) over w_nest_subcat
                        )::numeric
                end as weighted_avg_price_by_nested_subcategory,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.max_price_by_gig) over w_cat)::numeric /
                            nullif(sum(g.gig_volume) over w_cat, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.max_price_by_gig) over w_cat
                    )::numeric as weighted_max_price_by_category,
                coalesce(
                        nullif
                        (
                            (sum(g.gig_volume * g.max_price_by_gig) over w_subcat)::numeric /
                            nullif(sum(g.gig_volume) over w_subcat, 0)::numeric,
                            0::numeric
                        ),
                        avg(g.max_price_by_gig) over w_subcat
                    )::numeric as weighted_max_price_by_subcategory,
                case
                    when g.nested_sub_category_id is null then null
                    else coalesce(
                            nullif
                            (
                                (sum(g.gig_volume * g.max_price_by_gig) over w_nest_subcat)::numeric /
                                nullif(sum(g.gig_volume) over w_nest_subcat, 0)::numeric,
                                0::numeric
                            ),
                            avg(g.max_price_by_gig) over w_nest_subcat
                        )::numeric
                end as weighted_max_price_by_nested_subcategory,
                first_value(c.category_name) over (
                        partition by g.category_id
                        order by g.scraped_at desc, g.gig_id desc
                    ) as category_name,
                first_value(c.sub_category_name) over (
                        partition by g.sub_category_id
                        order by g.scraped_at desc, g.gig_id desc
                    ) as sub_category_name,
                first_value(c.nested_sub_category_name) over (
                        partition by g.nested_sub_category_id
                        order by g.scraped_at desc, g.gig_id desc
                    ) as nested_sub_category_name
            from etl_wrk.metrics_union g
            join etl_wrk.metrics_categories_names c
                on g.category_id = c.category_id
                and g.sub_category_id = c.sub_category_id
                and coalesce(g.nested_sub_category_id, -1)::int = c.nested_sub_category_id
            window
                w_cat as (partition by g.category_id),
                w_subcat as (partition by g.sub_category_id),
                w_nest_subcat as (partition by g.nested_sub_category_id);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists idx_metrics_categories_ids
            on etl_wrk.metrics_categories (category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk.metrics_categories;

        if debug_mode is false then
            drop table if exists etl_wrk.metrics_categories_names cascade;
        end if;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk' as tgt_schema,
            'metrics_categories' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;