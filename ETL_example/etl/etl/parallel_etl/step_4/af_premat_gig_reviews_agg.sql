create or replace function etl.af_premat_gig_reviews_agg
(
    cluster_threshold numeric default 0.2, -- threshold value of the ratio between modified / inserted rows and n_live_tup or the ratio of uncorrelated keys of the index that used for clusterization for *pre_materialization* targets'. CLUSTER + ANALYZE commands are being launched if threshold is reached
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '4.8'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
#variable_conflict use_variable

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

        empty_incr_flg boolean;
        do_cluster_flg boolean;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            cluster_threshold,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


            -- Step 1 - gig_reviews_agg

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.1' as src_step,
            'etl' as tgt_schema,
            'gig_reviews_agg' as tgt_name,
            'merge' as op_type
        into log_rec;

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.tgt_name = 'gig_reviews_agg_increment';

        if empty_incr_flg is false then

            alter table if exists etl.gig_reviews_agg
                set
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                );

            alter table if exists etl.gig_reviews_agg
                replica identity using index gig_reviews_agg_pkey;

            with incr as
            (
                select distinct on (gig_id)
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    m.fiverr_created_at as last_fiverr_created_at,
                    m.gig_id,
                    m.id as last_review_id,
                    (count(*) over (partition by m.gig_id))::int as reviews_count,
                    m.fiverr_review_id as last_fiverr_review_id
                from public.gig_reviews_meta m
                where exists
                    (
                        select
                        from etl_wrk.gig_reviews_agg_increment i
                        where
                            m.gig_id = i.gig_id
                    )
                order by
                    m.gig_id,
                    m.fiverr_created_at desc
            )

            merge into etl.gig_reviews_agg a
            using incr i on i.gig_id = a.gig_id
            when not matched then
                insert
                    (
                        "load_id",
                        processed_dttm,
                        last_fiverr_created_at,
                        gig_id,
                        last_review_id,
                        reviews_count,
                        last_fiverr_review_id
                    )
                values
                    (
                        i."load_id",
                        i.processed_dttm,
                        i.last_fiverr_created_at,
                        i.gig_id,
                        i.last_review_id,
                        i.reviews_count,
                        i.last_fiverr_review_id
                    )
            when matched then
                update set
                (
                    "load_id",
                    processed_dttm,
                    last_fiverr_created_at,
                    gig_id,
                    last_review_id,
                    reviews_count,
                    last_fiverr_review_id
                ) =
                (
                    i."load_id",
                    i.processed_dttm,
                    i.last_fiverr_created_at,
                    i.gig_id,
                    i.last_review_id,
                    i.reviews_count,
                    i.last_fiverr_review_id
                );

            get diagnostics
                row_cnt = row_count;

            create unique index if not exists uidx_gig_reviews_agg_gig_id_last_fiverr_created_at
                on etl.gig_reviews_agg (gig_id, last_fiverr_created_at desc);
            create index if not exists idx_gig_reviews_agg_last_fiverr_created_at
                on etl.gig_reviews_agg (last_fiverr_created_at desc);
            create index if not exists idx_gig_reviews_agg_load_id
                on etl.gig_reviews_agg ("load_id" desc);

                -- Clustering

            select
                coalesce
                (
                    (
                        coalesce(greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric, 0)::numeric >= cluster_threshold
                        or 1 - abs(st.correlation)::numeric >= cluster_threshold
                    ),
                    false
                )
            into do_cluster_flg
            from pg_catalog.pg_class c
            left join pg_catalog.pg_stat_user_tables s
                on c."oid" = s.relid
            left join pg_catalog.pg_stats st
                on c."oid" = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                and st.attname = 'gig_id' -- clustering by gig_id
            where
                c."oid" = (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass;

            if do_cluster_flg is true then

                cluster etl.gig_reviews_agg using gig_reviews_agg_pkey;

                create statistics if not exists etl.gig_reviews_agg_all_ids
                on gig_id, last_review_id, last_fiverr_review_id, last_fiverr_created_at
                from etl.gig_reviews_agg;

                analyze etl.gig_reviews_agg;

            end if;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.gig_reviews_agg_increment cascade;
        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (merge into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows merged', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            cluster_threshold,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;