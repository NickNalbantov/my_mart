create or replace function etl.af_premat_seller_status_history_merge
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '4.7.2'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
#variable_conflict use_variable

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

        empty_incr_flg boolean;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Seller status history increment merge

        task_start_dttm := clock_timestamp();

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.tgt_name = 'sellers_history_seller_increment';

        if empty_incr_flg is false then

            alter table if exists etl.seller_status_history
                set
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                );

            drop index if exists etl.uidx_seller_status_history_valid_to cascade; -- this index blocks merge, has to be recreated

            with new_revs as
            (
                select
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    valid_from_dttm,
                    coalesce(
                                lead(valid_from_dttm) over (partition by seller_id order by valid_from_dttm) - '1 us'::interval,
                                '2999-12-31 00:00:00 UTC'::timestamptz
                            ) as valid_to_dttm,
                    seller_id,
                    status
                from
                (
                    select
                        valid_from_dttm,
                        seller_id,
                        status,
                        case
                            when lag(status, 1, 'empty') over (partition by seller_id order by valid_from_dttm) != coalesce(status, 'empty')
                                then true
                            else false
                        end as is_group_start
                    from
                    (
                        select
                            valid_from_dttm,
                            seller_id,
                            case
                                when all_gigs_arr = not_avail_arr and all_gigs_arr != del_arr
                                    then 'not available'
                            else 'available' end as status
                        from
                        (
                            select
                                valid_from_dttm,
                                seller_id,
                                fiverr_status,
                                public.array_distinct_sort(
                                        array_agg(gig_id) over (partition by seller_id order by valid_from_dttm asc),
                                        do_grouping := true
                                    ) as all_gigs_arr,
                                case
                                    when fiverr_status in ('blocked', 'denied', 'suspended', 'deleted')
                                        then public.array_distinct_sort(
                                                array_agg(gig_id)
                                                    filter (where fiverr_status in ('blocked', 'denied', 'suspended', 'deleted'))
                                                    over (partition by seller_id, gr_not_avail order by valid_from_dttm),
                                                do_grouping := true
                                            )
                                    else array[]::int[]
                                end as not_avail_arr,
                                case
                                    when fiverr_status = 'deleted'
                                        then public.array_distinct_sort(
                                                array_agg(gig_id)
                                                    filter (where fiverr_status = 'deleted')
                                                    over (partition by seller_id, gr_del order by valid_from_dttm),
                                                do_grouping := true
                                            )
                                    else array[]::int[]
                                end as del_arr
                            from
                            (
                                select
                                    valid_from_dttm,
                                    seller_id,
                                    gig_id,
                                    fiverr_status,
                                    sum(not_avail_gr_start) over w2 as gr_not_avail,
                                    sum(del_gr_start) over w2 as gr_del
                                from
                                (
                                    select
                                        valid_from_dttm,
                                        seller_id,
                                        gig_id,
                                        fiverr_status,
                                        case
                                            when
                                                (
                                                    (
                                                        fiverr_status in ('blocked', 'denied', 'suspended', 'deleted')
                                                        and lag(fiverr_status, 1, 'empty') over w1 not in ('blocked', 'denied', 'suspended', 'deleted')
                                                    )
                                                    or
                                                    (
                                                        fiverr_status not in ('blocked', 'denied', 'suspended', 'deleted')
                                                        and lag(fiverr_status, 1, 'empty') over w1 in ('blocked', 'denied', 'suspended', 'deleted', 'empty')
                                                    )
                                                )
                                                and lag(valid_from_dttm, 1, '2000-01-01 00:00:00 UTC'::timestamptz) over w1 != valid_from_dttm
                                                    then 1
                                            else 0
                                        end as not_avail_gr_start,
                                        case
                                            when
                                                (
                                                    (fiverr_status = 'deleted' and lag(fiverr_status, 1, 'empty') over w1 != 'deleted')
                                                    or (fiverr_status != 'deleted' and lag(fiverr_status, 1, 'empty') over w1 in ('deleted', 'empty'))
                                                )
                                                and lag(valid_from_dttm, 1, '2000-01-01 00:00:00 UTC'::timestamptz) over w1 != valid_from_dttm
                                                    then 1
                                            else 0
                                        end as del_gr_start
                                    from
                                    (
                                        select
                                            h.valid_from_dttm,
                                            g.seller_id,
                                            g.id as gig_id,
                                            h.fiverr_status
                                        from etl.gig_status_history h
                                        join public.gigs g
                                            on g.id = h.gig_id
                                        where exists
                                            (
                                                select
                                                from etl_wrk.sellers_history_seller_increment s
                                                where
                                                    s.seller_id = g.seller_id
                                            )
                                    ) gh
                                    window w1 as (partition by seller_id order by valid_from_dttm)
                                ) gr_st
                                window w2 as (partition by seller_id order by valid_from_dttm)
                            ) gr
                        ) ar

                        union all

                        select
                            valid_from_dttm,
                            seller_id,
                            status
                        from etl.seller_status_history
                        where
                            valid_to_dttm = '2999-12-31 00:00:00 UTC'::timestamptz -- current slice
                    ) st
                ) hr
                where
                    is_group_start is true -- history rollup
            )

            merge into etl.seller_status_history as g
            using new_revs n
                on n.seller_id = g.seller_id
                and n.valid_from_dttm = g.valid_from_dttm
            when matched then
                do nothing -- to avoid inserting current slice again
            when not matched then
                insert
                    (
                        "load_id",
                        processed_dttm,
                        valid_from_dttm,
                        valid_to_dttm,
                        seller_id,
                        status
                    )
                values
                    (
                        n."load_id",
                        n.processed_dttm,
                        n.valid_from_dttm,
                        n.valid_to_dttm,
                        n.seller_id,
                        n.status
                    );

            get diagnostics
                row_cnt := row_count;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.sellers_history_seller_increment cascade;
        end if;

        select
            high_level_step || '.1' as src_step,
            'etl' as tgt_schema,
            'seller_status_history' as tgt_name,
            'merge' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (merge into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows merged', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;