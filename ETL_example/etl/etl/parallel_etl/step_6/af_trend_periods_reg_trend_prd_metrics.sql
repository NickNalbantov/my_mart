create or replace function etl.af_trend_periods_reg_trend_prd_metrics
(
    period_num smallint,
    ratings_to_volume_coeff numeric default 0.8, -- arbitrary coefficient representing ratio of reviews conversion to sales
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '6.2.7'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.' || period_num || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            period_num,
            ratings_to_volume_coeff,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Joining data from previous and current periods (regular trends)

        task_start_dttm := clock_timestamp();

        execute format($exec$
            drop table if exists etl_wrk.reg_trend_prd_metrics_%1$s cascade;

            create unlogged table if not exists etl_wrk.reg_trend_prd_metrics_%1$s
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    cur.gig_id,
                    cur.fiverr_seller_id,
                    cur.category_id,
                    cur.sub_category_id,
                    cur.nested_sub_category_id,
                    (
                        case
                            when %1$L::smallint = 1 then
                                case
                                    when
                                        nullif(cur.seller_ratings_count, 0::int) is null
                                        or nullif(prev.seller_ratings_count, 0::int) is null
                                        or (nullif(cur.seller_completed_orders_count, 0::int) is null and cur.gig_ratings_count > 0)
                                        or (nullif(prev.seller_completed_orders_count, 0::int) is null and prev.gig_ratings_count > 0)
                                    then
                                        round(cur.gig_ratings_count::numeric / %2$L::numeric) -- old version
                                    else
                                        round(coalesce(cur.gig_ratings_count::numeric * cur.seller_completed_orders_count::numeric / cur.seller_ratings_count::numeric, 0))
                                end
                            else
                                case
                                    when
                                        nullif(cur.seller_ratings_count, 0::int) is null
                                        or nullif(prev.seller_ratings_count, 0::int) is null
                                        or (nullif(cur.seller_completed_orders_count, 0::int) is null and cur.gig_ratings_count > 0)
                                        or (nullif(prev.seller_completed_orders_count, 0::int) is null and prev.gig_ratings_count > 0)
                                    then
                                        round((cur.gig_ratings_count + (cur.gig_ratings_count - coalesce(prev.gig_ratings_count, 0)))::numeric / %2$L::numeric) -- old version
                                    else
                                        round(
                                            (cur.gig_ratings_count - coalesce(prev.gig_ratings_count, 0))::numeric *
                                            cur.seller_completed_orders_count::numeric / cur.seller_ratings_count::numeric
                                        )
                                end
                        end
                    )::int as gig_volume,
                    cur.gig_is_active,
                    coalesce(rew.weighted_start_price_by_gig, 0::numeric) as weighted_start_price_by_gig,
                    coalesce(rew.weighted_end_price_by_gig, 'infinity'::numeric) as weighted_end_price_by_gig,
                    (select avg(price) from unnest(cur.prices) p (price)) as avg_price_by_gig
                from etl_wrk.reg_trend_prd_cur_metrics_%1$s cur
                left join etl_wrk.reg_trend_prd_prev_metrics_%1$s prev
                    on cur.gig_id = prev.gig_id
                left join etl_wrk.reg_trend_prd_gig_reviews_weighted_prices_%1$s rew
                    on cur.gig_id = rew.gig_id;

        $exec$,
        period_num,
        ratings_to_volume_coeff
        );

        get diagnostics
            row_cnt = row_count;

        execute format('analyze etl_wrk.reg_trend_prd_metrics_%1$s', period_num);

        if debug_mode is false then
            execute format ($exec$
                drop table if exists etl_wrk.reg_trend_prd_prev_metrics_%1$s cascade;
                drop table if exists etl_wrk.reg_trend_prd_gig_reviews_weighted_prices_%1$s cascade;
            $exec$, period_num);
        end if;

        select
            high_level_step || '.' || period_num || '.1' as src_step,
            'etl_wrk' as tgt_schema,
            'reg_trend_prd_metrics_' || period_num as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.' || period_num || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            period_num,
            ratings_to_volume_coeff,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;