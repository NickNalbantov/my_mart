create or replace function etl.af_trends_final
(
    intervals smallint default 3,
    students_coeff numeric default 1.96, -- critical value of correlation coefficient for Student's coefficient (by default - confidence interval = 0.95, t-statistics = 1.96)
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '10.1.5'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            intervals,
            students_coeff,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Calculating slope and R^2

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.trends cascade;

        create unlogged table if not exists etl_wrk.trends
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (gig_id)
                gig_id,
                fiverr_seller_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                full_avg_revenue_by_seller,
                full_avg_revenue_by_gig,
                full_avg_revenue_by_category,
                full_avg_revenue_by_subcategory,
                full_avg_revenue_by_nested_subcategory,
                coalesce(regr_slope(normalized_revenue_by_seller, period_num) over w_s, 0)::numeric as trend_by_seller,
                coalesce(regr_r2(normalized_revenue_by_seller, period_num) over w_s, 1)::numeric as r2_by_seller,
                coalesce(regr_slope(normalized_revenue_by_gig, period_num) over w_g, 0)::numeric as trend_by_gig,
                coalesce(regr_r2(normalized_revenue_by_gig, period_num) over w_g, 1)::numeric as r2_by_gig,
                coalesce(regr_slope(normalized_revenue_by_category, period_num) over w_cat, 0)::numeric as trend_by_category,
                coalesce(regr_r2(normalized_revenue_by_category, period_num) over w_cat, 1)::numeric as r2_by_category,
                coalesce(regr_slope(normalized_revenue_by_subcategory, period_num) over w_subcat, 0)::numeric as trend_by_subcategory,
                coalesce(regr_r2(normalized_revenue_by_subcategory, period_num) over w_subcat, 1)::numeric as r2_by_subcategory,
                case
                    when nested_sub_category_id is null then null
                    else
                        coalesce(regr_slope(normalized_revenue_by_nested_subcategory, period_num) over w_nest_subcat, 0)::numeric
                end as trend_by_nested_subcategory,
                case
                    when nested_sub_category_id is null then null
                    else
                        coalesce(regr_r2(normalized_revenue_by_nested_subcategory, period_num) over w_nest_subcat, 1)::numeric
                end as r2_by_nested_subcategory,
                students_coeff / sqrt((intervals + 1) - 2 + pow(students_coeff, 2))::numeric as rcrit
            from etl_wrk.trends_normalized
            window
                w_s as (partition by fiverr_seller_id),
                w_g as (partition by gig_id),
                w_cat as (partition by category_id),
                w_subcat as (partition by sub_category_id),
                w_nest_subcat as (partition by nested_sub_category_id)
            order by
                gig_id,
                period_num desc;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_trends
            on etl_wrk.trends (gig_id) with (fillfactor = 100);

        cluster etl_wrk.trends using uidx_trends;

        analyze etl_wrk.trends;

        if debug_mode is false then
            drop table if exists etl_wrk.trends_normalized cascade;
        end if;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk' as tgt_schema,
            'trends' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            intervals,
            students_coeff,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;