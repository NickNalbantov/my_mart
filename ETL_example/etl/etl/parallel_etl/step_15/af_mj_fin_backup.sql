create or replace function etl.af_mj_fin_backup
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '15.1.1'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Moving results to backup

        task_start_dttm := clock_timestamp();

        execute format ($exec$
            alter table if exists etl.metrics set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl_wrk.metrics_backup set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            drop table if exists etl_wrk.metrics_backup_%1$s cascade;
            alter table if exists etl_wrk.metrics_backup rename to metrics_backup_%1$s;
            alter index if exists etl_wrk.metrics_backup_pkey rename to metrics_backup_%1$s_pkey;
            alter index if exists etl_wrk.idx_seller_id_metrics_backup rename to idx_seller_id_metrics_backup_%1$s;
            alter index if exists etl_wrk.idx_category_id_metrics_backup rename to idx_category_id_metrics_backup_%1$s;
            alter index if exists etl_wrk.idx_sub_category_id_metrics_backup rename to idx_sub_category_id_metrics_backup_%1$s;
            alter index if exists etl_wrk.idx_nested_sub_category_id_metrics_backup rename to idx_nested_sub_category_id_metrics_backup_%1$s;
            alter index if exists etl_wrk.idx_categories_metrics_backup rename to idx_categories_metrics_backup_%1$s;
            alter statistics etl_wrk.metrics_all_ids_backup rename to metrics_all_ids_backup_%1$s;

            alter table if exists etl.gigs set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl_wrk.gigs_backup set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            drop table if exists etl_wrk.gigs_backup_%1$s cascade;
            alter table if exists etl_wrk.gigs_backup rename to gigs_backup_%1$s;
            alter index if exists etl_wrk.gigs_backup_pkey rename to gigs_backup_%1$s_pkey;
            alter index if exists etl_wrk.uidx_fiverr_gig_id_gigs_backup rename to uidx_fiverr_gig_id_gigs_backup_%1$s;
            alter index if exists etl_wrk.idx_seller_id_gigs_backup rename to idx_seller_id_gigs_backup_%1$s;
            alter index if exists etl_wrk.idx_fiverr_seller_id_gigs_backup rename to idx_fiverr_seller_id_gigs_backup_%1$s;
            alter index if exists etl_wrk.idx_category_id_gigs_backup rename to idx_category_id_gigs_backup_%1$s;
            alter index if exists etl_wrk.idx_sub_category_id_gigs_backup rename to idx_sub_category_id_gigs_backup_%1$s;
            alter index if exists etl_wrk.idx_nested_sub_category_id_gigs_backup rename to idx_nested_sub_category_id_gigs_backup_%1$s;
            alter index if exists etl_wrk.idx_categories_gigs_backup rename to idx_categories_gigs_backup_%1$s;
            alter index if exists etl_wrk.uidx_gigs_ranks_backup rename to uidx_gigs_ranks_backup_%1$s;
            alter index if exists etl_wrk.idx_cached_slug_gigs rename to idx_cached_slug_gigs_%1$s;
            alter index if exists etl_wrk.idx_trg_title_gigs_backup rename to idx_trg_title_gigs_backup_%1$s;
            alter index if exists etl_wrk.idx_trg_seller_name_gigs_backup rename to idx_trg_seller_name_gigs_backup_%1$s;
            alter index if exists etl_wrk.idx_trg_cat_name_gigs_backup rename to idx_trg_cat_name_gigs_backup_%1$s;
            alter index if exists etl_wrk.idx_trg_subcat_name_gigs_backup rename to idx_trg_subcat_name_gigs_backup_%1$s;
            alter index if exists etl_wrk.idx_trg_nest_subcat_name_gigs_backup rename to idx_trg_nest_subcat_name_gigs_backup_%1$s;
            alter statistics etl_wrk.gigs_all_ids_backup rename to gigs_all_ids_backup_%1$s;

            alter table if exists etl.sellers set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl_wrk.sellers_backup set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            drop table if exists etl_wrk.sellers_backup_%1$s cascade;
            alter table if exists etl_wrk.sellers_backup rename to sellers_backup_%1$s;
            alter index if exists etl_wrk.sellers_backup_pkey rename to sellers_backup_%1$s_pkey;
            alter index if exists etl_wrk.uidx_fiverr_seller_id_sellers_backup rename to uidx_fiverr_seller_id_sellers_backup_%1$s;
            alter index if exists etl_wrk.idx_sellers_best_selling_gig_id_backup rename to idx_sellers_best_selling_gig_id_backup_%1$s;
            alter index if exists etl_wrk.uidx_sellers_ranks_backup rename to uidx_sellers_ranks_backup_%1$s;
            alter index if exists etl_wrk.idx_trg_seller_name_sellers_backup rename to idx_trg_seller_name_sellers_backup_%1$s;
            alter statistics etl_wrk.sellers_all_ids_backup rename to sellers_all_ids_backup_%1$s;

            alter table if exists etl.categories set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl_wrk.categories_backup set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            drop table if exists etl_wrk.categories_backup_%1$s cascade;
            alter table if exists etl_wrk.categories_backup rename to categories_backup_%1$s;
            alter index if exists etl_wrk.categories_backup_pkey rename to categories_backup_%1$s_pkey;
            alter index if exists etl_wrk.uidx_categories_ranks_backup rename to uidx_categories_ranks_backup_%1$s;
            alter index if exists etl_wrk.idx_trg_cat_name_cat_backup rename to idx_trg_cat_name_cat_backup_%1$s;

            alter table if exists etl.subcategories set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl_wrk.subcategories_backup set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            drop table if exists etl_wrk.subcategories_backup_%1$s cascade;
            alter table if exists etl_wrk.subcategories_backup rename to subcategories_backup_%1$s;
            alter index if exists etl_wrk.subcategories_backup_pkey rename to subcategories_backup_%1$s_pkey;
            alter index if exists etl_wrk.uidx_subcategories_ranks_backup rename to uidx_subcategories_ranks_backup_%1$s;
            alter index if exists etl_wrk.idx_trg_subcat_name_subcat_backup rename to idx_trg_subcat_name_subcat_backup_%1$s;

            alter table if exists etl.nested_subcategories set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl_wrk.nested_subcategories_backup set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            drop table if exists etl_wrk.nested_subcategories_backup_%1$s cascade;
            alter table if exists etl_wrk.nested_subcategories_backup rename to nested_subcategories_backup_%1$s;
            alter index if exists etl_wrk.nested_subcategories_backup_pkey rename to nested_subcategories_backup_%1$s_pkey;
            alter index if exists etl_wrk.uidx_nested_subcategories_ranks_backup rename to uidx_nested_subcategories_ranks_backup_%1$s;
            alter index if exists etl_wrk.idx_trg_nest_subcat_name_nest_subcat_backup rename to idx_trg_nest_subcat_name_nest_subcat_backup_%1$s;

            alter table if exists etl.all_categories set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl_wrk.all_categories_backup set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            drop table if exists etl_wrk.all_categories_backup_%1$s cascade;
            alter table if exists etl_wrk.all_categories_backup rename to all_categories_backup_%1$s;
            alter index if exists etl_wrk.all_categories_backup_pkey rename to all_categories_backup_%1$s_pkey;
            alter index if exists etl_wrk.uidx_all_id_all_cats_backup rename to uidx_all_id_all_cats_backup_%1$s;
            alter index if exists etl_wrk.idx_cat_id_all_cats_backup rename to idx_cat_id_all_cats_backup_%1$s;
            alter index if exists etl_wrk.idx_sub_cat_id_all_cats_backup rename to idx_sub_cat_id_all_cats_backup_%1$s;
            alter index if exists etl_wrk.idx_nest_sub_cat_id_all_cats_backup rename to idx_nest_sub_cat_id_all_cats_backup_%1$s;
            alter index if exists etl_wrk.idx_trg_cat_name_all_cats rename to idx_trg_cat_name_all_cats_%1$s;
            alter index if exists etl_wrk.idx_trg_subcat_name_all_cats rename to idx_trg_subcat_name_all_cats_%1$s;
            alter index if exists etl_wrk.idx_trg_nest_subcat_name_all_cats rename to idx_trg_nest_subcat_name_all_cats_%1$s;

            alter table if exists etl.category_x_seller_levels set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl_wrk.category_x_seller_levels_backup set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            drop table if exists etl_wrk.category_x_seller_levels_backup_%1$s cascade;
            alter table if exists etl_wrk.category_x_seller_levels_backup rename to category_x_seller_levels_backup_%1$s;
            alter index if exists etl_wrk.category_x_seller_levels_backup_pkey rename to category_x_seller_levels_backup_%1$s_pkey;
            alter index if exists etl_wrk.uidx_cat_sel_lvl rename to uidx_cat_sel_lvl_%1$s;
            alter index if exists etl_wrk.idx_trg_cat_name_cat_sel_lvl rename to idx_trg_cat_name_cat_sel_lvl_%1$s;

            alter table if exists etl.subcategory_x_seller_levels set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl_wrk.subcategory_x_seller_levels_backup set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            drop table if exists etl_wrk.subcategory_x_seller_levels_backup_%1$s cascade;
            alter table if exists etl_wrk.subcategory_x_seller_levels_backup rename to subcategory_x_seller_levels_backup_%1$s;
            alter index if exists etl_wrk.subcategory_x_seller_levels_backup_pkey rename to subcategory_x_seller_levels_backup_%1$s_pkey;
            alter index if exists etl_wrk.uidx_subcat_sel_lvl rename to uidx_subcat_sel_lvl_%1$s;
            alter index if exists etl_wrk.idx_trg_subcat_name_subcat_sel_lvl rename to idx_trg_subcat_name_subcat_sel_lvl_%1$s;

            alter table if exists etl.nested_subcategory_x_seller_levels set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl_wrk.nested_subcategory_x_seller_levels_backup set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            drop table if exists etl_wrk.nested_subcategory_x_seller_levels_backup_%1$s cascade;
            alter table if exists etl_wrk.nested_subcategory_x_seller_levels_backup rename to nested_subcategory_x_seller_levels_backup_%1$s;
            alter index if exists etl_wrk.nested_subcategory_x_seller_levels_backup_pkey rename to nested_subcategory_x_seller_levels_backup_%1$s_pkey;
            alter index if exists etl_wrk.uidx_nest_subcat_sel_lvl rename to uidx_nest_subcat_sel_lvl_%1$s;
            alter index if exists etl_wrk.idx_trg_nest_subcat_name_nest_subcat_sel_lvl rename to idx_trg_nest_subcat_name_nest_subcat_sel_lvl_%1$s;

            select public.maintenance_partitions_autovacuum_switch('etl.gigs_revisions_pre_metrics'::regclass, true);
            alter table if exists etl.gigs_revisions_pre_metrics_invalid set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            select public.maintenance_partitions_autovacuum_switch('etl.gigs_pages_pre_metrics'::regclass, true);
            alter table if exists etl.gigs_pages_pre_metrics_invalid set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            select public.maintenance_partitions_autovacuum_switch('etl.sellers_revisions_pre_metrics'::regclass, true);
            alter table if exists etl.sellers_revisions_pre_metrics_invalid set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl.gig_status_history set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl.seller_status_history set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            alter table if exists etl.gig_reviews_agg set (autovacuum_enabled = true, toast.autovacuum_enabled = true);
            select public.maintenance_partitions_autovacuum_switch('etl.gig_reviews_prices'::regclass, true);
        $exec$,
        load_id::text
        );

        select
            high_level_step || '.1' as src_step,
            'etl_wrk' as tgt_schema,
            null as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (Backup and cleanup done)';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;