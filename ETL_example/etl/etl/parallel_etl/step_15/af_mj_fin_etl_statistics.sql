create or replace function etl.af_mj_fin_etl_statistics
(
    etl_schema text,
    metrics_start_dttm timestamptz,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '15.1.2'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
#variable_conflict use_variable

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

        targets_rec record;
        attr_rec record;

        tgt_stats_qry text := '';
        agg_qry text;
        to_js_qry text;
        js_arr_qry text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            etl_schema,
            metrics_start_dttm,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Gathering statistics

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.attributes_props cascade;

        create unlogged table if not exists etl_wrk.attributes_props
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on
                (
                    tgt_schema,
                    tgt_name,
                    attribute_number
                )
                    a.attnum::smallint as attribute_number,
                    a.attnotnull::boolean as is_not_null, -- does not require null / not null checks
                    substring(a.attname::text, '(^|_)id$') is not null as is_id, -- does not require checks for values
                    case when i.indexrelid is not null then true else false end as is_unique, -- does not require cnt_distinct check
                    etl_schema as tgt_schema,
                    c.relname::text as tgt_name,
                    a.attname::text as attribute_name,
                    a.atttypid::regtype::text as attribute_type,
                    l.src_schema as src_schema,
                    l.src_name
                from pg_catalog.pg_attribute a
                join pg_catalog.pg_class c
                    on a.attrelid = c."oid"
                join etl_log.etl_log l
                    on l.tgt_name = c.relname::text
                    and l.tgt_schema = etl_schema
                left join pg_catalog.pg_index i
                    on a.attrelid = i.indrelid
                    and i.indisunique is true
                    and i.indnkeyatts = 1
                    and i.indkey[0] = a.attnum
                where
                    a.attname not in ('processed_dttm', 'load_id')
                    and a.attisdropped is false
                    and a.attnum > 0
                    and c.relnamespace = etl_schema::regnamespace
                    and c.relkind in ('r', 'p')
                    and substring(c.relname, '(_invalid|(^|ly_)metrics$|all_categories)$') is null -- checking only final decomposed marts without xxx_pre_metrics_invalid tables and wide and heavy composed marts
                    and l.load_id = load_id
                order by
                    tgt_schema,
                    tgt_name,
                    attribute_number,
                    l.processed_dttm desc;

        -- insert query

        tgt_stats_qry := E'insert into etl_log.etl_attributes_stats' || E'\n(\n\t' ||
            'processed_dttm,' || E'\n\t'
            || 'load_id,' || E'\n\t'
            || 'cnt_null,' || E'\n\t'
            || 'cnt_not_null,' || E'\n\t'
            || 'cnt_distinct,' || E'\n\t'
            || 'cnt_zero,' || E'\n\t'
            || 'cnt_below_zero,' || E'\n\t'
            || 'cnt_true,' || E'\n\t'
            || 'cnt_false,' || E'\n\t'
            || 'attribute_number,' || E'\n\t'
            || 'min_value,' || E'\n\t'
            || 'avg_value,' || E'\n\t'
            || 'max_value,' || E'\n\t'
            || 'attribute_name,' || E'\n\t'
            || 'attribute_type,' || E'\n\t'
            || 'tgt_schema,' || E'\n\t'
            || 'tgt_name,' || E'\n\t'
            || 'src_schema,' || E'\n\t'
            || 'src_name' || E'\n)'
            || repeat(E'\n', 2) || E'\t'
            || 'with results as'
            || E'\n\t(\n' || repeat(E'\t', 2);

        <<targets_loop>>
        for targets_rec in
            select distinct
                tgt_schema,
                tgt_name,
                src_schema,
                src_name
            from etl_wrk.attributes_props
            order by
                tgt_schema,
                tgt_name
        loop

             -- outermost query (lvl 1) - unpacking results

            tgt_stats_qry = tgt_stats_qry ||
                'select' || E'\n' || repeat(E'\t', 3)
                || '(atts[''aggs''] ->> ''cnt_null'')::bigint as cnt_null,' || E'\n' || repeat(E'\t', 3)
                || '(atts[''aggs''] ->> ''cnt_not_null'')::bigint as cnt_not_null,' || E'\n' || repeat(E'\t', 3)
                || '(atts[''aggs''] ->> ''cnt_distinct'')::bigint as cnt_distinct,' || E'\n' || repeat(E'\t', 3)
                || '(atts[''aggs''] ->> ''cnt_zero'')::bigint as cnt_zero,' || E'\n' || repeat(E'\t', 3)
                || '(atts[''aggs''] ->> ''cnt_below_zero'')::bigint as cnt_below_zero,' || E'\n' || repeat(E'\t', 3)
                || '(atts[''aggs''] ->> ''cnt_true'')::bigint as cnt_true,' || E'\n' || repeat(E'\t', 3)
                || '(atts[''aggs''] ->> ''cnt_false'')::bigint as cnt_false,' || E'\n' || repeat(E'\t', 3)
                || '(atts ->> ''att_num'')::smallint as attribute_number,' || E'\n' || repeat(E'\t', 3)
                || '(atts[''aggs''] ->> ''min_value'')::numeric as min_value,' || E'\n' || repeat(E'\t', 3)
                || '(atts[''aggs''] ->> ''avg_value'')::numeric as avg_value,' || E'\n' || repeat(E'\t', 3)
                || '(atts[''aggs''] ->> ''max_value'')::numeric as max_value,' || E'\n' || repeat(E'\t', 3)
                || '(atts ->> ''att_name'')::text as attribute_name,' || E'\n' || repeat(E'\t', 3)
                || '(atts ->> ''att_type'')::text as attribute_type,' || E'\n' || repeat(E'\t', 3)
                || '''' || targets_rec.tgt_schema || E'''::text as tgt_schema,' || E'\n' || repeat(E'\t', 3)
                || '''' || targets_rec.tgt_name || E'''::text as tgt_name,' || E'\n' || repeat(E'\t', 3)
                || '''' || targets_rec.src_schema || E'''::text as src_schema,' || E'\n' || repeat(E'\t', 3)
                || '''' || targets_rec.src_name || E'''::text as src_name' || E'\n' || repeat(E'\t', 2)
                || 'from' || E'\n' || repeat(E'\t', 2) || E'(\n' || repeat(E'\t', 3);

            js_arr_qry := 'select' ||
                E'\n' || repeat(E'\t', 4)
                || 'jsonb_array_elements' || E'\n' || repeat(E'\t', 4)
                || '(' || E'\n' || repeat(E'\t', 5)
                || 'jsonb_build_array' || E'\n' || repeat(E'\t', 5)
                || '(' || E'\n' || repeat(E'\t', 6);

            to_js_qry := 'select' || E'\n' || repeat(E'\t', 5);

            agg_qry := 'select' || E'\n' || repeat(E'\t', 6);

            <<attr_loop>>
            for attr_rec in
                select
                    attribute_number,
                    is_not_null,
                    is_id,
                    is_unique,
                    attribute_name,
                    attribute_type
                from etl_wrk.attributes_props
                where
                    tgt_schema = targets_rec.tgt_schema
                    and tgt_name = targets_rec.tgt_name
                order by
                    attribute_number
            loop

                -- lvl 2 query - -- pivoting JSONB arrays

                js_arr_qry := js_arr_qry ||
                    'js_' || attr_rec.attribute_number || E',\n' || repeat(E'\t', 6);

                -- lvl 3 query - building JSONBs

                to_js_qry := to_js_qry ||
                    E'''{\n' || repeat(E'\t', 6) || '"att_name": "' || attr_rec.attribute_name || '",'
                    || E'\n' || repeat(E'\t', 6) || '"att_num": ' || attr_rec.attribute_number || ','
                    || E'\n' || repeat(E'\t', 6) || '"att_type": "' || attr_rec.attribute_type
                    || E'"\n' || repeat(E'\t', 5) || '}''::jsonb ||' || E'\n' || repeat(E'\t', 5)
                    || 'jsonb_build_object' || E'\n' || repeat(E'\t', 5)
                    || '(' || E'\n' || repeat(E'\t', 6)
                    || '''aggs'',' || E'\n' || repeat(E'\t', 6)
                    || 'to_jsonb' || E'\n' || repeat(E'\t', 6)
                    || '(' || E'\n' || repeat(E'\t', 7)
                    || '(' || E'\n' || repeat(E'\t', 8)
                    || 'select' || E'\n' || repeat(E'\t', 9)
                    || 'aggregations' || E'\n' || repeat(E'\t', 8)
                    || 'from' || E'\n' || repeat(E'\t', 8)
                    || '(' || E'\n' || repeat(E'\t', 9)
                    || 'select' || E'\n' || repeat(E'\t', 10)
                    || 'agg.cnt_null_' || attr_rec.attribute_number || ' as cnt_null,' || E'\n' || repeat(E'\t', 10)
                    || 'agg.cnt_not_null_' || attr_rec.attribute_number || ' as cnt_not_null,' || E'\n' || repeat(E'\t', 10)
                    || 'agg.cnt_distinct_' || attr_rec.attribute_number || ' as cnt_distinct,' || E'\n' || repeat(E'\t', 10)
                    || 'agg.cnt_zero_' || attr_rec.attribute_number || ' as cnt_zero,' || E'\n' || repeat(E'\t', 10)
                    || 'agg.cnt_below_zero_' || attr_rec.attribute_number || ' as cnt_below_zero,' || E'\n' || repeat(E'\t', 10)
                    || 'agg.cnt_true_' || attr_rec.attribute_number || ' as cnt_true,' || E'\n' || repeat(E'\t', 10)
                    || 'agg.cnt_false_' || attr_rec.attribute_number || ' as cnt_false,' || E'\n' || repeat(E'\t', 10)
                    || 'agg.min_value_' || attr_rec.attribute_number || ' as min_value,' || E'\n' || repeat(E'\t', 10)
                    || 'agg.avg_value_' || attr_rec.attribute_number || ' as avg_value,' || E'\n' || repeat(E'\t', 10)
                    || 'agg.max_value_' || attr_rec.attribute_number || ' as max_value' || E'\n' || repeat(E'\t', 8)
                    || ') aggregations' || E'\n' || repeat(E'\t', 7)
                    || ')' || E'\n' || repeat(E'\t', 6)
                    || ')' || E'\n' || repeat(E'\t', 5)
                    || ') js_' || attr_rec.attribute_number || E',\n' || repeat(E'\t', 5);

                -- innermost query (lvl 4) - aggregations

                agg_qry := agg_qry ||
                    case
                        when attr_rec.is_not_null is true then '0'
                        else 'count(*) filter (where ' || attr_rec.attribute_name || E' is null)'
                    end || '::bigint as cnt_null_' || attr_rec.attribute_number || E',\n' || repeat(E'\t', 6) ||
                    case
                        when attr_rec.is_not_null is true then 'count(*)'
                        else 'count(*) filter (where ' || attr_rec.attribute_name || E' is not null)'
                    end || '::bigint as cnt_not_null_' || attr_rec.attribute_number || E',\n' || repeat(E'\t', 6) ||
                    case
                        when attr_rec.is_unique is true then 'count(*)'
                        else 'count(distinct ' || attr_rec.attribute_name || E')'
                    end || '::bigint as cnt_distinct_' || attr_rec.attribute_number || E',\n' || repeat(E'\t', 6) ||
                    case
                        when attr_rec.attribute_type in ('numeric', 'bigint', 'integer') and attr_rec.is_id is false
                            then 'count(*) filter (where ' || attr_rec.attribute_name || ' = 0)'
                        else 'null'
                    end || '::bigint as cnt_zero_' || attr_rec.attribute_number || E',\n' || repeat(E'\t', 6) ||
                    case
                        when attr_rec.attribute_type = 'numeric' -- there are no bigint / int metrics that can be < 0
                            then 'count(*) filter (where ' || attr_rec.attribute_name || ' < 0)'
                        else 'null'
                    end || '::bigint as cnt_below_zero_' || attr_rec.attribute_number || E',\n' || repeat(E'\t', 6) ||
                    case
                        when attr_rec.attribute_type = 'boolean'
                            then 'count(*) filter (where ' || attr_rec.attribute_name || ' is true)'
                        else 'null'
                    end || '::bigint as cnt_true_' || attr_rec.attribute_number || E',\n' || repeat(E'\t', 6) ||
                    case
                        when attr_rec.attribute_type = 'boolean'
                            then 'count(*) filter (where ' || attr_rec.attribute_name || ' is false)'
                        else 'null'
                    end || '::bigint as cnt_false_' || attr_rec.attribute_number || E',\n' || repeat(E'\t', 6) ||
                    case
                        when attr_rec.attribute_type in ('numeric', 'bigint', 'integer') and attr_rec.is_id is false
                            then 'min(' || attr_rec.attribute_name || ')'
                        else 'null'
                    end || '::numeric as min_value_' || attr_rec.attribute_number || E',\n' || repeat(E'\t', 6) ||
                    case
                        when attr_rec.attribute_type in ('numeric', 'bigint', 'integer') and attr_rec.is_id is false
                            then 'avg(' || attr_rec.attribute_name || ')'
                        else 'null'
                    end || '::numeric as avg_value_' || attr_rec.attribute_number || E',\n' || repeat(E'\t', 6) ||
                    case
                        when attr_rec.attribute_type in ('numeric', 'bigint', 'integer') and attr_rec.is_id is false
                            then 'max(' || attr_rec.attribute_name || ')'
                        else 'null'
                    end || '::numeric as max_value_' || attr_rec.attribute_number || E',\n' || repeat(E'\t', 6);

            end loop attr_loop;

        -- building up resulting target query with froms and closing brackets

        tgt_stats_qry := tgt_stats_qry
            || rtrim(js_arr_qry, E',\n' || repeat(E'\t', 6))
            || E'\n' || repeat(E'\t', 5) || ')' -- closing jsonb_build_array
            || E'\n' || repeat(E'\t', 4) || ') as atts'  -- closing jsonb_array_elements
            || E'\n' || repeat(E'\t', 3) || 'from' || E'\n' || repeat(E'\t', 3)
            || '(' || E'\n' || repeat(E'\t', 4) -- opening to_js subquery (lvl 3)
            || rtrim(to_js_qry, E',\n' || repeat(E'\t', 5))
            || E'\n' || repeat(E'\t', 4) || 'from' || E'\n' || repeat(E'\t', 4)
            || '(' || E'\n' || repeat(E'\t', 5) -- opening agg subquery (lvl 4)
            || rtrim(agg_qry, E',\n' || repeat(E'\t', 6))
            || E'\n' || repeat(E'\t', 5)
            || 'from ' || targets_rec.tgt_schema || '.' || targets_rec.tgt_name
            || E'\n' || repeat(E'\t', 5)
            || case
                when substring(targets_rec.tgt_name, '(_pre_metrics|_status_history|gig_reviews_(agg|prices))$') is not null
                    and etl_schema = 'etl'
                    -- filtering persistent targets by current run load_id
                    then 'where' || E'\n' || repeat(E'\t', 6) || '"load_id" = ' || load_id || E'\n'
                when etl_schema = 'etl_agg'
                    -- filtering aggregated targets by partitioning key
                    then 'where' || E'\n' || repeat(E'\t', 6) || '"agg_period_start" = ''' || metrics_start_dttm::date || '''::date' || E'\n'
                else '' end
            || repeat(E'\t', 4) || ') agg' || E'\n' -- closing agg subquery (lvl 4)
            || repeat(E'\t', 3) || ') to_js' || E'\n' -- closing to_js subquery (lvl 3)
            || repeat(E'\t', 2) || ') js_arr' -- closing js_arr subquery (lvl 2)
            || repeat(E'\n', 2) || repeat(E'\t', 2) || 'union all' || repeat(E'\n', 2) || repeat(E'\t', 2);

        end loop targets_loop;

        tgt_stats_qry := rtrim(tgt_stats_qry, repeat(E'\n', 2) || repeat(E'\t', 2) || 'union all' || repeat(E'\n', 2) || repeat(E'\t', 2))
            || E'\n\t)' || repeat(E'\n', 2) || E'\t'
            || 'select' || E'\n' || repeat(E'\t', 2)
            || '''' || transaction_timestamp() || E'''::timestamptz as processed_dttm,' || E'\n' || repeat(E'\t', 2)
            || '''' || load_id || E'''::bigint as load_id,' || E'\n' || repeat(E'\t', 2)
            || 'r.cnt_null,' || E'\n' || repeat(E'\t', 2)
            || 'r.cnt_not_null,' || E'\n' || repeat(E'\t', 2)
            || 'r.cnt_distinct,' || E'\n' || repeat(E'\t', 2)
            || 'r.cnt_zero,' || E'\n' || repeat(E'\t', 2)
            || 'r.cnt_below_zero,' || E'\n' || repeat(E'\t', 2)
            || 'r.cnt_true,' || E'\n' || repeat(E'\t', 2)
            || 'r.cnt_false,' || E'\n' || repeat(E'\t', 2)
            || 'r.attribute_number,' || E'\n' || repeat(E'\t', 2)
            || 'r.min_value,' || E'\n' || repeat(E'\t', 2)
            || 'r.avg_value,' || E'\n' || repeat(E'\t', 2)
            || 'r.max_value,' || E'\n' || repeat(E'\t', 2)
            || 'r.attribute_name,' || E'\n' || repeat(E'\t', 2)
            || 'r.attribute_type,' || E'\n' || repeat(E'\t', 2)
            || 'r.tgt_schema,' || E'\n' || repeat(E'\t', 2)
            || 'r.tgt_name,' || E'\n' || repeat(E'\t', 2)
            || 'r.src_schema,' || E'\n' || repeat(E'\t', 2)
            || 'r.src_name' || E'\n\t'
            || 'from results r';

            -- executing generated insert

        execute tgt_stats_qry;

        get diagnostics
            row_cnt := row_count;

        cluster etl_log.etl_attributes_stats using etl_attributes_stats_pkey;

        analyze etl_log.etl_attributes_stats;

        if debug_mode is false then
            drop table if exists etl_wrk.metrics_ratings_stats cascade;
            drop table if exists etl_wrk.categories_prefin cascade;
            drop table if exists etl_wrk.seller_counts cascade;
            drop table if exists etl_wrk.attributes_props cascade;
        end if;

        select
            high_level_step || '.1' as src_step,
            'etl_log' as tgt_schema,
            'etl_attributes_stats' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            etl_schema,
            metrics_start_dttm,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;