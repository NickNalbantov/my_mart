create or replace function etl.af_mj_fin_log_maintenance
(
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    dag_name text default 'manual_launch',
    high_level_step text default '15.1.3'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        if
            public.dblink_get_connections() is not null
            and load_id || '_conn' = any(public.dblink_get_connections()) then

                perform public.dblink_disconnect(load_id || '_conn');
        end if;

        cluster etl_log.etl_log using etl_log_pkey;

        analyze etl_log.etl_log;

        perform
            pg_terminate_backend(pid)
        from pg_catalog.pg_stat_activity
        where
            usename = 'airflow'
            and
                (
                    case dag_name
                        when 'etl_agg_monthly' then application_name like 'Monthly aggregation ETL%'
                        when 'etl_agg_quarterly' then application_name like 'Quarterly aggregation ETL%'
                        when 'etl_agg_yearly' then application_name like 'Yearly aggregation ETL%'
                        when 'etl_regular' then application_name like 'Regular ETL%'
                    end
                    or dag_name = 'manual_launch'
                )
            and pid != pg_backend_pid();

        log_msg := 'Log maintenance has been finished';

        total_runtime := clock_timestamp() - transaction_timestamp();

        -- returning valid Python dictionary with load_id, runtime and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', high_level_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;