create or replace function etl.trends_func
(
    start_dttm timestamptz default (current_date::timestamp at time zone 'UTC' - '3 month'::interval)::timestamptz,
    end_dttm timestamptz default (current_date::timestamp at time zone 'UTC' + '1 day'::interval)::timestamptz,
    intervals smallint default 3,
    students_coeff numeric default 1.96, -- critical value of correlation coefficient for Student's coefficient (by default - confidence interval = 0.95, t-statistics = 1.96)
    ratings_to_volume_coeff numeric default 0.8, -- arbitrary coefficient representing ratio of reviews conversion to sales
    overpriced_coeff numeric default 10, -- crutch to filter gigs with suspiciously high prices (higher than weighted average price in subcategory multiplied by this coefficient)
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '5'
)
returns void
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        run_start_dttm timestamptz;
        total_runtime interval;

        periods record;

    begin

        -- Starting

        run_start_dttm := clock_timestamp();

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            start_dttm,
            end_dttm,
            intervals,
            students_coeff,
            ratings_to_volume_coeff,
            overpriced_coeff,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 -- Preparing table for results

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.trends_prefin cascade;

        create unlogged table if not exists etl_wrk.trends_prefin
        (
            gig_id int,
            fiverr_seller_id int,
            category_id int,
            sub_category_id int,
            nested_sub_category_id int,
            period_num int,
            avg_revenue_by_seller numeric,
            avg_revenue_by_gig numeric,
            avg_revenue_by_category numeric,
            avg_revenue_by_subcategory numeric,
            avg_revenue_by_nested_subcategory numeric
        )
        with
        (
            autovacuum_enabled = false,
            toast.autovacuum_enabled = false
        );

        select
            high_level_step || '.1' as src_step,
            'etl_wrk' as tgt_schema,
            'trends_prefin' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (create table ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 2 - calculating data for each period

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.2' as src_step,
            'etl' as tgt_schema,
            'trend_period_func' as tgt_name,
            'routine call' as op_type
        into log_rec;

        drop table if exists etl_wrk.trends_periods_borders cascade;

        create unlogged table if not exists etl_wrk.trends_periods_borders
            with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
            as

            select
                t.rn::smallint as period_num,
                start_dttm as period_start,
                coalesce(lead(t.pit) over (order by t.pit), end_dttm) as period_end
            from generate_series(start_dttm, end_dttm, (end_dttm - start_dttm) / intervals) with ordinality t (pit, rn)
            order by t.pit
            limit (intervals);

        for periods in
            select
                period_num,
                period_end
            from etl_wrk.trends_periods_borders
            order by period_num
        loop
            perform etl.trend_period_func(
                start_dttm,
                periods.period_end,
                periods.period_num,
                ratings_to_volume_coeff,
                overpriced_coeff,
                load_id,
                debug_mode,
                dag_name,
                log_rec.src_step
            );
        end loop;

        create unique index if not exists uidx_trends_prefin
            on etl_wrk.trends_prefin (gig_id, period_num desc) with (fillfactor = 100);

        analyze etl_wrk.trends_prefin;

        if debug_mode is false then
            drop table if exists etl_wrk.trends_periods_borders cascade;
        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (all trend periods calculated)';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 3 - IDs from the last period

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.trends_ids_pulling cascade;

        create unlogged table if not exists etl_wrk.trends_ids_pulling
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as
            -- gigs can switch categories between periods, picking only last known value
            select
                gig_id,
                first_value(fiverr_seller_id) over w as fiverr_seller_id,
                first_value(category_id) over w as category_id,
                first_value(sub_category_id) over w as sub_category_id,
                first_value(nested_sub_category_id) over w as nested_sub_category_id,
                period_num,
                avg_revenue_by_seller,
                avg_revenue_by_gig,
                avg_revenue_by_category,
                avg_revenue_by_subcategory,
                avg_revenue_by_nested_subcategory
            from etl_wrk.trends_prefin
            window
                w as (partition by gig_id order by period_num desc);

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_trends_ids_pulling
            on etl_wrk.trends_ids_pulling (gig_id, period_num) with (fillfactor = 100);

        analyze etl_wrk.trends_ids_pulling;

        if debug_mode is false then
            drop table if exists etl_wrk.trends_prefin cascade;
        end if;

        select
            high_level_step || '.3' as src_step,
            'etl_wrk' as tgt_schema,
            'trends_ids_pulling' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 4 - Fields pulling

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.trends_fields_pulling cascade;

        create unlogged table if not exists etl_wrk.trends_fields_pulling
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as
             -- notice the usage of custom aggregate public.last for correct building of trend history
            select
                gig_id,
                fiverr_seller_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                period_num,
                coalesce(public.last(avg_revenue_by_seller) over (partition by fiverr_seller_id order by period_num), 0) as avg_revenue_by_seller,
                coalesce(public.last(avg_revenue_by_gig) over (partition by gig_id order by period_num), 0) as avg_revenue_by_gig,
                coalesce(public.last(avg_revenue_by_category) over (partition by category_id order by period_num), 0) as avg_revenue_by_category,
                coalesce(public.last(avg_revenue_by_subcategory) over (partition by sub_category_id order by period_num), 0) as avg_revenue_by_subcategory,
                case
                    when nested_sub_category_id is null then null
                    else
                        coalesce(public.last(avg_revenue_by_nested_subcategory) over (partition by nested_sub_category_id order by period_num), 0)
                end as avg_revenue_by_nested_subcategory
            from etl_wrk.trends_ids_pulling;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_trends_fields_pulling
            on etl_wrk.trends_fields_pulling (gig_id, period_num desc) with (fillfactor = 100);

        analyze etl_wrk.trends_fields_pulling;

        if debug_mode is false then
            drop table if exists etl_wrk.trends_ids_pulling cascade;
        end if;

        select
            high_level_step || '.4' as src_step,
            'etl_wrk' as tgt_schema,
            'trends_fields_pulling' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 5 - Calculating full average

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.trends_full_average cascade;

        create unlogged table if not exists etl_wrk.trends_full_average
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                gig_id,
                fiverr_seller_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                period_num,
                -- average values by periods
                avg_revenue_by_seller,
                avg_revenue_by_gig,
                avg_revenue_by_category,
                avg_revenue_by_subcategory,
                avg_revenue_by_nested_subcategory,
                -- average values across all periods
                avg(avg_revenue_by_seller) over (partition by fiverr_seller_id) as full_avg_revenue_by_seller,
                avg(avg_revenue_by_gig) over (partition by gig_id) as full_avg_revenue_by_gig,
                avg(avg_revenue_by_category) over (partition by category_id) as full_avg_revenue_by_category,
                avg(avg_revenue_by_subcategory) over (partition by sub_category_id) as full_avg_revenue_by_subcategory,
                case
                    when nested_sub_category_id is null then null
                    else
                        avg(avg_revenue_by_nested_subcategory) over (partition by nested_sub_category_id)
                end as full_avg_revenue_by_nested_subcategory
            from etl_wrk.trends_fields_pulling;

        get diagnostics
            row_cnt = row_count;

        analyze etl_wrk.trends_full_average;

        if debug_mode is false then
            drop table if exists etl_wrk.trends_fields_pulling cascade;
        end if;

        select
            high_level_step || '.5' as src_step,
            'etl_wrk' as tgt_schema,
            'trends_full_average' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 6 - Normalization

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.trends_normalized cascade;

        create unlogged table if not exists etl_wrk.trends_normalized
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select
                gig_id,
                fiverr_seller_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                period_num,
                full_avg_revenue_by_seller,
                full_avg_revenue_by_gig,
                full_avg_revenue_by_category,
                full_avg_revenue_by_subcategory,
                full_avg_revenue_by_nested_subcategory,
                -- normalized revenue (difference between average revenue in period and average revenue across all periods of revenue)
                coalesce((avg_revenue_by_seller - full_avg_revenue_by_seller)::numeric /
                    nullif(full_avg_revenue_by_seller, 0)::numeric, 0)::numeric as normalized_revenue_by_seller,
                coalesce((avg_revenue_by_gig - full_avg_revenue_by_gig)::numeric /
                    nullif(full_avg_revenue_by_gig, 0)::numeric, 0)::numeric as normalized_revenue_by_gig,
                coalesce((avg_revenue_by_category - full_avg_revenue_by_category)::numeric /
                    nullif(full_avg_revenue_by_category, 0)::numeric, 0)::numeric as normalized_revenue_by_category,
                coalesce((avg_revenue_by_subcategory - full_avg_revenue_by_subcategory)::numeric /
                    nullif(full_avg_revenue_by_subcategory, 0)::numeric, 0)::numeric as normalized_revenue_by_subcategory,
                case
                    when nested_sub_category_id is null then null
                    else
                        coalesce((avg_revenue_by_nested_subcategory - full_avg_revenue_by_nested_subcategory)::numeric /
                            nullif(full_avg_revenue_by_nested_subcategory, 0)::numeric, 0)::numeric
                end as normalized_revenue_by_nested_subcategory
            from etl_wrk.trends_full_average;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_trends_normalized
            on etl_wrk.trends_normalized (gig_id, period_num desc) with (fillfactor = 100);

        analyze etl_wrk.trends_normalized;

        if debug_mode is false then
            drop table if exists etl_wrk.trends_full_average cascade;
        end if;

        select
            high_level_step || '.6' as src_step,
            'etl_wrk' as tgt_schema,
            'trends_normalized' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 7 - Calculating slope and R^2

        task_start_dttm := clock_timestamp();

        drop table if exists etl_wrk.trends cascade;

        create unlogged table if not exists etl_wrk.trends
            with
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            )
            as

            select distinct on (gig_id)
                gig_id,
                fiverr_seller_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                full_avg_revenue_by_seller,
                full_avg_revenue_by_gig,
                full_avg_revenue_by_category,
                full_avg_revenue_by_subcategory,
                full_avg_revenue_by_nested_subcategory,
                coalesce(regr_slope(normalized_revenue_by_seller, period_num) over w_s, 0)::numeric as trend_by_seller,
                coalesce(regr_r2(normalized_revenue_by_seller, period_num) over w_s, 1)::numeric as r2_by_seller,
                coalesce(regr_slope(normalized_revenue_by_gig, period_num) over w_g, 0)::numeric as trend_by_gig,
                coalesce(regr_r2(normalized_revenue_by_gig, period_num) over w_g, 1)::numeric as r2_by_gig,
                coalesce(regr_slope(normalized_revenue_by_category, period_num) over w_cat, 0)::numeric as trend_by_category,
                coalesce(regr_r2(normalized_revenue_by_category, period_num) over w_cat, 1)::numeric as r2_by_category,
                coalesce(regr_slope(normalized_revenue_by_subcategory, period_num) over w_subcat, 0)::numeric as trend_by_subcategory,
                coalesce(regr_r2(normalized_revenue_by_subcategory, period_num) over w_subcat, 1)::numeric as r2_by_subcategory,
                case
                    when nested_sub_category_id is null then null
                    else
                        coalesce(regr_slope(normalized_revenue_by_nested_subcategory, period_num) over w_nest_subcat, 0)::numeric
                end as trend_by_nested_subcategory,
                case
                    when nested_sub_category_id is null then null
                    else
                        coalesce(regr_r2(normalized_revenue_by_nested_subcategory, period_num) over w_nest_subcat, 1)::numeric
                end as r2_by_nested_subcategory,
                students_coeff / sqrt((intervals + 1) - 2 + pow(students_coeff, 2))::numeric as rcrit
            from etl_wrk.trends_normalized
            window
                w_s as (partition by fiverr_seller_id),
                w_g as (partition by gig_id),
                w_cat as (partition by category_id),
                w_subcat as (partition by sub_category_id),
                w_nest_subcat as (partition by nested_sub_category_id)
            order by
                gig_id,
                period_num desc;

        get diagnostics
            row_cnt = row_count;

        create unique index if not exists uidx_trends
            on etl_wrk.trends (gig_id) with (fillfactor = 100);

        cluster etl_wrk.trends using uidx_trends;

        analyze etl_wrk.trends;

        if debug_mode is false then
            drop table if exists etl_wrk.trends_normalized cascade;
        end if;

        select
            high_level_step || '.7' as src_step,
            'etl_wrk' as tgt_schema,
            'trends' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Exiting

        select
            high_level_step || '.8' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - run_start_dttm;

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            start_dttm,
            end_dttm,
            intervals,
            students_coeff,
            ratings_to_volume_coeff,
            overpriced_coeff,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

    end;
$func$;