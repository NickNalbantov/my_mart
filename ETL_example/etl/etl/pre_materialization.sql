create or replace function etl.pre_materialization
(
    cluster_threshold numeric default 0.2, -- threshold value of the ratio between modified / inserted rows and n_live_tup or the ratio of uncorrelated keys of the index that used for clusterization for *pre_materialization* targets'. CLUSTER + ANALYZE commands are being launched if threshold is reached
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '3'
)
returns void
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$
#variable_conflict use_variable

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        run_start_dttm timestamptz;
        total_runtime interval;

        empty_incr_flg boolean;
        incr_start_dttm timestamptz;

        updated_partitions_arr oid[];
        do_cluster_flg boolean;
        partitions_maintenance_rec record;
        stats_columns_list text[];
        maintenance_qry text;
        analyze_root_flg boolean;

    begin

        -- Starting

        run_start_dttm := clock_timestamp();

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Materializing parsed keys for metrics from gigs_revisions JSON

            -- Step 1.1 - Capture of gigs_revisions' id

        task_start_dttm := transaction_timestamp();

        select
            coalesce(max(max_scraped_at), '2000-01-01 00:00:00 UTC'::timestamptz)
        into incr_start_dttm
        from
        (
            select
                max(scraped_at) as max_scraped_at
            from etl.gigs_revisions_pre_metrics

            union all

            select
                max(scraped_at) as max_scraped_at
            from etl.gigs_revisions_pre_metrics_invalid
        ) a;

        select
            false
        into empty_incr_flg
        from public.gigs_revisions_meta
        where
            scraped_at > incr_start_dttm
        limit 1;

        if empty_incr_flg is false then

            drop table if exists etl_wrk.gigs_revisions_id_increment cascade;

            create unlogged table if not exists etl_wrk.gigs_revisions_id_increment
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    g.id
                from public.gigs_revisions_meta g
                where
                    g.fiverr_status in ('approved', 'active')
                    and g.scraped_at > incr_start_dttm;

            get diagnostics
                row_cnt := row_count;

            alter table if exists etl_wrk.gigs_revisions_id_increment
                add primary key (id) with (fillfactor = 100);

            cluster etl_wrk.gigs_revisions_id_increment using gigs_revisions_id_increment_pkey;

            analyze etl_wrk.gigs_revisions_id_increment;

        else

            row_cnt := 0;

        end if;

        select
            high_level_step || '.1.1' as src_step,
            'etl_wrk' as tgt_schema,
            'gigs_revisions_id_increment' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 1.2 - Preparing JSONs

        task_start_dttm := clock_timestamp();

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.tgt_name = 'gigs_revisions_id_increment';

        if empty_incr_flg is false then

            drop table if exists etl_wrk.gigs_revisions_raw_increment;

            create unlogged table if not exists etl_wrk.gigs_revisions_raw_increment
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    g.created_at,
                    g.scraped_at,
                    g.id,
                    g.gig_id,
                    g.schema_version,
                    case
                        when g.schema_version = 1 then r.raw['data']['gig']
                        when g.schema_version in (2, 3, 4) then r.raw
                    end as raw
                from public.gigs_revisions_meta g
                join public.gigs_revisions r
                    on g.id = r.id
                where
                    g.schema_version <= 4 -- setting latest parsed version manually
                    and exists
                    (
                        select
                        from etl_wrk.gigs_revisions_id_increment h
                        where
                            g.id = h.id
                    );

            get diagnostics
                row_cnt := row_count;

            create unique index if not exists uidx_gigs_revisions_raw_increment
                on etl_wrk.gigs_revisions_raw_increment (id, scraped_at desc) with (fillfactor = 100);

            analyze etl_wrk.gigs_revisions_raw_increment;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.gigs_revisions_id_increment cascade;
        end if;

        select
            high_level_step || '.1.2' as src_step,
            'etl_wrk' as tgt_schema,
            'gigs_revisions_raw_increment' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 1.3 - Parsing JSONs

        task_start_dttm := clock_timestamp();

        if empty_incr_flg is false then

            drop table if exists etl_wrk.gigs_revisions_parsed cascade;

            create unlogged table if not exists etl_wrk.gigs_revisions_parsed
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    id,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    gig_is_pro,
                    gig_rating,
                    gig_ratings_count,
                    prices,
                    gig_title,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name,
                    gig_cached_slug,
                    gig_preview_url
                from
                (
                    select
                        a.created_at,
                        case when a.schema_version = 1 then to_timestamp((a.raw ->> 'created_at')::bigint)::timestamptz end as fiverr_created_at,
                        a.scraped_at,
                        row_number() over (partition by a.id order by a.scraped_at desc, coalesce(t1.tp_rn, t2.tp_rn) asc) as image_url_rn,
                        a.id,
                        a.gig_id,
                        nullif(case
                            when a.schema_version = 1 then (a.raw ->> 'id')::int
                            when a.schema_version in (2, 3, 4) then (a.raw ->> 'gig_id')::int
                        end, 0::int) as fiverr_gig_id,
                        nullif((a.raw ->> 'seller_id')::int, 0::int) as fiverr_seller_id,
                        nullif((a.raw ->> 'category_id')::int, 0::int) as category_id,
                        nullif((a.raw ->> 'sub_category_id')::int, 0::int) as sub_category_id,
                        nullif((a.raw ->> 'nested_sub_category_id')::int, 0::int) as nested_sub_category_id,
                        case
                            when a.schema_version = 1 then (a.raw ->> 'ratings_count')::int
                            when a.schema_version in (2, 3, 4) then (a.raw ->> 'buying_review_rating_count')::int
                        end as gig_ratings_count,
                        (a.raw ->> 'is_pro')::boolean as gig_is_pro,
                        case
                            when a.schema_version = 1 then (a.raw ->> 'rating')::numeric
                            when a.schema_version in (2, 3, 4) then (a.raw ->> 'buying_review_rating')::numeric
                        end as gig_rating,
                        case
                            when a.schema_version = 1 then
                                array(select jsonb_array_elements_text(jsonb_path_query_array(a.raw -> 'packages', '$.price'))::numeric)
                            when a.schema_version in (2, 3, 4) then array[(a.raw ->> 'price_i')::numeric]
                        end as prices,
                        a.raw ->> 'title' as gig_title,
                        case when a.schema_version = 1 then a.raw ->> 'category_name' end as category_name,
                        case when a.schema_version = 1 then a.raw ->> 'sub_category_name' end as sub_category_name,
                        case when a.schema_version = 1 then a.raw ->> 'nested_sub_category_name' end as nested_sub_category_name,
                        a.raw ->> 'cached_slug' as gig_cached_slug,
                        coalesce(u1.url, u2.url) as gig_preview_url
                    from etl_wrk.gigs_revisions_raw_increment a
                        left join lateral unnest(array(select jsonb_array_elements(a.raw -> 'media') ->> 'type')) with ordinality t1 (tp, tp_rn)
                            on a.schema_version = 1 and t1.tp = 'image'
                        left join lateral unnest(array(select jsonb_array_elements(a.raw -> 'media') ->> 'preview_url')) with ordinality u1 (url, url_rn)
                            on a.schema_version = 1 and t1.tp_rn = u1.url_rn
                        left join lateral unnest(array(select jsonb_array_elements(a.raw -> 'assets') ->> 'type')) with ordinality t2 (tp, tp_rn)
                            on a.schema_version in (2, 3, 4) and t2.tp = 'ImageAsset'
                        left join lateral unnest(array(select jsonb_array_elements(a.raw -> 'assets') ->> 'cloud_img_main_gig')) with ordinality u2 (url, url_rn)
                            on a.schema_version in (2, 3, 4) and t2.tp_rn = u2.url_rn
                ) b
                where
                    image_url_rn = 1; -- performs better than distinct on for this query

            get diagnostics
                row_cnt := row_count;

            analyze etl_wrk.gigs_revisions_parsed;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.gigs_revisions_raw_increment cascade;
        end if;

        select
            high_level_step || '.1.3' as src_step,
            'etl_wrk' as tgt_schema,
            'gigs_revisions_parsed' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 1.4 - inserting valid data into pre-metrics table

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.1.4' as src_step,
            'etl' as tgt_schema,
            'gigs_revisions_pre_metrics' as tgt_name,
            'insert' as op_type
        into log_rec;

        if empty_incr_flg is false then

            perform public.maintenance_partitions_autovacuum_switch('etl.gigs_revisions_pre_metrics'::regclass, false);

            insert into etl.gigs_revisions_pre_metrics
                (
                    "load_id",
                    processed_dttm,
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    id,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    gig_ratings_count,
                    gig_is_pro,
                    gig_rating,
                    prices,
                    gig_title,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name,
                    gig_cached_slug,
                    gig_preview_url
                )
                select
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    id,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    gig_ratings_count,
                    gig_is_pro,
                    gig_rating,
                    prices,
                    gig_title,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name,
                    gig_cached_slug,
                    gig_preview_url
                from etl_wrk.gigs_revisions_parsed
                    -- Data-quality criteria
                where
                    prices != array[]::numeric[]
                    and array_positions(prices, null) = array[]::int[]
                    and gig_is_pro is not null
                    and gig_cached_slug is not null
                    and gig_preview_url is not null
                    and fiverr_gig_id is not null
                    and fiverr_seller_id is not null
                    and category_id is not null
                    and sub_category_id is not null;

            get diagnostics
                row_cnt := row_count;

            create unique index if not exists uidx_gigs_revisions_pre_metrics_id
                on etl.gigs_revisions_pre_metrics (id, scraped_at desc);
            create index if not exists idx_gigs_revisions_pre_metrics_gig_id
                on etl.gigs_revisions_pre_metrics (gig_id, scraped_at desc);
            create index if not exists idx_gigs_revisions_pre_metrics_fiverr_seller_id
                on etl.gigs_revisions_pre_metrics (fiverr_seller_id, scraped_at desc);
            create index if not exists idx_gigs_revisions_pre_metrics_scraped_at
                on etl.gigs_revisions_pre_metrics (scraped_at desc);
            create index if not exists idx_gigs_revisions_pre_metrics_load_id
                on etl.gigs_revisions_pre_metrics ("load_id" desc, scraped_at desc);

            alter table if exists etl.gigs_revisions_pre_metrics
                replica identity using index uidx_gigs_revisions_pre_metrics_id;

                -- Clustering

            analyze_root_flg := false;

            stats_columns_list := array[
                    'id', 'gig_id', 'fiverr_gig_id', 'fiverr_seller_id',
                    'category_id', 'sub_category_id', 'nested_sub_category_id'
                ];

                -- Finding partitions where new rows were inserted

            with part_key_values as
            (
                select
                    min(scraped_at) as min_part_key_value,
                    max(scraped_at) as max_part_key_value
                from etl_wrk.gigs_revisions_parsed
            )

            select
                array_agg(b.table_oid)
            into updated_partitions_arr
            from
            (
                select
                    a.table_oid,
                    case
                        when a.relpartbound = 'DEFAULT' then '2000-01-01 00:00:00 UTC'::timestamptz
                        when regexp_substr(a.relpartbound, '(?<=FROM \()(.*)(?=\) TO)') = 'MINVALUE' then '2000-01-01 00:00:00 UTC'::timestamptz
                        else regexp_substr(a.relpartbound, '(?<=FROM \('')(.*)(?=''\) TO)')::timestamptz
                    end as value_from,
                    case
                        when a.relpartbound = 'DEFAULT' then '2099-01-01 00:00:00 UTC'::timestamptz
                        when regexp_substr(a.relpartbound, '(?<=TO \()(.*)(?=\))') = 'MAXVALUE' then '2099-01-01 00:00:00 UTC'::timestamptz
                        else regexp_substr(a.relpartbound, '(?<=TO \('')(.*)(?=''\))')::timestamptz
                    end as value_to
                from
                (
                    select
                        t.table_oid,
                        pg_catalog.pg_get_expr(c.relpartbound, t.table_oid, true) as relpartbound
                    from public.get_partitions_tree
                        (
                            (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass
                        ) t
                    join pg_catalog.pg_class c
                        on t.table_oid = c."oid"
                    where
                        t.relkind in ('r', 'f')
                ) a
            ) b
            cross join part_key_values p
            where -- overlapping ranges
                tstzrange(p.min_part_key_value, p.max_part_key_value) && tstzrange(b.value_from, b.value_to);

                -- Clustering loop

            for partitions_maintenance_rec in
                select
                    quote_ident(t.schema_name) as partition_schema,
                    quote_ident(t.table_name) as partition_name,
                    quote_ident(ci.relname) as cluster_index_name,
                    (
                        coalesce
                            (
                                greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric,
                                0
                            )::numeric >= cluster_threshold
                        or 1::numeric - coalesce(abs(st.correlation), 0)::numeric >= cluster_threshold
                    ) as do_cluster_flg_partition
                from public.get_partitions_tree
                    (
                        (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass
                    ) t
                join pg_catalog.pg_attribute a
                    on a.attrelid = t.table_oid
                join pg_catalog.pg_index ix
                    on ix.indrelid = a.attrelid
                    and ix.indkey[0] = a.attnum
                join pg_catalog.pg_class ci
                    on ix.indexrelid = ci."oid"
                left join pg_catalog.pg_stat_user_tables s
                    on t.table_oid = s.relid
                left join pg_catalog.pg_stats st
                    on a.attrelid = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                    and a.attname = st.attname
                where
                    t.table_oid = any(updated_partitions_arr) -- clustering only paritions with new rows
                    and a.attname = 'scraped_at' -- clustering by scraped_at
                    and cardinality(ix.indkey) = 1
            loop

                if partitions_maintenance_rec.do_cluster_flg_partition is true then

                    -- CLUSTER + CREATE STATISTICS
                    maintenance_qry := 'cluster '
                        || partitions_maintenance_rec.partition_schema || '.' || partitions_maintenance_rec.partition_name
                        || ' using ' || partitions_maintenance_rec.cluster_index_name || ';' || repeat(E'\n', 2)

                        || 'create statistics if not exists ' || partitions_maintenance_rec.partition_schema || '.'
                        || substring('stats_' || partitions_maintenance_rec.partition_name, 1, 63) || E'\n'
                        || 'on ' || array_to_string(stats_columns_list, ', ') || E'\n'
                        || 'from ' || partitions_maintenance_rec.partition_schema || '.' || partitions_maintenance_rec.partition_name || ';';

                    execute maintenance_qry;

                    analyze_root_flg := true;

                end if;

            end loop;

            if analyze_root_flg is true then

                analyze etl.gigs_revisions_pre_metrics;

            end if;

        else

            row_cnt := 0;

        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 1.5 - inserting invalid data into pre-metrics table

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.1.5' as src_step,
            'etl' as tgt_schema,
            'gigs_revisions_pre_metrics_invalid' as tgt_name,
            'insert' as op_type
        into log_rec;

        if empty_incr_flg is false then

            alter table if exists etl.gigs_revisions_pre_metrics_invalid
                set
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                );

            insert into etl.gigs_revisions_pre_metrics_invalid
                (
                    "load_id",
                    processed_dttm,
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    id,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    gig_ratings_count,
                    gig_is_pro,
                    gig_rating,
                    prices,
                    gig_title,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name,
                    gig_cached_slug,
                    gig_preview_url
                )
                select
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    id,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    gig_ratings_count,
                    gig_is_pro,
                    gig_rating,
                    prices,
                    gig_title,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name,
                    gig_cached_slug,
                    gig_preview_url
                from etl_wrk.gigs_revisions_parsed
                    -- Data-quality criteria
                where
                    prices = array[]::numeric[]
                    or array_positions(prices, null) != array[]::int[]
                    or gig_is_pro is null
                    or gig_cached_slug is null
                    or gig_preview_url is null
                    or fiverr_gig_id is null
                    or fiverr_seller_id is null
                    or category_id is null
                    or sub_category_id is null;

            get diagnostics
                row_cnt := row_count;

            create unique index if not exists uidx_gigs_revisions_pre_metrics_invalid_id
                on etl.gigs_revisions_pre_metrics_invalid (id);
            create index if not exists idx_gigs_revisions_pre_metrics_invalid
                on etl.gigs_revisions_pre_metrics_invalid (gig_id, scraped_at desc);

            alter table if exists etl.gigs_revisions_pre_metrics_invalid
                replica identity using index uidx_gigs_revisions_pre_metrics_invalid_id;

                -- Clustering

            select
                coalesce
                (
                    (
                        coalesce(greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric, 0)::numeric >= cluster_threshold
                        or 1 - abs(st.correlation)::numeric >= cluster_threshold
                    ),
                    false
                )
            into do_cluster_flg
            from pg_catalog.pg_class c
            left join pg_catalog.pg_stat_user_tables s
                on c."oid" = s.relid
            left join pg_catalog.pg_stats st
                on c."oid" = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                and st.attname = 'id' -- clustering by id
            where
                c."oid" = (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass;

            if do_cluster_flg is true then

                cluster etl.gigs_revisions_pre_metrics_invalid using uidx_gigs_revisions_pre_metrics_invalid_id;

                analyze etl.gigs_revisions_pre_metrics_invalid;

            end if;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.gigs_revisions_parsed cascade;
        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 2 - Materializing parsed keys for metrics from gigs_pages JSON

            -- Step 2.1 - Capture of gigs_pages' id

        task_start_dttm := clock_timestamp();

        select
            coalesce(max(max_scraped_at), '2000-01-01 00:00:00 UTC'::timestamptz)
        into incr_start_dttm
        from
        (
            select
                max(scraped_at) as max_scraped_at
            from etl.gigs_pages_pre_metrics

            union all

            select
                max(scraped_at) as max_scraped_at
            from etl.gigs_pages_pre_metrics_invalid
        ) a;

        select
            false
        into empty_incr_flg
        from public.gigs_pages_meta
        where
            scraped_at > incr_start_dttm
        limit 1;

        if empty_incr_flg is false then

            drop table if exists etl_wrk.gigs_pages_id_increment cascade;

            create unlogged table if not exists etl_wrk.gigs_pages_id_increment
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    g.id
                from public.gigs_pages_meta g
                where
                    g.scraped_at > incr_start_dttm;

            get diagnostics
                row_cnt := row_count;

            alter table if exists etl_wrk.gigs_pages_id_increment
                add primary key (id) with (fillfactor = 100);

            cluster etl_wrk.gigs_pages_id_increment using gigs_pages_id_increment_pkey;

            analyze etl_wrk.gigs_pages_id_increment;

        else

            row_cnt := 0;

        end if;

        select
            high_level_step || '.2.1' as src_step,
            'etl_wrk' as tgt_schema,
            'gigs_pages_id_increment' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 2.2 - Preparing JSONs

        task_start_dttm := clock_timestamp();

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.tgt_name = 'gigs_pages_id_increment';

        if empty_incr_flg is false then

            drop table if exists etl_wrk.gigs_pages_raw_increment cascade;

            create unlogged table if not exists etl_wrk.gigs_pages_raw_increment
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    g.created_at,
                    g.scraped_at,
                    g.id,
                    g.gig_id,
                    g.schema_version,
                    case
                        when g.schema_version = 4 then r.raw
                    end as raw
            from public.gigs_pages_meta g
            join public.gigs_pages r
                on g.id = r.id
            where
                g.schema_version <= 4 -- setting latest parsed version manually
                and exists
                (
                    select
                    from etl_wrk.gigs_pages_id_increment h
                    where
                        h.id = g.id
                );

            get diagnostics
                row_cnt := row_count;

            analyze etl_wrk.gigs_pages_raw_increment;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.gigs_pages_id_increment cascade;
        end if;

        select
            high_level_step || '.2.2' as src_step,
            'etl_wrk' as tgt_schema,
            'gigs_pages_raw_increment' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 2.3 - Parsing JSONs

        task_start_dttm := clock_timestamp();

        if empty_incr_flg is false then

            drop table if exists etl_wrk.gigs_pages_parsed cascade;

            create unlogged table if not exists etl_wrk.gigs_pages_parsed
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    created_at,
                    scraped_at,
                    id,
                    gig_id,
                    nullif(case
                        when schema_version = 4 then (raw -> 'general' ->> 'gigId')::int
                    end, 0::int) as fiverr_gig_id,
                    nullif(case
                        when schema_version = 4 then (raw -> 'general' ->> 'sellerId')::int
                    end, 0::int) as fiverr_seller_id,
                    case
                        when schema_version = 4 then (raw['seller']['completedOrdersCount'])::int
                    end as seller_completed_orders_count,
                    case
                        when schema_version = 4 then
                            array(select jsonb_array_elements_text(jsonb_path_query_array(raw['packages']['packageList'], '$.price'))::numeric / 100::numeric)
                    end as prices
                from etl_wrk.gigs_pages_raw_increment;

            get diagnostics
                row_cnt := row_count;

            analyze etl_wrk.gigs_pages_parsed;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.gigs_pages_raw_increment cascade;
        end if;

        select
            high_level_step || '.2.3' as src_step,
            'etl_wrk' as tgt_schema,
            'gigs_pages_parsed' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 2.4 - inserting valid data into pre-metrics table

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.2.4' as src_step,
            'etl' as tgt_schema,
            'gigs_pages_pre_metrics' as tgt_name,
            'insert' as op_type
        into log_rec;

        if empty_incr_flg is false then

            perform public.maintenance_partitions_autovacuum_switch('etl.gigs_pages_pre_metrics'::regclass, false);

            insert into etl.gigs_pages_pre_metrics
                (
                    "load_id",
                    processed_dttm,
                    created_at,
                    scraped_at,
                    id,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    seller_completed_orders_count,
                    prices
                )
                select
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    created_at,
                    scraped_at,
                    id,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    seller_completed_orders_count,
                    prices
                from etl_wrk.gigs_pages_parsed
                    -- Data-quality criteria
                where
                    prices != array[]::numeric[]
                    and seller_completed_orders_count is not null
                    and fiverr_gig_id is not null
                    and fiverr_seller_id is not null;

            get diagnostics
                row_cnt := row_count;

            create unique index if not exists uidx_gigs_pages_pre_metrics_id
                on etl.gigs_pages_pre_metrics using btree (id, scraped_at desc);
            create index if not exists idx_gigs_pages_pre_metrics_gig_id
                on etl.gigs_pages_pre_metrics using btree (gig_id, scraped_at desc);
            create index if not exists idx_gigs_pages_pre_metrics_fiverr_seller_id
                on etl.gigs_pages_pre_metrics using btree (fiverr_seller_id, scraped_at desc);
            create index if not exists idx_gigs_pages_pre_metrics_scraped_at
                on etl.gigs_pages_pre_metrics (scraped_at desc);
            create index if not exists idx_gigs_pages_pre_metrics_load_id
                on etl.gigs_pages_pre_metrics using btree ("load_id" desc, scraped_at desc);

            alter table if exists etl.gigs_pages_pre_metrics
                replica identity using index uidx_gigs_pages_pre_metrics_id;

                -- Clustering

            analyze_root_flg := false;

            stats_columns_list := array[
                    'id', 'gig_id', 'fiverr_gig_id', 'fiverr_seller_id'
                ];

                -- Finding partitions where new rows were inserted

            with part_key_values as
            (
                select
                    min(scraped_at) as min_part_key_value,
                    max(scraped_at) as max_part_key_value
                from etl_wrk.gigs_pages_parsed
            )

            select
                array_agg(b.table_oid)
            into updated_partitions_arr
            from
            (
                select
                    a.table_oid,
                    case
                        when a.relpartbound = 'DEFAULT' then '2000-01-01 00:00:00 UTC'::timestamptz
                        when regexp_substr(a.relpartbound, '(?<=FROM \()(.*)(?=\) TO)') = 'MINVALUE' then '2000-01-01 00:00:00 UTC'::timestamptz
                        else regexp_substr(a.relpartbound, '(?<=FROM \('')(.*)(?=''\) TO)')::timestamptz
                    end as value_from,
                    case
                        when a.relpartbound = 'DEFAULT' then '2099-01-01 00:00:00 UTC'::timestamptz
                        when regexp_substr(a.relpartbound, '(?<=TO \()(.*)(?=\))') = 'MAXVALUE' then '2099-01-01 00:00:00 UTC'::timestamptz
                        else regexp_substr(a.relpartbound, '(?<=TO \('')(.*)(?=''\))')::timestamptz
                    end as value_to
                from
                (
                    select
                        t.table_oid,
                        pg_catalog.pg_get_expr(c.relpartbound, t.table_oid, true) as relpartbound
                    from public.get_partitions_tree
                        (
                            (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass
                        ) t
                    join pg_catalog.pg_class c
                        on t.table_oid = c."oid"
                    where
                        t.relkind in ('r', 'f')
                ) a
            ) b
            cross join part_key_values p
            where -- overlapping ranges
                tstzrange(p.min_part_key_value, p.max_part_key_value) && tstzrange(b.value_from, b.value_to);

                -- Clustering loop

            for partitions_maintenance_rec in
                select
                    quote_ident(t.schema_name) as partition_schema,
                    quote_ident(t.table_name) as partition_name,
                    quote_ident(ci.relname) as cluster_index_name,
                    (
                        coalesce
                            (
                                greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric,
                                0
                            )::numeric >= cluster_threshold
                        or 1::numeric - coalesce(abs(st.correlation), 0)::numeric >= cluster_threshold
                    ) as do_cluster_flg_partition
                from public.get_partitions_tree
                    (
                        (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass
                    ) t
                join pg_catalog.pg_attribute a
                    on a.attrelid = t.table_oid
                join pg_catalog.pg_index ix
                    on ix.indrelid = a.attrelid
                    and ix.indkey[0] = a.attnum
                join pg_catalog.pg_class ci
                    on ix.indexrelid = ci."oid"
                left join pg_catalog.pg_stat_user_tables s
                    on t.table_oid = s.relid
                left join pg_catalog.pg_stats st
                    on a.attrelid = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                    and a.attname = st.attname
                where
                    t.table_oid = any(updated_partitions_arr) -- clustering only paritions with new rows
                    and a.attname = 'scraped_at' -- clustering by scraped_at
                    and cardinality(ix.indkey) = 1
            loop

                if partitions_maintenance_rec.do_cluster_flg_partition is true then

                    -- CLUSTER + CREATE STATISTICS
                    maintenance_qry := 'cluster '
                        || partitions_maintenance_rec.partition_schema || '.' || partitions_maintenance_rec.partition_name
                        || ' using ' || partitions_maintenance_rec.cluster_index_name || ';' || repeat(E'\n', 2)

                        || 'create statistics if not exists ' || partitions_maintenance_rec.partition_schema || '.'
                        || substring('stats_' || partitions_maintenance_rec.partition_name, 1, 63) || E'\n'
                        || 'on ' || array_to_string(stats_columns_list, ', ') || E'\n'
                        || 'from ' || partitions_maintenance_rec.partition_schema || '.' || partitions_maintenance_rec.partition_name || ';';

                    execute maintenance_qry;

                    analyze_root_flg := true;

                end if;

            end loop;

            if analyze_root_flg is true then

                analyze etl.gigs_pages_pre_metrics;

            end if;

        else

            row_cnt := 0;

        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 2.5 - inserting invalid data into pre-metrics table

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.2.5' as src_step,
            'etl' as tgt_schema,
            'gigs_pages_pre_metrics_invalid' as tgt_name,
            'insert' as op_type
        into log_rec;

        if empty_incr_flg is false then

            alter table if exists etl.gigs_pages_pre_metrics_invalid
                set
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                );

            insert into etl.gigs_pages_pre_metrics_invalid
                (
                    "load_id",
                    processed_dttm,
                    created_at,
                    scraped_at,
                    id,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    seller_completed_orders_count,
                    prices
                )
                select
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    created_at,
                    scraped_at,
                    id,
                    gig_id,
                    fiverr_gig_id,
                    fiverr_seller_id,
                    seller_completed_orders_count,
                    prices
                from etl_wrk.gigs_pages_parsed
                    -- Data-quality criteria
                where
                    prices = array[]::numeric[]
                    or seller_completed_orders_count is null
                    or fiverr_gig_id is null
                    or fiverr_seller_id is null;

            get diagnostics
                row_cnt := row_count;

            create unique index if not exists uidx_gigs_pages_pre_metrics_invalid_id
                on etl.gigs_pages_pre_metrics_invalid (id);
            create index if not exists idx_gigs_pages_pre_metrics_invalid
                on etl.gigs_pages_pre_metrics_invalid (gig_id, scraped_at desc);

            alter table if exists etl.gigs_pages_pre_metrics_invalid
                replica identity using index uidx_gigs_pages_pre_metrics_invalid_id;

                -- Clustering

            select
                coalesce
                (
                    (
                        coalesce(greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric, 0)::numeric >= cluster_threshold
                        or 1 - abs(st.correlation)::numeric >= cluster_threshold
                    ),
                    false
                )
            into do_cluster_flg
            from pg_catalog.pg_class c
            left join pg_catalog.pg_stat_user_tables s
                on c."oid" = s.relid
            left join pg_catalog.pg_stats st
                on c."oid" = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                and st.attname = 'id' -- clustering by id
            where
                c."oid" = (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass;

            if do_cluster_flg is true then

                cluster etl.gigs_pages_pre_metrics_invalid using uidx_gigs_pages_pre_metrics_invalid_id;

                analyze etl.gigs_pages_pre_metrics_invalid;

            end if;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.gigs_pages_parsed cascade;
        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 3 - Materializing parsed keys for metrics from sellers_revisions JSON

            -- Step 3.1 - Capture of sellers_revisions' id

        task_start_dttm := clock_timestamp();

        select
            coalesce(max(max_scraped_at), '2000-01-01 00:00:00 UTC'::timestamptz)
        into incr_start_dttm
        from
        (
            select
                max(scraped_at) as max_scraped_at
            from etl.sellers_revisions_pre_metrics

            union all

            select
                max(scraped_at) as max_scraped_at
            from etl.sellers_revisions_pre_metrics_invalid
        ) a;

        select
            false
        into empty_incr_flg
        from public.sellers_revisions_meta
        where
            scraped_at > incr_start_dttm
        limit 1;

        if empty_incr_flg is false then

            drop table if exists etl_wrk.sellers_revisions_id_increment cascade;

            create unlogged table if not exists etl_wrk.sellers_revisions_id_increment
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    s.id
                from public.sellers_revisions_meta s
                where
                    s.scraped_at > incr_start_dttm;

            get diagnostics
                row_cnt := row_count;

            alter table if exists etl_wrk.sellers_revisions_id_increment
                add primary key (id) with (fillfactor = 100);

            cluster etl_wrk.sellers_revisions_id_increment using sellers_revisions_id_increment_pkey;

            analyze etl_wrk.sellers_revisions_id_increment;

        else

            row_cnt := 0;

        end if;

        select
            high_level_step || '.3.1' as src_step,
            'etl_wrk' as tgt_schema,
            'sellers_revisions_id_increment' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 3.2 - Preparing JSONs

        task_start_dttm := clock_timestamp();

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.tgt_name = 'sellers_revisions_id_increment';

        if empty_incr_flg is false then

            drop table if exists etl_wrk.sellers_revisions_raw_increment cascade;

            create unlogged table if not exists etl_wrk.sellers_revisions_raw_increment
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    s.created_at,
                    s.scraped_at,
                    s.id,
                    s.seller_id,
                    s.schema_version,
                    case
                        when s.schema_version = 1 then r.raw -> 'data'
                        when s.schema_version = 2 then r.raw
                        when s.schema_version in (3, 4) then r.raw -> 'seller'
                    end as raw
            from public.sellers_revisions_meta s
            join public.sellers_revisions r
                on s.id = r.id
            where
                s.schema_version <= 4 -- setting latest parsed version manually
                and exists
                (
                    select
                    from etl_wrk.sellers_revisions_id_increment h
                    where
                        h.id = s.id
                );

            get diagnostics
                row_cnt := row_count;

            analyze etl_wrk.sellers_revisions_raw_increment;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.sellers_revisions_id_increment cascade;
        end if;

        select
            high_level_step || '.3.2' as src_step,
            'etl_wrk' as tgt_schema,
            'sellers_revisions_raw_increment' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 3.3 - Parsing JSONs

        task_start_dttm := clock_timestamp();

        if empty_incr_flg is false then

            drop table if exists etl_wrk.sellers_revisions_parsed cascade;

            create unlogged table if not exists etl_wrk.sellers_revisions_parsed
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    a.created_at,
                    case
                        when a.schema_version = 1 then to_timestamp((a.raw ->> 'created_at')::bigint)::timestamptz
                        when a.schema_version in (3, 4) then to_timestamp((a.raw ->> 'joined_at')::bigint)::timestamptz
                    end as fiverr_created_at,
                    a.scraped_at,
                    a.id,
                    a.seller_id,
                    nullif(case
                        when a.schema_version = 1 then (a.raw ->> 'id')::int
                        when a.schema_version = 2 then (a.raw ->> 'seller_id')::int
                        when a.schema_version in (3, 4) then (a.raw -> 'user' ->> 'id')::int
                    end, 0::int) as fiverr_seller_id,
                    case
                        when a.schema_version = 1 then (a.raw ->> 'ratings_count')::int
                        when a.schema_version in (3, 4) then (a.raw -> 'rating' ->> 'count')::int
                    end as seller_ratings_count,
                    case
                        when a.schema_version in (1, 2) then (a.raw ->> 'is_pro')::boolean
                        when a.schema_version in (3, 4) then (a.raw ->> 'isPro')::boolean
                    end as seller_is_pro,
                    case
                        when a.schema_version = 1 then (a.raw ->> 'rating')::numeric
                        when a.schema_version in (3, 4) then (a.raw -> 'rating' ->> 'score')::numeric
                    end as seller_rating,
                    coalesce
                        (
                            case
                                when a.schema_version = 1 then
                                    (
                                        case
                                            when (a.raw ->> 'level')::int = 0 then 'no_level'
                                            when (a.raw ->> 'level')::int = 1 then 'level_one'
                                            when (a.raw ->> 'level')::int = 2 then 'level_two'
                                            when (a.raw ->> 'level')::int = 5 then 'level_trs'
                                            else null
                                        end
                                    )
                                when a.schema_version = 2 then
                                    (
                                        case
                                            when lower(a.raw ->> 'seller_level') = 'no_level' then 'no_level'
                                            when lower(a.raw ->> 'seller_level') = 'level_one_seller' then 'level_one'
                                            when lower(a.raw ->> 'seller_level') = 'level_two_seller' then 'level_two'
                                            when lower(a.raw ->> 'seller_level') = 'top_rated_seller' then 'level_trs'
                                            else null
                                        end
                                    )
                                when a.schema_version in (3, 4)
                                    then lower(a.raw ->> 'sellerLevel')
                                else null
                            end,
                            'new_seller'
                        )::text as seller_level,
                    case
                        when a.schema_version = 1 then a.raw ->> 'username'
                        when a.schema_version = 2 then a.raw ->> 'seller_name'
                        when a.schema_version in (3, 4) then a.raw -> 'user' ->> 'name'
                    end as seller_name,
                    case
                        when a.schema_version in (3, 4) then a.raw -> 'agency' ->> 'slug'
                        else null::text
                    end as agency_slug,
                    case
                        when a.schema_version in (3, 4) then lower(a.raw -> 'agency' ->> 'status')
                        else null::text
                    end as agency_status,
                    case
                        when a.schema_version = 1 then a.raw ->> 'profile_image'
                        when a.schema_version = 2 then a.raw ->> 'seller_img'
                        when a.schema_version in (3, 4) then a.raw -> 'user' ->> 'profileImageUrl'
                    end as seller_profile_image,
                    case
                        when a.schema_version = 1 then a.raw -> 'address' ->> 'country'
                        when a.schema_version in (3, 4) then a.raw['user']['address'] ->> 'countryName'
                    end as seller_country,
                    case
                        when a.schema_version = 1 then a.raw -> 'address' ->> 'country_code'
                        when a.schema_version = 2 then a.raw ->> 'seller_country'
                        when a.schema_version in (3, 4) then a.raw['user']['address'] ->> 'countryCode'
                    end as seller_country_code,
                    case
                        when a.schema_version = 1 then array(select lower(jsonb_array_elements_text(jsonb_path_query_array(a.raw -> 'languages', '$.code'))))
                        when a.schema_version = 2 then array(select lower(jsonb_array_elements_text(jsonb_path_query_array(a.raw -> 'seller_languages', '$.code'))))
                        when a.schema_version in (3, 4) then array(select lower(jsonb_array_elements_text(jsonb_path_query_array(a.raw['user']['languages'], '$.code'))))
                    end as seller_languages
                from etl_wrk.sellers_revisions_raw_increment a;

            get diagnostics
                row_cnt := row_count;

            analyze etl_wrk.sellers_revisions_parsed;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.sellers_revisions_raw_increment cascade;
        end if;

        select
            high_level_step || '.3.3' as src_step,
            'etl_wrk' as tgt_schema,
            'sellers_revisions_parsed' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 3.4 - inserting valid data into pre-metrics table

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.3.4' as src_step,
            'etl' as tgt_schema,
            'sellers_revisions_pre_metrics' as tgt_name,
            'insert' as op_type
        into log_rec;

        if empty_incr_flg is false then

            perform public.maintenance_partitions_autovacuum_switch('etl.sellers_revisions_pre_metrics'::regclass, false);

            insert into etl.sellers_revisions_pre_metrics
                (
                    "load_id",
                    processed_dttm,
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    id,
                    seller_id,
                    fiverr_seller_id,
                    seller_ratings_count,
                    seller_level,
                    seller_is_pro,
                    seller_rating,
                    seller_name,
                    agency_slug,
                    agency_status,
                    seller_profile_image,
                    seller_country,
                    seller_country_code,
                    seller_languages
                )
                select
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    id,
                    seller_id,
                    fiverr_seller_id,
                    seller_ratings_count,
                    seller_level,
                    seller_is_pro,
                    seller_rating,
                    seller_name,
                    agency_slug,
                    agency_status,
                    seller_profile_image,
                    seller_country,
                    seller_country_code,
                    seller_languages
                from etl_wrk.sellers_revisions_parsed
                    -- Data-quality criteria
                where
                    seller_name is not null
                    and fiverr_seller_id is not null
                    and seller_is_pro is not null;

            get diagnostics
                row_cnt := row_count;

            create unique index if not exists uidx_sellers_revisions_pre_metrics_id
                on etl.sellers_revisions_pre_metrics (id, scraped_at desc);
            create index if not exists idx_sellers_revisions_pre_metrics_seller_id
                on etl.sellers_revisions_pre_metrics (seller_id, scraped_at desc);
            create index if not exists idx_sellers_revisions_pre_metrics_fiverr_seller_id
                on etl.sellers_revisions_pre_metrics (fiverr_seller_id, scraped_at desc);
            create index if not exists idx_sellers_revisions_pre_metrics_scraped_at
                on etl.sellers_revisions_pre_metrics (scraped_at desc);
            create index if not exists idx_sellers_revisions_pre_metrics_load_id
                on etl.sellers_revisions_pre_metrics ("load_id" desc, scraped_at desc);

            alter table if exists etl.sellers_revisions_pre_metrics
                replica identity using index uidx_sellers_revisions_pre_metrics_id;

                -- Clustering

            analyze_root_flg := false;

            stats_columns_list := array[
                    'id', 'seller_id', 'fiverr_seller_id'
                ];

                -- Finding partitions where new rows were inserted

            with part_key_values as
            (
                select
                    min(scraped_at) as min_part_key_value,
                    max(scraped_at) as max_part_key_value
                from etl_wrk.sellers_revisions_parsed
            )

            select
                array_agg(b.table_oid)
            into updated_partitions_arr
            from
            (
                select
                    a.table_oid,
                    case
                        when a.relpartbound = 'DEFAULT' then '2000-01-01 00:00:00 UTC'::timestamptz
                        when regexp_substr(a.relpartbound, '(?<=FROM \()(.*)(?=\) TO)') = 'MINVALUE' then '2000-01-01 00:00:00 UTC'::timestamptz
                        else regexp_substr(a.relpartbound, '(?<=FROM \('')(.*)(?=''\) TO)')::timestamptz
                    end as value_from,
                    case
                        when a.relpartbound = 'DEFAULT' then '2099-01-01 00:00:00 UTC'::timestamptz
                        when regexp_substr(a.relpartbound, '(?<=TO \()(.*)(?=\))') = 'MAXVALUE' then '2099-01-01 00:00:00 UTC'::timestamptz
                        else regexp_substr(a.relpartbound, '(?<=TO \('')(.*)(?=''\))')::timestamptz
                    end as value_to
                from
                (
                    select
                        t.table_oid,
                        pg_catalog.pg_get_expr(c.relpartbound, t.table_oid, true) as relpartbound
                    from public.get_partitions_tree
                        (
                            (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass
                        ) t
                    join pg_catalog.pg_class c
                        on t.table_oid = c."oid"
                    where
                        t.relkind in ('r', 'f')
                ) a
            ) b
            cross join part_key_values p
            where -- overlapping ranges
                tstzrange(p.min_part_key_value, p.max_part_key_value) && tstzrange(b.value_from, b.value_to);

                -- Clustering loop

            for partitions_maintenance_rec in
                select
                    quote_ident(t.schema_name) as partition_schema,
                    quote_ident(t.table_name) as partition_name,
                    quote_ident(ci.relname) as cluster_index_name,
                    (
                        coalesce
                            (
                                greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric,
                                0
                            )::numeric >= cluster_threshold
                        or 1::numeric - coalesce(abs(st.correlation), 0)::numeric >= cluster_threshold
                    ) as do_cluster_flg_partition
                from public.get_partitions_tree
                    (
                        (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass
                    ) t
                join pg_catalog.pg_attribute a
                    on a.attrelid = t.table_oid
                join pg_catalog.pg_index ix
                    on ix.indrelid = a.attrelid
                    and ix.indkey[0] = a.attnum
                join pg_catalog.pg_class ci
                    on ix.indexrelid = ci."oid"
                left join pg_catalog.pg_stat_user_tables s
                    on t.table_oid = s.relid
                left join pg_catalog.pg_stats st
                    on a.attrelid = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                    and a.attname = st.attname
                where
                    t.table_oid = any(updated_partitions_arr) -- clustering only paritions with new rows
                    and a.attname = 'scraped_at' -- clustering by scraped_at
                    and cardinality(ix.indkey) = 1
            loop

                if partitions_maintenance_rec.do_cluster_flg_partition is true then

                    -- CLUSTER + CREATE STATISTICS
                    maintenance_qry := 'cluster '
                        || partitions_maintenance_rec.partition_schema || '.' || partitions_maintenance_rec.partition_name
                        || ' using ' || partitions_maintenance_rec.cluster_index_name || ';' || repeat(E'\n', 2)

                        || 'create statistics if not exists ' || partitions_maintenance_rec.partition_schema || '.'
                        || substring('stats_' || partitions_maintenance_rec.partition_name, 1, 63) || E'\n'
                        || 'on ' || array_to_string(stats_columns_list, ', ') || E'\n'
                        || 'from ' || partitions_maintenance_rec.partition_schema || '.' || partitions_maintenance_rec.partition_name || ';';

                    execute maintenance_qry;

                    analyze_root_flg := true;

                end if;

            end loop;

            if analyze_root_flg is true then

                analyze etl.sellers_revisions_pre_metrics;

            end if;

        else

            row_cnt := 0;

        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 3.5 - inserting invalid data into pre-metrics table

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.3.5' as src_step,
            'etl' as tgt_schema,
            'sellers_revisions_pre_metrics_invalid' as tgt_name,
            'insert' as op_type
        into log_rec;

        if empty_incr_flg is false then

            alter table if exists etl.sellers_revisions_pre_metrics_invalid
                set
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                );

            insert into etl.sellers_revisions_pre_metrics_invalid
                (
                    "load_id",
                    processed_dttm,
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    id,
                    seller_id,
                    fiverr_seller_id,
                    seller_ratings_count,
                    seller_level,
                    seller_is_pro,
                    seller_rating,
                    seller_name,
                    agency_slug,
                    agency_status,
                    seller_profile_image,
                    seller_country,
                    seller_country_code,
                    seller_languages
                )
                select
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    id,
                    seller_id,
                    fiverr_seller_id,
                    seller_ratings_count,
                    seller_level,
                    seller_is_pro,
                    seller_rating,
                    seller_name,
                    agency_slug,
                    agency_status,
                    seller_profile_image,
                    seller_country,
                    seller_country_code,
                    seller_languages
                from etl_wrk.sellers_revisions_parsed
                    -- Data-quality criteria
                where
                    seller_name is null
                    or fiverr_seller_id is null
                    or seller_is_pro is null;

            get diagnostics
                row_cnt := row_count;

            create unique index if not exists uidx_sellers_revisions_pre_metrics_invalid_id
                on etl.sellers_revisions_pre_metrics_invalid (id);
            create index if not exists idx_sellers_revisions_pre_metrics_invalid
                on etl.sellers_revisions_pre_metrics_invalid (seller_id, scraped_at desc);

            alter table if exists etl.sellers_revisions_pre_metrics_invalid
                replica identity using index uidx_sellers_revisions_pre_metrics_invalid_id;

                -- Clustering

            select
                coalesce
                (
                    (
                        coalesce(greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric, 0)::numeric >= cluster_threshold
                        or 1 - abs(st.correlation)::numeric >= cluster_threshold
                    ),
                    false
                )
            into do_cluster_flg
            from pg_catalog.pg_class c
            left join pg_catalog.pg_stat_user_tables s
                on c."oid" = s.relid
            left join pg_catalog.pg_stats st
                on c."oid" = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                and st.attname = 'id' -- clustering by id
            where
                c."oid" = (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass;

            if do_cluster_flg is true then

                cluster etl.sellers_revisions_pre_metrics_invalid using uidx_sellers_revisions_pre_metrics_invalid_id;

                analyze etl.sellers_revisions_pre_metrics_invalid;

            end if;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.sellers_revisions_parsed cascade;
        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 4 - Gig status SCD6-history

            -- Step 4.1 - Tracking fiverr_status changes and inserting new history

        task_start_dttm := clock_timestamp();

        select
            coalesce(max(valid_from_dttm), '2000-01-01 00:00:00 UTC'::timestamptz)
        into incr_start_dttm
        from etl.gig_status_history;

        select
            false
        into empty_incr_flg
        from public.gigs_revisions_meta
        where
            scraped_at > incr_start_dttm
        limit 1;

                -- merging data from new revisions

        if empty_incr_flg is false then

            alter table if exists etl.gig_status_history
                set
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                );

            drop index if exists etl.uidx_gig_status_history_valid_to cascade; -- this index blocks merge, has to be recreated

            with new_revs as
            (
                select
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    scraped_at as valid_from_dttm,
                    coalesce(
                                lead(scraped_at) over (partition by gig_id order by scraped_at) - '1 us'::interval,
                                '2999-12-31 00:00:00 UTC'::timestamptz
                            ) as valid_to_dttm,
                    gig_id,
                    fiverr_status
                from
                (
                    select
                        scraped_at,
                        gig_id,
                        case
                            when lag(fiverr_status, 1, 'empty') over (partition by gig_id order by scraped_at) != coalesce(fiverr_status, 'empty')
                                then true
                            else false
                        end as is_group_start,
                        fiverr_status
                    from
                    (
                        select
                            scraped_at,
                            gig_id,
                            fiverr_status
                        from public.gigs_revisions_meta
                        where
                            scraped_at > incr_start_dttm

                        union all

                        select
                            valid_from_dttm as scraped_at,
                            gig_id,
                            fiverr_status
                        from etl.gig_status_history
                        where
                            valid_to_dttm = '2999-12-31 00:00:00 UTC'::timestamptz -- current slice
                    ) u
                ) a
                where
                    is_group_start is true -- history rollup
            )

            merge into etl.gig_status_history as g
            using new_revs n
                on n.gig_id = g.gig_id
                and n.valid_from_dttm = g.valid_from_dttm
            when matched then
                do nothing -- to avoid inserting current slice again
            when not matched then
                insert
                    (
                        "load_id",
                        processed_dttm,
                        valid_from_dttm,
                        valid_to_dttm,
                        gig_id,
                        fiverr_status
                    )
                values
                    (
                        n."load_id",
                        n.processed_dttm,
                        n.valid_from_dttm,
                        n.valid_to_dttm,
                        n.gig_id,
                        n.fiverr_status
                    );

            get diagnostics
                row_cnt := row_count;

        else

            row_cnt := 0;

        end if;

        select
            high_level_step || '.4.1' as src_step,
            'etl' as tgt_schema,
            'gig_status_history' as tgt_name,
            'merge' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (merge into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows merged', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 4.2 - Updating valid_to_dttm

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.4.2' as src_step,
            'etl' as tgt_schema,
            'gig_status_history' as tgt_name,
            'update' as op_type
        into log_rec;

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.op_type = 'merge'
            and l.tgt_name = 'gig_status_history';

        if empty_incr_flg is false then

            create unique index if not exists uidx_gig_status_history_valid_from
                on etl.gig_status_history (gig_id, valid_from_dttm); -- this index has to be set up for replication

            alter table if exists etl.gig_status_history
                replica identity using index uidx_gig_status_history_valid_from;

            with history_to_fix as
            (
                select
                    valid_from_dttm,
                    lead_valid_from,
                    gig_id
                from
                (
                    select
                        valid_to_dttm,
                        valid_from_dttm,
                        lead(valid_from_dttm) over (partition by gig_id order by valid_from_dttm) as lead_valid_from,
                        gig_id
                    from etl.gig_status_history
                ) a
                where
                    valid_to_dttm > lead_valid_from -- normal valid_to_dttm update after new history arrives
                    or lead_valid_from - valid_to_dttm > '1 us'::interval -- finding holes in history
            )

            update etl.gig_status_history as h
            set
                valid_to_dttm = f.lead_valid_from - '1 us'::interval,
                processed_dttm = transaction_timestamp(),
                "load_id" = load_id
            from history_to_fix f
            where
                h.gig_id = f.gig_id
                and h.valid_from_dttm = f.valid_from_dttm;

            get diagnostics
                row_cnt := row_count;

            create unique index if not exists uidx_gig_status_history_valid_to
                on etl.gig_status_history (gig_id, valid_to_dttm) with (fillfactor = 100);
            create index if not exists idx_gig_status_history_valid_from
                on etl.gig_status_history (valid_from_dttm desc);
            create index if not exists idx_gig_status_history_valid_to
                on etl.gig_status_history (valid_to_dttm desc);
            create index if not exists idx_gig_status_history_load_id
                on etl.gig_status_history ("load_id" desc);

                -- Clustering

            select
                coalesce
                (
                    (
                        coalesce(greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric, 0)::numeric >= cluster_threshold
                        or 1 - abs(st.correlation)::numeric >= cluster_threshold
                    ),
                    false
                )
            into do_cluster_flg
            from pg_catalog.pg_class c
            left join pg_catalog.pg_stat_user_tables s
                on c."oid" = s.relid
            left join pg_catalog.pg_stats st
                on c."oid" = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                and st.attname = 'gig_id' -- clustering by gig_id
            where
                c."oid" = (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass;

            if do_cluster_flg is true then

                cluster etl.gig_status_history using uidx_gig_status_history_valid_from;

                create statistics if not exists etl.gig_status_history_id_dttm
                on gig_id, valid_from_dttm, valid_to_dttm
                from etl.gig_status_history;

                analyze etl.gig_status_history;

            end if;

        else

            row_cnt := 0;

        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (update ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows updated', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 5 - Seller status SCD6-history

            -- Step 5.1 - Getting gigs increment

        task_start_dttm := clock_timestamp();

        select
            coalesce(max(valid_from_dttm), '2000-01-01 00:00:00 UTC'::timestamptz)
        into incr_start_dttm
        from etl.seller_status_history;

        select
            false
        into empty_incr_flg
        from etl.gig_status_history
        where
            valid_from_dttm > incr_start_dttm
        limit 1;

        if empty_incr_flg is false then

            drop table if exists etl_wrk.sellers_history_seller_increment cascade;

            create unlogged table if not exists etl_wrk.sellers_history_seller_increment
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    g.seller_id
                from etl.gig_status_history gh
                join public.gigs g
                    on g.id = gh.gig_id
                where
                    gh.valid_from_dttm > incr_start_dttm
                group by
                    g.seller_id;

            get diagnostics
                row_cnt := row_count;

            alter table if exists etl_wrk.sellers_history_seller_increment
                add primary key (seller_id) with (fillfactor = 100);

            cluster etl_wrk.sellers_history_seller_increment using sellers_history_seller_increment_pkey;

            analyze etl_wrk.sellers_history_seller_increment;

        else

            row_cnt := 0;

        end if;

        select
            high_level_step || '.5.1' as src_step,
            'etl_wrk' as tgt_schema,
            'sellers_history_seller_increment' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 5.2 - Tracking fiverr_status changes and inserting new history

        task_start_dttm := clock_timestamp();

        if empty_incr_flg is false then

            alter table if exists etl.seller_status_history
                set
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                );

            drop index if exists etl.uidx_seller_status_history_valid_to cascade; -- this index blocks merge, has to be recreated

            with new_revs as
            (
                select
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    valid_from_dttm,
                    coalesce(
                                lead(valid_from_dttm) over (partition by seller_id order by valid_from_dttm) - '1 us'::interval,
                                '2999-12-31 00:00:00 UTC'::timestamptz
                            ) as valid_to_dttm,
                    seller_id,
                    status
                from
                (
                    select
                        valid_from_dttm,
                        seller_id,
                        status,
                        case
                            when lag(status, 1, 'empty') over (partition by seller_id order by valid_from_dttm) != coalesce(status, 'empty')
                                then true
                            else false
                        end as is_group_start
                    from
                    (
                        select
                            valid_from_dttm,
                            seller_id,
                            case
                                when all_gigs_arr = not_avail_arr and all_gigs_arr != del_arr
                                    then 'not available'
                            else 'available' end as status
                        from
                        (
                            select
                                valid_from_dttm,
                                seller_id,
                                fiverr_status,
                                public.array_distinct_sort(
                                        array_agg(gig_id) over (partition by seller_id order by valid_from_dttm asc),
                                        do_grouping := true
                                    ) as all_gigs_arr,
                                case
                                    when fiverr_status in ('blocked', 'denied', 'suspended', 'deleted')
                                        then public.array_distinct_sort(
                                                array_agg(gig_id)
                                                    filter (where fiverr_status in ('blocked', 'denied', 'suspended', 'deleted'))
                                                    over (partition by seller_id, gr_not_avail order by valid_from_dttm),
                                                do_grouping := true
                                            )
                                    else array[]::int[]
                                end as not_avail_arr,
                                case
                                    when fiverr_status = 'deleted'
                                        then public.array_distinct_sort(
                                                array_agg(gig_id)
                                                    filter (where fiverr_status = 'deleted')
                                                    over (partition by seller_id, gr_del order by valid_from_dttm),
                                                do_grouping := true
                                            )
                                    else array[]::int[]
                                end as del_arr
                            from
                            (
                                select
                                    valid_from_dttm,
                                    seller_id,
                                    gig_id,
                                    fiverr_status,
                                    sum(not_avail_gr_start) over w2 as gr_not_avail,
                                    sum(del_gr_start) over w2 as gr_del
                                from
                                (
                                    select
                                        valid_from_dttm,
                                        seller_id,
                                        gig_id,
                                        fiverr_status,
                                        case
                                            when
                                                (
                                                    (
                                                        fiverr_status in ('blocked', 'denied', 'suspended', 'deleted')
                                                        and lag(fiverr_status, 1, 'empty') over w1 not in ('blocked', 'denied', 'suspended', 'deleted')
                                                    )
                                                    or
                                                    (
                                                        fiverr_status not in ('blocked', 'denied', 'suspended', 'deleted')
                                                        and lag(fiverr_status, 1, 'empty') over w1 in ('blocked', 'denied', 'suspended', 'deleted', 'empty')
                                                    )
                                                )
                                                and lag(valid_from_dttm, 1, '2000-01-01 00:00:00 UTC'::timestamptz) over w1 != valid_from_dttm
                                                    then 1
                                            else 0
                                        end as not_avail_gr_start,
                                        case
                                            when
                                                (
                                                    (fiverr_status = 'deleted' and lag(fiverr_status, 1, 'empty') over w1 != 'deleted')
                                                    or (fiverr_status != 'deleted' and lag(fiverr_status, 1, 'empty') over w1 in ('deleted', 'empty'))
                                                )
                                                and lag(valid_from_dttm, 1, '2000-01-01 00:00:00 UTC'::timestamptz) over w1 != valid_from_dttm
                                                    then 1
                                            else 0
                                        end as del_gr_start
                                    from
                                    (
                                        select
                                            h.valid_from_dttm,
                                            g.seller_id,
                                            g.id as gig_id,
                                            h.fiverr_status
                                        from etl.gig_status_history h
                                        join public.gigs g
                                            on g.id = h.gig_id
                                        where exists
                                            (
                                                select
                                                from etl_wrk.sellers_history_seller_increment s
                                                where
                                                    s.seller_id = g.seller_id
                                            )
                                    ) gh
                                    window w1 as (partition by seller_id order by valid_from_dttm)
                                ) gr_st
                                window w2 as (partition by seller_id order by valid_from_dttm)
                            ) gr
                        ) ar

                        union all

                        select
                            valid_from_dttm,
                            seller_id,
                            status
                        from etl.seller_status_history
                        where
                            valid_to_dttm = '2999-12-31 00:00:00 UTC'::timestamptz -- current slice
                    ) st
                ) hr
                where
                    is_group_start is true -- history rollup
            )

            merge into etl.seller_status_history as g
            using new_revs n
                on n.seller_id = g.seller_id
                and n.valid_from_dttm = g.valid_from_dttm
            when matched then
                do nothing -- to avoid inserting current slice again
            when not matched then
                insert
                    (
                        "load_id",
                        processed_dttm,
                        valid_from_dttm,
                        valid_to_dttm,
                        seller_id,
                        status
                    )
                values
                    (
                        n."load_id",
                        n.processed_dttm,
                        n.valid_from_dttm,
                        n.valid_to_dttm,
                        n.seller_id,
                        n.status
                    );

            get diagnostics
                row_cnt := row_count;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.sellers_history_seller_increment cascade;
        end if;

        select
            high_level_step || '.5.2' as src_step,
            'etl' as tgt_schema,
            'seller_status_history' as tgt_name,
            'merge' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (merge into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows merged', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 5.3 - Updating valid_to_dttm

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.5.3' as src_step,
            'etl' as tgt_schema,
            'seller_status_history' as tgt_name,
            'update' as op_type
        into log_rec;

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.op_type = 'merge'
            and l.tgt_name = 'seller_status_history';

        if empty_incr_flg is false then

            create unique index if not exists uidx_seller_status_history_valid_from
                on etl.seller_status_history (seller_id, valid_from_dttm); -- this index has to be set up for replication

            alter table if exists etl.seller_status_history
                replica identity using index uidx_seller_status_history_valid_from;

            with history_to_fix as
            (
                select
                    valid_from_dttm,
                    lead_valid_from,
                    seller_id
                from
                (
                    select
                        valid_to_dttm,
                        valid_from_dttm,
                        lead(valid_from_dttm) over (partition by seller_id order by valid_from_dttm) as lead_valid_from,
                        seller_id
                    from etl.seller_status_history
                ) a
                where
                    valid_to_dttm > lead_valid_from -- normal valid_to_dttm update after new history arrives
                    or lead_valid_from - valid_to_dttm > '1 us'::interval -- finding holes in history
            )

            update etl.seller_status_history as h
            set
                valid_to_dttm = f.lead_valid_from - '1 us'::interval,
                processed_dttm = transaction_timestamp(),
                "load_id" = load_id
            from history_to_fix f
            where
                h.seller_id = f.seller_id
                and h.valid_from_dttm = f.valid_from_dttm;

            get diagnostics
                row_cnt := row_count;

            create unique index if not exists uidx_seller_status_history_valid_to
                on etl.seller_status_history (seller_id, valid_to_dttm) with (fillfactor = 100);
            create index if not exists idx_seller_status_history_valid_from
                on etl.seller_status_history (valid_from_dttm desc);
            create index if not exists idx_seller_status_history_valid_to
                on etl.seller_status_history (valid_to_dttm desc);
            create index if not exists idx_seller_status_history_load_id
                on etl.seller_status_history ("load_id" desc);

                -- Clustering

            select
                coalesce
                (
                    (
                        coalesce(greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric, 0)::numeric >= cluster_threshold
                        or 1 - abs(st.correlation)::numeric >= cluster_threshold
                    ),
                    false
                )
            into do_cluster_flg
            from pg_catalog.pg_class c
            left join pg_catalog.pg_stat_user_tables s
                on c."oid" = s.relid
            left join pg_catalog.pg_stats st
                on c."oid" = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                and st.attname = 'seller_id' -- clustering by seller_id
            where
                c."oid" = (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass;

            if do_cluster_flg is true then

                cluster etl.seller_status_history using uidx_seller_status_history_valid_from;

                create statistics if not exists etl.seller_status_history_id_dttm
                on seller_id, valid_from_dttm, valid_to_dttm
                from etl.seller_status_history;

                analyze etl.seller_status_history;

            end if;

        else

            row_cnt := 0;

        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (update into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows updated', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 6 - Bulding gig_reviews_agg table with review count and latest review for each gig

            -- Step 6.1 - gig_reviews_meta increment for gig_reviews_agg

        task_start_dttm := clock_timestamp();

        select
            coalesce(max(processed_dttm), '2000-01-01 00:00:00 UTC'::timestamptz)
        into incr_start_dttm
        from etl.gig_reviews_agg;

        select
            false
        into empty_incr_flg
        from public.gig_reviews_meta
        where
            scraped_at > incr_start_dttm
        limit 1;

        if empty_incr_flg is false then

            drop table if exists etl_wrk.gig_reviews_agg_increment cascade;

            create table if not exists etl_wrk.gig_reviews_agg_increment as

                select distinct
                    gig_id
                from public.gig_reviews_meta m
                where
                    scraped_at > incr_start_dttm;

            get diagnostics
                row_cnt = row_count;

            alter table if exists etl_wrk.gig_reviews_agg_increment
                add primary key (gig_id) with (fillfactor = 100);

            cluster etl_wrk.gig_reviews_agg_increment using gig_reviews_agg_increment_pkey;

            analyze etl_wrk.gig_reviews_agg_increment;

        else

            row_cnt := 0;

        end if;

        select
            high_level_step || '.6.1' as src_step,
            'etl_wrk' as tgt_schema,
            'gig_reviews_agg_increment' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 6.2 - gig_reviews_agg

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.6.2' as src_step,
            'etl' as tgt_schema,
            'gig_reviews_agg' as tgt_name,
            'merge' as op_type
        into log_rec;

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.tgt_name = 'gig_reviews_agg_increment';

        if empty_incr_flg is false then

            alter table if exists etl.gig_reviews_agg
                set
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                );

            alter table if exists etl.gig_reviews_agg
                replica identity using index gig_reviews_agg_pkey;

            with incr as
            (
                select distinct on (gig_id)
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    m.fiverr_created_at as last_fiverr_created_at,
                    m.gig_id,
                    m.id as last_review_id,
                    (count(*) over (partition by m.gig_id))::int as reviews_count,
                    m.fiverr_review_id as last_fiverr_review_id
                from public.gig_reviews_meta m
                where exists
                    (
                        select
                        from etl_wrk.gig_reviews_agg_increment i
                        where
                            m.gig_id = i.gig_id
                    )
                order by
                    m.gig_id,
                    m.fiverr_created_at desc
            )

            merge into etl.gig_reviews_agg a
            using incr i on i.gig_id = a.gig_id
            when not matched then
                insert
                    (
                        "load_id",
                        processed_dttm,
                        last_fiverr_created_at,
                        gig_id,
                        last_review_id,
                        reviews_count,
                        last_fiverr_review_id
                    )
                values
                    (
                        i."load_id",
                        i.processed_dttm,
                        i.last_fiverr_created_at,
                        i.gig_id,
                        i.last_review_id,
                        i.reviews_count,
                        i.last_fiverr_review_id
                    )
            when matched then
                update set
                (
                    "load_id",
                    processed_dttm,
                    last_fiverr_created_at,
                    gig_id,
                    last_review_id,
                    reviews_count,
                    last_fiverr_review_id
                ) =
                (
                    i."load_id",
                    i.processed_dttm,
                    i.last_fiverr_created_at,
                    i.gig_id,
                    i.last_review_id,
                    i.reviews_count,
                    i.last_fiverr_review_id
                );

            get diagnostics
                row_cnt = row_count;

            create unique index if not exists uidx_gig_reviews_agg_gig_id_last_fiverr_created_at
                on etl.gig_reviews_agg (gig_id, last_fiverr_created_at desc);
            create index if not exists idx_gig_reviews_agg_last_fiverr_created_at
                on etl.gig_reviews_agg (last_fiverr_created_at desc);
            create index if not exists idx_gig_reviews_agg_load_id
                on etl.gig_reviews_agg ("load_id" desc);

                -- Clustering

            select
                coalesce
                (
                    (
                        coalesce(greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric, 0)::numeric >= cluster_threshold
                        or 1 - abs(st.correlation)::numeric >= cluster_threshold
                    ),
                    false
                )
            into do_cluster_flg
            from pg_catalog.pg_class c
            left join pg_catalog.pg_stat_user_tables s
                on c."oid" = s.relid
            left join pg_catalog.pg_stats st
                on c."oid" = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                and st.attname = 'gig_id' -- clustering by gig_id
            where
                c."oid" = (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass;

            if do_cluster_flg is true then

                cluster etl.gig_reviews_agg using gig_reviews_agg_pkey;

                create statistics if not exists etl.gig_reviews_agg_all_ids
                on gig_id, last_review_id, last_fiverr_review_id, last_fiverr_created_at
                from etl.gig_reviews_agg;

                analyze etl.gig_reviews_agg;

            end if;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.gig_reviews_agg_increment cascade;
        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (merge into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows merged', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 7 - Materializing price ranges from gig_reviews JSON

            -- Step 7.1 - gig_reviews_meta increment for gig_reviews_prices

        task_start_dttm := transaction_timestamp();

        select
            coalesce(max(processed_dttm), '2000-01-01 00:00:00 UTC'::timestamptz)
        into incr_start_dttm
        from etl.gig_reviews_prices;

        select
            false
        into empty_incr_flg
        from public.gig_reviews_meta
        where
            scraped_at > incr_start_dttm
        limit 1;

        if empty_incr_flg is false then

            drop table if exists etl_wrk.gig_reviews_prices_id_increment cascade;

            create unlogged table if not exists etl_wrk.gig_reviews_prices_id_increment
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    g.id
                from public.gig_reviews_meta g
                where not exists -- Filtering out rows which were already processed
                    (
                        select
                        from etl.gig_reviews_prices p
                        where
                            p.id = g.id
                    );

            get diagnostics
                row_cnt := row_count;

            alter table if exists etl_wrk.gig_reviews_prices_id_increment
                add primary key (id) with (fillfactor = 100);

            cluster etl_wrk.gig_reviews_prices_id_increment using gig_reviews_prices_id_increment_pkey;

            analyze etl_wrk.gig_reviews_prices_id_increment;

        else

            row_cnt := 0;

        end if;

        select
            high_level_step || '.7.1' as src_step,
            'etl_wrk' as tgt_schema,
            'gig_reviews_prices_id_increment' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 7.2 - Parsing JSONs

        task_start_dttm := clock_timestamp();

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.tgt_name = 'gig_reviews_prices_id_increment';

        if empty_incr_flg is false then

            drop table if exists etl_wrk.gig_reviews_prices_parsed;

            create unlogged table if not exists etl_wrk.gig_reviews_prices_parsed
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    g.fiverr_created_at,
                    g.id,
                    g.gig_id,
                    case
                        when pg_input_is_valid((r.raw ->> 'price_range_start'), 'numeric') is true then
                            coalesce((r.raw ->> 'price_range_start')::numeric, 0::numeric)
                        else 0::numeric
                    end as price_range_start,
                    case
                        when pg_input_is_valid((r.raw ->> 'price_range_end'), 'numeric') is true then
                            coalesce((r.raw ->> 'price_range_end')::numeric, 'infinity'::numeric)
                        else 'infinity'::numeric
                    end as price_range_end
                from public.gig_reviews_meta g
                join public.gig_reviews r
                    on g.id = r.id
                where
                    g.schema_version <= 4 -- setting latest parsed version manually
                    and exists
                    (
                        select
                        from etl_wrk.gig_reviews_prices_id_increment h
                        where
                            g.id = h.id
                    );

            get diagnostics
                row_cnt := row_count;

            analyze etl_wrk.gig_reviews_prices_parsed;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.gig_reviews_prices_id_increment cascade;
        end if;

        select
            high_level_step || '.7.2' as src_step,
            'etl_wrk' as tgt_schema,
            'gig_reviews_prices_parsed' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 7.3 - inserting valid data into pre-metrics table

        task_start_dttm := clock_timestamp();

        select
            high_level_step || '.7.3' as src_step,
            'etl' as tgt_schema,
            'gig_reviews_prices' as tgt_name,
            'insert' as op_type
        into log_rec;

        select
            l."row_count" = 0
        into empty_incr_flg
        from etl_log.etl_log l
        where
            l."load_id" = load_id
            and l."row_count" is not null
            and l.tgt_name = 'gig_reviews_prices_parsed';

        if empty_incr_flg is false then

            perform public.maintenance_partitions_autovacuum_switch('etl.gig_reviews_prices'::regclass, false);

            insert into etl.gig_reviews_prices
                (
                    "load_id",
                    processed_dttm,
                    fiverr_created_at,
                    id,
                    gig_id,
                    price_range_start,
                    price_range_end
                )

                select
                    load_id as "load_id",
                    transaction_timestamp() as processed_dttm,
                    fiverr_created_at,
                    id,
                    gig_id,
                    price_range_start,
                    price_range_end
                from etl_wrk.gig_reviews_prices_parsed;

            get diagnostics
                row_cnt := row_count;

            create unique index if not exists uidx_gig_reviews_prices_id
                on etl.gig_reviews_prices (id, fiverr_created_at desc);
            create index if not exists idx_gig_reviews_prices_gig_id
                on etl.gig_reviews_prices (gig_id, fiverr_created_at desc);
            create index if not exists idx_gig_reviews_prices_fiverr_created_at
                on etl.gig_reviews_prices (fiverr_created_at desc);
            create index if not exists idx_gig_reviews_prices_load_id
                on etl.gig_reviews_prices ("load_id" desc, fiverr_created_at desc);

            alter table if exists etl.gig_reviews_prices
                replica identity using index uidx_gig_reviews_prices_id;

                -- Clustering

            analyze_root_flg := false;

            stats_columns_list := array[
                    'id', 'gig_id', 'price_range_start', 'price_range_end'
                ];

                -- Finding partitions where new rows were inserted

            with part_key_values as
            (
                select
                    min(fiverr_created_at) as min_part_key_value,
                    max(fiverr_created_at) as max_part_key_value
                from etl_wrk.gig_reviews_prices_parsed
            )

            select
                array_agg(b.table_oid)
            into updated_partitions_arr
            from
            (
                select
                    a.table_oid,
                    case
                        when a.relpartbound = 'DEFAULT' then '2000-01-01 00:00:00 UTC'::timestamptz
                        when regexp_substr(a.relpartbound, '(?<=FROM \()(.*)(?=\) TO)') = 'MINVALUE' then '2000-01-01 00:00:00 UTC'::timestamptz
                        else regexp_substr(a.relpartbound, '(?<=FROM \('')(.*)(?=''\) TO)')::timestamptz
                    end as value_from,
                    case
                        when a.relpartbound = 'DEFAULT' then '2099-01-01 00:00:00 UTC'::timestamptz
                        when regexp_substr(a.relpartbound, '(?<=TO \()(.*)(?=\))') = 'MAXVALUE' then '2099-01-01 00:00:00 UTC'::timestamptz
                        else regexp_substr(a.relpartbound, '(?<=TO \('')(.*)(?=''\))')::timestamptz
                    end as value_to
                from
                (
                    select
                        t.table_oid,
                        pg_catalog.pg_get_expr(c.relpartbound, t.table_oid, true) as relpartbound
                    from public.get_partitions_tree
                        (
                            (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass
                        ) t
                    join pg_catalog.pg_class c
                        on t.table_oid = c."oid"
                    where
                        t.relkind in ('r', 'f')
                ) a
            ) b
            cross join part_key_values p
            where -- overlapping ranges
                tstzrange(p.min_part_key_value, p.max_part_key_value) && tstzrange(b.value_from, b.value_to);

                -- Clustering loop

            for partitions_maintenance_rec in
                select
                    quote_ident(t.schema_name) as partition_schema,
                    quote_ident(t.table_name) as partition_name,
                    quote_ident(ci.relname) as cluster_index_name,
                    (
                        coalesce
                            (
                                greatest(row_cnt, s.n_dead_tup)::numeric / nullif(s.n_live_tup, 0)::numeric,
                                0
                            )::numeric >= cluster_threshold
                        or 1::numeric - coalesce(abs(st.correlation), 0)::numeric >= cluster_threshold
                    ) as do_cluster_flg_partition
                from public.get_partitions_tree
                    (
                        (quote_ident(log_rec.tgt_schema) || '.' || quote_ident(log_rec.tgt_name))::regclass
                    ) t
                join pg_catalog.pg_attribute a
                    on a.attrelid = t.table_oid
                join pg_catalog.pg_index ix
                    on ix.indrelid = a.attrelid
                    and ix.indkey[0] = a.attnum
                join pg_catalog.pg_class ci
                    on ix.indexrelid = ci."oid"
                left join pg_catalog.pg_stat_user_tables s
                    on t.table_oid = s.relid
                left join pg_catalog.pg_stats st
                    on a.attrelid = (quote_ident(st.schemaname) || '.' || quote_ident(st.tablename))::regclass
                    and a.attname = st.attname
                where
                    t.table_oid = any(updated_partitions_arr) -- clustering only paritions with new rows
                    and a.attname = 'fiverr_created_at' -- clustering by fiverr_created_at
                    and cardinality(ix.indkey) = 1
            loop

                if partitions_maintenance_rec.do_cluster_flg_partition is true then

                    -- CLUSTER + CREATE STATISTICS
                    maintenance_qry := 'cluster '
                        || partitions_maintenance_rec.partition_schema || '.' || partitions_maintenance_rec.partition_name
                        || ' using ' || partitions_maintenance_rec.cluster_index_name || ';' || repeat(E'\n', 2)

                        || 'create statistics if not exists ' || partitions_maintenance_rec.partition_schema || '.'
                        || substring('stats_' || partitions_maintenance_rec.partition_name, 1, 63) || E'\n'
                        || 'on ' || array_to_string(stats_columns_list, ', ') || E'\n'
                        || 'from ' || partitions_maintenance_rec.partition_schema || '.' || partitions_maintenance_rec.partition_name || ';';

                    execute maintenance_qry;

                    analyze_root_flg := true;

                end if;

            end loop;

            if analyze_root_flg is true then

                analyze etl.gig_reviews_prices;

            end if;

        else

            row_cnt := 0;

        end if;

        if debug_mode is false then
            drop table if exists etl_wrk.gig_reviews_prices_parsed cascade;
        end if;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Exiting

        select
            high_level_step || '.8' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - run_start_dttm;

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

    end;

$func$;