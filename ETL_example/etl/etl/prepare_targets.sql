create or replace function etl.prepare_targets
(
    end_dttm timestamptz,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    ddl_change_mode boolean default false,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '2'
)
returns void
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        task_start_dttm timestamptz;
        run_start_dttm timestamptz;
        total_runtime interval;

        split_point text;
        split_interval text := '1 month';
        split_amount smallint := 2;
        monthly_window_period interval := '1 year'::interval;
        quarterly_window_period interval;
        archive_lag interval := '2 year'::interval;
        archive_point timestamptz;
        partitions_merge_log jsonb;

    begin

        -- Starting

        run_start_dttm := clock_timestamp();

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            end_dttm,
            load_id,
            ddl_change_mode,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Preparing ETL targets

        task_start_dttm := clock_timestamp();

            -- Check if some targets needs to be recreated

        if ddl_change_mode is true then
            drop table if exists etl.metrics cascade;
            drop table if exists etl.gigs cascade;
            drop table if exists etl.sellers cascade;
            drop table if exists etl.all_categories cascade;
            drop table if exists etl.categories cascade;
            drop table if exists etl.subcategories cascade;
            drop table if exists etl.nested_subcategories cascade;
            drop table if exists etl.category_x_seller_levels cascade;
            drop table if exists etl.subcategory_x_seller_levels cascade;
            drop table if exists etl.nested_subcategory_x_seller_levels cascade;
        end if;

            -- Prepare partitioning variables

        split_point := date_trunc('month', end_dttm)::text;
        archive_point := date_trunc('year', end_dttm - archive_lag);
        quarterly_window_period := age(date_trunc('month', end_dttm), archive_point) - monthly_window_period;

            -- Executing DDLs

                -- gigs revisions prematerialization

        create table if not exists etl.gigs_revisions_pre_metrics
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            created_at timestamptz not null,
            fiverr_created_at timestamptz null,
            scraped_at timestamptz not null,
            id int not null,
            gig_id int not null,
            fiverr_gig_id int not null,
            fiverr_seller_id int not null,
            category_id int not null,
            sub_category_id int not null,
            nested_sub_category_id int null,
            gig_ratings_count int null,
            gig_is_pro boolean not null,
            gig_rating numeric null,
            prices numeric[] not null,
            gig_title text null,
            category_name text null,
            sub_category_name text null,
            nested_sub_category_name text null,
            gig_cached_slug text not null,
            gig_preview_url text not null
        )
        partition by range (scraped_at);

        select
            jsonb_agg(t)
        into partitions_merge_log
        from
        (
            select *
            from public.maintenance_partitions_merge
                (
                    "root_table_oid" := 'etl.gigs_revisions_pre_metrics'::regclass,
                    "split_point" := split_point,
                    "split_interval" := split_interval,
                    "merge_windows" := array[monthly_window_period::text, quarterly_window_period::text],
                    "merge_intervals" := array['1 month', '3 month'],
                    "split_amount" := split_amount,
                    "archive_partition_flg" := true,
                    "archive_point" := archive_point::text,
                    "split_unbounded_flg" := false,
                    "default_partition_flg" := false,
                    "dry_run" := false,
                    "drop_detached_flg" := not debug_mode,
                    "inactive_index_fillfactor_100_flg" := true,
                    "cluster_new_partitions_flg" := true,
                    "copy_local_options_flg" := true,
                    "copy_foreign_options_flg" :=  true
                )
        ) t;

        log_msg := 'Repartitioning of ' || log_rec.tgt_schema || '.gigs_revisions_pre_metrics has been completed' || E'\n'
            || 'Log:' || E'\n' || coalesce(jsonb_pretty(partitions_merge_log), 'No actions were needed');

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

                -- invalid gigs revisions prematerialization

        create table if not exists etl.gigs_revisions_pre_metrics_invalid
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            created_at timestamptz not null,
            fiverr_created_at timestamptz null,
            scraped_at timestamptz not null,
            id int not null,
            gig_id int not null,
            fiverr_gig_id int null,
            fiverr_seller_id int null,
            category_id int null,
            sub_category_id int null,
            nested_sub_category_id int null,
            gig_ratings_count int null,
            gig_is_pro boolean null,
            gig_rating numeric null,
            prices numeric[] null,
            gig_title text null,
            category_name text null,
            sub_category_name text null,
            nested_sub_category_name text null,
            gig_cached_slug text null,
            gig_preview_url text null
        );

                -- gigs pages prematerialization

        create table if not exists etl.gigs_pages_pre_metrics
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            created_at timestamptz not null,
            scraped_at timestamptz not null,
            id int not null,
            gig_id int not null,
            fiverr_gig_id int not null,
            fiverr_seller_id int not null,
            seller_completed_orders_count int not null,
            prices numeric[] not null
        )
        partition by range (scraped_at);

        select
            jsonb_agg(t)
        into partitions_merge_log
        from
        (
            select *
            from public.maintenance_partitions_merge
                (
                    "root_table_oid" := 'etl.gigs_pages_pre_metrics'::regclass,
                    "split_point" := split_point,
                    "split_interval" := split_interval,
                    "merge_windows" := array[monthly_window_period::text, quarterly_window_period::text],
                    "merge_intervals" := array['1 month', '3 month'],
                    "split_amount" := split_amount,
                    "archive_partition_flg" := true,
                    "archive_point" := archive_point::text,
                    "split_unbounded_flg" := false,
                    "default_partition_flg" := false,
                    "dry_run" := false,
                    "drop_detached_flg" := not debug_mode,
                    "inactive_index_fillfactor_100_flg" := true,
                    "cluster_new_partitions_flg" := true,
                    "copy_local_options_flg" := true,
                    "copy_foreign_options_flg" :=  true
                )
        ) t;

        log_msg := 'Repartitioning of ' || log_rec.tgt_schema || '.gigs_pages_pre_metrics has been completed' || E'\n'
            || 'Log:' || E'\n' || coalesce(jsonb_pretty(partitions_merge_log), 'No actions were needed');

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

                -- invalid gigs pages prematerialization

        create table if not exists etl.gigs_pages_pre_metrics_invalid
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            created_at timestamptz not null,
            scraped_at timestamptz not null,
            id int not null,
            gig_id int not null,
            fiverr_gig_id int null,
            fiverr_seller_id int null,
            seller_completed_orders_count int null,
            prices numeric[] null
        );

                -- sellers revisions prematerialization

        create table if not exists etl.sellers_revisions_pre_metrics
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            created_at timestamptz not null,
            fiverr_created_at timestamptz null,
            scraped_at timestamptz not null,
            id int not null,
            seller_id int not null,
            fiverr_seller_id int not null,
            seller_ratings_count int null,
            seller_is_pro boolean not null,
            seller_rating numeric null,
            seller_level text not null,
            seller_name text not null,
            agency_slug text null,
            agency_status text null,
            seller_profile_image text null,
            seller_country text null,
            seller_country_code text null,
            seller_languages text[] null
        )
        partition by range (scraped_at);

        select
            jsonb_agg(t)
        into partitions_merge_log
        from
        (
            select *
            from public.maintenance_partitions_merge
                (
                    "root_table_oid" := 'etl.sellers_revisions_pre_metrics'::regclass,
                    "split_point" := split_point,
                    "split_interval" := split_interval,
                    "merge_windows" := array[monthly_window_period::text, quarterly_window_period::text],
                    "merge_intervals" := array['1 month', '3 month'],
                    "split_amount" := split_amount,
                    "archive_partition_flg" := true,
                    "archive_point" := archive_point::text,
                    "split_unbounded_flg" := false,
                    "default_partition_flg" := false,
                    "dry_run" := false,
                    "drop_detached_flg" := not debug_mode,
                    "inactive_index_fillfactor_100_flg" := true,
                    "cluster_new_partitions_flg" := true,
                    "copy_local_options_flg" := true,
                    "copy_foreign_options_flg" :=  true
                )
        ) t;

        log_msg := 'Repartitioning of ' || log_rec.tgt_schema || '.sellers_revisions_pre_metrics has been completed' || E'\n'
            || 'Log:' || E'\n' || coalesce(jsonb_pretty(partitions_merge_log), 'No actions were needed');

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

                -- invalid sellers revisions prematerialization

        create table if not exists etl.sellers_revisions_pre_metrics_invalid
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            created_at timestamptz not null,
            fiverr_created_at timestamptz null,
            scraped_at timestamptz not null,
            id int not null,
            seller_id int not null,
            fiverr_seller_id int null,
            seller_ratings_count int null,
            seller_is_pro boolean null,
            seller_rating numeric null,
            seller_level text not null,
            seller_name text null,
            agency_slug text null,
            agency_status text null,
            seller_profile_image text null,
            seller_country text null,
            seller_country_code text null,
            seller_languages text[] null
        );

                -- gig status history

        create table if not exists etl.gig_status_history
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            valid_from_dttm timestamptz not null,
            valid_to_dttm timestamptz not null,
            gig_id int not null,
            fiverr_status text not null
        );

                -- gig current status view

        create or replace view etl.gig_current_status as
            select
                gig_id,
                fiverr_status,
                valid_from_dttm
            from etl.gig_status_history
            where
                valid_to_dttm = '2999-12-31 00:00:00 UTC'::timestamptz;

                -- seller status history

        create table if not exists etl.seller_status_history
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            valid_from_dttm timestamptz not null,
            valid_to_dttm timestamptz not null,
            seller_id int not null,
            status text not null
        );

                -- seller current status view

        create or replace view etl.seller_current_status as
            select
                seller_id,
                status,
                valid_from_dttm
            from etl.seller_status_history
            where
                valid_to_dttm = '2999-12-31 00:00:00 UTC'::timestamptz;

                -- gig_reviews_agg

        create table if not exists etl.gig_reviews_agg
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            last_fiverr_created_at timestamptz,
            gig_id int primary key,
            last_review_id int not null,
            reviews_count int not null,
            last_fiverr_review_id text not null
        );

                -- gig_reviews_prices

        create table if not exists etl.gig_reviews_prices
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            fiverr_created_at timestamptz not null,
            id int not null,
            gig_id int not null,
            price_range_start numeric null default 0::numeric,
            price_range_end numeric not null default 'infinity'::numeric
        )
        partition by range (fiverr_created_at);

        select
            jsonb_agg(t)
        into partitions_merge_log
        from
        (
            select *
            from public.maintenance_partitions_merge
                (
                    "root_table_oid" := 'etl.gig_reviews_prices'::regclass,
                    "split_point" := split_point,
                    "split_interval" := split_interval,
                    "merge_windows" := array[monthly_window_period::text, quarterly_window_period::text],
                    "merge_intervals" := array['1 month', '3 month'],
                    "split_amount" := split_amount,
                    "archive_partition_flg" := true,
                    "archive_point" := archive_point::text,
                    "split_unbounded_flg" := false,
                    "default_partition_flg" := false,
                    "dry_run" := false,
                    "drop_detached_flg" := not debug_mode,
                    "inactive_index_fillfactor_100_flg" := true,
                    "cluster_new_partitions_flg" := true,
                    "copy_local_options_flg" := true,
                    "copy_foreign_options_flg" :=  true
                )
        ) t;

        log_msg := 'Repartitioning of ' || log_rec.tgt_schema || '.gig_reviews_prices has been completed' || E'\n'
            || 'Log:' || E'\n' || coalesce(jsonb_pretty(partitions_merge_log), 'No actions were needed');

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

                -- category_tree_latest_revision

        create or replace view etl.category_tree_latest_revision as
            select distinct on (category_id, sub_category_id, nested_sub_category_id)
                id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                revision,
                category_name,
                sub_category_name,
                nested_sub_category_name,
                category_url,
                sub_category_url,
                nested_sub_category_url
            from public.category_tree
            order by
                category_id,
                sub_category_id,
                nested_sub_category_id,
                revision desc;

                -- metrics

        create table if not exists etl.metrics
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            seller_created_at timestamptz not null,
            seller_fiverr_created_at timestamptz null,
            seller_scraped_at timestamptz not null,
            gig_created_at timestamptz not null,
            gig_fiverr_created_at timestamptz null,
            gig_scraped_at timestamptz not null,
            seller_id int not null,
            fiverr_seller_id int not null,
            gig_id int not null,
            fiverr_gig_id int not null,
            category_id int not null,
            sub_category_id int not null,
            nested_sub_category_id int null,
            all_gigs_count_by_category int not null,
            all_gigs_count_by_subcategory int not null,
            all_gigs_count_by_nested_subcategory int null,
            regular_gigs_by_category int not null,
            regular_gigs_by_subcategory int not null,
            regular_gigs_by_nested_subcategory int null,
            pro_gigs_by_category int not null,
            pro_gigs_by_subcategory int not null,
            pro_gigs_by_nested_subcategory int null,
            seller_count_by_level_and_category int not null,
            seller_count_by_level_and_subcategory int not null,
            seller_count_by_level_and_nested_subcategory int null,
            gig_count_by_seller int not null,
            seller_ratings_count int not null,
            seller_completed_orders_count int not null,
            active_gigs_ratings_count_by_seller int not null,
            gig_ratings_count int not null,
            volume_by_seller int not null,
            volume_by_gig int not null,
            volume_by_category int not null,
            volume_by_subcategory int not null,
            volume_by_nested_subcategory int null,
            total_historical_volume_by_seller int not null,
            total_historical_volume_by_gig int not null,
            total_historical_volume_by_category int not null,
            total_historical_volume_by_subcategory int not null,
            total_historical_volume_by_nested_subcategory int null,
            heuristic bool not null,
            seller_is_pro bool not null,
            seller_is_active bool not null,
            gig_is_pro bool not null,
            gig_is_active bool not null,
            is_trend_valid_for_seller bool not null,
            is_trend_valid_for_gig bool not null,
            is_trend_valid_for_category bool not null,
            is_trend_valid_for_subcategory bool not null,
            is_trend_valid_for_nested_subcategory bool null,
            sales_volume_growth_percent_by_seller numeric not null,
            sales_volume_growth_percent_by_gig numeric not null,
            sales_volume_growth_percent_by_category numeric not null,
            sales_volume_growth_percent_by_subcategory numeric not null,
            sales_volume_growth_percent_by_nested_subcategory numeric null,
            competition_score_for_gig_by_category numeric not null,
            competition_score_for_gig_by_subcategory numeric not null,
            competition_score_for_gig_by_nested_subcategory numeric null,
            competition_score_for_category numeric not null,
            competition_score_for_subcategory numeric not null,
            competition_score_for_nested_subcategory numeric null,
            market_share_for_gig_by_category numeric not null,
            market_share_for_gig_by_subcategory numeric not null,
            market_share_for_gig_by_nested_subcategory numeric null,
            market_share_for_category numeric not null,
            market_share_for_subcategory numeric not null,
            market_share_for_nested_subcategory numeric null,
            seller_rating numeric not null,
            gig_rating numeric not null,
            min_price_by_seller numeric not null,
            min_price_by_gig numeric not null,
            min_price_by_category numeric not null,
            min_price_by_subcategory numeric not null,
            min_price_by_nested_subcategory numeric null,
            avg_price_by_seller numeric not null,
            avg_price_by_gig numeric not null,
            avg_price_by_category numeric not null,
            avg_price_by_subcategory numeric not null,
            avg_price_by_nested_subcategory numeric null,
            max_price_by_seller numeric not null,
            max_price_by_gig numeric not null,
            max_price_by_category numeric not null,
            max_price_by_subcategory numeric not null,
            max_price_by_nested_subcategory numeric null,
            weighted_min_price_by_seller numeric not null,
            weighted_min_price_by_category numeric not null,
            weighted_min_price_by_subcategory numeric not null,
            weighted_min_price_by_nested_subcategory numeric null,
            weighted_avg_price_by_seller numeric not null,
            weighted_avg_price_by_category numeric not null,
            weighted_avg_price_by_subcategory numeric not null,
            weighted_avg_price_by_nested_subcategory numeric null,
            weighted_max_price_by_seller numeric not null,
            weighted_max_price_by_category numeric not null,
            weighted_max_price_by_subcategory numeric not null,
            weighted_max_price_by_nested_subcategory numeric null,
            min_active_revenue_by_seller numeric not null,
            min_inactive_revenue_by_seller numeric not null,
            min_total_revenue_by_seller numeric not null,
            min_revenue_by_gig numeric not null,
            min_active_revenue_by_category numeric not null,
            min_inactive_revenue_by_category numeric not null,
            min_total_revenue_by_category numeric not null,
            min_active_revenue_by_subcategory numeric not null,
            min_inactive_revenue_by_subcategory numeric not null,
            min_total_revenue_by_subcategory numeric not null,
            min_active_revenue_by_nested_subcategory numeric null,
            min_inactive_revenue_by_nested_subcategory numeric null,
            min_total_revenue_by_nested_subcategory numeric null,
            avg_active_revenue_by_seller numeric not null,
            avg_inactive_revenue_by_seller numeric not null,
            avg_total_revenue_by_seller numeric not null,
            avg_revenue_by_gig numeric not null,
            avg_active_revenue_by_category numeric not null,
            avg_inactive_revenue_by_category numeric not null,
            avg_total_revenue_by_category numeric not null,
            avg_active_revenue_by_subcategory numeric not null,
            avg_inactive_revenue_by_subcategory numeric not null,
            avg_total_revenue_by_subcategory numeric not null,
            avg_active_revenue_by_nested_subcategory numeric null,
            avg_inactive_revenue_by_nested_subcategory numeric null,
            avg_total_revenue_by_nested_subcategory numeric null,
            max_active_revenue_by_seller numeric not null,
            max_inactive_revenue_by_seller numeric not null,
            max_total_revenue_by_seller numeric not null,
            max_revenue_by_gig numeric not null,
            max_active_revenue_by_category numeric not null,
            max_inactive_revenue_by_category numeric not null,
            max_total_revenue_by_category numeric not null,
            max_active_revenue_by_subcategory numeric not null,
            max_inactive_revenue_by_subcategory numeric not null,
            max_total_revenue_by_subcategory numeric not null,
            max_active_revenue_by_nested_subcategory numeric null,
            max_inactive_revenue_by_nested_subcategory numeric null,
            max_total_revenue_by_nested_subcategory numeric null,
            min_total_historical_revenue_by_seller numeric not null,
            min_total_historical_revenue_by_gig numeric not null,
            min_total_historical_revenue_by_category numeric not null,
            min_total_historical_revenue_by_subcategory numeric not null,
            min_total_historical_revenue_by_nested_subcategory numeric null,
            avg_total_historical_revenue_by_seller numeric not null,
            avg_total_historical_revenue_by_gig numeric not null,
            avg_total_historical_revenue_by_category numeric not null,
            avg_total_historical_revenue_by_subcategory numeric not null,
            avg_total_historical_revenue_by_nested_subcategory numeric null,
            max_total_historical_revenue_by_seller numeric not null,
            max_total_historical_revenue_by_gig numeric not null,
            max_total_historical_revenue_by_category numeric not null,
            max_total_historical_revenue_by_subcategory numeric not null,
            max_total_historical_revenue_by_nested_subcategory numeric null,
            revenue_growth_percent_by_seller numeric not null,
            revenue_growth_percent_by_gig numeric not null,
            revenue_growth_percent_by_category numeric not null,
            revenue_growth_percent_by_subcategory numeric not null,
            revenue_growth_percent_by_nested_subcategory numeric null,
            trend_by_seller numeric not null,
            r2_by_seller numeric not null,
            trends_avg_revenue_by_seller numeric not null,
            trend_by_gig numeric not null,
            r2_by_gig numeric not null,
            trends_avg_revenue_by_gig numeric not null,
            trend_by_category numeric not null,
            r2_by_category numeric not null,
            trends_avg_revenue_by_category numeric not null,
            trend_by_subcategory numeric not null,
            r2_by_subcategory numeric not null,
            trends_avg_revenue_by_subcategory numeric not null,
            trend_by_nested_subcategory numeric null,
            r2_by_nested_subcategory numeric null,
            trends_avg_revenue_by_nested_subcategory numeric null,
            rcrit numeric not null,
            seller_level text not null,
            seller_name text not null,
            agency_slug text null,
            agency_status text null,
            gig_title text null,
            gig_cached_slug text not null,
            category_name text not null,
            sub_category_name text not null,
            nested_sub_category_name text null,
            seller_country text null,
            seller_country_code text null,
            seller_languages text[] null,
            seller_profile_image text null,
            gig_preview_url text not null
        );

                -- gigs

        create table if not exists etl.gigs
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            seller_created_at timestamptz not null,
            seller_fiverr_created_at timestamptz null,
            seller_scraped_at timestamptz not null,
            gig_created_at timestamptz not null,
            gig_fiverr_created_at timestamptz null,
            gig_scraped_at timestamptz not null,
            min_total_historical_revenue bigint not null,
            avg_total_historical_revenue bigint not null,
            max_total_historical_revenue bigint not null,
            seller_id int not null,
            fiverr_seller_id int not null,
            gig_id int not null,
            fiverr_gig_id int not null,
            category_id int not null,
            sub_category_id int not null,
            nested_sub_category_id int null,
            global_rank int not null,
            seller_ratings_count int not null,
            ratings_count int not null,
            sales_volume int not null,
            total_historical_sales_volume int not null,
            min_price int not null,
            avg_price int not null,
            max_price int not null,
            min_revenue int not null,
            avg_revenue int not null,
            max_revenue int not null,
            heuristic bool not null,
            is_pro bool not null,
            is_active bool not null,
            is_trend_valid bool not null,
            sales_volume_growth_percent numeric not null,
            competition_score_for_gig_by_category numeric not null,
            competition_score_for_gig_by_subcategory numeric not null,
            competition_score_for_gig_by_nested_subcategory numeric null,
            market_share_for_gig_by_category numeric not null,
            market_share_for_gig_by_subcategory numeric not null,
            market_share_for_gig_by_nested_subcategory numeric null,
            seller_rating numeric not null,
            rating numeric not null,
            revenue_growth_percent numeric not null,
            revenue_trend numeric not null,
            seller_level text not null,
            seller_name text not null,
            title text null,
            cached_slug text not null,
            category_name text not null,
            sub_category_name text not null,
            nested_sub_category_name text null,
            seller_country text null,
            seller_country_code text null,
            image_preview text not null,
            seller_profile_image text null
        );

                -- sellers

        create table if not exists etl.sellers
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            created_at timestamptz not null,
            fiverr_created_at timestamptz null,
            scraped_at timestamptz not null,
            min_total_historical_revenue bigint not null,
            avg_total_historical_revenue bigint not null,
            max_total_historical_revenue bigint not null,
            seller_id int not null,
            fiverr_seller_id int not null,
            best_selling_gig_id int null,
            best_selling_fiverr_gig_id int null,
            global_rank int not null,
            gig_count_by_seller int not null,
            ratings_count int not null,
            completed_orders_count int not null,
            active_gigs_ratings_count_by_seller int not null,
            sales_volume int not null,
            total_historical_sales_volume int not null,
            best_selling_gig_total_historical_volume int null,
            weighted_avg_gig_price int not null,
            min_active_revenue int not null,
            min_inactive_revenue int not null,
            min_revenue int not null,
            avg_active_revenue int not null,
            avg_inactive_revenue int not null,
            avg_revenue int not null,
            max_active_revenue int not null,
            max_inactive_revenue int not null,
            max_revenue int not null,
            best_selling_gig_revenue int not null,
            is_pro bool not null,
            is_active bool not null,
            is_trend_valid bool not null,
            sales_volume_growth_percent numeric not null,
            rating numeric not null,
            revenue_growth_percent numeric not null,
            revenue_trend numeric not null,
            best_selling_gig_revenue_share numeric not null,
            best_selling_gig_title text null,
            best_selling_gig_cached_slug text null,
            "level" text not null,
            seller_name text not null,
            agency_slug text null,
            agency_status text null,
            country text null,
            country_code text null,
            languages text[] null,
            profile_image text null,
            best_selling_gig_image_preview text null
        );

                -- all_categories

        create table if not exists etl.all_categories
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            min_total_historical_revenue_by_category bigint not null,
            min_total_historical_revenue_by_subcategory bigint not null,
            min_total_historical_revenue_by_nested_subcategory bigint null,
            avg_total_historical_revenue_by_category bigint not null,
            avg_total_historical_revenue_by_subcategory bigint not null,
            avg_total_historical_revenue_by_nested_subcategory bigint null,
            max_total_historical_revenue_by_category bigint not null,
            max_total_historical_revenue_by_subcategory bigint not null,
            max_total_historical_revenue_by_nested_subcategory bigint null,
            id int not null,
            category_id int not null,
            sub_category_id int not null,
            nested_sub_category_id int null,
            global_rank_for_category int not null,
            global_rank_for_subcategory int not null,
            global_rank_for_nested_subcategory int null,
            all_gigs_count_by_category int not null,
            all_gigs_count_by_subcategory int not null,
            all_gigs_count_by_nested_subcategory int null,
            regular_gigs_count_by_category int not null,
            regular_gigs_count_by_subcategory int not null,
            regular_gigs_count_by_nested_subcategory int null,
            pro_gigs_count_by_category int not null,
            pro_gigs_count_by_subcategory int not null,
            pro_gigs_count_by_nested_subcategory int null,
            seller_count_by_category int not null,
            seller_count_by_subcategory int not null,
            seller_count_by_nested_subcategory int null,
            sales_volume_by_category int not null,
            sales_volume_by_subcategory int not null,
            sales_volume_by_nested_subcategory int null,
            total_historical_sales_volume_by_category int not null,
            total_historical_sales_volume_by_subcategory int not null,
            total_historical_sales_volume_by_nested_subcategory int null,
            weighted_avg_gig_price_by_category int not null,
            weighted_avg_gig_price_by_subcategory int not null,
            weighted_avg_gig_price_by_nested_subcategory int null,
            min_active_revenue_by_category int not null,
            min_inactive_revenue_by_category int not null,
            min_revenue_by_category int not null,
            min_active_revenue_by_subcategory int not null,
            min_inactive_revenue_by_subcategory int not null,
            min_revenue_by_subcategory int not null,
            min_active_revenue_by_nested_subcategory int null,
            min_inactive_revenue_by_nested_subcategory int null,
            min_revenue_by_nested_subcategory int null,
            avg_active_revenue_by_category int not null,
            avg_inactive_revenue_by_category int not null,
            avg_revenue_by_category int not null,
            avg_active_revenue_by_subcategory int not null,
            avg_inactive_revenue_by_subcategory int not null,
            avg_revenue_by_subcategory int not null,
            avg_active_revenue_by_nested_subcategory int null,
            avg_inactive_revenue_by_nested_subcategory int null,
            avg_revenue_by_nested_subcategory int null,
            max_active_revenue_by_category int not null,
            max_inactive_revenue_by_category int not null,
            max_revenue_by_category int not null,
            max_active_revenue_by_subcategory int not null,
            max_inactive_revenue_by_subcategory int not null,
            max_revenue_by_subcategory int not null,
            max_active_revenue_by_nested_subcategory int null,
            max_inactive_revenue_by_nested_subcategory int null,
            max_revenue_by_nested_subcategory int null,
            is_trend_valid_for_category bool not null,
            is_trend_valid_for_subcategory bool not null,
            is_trend_valid_for_nested_subcategory bool null,
            sales_volume_growth_percent_by_category numeric not null,
            sales_volume_growth_percent_by_subcategory numeric not null,
            sales_volume_growth_percent_by_nested_subcategory numeric null,
            competition_score_for_category numeric not null,
            competition_score_for_subcategory numeric not null,
            competition_score_for_nested_subcategory numeric null,
            market_share_for_category numeric not null,
            market_share_for_subcategory numeric not null,
            market_share_for_nested_subcategory numeric null,
            revenue_growth_percent_by_category numeric not null,
            revenue_growth_percent_by_subcategory numeric not null,
            revenue_growth_percent_by_nested_subcategory numeric null,
            revenue_trend_by_category numeric not null,
            revenue_trend_by_subcategory numeric not null,
            revenue_trend_by_nested_subcategory numeric null,
            category_name text not null,
            sub_category_name text not null,
            nested_sub_category_name text null
        );

                -- categories

        create table if not exists etl.categories
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            min_total_historical_revenue bigint not null,
            avg_total_historical_revenue bigint not null,
            max_total_historical_revenue bigint not null,
            category_id int not null,
            global_rank int not null,
            all_gigs_count int not null,
            regular_gigs_count int not null,
            pro_gigs_count int not null,
            seller_count int not null,
            sales_volume int not null,
            total_historical_sales_volume int not null,
            weighted_avg_gig_price int not null,
            min_active_revenue int not null,
            min_inactive_revenue int not null,
            min_revenue int not null,
            avg_active_revenue int not null,
            avg_inactive_revenue int not null,
            avg_revenue int not null,
            max_active_revenue int not null,
            max_inactive_revenue int not null,
            max_revenue int not null,
            is_trend_valid bool not null,
            sales_volume_growth_percent numeric not null,
            competition_score numeric not null,
            market_share numeric not null,
            revenue_growth_percent numeric not null,
            revenue_trend numeric not null,
            category_name text not null
        );

                -- subcategories

        create table if not exists etl.subcategories
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            min_total_historical_revenue bigint not null,
            avg_total_historical_revenue bigint not null,
            max_total_historical_revenue bigint not null,
            sub_category_id int not null,
            global_rank int not null,
            all_gigs_count int not null,
            regular_gigs_count int not null,
            pro_gigs_count int not null,
            seller_count int not null,
            sales_volume int not null,
            total_historical_sales_volume int not null,
            weighted_avg_gig_price int not null,
            min_active_revenue int not null,
            min_inactive_revenue int not null,
            min_revenue int not null,
            avg_active_revenue int not null,
            avg_inactive_revenue int not null,
            avg_revenue int not null,
            max_active_revenue int not null,
            max_inactive_revenue int not null,
            max_revenue int not null,
            is_trend_valid bool not null,
            sales_volume_growth_percent numeric not null,
            competition_score numeric not null,
            market_share numeric not null,
            revenue_growth_percent numeric not null,
            revenue_trend numeric not null,
            sub_category_name text not null
        );

                -- nested_subcategories

        create table if not exists etl.nested_subcategories
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            min_total_historical_revenue bigint not null,
            avg_total_historical_revenue bigint not null,
            max_total_historical_revenue bigint not null,
            nested_sub_category_id int not null,
            global_rank int not null,
            all_gigs_count int not null,
            regular_gigs_count int not null,
            pro_gigs_count int not null,
            seller_count int not null,
            sales_volume int not null,
            total_historical_sales_volume int not null,
            weighted_avg_gig_price int not null,
            min_active_revenue int not null,
            min_inactive_revenue int not null,
            min_revenue int not null,
            avg_active_revenue int not null,
            avg_inactive_revenue int not null,
            avg_revenue int not null,
            max_active_revenue int not null,
            max_inactive_revenue int not null,
            max_revenue int not null,
            is_trend_valid bool not null,
            sales_volume_growth_percent numeric not null,
            competition_score numeric not null,
            market_share numeric not null,
            revenue_growth_percent numeric not null,
            revenue_trend numeric not null,
            nested_sub_category_name text not null
        );

                -- category_x_seller_levels

        create table if not exists etl.category_x_seller_levels
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            id int not null,
            category_id int not null,
            seller_count int not null,
            seller_level text not null,
            category_name text not null
        );

                -- subcategory_x_seller_levels

        create table if not exists etl.subcategory_x_seller_levels
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            id int not null,
            sub_category_id int not null,
            seller_count int not null,
            seller_level text not null,
            sub_category_name text not null
        );

                -- nested_subcategory_x_seller_levels

        create table if not exists etl.nested_subcategory_x_seller_levels
        (
            "load_id" bigint not null default to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint,
            processed_dttm timestamptz not null default transaction_timestamp(),
            id int not null,
            nested_sub_category_id int not null,
            seller_count int not null,
            seller_level text not null,
            nested_sub_category_name text not null
        );

            -- Logging

        select
            high_level_step || '.1' as src_step,
            'etl' as tgt_schema,
            null as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (target tables have been created in schema ' || log_rec.tgt_schema || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Exiting

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - run_start_dttm;

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            end_dttm,
            load_id,
            ddl_change_mode,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

    end;

$func$;