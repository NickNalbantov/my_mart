create schema if not exists etl_agg_dev;
create schema if not exists etl_wrk_dev;

create extension if not exists pg_trgm with schema public cascade;

create or replace function etl_agg_dev.agg_etl_sequential
(
    tables_prefix text,
    metrics_start_dttm timestamptz,
    trends_start_dttm timestamptz,
    end_dttm timestamptz,
    trends_intervals smallint default 3,
    trends_students_coeff numeric default 1.96, -- critical value of correlation coefficient for Student's coefficient (by default - confidence interval = 0.95, t-statistics = 1.96)
    ratings_to_volume_coeff numeric default 0.8, -- arbitrary coefficient representing ratio of reviews conversion to sales
    overpriced_coeff numeric default 10, -- crutch to filter gigs with suspiciously high prices (higher than weighted average price in subcategory multiplied by this coefficient)
    competition_score_percentile numeric default 0.95, -- percentile of inversed sales ratio metrics which used for calculations of gigs' competition scores
    competition_score_params jsonb default -- set of parameters for etl_dev.competition_score function for calculating competition_score_xxx attributes
        $js$
            {
                "volume_exp_scale_coeff": 100,
                "sigmoid_scale_coeff": -5,
                "sigmoid_shift_coeff": 0.5,
                "competition_score_scale_coeff": 0.8,
                "competition_score_add_coeff": 0.1,
                "volume_scale_coeff": 0.5
            }
        $js$::jsonb,
    rank_scale_factor numeric default 0.0001, -- scaling coefficient for relevance_sorting function
    debug_mode boolean default false,
    dag_name text default 'manual_launch'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        load_id bigint;

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

        partitioning_step text;
        period_partition_rec record;
        empty_partition_flg boolean;
        root_pkey text;
        partition_pkey text;

        stats_columns_list text[];

        result jsonb;

    begin

        -- Step 1 - Initial checks and preparations

        task_start_dttm := clock_timestamp();
        load_id := to_char(transaction_timestamp(), 'yyyymmddhh24miss')::bigint;

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

            -- Custom exceptions

        if metrics_start_dttm >= end_dttm then
            raise exception 'Value of argument "metrics_start_dttm" should be less than value of argument "end_dttm"'
                using errcode = 'invalid_parameter_value';

        elsif trends_start_dttm >= end_dttm then
            raise exception 'Value of argument "trends_start_dttm" should be less than value of argument "end_dttm"'
                using errcode = 'invalid_parameter_value';

        elsif trends_intervals < 2 then
            raise exception 'Value of argument "trends_intervals" should not be less than 2'
                using errcode = 'invalid_parameter_value';

        elsif trends_intervals > (end_dttm::date - trends_start_dttm::date) then
            raise exception 'Value of argument "trends_intervals" should not be greater than the difference between "end_dttm" and "trends_start_dttm" arguments'' values in days'
                using errcode = 'invalid_parameter_value';

        elsif trends_intervals > 12 then
            raise exception 'No more than 12 trend periods iterations ("trends_intervals") are allowed'
                using errcode = 'invalid_parameter_value';

        elsif trends_students_coeff <= 0 then
            raise exception 't-distribution coefficient ("trends_students_coeff") can''t be <= 0'
                using errcode = 'invalid_parameter_value';

        elsif not ratings_to_volume_coeff <@ '(0, 1]'::numrange then
            raise exception 'Value of argument "ratings_to_volume_coeff" must be in range (0, 1]'
                using errcode = 'invalid_parameter_value';

        elsif not competition_score_percentile <@ '(0, 1)'::numrange then
            raise exception 'Value of argument "competition_score_percentile" must be in range (0, 1)'
                using errcode = 'invalid_parameter_value';

        elsif not (competition_score_params ->> 'sigmoid_shift_coeff')::numeric <@ '[0, 1]'::numrange then
            raise exception 'Value of argument "competition_score_params"[''sigmoid_shift_coeff''] must be in range [0, 1]'
                using errcode = 'invalid_parameter_value';

        elsif not (competition_score_params ->> 'competition_score_scale_coeff')::numeric <@ '[0, 1]'::numrange then
            raise exception 'Value of argument "competition_score_params"[''competition_score_scale_coeff''] must be in range [0, 1]'
                using errcode = 'invalid_parameter_value';

        elsif not (competition_score_params ->> 'competition_score_add_coeff')::numeric <@ '[0, 1]'::numrange then
            raise exception 'Value of argument "competition_score_params"[''competition_score_add_coeff''] must be in range [0, 1]'
                using errcode = 'invalid_parameter_value';

        elsif not (competition_score_params ->> 'volume_scale_coeff')::numeric <@ '[0, 1]'::numrange then
            raise exception 'Value of argument "competition_score_params"[''volume_scale_coeff''] must be in range [0, 1]'
                using errcode = 'invalid_parameter_value';

        elsif tables_prefix != all(array['monthly', 'quarterly', 'yearly']::text[]) then
            raise exception 'Value of argument "tables_prefix" must be one of: ''monthly''::text, ''quarterly''::text, ''yearly''::text'
            using
                errcode = 'invalid_parameter_value';

        elsif extract(day from metrics_start_dttm) != 1 and metrics_start_dttm::timetz != '00:00:00 +0000'::timetz then
            raise exception 'Value of argument "metrics_start_dttm" must be a start of % (at 00:00:00 +0000)',
                replace(tables_prefix, 'ly', '')
            using
                errcode = 'invalid_parameter_value';

        elsif tables_prefix = 'quarterly' and extract(month from metrics_start_dttm) not in (1, 4, 7, 10) then
            raise exception 'Value of argument "metrics_start_dttm" must be a start of quarter (at 00:00:00 +0000)'
            using
                errcode = 'invalid_parameter_value';

        elsif tables_prefix = 'yearly' and extract(month from metrics_start_dttm) != 1 then
            raise exception 'Value of argument "metrics_start_dttm" must be a start of year (at 00:00:00 +0000)'
            using
                errcode = 'invalid_parameter_value';

        end if;

            -- dblink autonomous transaction

        if public.dblink_get_connections() is null or load_id || '_conn' != all(public.dblink_get_connections()) then
            perform public.dblink_connect(load_id || '_conn', 'loopback_dblink_' || current_database());
        else
            raise warning 'Connection % already existed', load_id || '_conn';
            perform public.dblink_disconnect(load_id || '_conn');
            perform public.dblink_connect(load_id || '_conn', 'loopback_dblink_' || current_database());
        end if;

        select
            '1' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            tables_prefix,
            metrics_start_dttm,
            trends_start_dttm,
            end_dttm,
            trends_intervals,
            trends_students_coeff,
            ratings_to_volume_coeff,
            overpriced_coeff,
            competition_score_percentile,
            competition_score_params,
            rank_scale_factor,
            debug_mode,
            dag_name
        );

            -- Interval of partitioning

        select
            case
                when tables_prefix = 'quarterly' then '3 month'
                else '1 ' || replace(tables_prefix, 'ly', '')
            end
        into partitioning_step;

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 2 - Preparing target tables

        task_start_dttm := clock_timestamp();

            -- It is necessary to execute target tables' DDL separately in autonomous transaction because we will need metadata of these tables later for the gathering run statistics
            -- There could be a corner case when target tables weren't created before the run of this function - in this case runs stats wouldn't build up and the job would fail
            -- So, running both of these steps autonomously prevents this problem

        select
            '2' as src_step,
            'etl_agg_dev' as tgt_schema,
            'agg_prepare_targets' as tgt_name,
            'routine call' as op_type
        into log_rec;

        perform etl_agg_dev.agg_prepare_targets(
            tables_prefix,
            end_dttm,
            load_id,
            debug_mode,
            dag_name,
            log_rec.src_step
        );

        log_msg := format($fmt$Step %s completed (successful run of %I.%I(%L, %L, %L, %L, %L, %L))$fmt$,
            log_rec.src_step,
            log_rec.tgt_schema,
            log_rec.tgt_name,
            tables_prefix,
            end_dttm,
            load_id,
            debug_mode,
            dag_name,
            log_rec.src_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 3 - running calculation of metrics

        task_start_dttm := clock_timestamp();

        select
            '3' as src_step,
            'etl_dev' as tgt_schema,
            'metrics_func' as tgt_name,
            'routine call' as op_type
        into log_rec;

        perform etl_dev.metrics_func(
            metrics_start_dttm,
            end_dttm,
            ratings_to_volume_coeff,
            overpriced_coeff,
            competition_score_percentile,
            competition_score_params,
            load_id,
            debug_mode,
            dag_name,
            log_rec.src_step
        );

        log_msg := format($fmt$Step %s completed (successful run of %I.%I(%L, %L, %L, %L, %L, %L, %L, %L, %L, %L))$fmt$,
            log_rec.src_step,
            log_rec.tgt_schema,
            log_rec.tgt_name,
            metrics_start_dttm,
            end_dttm,
            ratings_to_volume_coeff,
            overpriced_coeff,
            competition_score_percentile,
            competition_score_params,
            load_id,
            debug_mode,
            dag_name,
            log_rec.src_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 4 - running calculation of trends

        task_start_dttm := clock_timestamp();

        select
            '4' as src_step,
            'etl_dev' as tgt_schema,
            'trends_func' as tgt_name,
            'routine call' as op_type
        into log_rec;

        perform etl_dev.trends_func(
            trends_start_dttm,
            end_dttm,
            trends_intervals,
            trends_students_coeff,
            ratings_to_volume_coeff,
            load_id,
            debug_mode,
            dag_name,
            log_rec.src_step
        );

        log_msg := format($fmt$Step %s completed (successful run of %I.%I(%L, %L, %L, %L, %L, %L, %L, %L, %L))$fmt$,
            log_rec.src_step,
            log_rec.tgt_schema,
            log_rec.tgt_name,
            trends_start_dttm,
            end_dttm,
            trends_intervals,
            trends_students_coeff,
            ratings_to_volume_coeff,
            load_id,
            debug_mode,
            dag_name,
            log_rec.src_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 5 - All metrics mart

            -- Step 5.1 - Preparing partition and backup for metrics

        task_start_dttm := clock_timestamp();

                -- Getting name of current period partition

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg_dev.' || tables_prefix || '_metrics')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

                -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk_dev.agg_backup_%2$s cascade;

                    -- This backup won't be cleaned up automatically
                        -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk_dev.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk_dev.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk_dev.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            '5.1' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_metrics' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 5.2 - Joining trends into metrics

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                seller_created_at,
                seller_fiverr_created_at,
                seller_scraped_at,
                gig_created_at,
                gig_fiverr_created_at,
                gig_scraped_at,
                agg_period_start,
                seller_id,
                fiverr_seller_id,
                gig_id,
                fiverr_gig_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                all_gigs_count_by_category,
                all_gigs_count_by_subcategory,
                all_gigs_count_by_nested_subcategory,
                regular_gigs_by_category,
                regular_gigs_by_subcategory,
                regular_gigs_by_nested_subcategory,
                pro_gigs_by_category,
                pro_gigs_by_subcategory,
                pro_gigs_by_nested_subcategory,
                seller_count_by_level_and_category,
                seller_count_by_level_and_subcategory,
                seller_count_by_level_and_nested_subcategory,
                gig_count_by_seller,
                seller_ratings_count,
                seller_completed_orders_count,
                active_gigs_ratings_count_by_seller,
                gig_ratings_count,
                volume_by_seller,
                volume_by_gig,
                volume_by_category,
                volume_by_subcategory,
                volume_by_nested_subcategory,
                total_historical_volume_by_seller,
                total_historical_volume_by_gig,
                total_historical_volume_by_category,
                total_historical_volume_by_subcategory,
                total_historical_volume_by_nested_subcategory,
                heuristic,
                seller_is_pro,
                seller_is_active,
                gig_is_pro,
                gig_is_active,
                is_trend_valid_for_seller,
                is_trend_valid_for_gig,
                is_trend_valid_for_category,
                is_trend_valid_for_subcategory,
                is_trend_valid_for_nested_subcategory,
                sales_volume_growth_percent_by_seller,
                sales_volume_growth_percent_by_gig,
                sales_volume_growth_percent_by_category,
                sales_volume_growth_percent_by_subcategory,
                sales_volume_growth_percent_by_nested_subcategory,
                competition_score_for_gig_by_category,
                competition_score_for_gig_by_subcategory,
                competition_score_for_gig_by_nested_subcategory,
                competition_score_for_category,
                competition_score_for_subcategory,
                competition_score_for_nested_subcategory,
                market_share_for_gig_by_category,
                market_share_for_gig_by_subcategory,
                market_share_for_gig_by_nested_subcategory,
                market_share_for_category,
                market_share_for_subcategory,
                market_share_for_nested_subcategory,
                seller_rating,
                gig_rating,
                min_price_by_seller,
                min_price_by_gig,
                min_price_by_category,
                min_price_by_subcategory,
                min_price_by_nested_subcategory,
                avg_price_by_seller,
                avg_price_by_gig,
                avg_price_by_category,
                avg_price_by_subcategory,
                avg_price_by_nested_subcategory,
                max_price_by_seller,
                max_price_by_gig,
                max_price_by_category,
                max_price_by_subcategory,
                max_price_by_nested_subcategory,
                weighted_min_price_by_seller,
                weighted_min_price_by_category,
                weighted_min_price_by_subcategory,
                weighted_min_price_by_nested_subcategory,
                weighted_avg_price_by_seller,
                weighted_avg_price_by_category,
                weighted_avg_price_by_subcategory,
                weighted_avg_price_by_nested_subcategory,
                weighted_max_price_by_seller,
                weighted_max_price_by_category,
                weighted_max_price_by_subcategory,
                weighted_max_price_by_nested_subcategory,
                min_active_revenue_by_seller,
                min_inactive_revenue_by_seller,
                min_total_revenue_by_seller,
                min_revenue_by_gig,
                min_active_revenue_by_category,
                min_inactive_revenue_by_category,
                min_total_revenue_by_category,
                min_active_revenue_by_subcategory,
                min_inactive_revenue_by_subcategory,
                min_total_revenue_by_subcategory,
                min_active_revenue_by_nested_subcategory,
                min_inactive_revenue_by_nested_subcategory,
                min_total_revenue_by_nested_subcategory,
                avg_active_revenue_by_seller,
                avg_inactive_revenue_by_seller,
                avg_total_revenue_by_seller,
                avg_revenue_by_gig,
                avg_active_revenue_by_category,
                avg_inactive_revenue_by_category,
                avg_total_revenue_by_category,
                avg_active_revenue_by_subcategory,
                avg_inactive_revenue_by_subcategory,
                avg_total_revenue_by_subcategory,
                avg_active_revenue_by_nested_subcategory,
                avg_inactive_revenue_by_nested_subcategory,
                avg_total_revenue_by_nested_subcategory,
                max_active_revenue_by_seller,
                max_inactive_revenue_by_seller,
                max_total_revenue_by_seller,
                max_revenue_by_gig,
                max_active_revenue_by_category,
                max_inactive_revenue_by_category,
                max_total_revenue_by_category,
                max_active_revenue_by_subcategory,
                max_inactive_revenue_by_subcategory,
                max_total_revenue_by_subcategory,
                max_active_revenue_by_nested_subcategory,
                max_inactive_revenue_by_nested_subcategory,
                max_total_revenue_by_nested_subcategory,
                min_total_historical_revenue_by_seller,
                min_total_historical_revenue_by_gig,
                min_total_historical_revenue_by_category,
                min_total_historical_revenue_by_subcategory,
                min_total_historical_revenue_by_nested_subcategory,
                avg_total_historical_revenue_by_seller,
                avg_total_historical_revenue_by_gig,
                avg_total_historical_revenue_by_category,
                avg_total_historical_revenue_by_subcategory,
                avg_total_historical_revenue_by_nested_subcategory,
                max_total_historical_revenue_by_seller,
                max_total_historical_revenue_by_gig,
                max_total_historical_revenue_by_category,
                max_total_historical_revenue_by_subcategory,
                max_total_historical_revenue_by_nested_subcategory,
                revenue_growth_percent_by_seller,
                revenue_growth_percent_by_gig,
                revenue_growth_percent_by_category,
                revenue_growth_percent_by_subcategory,
                revenue_growth_percent_by_nested_subcategory,
                trend_by_seller,
                r2_by_seller,
                trends_avg_revenue_by_seller,
                trend_by_gig,
                r2_by_gig,
                trends_avg_revenue_by_gig,
                trend_by_category,
                r2_by_category,
                trends_avg_revenue_by_category,
                trend_by_subcategory,
                r2_by_subcategory,
                trends_avg_revenue_by_subcategory,
                trend_by_nested_subcategory,
                r2_by_nested_subcategory,
                trends_avg_revenue_by_nested_subcategory,
                rcrit,
                seller_level,
                seller_name,
                agency_slug,
                agency_status,
                gig_title,
                gig_cached_slug,
                category_name,
                sub_category_name,
                nested_sub_category_name,
                seller_country,
                seller_country_code,
                seller_languages,
                seller_profile_image,
                gig_preview_url
            )

            select
                %3$L::bigint as "load_id",
                transaction_timestamp() as processed_dttm,
                m.seller_created_at,
                m.seller_fiverr_created_at,
                m.seller_scraped_at,
                m.gig_created_at,
                m.gig_fiverr_created_at,
                m.gig_scraped_at,
                %4$L::date as agg_period_start,
                m.seller_id,
                m.fiverr_seller_id,
                m.gig_id,
                m.fiverr_gig_id,
                m.category_id,
                m.sub_category_id,
                m.nested_sub_category_id,
                m.all_gigs_count_by_category,
                m.all_gigs_count_by_subcategory,
                m.all_gigs_count_by_nested_subcategory,
                m.regular_gigs_by_category,
                m.regular_gigs_by_subcategory,
                m.regular_gigs_by_nested_subcategory,
                m.pro_gigs_by_category,
                m.pro_gigs_by_subcategory,
                m.pro_gigs_by_nested_subcategory,
                m.seller_count_by_level_and_category,
                m.seller_count_by_level_and_subcategory,
                m.seller_count_by_level_and_nested_subcategory,
                m.gig_count_by_seller,
                m.seller_ratings_count,
                m.seller_completed_orders_count,
                m.active_gigs_ratings_count_by_seller,
                m.gig_ratings_count,
                m.volume_by_seller,
                m.volume_by_gig,
                m.volume_by_category,
                m.volume_by_subcategory,
                m.volume_by_nested_subcategory,
                m.total_historical_volume_by_seller,
                m.total_historical_volume_by_gig,
                m.total_historical_volume_by_category,
                m.total_historical_volume_by_subcategory,
                m.total_historical_volume_by_nested_subcategory,
                m.heuristic,
                m.seller_is_pro,
                m.seller_is_active,
                m.gig_is_pro,
                m.gig_is_active,
                case
                    when m.fiverr_seller_id = t.fiverr_seller_id
                        then sqrt(t.r2_by_seller) > t.rcrit
                    else
                        (max((sqrt(t.r2_by_seller) > t.rcrit)::int) filter (where m.fiverr_seller_id = t.fiverr_seller_id) over w_s)::boolean
                end as is_trend_valid_for_seller,
                sqrt(t.r2_by_gig) > t.rcrit as is_trend_valid_for_gig,
                case
                    when m.category_id = t.category_id
                        then sqrt(t.r2_by_category) > t.rcrit
                    else
                        (max((sqrt(t.r2_by_category) > t.rcrit)::int) filter (where m.category_id = t.category_id) over w_cat)::boolean
                end as is_trend_valid_for_category,
                case
                    when m.sub_category_id = t.sub_category_id
                        then sqrt(t.r2_by_subcategory) > t.rcrit
                    else
                        (max((sqrt(t.r2_by_subcategory) > t.rcrit)::int) filter (where m.sub_category_id = t.sub_category_id) over w_subcat)::boolean
                end as is_trend_valid_for_subcategory,
                case
                    when m.nested_sub_category_id is null then null
                    else
                        case
                            when m.nested_sub_category_id = t.nested_sub_category_id
                                then sqrt(t.r2_by_nested_subcategory) > t.rcrit
                            else
                                (max((sqrt(t.r2_by_nested_subcategory) > t.rcrit)::int) filter (where m.nested_sub_category_id = t.nested_sub_category_id) over w_nest_subcat)::boolean
                        end
                end as is_trend_valid_for_nested_subcategory,
                m.sales_volume_growth_percent_by_seller,
                m.sales_volume_growth_percent_by_gig,
                m.sales_volume_growth_percent_by_category,
                m.sales_volume_growth_percent_by_subcategory,
                m.sales_volume_growth_percent_by_nested_subcategory,
                m.competition_score_for_gig_by_category,
                m.competition_score_for_gig_by_subcategory,
                m.competition_score_for_gig_by_nested_subcategory,
                m.competition_score_for_category,
                m.competition_score_for_subcategory,
                m.competition_score_for_nested_subcategory,
                m.market_share_for_gig_by_category,
                m.market_share_for_gig_by_subcategory,
                m.market_share_for_gig_by_nested_subcategory,
                m.market_share_for_category,
                m.market_share_for_subcategory,
                m.market_share_for_nested_subcategory,
                m.seller_rating,
                m.gig_rating,
                m.min_price_by_seller,
                m.min_price_by_gig,
                m.min_price_by_category,
                m.min_price_by_subcategory,
                m.min_price_by_nested_subcategory,
                m.avg_price_by_seller,
                m.avg_price_by_gig,
                m.avg_price_by_category,
                m.avg_price_by_subcategory,
                m.avg_price_by_nested_subcategory,
                m.max_price_by_seller,
                m.max_price_by_gig,
                m.max_price_by_category,
                m.max_price_by_subcategory,
                m.max_price_by_nested_subcategory,
                m.weighted_min_price_by_seller,
                m.weighted_min_price_by_category,
                m.weighted_min_price_by_subcategory,
                m.weighted_min_price_by_nested_subcategory,
                m.weighted_avg_price_by_seller,
                m.weighted_avg_price_by_category,
                m.weighted_avg_price_by_subcategory,
                m.weighted_avg_price_by_nested_subcategory,
                m.weighted_max_price_by_seller,
                m.weighted_max_price_by_category,
                m.weighted_max_price_by_subcategory,
                m.weighted_max_price_by_nested_subcategory,
                m.min_active_revenue_by_seller,
                m.min_inactive_revenue_by_seller,
                m.min_active_revenue_by_seller + m.min_inactive_revenue_by_seller as min_total_revenue_by_seller,
                m.min_revenue_by_gig,
                m.min_active_revenue_by_category,
                m.min_inactive_revenue_by_category,
                m.min_active_revenue_by_category + m.min_inactive_revenue_by_category as min_total_revenue_by_category,
                m.min_active_revenue_by_subcategory,
                m.min_inactive_revenue_by_subcategory,
                m.min_active_revenue_by_subcategory + m.min_inactive_revenue_by_subcategory as min_total_revenue_by_subcategory,
                m.min_active_revenue_by_nested_subcategory,
                m.min_inactive_revenue_by_nested_subcategory,
                m.min_active_revenue_by_nested_subcategory + m.min_inactive_revenue_by_nested_subcategory as min_total_revenue_by_nested_subcategory,
                m.avg_active_revenue_by_seller,
                m.avg_inactive_revenue_by_seller,
                m.avg_active_revenue_by_seller + m.avg_inactive_revenue_by_seller as avg_total_revenue_by_seller,
                m.avg_revenue_by_gig,
                m.avg_active_revenue_by_category,
                m.avg_inactive_revenue_by_category,
                m.avg_active_revenue_by_category + m.avg_inactive_revenue_by_category as avg_total_revenue_by_category,
                m.avg_active_revenue_by_subcategory,
                m.avg_inactive_revenue_by_subcategory,
                m.avg_active_revenue_by_subcategory + m.avg_inactive_revenue_by_subcategory as avg_total_revenue_by_subcategory,
                m.avg_active_revenue_by_nested_subcategory,
                m.avg_inactive_revenue_by_nested_subcategory,
                m.avg_active_revenue_by_nested_subcategory + m.avg_inactive_revenue_by_nested_subcategory as avg_total_revenue_by_nested_subcategory,
                m.max_active_revenue_by_seller,
                m.max_inactive_revenue_by_seller,
                m.max_active_revenue_by_seller + m.max_inactive_revenue_by_seller as max_total_revenue_by_seller,
                m.max_revenue_by_gig,
                m.max_active_revenue_by_category,
                m.max_inactive_revenue_by_category,
                m.max_active_revenue_by_category + m.max_inactive_revenue_by_category as max_total_revenue_by_category,
                m.max_active_revenue_by_subcategory,
                m.max_inactive_revenue_by_subcategory,
                m.max_active_revenue_by_subcategory + m.max_inactive_revenue_by_subcategory as max_total_revenue_by_subcategory,
                m.max_active_revenue_by_nested_subcategory,
                m.max_inactive_revenue_by_nested_subcategory,
                m.max_active_revenue_by_nested_subcategory + m.max_inactive_revenue_by_nested_subcategory as max_total_revenue_by_nested_subcategory,
                m.min_total_historical_revenue_by_seller,
                m.min_total_historical_revenue_by_gig,
                m.min_total_historical_revenue_by_category,
                m.min_total_historical_revenue_by_subcategory,
                m.min_total_historical_revenue_by_nested_subcategory,
                m.avg_total_historical_revenue_by_seller,
                m.avg_total_historical_revenue_by_gig,
                m.avg_total_historical_revenue_by_category,
                m.avg_total_historical_revenue_by_subcategory,
                m.avg_total_historical_revenue_by_nested_subcategory,
                m.max_total_historical_revenue_by_seller,
                m.max_total_historical_revenue_by_gig,
                m.max_total_historical_revenue_by_category,
                m.max_total_historical_revenue_by_subcategory,
                m.max_total_historical_revenue_by_nested_subcategory,
                coalesce
                    (
                        round(m.min_active_revenue_by_seller + m.min_inactive_revenue_by_seller)::numeric
                        / nullif
                            (
                                round(m.min_total_historical_revenue_by_seller) - round(least((m.min_active_revenue_by_seller + m.min_inactive_revenue_by_seller), m.min_total_historical_revenue_by_seller)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as revenue_growth_percent_by_seller,
                coalesce
                    (
                        round(m.min_revenue_by_gig)::numeric
                        / nullif
                            (
                                round(m.min_total_historical_revenue_by_gig) - round(least(m.min_revenue_by_gig, m.min_total_historical_revenue_by_gig)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as revenue_growth_percent_by_gig,
                coalesce
                    (
                        round(m.min_active_revenue_by_category + m.min_inactive_revenue_by_category)::numeric
                        / nullif
                            (
                               round(m.min_total_historical_revenue_by_category) - round(least((m.min_active_revenue_by_category + m.min_inactive_revenue_by_category), m.min_total_historical_revenue_by_category)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as revenue_growth_percent_by_category,
                coalesce
                    (
                        round(m.min_active_revenue_by_subcategory + m.min_inactive_revenue_by_subcategory)::numeric
                        / nullif
                            (
                                round(m.min_total_historical_revenue_by_subcategory) - round(least((m.min_active_revenue_by_subcategory + m.min_inactive_revenue_by_subcategory), m.min_total_historical_revenue_by_subcategory)),
                                0
                            )::numeric,
                        0::numeric
                    ) * 100 as revenue_growth_percent_by_subcategory,
                case
                    when m.nested_sub_category_id is null then null
                    else coalesce
                        (
                            round(m.min_active_revenue_by_nested_subcategory + m.min_inactive_revenue_by_nested_subcategory)::numeric
                            / nullif
                                (
                                    round(m.min_total_historical_revenue_by_nested_subcategory) - round(least((m.min_active_revenue_by_nested_subcategory + m.min_inactive_revenue_by_nested_subcategory), m.min_total_historical_revenue_by_nested_subcategory)),
                                    0
                                )::numeric,
                            0::numeric
                        ) * 100
                end as revenue_growth_percent_by_nested_subcategory,
                case
                    when m.fiverr_seller_id = t.fiverr_seller_id
                        then t.trend_by_seller
                    else
                        max(t.trend_by_seller) filter (where m.fiverr_seller_id = t.fiverr_seller_id) over w_s
                end as trend_by_seller,
                case
                    when m.fiverr_seller_id = t.fiverr_seller_id
                        then t.r2_by_seller
                    else
                        max(t.r2_by_seller) filter (where m.fiverr_seller_id = t.fiverr_seller_id) over w_s
                end as r2_by_seller,
                case
                    when m.fiverr_seller_id = t.fiverr_seller_id
                        then t.full_avg_revenue_by_seller
                    else
                        max(t.full_avg_revenue_by_seller) filter (where m.fiverr_seller_id = t.fiverr_seller_id) over w_s
                end as trends_avg_revenue_by_seller,
                t.trend_by_gig,
                t.r2_by_gig,
                t.full_avg_revenue_by_gig as trends_avg_revenue_by_gig,
                case
                    when m.category_id = t.category_id
                        then t.trend_by_category
                    else
                        max(t.trend_by_category) filter (where m.category_id = t.category_id) over w_cat
                end as trend_by_category,
                case
                    when m.category_id = t.category_id
                        then t.r2_by_category
                    else
                        max(t.r2_by_category) filter (where m.category_id = t.category_id) over w_cat
                end as r2_by_category,
                case
                    when m.category_id = t.category_id
                        then t.full_avg_revenue_by_category
                    else
                        max(t.full_avg_revenue_by_category) filter (where m.category_id = t.category_id) over w_cat
                end as trends_avg_revenue_by_category,
                case
                    when m.sub_category_id = t.sub_category_id
                        then t.trend_by_subcategory
                    else
                        max(t.trend_by_subcategory) filter (where m.sub_category_id = t.sub_category_id) over w_subcat
                end as trend_by_subcategory,
                case
                    when m.sub_category_id = t.sub_category_id
                        then t.r2_by_subcategory
                    else
                        max(t.r2_by_subcategory) filter (where m.sub_category_id = t.sub_category_id) over w_subcat
                end as r2_by_subcategory,
                case
                    when m.sub_category_id = t.sub_category_id
                        then t.full_avg_revenue_by_subcategory
                    else
                        max(t.full_avg_revenue_by_subcategory) filter (where m.sub_category_id = t.sub_category_id) over w_subcat
                end as trends_avg_revenue_by_subcategory,
                case
                    when m.nested_sub_category_id is null then null
                    else
                        case
                            when m.nested_sub_category_id = t.nested_sub_category_id
                                then t.trend_by_nested_subcategory
                            else
                                max(t.trend_by_nested_subcategory) filter (where m.nested_sub_category_id = t.nested_sub_category_id) over w_nest_subcat
                        end
                end as trend_by_nested_subcategory,
                case
                    when m.nested_sub_category_id is null then null
                    else
                        case
                            when m.nested_sub_category_id = t.nested_sub_category_id
                                then t.r2_by_nested_subcategory
                            else
                                max(t.r2_by_nested_subcategory) filter (where m.nested_sub_category_id = t.nested_sub_category_id) over w_nest_subcat
                        end
                end as r2_by_nested_subcategory,
                case
                    when m.nested_sub_category_id is null then null
                    else
                        case
                            when m.nested_sub_category_id = t.nested_sub_category_id
                                then t.full_avg_revenue_by_nested_subcategory
                            else
                                max(t.full_avg_revenue_by_nested_subcategory) filter (where m.nested_sub_category_id = t.nested_sub_category_id) over w_nest_subcat
                        end
                end as trends_avg_revenue_by_nested_subcategory,
                t.rcrit,
                m.seller_level,
                m.seller_name,
                m.agency_slug,
                m.agency_status,
                m.gig_title,
                m.gig_cached_slug,
                m.category_name,
                m.sub_category_name,
                m.nested_sub_category_name,
                m.seller_country,
                m.seller_country_code,
                m.seller_languages,
                m.seller_profile_image,
                m.gig_preview_url
            from etl_wrk_dev.metrics_prefin m
            join etl_wrk_dev.trends t
                on m.gig_id = t.gig_id
            window
                w_s as (partition by m.fiverr_seller_id),
                w_cat as (partition by m.category_id),
                w_subcat as (partition by m.sub_category_id),
                w_nest_subcat as (partition by m.nested_sub_category_id);

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt = row_count;

                -- Maintenance

                    -- Indices

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg_dev.' || tables_prefix || '_metrics')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg_dev.%1$s_metrics
                    add primary key (agg_period_start, gig_id) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create index if not exists idx_seller_id_%1$s_metrics
                on etl_agg_dev.%1$s_metrics (agg_period_start, seller_id) with (fillfactor = 100);
            create index if not exists idx_category_id_%1$s_metrics
                on etl_agg_dev.%1$s_metrics (agg_period_start, category_id) with (fillfactor = 100);
            create index if not exists idx_sub_category_id_%1$s_metrics
                on etl_agg_dev.%1$s_metrics (agg_period_start, sub_category_id) with (fillfactor = 100);
            create index if not exists idx_nested_sub_category_id_%1$s_metrics
                on etl_agg_dev.%1$s_metrics (agg_period_start, nested_sub_category_id) with (fillfactor = 100);
            create index if not exists idx_categories_%1$s_metrics
                on etl_agg_dev.%1$s_metrics (agg_period_start, category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);

            alter table if exists etl_agg_dev.%1$s_metrics
                replica identity using index %1$s_metrics_pkey;

        $exec$,
        tables_prefix
        );

                    -- CLUSTER

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

                    -- CREATE STATISTICS and ANALYZE

        stats_columns_list := array[
                'gig_id', 'seller_id', 'fiverr_gig_id', 'fiverr_seller_id',
                'category_id', 'sub_category_id', 'nested_sub_category_id'
            ];

        execute format($exec$
            create statistics if not exists %1$I.%2$s
            on %3$s
            from %1$I.%4$I;
        $exec$,
        period_partition_rec.part_schema,
        substring('stats_' || period_partition_rec.part_name, 1, 63),
        array_to_string(stats_columns_list, ', '),
        period_partition_rec.part_name
        );

        execute format($exec$
            analyze etl_agg_dev.%1$s_metrics;
        $exec$,
        tables_prefix
        );

        if debug_mode is false then
            drop table if exists etl_wrk_dev.metrics_prefin cascade;
            drop table if exists etl_wrk_dev.trends cascade;
        end if;

        select
            '5.2' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_metrics' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 5.3 - Calculating standart deviation and average for gigs' and sellers' ratings

        task_start_dttm := clock_timestamp();

        execute format($exec$

            drop table if exists etl_wrk_dev.metrics_ratings_stats cascade;

            create unlogged table if not exists etl_wrk_dev.metrics_ratings_stats
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    avg(gig_rating)
                        filter (where gig_rating != 0::int and gig_ratings_count != 0::int) as avg_gig_rating,
                    stddev_pop(gig_rating)
                        filter (where gig_rating != 0::int and gig_ratings_count != 0::int) as stddev_gig_rating,
                    avg(seller_rating)
                        filter (where seller_rating != 0::int and active_gigs_ratings_count_by_seller != 0::int) as avg_seller_rating,
                    stddev_pop(seller_rating)
                        filter (where seller_rating != 0::int and active_gigs_ratings_count_by_seller != 0::int) as stddev_seller_rating
                from etl_agg_dev.%1$s_metrics
                where
                    agg_period_start = %2$L::date;

        $exec$,
        tables_prefix,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt = row_count;

        analyze etl_wrk_dev.metrics_ratings_stats;

        select
            '5.3' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'metrics_ratings_stats' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 6 - Materializing gigs metrics

            -- Step 6.1 - Gathering gigs' attributes

        task_start_dttm := clock_timestamp();

        execute format($exec$

            drop table if exists etl_wrk_dev.gigs_attributes cascade;

            create unlogged table if not exists etl_wrk_dev.gigs_attributes
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    g.seller_created_at,
                    g.seller_fiverr_created_at,
                    g.seller_scraped_at,
                    g.gig_created_at,
                    g.gig_fiverr_created_at,
                    g.gig_scraped_at,
                    g.seller_id,
                    g.fiverr_seller_id,
                    g.gig_id,
                    g.fiverr_gig_id,
                    g.category_id,
                    g.sub_category_id,
                    g.nested_sub_category_id,
                    g.seller_ratings_count,
                    g.gig_ratings_count as ratings_count,
                    g.volume_by_gig as sales_volume,
                    g.total_historical_volume_by_gig as total_historical_sales_volume,
                    g.heuristic,
                    g.gig_is_pro as is_pro,
                    g.gig_is_active as is_active,
                    g.is_trend_valid_for_gig as is_trend_valid,
                    g.sales_volume_growth_percent_by_gig as sales_volume_growth_percent,
                    g.competition_score_for_gig_by_category,
                    g.competition_score_for_gig_by_subcategory,
                    g.competition_score_for_gig_by_nested_subcategory,
                    g.market_share_for_gig_by_category,
                    g.market_share_for_gig_by_subcategory,
                    g.market_share_for_gig_by_nested_subcategory,
                    g.seller_rating,
                    g.gig_rating as rating,
                    g.min_price_by_gig as min_price,
                    g.avg_price_by_gig as avg_price,
                    g.max_price_by_gig as max_price,
                    g.min_revenue_by_gig as min_revenue,
                    g.avg_revenue_by_gig as avg_revenue,
                    g.max_revenue_by_gig as max_revenue,
                    g.min_total_historical_revenue_by_gig as min_total_historical_revenue,
                    g.avg_total_historical_revenue_by_gig as avg_total_historical_revenue,
                    g.max_total_historical_revenue_by_gig as max_total_historical_revenue,
                    g.revenue_growth_percent_by_gig as revenue_growth_percent,
                    g.trend_by_gig as revenue_trend,
                    etl_dev.relevance_sorting(
                            g.min_revenue_by_gig,
                            coalesce(g.gig_fiverr_created_at, g.gig_scraped_at),
                            %2$L::timestamptz,
                            %3$L::numeric
                        ) as coeff_revenue,
                    etl_dev.relevance_sorting(
                            g.trend_by_gig,
                            coalesce(g.gig_fiverr_created_at, g.gig_scraped_at),
                            %2$L::timestamptz,
                            %3$L::numeric
                        ) as coeff_trend,
                    etl_dev.relevance_sorting(
                            100 * coalesce((g.gig_rating * g.gig_ratings_count + r.avg_gig_rating * r.stddev_gig_rating)::numeric
                                / nullif((g.gig_ratings_count + r.stddev_gig_rating), 0)::numeric, 0::numeric),
                            coalesce(g.gig_fiverr_created_at, g.gig_scraped_at),
                            %2$L::timestamptz,
                            %3$L::numeric
                        ) as coeff_rating,
                    g.seller_level,
                    g.seller_name,
                    case
                        when g.agency_slug is null then 'I will ' || g.gig_title
                        else 'Our agency will ' || g.gig_title
                    end as title,
                    g.gig_cached_slug as cached_slug,
                    g.category_name,
                    g.sub_category_name,
                    g.nested_sub_category_name,
                    g.seller_country,
                    g.seller_country_code,
                    g.gig_preview_url as image_preview,
                    g.seller_profile_image
                from etl_agg_dev.%1$s_metrics g
                cross join etl_wrk_dev.metrics_ratings_stats as r
                where
                    g.agg_period_start = %4$L::date;

        $exec$,
        tables_prefix,
        end_dttm,
        rank_scale_factor,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt := row_count;

        analyze etl_wrk_dev.gigs_attributes;

        select
            '6.1' as src_step,
            'etl_agg_dev' as tgt_schema,
            'gigs_attributes' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 6.2 - Preparing partition and backup for gigs

        task_start_dttm := clock_timestamp();

                -- Getting name of current period partition

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg_dev.' || tables_prefix || '_gigs')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

                -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk_dev.agg_backup_%2$s cascade;

                    -- This backup won't be cleaned up automatically
                        -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk_dev.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk_dev.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk_dev.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            '6.2' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_gigs' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 6.3 - Materializing gigs metrics

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                seller_created_at,
                seller_fiverr_created_at,
                seller_scraped_at,
                gig_created_at,
                gig_fiverr_created_at,
                gig_scraped_at,
                min_revenue,
                avg_revenue,
                max_revenue,
                min_total_historical_revenue,
                avg_total_historical_revenue,
                max_total_historical_revenue,
                agg_period_start,
                seller_id,
                fiverr_seller_id,
                gig_id,
                fiverr_gig_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                global_rank,
                seller_ratings_count,
                ratings_count,
                sales_volume,
                total_historical_sales_volume,
                min_price,
                avg_price,
                max_price,
                heuristic,
                is_pro,
                is_active,
                is_trend_valid,
                sales_volume_growth_percent,
                competition_score_for_gig_by_category,
                competition_score_for_gig_by_subcategory,
                competition_score_for_gig_by_nested_subcategory,
                market_share_for_gig_by_category,
                market_share_for_gig_by_subcategory,
                market_share_for_gig_by_nested_subcategory,
                seller_rating,
                rating,
                revenue_growth_percent,
                revenue_trend,
                seller_level,
                seller_name,
                title,
                cached_slug,
                category_name,
                sub_category_name,
                nested_sub_category_name,
                seller_country,
                seller_country_code,
                image_preview,
                seller_profile_image
            )

                select
                    %3$L::bigint as "load_id",
                    transaction_timestamp() as processed_dttm,
                    seller_created_at,
                    seller_fiverr_created_at,
                    seller_scraped_at,
                    gig_created_at,
                    gig_fiverr_created_at,
                    gig_scraped_at,
                    round(min_revenue)::bigint as min_revenue,
                    round(avg_revenue)::bigint as avg_revenue,
                    round(max_revenue)::bigint as max_revenue,
                    round(min_total_historical_revenue)::bigint as min_total_historical_revenue,
                    round(avg_total_historical_revenue)::bigint as avg_total_historical_revenue,
                    round(max_total_historical_revenue)::bigint as max_total_historical_revenue,
                    %4$L::date as agg_period_start,
                    seller_id,
                    fiverr_seller_id,
                    gig_id,
                    fiverr_gig_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    (dense_rank() over (order by
                        is_active desc nulls last, -- true first
                        coeff_revenue desc nulls last, -- bigger revenue first
                        coeff_trend desc nulls last, -- bigger trend growth first
                        coeff_rating desc nulls last, -- bigger rating first
                        coalesce(gig_fiverr_created_at, gig_scraped_at) asc nulls last, -- oldest first
                        coalesce(seller_fiverr_created_at, seller_scraped_at) asc nulls last, -- oldest first
                        gig_id asc nulls last -- oldest first
                    ))::int as global_rank,
                    seller_ratings_count,
                    ratings_count,
                    sales_volume,
                    total_historical_sales_volume,
                    round(min_price)::int as min_price,
                    round(avg_price)::int as avg_price,
                    round(max_price)::int as max_price,
                    heuristic,
                    is_pro,
                    is_active,
                    is_trend_valid,
                    round(sales_volume_growth_percent, 2) as sales_volume_growth_percent,
                    round(competition_score_for_gig_by_category, 2) as competition_score_for_gig_by_category,
                    round(competition_score_for_gig_by_subcategory, 2) as competition_score_for_gig_by_subcategory,
                    round(competition_score_for_gig_by_nested_subcategory, 2) as competition_score_for_gig_by_nested_subcategory,
                    round(market_share_for_gig_by_category, 2) as market_share_for_gig_by_category,
                    round(market_share_for_gig_by_subcategory, 2) as market_share_for_gig_by_subcategory,
                    round(market_share_for_gig_by_nested_subcategory, 2) as market_share_for_gig_by_nested_subcategory,
                    round(seller_rating, 2) as seller_rating,
                    round(rating, 2) as rating,
                    round(revenue_growth_percent, 2) as revenue_growth_percent,
                    round(revenue_trend, 2) as revenue_trend,
                    seller_level,
                    seller_name,
                    title,
                    cached_slug,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name,
                    seller_country,
                    seller_country_code,
                    image_preview,
                    seller_profile_image
                from etl_wrk_dev.gigs_attributes;

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt = row_count;

                -- Maintenance

                    -- Indices

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg_dev.' || tables_prefix || '_gigs')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg_dev.%1$s_gigs
                    add primary key (agg_period_start, gig_id) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create unique index if not exists uidx_fiverr_gig_id_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, fiverr_gig_id) with (fillfactor = 100);
            create index if not exists idx_seller_id_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, seller_id) with (fillfactor = 100);
            create index if not exists idx_fiverr_seller_id_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, fiverr_seller_id) with (fillfactor = 100);
            create index if not exists idx_category_id_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, category_id) with (fillfactor = 100);
            create index if not exists idx_sub_category_id_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, sub_category_id) with (fillfactor = 100);
            create index if not exists idx_nested_sub_category_id_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, nested_sub_category_id) with (fillfactor = 100);
            create index if not exists idx_categories_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);
            create unique index if not exists uidx_%1$s_gigs_ranks
                on etl_agg_dev.%1$s_gigs (agg_period_start, global_rank desc) with (fillfactor = 100);
            create index if not exists idx_cached_slug_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, cached_slug) with (fillfactor = 100);
            create index if not exists idx_trg_title_%1$s_gigs
                on etl_agg_dev.%1$s_gigs using gin (title gin_trgm_ops);
            create index if not exists idx_trg_seller_name_%1$s_gigs
                on etl_agg_dev.%1$s_gigs using gin (seller_name gin_trgm_ops);
            create index if not exists idx_trg_cat_name_%1$s_gigs
                on etl_agg_dev.%1$s_gigs using gin (category_name gin_trgm_ops);
            create index if not exists idx_trg_subcat_name_%1$s_gigs
                on etl_agg_dev.%1$s_gigs using gin (sub_category_name gin_trgm_ops);
            create index if not exists idx_trg_nest_subcat_name_%1$s_gigs
                on etl_agg_dev.%1$s_gigs using gin (nested_sub_category_name gin_trgm_ops);

            alter table if exists etl_agg_dev.%1$s_gigs
                replica identity using index %1$s_gigs_pkey;

        $exec$,
        tables_prefix
        );

                    -- CLUSTER

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

                    -- CREATE STATISTICS and ANALYZE

        stats_columns_list := array[
                'gig_id', 'seller_id', 'fiverr_gig_id', 'fiverr_seller_id',
                'category_id', 'sub_category_id', 'nested_sub_category_id'
            ];

        execute format($exec$
            create statistics if not exists %1$I.%2$s
            on %3$s
            from %1$I.%4$I;
        $exec$,
        period_partition_rec.part_schema,
        substring('stats_' || period_partition_rec.part_name, 1, 63),
        array_to_string(stats_columns_list, ', '),
        period_partition_rec.part_name
        );

        execute format($exec$
            analyze etl_agg_dev.%1$s_gigs;
        $exec$,
        tables_prefix
        );

        if debug_mode is false then
            drop table if exists etl_wrk_dev.gigs_attributes cascade;
        end if;

        select
            '6.3' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_gigs' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 7 - Materializing sellers metrics

            -- Step 7.1 - Gathering sellers' attributes

        task_start_dttm := clock_timestamp();

        execute format($exec$

            drop table if exists etl_wrk_dev.sellers_attributes cascade;

            create unlogged table if not exists etl_wrk_dev.sellers_attributes
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select distinct
                    s.seller_created_at as created_at,
                    s.seller_fiverr_created_at as fiverr_created_at,
                    s.seller_scraped_at as scraped_at,
                    s.seller_id,
                    s.fiverr_seller_id,
                    first_value(s.gig_id) over w_s as best_selling_gig_id,
                    first_value(s.fiverr_gig_id) over w_s as best_selling_fiverr_gig_id,
                    s.gig_count_by_seller,
                    s.seller_ratings_count as ratings_count,
                    s.seller_completed_orders_count as completed_orders_count,
                    s.active_gigs_ratings_count_by_seller as active_gigs_ratings_count,
                    s.volume_by_seller as sales_volume,
                    s.total_historical_volume_by_seller as total_historical_sales_volume,
                    first_value(s.total_historical_volume_by_gig) over w_s as best_selling_gig_total_historical_volume,
                    s.seller_is_pro as is_pro,
                    s.seller_is_active as is_active,
                    s.is_trend_valid_for_seller as is_trend_valid,
                    s.sales_volume_growth_percent_by_seller as sales_volume_growth_percent,
                    s.seller_rating as rating,
                    s.weighted_avg_price_by_seller as weighted_avg_gig_price,
                    s.min_active_revenue_by_seller as min_active_revenue,
                    s.min_inactive_revenue_by_seller as min_inactive_revenue,
                    s.min_total_revenue_by_seller as min_total_revenue,
                    s.avg_active_revenue_by_seller as avg_active_revenue,
                    s.avg_inactive_revenue_by_seller as avg_inactive_revenue,
                    s.avg_total_revenue_by_seller as avg_total_revenue,
                    s.max_active_revenue_by_seller as max_active_revenue,
                    s.max_inactive_revenue_by_seller as max_inactive_revenue,
                    s.max_total_revenue_by_seller as max_total_revenue,
                    s.min_total_historical_revenue_by_seller as min_total_historical_revenue,
                    s.avg_total_historical_revenue_by_seller as avg_total_historical_revenue,
                    s.max_total_historical_revenue_by_seller as max_total_historical_revenue,
                    coalesce
                        (
                            first_value(s.min_total_historical_revenue_by_gig) over w_s,
                            0::numeric
                        ) as best_selling_gig_revenue,
                    s.revenue_growth_percent_by_seller as revenue_growth_percent,
                    s.trend_by_seller as revenue_trend,
                    etl_dev.relevance_sorting(
                            s.min_active_revenue_by_seller,
                            coalesce(s.seller_fiverr_created_at, s.seller_scraped_at),
                            %2$L::timestamptz,
                            %3$L::numeric
                        ) as coeff_revenue,
                    etl_dev.relevance_sorting(
                            s.trend_by_seller,
                            coalesce(s.seller_fiverr_created_at, s.seller_scraped_at),
                            %2$L::timestamptz,
                            %3$L::numeric
                        ) as coeff_trend,
                    etl_dev.relevance_sorting(
                            100 * coalesce((s.seller_rating * s.active_gigs_ratings_count_by_seller + r.avg_seller_rating * r.stddev_seller_rating)::numeric
                                / nullif((s.active_gigs_ratings_count_by_seller + r.stddev_seller_rating), 0)::numeric, 0::numeric),
                            coalesce(s.seller_fiverr_created_at, s.seller_scraped_at),
                            %2$L::timestamptz,
                            %3$L::numeric
                        ) as coeff_rating,
                    s.seller_level as "level",
                    s.seller_name,
                    s.agency_slug,
                    s.agency_status,
                    s.seller_country as country,
                    s.seller_country_code as country_code,
                    s.seller_languages as languages,
                    s.seller_profile_image as profile_image,
                    first_value(s.gig_title) over w_s as best_selling_gig_title,
                    first_value(s.gig_cached_slug) over w_s as best_selling_gig_cached_slug,
                    first_value(s.gig_preview_url) over w_s as best_selling_gig_image_preview
                from etl_agg_dev.%1$s_metrics s
                cross join etl_wrk_dev.metrics_ratings_stats r
                where
                    s.agg_period_start = %4$L::date
                window
                    w_s as
                        (
                            partition by s.seller_id
                            order by
                                s.gig_is_active desc,
                                s.min_total_historical_revenue_by_gig desc,
                                s.gig_scraped_at desc,
                                s.gig_id desc
                        );
        $exec$,
        tables_prefix,
        end_dttm,
        rank_scale_factor,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt := row_count;

        analyze etl_wrk_dev.sellers_attributes;

        if debug_mode is false then
            drop table if exists etl_wrk_dev.metrics_ratings_stats cascade;
        end if;

        select
            '7.1' as src_step,
            'etl_agg_dev' as tgt_schema,
            'sellers_attributes' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

        -- Step 7.2 - Preparing partition and backup for sellers

        task_start_dttm := clock_timestamp();

                -- Getting name of current period partition

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg_dev.' || tables_prefix || '_sellers')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

                -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk_dev.agg_backup_%2$s cascade;

                    -- This backup won't be cleaned up automatically
                        -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk_dev.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk_dev.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk_dev.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            '7.2' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_sellers' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 7.3 - Materializing sellers metrics

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                created_at,
                fiverr_created_at,
                scraped_at,
                min_active_revenue,
                min_inactive_revenue,
                min_revenue,
                avg_active_revenue,
                avg_inactive_revenue,
                avg_revenue,
                max_active_revenue,
                max_inactive_revenue,
                max_revenue,
                min_total_historical_revenue,
                avg_total_historical_revenue,
                max_total_historical_revenue,
                best_selling_gig_revenue,
                agg_period_start,
                seller_id,
                fiverr_seller_id,
                best_selling_gig_id,
                best_selling_fiverr_gig_id,
                global_rank,
                gig_count_by_seller,
                ratings_count,
                completed_orders_count,
                active_gigs_ratings_count_by_seller,
                sales_volume,
                total_historical_sales_volume,
                best_selling_gig_total_historical_volume,
                weighted_avg_gig_price,
                is_pro,
                is_active,
                is_trend_valid,
                sales_volume_growth_percent,
                rating,
                revenue_growth_percent,
                revenue_trend,
                best_selling_gig_revenue_share,
                best_selling_gig_title,
                best_selling_gig_cached_slug,
                "level",
                seller_name,
                agency_slug,
                agency_status,
                country,
                country_code,
                languages,
                profile_image,
                best_selling_gig_image_preview
            )

                select
                    %3$L::bigint as "load_id",
                    transaction_timestamp() as processed_dttm,
                    created_at,
                    fiverr_created_at,
                    scraped_at,
                    round(min_active_revenue)::bigint as min_active_revenue,
                    round(min_inactive_revenue)::bigint as min_inactive_revenue,
                    round(min_total_revenue)::bigint as min_revenue,
                    round(avg_active_revenue)::bigint as avg_active_revenue,
                    round(avg_inactive_revenue)::bigint as avg_inactive_revenue,
                    round(avg_total_revenue)::bigint as avg_revenue,
                    round(max_active_revenue)::bigint as max_active_revenue,
                    round(max_inactive_revenue)::bigint as max_inactive_revenue,
                    round(max_total_revenue)::bigint as max_revenue,
                    round(min_total_historical_revenue)::bigint as min_total_historical_revenue,
                    round(avg_total_historical_revenue)::bigint as avg_total_historical_revenue,
                    round(max_total_historical_revenue)::bigint as max_total_historical_revenue,
                    round(best_selling_gig_revenue)::bigint as best_selling_gig_revenue,
                    %4$L::date as agg_period_start,
                    seller_id,
                    fiverr_seller_id,
                    best_selling_gig_id,
                    best_selling_fiverr_gig_id,
                    (dense_rank() over (order by
                        is_active desc nulls last, -- true first
                        coeff_revenue desc nulls last, -- bigger revenue first
                        (
                            case "level"
                                when 'bad_actor' then -3
                                when 'low_quality' then -2
                                when 'no_level' then -1
                                when 'new_seller' then 0
                                when 'level_one' then 1
                                when 'level_two' then 2
                                when 'level_trs' then 3
                            else -4 end
                        ) desc nulls last, -- higher level first
                        coeff_trend desc nulls last, -- bigger trend growth first
                        coeff_rating desc nulls last, -- bigger rating first
                        coalesce(fiverr_created_at, scraped_at) asc nulls last, -- oldest first
                        seller_id asc nulls last -- oldest first
                    ))::int as global_rank,
                    gig_count_by_seller,
                    ratings_count,
                    completed_orders_count,
                    active_gigs_ratings_count,
                    sales_volume,
                    total_historical_sales_volume,
                    best_selling_gig_total_historical_volume,
                    round(weighted_avg_gig_price)::int as weighted_avg_gig_price,
                    is_pro,
                    is_active,
                    is_trend_valid,
                    round(sales_volume_growth_percent, 2) as sales_volume_growth_percent,
                    round(rating, 2) as rating,
                    round(revenue_growth_percent, 2) as revenue_growth_percent,
                    round(revenue_trend, 2) as revenue_trend,
                    round(coalesce(
                            best_selling_gig_revenue::numeric / nullif(min_total_historical_revenue, 0::numeric),
                            0::numeric
                        ), 2) as best_selling_gig_revenue_share,
                    best_selling_gig_title,
                    best_selling_gig_cached_slug,
                    "level",
                    seller_name,
                    agency_slug,
                    agency_status,
                    country,
                    country_code,
                    languages,
                    profile_image,
                    best_selling_gig_image_preview
                from etl_wrk_dev.sellers_attributes;

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt = row_count;

                -- Maintenance

                    -- Indices

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg_dev.' || tables_prefix || '_sellers')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg_dev.%1$s_sellers
                    add primary key (agg_period_start, seller_id) include (is_active) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create unique index if not exists uidx_fiverr_seller_id_%1$s_sellers
                on etl_agg_dev.%1$s_sellers (agg_period_start, fiverr_seller_id) with (fillfactor = 100);
            create index if not exists idx_%1$s_sellers_best_selling_gig_id
                on etl_agg_dev.%1$s_sellers (agg_period_start, best_selling_gig_id) with (fillfactor = 100);
            create unique index if not exists uidx_%1$s_sellers_ranks
                on etl_agg_dev.%1$s_sellers (agg_period_start, global_rank desc) with (fillfactor = 100);
            create index if not exists idx_trg_seller_name_%1$s_sellers
                on etl_agg_dev.%1$s_sellers using gin (seller_name gin_trgm_ops);

            alter table if exists etl_agg_dev.%1$s_sellers
                replica identity using index %1$s_sellers_pkey;

        $exec$,
        tables_prefix
        );

                    -- CLUSTER

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

                    -- CREATE STATISTICS and ANALYZE

        stats_columns_list := array[
                'seller_id', 'fiverr_seller_id',
                'best_selling_gig_id', 'best_selling_fiverr_gig_id'
            ];

        execute format($exec$
            create statistics if not exists %1$I.%2$s
            on %3$s
            from %1$I.%4$I;
        $exec$,
        period_partition_rec.part_schema,
        substring('stats_' || period_partition_rec.part_name, 1, 63),
        array_to_string(stats_columns_list, ', '),
        period_partition_rec.part_name
        );

        execute format($exec$
            analyze etl_agg_dev.%1$s_sellers;
        $exec$,
        tables_prefix
        );

        if debug_mode is false then
            drop table if exists etl_wrk_dev.sellers_attributes cascade;
        end if;

        select
            '7.3' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_sellers' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 8 - Materializing categories metrics

            -- Step 8.1 - Counting sellers by categories and levels

        task_start_dttm := clock_timestamp();

        execute format($exec$

            drop table if exists etl_wrk_dev.seller_counts cascade;

            create unlogged table if not exists etl_wrk_dev.seller_counts
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    (count(distinct seller_id))::int as cnt,
                    seller_level,
                    case grouping(category_id, sub_category_id, nested_sub_category_id, seller_level)
                        when 7 then 'category_id' -- 0111 = 7
                        when 11 then 'sub_category_id' -- 1011 = 11
                        when 13 then 'nested_sub_category_id' -- 1101 = 13
                        when 6 then 'category_id_x_seller_level' -- 0110 = 6
                        when 10 then 'sub_category_id_x_seller_level' -- 1010 = 10
                        when 12 then 'nested_sub_category_id_x_seller_level' -- 1100 = 12
                    end as group_type
                from etl_agg_dev.%1$s_metrics
                where
                    agg_period_start = %2$L::date
                group by grouping sets
                    (
                        (category_id),
                        (sub_category_id),
                        (nested_sub_category_id),
                        (category_id, seller_level),
                        (sub_category_id, seller_level),
                        (nested_sub_category_id, seller_level)
                    );

        $exec$,
        tables_prefix,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_seller_counts_cat
            on etl_wrk_dev.seller_counts (category_id) with (fillfactor = 100);
        create index if not exists idx_seller_counts_subcat
            on etl_wrk_dev.seller_counts (sub_category_id) with (fillfactor = 100);
        create index if not exists idx_seller_counts_nest_subcat
            on etl_wrk_dev.seller_counts (nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk_dev.seller_counts;

        select
            '8.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'seller_counts' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 8.2 - Preparing dataset for categories' metrics

        task_start_dttm := clock_timestamp();

        execute format($exec$

            drop table if exists etl_wrk_dev.categories_prefin cascade;

            create unlogged table if not exists etl_wrk_dev.categories_prefin
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select distinct
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    all_gigs_count_by_category,
                    all_gigs_count_by_subcategory,
                    all_gigs_count_by_nested_subcategory,
                    regular_gigs_by_category as regular_gigs_count_by_category,
                    regular_gigs_by_subcategory as regular_gigs_count_by_subcategory,
                    regular_gigs_by_nested_subcategory as regular_gigs_count_by_nested_subcategory,
                    pro_gigs_by_category as pro_gigs_count_by_category,
                    pro_gigs_by_subcategory as pro_gigs_count_by_subcategory,
                    pro_gigs_by_nested_subcategory as pro_gigs_count_by_nested_subcategory,
                    volume_by_category as sales_volume_by_category,
                    volume_by_subcategory as sales_volume_by_subcategory,
                    volume_by_nested_subcategory as sales_volume_by_nested_subcategory,
                    total_historical_volume_by_category as total_historical_sales_volume_by_category,
                    total_historical_volume_by_subcategory as total_historical_sales_volume_by_subcategory,
                    total_historical_volume_by_nested_subcategory as total_historical_sales_volume_by_nested_subcategory,
                    is_trend_valid_for_category,
                    is_trend_valid_for_subcategory,
                    is_trend_valid_for_nested_subcategory,
                    sales_volume_growth_percent_by_category,
                    sales_volume_growth_percent_by_subcategory,
                    sales_volume_growth_percent_by_nested_subcategory,
                    competition_score_for_category,
                    competition_score_for_subcategory,
                    competition_score_for_nested_subcategory,
                    market_share_for_category,
                    market_share_for_subcategory,
                    market_share_for_nested_subcategory,
                    weighted_avg_price_by_category as weighted_avg_gig_price_by_category,
                    weighted_avg_price_by_subcategory as weighted_avg_gig_price_by_subcategory,
                    weighted_avg_price_by_nested_subcategory as weighted_avg_gig_price_by_nested_subcategory,
                    min_active_revenue_by_category,
                    min_inactive_revenue_by_category,
                    min_total_revenue_by_category,
                    min_active_revenue_by_subcategory,
                    min_inactive_revenue_by_subcategory,
                    min_total_revenue_by_subcategory,
                    min_active_revenue_by_nested_subcategory,
                    min_inactive_revenue_by_nested_subcategory,
                    min_total_revenue_by_nested_subcategory,
                    avg_active_revenue_by_category,
                    avg_inactive_revenue_by_category,
                    avg_total_revenue_by_category,
                    avg_active_revenue_by_subcategory,
                    avg_inactive_revenue_by_subcategory,
                    avg_total_revenue_by_subcategory,
                    avg_active_revenue_by_nested_subcategory,
                    avg_inactive_revenue_by_nested_subcategory,
                    avg_total_revenue_by_nested_subcategory,
                    max_active_revenue_by_category,
                    max_inactive_revenue_by_category,
                    max_total_revenue_by_category,
                    max_active_revenue_by_subcategory,
                    max_inactive_revenue_by_subcategory,
                    max_total_revenue_by_subcategory,
                    max_active_revenue_by_nested_subcategory,
                    max_inactive_revenue_by_nested_subcategory,
                    max_total_revenue_by_nested_subcategory,
                    min_total_historical_revenue_by_category,
                    min_total_historical_revenue_by_subcategory,
                    min_total_historical_revenue_by_nested_subcategory,
                    avg_total_historical_revenue_by_category,
                    avg_total_historical_revenue_by_subcategory,
                    avg_total_historical_revenue_by_nested_subcategory,
                    max_total_historical_revenue_by_category,
                    max_total_historical_revenue_by_subcategory,
                    max_total_historical_revenue_by_nested_subcategory,
                    revenue_growth_percent_by_category,
                    revenue_growth_percent_by_subcategory,
                    revenue_growth_percent_by_nested_subcategory,
                    trend_by_category as revenue_trend_by_category,
                    trend_by_subcategory as revenue_trend_by_subcategory,
                    trend_by_nested_subcategory as revenue_trend_by_nested_subcategory,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name
                from etl_agg_dev.%1$s_metrics
                where
                    agg_period_start = %2$L::date;

        $exec$,
        tables_prefix,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt = row_count;

        create index if not exists idx_cat_id_cat_prefin
            on etl_wrk_dev.categories_prefin (category_id) with (fillfactor = 100);
        create index if not exists idx_subcat_id_cat_prefin
            on etl_wrk_dev.categories_prefin (sub_category_id) with (fillfactor = 100);
        create index if not exists idx_nested_subcat_id_cat_prefin
            on etl_wrk_dev.categories_prefin (nested_sub_category_id) with (fillfactor = 100);
        create unique index if not exists uidx_id_cat_prefin
            on etl_wrk_dev.categories_prefin (category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);

        analyze etl_wrk_dev.categories_prefin;

        select
            '8.2' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'categories_prefin' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 8.3.1 - Preparing partition and backup for categories

        task_start_dttm := clock_timestamp();

                -- Getting name of current period partition

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg_dev.' || tables_prefix || '_categories')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

                -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk_dev.agg_backup_%2$s cascade;

                    -- This backup won't be cleaned up automatically
                        -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk_dev.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk_dev.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk_dev.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            '8.3.1' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_categories' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 8.3.2 - Inserting metrics for categories

        task_start_dttm := clock_timestamp();

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                min_active_revenue,
                min_inactive_revenue,
                min_revenue,
                avg_active_revenue,
                avg_inactive_revenue,
                avg_revenue,
                max_active_revenue,
                max_inactive_revenue,
                max_revenue,
                min_total_historical_revenue,
                avg_total_historical_revenue,
                max_total_historical_revenue,
                agg_period_start,
                category_id,
                global_rank,
                all_gigs_count,
                regular_gigs_count,
                pro_gigs_count,
                seller_count,
                sales_volume,
                total_historical_sales_volume,
                weighted_avg_gig_price,
                is_trend_valid,
                sales_volume_growth_percent,
                competition_score,
                market_share,
                revenue_growth_percent,
                revenue_trend,
                category_name
            )

                select
                    %3$L::bigint as "load_id",
                    transaction_timestamp() as processed_dttm,
                    round(min_active_revenue)::bigint as min_active_revenue,
                    round(min_inactive_revenue)::bigint as min_inactive_revenue,
                    round(min_revenue)::bigint as min_revenue,
                    round(avg_active_revenue)::bigint as avg_active_revenue,
                    round(avg_inactive_revenue)::bigint as avg_inactive_revenue,
                    round(avg_revenue)::bigint as avg_revenue,
                    round(max_active_revenue)::bigint as max_active_revenue,
                    round(max_inactive_revenue)::bigint as max_inactive_revenue,
                    round(max_revenue)::bigint as max_revenue,
                    round(min_total_historical_revenue)::bigint as min_total_historical_revenue,
                    round(avg_total_historical_revenue)::bigint as avg_total_historical_revenue,
                    round(max_total_historical_revenue)::bigint as max_total_historical_revenue,
                    %4$L::date as agg_period_start,
                    category_id,
                    (dense_rank() over (order by
                        min_revenue desc nulls last,
                        revenue_trend desc nulls last,
                        category_id desc nulls last
                    ))::int as global_rank,
                    all_gigs_count,
                    regular_gigs_count,
                    pro_gigs_count,
                    seller_count,
                    sales_volume,
                    total_historical_sales_volume,
                    round(weighted_avg_gig_price)::int as weighted_avg_gig_price,
                    is_trend_valid,
                    round(sales_volume_growth_percent, 2) as sales_volume_growth_percent,
                    round(competition_score, 2) as competition_score,
                    round(market_share, 2) as market_share,
                    round(revenue_growth_percent, 2) as revenue_growth_percent,
                    round(revenue_trend, 2) as revenue_trend,
                    category_name
                from
                (
                    select distinct
                        c.category_id,
                        c.all_gigs_count_by_category as all_gigs_count,
                        c.regular_gigs_count_by_category as regular_gigs_count,
                        c.pro_gigs_count_by_category as pro_gigs_count,
                        s.cnt as seller_count,
                        c.sales_volume_by_category as sales_volume,
                        c.total_historical_sales_volume_by_category as total_historical_sales_volume,
                        c.is_trend_valid_for_category as is_trend_valid,
                        c.sales_volume_growth_percent_by_category as sales_volume_growth_percent,
                        c.competition_score_for_category as competition_score,
                        c.market_share_for_category as market_share,
                        c.weighted_avg_gig_price_by_category as weighted_avg_gig_price,
                        c.min_active_revenue_by_category as min_active_revenue,
                        c.min_inactive_revenue_by_category as min_inactive_revenue,
                        c.min_total_revenue_by_category as min_revenue,
                        c.avg_active_revenue_by_category as avg_active_revenue,
                        c.avg_inactive_revenue_by_category as avg_inactive_revenue,
                        c.avg_total_revenue_by_category as avg_revenue,
                        c.max_active_revenue_by_category as max_active_revenue,
                        c.max_inactive_revenue_by_category as max_inactive_revenue,
                        c.max_total_revenue_by_category as max_revenue,
                        c.min_total_historical_revenue_by_category as min_total_historical_revenue,
                        c.avg_total_historical_revenue_by_category as avg_total_historical_revenue,
                        c.max_total_historical_revenue_by_category as max_total_historical_revenue,
                        c.revenue_growth_percent_by_category as revenue_growth_percent,
                        c.revenue_trend_by_category as revenue_trend,
                        c.category_name
                    from etl_wrk_dev.categories_prefin c
                    join etl_wrk_dev.seller_counts s
                        on c.category_id = s.category_id
                        and s.group_type = 'category_id'
                    where
                        c.category_id is not null
                ) a;

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt = row_count;

                -- Maintenance

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg_dev.' || tables_prefix || '_categories')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg_dev.%1$s_categories
                    add primary key (agg_period_start, category_id) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create unique index if not exists uidx_%1$s_categories_ranks
                on etl_agg_dev.%1$s_categories (agg_period_start, global_rank desc) with (fillfactor = 100);
            create index if not exists idx_trg_cat_name_%1$s_cat
                on etl_agg_dev.%1$s_categories using gin (category_name gin_trgm_ops);

            alter table if exists etl_agg_dev.%1$s_categories
                replica identity using index %1$s_categories_pkey;

        $exec$,
        tables_prefix
        );

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

        execute format($exec$
            analyze etl_agg_dev.%1$s_categories;
        $exec$,
        tables_prefix
        );

        select
            '8.3.2' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_categories' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 8.4.1 - Preparing partition and backup for subcategories

        task_start_dttm := clock_timestamp();

                -- Getting name of current period partition

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg_dev.' || tables_prefix || '_subcategories')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

                -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk_dev.agg_backup_%2$s cascade;

                    -- This backup won't be cleaned up automatically
                        -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk_dev.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk_dev.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk_dev.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            '8.4.1' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_subcategories' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 8.4.2 - Inserting metrics for subcategories

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                min_active_revenue,
                min_inactive_revenue,
                min_revenue,
                avg_active_revenue,
                avg_inactive_revenue,
                avg_revenue,
                max_active_revenue,
                max_inactive_revenue,
                max_revenue,
                min_total_historical_revenue,
                avg_total_historical_revenue,
                max_total_historical_revenue,
                agg_period_start,
                sub_category_id,
                global_rank,
                all_gigs_count,
                regular_gigs_count,
                pro_gigs_count,
                seller_count,
                sales_volume,
                total_historical_sales_volume,
                weighted_avg_gig_price,
                is_trend_valid,
                sales_volume_growth_percent,
                competition_score,
                market_share,
                revenue_growth_percent,
                revenue_trend,
                sub_category_name
            )

                select
                    %3$L::bigint as "load_id",
                    transaction_timestamp() as processed_dttm,
                    round(min_active_revenue)::bigint as min_active_revenue,
                    round(min_inactive_revenue)::bigint as min_inactive_revenue,
                    round(min_revenue)::bigint as min_revenue,
                    round(avg_active_revenue)::bigint as avg_active_revenue,
                    round(avg_inactive_revenue)::bigint as avg_inactive_revenue,
                    round(avg_revenue)::bigint as avg_revenue,
                    round(max_active_revenue)::bigint as max_active_revenue,
                    round(max_inactive_revenue)::bigint as max_inactive_revenue,
                    round(max_revenue)::bigint as max_revenue,
                    round(min_total_historical_revenue)::bigint as min_total_historical_revenue,
                    round(avg_total_historical_revenue)::bigint as avg_total_historical_revenue,
                    round(max_total_historical_revenue)::bigint as max_total_historical_revenue,
                    %4$L::date as agg_period_start,
                    sub_category_id,
                    (dense_rank() over (order by
                        min_revenue desc nulls last,
                        revenue_trend desc nulls last,
                        sub_category_id desc nulls last
                    ))::int as global_rank,
                    all_gigs_count,
                    regular_gigs_count,
                    pro_gigs_count,
                    seller_count,
                    sales_volume,
                    total_historical_sales_volume,
                    round(weighted_avg_gig_price)::int as weighted_avg_gig_price,
                    is_trend_valid,
                    round(sales_volume_growth_percent, 2) as sales_volume_growth_percent,
                    round(competition_score, 2) as competition_score,
                    round(market_share, 2) as market_share,
                    round(revenue_growth_percent, 2) as revenue_growth_percent,
                    round(revenue_trend, 2) as revenue_trend,
                    sub_category_name
                from
                (
                    select distinct
                        c.sub_category_id,
                        c.all_gigs_count_by_subcategory as all_gigs_count,
                        c.regular_gigs_count_by_subcategory as regular_gigs_count,
                        c.pro_gigs_count_by_subcategory as pro_gigs_count,
                        s.cnt as seller_count,
                        c.sales_volume_by_subcategory as sales_volume,
                        c.total_historical_sales_volume_by_subcategory as total_historical_sales_volume,
                        c.is_trend_valid_for_subcategory as is_trend_valid,
                        c.sales_volume_growth_percent_by_subcategory as sales_volume_growth_percent,
                        c.competition_score_for_subcategory as competition_score,
                        c.market_share_for_subcategory as market_share,
                        c.weighted_avg_gig_price_by_subcategory as weighted_avg_gig_price,
                        c.min_active_revenue_by_subcategory as min_active_revenue,
                        c.min_inactive_revenue_by_subcategory as min_inactive_revenue,
                        c.min_total_revenue_by_subcategory as min_revenue,
                        c.avg_active_revenue_by_subcategory as avg_active_revenue,
                        c.avg_inactive_revenue_by_subcategory as avg_inactive_revenue,
                        c.avg_total_revenue_by_subcategory as avg_revenue,
                        c.max_active_revenue_by_subcategory as max_active_revenue,
                        c.max_inactive_revenue_by_subcategory as max_inactive_revenue,
                        c.max_total_revenue_by_subcategory as max_revenue,
                        c.min_total_historical_revenue_by_subcategory as min_total_historical_revenue,
                        c.avg_total_historical_revenue_by_subcategory as avg_total_historical_revenue,
                        c.max_total_historical_revenue_by_subcategory as max_total_historical_revenue,
                        c.revenue_growth_percent_by_subcategory as revenue_growth_percent,
                        c.revenue_trend_by_subcategory as revenue_trend,
                        c.sub_category_name
                    from etl_wrk_dev.categories_prefin c
                    join etl_wrk_dev.seller_counts s
                        on c.sub_category_id = s.sub_category_id
                        and s.group_type = 'sub_category_id'
                    where
                        c.sub_category_id is not null
                ) a;

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt = row_count;

                -- Maintenance

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg_dev.' || tables_prefix || '_subcategories')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg_dev.%1$s_subcategories
                    add primary key (agg_period_start, sub_category_id) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create unique index if not exists uidx_%1$s_subcategories_ranks
                on etl_agg_dev.%1$s_subcategories (agg_period_start, global_rank desc) with (fillfactor = 100);
            create index if not exists idx_trg_subcat_name_%1$s_subcat
                on etl_agg_dev.%1$s_subcategories using gin (sub_category_name gin_trgm_ops);

            alter table if exists etl_agg_dev.%1$s_subcategories
                replica identity using index %1$s_subcategories_pkey;

        $exec$,
        tables_prefix
        );

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

        execute format($exec$
            analyze etl_agg_dev.%1$s_subcategories;
        $exec$,
        tables_prefix
        );

        select
            '8.4.2' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_subcategories' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 8.5.1 - Preparing partition and backup for nested subcategories

        task_start_dttm := clock_timestamp();

                -- Getting name of current period partition

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg_dev.' || tables_prefix || '_nested_subcategories')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

                -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk_dev.agg_backup_%2$s cascade;

                    -- This backup won't be cleaned up automatically
                        -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk_dev.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk_dev.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk_dev.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            '8.5.1' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_nested_subcategories' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 8.5.2 - Inserting metrics for nested subcategories

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                min_active_revenue,
                min_inactive_revenue,
                min_revenue,
                avg_active_revenue,
                avg_inactive_revenue,
                avg_revenue,
                max_active_revenue,
                max_inactive_revenue,
                max_revenue,
                min_total_historical_revenue,
                avg_total_historical_revenue,
                max_total_historical_revenue,
                agg_period_start,
                nested_sub_category_id,
                global_rank,
                all_gigs_count,
                regular_gigs_count,
                pro_gigs_count,
                seller_count,
                sales_volume,
                total_historical_sales_volume,
                weighted_avg_gig_price,
                is_trend_valid,
                sales_volume_growth_percent,
                competition_score,
                market_share,
                revenue_growth_percent,
                revenue_trend,
                nested_sub_category_name
            )

                select
                    %3$L::bigint as "load_id",
                    transaction_timestamp() as processed_dttm,
                    round(min_active_revenue)::bigint as min_active_revenue,
                    round(min_inactive_revenue)::bigint as min_inactive_revenue,
                    round(min_revenue)::bigint as min_revenue,
                    round(avg_active_revenue)::bigint as avg_active_revenue,
                    round(avg_inactive_revenue)::bigint as avg_inactive_revenue,
                    round(avg_revenue)::bigint as avg_revenue,
                    round(max_active_revenue)::bigint as max_active_revenue,
                    round(max_inactive_revenue)::bigint as max_inactive_revenue,
                    round(max_revenue)::bigint as max_revenue,
                    round(min_total_historical_revenue)::bigint as min_total_historical_revenue,
                    round(avg_total_historical_revenue)::bigint as avg_total_historical_revenue,
                    round(max_total_historical_revenue)::bigint as max_total_historical_revenue,
                    %4$L::date as agg_period_start,
                    nested_sub_category_id,
                    (dense_rank() over (order by
                        min_revenue desc nulls last,
                        revenue_trend desc nulls last,
                        nested_sub_category_id desc nulls last
                    ))::int as global_rank,
                    all_gigs_count,
                    regular_gigs_count,
                    pro_gigs_count,
                    seller_count,
                    sales_volume,
                    total_historical_sales_volume,
                    round(weighted_avg_gig_price)::int as weighted_avg_gig_price,
                    is_trend_valid,
                    round(sales_volume_growth_percent, 2) as sales_volume_growth_percent,
                    round(competition_score, 2) as competition_score,
                    round(market_share, 2) as market_share,
                    round(revenue_growth_percent, 2) as revenue_growth_percent,
                    round(revenue_trend, 2) as revenue_trend,
                    nested_sub_category_name
                from
                (
                    select distinct
                        c.nested_sub_category_id,
                        c.all_gigs_count_by_nested_subcategory as all_gigs_count,
                        c.regular_gigs_count_by_nested_subcategory as regular_gigs_count,
                        c.pro_gigs_count_by_nested_subcategory as pro_gigs_count,
                        s.cnt as seller_count,
                        c.sales_volume_by_nested_subcategory as sales_volume,
                        c.total_historical_sales_volume_by_nested_subcategory as total_historical_sales_volume,
                        c.is_trend_valid_for_nested_subcategory as is_trend_valid,
                        c.sales_volume_growth_percent_by_nested_subcategory as sales_volume_growth_percent,
                        c.competition_score_for_nested_subcategory as competition_score,
                        c.market_share_for_nested_subcategory as market_share,
                        c.weighted_avg_gig_price_by_nested_subcategory as weighted_avg_gig_price,
                        c.min_active_revenue_by_nested_subcategory as min_active_revenue,
                        c.min_inactive_revenue_by_nested_subcategory as min_inactive_revenue,
                        c.min_total_revenue_by_nested_subcategory as min_revenue,
                        c.avg_active_revenue_by_nested_subcategory as avg_active_revenue,
                        c.avg_inactive_revenue_by_nested_subcategory as avg_inactive_revenue,
                        c.avg_total_revenue_by_nested_subcategory as avg_revenue,
                        c.max_active_revenue_by_nested_subcategory as max_active_revenue,
                        c.max_inactive_revenue_by_nested_subcategory as max_inactive_revenue,
                        c.max_total_revenue_by_nested_subcategory as max_revenue,
                        c.min_total_historical_revenue_by_nested_subcategory as min_total_historical_revenue,
                        c.avg_total_historical_revenue_by_nested_subcategory as avg_total_historical_revenue,
                        c.max_total_historical_revenue_by_nested_subcategory as max_total_historical_revenue,
                        c.revenue_growth_percent_by_nested_subcategory as revenue_growth_percent,
                        c.revenue_trend_by_nested_subcategory as revenue_trend,
                        c.nested_sub_category_name
                    from etl_wrk_dev.categories_prefin c
                    join etl_wrk_dev.seller_counts s
                        on c.nested_sub_category_id = s.nested_sub_category_id
                        and s.group_type = 'nested_sub_category_id'
                    where
                        c.nested_sub_category_id is not null
                ) a;

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt = row_count;

                -- Maintenance

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg_dev.' || tables_prefix || '_nested_subcategories')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg_dev.%1$s_nested_subcategories
                    add primary key (agg_period_start, nested_sub_category_id) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create unique index if not exists uidx_%1$s_nested_subcategories_ranks
                on etl_agg_dev.%1$s_nested_subcategories (agg_period_start, global_rank desc) with (fillfactor = 100);
            create index if not exists idx_trg_nest_subcat_name_%1$s_nest_subcat
                on etl_agg_dev.%1$s_nested_subcategories using gin (nested_sub_category_name gin_trgm_ops);

            alter table if exists etl_agg_dev.%1$s_nested_subcategories
                replica identity using index %1$s_nested_subcategories_pkey;

        $exec$,
        tables_prefix
        );

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

        execute format($exec$
            analyze etl_agg_dev.%1$s_nested_subcategories;
        $exec$,
        tables_prefix
        );

        select
            '8.5.2' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_nested_subcategories' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

        -- Step 8.6.1 - Preparing partition and backup for all categories metrics

        task_start_dttm := clock_timestamp();

                -- Getting name of current period partition

        select
            case
                when tables_prefix = 'quarterly' then '3 month'
                else '1 ' || replace(tables_prefix, 'ly', '')
            end
        into partitioning_step;

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg_dev.' || tables_prefix || '_all_categories')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

                -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk_dev.agg_backup_%2$s cascade;

                    -- This backup won't be cleaned up automatically
                        -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk_dev.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk_dev.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk_dev.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            '8.6.1' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_all_categories' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 8.6.2 - Inserting combined metrics for all category types

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                min_active_revenue_by_category,
                min_inactive_revenue_by_category,
                min_revenue_by_category,
                min_active_revenue_by_subcategory,
                min_inactive_revenue_by_subcategory,
                min_revenue_by_subcategory,
                min_active_revenue_by_nested_subcategory,
                min_inactive_revenue_by_nested_subcategory,
                min_revenue_by_nested_subcategory,
                avg_active_revenue_by_category,
                avg_inactive_revenue_by_category,
                avg_revenue_by_category,
                avg_active_revenue_by_subcategory,
                avg_inactive_revenue_by_subcategory,
                avg_revenue_by_subcategory,
                avg_active_revenue_by_nested_subcategory,
                avg_inactive_revenue_by_nested_subcategory,
                avg_revenue_by_nested_subcategory,
                max_active_revenue_by_category,
                max_inactive_revenue_by_category,
                max_revenue_by_category,
                max_active_revenue_by_subcategory,
                max_inactive_revenue_by_subcategory,
                max_revenue_by_subcategory,
                max_active_revenue_by_nested_subcategory,
                max_inactive_revenue_by_nested_subcategory,
                max_revenue_by_nested_subcategory,
                min_total_historical_revenue_by_category,
                min_total_historical_revenue_by_subcategory,
                min_total_historical_revenue_by_nested_subcategory,
                avg_total_historical_revenue_by_category,
                avg_total_historical_revenue_by_subcategory,
                avg_total_historical_revenue_by_nested_subcategory,
                max_total_historical_revenue_by_category,
                max_total_historical_revenue_by_subcategory,
                max_total_historical_revenue_by_nested_subcategory,
                agg_period_start,
                id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                global_rank_for_category,
                global_rank_for_subcategory,
                global_rank_for_nested_subcategory,
                all_gigs_count_by_category,
                all_gigs_count_by_subcategory,
                all_gigs_count_by_nested_subcategory,
                regular_gigs_count_by_category,
                regular_gigs_count_by_subcategory,
                regular_gigs_count_by_nested_subcategory,
                pro_gigs_count_by_category,
                pro_gigs_count_by_subcategory,
                pro_gigs_count_by_nested_subcategory,
                seller_count_by_category,
                seller_count_by_subcategory,
                seller_count_by_nested_subcategory,
                sales_volume_by_category,
                sales_volume_by_subcategory,
                sales_volume_by_nested_subcategory,
                total_historical_sales_volume_by_category,
                total_historical_sales_volume_by_subcategory,
                total_historical_sales_volume_by_nested_subcategory,
                weighted_avg_gig_price_by_category,
                weighted_avg_gig_price_by_subcategory,
                weighted_avg_gig_price_by_nested_subcategory,
                is_trend_valid_for_category,
                is_trend_valid_for_subcategory,
                is_trend_valid_for_nested_subcategory,
                sales_volume_growth_percent_by_category,
                sales_volume_growth_percent_by_subcategory,
                sales_volume_growth_percent_by_nested_subcategory,
                competition_score_for_category,
                competition_score_for_subcategory,
                competition_score_for_nested_subcategory,
                market_share_for_category,
                market_share_for_subcategory,
                market_share_for_nested_subcategory,
                revenue_growth_percent_by_category,
                revenue_growth_percent_by_subcategory,
                revenue_growth_percent_by_nested_subcategory,
                revenue_trend_by_category,
                revenue_trend_by_subcategory,
                revenue_trend_by_nested_subcategory,
                category_name,
                sub_category_name,
                nested_sub_category_name
            )

                select
                    %3$L::bigint as "load_id",
                    transaction_timestamp() as processed_dttm,
                    c.min_active_revenue as min_active_revenue_by_category,
                    c.min_inactive_revenue as min_inactive_revenue_by_category,
                    c.min_revenue as min_revenue_by_category,
                    s.min_active_revenue as min_active_revenue_by_subcategory,
                    s.min_inactive_revenue as min_inactive_revenue_by_subcategory,
                    s.min_revenue as min_revenue_by_subcategory,
                    n.min_active_revenue as min_active_revenue_by_nested_subcategory,
                    n.min_inactive_revenue as min_inactive_revenue_by_nested_subcategory,
                    n.min_revenue as min_revenue_by_nested_subcategory,
                    c.avg_active_revenue as avg_active_revenue_by_category,
                    c.avg_inactive_revenue as avg_inactive_revenue_by_category,
                    c.avg_revenue as avg_revenue_by_category,
                    s.avg_active_revenue as avg_active_revenue_by_subcategory,
                    s.avg_inactive_revenue as avg_inactive_revenue_by_subcategory,
                    s.avg_revenue as avg_revenue_by_subcategory,
                    n.avg_active_revenue as avg_active_revenue_by_nested_subcategory,
                    n.avg_inactive_revenue as avg_inactive_revenue_by_nested_subcategory,
                    n.avg_revenue as avg_revenue_by_nested_subcategory,
                    c.max_active_revenue as max_active_revenue_by_category,
                    c.max_inactive_revenue as max_inactive_revenue_by_category,
                    c.max_revenue as max_revenue_by_category,
                    s.max_active_revenue as max_active_revenue_by_subcategory,
                    s.max_inactive_revenue as max_inactive_revenue_by_subcategory,
                    s.max_revenue as max_revenue_by_subcategory,
                    n.max_active_revenue as max_active_revenue_by_nested_subcategory,
                    n.max_inactive_revenue as max_inactive_revenue_by_nested_subcategory,
                    n.max_revenue as max_revenue_by_nested_subcategory,
                    c.min_total_historical_revenue as min_total_historical_revenue_by_category,
                    s.min_total_historical_revenue as min_total_historical_revenue_by_subcategory,
                    n.min_total_historical_revenue as min_total_historical_revenue_by_nested_subcategory,
                    c.avg_total_historical_revenue as avg_total_historical_revenue_by_category,
                    s.avg_total_historical_revenue as avg_total_historical_revenue_by_subcategory,
                    n.avg_total_historical_revenue as avg_total_historical_revenue_by_nested_subcategory,
                    c.max_total_historical_revenue as max_total_historical_revenue_by_category,
                    s.max_total_historical_revenue as max_total_historical_revenue_by_subcategory,
                    n.max_total_historical_revenue as max_total_historical_revenue_by_nested_subcategory,
                    %4$L::date as agg_period_start,
                    (row_number() over (order by c.category_id, s.sub_category_id, n.nested_sub_category_id))::int as id,
                    c.category_id,
                    s.sub_category_id,
                    n.nested_sub_category_id,
                    c.global_rank as global_rank_for_category,
                    s.global_rank as global_rank_for_subcategory,
                    n.global_rank as global_rank_for_nested_subcategory,
                    c.all_gigs_count as all_gigs_count_by_category,
                    s.all_gigs_count as all_gigs_count_by_subcategory,
                    n.all_gigs_count as all_gigs_count_by_nested_subcategory,
                    c.regular_gigs_count as regular_gigs_count_by_category,
                    s.regular_gigs_count as regular_gigs_count_by_subcategory,
                    n.regular_gigs_count as regular_gigs_count_by_nested_subcategory,
                    c.pro_gigs_count as pro_gigs_count_by_category,
                    s.pro_gigs_count as pro_gigs_count_by_subcategory,
                    n.pro_gigs_count as pro_gigs_count_by_nested_subcategory,
                    c.seller_count as seller_count_by_category,
                    s.seller_count as seller_count_by_subcategory,
                    n.seller_count as seller_count_by_nested_subcategory,
                    c.sales_volume as sales_volume_by_category,
                    s.sales_volume as sales_volume_by_subcategory,
                    n.sales_volume as sales_volume_by_nested_subcategory,
                    c.total_historical_sales_volume as total_historical_sales_volume_by_category,
                    s.total_historical_sales_volume as total_historical_sales_volume_by_subcategory,
                    n.total_historical_sales_volume as total_historical_sales_volume_by_nested_subcategory,
                    c.weighted_avg_gig_price as weighted_avg_gig_price_by_category,
                    s.weighted_avg_gig_price as weighted_avg_gig_price_by_subcategory,
                    n.weighted_avg_gig_price as weighted_avg_gig_price_by_nested_subcategory,
                    c.is_trend_valid as is_trend_valid_for_category,
                    s.is_trend_valid as is_trend_valid_for_subcategory,
                    n.is_trend_valid as is_trend_valid_for_nested_subcategory,
                    c.sales_volume_growth_percent as sales_volume_growth_percent_by_category,
                    s.sales_volume_growth_percent as sales_volume_growth_percent_by_subcategory,
                    n.sales_volume_growth_percent as sales_volume_growth_percent_by_nested_subcategory,
                    c.competition_score as competition_score_for_category,
                    s.competition_score as competition_score_for_subcategory,
                    n.competition_score as competition_score_for_nested_subcategory,
                    c.market_share as market_share_for_category,
                    s.market_share as market_share_for_subcategory,
                    n.market_share as market_share_for_nested_subcategory,
                    c.revenue_growth_percent as revenue_growth_percent_by_category,
                    s.revenue_growth_percent as revenue_growth_percent_by_subcategory,
                    n.revenue_growth_percent as revenue_growth_percent_by_nested_subcategory,
                    c.revenue_trend as revenue_trend_by_category,
                    s.revenue_trend as revenue_trend_by_subcategory,
                    n.revenue_trend as revenue_trend_by_nested_subcategory,
                    c.category_name,
                    s.sub_category_name,
                    n.nested_sub_category_name
                from etl_wrk_dev.categories_prefin p
                join etl_agg_dev.%5$s_categories c
                    on p.category_id = c.category_id
                    and c.agg_period_start = %4$L::date
                join etl_agg_dev.%5$s_subcategories s
                    on p.sub_category_id = s.sub_category_id
                    and s.agg_period_start = %4$L::date
                left join etl_agg_dev.%5$s_nested_subcategories n
                    on p.nested_sub_category_id = n.nested_sub_category_id
                    and n.agg_period_start = %4$L::date;

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date,
        tables_prefix
        );

        get diagnostics
            row_cnt = row_count;

                -- Maintenance

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg_dev.' || tables_prefix || '_all_categories')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg_dev.%1$s_all_categories
                    add primary key (agg_period_start, id) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create unique index if not exists uidx_all_id_%1$s_all_cats
                on etl_agg_dev.%1$s_all_categories (agg_period_start, category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);
            create index if not exists idx_cat_id_%1$s_all_cats
                on etl_agg_dev.%1$s_all_categories (agg_period_start, category_id) with (fillfactor = 100);
            create index if not exists idx_sub_cat_id_%1$s_all_cats
                on etl_agg_dev.%1$s_all_categories (agg_period_start, sub_category_id) with (fillfactor = 100);
            create index if not exists idx_nest_sub_cat_id_%1$s_all_cats
                on etl_agg_dev.%1$s_all_categories (agg_period_start, nested_sub_category_id) with (fillfactor = 100);
            create index if not exists idx_trg_cat_name_%1$s_all_cats
                on etl_agg_dev.%1$s_all_categories using gin (category_name gin_trgm_ops);
            create index if not exists idx_trg_subcat_name_%1$s_all_cats
                on etl_agg_dev.%1$s_all_categories using gin (sub_category_name gin_trgm_ops);
            create index if not exists idx_trg_nest_subcat_name_%1$s_all_cats
                on etl_agg_dev.%1$s_all_categories using gin (nested_sub_category_name gin_trgm_ops);

            alter table if exists etl_agg_dev.%1$s_all_categories
                replica identity using index %1$s_all_categories_pkey;

        $exec$,
        tables_prefix
        );

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

        execute format($exec$
            analyze etl_agg_dev.%1$s_all_categories;
        $exec$,
        tables_prefix
        );

        if debug_mode is false then
            drop table if exists etl_wrk_dev.categories_prefin cascade;
        end if;

        select
            '8.6.2' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_all_categories' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 9 - Populating sellers levels marts

            -- Step 9.1.1 - Preparing partition and backup for category_x_seller_levels

        task_start_dttm := clock_timestamp();

                -- Getting name of current period partition

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg_dev.' || tables_prefix || '_category_x_seller_levels')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

                -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk_dev.agg_backup_%2$s cascade;

                    -- This backup won't be cleaned up automatically
                        -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk_dev.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk_dev.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk_dev.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            '9.1.1' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_category_x_seller_levels' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 9.1.2 - Inserting into category_x_seller_levels

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                agg_period_start,
                id,
                category_id,
                seller_count,
                seller_level,
                category_name
            )

                select
                    %3$L::bigint as "load_id",
                    transaction_timestamp() as processed_dttm,
                    %4$L::date as agg_period_start,
                    (row_number() over (
                        order by
                            m.category_id,
                            (
                                case c.seller_level
                                    when 'bad_actor' then -3
                                    when 'low_quality' then -2
                                    when 'no_level' then -1
                                    when 'new_seller' then 0
                                    when 'level_one' then 1
                                    when 'level_two' then 2
                                    when 'level_trs' then 3
                                else -4 end
                            ) desc
                        ))::int as id,
                    m.category_id,
                    c.cnt as seller_count,
                    c.seller_level,
                    m.category_name
                from etl_wrk_dev.seller_counts c
                join etl_agg_dev.%5$s_categories m
                    on c.category_id = m.category_id
                    and m.agg_period_start = %4$L::date
                where
                    c.group_type = 'category_id_x_seller_level';

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date,
        tables_prefix
        );

        get diagnostics
            row_cnt = row_count;

                -- Maintenance

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg_dev.' || tables_prefix || '_category_x_seller_levels')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg_dev.%1$s_category_x_seller_levels
                    add primary key (agg_period_start, id) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create unique index if not exists uidx_%1$s_cat_sel_lvl
                on etl_agg_dev.%1$s_category_x_seller_levels (agg_period_start, category_id, seller_level) with (fillfactor = 100);
            create index if not exists idx_trg_cat_name_%1$s_cat_sel_lvl
                on etl_agg_dev.%1$s_category_x_seller_levels using gin (category_name gin_trgm_ops);

            alter table if exists etl_agg_dev.%1$s_category_x_seller_levels
                replica identity using index %1$s_category_x_seller_levels_pkey;

        $exec$,
        tables_prefix
        );

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

        execute format($exec$
            analyze etl_agg_dev.%1$s_category_x_seller_levels;
        $exec$,
        tables_prefix
        );

        select
            '9.1.2' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_category_x_seller_levels' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

        -- Step 9.2.1 - Preparing partition and backup for subcategory_x_seller_levels

        task_start_dttm := clock_timestamp();

            -- Getting name of current period partition

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg_dev.' || tables_prefix || '_subcategory_x_seller_levels')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

            -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk_dev.agg_backup_%2$s cascade;

                -- This backup won't be cleaned up automatically
                    -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk_dev.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk_dev.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk_dev.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            '9.2.1' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_subcategory_x_seller_levels' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 9.2.2 - Inserting into subcategory_x_seller_levels

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                agg_period_start,
                id,
                sub_category_id,
                seller_count,
                seller_level,
                sub_category_name
            )

                select
                    %3$L::bigint as "load_id",
                    transaction_timestamp() as processed_dttm,
                    %4$L::date as agg_period_start,
                    (row_number() over (
                        order by
                            m.sub_category_id,
                            (
                                case c.seller_level
                                    when 'bad_actor' then -3
                                    when 'low_quality' then -2
                                    when 'no_level' then -1
                                    when 'new_seller' then 0
                                    when 'level_one' then 1
                                    when 'level_two' then 2
                                    when 'level_trs' then 3
                                else -4 end
                            ) desc
                        ))::int as id,
                    m.sub_category_id,
                    c.cnt as seller_count,
                    c.seller_level,
                    m.sub_category_name
                from etl_wrk_dev.seller_counts c
                join etl_agg_dev.%5$s_subcategories m
                    on c.sub_category_id = m.sub_category_id
                    and m.agg_period_start = %4$L::date
                where
                    c.group_type = 'sub_category_id_x_seller_level';

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date,
        tables_prefix
        );

        get diagnostics
            row_cnt = row_count;

                -- Maintenance

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg_dev.' || tables_prefix || '_subcategory_x_seller_levels')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg_dev.%1$s_subcategory_x_seller_levels
                    add primary key (agg_period_start, id) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create unique index if not exists uidx_%1$s_subcat_sel_lvl
                on etl_agg_dev.%1$s_subcategory_x_seller_levels (agg_period_start, sub_category_id, seller_level) with (fillfactor = 100);
            create index if not exists idx_trg_subcat_name_%1$s_subcat_sel_lvl
                on etl_agg_dev.%1$s_subcategory_x_seller_levels using gin (sub_category_name gin_trgm_ops);

            alter table if exists etl_agg_dev.%1$s_subcategory_x_seller_levels
                replica identity using index %1$s_subcategory_x_seller_levels_pkey;

        $exec$,
        tables_prefix
        );

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

        execute format($exec$
            analyze etl_agg_dev.%1$s_subcategory_x_seller_levels;
        $exec$,
        tables_prefix
        );

        select
            '9.2.2' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_subcategory_x_seller_levels' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

        -- Step 9.3.1 - Preparing partition and backup for nested_subcategory_x_seller_levels

        task_start_dttm := clock_timestamp();

                -- Getting name of current period partition

        select
            case
                when tables_prefix = 'quarterly' then '3 month'
                else '1 ' || replace(tables_prefix, 'ly', '')
            end
        into partitioning_step;

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg_dev.' || tables_prefix || '_nested_subcategory_x_seller_levels')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

                -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk_dev.agg_backup_%2$s cascade;

                    -- This backup won't be cleaned up automatically
                        -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk_dev.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk_dev.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk_dev.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            '9.3.1' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_nested_subcategory_x_seller_levels' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;

            -- Step 9.3.2 - Inserting into nested_subcategory_x_seller_levels

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                agg_period_start,
                id,
                nested_sub_category_id,
                seller_count,
                seller_level,
                nested_sub_category_name
            )

                select
                    %3$L::bigint as "load_id",
                    transaction_timestamp() as processed_dttm,
                    %4$L::date as agg_period_start,
                    (row_number() over (
                        order by
                            m.nested_sub_category_id,
                            (
                                case c.seller_level
                                    when 'bad_actor' then -3
                                    when 'low_quality' then -2
                                    when 'no_level' then -1
                                    when 'new_seller' then 0
                                    when 'level_one' then 1
                                    when 'level_two' then 2
                                    when 'level_trs' then 3
                                else -4 end
                            ) desc
                        ))::int as id,
                    m.nested_sub_category_id,
                    c.cnt as seller_count,
                    c.seller_level,
                    m.nested_sub_category_name
                from etl_wrk_dev.seller_counts c
                join etl_agg_dev.%5$s_nested_subcategories m
                    on c.nested_sub_category_id = m.nested_sub_category_id
                    and m.agg_period_start = %4$L::date
                where
                    c.group_type = 'nested_sub_category_id_x_seller_level';

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date,
        tables_prefix
        );

        get diagnostics
            row_cnt = row_count;

                -- Maintenance

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg_dev.' || tables_prefix || '_nested_subcategory_x_seller_levels')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg_dev.%1$s_nested_subcategory_x_seller_levels
                    add primary key (agg_period_start, id) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create unique index if not exists uidx_%1$s_nest_subcat_sel_lvl
                on etl_agg_dev.%1$s_nested_subcategory_x_seller_levels (agg_period_start, nested_sub_category_id, seller_level) with (fillfactor = 100);
            create index if not exists idx_trg_nest_subcat_name_%1$s_nest_subcat_sel_lvl
                on etl_agg_dev.%1$s_nested_subcategory_x_seller_levels using gin (nested_sub_category_name gin_trgm_ops);

            alter table if exists etl_agg_dev.%1$s_nested_subcategory_x_seller_levels
                replica identity using index %1$s_nested_subcategory_x_seller_levels_pkey;

        $exec$,
        tables_prefix
        );

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

        execute format($exec$
            analyze etl_agg_dev.%1$s_nested_subcategory_x_seller_levels;
        $exec$,
        tables_prefix
        );

        if debug_mode is false then
            drop table if exists etl_wrk_dev.seller_counts cascade;
        end if;

        select
            '9.3.2' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_nested_subcategory_x_seller_levels' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 10 - Gathering statistics and restoring settings

        task_start_dttm := clock_timestamp();

        select
            '10' as src_step,
            'etl_dev' as tgt_schema,
            'etl_statistics' as tgt_name,
            'routine call' as op_type
        into log_rec;

            -- Run statistics

        perform etl_dev.etl_statistics(
            src_schema,
            metrics_start_dttm,
            load_id,
            debug_mode,
            dag_name,
            log_rec.src_step
        );

            -- Restoring table settings

        for period_partition_rec in

            select
                c.relnamespace::regnamespace::text as part_schema,
                c.relname::text as part_name
            from pg_catalog.pg_class c
            join pg_catalog.pg_inherits p
                on c."oid" = p.inhrelid
            where
                c.relkind = 'r'
                and c.relispartition is true
                and p.inhparent = any(array[
                        ('etl_agg_dev.' || tables_prefix || '_metrics')::regclass,
                        ('etl_agg_dev.' || tables_prefix || '_gigs')::regclass,
                        ('etl_agg_dev.' || tables_prefix || '_sellers')::regclass,
                        ('etl_agg_dev.' || tables_prefix || '_categories')::regclass,
                        ('etl_agg_dev.' || tables_prefix || '_subcategories')::regclass,
                        ('etl_agg_dev.' || tables_prefix || '_nested_subcategories')::regclass,
                        ('etl_agg_dev.' || tables_prefix || '_all_categories')::regclass,
                        ('etl_agg_dev.' || tables_prefix || '_category_x_seller_levels')::regclass,
                        ('etl_agg_dev.' || tables_prefix || '_subcategory_x_seller_levels')::regclass,
                        ('etl_agg_dev.' || tables_prefix || '_nested_subcategory_x_seller_levels')::regclass
                    ])
                and metrics_start_dttm::date
                    = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
                and (metrics_start_dttm::date + partitioning_step::interval)::date
                    = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date

        loop

            execute format($exec$

                alter table if exists %1$I.%2$I
                    set
                    (
                        autovacuum_enabled = true,
                        toast.autovacuum_enabled = true
                    );

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end loop;

        log_msg := format($fmt$Step %s completed (successful run of %I.%I(%L, %L, %L, %L, %L, %L))$fmt$,
            log_rec.src_step,
            log_rec.tgt_schema,
            log_rec.tgt_name,
            src_schema,
            metrics_start_dttm,
            load_id,
            debug_mode,
            dag_name,
            log_rec.src_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Exiting

        select
            '11' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            tables_prefix,
            metrics_start_dttm,
            trends_start_dttm,
            end_dttm,
            trends_intervals,
            trends_students_coeff,
            ratings_to_volume_coeff,
            overpriced_coeff,
            competition_score_percentile,
            competition_score_params,
            rank_scale_factor,
            debug_mode,
            dag_name,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        perform public.dblink_disconnect(load_id || '_conn');

        cluster etl_log.etl_log using etl_log_pkey;

        analyze etl_log.etl_log;

        -- returning valid Python dictionary with load_id, runtime and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;

            -- Catching exceptions
        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            perform public.dblink_disconnect(load_id || '_conn');

            cluster etl_log.etl_log using etl_log_pkey;

            analyze etl_log.etl_log;

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;