create or replace function etl_agg_dev.af_agg_gigs_insert
(
    tables_prefix text,
    metrics_start_dttm timestamptz,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '10.1.2'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

        partitioning_step text;
        period_partition_rec record;
        empty_partition_flg boolean;
        root_pkey text;
        partition_pkey text;

        stats_columns_list text[];

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            tables_prefix,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Preparations

        task_start_dttm := clock_timestamp();

            -- Getting name of current period partition

        select
            case
                when tables_prefix = 'quarterly' then '3 month'
                else '1 ' || replace(tables_prefix, 'ly', '')
            end
        into partitioning_step;

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg_dev.' || tables_prefix || '_gigs')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

            -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk_dev.agg_backup_%2$s cascade;

                -- This backup won't be cleaned up automatically
                    -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk_dev.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk_dev.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk_dev.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            high_level_step || '.1' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_gigs' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 2 - Materializing gigs metrics

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                seller_created_at,
                seller_fiverr_created_at,
                seller_scraped_at,
                gig_created_at,
                gig_fiverr_created_at,
                gig_scraped_at,
                min_revenue,
                avg_revenue,
                max_revenue,
                min_total_historical_revenue,
                avg_total_historical_revenue,
                max_total_historical_revenue,
                agg_period_start,
                seller_id,
                fiverr_seller_id,
                gig_id,
                fiverr_gig_id,
                category_id,
                sub_category_id,
                nested_sub_category_id,
                global_rank,
                seller_ratings_count,
                ratings_count,
                sales_volume,
                total_historical_sales_volume,
                min_price,
                avg_price,
                max_price,
                heuristic,
                is_pro,
                is_active,
                is_trend_valid,
                sales_volume_growth_percent,
                competition_score_for_gig_by_category,
                competition_score_for_gig_by_subcategory,
                competition_score_for_gig_by_nested_subcategory,
                market_share_for_gig_by_category,
                market_share_for_gig_by_subcategory,
                market_share_for_gig_by_nested_subcategory,
                seller_rating,
                rating,
                revenue_growth_percent,
                revenue_trend,
                seller_level,
                seller_name,
                title,
                cached_slug,
                category_name,
                sub_category_name,
                nested_sub_category_name,
                seller_country,
                seller_country_code,
                image_preview,
                seller_profile_image
            )

                select
                    %3$L::bigint as "load_id",
                    transaction_timestamp() as processed_dttm,
                    seller_created_at,
                    seller_fiverr_created_at,
                    seller_scraped_at,
                    gig_created_at,
                    gig_fiverr_created_at,
                    gig_scraped_at,
                    round(min_revenue)::bigint as min_revenue,
                    round(avg_revenue)::bigint as avg_revenue,
                    round(max_revenue)::bigint as max_revenue,
                    round(min_total_historical_revenue)::bigint as min_total_historical_revenue,
                    round(avg_total_historical_revenue)::bigint as avg_total_historical_revenue,
                    round(max_total_historical_revenue)::bigint as max_total_historical_revenue,
                    %4$L::date as agg_period_start,
                    seller_id,
                    fiverr_seller_id,
                    gig_id,
                    fiverr_gig_id,
                    category_id,
                    sub_category_id,
                    nested_sub_category_id,
                    (dense_rank() over (order by
                        is_active desc nulls last, -- true first
                        coeff_revenue desc nulls last, -- bigger revenue first
                        coeff_trend desc nulls last, -- bigger trend growth first
                        coeff_rating desc nulls last, -- bigger rating first
                        coalesce(gig_fiverr_created_at, gig_scraped_at) asc nulls last, -- oldest first
                        coalesce(seller_fiverr_created_at, seller_scraped_at) asc nulls last, -- oldest first
                        gig_id asc nulls last -- oldest first
                    ))::int as global_rank,
                    seller_ratings_count,
                    ratings_count,
                    sales_volume,
                    total_historical_sales_volume,
                    round(min_price)::int as min_price,
                    round(avg_price)::int as avg_price,
                    round(max_price)::int as max_price,
                    heuristic,
                    is_pro,
                    is_active,
                    is_trend_valid,
                    round(sales_volume_growth_percent, 2) as sales_volume_growth_percent,
                    round(competition_score_for_gig_by_category, 2) as competition_score_for_gig_by_category,
                    round(competition_score_for_gig_by_subcategory, 2) as competition_score_for_gig_by_subcategory,
                    round(competition_score_for_gig_by_nested_subcategory, 2) as competition_score_for_gig_by_nested_subcategory,
                    round(market_share_for_gig_by_category, 2) as market_share_for_gig_by_category,
                    round(market_share_for_gig_by_subcategory, 2) as market_share_for_gig_by_subcategory,
                    round(market_share_for_gig_by_nested_subcategory, 2) as market_share_for_gig_by_nested_subcategory,
                    round(seller_rating, 2) as seller_rating,
                    round(rating, 2) as rating,
                    round(revenue_growth_percent, 2) as revenue_growth_percent,
                    round(revenue_trend, 2) as revenue_trend,
                    seller_level,
                    seller_name,
                    title,
                    cached_slug,
                    category_name,
                    sub_category_name,
                    nested_sub_category_name,
                    seller_country,
                    seller_country_code,
                    image_preview,
                    seller_profile_image
                from etl_wrk_dev.gigs_attributes;

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt = row_count;

            -- Maintenance

                -- Indices

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg_dev.' || tables_prefix || '_gigs')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg_dev.%1$s_gigs
                    add primary key (agg_period_start, gig_id) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create unique index if not exists uidx_fiverr_gig_id_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, fiverr_gig_id) with (fillfactor = 100);
            create index if not exists idx_seller_id_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, seller_id) with (fillfactor = 100);
            create index if not exists idx_fiverr_seller_id_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, fiverr_seller_id) with (fillfactor = 100);
            create index if not exists idx_category_id_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, category_id) with (fillfactor = 100);
            create index if not exists idx_sub_category_id_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, sub_category_id) with (fillfactor = 100);
            create index if not exists idx_nested_sub_category_id_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, nested_sub_category_id) with (fillfactor = 100);
            create index if not exists idx_categories_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, category_id, sub_category_id, nested_sub_category_id) with (fillfactor = 100);
            create unique index if not exists uidx_%1$s_gigs_ranks
                on etl_agg_dev.%1$s_gigs (agg_period_start, global_rank desc) with (fillfactor = 100);
            create index if not exists idx_cached_slug_%1$s_gigs
                on etl_agg_dev.%1$s_gigs (agg_period_start, cached_slug) with (fillfactor = 100);
            create index if not exists idx_trg_title_%1$s_gigs
                on etl_agg_dev.%1$s_gigs using gin (title gin_trgm_ops);
            create index if not exists idx_trg_seller_name_%1$s_gigs
                on etl_agg_dev.%1$s_gigs using gin (seller_name gin_trgm_ops);
            create index if not exists idx_trg_cat_name_%1$s_gigs
                on etl_agg_dev.%1$s_gigs using gin (category_name gin_trgm_ops);
            create index if not exists idx_trg_subcat_name_%1$s_gigs
                on etl_agg_dev.%1$s_gigs using gin (sub_category_name gin_trgm_ops);
            create index if not exists idx_trg_nest_subcat_name_%1$s_gigs
                on etl_agg_dev.%1$s_gigs using gin (nested_sub_category_name gin_trgm_ops);

            alter table if exists etl_agg_dev.%1$s_gigs
                replica identity using index %1$s_gigs_pkey;

        $exec$,
        tables_prefix
        );

                -- CLUSTER

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

                -- CREATE STATISTICS and ANALYZE

        stats_columns_list := array[
                'gig_id', 'seller_id', 'fiverr_gig_id', 'fiverr_seller_id',
                'category_id', 'sub_category_id', 'nested_sub_category_id'
            ];

        execute format($exec$
            create statistics if not exists %1$I.%2$s
            on %3$s
            from %1$I.%4$I;
        $exec$,
        period_partition_rec.part_schema,
        substring('stats_' || period_partition_rec.part_name, 1, 63),
        array_to_string(stats_columns_list, ', '),
        period_partition_rec.part_name
        );

        execute format($exec$
            analyze etl_agg_dev.%1$s_gigs;
        $exec$,
        tables_prefix
        );

        if debug_mode is false then
            drop table if exists etl_wrk_dev.gigs_attributes cascade;
        end if;

        select
            high_level_step || '.2' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_gigs' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.3' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            tables_prefix,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;