create or replace function etl_agg_dev.af_agg_gigs_attributes
(
    tables_prefix text,
    metrics_start_dttm timestamptz,
    end_dttm timestamptz default (current_date::timestamp at time zone 'UTC' + '1 day'::interval)::timestamptz,
    rank_scale_factor numeric default 0.0001, -- scaling coefficient for relevance_sorting function
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '10.1.1'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            tables_prefix,
            end_dttm,
            rank_scale_factor,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Gathering gigs' attributes

        task_start_dttm := clock_timestamp();

        execute format($exec$

            drop table if exists etl_wrk_dev.gigs_attributes cascade;

            create unlogged table if not exists etl_wrk_dev.gigs_attributes
                with
                (
                    autovacuum_enabled = false,
                    toast.autovacuum_enabled = false
                )
                as

                select
                    g.seller_created_at,
                    g.seller_fiverr_created_at,
                    g.seller_scraped_at,
                    g.gig_created_at,
                    g.gig_fiverr_created_at,
                    g.gig_scraped_at,
                    g.seller_id,
                    g.fiverr_seller_id,
                    g.gig_id,
                    g.fiverr_gig_id,
                    g.category_id,
                    g.sub_category_id,
                    g.nested_sub_category_id,
                    g.seller_ratings_count,
                    g.gig_ratings_count as ratings_count,
                    g.volume_by_gig as sales_volume,
                    g.total_historical_volume_by_gig as total_historical_sales_volume,
                    g.heuristic,
                    g.gig_is_pro as is_pro,
                    g.gig_is_active as is_active,
                    g.is_trend_valid_for_gig as is_trend_valid,
                    g.sales_volume_growth_percent_by_gig as sales_volume_growth_percent,
                    g.competition_score_for_gig_by_category,
                    g.competition_score_for_gig_by_subcategory,
                    g.competition_score_for_gig_by_nested_subcategory,
                    g.market_share_for_gig_by_category,
                    g.market_share_for_gig_by_subcategory,
                    g.market_share_for_gig_by_nested_subcategory,
                    g.seller_rating,
                    g.gig_rating as rating,
                    g.min_price_by_gig as min_price,
                    g.avg_price_by_gig as avg_price,
                    g.max_price_by_gig as max_price,
                    g.min_revenue_by_gig as min_revenue,
                    g.avg_revenue_by_gig as avg_revenue,
                    g.max_revenue_by_gig as max_revenue,
                    g.min_total_historical_revenue_by_gig as min_total_historical_revenue,
                    g.avg_total_historical_revenue_by_gig as avg_total_historical_revenue,
                    g.max_total_historical_revenue_by_gig as max_total_historical_revenue,
                    g.revenue_growth_percent_by_gig as revenue_growth_percent,
                    g.trend_by_gig as revenue_trend,
                    etl_dev.relevance_sorting(
                            g.min_revenue_by_gig,
                            coalesce(g.gig_fiverr_created_at, g.gig_scraped_at),
                            %2$L::timestamptz,
                            %3$L::numeric
                        ) as coeff_revenue,
                    etl_dev.relevance_sorting(
                            g.trend_by_gig,
                            coalesce(g.gig_fiverr_created_at, g.gig_scraped_at),
                            %2$L::timestamptz,
                            %3$L::numeric
                        ) as coeff_trend,
                    etl_dev.relevance_sorting(
                            100 * coalesce((g.gig_rating * g.gig_ratings_count + r.avg_gig_rating * r.stddev_gig_rating)::numeric
                                / nullif((g.gig_ratings_count + r.stddev_gig_rating), 0)::numeric, 0::numeric),
                            coalesce(g.gig_fiverr_created_at, g.gig_scraped_at),
                            %2$L::timestamptz,
                            %3$L::numeric
                        ) as coeff_rating,
                    g.seller_level,
                    g.seller_name,
                    case
                        when g.agency_slug is null then 'I will ' || g.gig_title
                        else 'Our agency will ' || g.gig_title
                    end as title,
                    g.gig_cached_slug as cached_slug,
                    g.category_name,
                    g.sub_category_name,
                    g.nested_sub_category_name,
                    g.seller_country,
                    g.seller_country_code,
                    g.gig_preview_url as image_preview,
                    g.seller_profile_image
                from etl_agg_dev.%1$s_metrics g
                cross join etl_wrk_dev.metrics_ratings_stats as r
                where
                    g.agg_period_start = %4$L::date;

        $exec$,
        tables_prefix,
        end_dttm,
        rank_scale_factor,
        metrics_start_dttm::date
        );

        get diagnostics
            row_cnt := row_count;

        analyze etl_wrk_dev.gigs_attributes;

        select
            high_level_step || '.1' as src_step,
            'etl_wrk_dev' as tgt_schema,
            'gigs_attributes' as tgt_name,
            'create table as' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (materialize ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.2' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            tables_prefix,
            end_dttm,
            rank_scale_factor,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;