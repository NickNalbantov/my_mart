create or replace function etl_agg_dev.af_agg_subcategory_x_seller_levels_insert
(
    tables_prefix text,
    metrics_start_dttm timestamptz,
    load_id bigint default to_char(transaction_timestamp() at time zone 'UTC', 'yyyymmddhh24miss')::bigint,
    debug_mode boolean default false,
    dag_name text default 'manual_launch',
    high_level_step text default '11.4'
)
returns jsonb
language plpgsql
strict
security definer
set timezone = 'UTC'
set search_path = public, pg_temp
as
$func$

    declare

        log_msg text;
        log_rec record;
        log_type_def text := 'NOTICE';
        func_oid oid;
        src_schema text;
        src_name text;
        call_stack text;
        row_cnt bigint;
        task_start_dttm timestamptz;
        total_runtime interval;
        result jsonb;

        exception_sqlstate text;
        exception_message text;
        exception_context text;
        exception_detail text;
        exception_hint text;

        partitioning_step text;
        period_partition_rec record;
        empty_partition_flg boolean;
        root_pkey text;
        partition_pkey text;

    begin

        -- Starting

        get diagnostics
            func_oid := pg_routine_oid;

        select
            pronamespace::regnamespace::text,
            proname::text
        into
            src_schema,
            src_name
        from pg_catalog.pg_proc
        where
            "oid" = func_oid;

        select
            high_level_step || '.0' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'start' as op_type
        into log_rec;

        log_msg := format($fmt$Start of routine %I.%I(%L, %L, %L, %L, %L)$fmt$,
            src_schema,
            src_name,
            tables_prefix,
            load_id,
            debug_mode,
            dag_name,
            high_level_step
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            '0'::interval, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;


        -- Step 1 - Preparations

        task_start_dttm := clock_timestamp();

            -- Getting name of current period partition

        select
            case
                when tables_prefix = 'quarterly' then '3 month'
                else '1 ' || replace(tables_prefix, 'ly', '')
            end
        into partitioning_step;

        select
            c.relnamespace::regnamespace::text as part_schema,
            c.relname::text as part_name
        into period_partition_rec
        from pg_catalog.pg_class c
        join pg_catalog.pg_inherits p
            on c."oid" = p.inhrelid
        where
            c.relkind = 'r'
            and c.relispartition is true
            and p.inhparent = ('etl_agg_dev.' || tables_prefix || '_subcategory_x_seller_levels')::regclass
            and metrics_start_dttm::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(FROM \(''))(.*?)(?=(''\)))')::date
            and (metrics_start_dttm::date + partitioning_step::interval)::date
                = substring(pg_catalog.pg_get_expr(c.relpartbound, c."oid"), '(?<=(TO \(''))(.*?)(?=(''\)))')::date;

            -- If current period partition is not empty - backup it before truncation

        execute format($exec$
            select
                false
            from %1$I.%2$I
            limit 1
        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        )
        into empty_partition_flg;

        if empty_partition_flg is false then
            execute format($exec$

                drop table if exists etl_wrk_dev.agg_backup_%2$s cascade;

                -- This backup won't be cleaned up automatically
                    -- But there won't be more than one version of the backup for each partition
                create table if not exists etl_wrk_dev.agg_backup_%2$s
                (like %1$I.%2$I including all);

                insert into etl_wrk_dev.agg_backup_%2$s
                    select *
                    from %1$I.%2$I;

                analyze etl_wrk_dev.agg_backup_%2$s;

            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name
            );

        end if;

        execute format($exec$

            alter table if exists %1$I.%2$I
            set
            (
                autovacuum_enabled = false,
                toast.autovacuum_enabled = false
            );

            truncate table %1$I.%2$I restart identity cascade; -- truncating current period partition

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name
        );

        select
            high_level_step || '.1' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_subcategory_x_seller_levels' as tgt_name,
            'ddl' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (preparations for inserting to ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Step 2 - Materializing subcategory_x_seller_levels

        task_start_dttm := clock_timestamp();

        execute format($exec$

            insert into %1$I.%2$I
            (
                "load_id",
                processed_dttm,
                agg_period_start,
                id,
                sub_category_id,
                seller_count,
                seller_level,
                sub_category_name
            )

                select
                    %3$L::bigint as "load_id",
                    transaction_timestamp() as processed_dttm,
                    %4$L::date as agg_period_start,
                    (row_number() over (
                        order by
                            m.sub_category_id,
                            (
                                case c.seller_level
                                    when 'bad_actor' then -3
                                    when 'low_quality' then -2
                                    when 'no_level' then -1
                                    when 'new_seller' then 0
                                    when 'level_one' then 1
                                    when 'level_two' then 2
                                    when 'level_trs' then 3
                                else -4 end
                            ) desc
                        ))::int as id,
                    m.sub_category_id,
                    c.cnt as seller_count,
                    c.seller_level,
                    m.sub_category_name
                from etl_wrk_dev.seller_counts c
                join etl_agg_dev.%5$s_subcategories m
                    on c.sub_category_id = m.sub_category_id
                    and m.agg_period_start = %4$L::date
                where
                    c.group_type = 'sub_category_id_x_seller_level';

        $exec$,
        period_partition_rec.part_schema,
        period_partition_rec.part_name,
        load_id,
        metrics_start_dttm::date,
        tables_prefix
        );

        get diagnostics
            row_cnt = row_count;

            -- Maintenance

        select
            ci.relname::text
        into root_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = ('etl_agg_dev.' || tables_prefix || '_subcategory_x_seller_levels')::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if root_pkey is null then
            execute format($exec$
                alter table if exists etl_agg_dev.%1$s_subcategory_x_seller_levels
                    add primary key (agg_period_start, id) with (fillfactor = 100);
            $exec$,
            tables_prefix
            );
        end if;

        execute format($exec$

            create unique index if not exists uidx_%1$s_subcat_sel_lvl
                on etl_agg_dev.%1$s_subcategory_x_seller_levels (agg_period_start, sub_category_id, seller_level) with (fillfactor = 100);
            create index if not exists idx_trg_subcat_name_%1$s_subcat_sel_lvl
                on etl_agg_dev.%1$s_subcategory_x_seller_levels using gin (sub_category_name gin_trgm_ops);

            alter table if exists etl_agg_dev.%1$s_subcategory_x_seller_levels
                replica identity using index %1$s_subcategory_x_seller_levels_pkey;

        $exec$,
        tables_prefix
        );

        select
            ci.relname::text
        into partition_pkey
        from pg_catalog.pg_index i
        join pg_catalog.pg_class ci
            on i.indexrelid = ci."oid"
        where
            i.indrelid = (period_partition_rec.part_schema || '.' || period_partition_rec.part_name)::regclass
            and i.indisprimary is true
            and i.indisvalid is true
            and i.indisready is true
            and i.indislive is true
            and i.indcheckxmin is false;

        if partition_pkey is not null then
            execute format($exec$
                cluster %1$I.%2$I using %3$I;
            $exec$,
            period_partition_rec.part_schema,
            period_partition_rec.part_name,
            partition_pkey
            );
        end if;

        execute format($exec$
            analyze etl_agg_dev.%1$s_subcategory_x_seller_levels;
        $exec$,
        tables_prefix
        );

        select
            high_level_step || '.2' as src_step,
            'etl_agg_dev' as tgt_schema,
            tables_prefix || '_subcategory_x_seller_levels' as tgt_name,
            'insert' as op_type
        into log_rec;

        log_msg := 'Step ' || log_rec.src_step || ' completed (insert into ' || log_rec.tgt_schema || '.' || log_rec.tgt_name || ')';

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            clock_timestamp() - task_start_dttm, load_id, row_cnt, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        raise notice '%. % - % - % rows inserted', log_rec.src_step, clock_timestamp(), log_msg, row_cnt;


        -- Logging

        select
            high_level_step || '.3' as src_step,
            null as tgt_schema,
            null as tgt_name,
            'end' as op_type
        into log_rec;

        total_runtime := clock_timestamp() - transaction_timestamp();

        log_msg := format(E'End of routine %I.%I(%L, %L, %L, %L, %L)\nTotal runtime - %s',
            src_schema,
            src_name,
            tables_prefix,
            load_id,
            debug_mode,
            dag_name,
            high_level_step,
            total_runtime
        );

        get diagnostics
            call_stack := pg_context;

        perform etl_log.etl_log_writer(
            total_runtime, load_id, null, debug_mode, log_type_def, dag_name, src_schema, src_name,
            log_rec.src_step, log_rec.tgt_schema, log_rec.tgt_name, log_rec.op_type, call_stack, log_msg
        );

        -- returning valid Python dictionary with load_id, runtime, row count and 'Success' status
        result := (
            '{"load_id": ' || load_id
            || ', "runtime": "' || total_runtime
            || '", "row_count": ' || row_cnt
            || ', "status": "Success"}'
        )::jsonb;

        raise notice '%. % - %', log_rec.src_step, clock_timestamp(), log_msg;

        return result;


        -- Catching exceptions

        exception when others then
            get stacked diagnostics
                exception_sqlstate := returned_sqlstate,
                exception_message := message_text,
                exception_context := pg_exception_context,
                exception_detail := pg_exception_detail,
                exception_hint := pg_exception_hint;

            log_msg := 'SQLSTATE: ' || exception_sqlstate || repeat(E'\n', 2)
                || 'MESSAGE: ' || exception_message || repeat(E'\n', 2)
                || 'CONTEXT: ' || exception_context
                || case when coalesce(exception_detail, '') != '' then repeat(E'\n', 2) || 'DETAIL: ' || exception_detail else '' end
                || case when coalesce(exception_hint, '') != '' then repeat(E'\n', 2) || 'HINT: ' || exception_hint else '' end;

            total_runtime := clock_timestamp() - transaction_timestamp();

            perform etl_log.etl_log_writer(
                total_runtime, load_id, null, debug_mode, 'ERROR', dag_name,
                null, null, null, null, null, null, exception_context, log_msg
            );

            -- returning valid Python dictionary with load_id, runtime and error status
            result := (
                '{"load_id": ' || load_id
                || ', "runtime": "' || total_runtime
                || '", "status": {'
                    || '"SQLSTATE": "' || exception_sqlstate || '", '
                    || '"MESSAGE": ' || to_jsonb(exception_message) || ', '
                    || '"CONTEXT": ' || to_jsonb(exception_context) || ', '
                    || '"DETAIL": ' || to_jsonb(exception_detail) || ', '
                    || '"HINT": ' || to_jsonb(exception_hint)
                || '}}'
            )::jsonb;

            raise notice E'Error occured at %\n\n%', clock_timestamp(), log_msg;

            return result;

    end;
$func$;