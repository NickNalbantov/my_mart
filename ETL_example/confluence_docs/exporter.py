"""Module for exporting Confluence space page structure into a tree of local folders and exporting Confluence pages as .pdf and .docx documents into these directories"""

import configparser
import logging
import sys

from pathlib import Path
from atlassian import Confluence

logging.basicConfig(level = 'INFO', stream = sys.stdout, format = '%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger("confluence_exporter")

CONFIG_FILE = f'{Path(__file__).resolve().parent}/confluence.ini'

config = configparser.ConfigParser(allow_no_value = True, empty_lines_in_values = True)
config.read(CONFIG_FILE, 'utf8')

login = config['Login Settings']
pages = config['Pages']

# Recursive search of childrens
def children_tree(confluence: Confluence, children: list) -> list:
    "Recursive search of pages' childrens and saving their metadata"
    res_list = []
    for child in children:
        grandchildren = list(confluence.get_child_pages(child['id']))
        for grandchild in grandchildren:
            grandchild['page_path'] = f"{child['page_path']}/{grandchild['title']}"
        res_list.append(child)
        if grandchildren:
            res_list.extend(children_tree(confluence, grandchildren))
    return res_list

# Folder creation by title
def mkdir_by_title(title: str):
    "Creates local folders"
    docs_dir = f'{Path(__file__).resolve().parent}/docs/{title}'
    Path(f'{docs_dir}').mkdir(mode = 755, parents = True, exist_ok = True)


# Creating folder structure from CF pages structure for PDF and DOCX exports
def file_processing(confluence: Confluence, parent_id):
    "Exports folder structure and files from Confluence"
    parent = confluence.get_page_by_id(parent_id)
    parent['page_path'] = f"{parent['title']}"

    first_gen_children = list(confluence.get_child_pages(parent_id))
    for child in first_gen_children:
        child['page_path'] = f"{parent['title']}/{child['title']}"

    children = children_tree(confluence, first_gen_children)
    children.insert(0, parent)

    err_file_list = []

    if children:
        for child in children:

            page_path = child['page_path']
            page_title = child['title']
            page_id = child['id']

            mkdir_by_title(child['page_path'])

            try:
                pdf_name = f'{Path(__file__).resolve().parent}/docs/{page_path}/{page_title}.pdf'
                content = cf.export_page(page_id)
                with open(pdf_name, 'wb') as file_pdf:
                    file_pdf.write(content)
                logger.info('File %s has been created', pdf_name)
            except Exception as err:
                err_file_list.extend([pdf_name])
                logger.error('File %s has not been created.\nError: %s', pdf_name, err)

            try:
                docx_name = f'{Path(__file__).resolve().parent}/docs/{page_path}/{page_title}.docx'
                content = cf.get_page_as_word(page_id)
                with open(docx_name, 'wb') as file_docx:
                    file_docx.write(content)
                logger.info('File %s has been created', docx_name)
            except Exception as err:
                err_file_list.extend([docx_name])
                logger.error('File %s has not been created.\nError: %s', docx_name, err)

    return err_file_list


logger.info('File processing has been started')

cf = Confluence(
    url = login['url'],
    username = login['username'],
    password = login['api_token'],
    api_version = 'cloud',
    timeout = 86400
    )

error_file_list = file_processing(cf, pages['parent_id'])
# Some of the biggest pages can't be exported to PDF even manually, so there's only DOCX version for a while
# For now - just catching names of these pages into error_file_list
logger.info('File processing has been finished')
logger.warning('Failed exports: %s', error_file_list)
