"""Module for exporting Confluence space page structure into a tree of local folders and exporting Confluence pages as .pdf and .docx documents into these directories
Accepts --pages (-p) argument with a list of ID of Confluence pages to export
Example: python export_chosen_pages.py -p 121143297 119504918 119504898 119537665 118718504
"""

import configparser
import argparse
import logging
import sys

from pathlib import Path
from atlassian import Confluence

logging.basicConfig(level = 'INFO', stream = sys.stdout, format = '%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger("confluence_exporter")

CONFIG_FILE = f'{Path(__file__).resolve().parent}/confluence.ini'

config = configparser.ConfigParser(allow_no_value = True, empty_lines_in_values = True)
config.read(CONFIG_FILE, 'utf8')

login = config['Login Settings']

# Arguments parsing
descr = "List of ID of Confluence pages to export" \
    "Accepts --pages and -p options" \
    "Empty by default" \
    "Example: python export_chosen_pages.py -p 121143297 119504918 119504898 119537665 118718504"

parser = argparse.ArgumentParser(description = descr)

parser.add_argument(
        '-p',
        '--pages',
        metavar = 'Pages',
        type = int,
        const = None,
        nargs = '*',
        help = descr,
        action = 'store'
    )

pages_id = vars(parser.parse_args())['pages']


# File export
def file_processing(confluence: Confluence, pages_id_list: list):
    "Exports files from Confluence"
    pages_meta = []

    for page_id in pages_id_list:
        pages_meta.append(confluence.get_page_by_id(page_id))

    err_file_list = []

    if pages_meta:
        for page in pages_meta:

            page_title = page['title']
            page_id = page['id']

            try:
                pdf_name = f'{Path(__file__).resolve().parent}/{page_title}.pdf'
                content = cf.export_page(page_id)
                with open(pdf_name, 'wb') as file_pdf:
                    file_pdf.write(content)
                logger.info('File %s has been created', pdf_name)
            except Exception as err:
                err_file_list.extend([pdf_name])
                logger.error('File %s has not been created.\nError: %s', pdf_name, err)

            try:
                docx_name = f'{Path(__file__).resolve().parent}/{page_title}.docx'
                content = cf.get_page_as_word(page_id)
                with open(docx_name, 'wb') as file_docx:
                    file_docx.write(content)
                logger.info('File %s has been created', docx_name)
            except Exception as err:
                err_file_list.extend([docx_name])
                logger.error('File %s has not been created.\nError: %s', docx_name, err)

    return err_file_list


logger.info('File processing has been started')

cf = Confluence(
    url = login['url'],
    username = login['username'],
    password = login['api_token'],
    api_version = 'cloud',
    timeout = 86400
    )

error_file_list = file_processing(cf, pages_id)
# Some of the biggest pages can't be exported to PDF even manually, so there's only DOCX version for a while
# For now - just catching names of these pages into error_file_list
logger.info('File processing has been finished')
logger.warning('Failed exports: %s', error_file_list)
