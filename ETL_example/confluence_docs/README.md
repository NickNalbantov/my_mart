Just a copy of documentation from Confluence [Data](https://fiverr-dev.atlassian.net/wiki/spaces/MVP/pages/106954757/Data) page and all its ancestors in .PDF and .DOCX formats

Folders' tree and files are generated automatically by <em>exporter.py</em> script

Some of the biggest pages (such as [main_job](https://fiverr-dev.atlassian.net/wiki/spaces/MVP/pages/124223491/main+job) or [metrics_func](https://fiverr-dev.atlassian.net/wiki/spaces/MVP/pages/123764737/metrics+func)) sometimes can't be exported to .PDF even manually, so there's only .DOCX version for a while

---

*export_schosen_pages.py* script does not try to generate folders structure for the pages. Instead, it exports to .PDF and .DOCX only those pages that are specified in --pages (-p) argparse argument

Example:
```bash
python export_schosen_pages.py --pages 106954757 121143297 119504918
```

Resulting files will be placed in the same directory there the script is located and must be moved to the right folder manually.