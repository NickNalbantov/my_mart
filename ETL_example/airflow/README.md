# Airflow repo README

Check more details about ETL and maintenance functions in [ETL repo README](https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/README.md)

---

## /dags

Python scripts with Airflow DAGs and supporting custom modules (in /classes folder)

Code has been styled with [Pylint](https://pypi.org/project/pylint/) recommendations with a few modifications in its settings:

- "--max-line-length=175"
- "--disable=E1129"
- "--disable=W0718"
- "--disable=W0719"
- "--disable=W0106"
- "--disable=C0302"
- "--disable=C0103"

Here:

- [E1129](https://pylint.readthedocs.io/en/latest/user_guide/messages/error/not-context-manager.html) - false positive trigger on context managers with objects from psycopg library

- [W0718](https://pylint.readthedocs.io/en/latest/user_guide/messages/warning/broad-exception-caught.html) and [W0719](https://pylint.readthedocs.io/en/stable/user_guide/messages/warning/broad-exception-raised.html) - warnings about broad exception catching. In our case, it has been done on purpose in order to send all exceptions from certain blocks of code to the log without breaking the script

- [W0106](https://pylint.readthedocs.io/en/latest/user_guide/messages/warning/expression-not-assigned.html) - "Expression not assigned" warning gives false positive on Airflow task dependency syntax

- [C0103](https://pylint.readthedocs.io/en/latest/user_guide/messages/convention/invalid-name.html) and [C0302](https://pylint.readthedocs.io/en/latest/user_guide/messages/convention/too-many-lines.html) are just a cosmetical issues

pip requirements are listed in requirements.txt

---

### ETL DAGs

DAGs for data processing in ETL database

#### etl_regular.py

DAG for launching regular ETL operations

##### DAG params

- *contour* - enum ['prod', 'dev'] for choosing which version of ETL to launch
- *parallel_mode* - runs parallelized DAG branch if True, runs sequential DAG branch if False
- *metrics_start_dttm* - lower bound for general metrics calculation time period
- *trends_start_dttm* - lower bound for trends calculation time period
- *end_dttm* - upper bound for calculation time period
- *trends_intervals* (from 2 to 12 with step 1) - amount of iterations for the calculations of revenue trend lines
- *trends_students_coeff* (> 0) - critical value of correlation coefficient for Student's coefficient (by default - confidence interval = 0.95, t-statistics = 1.96)
- *ratings_to_volume_coeff* (from 0 to 1) - coefficient of ratings to sales conversion
- *overpriced_coeff* - crutch to filter gigs with suspiciously high prices (higher than weighted average price in subcategory multiplied by this coefficient)
- *competition_score_percentile* (from 0 to 1) - percentile of inversed sales ratio metrics which used for calculations of gigs' competition scores
- *competition_score_params* - set of tuning parameters for etl.competition_score function for calculating competition_score_xxx attributes, see [function](https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/competition_score.sql) and [notebook](https://gitlab.com/fiverr-dev/data-engineering/analytics-notebooks/-/blob/main/competition_score/gigs_competition.py)
    {
        *volume_exp_scale_coeff* (no limits) - scaling factor that determines how fast sales volume affects competition score
        *sigmoid_scale_coeff* (no limits) - scaling factor that makes transitions between competition score levels more sharp
        *sigmoid_shift_coeff* (from 0 to 1) - correction factor that shifts the center of the scale for transitions between competition score levels
        *competition_score_scale_coeff* (from 0 to 1) - scaling factor that helps to avoid values close to the borders [0, 1]
        *competition_score_add_coeff* (from 0 to 1) - correction factor that helps to avoid values close to the borders [0, 1]
        *volume_scale_coeff* (from 0 to 1) - scale factor that reduces outbursts for largest volume values
    }
- *rank_scale_factor* - scaling argument for etl.relevance_sorting function
- *debug_mode* - saves temporary tables if True, drops it if False
- *cleanup_preserve* (from 0 to 90 with step 10) - amount of backup tables that will be saved after the end of current run
- *cluster_threshold* (from 0 to 1) - threshold value of the ratio between modified / inserted rows and n_live_tup or the ratio of uncorrelated keys of the index that used for clusterization for *pre_materialization* targets'. CLUSTER + ANALYZE commands are being launched if threshold is reached
- *ddl_change_mode* - (applies only when *parallel_mode* is False) if True then targets (non-technical tables only) will be dropped and recreated instead of truncation (if False)
- *load_id* - identificator of the ETL run across all DAGs for a database log

##### Default values in DAG

```python
{
    "contour": 'prod',
    "parallel_mode": True,
    "metrics_start_dttm": pendulum.now(tz = "UTC").start_of("day").subtract(months = 1).isoformat(),
    "trends_start_dttm": pendulum.now(tz = "UTC").start_of("day").subtract(months = 3).isoformat(),
    "end_dttm": pendulum.now(tz = "UTC").add(days = 1).start_of("day").isoformat(),
    "trends_intervals": "3",
    "trends_students_coeff": 1.96,
    "ratings_to_volume_coeff": 0.8,
    "overpriced_coeff": 10,
    "competition_score_percentile": 0.95,
    "competition_score_params":
        {
            "volume_exp_scale_coeff": 100,
            "sigmoid_scale_coeff": -5,
            "sigmoid_shift_coeff": 0.5,
            "competition_score_scale_coeff": 0.8,
            "competition_score_add_coeff": 0.1,
            "volume_scale_coeff": 0.5
        },
    "rank_scale_factor": 0.0001,
    "debug_mode": False,
    "cleanup_preserve": 40,
    "cluster_threshold": 0.2,
    "ddl_change_mode": False,
    "load_id": int(pendulum.now(tz = "UTC").format('YYYYMMDDHHmmss'))
}
```

If DAG will be triggered with custom values of arguments then they'll override all defaults and will be passed to function call in DB.

##### Example of custom configuration

```json
{
    "contour": "dev",
    "parallel_mode": false,
    "metrics_start_dttm": "2023-04-26T07:27:22.732669",
    "trends_start_dttm": "2023-02-26T07:27:22.732669",
    "end_dttm": "2023-05-26T07:27:22.732669",
    "trends_intervals": 4,
    "trends_students_coeff": 1.645,
    "ratings_to_volume_coeff": 0.7,
    "overpriced_coeff": 3,
    "competition_score_percentile": 0.75,
    "competition_score_params":
        {
            "volume_exp_scale_coeff": 50,
            "sigmoid_scale_coeff": -10,
            "sigmoid_shift_coeff": 0.75,
            "competition_score_scale_coeff": 0.6,
            "competition_score_add_coeff": 0.2,
            "volume_scale_coeff": 0.8
        },
    "rank_scale_factor": 0.00005,
    "debug_mode": true,
    "cleanup_preserve": 100,
    "cluster_threshold": 0.25,
    "ddl_change_mode": true,
    "load_id": -999
}
```

##### JSON schema of *competition_score_params* object

```json
{
    "type": "object",
    "properties":
    {
        "volume_exp_scale_coeff": {"type": "number"},
        "sigmoid_scale_coeff": {"type": "number"},
        "sigmoid_shift_coeff": {"type": "number", "minimum": 0, "maximum": 1},
        "competition_score_scale_coeff": {"type": "number", "minimum": 0, "maximum": 1},
        "competition_score_add_coeff": {"type": "number", "minimum": 0, "maximum": 1},
        "volume_scale_coeff": {"type": "number", "minimum": 0, "maximum": 1}
    },
    "required":
        [
            "volume_exp_scale_coeff",
            "sigmoid_scale_coeff",
            "sigmoid_shift_coeff",
            "competition_score_scale_coeff",
            "competition_score_add_coeff",
            "volume_scale_coeff"
        ],
    "minProperties": 6,
    "maxProperties": 6
}
```

##### Settings

- schedule: 0 3 \* \* * - every day at 03:00 in UTC timezone
- max_active_runs: 1
- max_active_tasks: 20
- retries: 0
- wait_for_downstream": True

##### External integrations

- DAG builds CSV reports from logs of SQL operations (views *etl_log.last_run_log* and *etl_log.last_run_attributes_stats*) and uploads it to the folder /dags/logs in Gitlab (overwriting existing logs, if any)
- DAG sends alerts to Slack (channel - *airflow-notifications*, user - *airflow*). Each alert includes *load_id* of the run (if it has been assigned), run duration, values of run input arguments, run status (either 'Success' or error message) and links to logs in Gitlab (if they have been updated)

---

#### etl_agg_xxx.py

DAGs for launching aggregated ETL calculations on yearly / quarterly / monthly basis

These DAGs have the same input parameters and settings as *etl_regular* DAG (see above) with some exclusions listed below:

- *metrics_start_dttm*, *trends_start_dttm*, *cleanup_preserve*, *cluster_threshold* and *ddl_change_mode* input parameters are deleted
- Some input parameters have different default values:
  - For *etl_agg_yearly.py*

```python
{
    "end_dttm": pendulum.now(tz = "UTC").start_of("month").isoformat(),
    "trends_intervals": 3
}
```

  - For *etl_agg_quarterly.py*

```python
{
    "end_dttm": pendulum.now(tz = "UTC").start_of("month").isoformat(),
    "trends_intervals": 4
}
```

  - For *etl_agg_monthly.py*

```python
{
    "end_dttm": pendulum.now(tz = "UTC").start_of("month").isoformat()
}
```

- Schedules:
  - For *etl_agg_monthly.py*: 0 12 1 \* \* - first day of every month at 12:00 in UTC timezone
  - For *etl_agg_quarterly.py*: 0 18 1 \* \* - first day of every month at 18:00 in UTC timezone
  - For *etl_agg_yearly.py*: 0 21 1 \* \* - first day of every month at 21:00 in UTC timezone

---

---

### Maintenance DAGs

#### maintenance_cleanup.py

This DAG helps to run CLUSTER and ANALYZE for chosen databases, schemas and relations on schedule

##### DAG params

- *clean_before_timestamp* - DAG will delete DB records older than this timestamp in ETL log tables and Airflow meta DB tables
- *airflow_cleanup_objects* - List of objects where old rows will be deleted in Airflow meta DB
- *bloat_cleanup_objects* - JSON structure with database -> schema -> relations to be included / excluded hierarchy (each level is a list with unique items)

*bloat_cleanup_objects* parameter description:

All whitelisted ("include" list - "exclude" list) relations will be maintained (CLUSTER and ANALYZE) in order of lists' indices and hierarchy.

```["*"]``` means all local tables and materialized views in corresponding schema (```pg_catalog.pg_class.relkind in ('r', 'p', 'm')```) in alphabetical order by name

If any relation list (included or excluded) contains ```"*"``` then the whole list will be reduced to ```["*"]```

"exclude" list is always overriding the corresponding "include" list. If "exclude" list contains ```"*"``` then all relations in the schema will be blacklisted

For database "etl" schemas *etl_agg* and *etl_agg_dev* are not included in the list because all objects there are already clusterd by ETL

Same thing is applicable to the marts in schemas *etl* and *etl_dev*, but all objects (```"*"```) from them are included in the list - it has been done solely for shortening default argument value so it will fit into the Slack message 3000 symbols limit

It's safe because *public.maintenance_bloat_cleanup* function is clustering only those objects that need clustering (have dead tuples or cluster index keys correlation coefficient < 1)

##### Default values in DAG

```python
{
    "clean_before_timestamp": pendulum.now(tz = "UTC").start_of("day").subtract(months = 1).isoformat(),
    "airflow_cleanup_objects": airflow.cli.commands.db_command.all_tables,
    "bloat_cleanup_objects":
        [
            {
                "database": "etl",
                "schemas":
                    [
                        {
                            "schema": "public",
                            "include": ["*"],
                            "exclude":
                                [
                                    "gigs_pages",
                                    "gigs_revisions",
                                    "sellers_revisions",
                                    "gig_reviews"
                                ]
                        },
                        {
                            "schema": "etl",
                            "include":
                                [
                                    "gigs_revisions_pre_metrics",
                                    "gigs_pages_pre_metrics",
                                    "sellers_revisions_pre_metrics",
                                    "gig_status_history",
                                    "seller_status_history",
                                    "gig_reviews_agg",
                                    "gig_reviews_prices"
                                ],
                            "exclude": []
                        },
                        {
                            "schema": "auth",
                            "include": ["*"],
                            "exclude": []
                        },
                        {
                            "schema": "payment",
                            "include": ["*"],
                            "exclude": []
                        },
                        {
                            "schema": "utils",
                            "include": ["*"],
                            "exclude": []
                        }
                    ]
            },
            {
                "database": "scraping",
                "schemas":
                    [
                        {
                            "schema": "public",
                            "include": ["*"],
                            "exclude": []
                        }
                    ]
            },
            {
                "database": "airflow_meta",
                "schemas":
                    [
                        {
                            "schema": "public",
                            "include": ["*"],
                            "exclude": []
                        }
                    ]
            }
        ]
}
```

##### Example of custom configuration

```json
{
    "clean_before_timestamp": "2023-04-26T07:27:22.732669",
    "airflow_cleanup_objects":
        [
            "callback_request",
            "celery_taskmeta",
            "celery_tasksetmeta",
            "dag",
            "dag_run",
            "dataset_event",
            "import_error",
            "job",
            "log",
            "session",
            "sla_miss",
            "task_fail",
            "task_instance",
            "task_instance_history",
            "task_reschedule",
            "trigger",
            "xcom"
        ],
    "bloat_cleanup_objects":
        [
            {
                "database": "etl",
                "schemas":
                    [
                        {
                            "schema": "public",
                            "include": ["alembic_version"],
                            "exclude": []
                        },
                        {
                            "schema": "etl_log",
                            "include": ["*"],
                            "exclude": ["etl_attributes_stats"]
                        }
                    ]
            },
            {
                "database": "scraping",
                "schemas":
                    [
                        {
                            "schema": "public",
                            "include":
                                [
                                    "db_gigs",
                                    "db_sellers"
                                ],
                            "exclude": []
                        }
                    ]
            }
        ]
}
```

##### JSON schema of input parameters

```json
{
    "type": "array",
    "minItems": 1,
    "uniqueItems": true,
    "unevaluatedItems": false,
    "items":
        {
            "type": "object",
            "minProperties": 2,
            "maxProperties": 2,
            "properties":
                {
                    "database":
                        {
                            "enum":
                                [
                                    "etl",
                                    "scraping",
                                    "airflow_meta"
                                ]
                        },
                    "schemas":
                        {
                            "type": "array",
                            "minItems": 1,
                            "uniqueItems": true,
                            "unevaluatedItems": false,
                            "items":
                                {
                                    "type": "object",
                                    "minProperties": 3,
                                    "maxProperties": 3,
                                    "properties":
                                        {
                                            "schema":
                                                {
                                                    "type": "string"
                                                },
                                            "include":
                                                {
                                                    "type": "array",
                                                    "uniqueItems": true,
                                                    "unevaluatedItems": false,
                                                    "items":
                                                        {
                                                            "type": "string"
                                                        }
                                                },
                                            "exclude":
                                                {
                                                    "type": "array",
                                                    "uniqueItems": true,
                                                    "unevaluatedItems": false,
                                                    "items":
                                                        {
                                                            "type": "string"
                                                        }
                                                }
                                        },
                                    "required":
                                        [
                                            "schema",
                                            "include",
                                            "exclude"
                                        ]
                                }
                        }
                },
            "required":
                [
                    "database",
                    "schemas"
                ]
        }
}
```

##### Settings

- schedule: 5 6 \* \* SUN - every Sunday at 06:05 in UTC timezone
- max_active_runs: 1
- retries: 0
- wait_for_downstream": True

##### External integrations

- DAG sends alerts to Slack (channel - *airflow-notifications*, user - *airflow*). Each alert includes run duration, values of run input arguments, run status (either 'Success' or error message) and links to logs in Gitlab (if they have been updated)

---

#### maintenance_idle_killer.py

DAG for killing idle connections in PostgreSQL databases

##### DAG params

- *databases* - array of database aliases where DAG must run sessions cleanup
  - Default: ['etl']

##### Settings

- schedule: \*/30 \* \* \* \* - every 30 minutes
- max_active_runs: 1
- retries: 0

---

---

## /dags/classes

- **pg_driver.py** - module for direct low-level connections to PostgreSQL DBs via psycopg2 driver (no need in SQLAlchemy).
Automatically reconnects to DB, logging every step to Airflow log.

- **db_config.py** - module for keeping authentication information for any type and any number of databases

---

## /dags/logs

Includes CSV files built by COPY command from views *etl_log.last_run_log* and *etl_log.last_run_attributes_stats* from the *etl_regular* DAG. These files are being uploaded to the Gitlab repository, and links to them are being sent to the Slack reports about *etl_regular* DAG completion (channel - *airflow-notifications*, user - *airflow*)

---

## /system

Various OS-level scripts and configs:

- *logrotate.conf* - config for logrotate (/etc/logrotate.d) utility, rotates dag_processor_manager logs and gunicorn logs
- *airflow_logs_cleanup* - script placed in /etc/cron.daily, removes all logs in /opt/airflow/ directory older than 31 days

- *airflow_upgrade.sh* - script for Airflow version upgarde

- *airflow_local_settings.py* - script for external setup of Airflow configs
- *airflow.conf* - template for airflow.cfg without any sensitive data

- *variables.sh* - template of script for setting up Airflow variables and connections (without any sensitive data)

- *airflow-scheduler-service.sh* - script for a systemctl airflow-scheduler.service
- *airflow-scheduler.service* - config for a systemctl airflow-scheduler.service (/usr/lib/systemd/system)

- *airflow-webserver-service.sh* - script for a systemctl airflow-webserver.service
- *airflow-webserver.service* - config for a systemctl airflow-webserver.service (/usr/lib/systemd/system)
