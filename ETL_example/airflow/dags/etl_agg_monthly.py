"""DAG for launching monthly aggregation for calculations of metrics and trends via set of PL/pgSQL functions"""

# Imports

import os
import errno
import json
import re
import csv
from subprocess import run
from ast import literal_eval
from textwrap import dedent

import logging
import pendulum

from classes.pg_driver import PostgreSQL
from classes.db_config import DBConfig
from psycopg import sql

from airflow.decorators import dag, task, task_group
from airflow.utils.context import Context
from airflow.operators.python import get_current_context
from airflow.models import Variable
from airflow.models.param import Param
from airflow.hooks.base import BaseHook
from airflow.providers.slack.operators.slack_webhook import SlackWebhookOperator
from airflow.models.baseoperator import chain, chain_linear


# Job start and load_id

job_start = pendulum.now(tz = "UTC")
load_id_global = int(job_start.format('YYYYMMDDHHmmss'))


# Setting the logger

task_logger = logging.getLogger("airflow.task")
task_logger.setLevel('DEBUG')

fail_msg = ':x: DAG run failed'


# Connections

CON_ETL_PG = DBConfig(db_type = 'PG', db_name = 'etl')
db_con = CON_ETL_PG.pg_connections()
ETL_PG = PostgreSQL(
    host = db_con["proxy_host"],
    port = db_con["port"],
    db = db_con["db"],
    user = db_con["user"],
    password = db_con["password"],
    row_type = "dict",
    application_name = None # calculated inside the DAG
)

slack_conn_id = Variable.get('slack_conn')


# Other variables

git_dir = '/opt/af_repo_logs_mirror/airflow'
errors_file = f"""{git_dir}/dags/logs/{os.path.basename(__file__).replace('.py', '')}_errors.csv"""


# Docs

dag_doc_md = """
DAG for launching monthly aggregation for calculations of metrics and trends via set of PL/pgSQL functions

**READMEs**:
[Airflow repo README](https://gitlab.com/fiverr-dev/data-engineering/airflow/-/blob/main/README.md),
[ETL repo README](https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/README.md)

**Schedule**: 0 12 1 * *

**Retries**: 0

**DAG params**:
- *contour* - enum ['prod', 'dev'] for choosing which version of ETL to launch
- *parallel_mode* - runs parallelized DAG branch if True, runs sequential DAG branch if False
- *end_dttm* - upper bound for calculation time period
- *trends_intervals* (from 2 to 12 with step 1) - amount of iterations for the calculations of revenue trend lines
- *trends_students_coeff* (> 0) - critical value of correlation coefficient for Student's coefficient""" \
""" (by default - confidence interval = 0.95, t-statistics = 1.96)
- *ratings_to_volume_coeff* (from 0 to 1) - coefficient of ratings to sales conversion
- *overpriced_coeff* - crutch to filter gigs with suspiciously high prices (higher than weighted average price in subcategory multiplied by this coefficient)
- *competition_score_percentile* (from 0 to 1) - percentile of inversed sales ratio metrics which used for calculations of gigs' competition scores
- *competition_score_params* - set of tuning parameters for etl.competition_score function for calculating competition_score_xxx attributes""" \
""", see [function](https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/competition_score.sql)""" \
""" and [notebook](https://gitlab.com/fiverr-dev/data-engineering/analytics-notebooks/-/blob/main/competition_score/gigs_competition.py)
    - *volume_exp_scale_coeff* (no limits) - scaling factor that determines how fast sales volume affects competition score
    - *sigmoid_scale_coeff* (no limits) - scaling factor that makes transitions between competition score levels more sharp
    - *sigmoid_shift_coeff* (from 0 to 1) - correction factor that shifts the center of the scale for transitions between competition score levels
    - *competition_score_scale_coeff* (from 0 to 1) - scaling factor that helps to avoid values close to the borders [0, 1]
    - *competition_score_add_coeff* (from 0 to 1) - correction factor that helps to avoid values close to the borders [0, 1]
    - *volume_scale_coeff* (from 0 to 1) - scale factor that reduces outbursts for largest volume values
- *rank_scale_factor* - scaling argument for etl.relevance_sorting function
- *debug_mode* - saves temporary tables if True, drops it if False
- *load_id* - identificator of the ETL run across all DAGs for a database log

**Default params values**:
```python
{
    "contour": 'prod',
    "parallel_mode": True,
    "end_dttm": pendulum.now(tz = "UTC").start_of("month").isoformat(),
    "trends_intervals": 3,
    "trends_students_coeff": 1.96,
    "ratings_to_volume_coeff": 0.8,
    "overpriced_coeff": 10,
    "competition_score_percentile": 0.95,
    "competition_score_params":
        {
            "volume_exp_scale_coeff": 100,
            "sigmoid_scale_coeff": -5,
            "sigmoid_shift_coeff": 0.5,
            "competition_score_scale_coeff": 0.8,
            "competition_score_add_coeff": 0.1,
            "volume_scale_coeff": 0.5
        },
    "rank_scale_factor": 0.0001,
    "debug_mode": False,
    "load_id": int(pendulum.now(tz = "UTC").format('YYYYMMDDHHmmss'))
}
```
"""


###################################################################################
# Supporting functions
###################################################################################

# Removing file if exists
def file_remove(filename):
    "File removal function"
    try:
        os.remove(filename)
    except OSError as err:
        if err.errno != errno.ENOENT:
            task_logger.error("Exception: %s", err)
            raise


# JSON pretty print
def prettify_json(json_input, sort = True, indents = 4) -> str:
    "Returns properly indented JSON string"

    try:
        if isinstance(json_input, str) is True:
            trans = {
                '\\n': '\n',
                '    ': '',
                '\t': '',
                '"{': '{',
                '}"': '}',
                '\\"': "'"
            }
            res_string = re.sub(f"""({'|'.join(map(re.escape, trans.keys()))})""", lambda m: trans[m.group()], json_input)
            return json.dumps(json.loads(res_string), sort_keys = sort, indent = indents)

        return json.dumps(json_input, sort_keys = sort, indent = indents)

    except ValueError:
        task_logger.error("Invalid JSON input: %s", json_input)
        return json_input


# Slack alert function
def slack_alert(
        context,
        status_msg: str,
        input_args: dict,
        result,
        logs: str = ''
    ):
    "Function for sending messages to Slack channel"

    channel = BaseHook.get_connection(slack_conn_id).login

    # Slack mrkdwn pretty string versions of arguments
    input_args_str = prettify_json(input_args)

    if isinstance(result, dict) is True and 'status' in result:
        status_str = prettify_json(result['status'])
    elif isinstance(result, dict) is True and 'status' not in result:
        status_str = prettify_json(result)
    else:
        status_str = prettify_json(fr"{str(result)}")

    if isinstance(result, dict) is True and 'load_id' in result:
        load_id = result['load_id']
    else:
        load_id = input_args['load_id']

    if isinstance(result, dict) is True and 'runtime' in result:
        runtime = str(result['runtime'])
    else:
        dag_run = context["dag_run"]
        dag_start = dag_run.get_task_instance('step_0.get_input_args').queued_dttm
        runtime = str((pendulum.now(tz = "UTC") - dag_start).as_timedelta())

    # Message body
        # Slack message length is limited to 3001 characters
    payload = f"""*DAG*: {context["dag"].dag_id}\n\n""" \
        f"""*Input parameters*:\n```{input_args_str}```\n\n""" \
        f"""*load_id*: {load_id}\n\n""" \
        f"""*Runtime*: {runtime}\n\n""" \
        f"""*Status*:\n```{status_str[0:1500]}```""" \
        f"""{logs}"""

    slack_msg = [ # should be a list of dictionaries
            {
                "type": "divider"
            },
            {
                "type": "header",
                "text": {
                    "type": "plain_text",
                    "text": status_msg
                }
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": payload
                }
            }
        ]

    # full Blocks message for the Block Kit Builder https://app.slack.com/block-kit-builder/T05PNU32MQE
    task_logger.info('{"blocks": %s}', slack_msg)

    slack_operator = SlackWebhookOperator(
        task_id = 'slack_alert',
        message = status_msg,
        blocks = slack_msg,
        channel = channel,
        username = 'airflow',
        slack_webhook_conn_id = slack_conn_id
    )

    return slack_operator.execute(context = context)


# Getting subprocess response
def subprocess_log(cmd):
    "Bash commands execution logger"

    task_logger.info('Command: %s', " ".join(cmd.args))
    task_logger.info('Exit status: %s', cmd.returncode if cmd.returncode is not None else None)
    task_logger.info('STDOUT: %s', cmd.stdout.decode() if cmd.stdout is not None else None)
    task_logger.error('STDERR: %s', cmd.stderr.decode() if cmd.stderr is not None else None)


# ETL logs -> Gitlab uploader
def logs_upload(
        load_id: int,
        application_name: str
    ) -> str:
    "Uploads data from ETL logs for current load_id to Gitlab"

    if load_id is None:
        return ''

    try:
        log_query = sql.SQL("""
            copy
            (
                select *
                from etl_log.etl_log
                where
                    load_id = {load_id}::int8
                order by
                    id
            )
            to stdout with (format csv, delimiter ';', header true)
        """).format(load_id = sql.Literal(load_id))

        log_csv = f"""{git_dir}/dags/logs/last_run_log.csv"""
        log_copy_status, _ = ETL_PG.copy_expert(
            to_file = True,
            query = log_query,
            file = log_csv,
            application_name = application_name
        )

        stats_query = sql.SQL("""
            copy
            (
                select *
                from etl_log.etl_attributes_stats
                where
                    load_id = {load_id}::int8
                order by
                    tgt_schema,
                    tgt_name,
                    attribute_number
            )
            to stdout with (format csv, delimiter ';', header true)
        """).format(load_id = sql.Literal(load_id))

        stats_csv = f"""{git_dir}/dags/logs/last_run_attributes_stats.csv"""
        stats_copy_status, _ = ETL_PG.copy_expert(
            to_file = True,
            query = stats_query,
            file = stats_csv,
            application_name = application_name
        )

        # Slack Webhook does not support sending files, so sending links to files on Gitlab instead
        cmd = run(["git", "add", "*.csv"], cwd = git_dir, shell = False, capture_output = True, check = False)
        subprocess_log(cmd)

        cmd =  run(["git", "commit", "-m", f"load_id={load_id}"], cwd = git_dir, shell = False, capture_output = True, check = False)
        subprocess_log(cmd)

        cmd = run(["git", "push"], cwd = git_dir, shell = False, capture_output = True, check = False)
        subprocess_log(cmd)

        git_upd = True

    except Exception as err:
        git_upd = False
        task_logger.error(err)

    log_links = ''
    if all([log_copy_status, stats_copy_status, git_upd]) is True:
        log_links = "\n\n*Logs*:\n" \
        "<https://gitlab.com/fiverr-dev/data-engineering/airflow/-/blob/main/dags/logs/last_run_log.csv|Full run log>\n" \
        "<https://gitlab.com/fiverr-dev/data-engineering/airflow/-/blob/main/dags/logs/last_run_attributes_stats.csv|Target tables statistics>"

    return log_links


###################################################################################
# Callbacks
###################################################################################

def task_failure_callback(context):
    "Task level failure callback function"

    task_logger.info("Task failure callback context: %s", context)

    failed_task_name = context.get('task_instance').task_id
    failed_task_error = context.get("exception")

    try:
        if (
            isinstance(failed_task_error, str) is True
            and failed_task_error.startswith('{')
        ):
            failed_task_error = literal_eval(failed_task_error)

        with open(file = errors_file, mode = "a+") as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow([failed_task_name, failed_task_error])
    except Exception:
        task_logger.error("Invalid dict input: %s", failed_task_error)


def dag_failure_callback(context):
    "DAG level failure callback function"

    task_logger.info("DAG failure callback context: %s", context)

    try:
        ti = context['task_instance']
        input_args = ti.xcom_pull(task_ids = 'step_0.get_input_args')

        if input_args is None or input_args == dict():
            raise ValueError('Empty input_args dictionary')
    except Exception:
        etl_schema = "etl" if context["params"]["contour"] != "dev" else "etl_dev"
        etl_agg_schema = "etl_agg" if context["params"]["contour"] != "dev" else "etl_agg_dev"

        trends_intervals = context["params"]["trends_intervals"]
        end_dttm = pendulum.from_format(context["params"]["end_dttm"], 'YYYY-MM-DDTHH:mm:SSZ')

        metrics_start_dttm = end_dttm.start_of("month").subtract(months = 1).isoformat()
        trends_start_dttm = end_dttm.start_of("month").subtract(months = trends_intervals).isoformat()

        application_name = f"""Monthly aggregation ETL | load_id = {context["params"]["load_id"]}"""

        input_args = (
            context["params"] |
            {"etl_schema": etl_schema} |
            {"etl_agg_schema": etl_agg_schema} |
            {"tables_prefix": "monthly"} |
            {"metrics_start_dttm": metrics_start_dttm} |
            {"trends_start_dttm": trends_start_dttm} |
            {"application_name": application_name}
        )

    try:
        etl_plpgsql_function_call(
            func_schema = input_args["etl_agg_schema"],
            func_name = "af_agg_rollback",
            func_args = {
                "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                "high_level_step": {"value": "-1", "data_type": "text"}
            },
            context = context,
            application_name = input_args["application_name"]
        )
    except RuntimeError as runtime_err:
        task_logger.error("RuntimeError: %s", runtime_err)
    except Exception as err:
        task_logger.error("Exception: %s", err)

    try:
        with open(file = errors_file, mode = 'r+') as csv_file:
            reader = csv.reader(csv_file)
            err_result = {
                rows[0]:
                    literal_eval(prettify_json(fr"{rows[1]}")) if fr"{rows[1]}".startswith('{')
                    else prettify_json(fr"{rows[1]}")
                for rows in reader
            }

        file_remove(errors_file)
    except Exception as err:
        task_logger.error("Exception: %s", err)
        err_result = f"""Check $AIRFLOW_HOME/logs/scheduler/latest/{context["dag"].dag_id}.py.log for callback log"""

    task_logger.error('err_result type: %s, err_result value: %s', type(err_result), err_result)

    logs = logs_upload(
        load_id = input_args["load_id"],
        application_name = input_args["application_name"]
    )

    slack_alert(
        context = context,
        status_msg = fail_msg,
        input_args = input_args,
        result = err_result,
        logs = logs
    )


###################################################################################
# Task-formatting functions
###################################################################################

def etl_plpgsql_function_call(
    func_schema: str,
    func_name: str,
    func_args: dict,
    context: Context,
    application_name: str = None
) -> bool:
    """
    Function for generating task code for a simple calls of ETL PL/pgSQL-functions.
    "func_args" argument must be a dictionary with this structure:
        {
            "arg_1_name":
                {
                    "value": arg_value,
                    "data_type": PostgreSQL type name identifier (value from pg_catalog.pg_type.typname)
                },
            ...
        }

    "data_type" value must be exactly the same as type name from pg_catalog.pg_type.typname.
    Any aliases (like "bigint" instead of "int8") are deliberately not supported
    Most common types for this DAG: timestamptz, int8, int4, int2, bool, numeric, text, jsonb

    "application_name" - optional string for backend's "application_name" in pg_catalog.pg_stat_activity
    """

    func_list = []

    for arg_name, arg_meta in func_args.items():

        func_list.append(
            sql.SQL('::').join(
                [
                    sql.SQL(' := ').join(
                        [
                            sql.Identifier(arg_name),
                            sql.Literal(
                                arg_meta["value"] if arg_meta["data_type"] not in ['json', 'jsonb']
                                else json.dumps(arg_meta["value"])
                            )
                        ]
                    ),
                    sql.Identifier(arg_meta["data_type"])
                ]
            )
        )

    query = sql.SQL("""select {function}({arguments});""").format(
        function = sql.Identifier(func_schema, func_name),
        arguments = sql.SQL(', ').join(func_list)
    )

    query_status, query_result = ETL_PG.fetch_one(query = query, application_name = application_name)

    log_msg = f"""{{"query_status": {query_status}, "query_result": {query_result}}}"""
    task_logger.info('%s', log_msg)

    if query_status is False or query_result.get("status") != 'Success':
        dag_run = context["dag_run"]
        dag_start = dag_run.get_task_instance('step_0.get_input_args').queued_dttm
        query_result.update(runtime = str((pendulum.now(tz = "UTC") - dag_start).as_timedelta()))
        task_logger.error('%s', log_msg)
        raise RuntimeError(prettify_json(query_result))

    return query_status


###################################################################################
# DAG definition
###################################################################################

@dag(
    dag_id = "etl_agg_monthly",
    default_args = {
        "retries": 0,
        "wait_for_downstream": True,
        "on_failure_callback": task_failure_callback
    },
    description = "DAG for launching monthly aggregation for calculations of metrics and trends via set of PL/pgSQL functions",
    max_active_runs = 1,
    max_active_tasks = 20,
    schedule = '0 12 1 * *', # first day of every month at 12:00 in UTC timezone
    start_date = pendulum.datetime(2023, 1, 1, 0, 0, 0),
    catchup = False,
    tags = ["RDS", "ETL", "Aggregated ETL"],
    on_failure_callback = dag_failure_callback,
    params = {
            "contour": Param(
                default = 'prod',
                type = "string",
                enum = ['prod', 'dev'],
                title = "contour",
                description = "Enum ['prod', 'dev'] for choosing which version of ETL to launch"
                ),
            "parallel_mode": Param(
                default = True,
                type = "boolean",
                title = "parallel_mode",
                description = "Runs parallelized DAG branch if True, runs sequential DAG branch if False"
                ),
            "end_dttm": Param(
                default = job_start.start_of("month").isoformat(),
                type = "string",
                format = 'date-time',
                title = "end_dttm",
                description = "Upper bound for calculation time period"
                ),
            "trends_intervals": Param(
                default = 3,
                type = "integer",
                title = "trends_intervals",
                description_md = "(from 2 to 12 with step 1) Amount of iterations of *etl.trends_period_func* that will be launched",
                minimum = 2,
                maximum = 12
                ),
            "trends_students_coeff": Param(
                default = 1.96,
                type = "number",
                title = "trends_students_coeff",
                description = "(> 0) Critical value of correlation coefficient for Student's coefficient",
                exclusiveMinimum = 0
                ),
            "ratings_to_volume_coeff": Param(
                default = 0.8,
                type = "number",
                title = "ratings_to_volume_coeff",
                description = "(from 0 to 1) Coefficient of ratings to sales conversion",
                exclusiveMinimum = 0,
                maximum = 1
                ),
            "overpriced_coeff": Param(
                default = 10,
                type = "number",
                title = "overpriced_coeff",
                description = "Crutch to filter gigs with suspiciously high prices (higher than weighted average price in subcategory multiplied by this coefficient)"
                ),
            "competition_score_percentile": Param(
                default = 0.95,
                type = "number",
                title = "competition_score_percentile",
                description = "(from 0 to 1) Percentile of inversed sales ratio metrics which used for calculations of gigs' competition scores",
                exclusiveMinimum = 0,
                exclusiveMaximum = 1
                ),
            "competition_score_params": Param(
                default =
                    {
                        "volume_exp_scale_coeff": 100,
                        "sigmoid_scale_coeff": -5,
                        "sigmoid_shift_coeff": 0.5,
                        "competition_score_scale_coeff": 0.8,
                        "competition_score_add_coeff": 0.1,
                        "volume_scale_coeff": 0.5
                    },
                type = "object",
                title = "competition_score_params",
                description_md = dedent("(last 4 params from 0 to 1) " \
                        "Set of tuning parameters for etl.competition_score function for calculating competition_score_xxx attributes, " \
                        "see [function](https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/competition_score.sql) " \
                        "and [notebook](https://gitlab.com/fiverr-dev/data-engineering/analytics-notebooks/-/blob/main/competition_score/gigs_competition.py)"
                    ),
                properties =
                    {
                        "volume_exp_scale_coeff": {"type": "number"},
                        "sigmoid_scale_coeff": {"type": "number"},
                        "sigmoid_shift_coeff": {"type": "number", "minimum": 0, "maximum": 1},
                        "competition_score_scale_coeff": {"type": "number", "minimum": 0, "maximum": 1},
                        "competition_score_add_coeff": {"type": "number", "minimum": 0, "maximum": 1},
                        "volume_scale_coeff": {"type": "number", "minimum": 0, "maximum": 1}
                    },
                required =
                    [
                        "volume_exp_scale_coeff",
                        "sigmoid_scale_coeff",
                        "sigmoid_shift_coeff",
                        "competition_score_scale_coeff",
                        "competition_score_add_coeff",
                        "volume_scale_coeff"
                    ],
                minProperties = 6,
                maxProperties = 6
                ),
            "rank_scale_factor": Param(
                default = 0.0001,
                type = "number",
                title = "rank_scale_factor",
                description_md = "Scaling argument for *etl.relevance_sorting* function"
                ),
            "debug_mode": Param(
                default = False,
                type = "boolean",
                title = "debug_mode",
                description = "Saves temporary tables if True, drops it if False"
                ),
            "load_id": Param(
                default = load_id_global,
                type = "integer",
                title = "load_id",
                description = "Identificator of the ETL run across all DAGs for a database log"
                )
        },
    doc_md = dedent(dag_doc_md)
)
def etl_agg_monthly():
    "Upper-level callable for DAG"

###################################################################################
# Tasks
###################################################################################

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 0 - Parsing input parameters, preparing local git repository and branching the DAG
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 0.1 - Parsing input parameters from context, calculating load_id
    @task(
        doc = "Getting DAG input arguments and load_id"
    )
    def get_input_args() -> dict:
        context = get_current_context()

        # Cleaning up any existing file with task instances errors
        file_remove(errors_file)

        # Printing and saving input arguments
        task_logger.info("Context: %s", context)

        # Adding calculated parameters to input arguments
        etl_schema = "etl" if context["params"]["contour"] != "dev" else "etl_dev"
        etl_agg_schema = "etl_agg" if context["params"]["contour"] != "dev" else "etl_agg_dev"

        trends_intervals = context["params"]["trends_intervals"]
        end_dttm = pendulum.from_format(context["params"]["end_dttm"], 'YYYY-MM-DDTHH:mm:SSZ')

        metrics_start_dttm = end_dttm.start_of("month").subtract(months = 1).isoformat()
        trends_start_dttm = end_dttm.start_of("month").subtract(months = trends_intervals).isoformat()

        application_name = f"""Monthly aggregation ETL | load_id = {context["params"]["load_id"]}"""

        input_args = (
            context["params"] |
            {"etl_schema": etl_schema} |
            {"etl_agg_schema": etl_agg_schema} |
            {"tables_prefix": "monthly"} |
            {"metrics_start_dttm": metrics_start_dttm} |
            {"trends_start_dttm": trends_start_dttm} |
            {"application_name": application_name}
        )
        task_logger.info("Input arguments: %s", input_args)

        none_args = [key for key, value in input_args.items() if value is None]
        if none_args != []:
            error_msg = f"""There are input arguments with None values: {none_args}"""
            raise ValueError(error_msg)

        return input_args


    # Step 0.2 - Updating local git mirror repo
    @task(
        doc = "Preparing local Gitlab repo (git pull) for adding new versions of logs later at postprocessing task"
    )
    def git_pull():
        try:
            cmd = run(["git", "reset", "--hard", "origin/main"], cwd = git_dir, shell = False, capture_output = True, check = False)
            subprocess_log(cmd)

            cmd = run(["git", "pull"], cwd = git_dir, shell = False, capture_output = True, check = False)
            subprocess_log(cmd)

            cmd = run(["sudo", "chown", "ubuntu:airflow", "-R", f"{git_dir}"], cwd = git_dir, shell = False, capture_output = True, check = False)
            subprocess_log(cmd)

            cmd = run(["sudo", "chmod", "-R", "755", f"{git_dir}"], cwd = git_dir, shell = False, capture_output = True, check = False)
            subprocess_log(cmd)

            return True

        except Exception as err:
            task_logger.error(err)
            return False


    # Step 0.3 - Branching parallel and sequential executions
    @task.branch(
        doc = "Branching parallel and sequential ETL executions"
    )
    def parallel_mode_branching():
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        if input_args["parallel_mode"] is False:
            return 'step_1.sequential_etl'
        return 'step_1.mj_prep_start'


    @task_group(
        group_id = 'step_0',
        tooltip = "Parsing input parameters, preparing local git repository and branching the DAG"
    )
    def step_0():
        objects = [
            get_input_args(),
            git_pull(),
            parallel_mode_branching()
        ]

        chain(
            objects[0],
            objects[2]
        )

        return objects


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 1 - Preparations in the database
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 1.1 - etl_agg.af_agg_mj_prep_start
    @task.short_circuit(
        doc_md = dedent("""Preparations: opening dblink connection, checking input arguments.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_1/af_agg_mj_prep_start.sql)"""
        )
    )
    def mj_prep_start(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_mj_prep_start",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "trends_start_dttm": {"value": input_args["trends_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "trends_intervals": {"value": input_args["trends_intervals"], "data_type": "int2"},
                    "trends_students_coeff": {"value": input_args["trends_students_coeff"], "data_type": "numeric"},
                    "ratings_to_volume_coeff": {"value": input_args["ratings_to_volume_coeff"], "data_type": "numeric"},
                    "competition_score_percentile": {"value": input_args["competition_score_percentile"], "data_type": "numeric"},
                    "competition_score_params": {"value": input_args["competition_score_params"], "data_type": "jsonb"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 1.2 - etl_agg.agg_etl_sequential
    @task(
        doc_md = dedent("""Sequential ETL - running (etl_agg_schema).agg_etl_sequential function".
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/agg_etl_sequential.sql)"""
        )
    )
    def sequential_etl():
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        query = sql.SQL("""
            select {etl_agg_schema}.agg_etl_sequential(
                tables_prefix := {tables_prefix}::text,
                metrics_start_dttm := {metrics_start_dttm}::timestamptz,
                trends_start_dttm := {trends_start_dttm}::timestamptz,
                end_dttm := {end_dttm}::timestamptz,
                trends_intervals := {trends_intervals}::int2,
                ratings_to_volume_coeff := {ratings_to_volume_coeff}::numeric,
                overpriced_coeff := {overpriced_coeff}::numeric,
                competition_score_percentile := {competition_score_percentile}::numeric,
                competition_score_params := {competition_score_params}::jsonb,
                rank_scale_factor := {rank_scale_factor}::numeric,
                load_id := {load_id}::int8,
                debug_mode := {debug_mode}::bool,
                dag_name := {dag_name}::text
            );
            """
        ).format(
            etl_agg_schema = sql.Identifier(input_args["etl_agg_schema"]),
            tables_prefix = sql.Literal(input_args["tables_prefix"]),
            metrics_start_dttm = sql.Literal(input_args["metrics_start_dttm"]),
            trends_start_dttm = sql.Literal(input_args["trends_start_dttm"]),
            end_dttm = sql.Literal(input_args["end_dttm"]),
            trends_intervals = sql.Literal(input_args["trends_intervals"]),
            ratings_to_volume_coeff = sql.Literal(input_args["ratings_to_volume_coeff"]),
            overpriced_coeff = sql.Literal(input_args["overpriced_coeff"]),
            competition_score_percentile = sql.Literal(input_args["competition_score_percentile"]),
            competition_score_params = sql.Literal(json.dumps(input_args["competition_score_params"])),
            rank_scale_factor = sql.Literal(input_args["rank_scale_factor"]),
            load_id = sql.Literal(input_args["load_id"]),
            debug_mode = sql.Literal(input_args["debug_mode"]),
            dag_name = sql.Literal(f"""{context["dag"].dag_id}_sequential""")
        )

        query_status, query_result = ETL_PG.fetch_one(query = query, application_name = input_args["application_name"])

        dag_run = context["dag_run"]
        dag_start = dag_run.get_task_instance('step_0.get_input_args').queued_dttm
        query_result.update(runtime = str((pendulum.now(tz = "UTC") - dag_start).as_timedelta()))

        log_msg = f"""{{"query_status": {query_status}, "query_result": {query_result}}}"""

        logs = logs_upload(
            load_id = input_args["load_id"],
            application_name = input_args["application_name"]
        )

        if query_status is False or query_result.get("status") != 'Success':
            task_logger.error('%s', log_msg)
            slack_alert(
                context = context,
                status_msg = fail_msg,
                input_args = input_args,
                result = query_result,
                logs = logs
            )
            raise RuntimeError(prettify_json(query_result))

        task_logger.info('%s', log_msg)
        status_msg = ':white_check_mark: DAG run has succeeded'

        slack_alert(
            context = context,
            status_msg = status_msg,
            input_args = input_args,
            result = query_result,
            logs = logs
        )

        return query_status


    @task_group(
        group_id = 'step_1',
        tooltip = "Preparations for parallel ETL or sequential ETL"
    )
    def step_1():
        objects = [
            mj_prep_start('1.1'),
            sequential_etl()
        ]

        chain(
            step_0_objects[2], # parallel_mode_branching()
            objects[1] # sequential_etl()
        )

        return objects


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 2 - Preparing targets and supporting objects for trends calculations
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 2.1 - etl_agg.af_agg_prepare_targets
    @task.short_circuit(
        doc_md = dedent("""Preparing target tables.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_2/af_agg_prepare_targets.sql)"""
        )
    )
    def agg_prepare_targets(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_prepare_targets",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 2.2 - etl.af_trends_prepare
    @task.short_circuit(
        doc_md = dedent("""Preparing etl_wrk.trends_prefin table.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_2/af_trends_prepare.sql)"""
        )
    )
    def trends_prepare(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trends_prepare",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 2.3.1 - etl.af_trends_trend_periods_borders
    @task(
        doc_md = dedent("""Calculating timestamp borders for trends periods.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_2/af_trends_trend_periods_borders.sql)"""
        )
    )
    def trends_trend_periods_borders(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        query = sql.SQL("""
            select
                period_num,
                period_start as start_dttm,
                period_end as end_dttm
            from {etl_schema}.af_trends_trend_periods_borders(
                start_dttm := {start_dttm}::timestamptz,
                end_dttm := {end_dttm}::timestamptz,
                intervals := {intervals}::int2,
                load_id := {load_id}::int8,
                debug_mode := {debug_mode}::bool,
                dag_name := {dag_name}::text,
                high_level_step := {high_level_step}::text
            )
            order by
                period_num;
            """
        ).format(
            etl_schema = sql.Identifier(input_args["etl_schema"]),
            start_dttm = sql.Literal(input_args["trends_start_dttm"]),
            end_dttm = sql.Literal(input_args["end_dttm"]),
            intervals = sql.Literal(input_args["trends_intervals"]),
            load_id = sql.Literal(input_args["load_id"]),
            debug_mode = sql.Literal(input_args["debug_mode"]),
            dag_name = sql.Literal(context["dag"].dag_id),
            high_level_step = sql.Literal(high_level_step)
        )

        query_status, query_result = ETL_PG.fetch_all(query)

        log_msg = f"""{{"query_status": {query_status}, "query_result": {query_result}}}"""
        task_logger.info('%s', log_msg)

        if (
                query_status is False
                or isinstance(query_result, list) is False
                or (isinstance(query_result, list) is True and len(query_result) < int(input_args["trends_intervals"]))
            ):
            dag_run = context["dag_run"]
            dag_start = dag_run.get_task_instance('step_0.get_input_args').queued_dttm
            query_result.update(runtime = (pendulum.now(tz = "UTC") - dag_start).as_timedelta())
            task_logger.error('%s', log_msg)
            raise RuntimeError(prettify_json(query_result))

        return query_result


    # Step 2.3.2.1 - pushing trend period borders (start_dttm, end_dttm, period_num) dict to XCom to expand on without repeating PL/pgSQL function call
    @task(
        doc = "Getting XCom for expanding trends periods tasks (start_dttm, end_dttm, period_num)"
    )
    def make_trend_periods_borders_dict():
        context = get_current_context()
        trend_periods_borders_dicts = context["ti"].xcom_pull(task_ids = 'step_2.step_2_trends.step_2_trend_periods_borders.trends_trend_periods_borders')

        return trend_periods_borders_dicts


    # Step 2.3.2.2 - pushing subset of trend period borders (start_dttm, period_num) dict to XCom to expand on without repeating PL/pgSQL function call
    @task(
        doc = "Getting XCom for expanding trends periods tasks (start_dttm, period_num)"
    )
    def make_trend_periods_borders_start_subdict():
        context = get_current_context()
        trend_periods_borders_dicts = context["ti"].xcom_pull(task_ids = 'step_2.step_2_trends.step_2_trend_periods_borders.trends_trend_periods_borders')
        subdicts = [{"period_num": tpbd["period_num"], "start_dttm": tpbd["start_dttm"]} for tpbd in trend_periods_borders_dicts]

        return subdicts


    # Step 2.3.2.3 - pushing list of period numbers list to XCom to expand on without repeating PL/pgSQL function call
    @task(
        doc = "Getting XCom for expanding trends periods tasks (period_num)"
    )
    def make_periods_list():
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        periods_list = list(range(1, input_args["trends_intervals"] + 1))

        return periods_list


    @task_group(
        group_id = 'step_2',
        tooltip = "Preparing targets and supporting objects for trends calculations"
    )
    def step_2():

        @task_group(
            group_id = 'step_2_trends',
            tooltip = "Preparations for trends calculations"
        )
        def step_2_trends():

            @task_group(
                group_id = 'step_2_trend_periods_borders',
                tooltip = "Getting arguments' lists for dynamic tasks"
            )
            def step_2_trend_periods_borders():

                objects = [
                    trends_trend_periods_borders('2.3.1'),
                    make_trend_periods_borders_dict(),
                    make_trend_periods_borders_start_subdict(),
                    make_periods_list()
                ]

                chain(
                    objects[0], # trends_trend_periods_borders('2.3.1')
                    objects[1:] # [make_trend_periods_borders_dict(), make_trend_periods_borders_start_subdict(), make_periods_list()]
                )

                return objects

            objects = [
                trends_prepare('2.2'),
                step_2_trend_periods_borders()
            ]

            return objects

        prepare_targets_object = agg_prepare_targets('2.1')
        trends_objects = step_2_trends()

        objects = [
            prepare_targets_object,
            trends_objects
        ]

        chain(
            step_0_objects[0], # get_input_args()
            step_1_objects[0], # mj_prep_start('1.1')
            [
                prepare_targets_object, # agg_prepare_targets('2.1')
                trends_objects[0], # trends_prepare('2.2')
                trends_objects[1][0] # trends_trend_periods_borders('2.3.1')
            ]
        )

        return objects


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 3 - Preparing active slices of gigs and sellers for metrics and trend periods
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 3.1 - etl.af_metrics_gigs_hist_active
    @task.short_circuit(
        doc_md = dedent("""Getting active slice of gigs for metrics calculations.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_5/af_metrics_gigs_hist_active.sql)"""
        )
    )
    def metrics_gigs_hist_active(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_gigs_hist_active",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )

    # Step 3.2.1 - etl.af_metrics_sellers_hist_active
    @task.short_circuit(
        doc_md = dedent("""Getting active slice of gigs for metrics calculations.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_5/af_metrics_sellers_hist_active.sql)"""
        )
    )
    def metrics_sellers_hist_active(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_sellers_hist_active",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 3.2.2 - etl.af_metrics_sellers_revisions
    @task.short_circuit(
        doc_md = dedent("""Preparing data about sellers.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_5/af_metrics_sellers_revisions.sql)"""
        )
    )
    def metrics_sellers_revisions(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_sellers_revisions",
                func_args = {
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 3.3 - etl.af_trend_periods_gigs_hist_active
    @task.short_circuit(
        doc_md = dedent("""Getting active slices of gigs for trends periods calculations.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_5/af_trend_periods_gigs_hist_active.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_gigs_hist_active(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        end_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_gigs_hist_active",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "end_dttm": {"value": end_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 3.4 - etl.af_trend_periods_sellers_hist_active
    @task.short_circuit(
        doc_md = dedent("""Getting active slices of sellers for trends periods calculations.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_5/af_trend_periods_sellers_hist_active.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_sellers_hist_active(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        end_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_sellers_hist_active",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "end_dttm": {"value": end_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    @task_group(
        group_id = 'step_3',
        tooltip = "Preparing active slices of gigs and sellers for metrics and trend periods"
    )
    def step_3():

        @task_group(
            group_id = 'step_3_metrics_active_history',
            tooltip = "Active history for metrics"
        )
        def step_3_metrics_active_history():
            gigs_hist = metrics_gigs_hist_active('3.1')
            sellers_hist = metrics_sellers_hist_active('3.2.1')
            sellers_rev = metrics_sellers_revisions('3.2.2')

            chain(
                sellers_hist, # metrics_sellers_hist_active('3.2.1')
                sellers_rev # metrics_sellers_revisions('3.2.2')
            )

            objects = [gigs_hist, sellers_hist, sellers_rev]

            return objects

        @task_group(
            group_id = 'step_3_trend_periods_active_history',
            tooltip = "Active history for trends"
        )
        def step_3_trend_periods_active_history():
            return [
                trend_periods_gigs_hist_active.partial(high_level_step = '3.3').expand_kwargs(step_2_objects[1][1][1]),
                trend_periods_sellers_hist_active.partial(high_level_step = '3.4').expand_kwargs(step_2_objects[1][1][1])
            ]

        metrics_active_hist_objects = step_3_metrics_active_history()
        trend_periods_active_hist_objects = step_3_trend_periods_active_history()

        objects = [
            metrics_active_hist_objects,
            trend_periods_active_hist_objects
        ]

        chain(
            step_1_objects[0], # mj_prep_start('1.1')
            [
                metrics_active_hist_objects[0], # metrics_gigs_hist_active('3.1')
                metrics_active_hist_objects[1] # metrics_sellers_hist_active('3.2.1')
            ]
        )

        chain(
            step_1_objects[0], # mj_prep_start('1.1')
            trend_periods_active_hist_objects # step_3_trend_periods_active_history()
        )

        return objects


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 4 - Preparing regular gigs metrics, active sellers slice for metrics, and data for trends (regular gigs)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 4.1.1.1 - etl.af_metrics_reg_metrics_cur_sellers_revisions
    @task.short_circuit(
        doc_md = dedent("""Sellers revisions from current assessed period (regular metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_metrics_reg_metrics_cur_sellers_revisions.sql)"""
        )
    )
    def metrics_reg_metrics_cur_sellers_revisions(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_reg_metrics_cur_sellers_revisions",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.1.1.2 - etl.af_metrics_reg_metrics_cur_gigs_pages
    @task.short_circuit(
        doc_md = dedent("""Gigs pages from current assessed period (regular metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_metrics_reg_metrics_cur_gigs_pages.sql)"""
        )
    )
    def metrics_reg_metrics_cur_gigs_pages(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_reg_metrics_cur_gigs_pages",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.1.1.3 - etl.af_metrics_reg_metrics_cur_gigs_revisions
    @task.short_circuit(
        doc_md = dedent("""Gigs revisions from current assessed period (regular metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_metrics_reg_metrics_cur_gigs_revisions.sql)"""
        )
    )
    def metrics_reg_metrics_cur_gigs_revisions(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_reg_metrics_cur_gigs_revisions",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.1.2 - etl.af_metrics_reg_metrics_cur
    @task.short_circuit(
        doc_md = dedent("""Metrics from current assessed period (regular metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_metrics_reg_metrics_cur.sql)"""
        )
    )
    def metrics_reg_metrics_cur(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_reg_metrics_cur",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.1.3.1 - etl.af_metrics_reg_metrics_prev_sellers_revisions_filter
    @task.short_circuit(
        doc_md = dedent("""Active sellers in current period (regular metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_metrics_reg_metrics_prev_sellers_revisions_filter.sql)"""
        )
    )
    def metrics_reg_metrics_prev_sellers_revisions_filter(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_reg_metrics_prev_sellers_revisions_filter",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.1.3.2 - etl.af_metrics_reg_metrics_prev_gigs_pages
    @task.short_circuit(
        doc_md = dedent("""Gigs pages from previous assessed period (regular metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_metrics_reg_metrics_prev_gigs_pages.sql)"""
        )
    )
    def metrics_reg_metrics_prev_gigs_pages(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_reg_metrics_prev_gigs_pages",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.1.3.3 - etl.af_metrics_reg_metrics_prev_gigs_revisions
    @task.short_circuit(
        doc_md = dedent("""Gigs revisions from previous assessed period (regular metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_metrics_reg_metrics_prev_gigs_revisions.sql)"""
        )
    )
    def metrics_reg_metrics_prev_gigs_revisions(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_reg_metrics_prev_gigs_revisions",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.1.4 - etl.af_metrics_reg_metrics_prev_sellers_revisions
    @task.short_circuit(
        doc_md = dedent("""Sellers revisions from previous assessed period (regular metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_metrics_reg_metrics_prev_sellers_revisions.sql)"""
        )
    )
    def metrics_reg_metrics_prev_sellers_revisions(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_reg_metrics_prev_sellers_revisions",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.1.5 - etl.af_metrics_reg_metrics_prev
    @task.short_circuit(
        doc_md = dedent("""Metrics from previous assessed period (regular metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_metrics_reg_metrics_prev.sql)"""
        )
    )
    def metrics_reg_metrics_prev(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_reg_metrics_prev",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.1.6.1 - etl.af_metrics_reg_metrics_gig_reviews_prices_counts
    @task.short_circuit(
        doc_md = dedent("""Gig reviews prices counts (regular metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_metrics_reg_metrics_gig_reviews_prices_counts.sql)"""
        )
    )
    def metrics_reg_metrics_gig_reviews_prices_counts(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_reg_metrics_gig_reviews_prices_counts",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.1.6.2 - etl.af_metrics_reg_metrics_gig_reviews_weighted_prices
    @task.short_circuit(
        doc_md = dedent("""Gig reviews weighted prices (regular metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_metrics_reg_metrics_gig_reviews_weighted_prices.sql)"""
        )
    )
    def metrics_reg_metrics_gig_reviews_weighted_prices(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_reg_metrics_gig_reviews_weighted_prices",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.1.7 - etl.af_metrics_reg_metrics
    @task.short_circuit(
        doc_md = dedent("""Joining data from previous and current periods (regular metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_metrics_reg_metrics.sql)"""
        )
    )
    def metrics_reg_metrics(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_reg_metrics",
                func_args = {
                    "ratings_to_volume_coeff": {"value": input_args["ratings_to_volume_coeff"], "data_type": "numeric"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.2.1.1 - etl.af_trend_periods_reg_trend_prd_cur_sellers_revisions
    @task.short_circuit(
        doc_md = dedent("""Sellers revisions from current assessed period (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_trend_periods_reg_trend_prd_cur_sellers_revisions.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_reg_trend_prd_cur_sellers_revisions(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        end_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_reg_trend_prd_cur_sellers_revisions",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "end_dttm": {"value": end_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.2.1.2 - etl.af_trend_periods_reg_trend_prd_cur_gigs_pages
    @task.short_circuit(
        doc_md = dedent("""Gigs pages from current assessed period (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_trend_periods_reg_trend_prd_cur_gigs_pages.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_reg_trend_prd_cur_gigs_pages(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        end_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_reg_trend_prd_cur_gigs_pages",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "end_dttm": {"value": end_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.2.1.3 - etl.af_trend_periods_reg_trend_prd_cur_gigs_revisions
    @task.short_circuit(
        doc_md = dedent("""Gigs revisions from current assessed period (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_trend_periods_reg_trend_prd_cur_gigs_revisions.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_reg_trend_prd_cur_gigs_revisions(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        end_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_reg_trend_prd_cur_gigs_revisions",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "end_dttm": {"value": end_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.2.2 - etl.af_trend_periods_reg_trend_prd_cur
    @task.short_circuit(
        doc_md = dedent("""Metrics from current assessed period (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_trend_periods_reg_trend_prd_cur.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_reg_trend_prd_cur(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_reg_trend_prd_cur",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.2.3.1 - etl.af_trend_periods_reg_trend_prd_prev_sellers_revisions_filter
    @task.short_circuit(
        doc_md = dedent("""Active sellers in current period (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_trend_periods_reg_trend_prd_prev_sellers_revisions_filter.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_reg_trend_prd_prev_sellers_revisions_filter(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_reg_trend_prd_prev_sellers_revisions_filter",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.2.3.2 - etl.af_trend_periods_reg_trend_prd_prev_gigs_pages
    @task.short_circuit(
        doc_md = dedent("""Gigs pages from previous assessed period (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_trend_periods_reg_trend_prd_prev_gigs_pages.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_reg_trend_prd_prev_gigs_pages(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_reg_trend_prd_prev_gigs_pages",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.2.3.3 - etl.af_trend_periods_reg_trend_prd_prev_gigs_revisions
    @task.short_circuit(
        doc_md = dedent("""Gigs revisions from previous assessed period (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_trend_periods_reg_trend_prd_prev_gigs_revisions.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_reg_trend_prd_prev_gigs_revisions(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_reg_trend_prd_prev_gigs_revisions",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.2.4 - etl.af_trend_periods_reg_trend_prd_prev_sellers_revisions
    @task.short_circuit(
        doc_md = dedent("""Sellers revisions from previous assessed period (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_trend_periods_reg_trend_prd_prev_sellers_revisions.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_reg_trend_prd_prev_sellers_revisions(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_reg_trend_prd_prev_sellers_revisions",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.2.5 - etl.af_trend_periods_reg_trend_prd_prev
    @task.short_circuit(
        doc_md = dedent("""Metrics from previous assessed period (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_trend_periods_reg_trend_prd_prev.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_reg_trend_prd_prev(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_reg_trend_prd_prev",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.2.6.1 - etl.af_trend_periods_reg_trend_prd_gig_reviews_prices_counts
    @task.short_circuit(
        doc_md = dedent("""Gig reviews prices counts (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_trend_periods_reg_trend_prd_gig_reviews_prices_counts.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_reg_trend_prd_gig_reviews_prices_counts(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        end_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_reg_trend_prd_gig_reviews_prices_counts",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "end_dttm": {"value": end_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.2.6.2 - etl.af_trend_periods_reg_trend_prd_gig_reviews_weighted_prices
    @task.short_circuit(
        doc_md = dedent("""Gig reviews weighted prices (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_trend_periods_reg_trend_prd_gig_reviews_weighted_prices.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_reg_trend_prd_gig_reviews_weighted_prices(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_reg_trend_prd_gig_reviews_weighted_prices",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 4.2.7 - etl.af_trend_periods_reg_trend_prd_metrics
    @task.short_circuit(
        doc_md = dedent("""Joining data from previous and current periods (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_6/af_trend_periods_reg_trend_prd_metrics.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_reg_trend_prd_metrics(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_reg_trend_prd_metrics",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "ratings_to_volume_coeff": {"value": input_args["ratings_to_volume_coeff"], "data_type": "numeric"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    @task_group(
        group_id = 'step_4',
        tooltip = "Preparing gigs metrics and data for trends (regular gigs)"
    )
    def step_4():

        @task_group(
            group_id = 'step_4_metrics',
            tooltip = "Preparing gigs metrics data and sellers data (regular gigs)"
        )
        def step_4_metrics():

            @task_group(
                group_id = 'step_4_metrics_regular_current',
                tooltip = "Preparing gigs metrics in a current period (regular gigs)"
            )
            def step_4_metrics_regular_current():
                sel_rev = metrics_reg_metrics_cur_sellers_revisions('4.1.1.1')
                gig_page = metrics_reg_metrics_cur_gigs_pages('4.1.1.2')
                gig_rev = metrics_reg_metrics_cur_gigs_revisions('4.1.1.3')
                metrics = metrics_reg_metrics_cur('4.1.2')

                objects = [sel_rev, gig_page, gig_rev, metrics]

                chain(
                    [
                        sel_rev, # metrics_reg_metrics_cur_sellers_revisions('4.1.1.1')
                        gig_page, # metrics_reg_metrics_cur_gigs_pages('4.1.1.2')
                        gig_rev # metrics_reg_metrics_cur_gigs_revisions('4.1.1.3')
                    ],
                    metrics # metrics_reg_metrics_cur('4.1.2')
                )

                return objects

            @task_group(
                group_id = 'step_4_metrics_regular_previous',
                tooltip = "Preparing gigs metrics in a previous period (regular gigs)"
            )
            def step_4_metrics_regular_previous():
                sel_rev_filt = metrics_reg_metrics_prev_sellers_revisions_filter('4.1.3.1')
                gig_page = metrics_reg_metrics_prev_gigs_pages('4.1.3.2')
                gig_rev = metrics_reg_metrics_prev_gigs_revisions('4.1.3.3')
                sel_rev = metrics_reg_metrics_prev_sellers_revisions('4.1.4')
                metrics = metrics_reg_metrics_prev('4.1.5')

                objects = [sel_rev_filt, gig_page, gig_rev, sel_rev, metrics]

                chain(
                    sel_rev_filt, # metrics_reg_metrics_prev_sellers_revisions_filter('4.1.3.1')
                    sel_rev # metrics_reg_metrics_prev_sellers_revisions('4.1.4')
                )

                chain(
                    [
                        gig_page, # metrics_reg_metrics_prev_gigs_pages('4.1.3.2')
                        gig_rev, # metrics_reg_metrics_prev_gigs_revisions('4.1.3.3')
                        sel_rev # metrics_reg_metrics_prev_sellers_revisions('4.1.4')
                    ],
                    metrics # metrics_reg_metrics_prev('4.1.5')
                )

                return objects

            @task_group(
                group_id = 'step_4_metrics_regular_gig_reviews',
                tooltip = "Preparing gigs reviews prices (regular gigs)"
            )
            def step_4_metrics_regular_gig_reviews():
                counts = metrics_reg_metrics_gig_reviews_prices_counts('4.1.6.1')
                prices = metrics_reg_metrics_gig_reviews_weighted_prices('4.1.6.2')

                objects = [counts, prices]

                chain(*objects)

                return objects

            current_objects = step_4_metrics_regular_current()
            previous_objects = step_4_metrics_regular_previous()
            gig_reviews_objects = step_4_metrics_regular_gig_reviews()
            reg_metrics_object = metrics_reg_metrics('4.1.7')

            chain(
                current_objects[3], # metrics_reg_metrics_cur('4.1.2')
                [
                    previous_objects[0], # metrics_reg_metrics_prev_sellers_revisions_filter('4.1.3.1')
                    previous_objects[1], # metrics_reg_metrics_prev_gigs_pages('4.1.3.2')
                    previous_objects[2] # metrics_reg_metrics_prev_gigs_revisions('4.1.3.3')
                ]
            )

            chain(
                [
                    current_objects[3], # metrics_reg_metrics_cur('4.1.2')
                    previous_objects[4], # metrics_reg_metrics_prev('4.1.5')
                    gig_reviews_objects[1] # metrics_reg_metrics_gig_reviews_weighted_prices('4.1.6.2')
                ],
                reg_metrics_object # metrics_reg_metrics('4.1.7')
            )

            objects = [
                current_objects,
                previous_objects,
                gig_reviews_objects,
                reg_metrics_object
            ]

            return objects

        @task_group(
            group_id = 'step_4_trends',
            tooltip = "Preparing data for trends (regular gigs)"
        )
        def step_4_trends():

            @task_group(
                group_id = 'step_4_trend_periods_regular_current',
                tooltip = "Preparing data for trends (regular gigs) in a current period"
            )
            def step_4_trend_periods_regular_current():
                sel_rev = trend_periods_reg_trend_prd_cur_sellers_revisions.partial(high_level_step = '4.2.1.1').expand_kwargs(step_2_objects[1][1][1])
                gig_page = trend_periods_reg_trend_prd_cur_gigs_pages.partial(high_level_step = '4.2.1.2').expand_kwargs(step_2_objects[1][1][1])
                gig_rev = trend_periods_reg_trend_prd_cur_gigs_revisions.partial(high_level_step = '4.2.1.3').expand_kwargs(step_2_objects[1][1][1])
                metrics = trend_periods_reg_trend_prd_cur.partial(high_level_step = '4.2.2').expand(period_num = step_2_objects[1][1][3])

                objects = [sel_rev, gig_page, gig_rev, metrics]

                chain(
                    [
                        sel_rev, # trend_periods_reg_trend_prd_cur_sellers_revisions.partial(high_level_step = '4.2.1.1').expand_kwargs(step_2_objects[1][1][1])
                        gig_page, # trend_periods_reg_trend_prd_cur_gigs_pages.partial(high_level_step = '4.2.1.2').expand_kwargs(step_2_objects[1][1][1])
                        gig_rev # trend_periods_reg_trend_prd_cur_gigs_revisions.partial(high_level_step = '4.2.1.3').expand_kwargs(step_2_objects[1][1][1])
                    ],
                    metrics # trend_periods_reg_trend_prd_cur.partial(high_level_step = '4.2.2').expand(period_num = step_2_objects[1][1][3])
                )

                return objects

            @task_group(
                group_id = 'step_4_trend_periods_regular_previous',
                tooltip = "Preparing data for trends (regular gigs) in a previous period"
            )
            def step_4_trend_periods_regular_previous():
                sel_rev_filt = trend_periods_reg_trend_prd_prev_sellers_revisions_filter.partial(high_level_step = '4.2.3.1').expand(period_num = step_2_objects[1][1][3])
                gig_page = trend_periods_reg_trend_prd_prev_gigs_pages.partial(high_level_step = '4.2.3.2').expand_kwargs(step_2_objects[1][1][2])
                gig_rev = trend_periods_reg_trend_prd_prev_gigs_revisions.partial(high_level_step = '4.2.3.3').expand_kwargs(step_2_objects[1][1][2])
                sel_rev = trend_periods_reg_trend_prd_prev_sellers_revisions.partial(high_level_step = '4.2.4').expand_kwargs(step_2_objects[1][1][2])
                metrics = trend_periods_reg_trend_prd_prev.partial(high_level_step = '4.2.5').expand(period_num = step_2_objects[1][1][3])

                objects = [sel_rev_filt, gig_page, gig_rev, sel_rev, metrics]

                chain(
                    sel_rev_filt, # trend_periods_reg_trend_prd_prev_sellers_revisions_filter.partial(high_level_step = '4.2.3.1').expand(period_num = step_2_objects[1][1][3])
                    sel_rev # trend_periods_reg_trend_prd_prev_sellers_revisions.partial(high_level_step = '4.2.4').expand_kwargs(step_2_objects[1][1][2])
                )

                chain(
                    [
                        gig_page, # trend_periods_reg_trend_prd_prev_gigs_pages.partial(high_level_step = '4.2.3.2').expand_kwargs(step_2_objects[1][1][2])
                        gig_rev, # trend_periods_reg_trend_prd_prev_gigs_revisions.partial(high_level_step = '4.2.3.3').expand_kwargs(step_2_objects[1][1][2])
                        sel_rev # trend_periods_reg_trend_prd_prev_sellers_revisions.partial(high_level_step = '4.2.4').expand_kwargs(step_2_objects[1][1][2])
                    ],
                    metrics # trend_periods_reg_trend_prd_prev.partial(high_level_step = '4.2.5').expand(period_num = step_2_objects[1][1][3])
                )

                return objects

            @task_group(
                group_id = 'step_4_trend_periods_regular_gig_reviews',
                tooltip = "Preparing gigs reviews prices (regular trends)"
            )
            def step_4_trend_periods_regular_gig_reviews():
                counts = trend_periods_reg_trend_prd_gig_reviews_prices_counts.partial(high_level_step = '4.2.6.1').expand_kwargs(step_2_objects[1][1][1])
                prices = trend_periods_reg_trend_prd_gig_reviews_weighted_prices.partial(high_level_step = '4.2.6.2').expand(period_num = step_2_objects[1][1][3])

                objects = [counts, prices]

                chain(*objects)

                return objects

            current_objects = step_4_trend_periods_regular_current()
            previous_objects = step_4_trend_periods_regular_previous()
            gig_reviews_objects = step_4_trend_periods_regular_gig_reviews()
            reg_trends_object = trend_periods_reg_trend_prd_metrics.partial(high_level_step = '4.2.7').expand(period_num = step_2_objects[1][1][3])

            chain(
                current_objects[3], # trend_periods_reg_trend_prd_cur.partial(high_level_step = '4.2.2').expand(period_num = step_2_objects[1][1][3])
                [
                    previous_objects[0], # trend_periods_reg_trend_prd_prev_sellers_revisions_filter.partial(high_level_step = '4.2.3.1').expand(period_num = step_2_objects[1][1][3])
                    previous_objects[1], # trend_periods_reg_trend_prd_prev_gigs_pages.partial(high_level_step = '4.2.3.2').expand_kwargs(step_2_objects[1][1][2])
                    previous_objects[2] # trend_periods_reg_trend_prd_prev_gigs_revisions.partial(high_level_step = '4.2.3.3').expand_kwargs(step_2_objects[1][1][2])
                ]
            )

            chain(
                [
                    current_objects[3], # trend_periods_reg_trend_prd_cur.partial(high_level_step = '4.2.2').expand(period_num = step_2_objects[1][1][3])
                    previous_objects[4], # trend_periods_reg_trend_prd_prev.partial(high_level_step = '4.2.5').expand(period_num = step_2_objects[1][1][3])
                    gig_reviews_objects[1] # trend_periods_reg_trend_prd_gig_reviews_weighted_prices.partial(high_level_step = '4.2.6.2').expand(period_num = step_2_objects[1][1][3])
                ],
                reg_trends_object # trend_periods_reg_trend_prd_metrics.partial(high_level_step = '4.2.7').expand(period_num = step_2_objects[1][1][3])
            )

            objects = [
                current_objects,
                previous_objects,
                gig_reviews_objects,
                reg_trends_object
            ]

            return objects

        metrics_objects = step_4_metrics()
        trends_objects = step_4_trends()

        objects = [
            metrics_objects,
            trends_objects
        ]

        chain(
            step_3_objects[0][0], # metrics_gigs_hist_active('3.1')
            [
                metrics_objects[0][1], # metrics_reg_metrics_cur_gigs_pages('4.1.1.2')
                metrics_objects[0][2], # metrics_reg_metrics_cur_gigs_revisions('4.1.1.3')
                metrics_objects[2][0] # metrics_reg_metrics_gig_reviews_prices_counts('4.1.6.1')
            ]
        )

        chain(
            step_3_objects[1][0], # trend_periods_gigs_hist_active.partial(high_level_step = '3.3').expand_kwargs(step_2_objects[1][1][1])
            [
                trends_objects[0][1], # trend_periods_reg_trend_prd_cur_gigs_pages.partial(high_level_step = '4.2.1.2').expand_kwargs(step_2_objects[1][1][1])
                trends_objects[0][2], # trend_periods_reg_trend_prd_cur_gigs_revisions.partial(high_level_step = '4.2.1.3').expand_kwargs(step_2_objects[1][1][1])
                trends_objects[2][0] # trend_periods_reg_trend_prd_gig_reviews_prices_counts.partial(high_level_step = '4.2.6.1').expand_kwargs(step_2_objects[1][1][1])
            ]
        )

        chain(
            step_3_objects[0][1], # metrics_sellers_hist_active('3.2.1')
            metrics_objects[0][0] # metrics_reg_metrics_cur_sellers_revisions('4.1.1.1')
        )

        chain(
            step_3_objects[1][1], # trend_periods_sellers_hist_active.partial(high_level_step = '3.4').expand_kwargs(step_2_objects[1][1][1])
            trends_objects[0][0] # trend_periods_reg_trend_prd_cur_sellers_revisions.partial(high_level_step = '4.2.1.1').expand_kwargs(step_2_objects[1][1][1])
        )

        return objects


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 5 - Preparing heuristic gigs metrics, sellers data, and data for trends (heuristic gigs)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 5.1.1.1 - etl.af_metrics_heur_metrics_cur_sellers_revisions
    @task.short_circuit(
        doc_md = dedent("""Sellers revisions from current assessed period (heuristic metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_metrics_heur_metrics_cur_sellers_revisions.sql)"""
        )
    )
    def metrics_heur_metrics_cur_sellers_revisions(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_heur_metrics_cur_sellers_revisions",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.1.1.2 - etl.af_metrics_heur_metrics_cur_gigs_pages
    @task.short_circuit(
        doc_md = dedent("""Gigs pages from current assessed period (heuristic metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_metrics_heur_metrics_cur_gigs_pages.sql)"""
        )
    )
    def metrics_heur_metrics_cur_gigs_pages(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_heur_metrics_cur_gigs_pages",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.1.1.3 - etl.af_metrics_heur_metrics_cur_gigs_revisions
    @task.short_circuit(
        doc_md = dedent("""Gigs revisions from current assessed period (heuristic metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_metrics_heur_metrics_cur_gigs_revisions.sql)"""
        )
    )
    def metrics_heur_metrics_cur_gigs_revisions(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_heur_metrics_cur_gigs_revisions",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.1.2 - etl.af_metrics_heur_metrics_cur
    @task.short_circuit(
        doc_md = dedent("""Metrics from current assessed period (heuristic metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_metrics_heur_metrics_cur.sql)"""
        )
    )
    def metrics_heur_metrics_cur(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_heur_metrics_cur",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.1.3.1 - etl.af_metrics_heur_metrics_prev_sellers_revisions_filter
    @task.short_circuit(
        doc_md = dedent("""Active sellers in current period (heuristic metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_metrics_heur_metrics_prev_sellers_revisions_filter.sql)"""
        )
    )
    def metrics_heur_metrics_prev_sellers_revisions_filter(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_heur_metrics_prev_sellers_revisions_filter",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.1.3.2 - etl.af_metrics_heur_metrics_prev_gigs_pages
    @task.short_circuit(
        doc_md = dedent("""Gigs pages from previous assessed period (heuristic metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_metrics_heur_metrics_prev_gigs_pages.sql)"""
        )
    )
    def metrics_heur_metrics_prev_gigs_pages(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_heur_metrics_prev_gigs_pages",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.1.3.3 - etl.af_metrics_heur_metrics_prev_gigs_revisions
    @task.short_circuit(
        doc_md = dedent("""Gigs revisions from previous assessed period (heuristic metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_metrics_heur_metrics_prev_gigs_revisions.sql)"""
        )
    )
    def metrics_heur_metrics_prev_gigs_revisions(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_heur_metrics_prev_gigs_revisions",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.1.4 - etl.af_metrics_heur_metrics_prev_sellers_revisions
    @task.short_circuit(
        doc_md = dedent("""Sellers revisions from previous assessed period (heuristic metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_metrics_heur_metrics_prev_sellers_revisions.sql)"""
        )
    )
    def metrics_heur_metrics_prev_sellers_revisions(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_heur_metrics_prev_sellers_revisions",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.1.5 - etl.af_metrics_heur_metrics_prev
    @task.short_circuit(
        doc_md = dedent("""Metrics from previous assessed period (heuristic metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_metrics_heur_metrics_prev.sql)"""
        )
    )
    def metrics_heur_metrics_prev(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_heur_metrics_prev",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.1.6.1 - etl.af_metrics_heur_metrics_gig_reviews_prices_counts
    @task.short_circuit(
        doc_md = dedent("""Gig reviews prices counts (heuristic metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_metrics_heur_metrics_gig_reviews_prices_counts.sql)"""
        )
    )
    def metrics_heur_metrics_gig_reviews_prices_counts(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_heur_metrics_gig_reviews_prices_counts",
                func_args = {
                    "start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.1.6.2 - etl.af_metrics_heur_metrics_gig_reviews_weighted_prices
    @task.short_circuit(
        doc_md = dedent("""Gig reviews weighted prices (heuristic metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_metrics_heur_metrics_gig_reviews_weighted_prices.sql)"""
        )
    )
    def metrics_heur_metrics_gig_reviews_weighted_prices(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_heur_metrics_gig_reviews_weighted_prices",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.1.7 - etl.af_metrics_heur_metrics
    @task.short_circuit(
        doc_md = dedent("""Joining data from previous and current periods (heuristic metrics).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_metrics_heur_metrics.sql)"""
        )
    )
    def metrics_heur_metrics(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_heur_metrics",
                func_args = {
                    "ratings_to_volume_coeff": {"value": input_args["ratings_to_volume_coeff"], "data_type": "numeric"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.2.1.1 - etl.af_trend_periods_heur_trend_prd_cur_sellers_revisions
    @task.short_circuit(
        doc_md = dedent("""Sellers revisions from current assessed period (heuristic trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_trend_periods_heur_trend_prd_cur_sellers_revisions.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_heur_trend_prd_cur_sellers_revisions(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_heur_trend_prd_cur_sellers_revisions",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.2.1.2 - etl.af_trend_periods_heur_trend_prd_cur_gigs_pages
    @task.short_circuit(
        doc_md = dedent("""Gigs pages from current assessed period (heuristic trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_trend_periods_heur_trend_prd_cur_gigs_pages.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_heur_trend_prd_cur_gigs_pages(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_heur_trend_prd_cur_gigs_pages",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.2.1.3 - etl.af_trend_periods_heur_trend_prd_cur_gigs_revisions
    @task.short_circuit(
        doc_md = dedent("""Gigs revisions from current assessed period (heuristic trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_trend_periods_heur_trend_prd_cur_gigs_revisions.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_heur_trend_prd_cur_gigs_revisions(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        end_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_heur_trend_prd_cur_gigs_revisions",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "end_dttm": {"value": end_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.2.2 - etl.af_trend_periods_heur_trend_prd_cur
    @task.short_circuit(
        doc_md = dedent("""Metrics from current assessed period (heuristic trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_trend_periods_heur_trend_prd_cur.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_heur_trend_prd_cur(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_heur_trend_prd_cur",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.2.3.1 - etl.af_trend_periods_heur_trend_prd_prev_sellers_revisions_filter
    @task.short_circuit(
        doc_md = dedent("""Active sellers in current period (heuristic trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_trend_periods_heur_trend_prd_prev_sellers_revisions_filter.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_heur_trend_prd_prev_sellers_revisions_filter(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_heur_trend_prd_prev_sellers_revisions_filter",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.2.3.2 - etl.af_trend_periods_heur_trend_prd_prev_gigs_pages
    @task.short_circuit(
        doc_md = dedent("""Gigs pages from previous assessed period (heuristic trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_trend_periods_heur_trend_prd_prev_gigs_pages.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_heur_trend_prd_prev_gigs_pages(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        end_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_heur_trend_prd_prev_gigs_pages",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "end_dttm": {"value": end_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.2.3.3 - etl.af_trend_periods_heur_trend_prd_prev_gigs_revisions
    @task.short_circuit(
        doc_md = dedent("""Gigs revisions from previous assessed period (heuristic trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_trend_periods_heur_trend_prd_prev_gigs_revisions.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_heur_trend_prd_prev_gigs_revisions(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        end_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_heur_trend_prd_prev_gigs_revisions",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "end_dttm": {"value": end_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.2.4 - etl.af_trend_periods_heur_trend_prd_prev_sellers_revisions
    @task.short_circuit(
        doc_md = dedent("""Sellers revisions from previous assessed period (heuristic trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_trend_periods_heur_trend_prd_prev_sellers_revisions.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_heur_trend_prd_prev_sellers_revisions(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        end_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_heur_trend_prd_prev_sellers_revisions",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "end_dttm": {"value": end_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.2.5 - etl.af_trend_periods_heur_trend_prd_prev
    @task.short_circuit(
        doc_md = dedent("""Metrics from previous assessed period (heuristic trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_trend_periods_heur_trend_prd_prev.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_heur_trend_prd_prev(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_heur_trend_prd_prev",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.2.6.1 - etl.af_trend_periods_heur_trend_prd_gig_reviews_prices_counts
    @task.short_circuit(
        doc_md = dedent("""Gig reviews prices counts (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_trend_periods_heur_trend_prd_gig_reviews_prices_counts.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_heur_trend_prd_gig_reviews_prices_counts(
        high_level_step: str,
        start_dttm: pendulum.DateTime,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_heur_trend_prd_gig_reviews_prices_counts",
                func_args = {
                    "start_dttm": {"value": start_dttm, "data_type": "timestamptz"},
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.2.6.2 - etl.af_trend_periods_heur_trend_prd_gig_reviews_weighted_prices
    @task.short_circuit(
        doc_md = dedent("""Gig reviews weighted prices (regular trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_trend_periods_heur_trend_prd_gig_reviews_weighted_prices.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_heur_trend_prd_gig_reviews_weighted_prices(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_heur_trend_prd_gig_reviews_weighted_prices",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 5.2.7 - etl.af_trend_periods_heur_trend_prd_metrics
    @task.short_circuit(
        doc_md = dedent("""Joining data from previous and current periods (heuristic trends).
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_7/af_trend_periods_heur_trend_prd_metrics.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_heur_trend_prd_metrics(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_heur_trend_prd_metrics",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "ratings_to_volume_coeff": {"value": input_args["ratings_to_volume_coeff"], "data_type": "numeric"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    @task_group(
        group_id = 'step_5',
        tooltip = "Preparing gigs metrics and data for trends (heuristic gigs)"
    )
    def step_5():

        @task_group(
            group_id = 'step_5_metrics',
            tooltip = "Preparing gigs metrics data and sellers data (heuristic gigs)"
        )
        def step_5_metrics():

            @task_group(
                group_id = 'step_5_metrics_heuristic_current',
                tooltip = "Preparing gigs metrics in a current period (heuristic gigs)"
            )
            def step_5_metrics_heuristic_current():
                sel_rev = metrics_heur_metrics_cur_sellers_revisions('5.1.1.1')
                gig_page = metrics_heur_metrics_cur_gigs_pages('5.1.1.2')
                gig_rev = metrics_heur_metrics_cur_gigs_revisions('5.1.1.3')
                metrics = metrics_heur_metrics_cur('5.1.2')

                objects = [sel_rev, gig_page, gig_rev, metrics]

                chain(
                    [
                        sel_rev, # metrics_heur_metrics_cur_sellers_revisions('5.1.1.1')
                        gig_page, # metrics_heur_metrics_cur_gigs_pages('5.1.1.2')
                        gig_rev # metrics_heur_metrics_cur_gigs_revisions('5.1.1.3')
                    ],
                    metrics # metrics_heur_metrics_cur('5.1.2')
                )

                return objects

            @task_group(
                group_id = 'step_5_metrics_heuristic_previous',
                tooltip = "Preparing gigs metrics in a previous period (heuristic gigs)"
            )
            def step_5_metrics_heuristic_previous():
                sel_rev_filt = metrics_heur_metrics_prev_sellers_revisions_filter('5.1.3.1')
                gig_page = metrics_heur_metrics_prev_gigs_pages('5.1.3.2')
                gig_rev = metrics_heur_metrics_prev_gigs_revisions('5.1.3.3')
                sel_rev = metrics_heur_metrics_prev_sellers_revisions('5.1.4')
                metrics = metrics_heur_metrics_prev('5.1.5')

                objects = [sel_rev_filt, gig_page, gig_rev, sel_rev, metrics]

                chain(
                    sel_rev_filt, # metrics_heur_metrics_prev_sellers_revisions_filter('5.1.3.1')
                    sel_rev # metrics_heur_metrics_prev_sellers_revisions('5.1.4')
                )

                chain(
                    [
                        gig_page, # metrics_heur_metrics_prev_gigs_pages('5.1.3.2')
                        gig_rev, # metrics_heur_metrics_prev_gigs_revisions('5.1.3.3')
                        sel_rev # metrics_heur_metrics_prev_sellers_revisions('5.1.4')
                    ],
                    metrics # metrics_heur_metrics_prev('5.1.5')
                )

                return objects

            @task_group(
                group_id = 'step_5_metrics_heuristic_gig_reviews',
                tooltip = "Preparing gigs reviews prices (heuristic gigs)"
            )
            def step_5_metrics_heuristic_gig_reviews():
                counts = metrics_heur_metrics_gig_reviews_prices_counts('5.1.6.1')
                prices = metrics_heur_metrics_gig_reviews_weighted_prices('5.1.6.2')

                objects = [counts, prices]

                chain(*objects)

                return objects

            current_objects = step_5_metrics_heuristic_current()
            previous_objects = step_5_metrics_heuristic_previous()
            gig_reviews_objects = step_5_metrics_heuristic_gig_reviews()
            reg_metrics_object = metrics_heur_metrics('5.1.7')

            chain(
                current_objects[3], # metrics_heur_metrics_cur('5.1.2')
                [
                    previous_objects[0], # metrics_heur_metrics_prev_sellers_revisions_filter('5.1.3.1')
                    previous_objects[1], # metrics_heur_metrics_prev_gigs_pages('5.1.3.2')
                    previous_objects[2] # metrics_heur_metrics_prev_gigs_revisions('5.1.3.3')
                ]
            )

            chain(
                [
                    current_objects[3], # metrics_heur_metrics_cur('5.1.2')
                    previous_objects[4], # metrics_heur_metrics_prev('5.1.5')
                    gig_reviews_objects[1] # metrics_heur_metrics_gig_reviews_weighted_prices('5.1.6.2')
                ],
                reg_metrics_object # metrics_heur_metrics('5.1.7')
            )

            objects = [
                current_objects,
                previous_objects,
                gig_reviews_objects,
                reg_metrics_object
            ]

            return objects

        @task_group(
            group_id = 'step_5_trends',
            tooltip = "Preparing data for trends (heuristic gigs)"
        )
        def step_5_trends():

            @task_group(
                group_id = 'step_5_trend_periods_heuristic_current',
                tooltip = "Preparing data for trends (heuristic gigs) in a current period"
            )
            def step_5_trend_periods_heuristic_current():
                sel_rev = trend_periods_heur_trend_prd_cur_sellers_revisions.partial(high_level_step = '5.2.1.1').expand_kwargs(step_2_objects[1][1][2])
                gig_page = trend_periods_heur_trend_prd_cur_gigs_pages.partial(high_level_step = '5.2.1.2').expand_kwargs(step_2_objects[1][1][2])
                gig_rev = trend_periods_heur_trend_prd_cur_gigs_revisions.partial(high_level_step = '5.2.1.3').expand_kwargs(step_2_objects[1][1][1])
                metrics = trend_periods_heur_trend_prd_cur.partial(high_level_step = '5.2.2').expand(period_num = step_2_objects[1][1][3])

                objects = [sel_rev, gig_page, gig_rev, metrics]

                chain(
                    [
                        sel_rev, # trend_periods_heur_trend_prd_cur_sellers_revisions.partial(high_level_step = '5.2.1.1').expand_kwargs(step_2_objects[1][1][2])
                        gig_page, # trend_periods_heur_trend_prd_cur_gigs_pages.partial(high_level_step = '5.2.1.2').expand_kwargs(step_2_objects[1][1][2])
                        gig_rev # trend_periods_heur_trend_prd_cur_gigs_revisions.partial(high_level_step = '5.2.1.3').expand_kwargs(step_2_objects[1][1][1])
                    ],
                    metrics # trend_periods_heur_trend_prd_cur.partial(high_level_step = '5.2.2').expand(period_num = step_2_objects[1][1][3])
                )

                return objects

            @task_group(
                group_id = 'step_5_trend_periods_heuristic_previous',
                tooltip = "Preparing data for trends (heuristic gigs) in a previous period"
            )
            def step_5_trend_periods_heuristic_previous():
                sel_rev_filt = trend_periods_heur_trend_prd_prev_sellers_revisions_filter.partial(high_level_step = '5.2.3.1').expand(period_num = step_2_objects[1][1][3])
                gig_page = trend_periods_heur_trend_prd_prev_gigs_pages.partial(high_level_step = '5.2.3.2').expand_kwargs(step_2_objects[1][1][1])
                gig_rev = trend_periods_heur_trend_prd_prev_gigs_revisions.partial(high_level_step = '5.2.3.3').expand_kwargs(step_2_objects[1][1][1])
                sel_rev = trend_periods_heur_trend_prd_prev_sellers_revisions.partial(high_level_step = '5.2.4').expand_kwargs(step_2_objects[1][1][1])
                metrics = trend_periods_heur_trend_prd_prev.partial(high_level_step = '5.2.5').expand(period_num = step_2_objects[1][1][3])

                objects = [sel_rev_filt, gig_page, gig_rev, sel_rev, metrics]

                chain(
                    sel_rev_filt, # trend_periods_heur_trend_prd_prev_sellers_revisions_filter.partial(high_level_step = '5.2.3.1').expand(period_num = step_2_objects[1][1][3])
                    sel_rev # trend_periods_heur_trend_prd_prev_sellers_revisions.partial(high_level_step = '5.2.4').expand_kwargs(step_2_objects[1][1][1])
                )

                chain(
                    [
                        gig_page, # trend_periods_heur_trend_prd_prev_gigs_pages.partial(high_level_step = '5.2.3.2').expand_kwargs(step_2_objects[1][1][1])
                        gig_rev, # trend_periods_heur_trend_prd_prev_gigs_revisions.partial(high_level_step = '5.2.3.3').expand_kwargs(step_2_objects[1][1][1])
                        sel_rev # trend_periods_heur_trend_prd_prev_sellers_revisions.partial(high_level_step = '5.2.4').expand_kwargs(step_2_objects[1][1][1])
                    ],
                    metrics # trend_periods_heur_trend_prd_prev.partial(high_level_step = '5.2.5').expand(period_num = step_2_objects[1][1][3])
                )

                return objects

            @task_group(
                group_id = 'step_5_trend_periods_heuristic_gig_reviews',
                tooltip = "Preparing gigs reviews prices (heuristic trends)"
            )
            def step_5_trend_periods_heuristic_gig_reviews():
                counts = trend_periods_heur_trend_prd_gig_reviews_prices_counts.partial(high_level_step = '5.2.6.1').expand_kwargs(step_2_objects[1][1][2])
                prices = trend_periods_heur_trend_prd_gig_reviews_weighted_prices.partial(high_level_step = '5.2.6.2').expand(period_num = step_2_objects[1][1][3])

                objects = [counts, prices]

                chain(*objects)

                return objects

            current_objects = step_5_trend_periods_heuristic_current()
            previous_objects = step_5_trend_periods_heuristic_previous()
            gig_reviews_objects = step_5_trend_periods_heuristic_gig_reviews()
            reg_trends_object = trend_periods_heur_trend_prd_metrics.partial(high_level_step = '5.2.7').expand(period_num = step_2_objects[1][1][3])

            chain(
                current_objects[3], # trend_periods_heur_trend_prd_cur.partial(high_level_step = '5.2.2').expand(period_num = step_2_objects[1][1][3])
                [
                    previous_objects[0], # trend_periods_heur_trend_prd_prev_sellers_revisions_filter.partial(high_level_step = '5.2.3.1').expand(period_num = step_2_objects[1][1][3])
                    previous_objects[1], # trend_periods_heur_trend_prd_prev_gigs_pages.partial(high_level_step = '5.2.3.2').expand_kwargs(step_2_objects[1][1][1])
                    previous_objects[2] # # trend_periods_heur_trend_prd_prev_gigs_revisions.partial(high_level_step = '5.2.3.3').expand_kwargs(step_2_objects[1][1][1])
                ]
            )

            chain(
                [
                    current_objects[3], # trend_periods_heur_trend_prd_cur.partial(high_level_step = '5.2.2').expand(period_num = step_2_objects[1][1][3])
                    previous_objects[4], # trend_periods_heur_trend_prd_prev.partial(high_level_step = '5.2.5').expand(period_num = step_2_objects[1][1][3])
                    gig_reviews_objects[1] # trend_periods_heur_trend_prd_gig_reviews_weighted_prices.partial(high_level_step = '5.2.6.2').expand(period_num = step_2_objects[1][1][3])
                ],
                reg_trends_object # trend_periods_heur_trend_prd_metrics.partial(high_level_step = '5.2.7').expand(period_num = step_2_objects[1][1][3])
            )

            objects = [
                current_objects,
                previous_objects,
                gig_reviews_objects,
                reg_trends_object
            ]

            return objects

        metrics_objects = step_5_metrics()
        trends_objects = step_5_trends()

        objects = [
            metrics_objects,
            trends_objects
        ]

        chain(
            step_3_objects[0][0], # metrics_gigs_hist_active('3.1')
            [
                metrics_objects[0][1], # metrics_heur_metrics_cur_gigs_pages('5.1.1.2')
                metrics_objects[0][2], # metrics_heur_metrics_cur_gigs_revisions('5.1.1.3')
                metrics_objects[2][0] # metrics_heur_metrics_gig_reviews_prices_counts('5.1.6.1')
            ]
        )

        chain(
            step_3_objects[1][0], # trend_periods_gigs_hist_active.partial(high_level_step = '3.3').expand_kwargs(step_2_objects[1][1][1])
            [
                trends_objects[0][1], # trend_periods_heur_trend_prd_cur_gigs_pages.partial(high_level_step = '5.2.1.2').expand_kwargs(step_2_objects[1][1][2])
                trends_objects[0][2], # trend_periods_heur_trend_prd_cur_gigs_revisions.partial(high_level_step = '5.2.1.3').expand_kwargs(step_2_objects[1][1][1])
                trends_objects[2][0] # trend_periods_heur_trend_prd_gig_reviews_prices_counts.partial(high_level_step = '5.2.6.1').expand_kwargs(step_2_objects[1][1][2])
            ]
        )

        chain(
            step_3_objects[0][1], # metrics_sellers_hist_active('3.2.1')
            metrics_objects[0][0] # metrics_heur_metrics_cur_sellers_revisions('5.1.1.1')
        )

        chain(
            step_3_objects[1][1], # trend_periods_sellers_hist_active.partial(high_level_step = '3.4').expand_kwargs(step_2_objects[1][1][1])
            trends_objects[0][0] # trend_periods_heur_trend_prd_cur_sellers_revisions.partial(high_level_step = '5.2.1.1').expand_kwargs(step_2_objects[1][1][2])
        )

        chain(
            step_4_objects[0][0][3], # metrics_reg_metrics_cur('4.1.2')
            [
                metrics_objects[0][1], # metrics_heur_metrics_cur_gigs_pages('5.1.1.2')
                metrics_objects[0][2], # metrics_heur_metrics_cur_gigs_revisions('5.1.1.3')
                metrics_objects[2][0] # metrics_heur_metrics_gig_reviews_prices_counts('5.1.6.1')
            ]
        )

        chain(
            step_4_objects[1][0][3], # trend_periods_reg_trend_prd_cur.partial(high_level_step = '4.2.2').expand(period_num = step_2_objects[1][1][3])
            [
                trends_objects[0][1], # trend_periods_heur_trend_prd_cur_gigs_pages.partial(high_level_step = '5.2.1.2').expand_kwargs(step_2_objects[1][1][2])
                trends_objects[0][2], # trend_periods_heur_trend_prd_cur_gigs_revisions.partial(high_level_step = '5.2.1.3').expand_kwargs(step_2_objects[1][1][1])
                trends_objects[2][0] # trend_periods_heur_trend_prd_gig_reviews_prices_counts.partial(high_level_step = '5.2.6.1').expand_kwargs(step_2_objects[1][1][2])
            ]
        )

        return objects


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 6 - Regular + heuristic data union
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 6.1 - etl.af_metrics_union
    @task.short_circuit(
        doc_md = dedent("""Union regular and heuristic gigs metrics data.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_8/af_metrics_union.sql)"""
        )
    )
    def metrics_union(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_union",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 6.2 - etl.af_trend_prd_metrics_union
    @task.short_circuit(
        doc_md = dedent("""Union regular and heuristic trends data for gigs in a trend period.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_8/af_trend_prd_metrics_union.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_prd_metrics_union(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_prd_metrics_union",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    @task_group(
        group_id = 'step_6',
        tooltip = "Union data for metrics and trends (regular + heuristic)"
    )
    def step_6():

        objects = [
            metrics_union('6.1'),
            trend_prd_metrics_union.partial(high_level_step = '6.2').expand(period_num = step_2_objects[1][1][3])
        ]

        chain(
            [
                step_4_objects[0][3], # metrics_reg_metrics('4.1.7')
                step_5_objects[0][3] # metrics_heur_metrics('5.1.7')
            ],
            objects[0] # metrics_union('6.1')
        )

        chain(
            [
                step_4_objects[1][3], # trend_periods_reg_trend_prd_metrics.partial(high_level_step = '4.2.7').expand(period_num = step_2_objects[1][1][3])
                step_5_objects[1][3], # trend_periods_heur_trend_prd_metrics.partial(high_level_step = '5.2.7').expand(period_num = step_2_objects[1][1][3])
            ],
            objects[1] # trend_prd_metrics_union.partial(high_level_step = '6.2').expand(period_num = step_2_objects[1][1][3])
        )

        return objects


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 7 - Final calculations for metrics and trends periods data
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 7.1.1 - etl.af_metrics_categories_names
    @task.short_circuit(
        doc_md = dedent("""Heuristics to fill categories names.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_9/af_metrics_categories_names.sql)"""
        )
    )
    def metrics_categories_names(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_categories_names",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 7.1.2 - etl.af_metrics_categories
    @task.short_circuit(
        doc_md = dedent("""Categories' metrics.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_9/af_metrics_categories.sql)"""
        )
    )
    def metrics_categories(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_categories",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 7.1.3 - etl.af_metrics_join
    @task.short_circuit(
        doc_md = dedent("""Combining attributes from gigs, sellers and categories.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_9/af_metrics_join.sql)"""
        )
    )
    def metrics_join(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_join",
                func_args = {
                    "overpriced_coeff": {"value": input_args["overpriced_coeff"], "data_type": "numeric"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 7.1.4 - etl.af_metrics_competition_score_prepare
    @task.short_circuit(
        doc_md = dedent("""Preparing dataset for categories' competition score calculations.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_9/af_metrics_competition_score_prepare.sql)"""
        )
    )
    def metrics_competition_score_prepare(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_competition_score_prepare",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 7.1.5.1 - etl.af_metrics_categories_percentile
    @task.short_circuit(
        doc_md = dedent("""Sales percentile for categories' competition score.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_9/af_metrics_categories_percentile.sql)"""
        )
    )
    def metrics_categories_percentile(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_categories_percentile",
                func_args = {
                    "competition_score_percentile": {"value": input_args["competition_score_percentile"], "data_type": "numeric"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 7.1.5.2 - etl.af_metrics_subcategories_percentile
    @task.short_circuit(
        doc_md = dedent("""Sales percentile for subcategories' competition score.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_9/af_metrics_subcategories_percentile.sql)"""
        )
    )
    def metrics_subcategories_percentile(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_subcategories_percentile",
                func_args = {
                    "competition_score_percentile": {"value": input_args["competition_score_percentile"], "data_type": "numeric"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 7.1.5.3 - etl.af_metrics_nested_subcategories_percentile
    @task.short_circuit(
        doc_md = dedent("""Sales percentile for nested subcategories' competition score.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_9/af_metrics_nested_subcategories_percentile.sql)"""
        )
    )
    def metrics_nested_subcategories_percentile(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_nested_subcategories_percentile",
                func_args = {
                    "competition_score_percentile": {"value": input_args["competition_score_percentile"], "data_type": "numeric"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 7.1.6 - etl.af_metrics_final
    @task.short_circuit(
        doc_md = dedent("""Final transformations for metrics.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_9/af_metrics_final.sql)"""
        )
    )
    def metrics_final(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_metrics_final",
                func_args = {
                    "competition_score_params": {"value": input_args["competition_score_params"], "data_type": "jsonb"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 7.2.1 - etl.af_trend_periods_weighted_prices
    @task.short_circuit(
        doc_md = dedent("""Calculating weighted prices in subcategories.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_9/af_trend_periods_weighted_prices.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_weighted_prices(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_weighted_prices",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 7.2.2 - etl.af_trend_periods_revenue
    @task.short_circuit(
        doc_md = dedent("""Calculating gigs' revenues by trend period.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_9/af_trend_periods_revenue.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_revenue(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_revenue",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "overpriced_coeff": {"value": input_args["overpriced_coeff"], "data_type": "numeric"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 7.2.3 - etl.af_trend_periods_final
    @task.short_circuit(
        doc_md = dedent("""Inserting results into trends_prefin table.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_9/af_trend_periods_final.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def trend_periods_final(
        high_level_step: str,
        period_num: int
    ):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')
        context["db_map_index"] = f"Trend period number: {period_num}"

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trend_periods_final",
                func_args = {
                    "period_num": {"value": period_num, "data_type": "int2"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    @task_group(
        group_id = 'step_7',
        tooltip = "Final calculations for metrics and trends periods data"
    )
    def step_7():

        @task_group(
            group_id = 'step_7_metrics',
            tooltip = "Final calculations for metrics data"
        )
        def step_7_metrics():
            objects = [
                metrics_categories_names('7.1.1'),
                metrics_categories('7.1.2'),
                metrics_join('7.1.3'),
                metrics_competition_score_prepare('7.1.4'),
                [
                    metrics_categories_percentile('7.1.5.1'),
                    metrics_subcategories_percentile('7.1.5.2'),
                    metrics_nested_subcategories_percentile('7.1.5.3')
                ],
                metrics_final('7.1.6')
            ]

            chain(*objects)

            chain(
                objects[2], # metrics_join('7.1.3')
                objects[5] # metrics_final('7.1.6')
            )

            return objects

        @task_group(
            group_id = 'step_7_trends',
            tooltip = "Final calculations for trends periods data"
        )
        def step_7_trends():
            t_prices = trend_periods_weighted_prices.partial(high_level_step = '7.2.1').expand(period_num = step_2_objects[1][1][3])
            t_rev = trend_periods_revenue.partial(high_level_step = '7.2.2').expand(period_num = step_2_objects[1][1][3])
            t_final = trend_periods_final.partial(high_level_step = '7.2.3').expand(period_num = step_2_objects[1][1][3])
            objects = [t_prices, t_rev, t_final]
            chain(*objects)
            return objects

        metrics_objects = step_7_metrics()
        trends_objects = step_7_trends()

        objects = [
            metrics_objects,
            trends_objects
        ]

        chain(
            [
                step_2_objects[0], # agg_prepare_targets('2.1')
                step_6_objects[0] # metrics_union('6.1')
            ],
            metrics_objects[0] # metrics_categories_names('7.1.1')
        )

        chain(
            step_6_objects[0], # metrics_union('6.1')
            metrics_objects[1] # metrics_categories('7.1.2')
        )

        chain(
            [
                step_3_objects[0][2], # metrics_sellers_revisions('3.2.2')
                step_6_objects[0] # metrics_union('6.1')
            ],
            metrics_objects[2] # metrics_join('7.1.3')
        )

        chain(
            step_6_objects[1], # trend_prd_metrics_union.partial(high_level_step = '6.2').expand(period_num = step_2_objects[1][1][3])
            trends_objects[0] # trend_periods_weighted_prices.partial(high_level_step = '7.2.1').expand(period_num = step_2_objects[1][1][3])
        )

        chain(
            step_2_objects[1][0], # trends_prepare('2.2')
            trends_objects[2] # trend_periods_final.partial(high_level_step = '7.2.3').expand(period_num = step_2_objects[1][1][3])
        )

        return objects


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 8 - Final calculations for trends data
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 8.1.1 - etl.af_trends_ids_pulling
    @task.short_circuit(
        doc_md = dedent("""Pulling IDs from the last period.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_10/af_trends_ids_pulling.sql)"""
        )
    )
    def trends_ids_pulling(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trends_ids_pulling",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 8.1.2 - etl.af_trends_fields_pulling
    @task.short_circuit(
        doc_md = dedent("""Pulling revenue from previous period for periods without sales.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_10/af_trends_fields_pulling.sql)"""
        )
    )
    def trends_fields_pulling(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trends_fields_pulling",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 8.1.3 - etl.af_trends_full_average
    @task.short_circuit(
        doc_md = dedent("""Calculating.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_10/af_trends_full_average.sql)"""
        )
    )
    def trends_full_average(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trends_full_average",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 8.1.4 - etl.af_trends_normalization
    @task.short_circuit(
        doc_md = dedent("""Monthly revenue normalization by average revenue from all periods.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_10/af_trends_normalization.sql)"""
        )
    )
    def trends_normalization(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trends_normalization",
                func_args = {
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 8.1.5 - etl.af_trends_final
    @task.short_circuit(
        doc_md = dedent("""Final aggregations and other calculations for trends.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_10/af_trends_final.sql)"""
        )
    )
    def trends_final(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_trends_final",
                func_args = {
                    "intervals": {"value": input_args["trends_intervals"], "data_type": "int2"},
                    "students_coeff": {"value": input_args["trends_students_coeff"], "data_type": "numeric"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    @task_group(
        group_id = 'step_8',
        tooltip = "Final calculations for trends data"
    )
    def step_8():
        ids_pulling = trends_ids_pulling('8.1.1')
        fields_pulling = trends_fields_pulling('8.1.2')
        full_average = trends_full_average('8.1.3')
        normalization = trends_normalization('8.1.4')
        final = trends_final('8.1.5')

        objects = [ids_pulling, fields_pulling, full_average, normalization, final]

        chain(*objects)

        chain(
            step_7_objects[1][2], # trend_periods_final.partial(high_level_step = '7.2.3').expand(period_num = step_2_objects[1][1][3])
            objects[0] # trends_ids_pulling('8.1.1')
        )

        return objects


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 9 - Populating aggregated metrics table with data from metrics and trends, preparing datasets for other targets
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 9.1 - etl_agg.af_agg_metrics_insert
    @task.short_circuit(
        doc_md = dedent("""Populating aggregated metrics table with data from metrics and trends.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_9/af_agg_metrics_insert.sql)"""
        )
    )
    def agg_metrics_insert(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_metrics_insert",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 9.2.1 - etl_agg.af_agg_metrics_ratings_stats
    @task.short_circuit(
        doc_md = dedent("""Calculating standart deviation and average for gigs' and sellers' ratings.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_9/af_agg_metrics_ratings_stats.sql)"""
        )
    )
    def agg_metrics_ratings_stats(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_metrics_ratings_stats",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 9.2.2 - etl_agg.af_agg_seller_counts
    @task.short_circuit(
        doc_md = dedent("""Preparing etl_wrk.seller_counts table.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_9/af_agg_seller_counts.sql)"""
        )
    )
    def agg_seller_counts(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_seller_counts",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 9.2.3 - etl_agg.af_agg_categories_prefin
    @task.short_circuit(
        doc_md = dedent("""Preparing etl_wrk.categories_prefin table.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_9/af_agg_categories_prefin.sql)"""
        )
    )
    def agg_categories_prefin(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_categories_prefin",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    @task_group(
        group_id = 'step_9',
        tooltip = "Populating aggregated metrics table with data from metrics and trends, preparing datasets for other targets"
    )
    def step_9():

        @task_group(
            group_id = 'step_9_categories',
            tooltip = "Intermediate calculations for categories"
        )
        def step_9_categories():
            return [
                agg_seller_counts('9.2.2'),
                agg_categories_prefin('9.2.3')
            ]

        objects = [
            agg_metrics_insert('9.1'),
            agg_metrics_ratings_stats('9.2.1'),
            step_9_categories()
        ]

        chain(
            [
                step_2_objects[0], # agg_prepare_targets('2.1')
                step_7_objects[0][5], # metrics_final('7.1.6')
                step_8_objects[4] # trends_final('8.1.5')
            ],
            objects[0], # agg_metrics_insert('9.1')
            [
                objects[1], # agg_metrics_ratings_stats('9.2.1')
                objects[2][0], # agg_seller_counts('9.2.2')
                objects[2][1] # agg_categories_prefin('9.2.3')
            ]
        )

        return objects


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 10 - Populating aggregated tables for gigs, sellers and all categories levels metrics
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 10.1.1 - etl_agg.af_agg_gigs_attributes
    @task.short_circuit(
        doc_md = dedent("""Preparing data for aggregated gigs table.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_10/af_agg_gigs_attributes.sql)"""
        )
    )
    def agg_gigs_attributes(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_gigs_attributes",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "rank_scale_factor": {"value": input_args["rank_scale_factor"], "data_type": "numeric"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 10.1.2 - etl_agg.af_agg_gigs_insert
    @task.short_circuit(
        doc_md = dedent("""Populating aggregated gigs table with data.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_10/af_agg_gigs_insert.sql)"""
        )
    )
    def agg_gigs_insert(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_gigs_insert",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 10.2.1 - etl_agg.af_agg_sellers_attributes
    @task.short_circuit(
        doc_md = dedent("""Preparing data for aggregated sellers table.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_10/af_agg_sellers_attributes.sql)"""
        )
    )
    def agg_sellers_attributes(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_sellers_attributes",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "end_dttm": {"value": input_args["end_dttm"], "data_type": "timestamptz"},
                    "rank_scale_factor": {"value": input_args["rank_scale_factor"], "data_type": "numeric"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 10.2.2 - etl_agg.af_agg_sellers_insert
    @task.short_circuit(
        doc_md = dedent("""Populating aggregated sellers table with data.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_10/af_agg_sellers_insert.sql)"""
        )
    )
    def agg_sellers_insert(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_sellers_insert",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 10.3 - etl_agg.af_agg_categories_insert
    @task.short_circuit(
        doc_md = dedent("""Populating aggregated categories table with data.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_10/af_agg_categories_insert.sql)"""
        )
    )
    def agg_categories_insert(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_categories_insert",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 10.4 - etl_agg.af_agg_subcategories_insert
    @task.short_circuit(
        doc_md = dedent("""Populating aggregated subcategories table with data.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_10/af_agg_subcategories_insert.sql)"""
        )
    )
    def agg_subcategories_insert(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_subcategories_insert",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 10.5 - etl_agg.af_agg_nested_subcategories_insert
    @task.short_circuit(
        doc_md = dedent("""Populating aggregated nested_subcategories table with data.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_10/af_agg_nested_subcategories_insert.sql)"""
        )
    )
    def agg_nested_subcategories_insert(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_nested_subcategories_insert",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    @task_group(
        group_id = 'step_10',
        tooltip = "Populating aggregated tables for gigs, sellers and all categories levels metrics"
    )
    def step_10():

        @task_group(
            group_id = 'step_10_gigs',
            tooltip = "Populating aggregated gigs table"
        )
        def step_10_gigs():
            gigs_attributes = agg_gigs_attributes('10.1.1')
            gigs_backup = agg_gigs_insert('10.1.2')
            objects = [gigs_attributes, gigs_backup]
            chain(*objects)
            return objects

        @task_group(
            group_id = 'step_10_sellers',
            tooltip = "Populating aggregated sellers table"
        )
        def step_10_sellers():
            sellers_attributes = agg_sellers_attributes('10.2.1')
            sellers_backup = agg_sellers_insert('10.2.2')
            objects = [sellers_attributes, sellers_backup]
            chain(*objects)
            return objects

        @task_group(
            group_id = 'step_10_categories',
            tooltip = "Populating aggregated categories, subcategories and nested_subcategories tables"
        )
        def step_10_categories():
            return [
                agg_categories_insert('10.3'),
                agg_subcategories_insert('10.4'),
                agg_nested_subcategories_insert('10.5')
            ]

        gigs_objects = step_10_gigs()
        sellers_objects = step_10_sellers()
        categories_objects = step_10_categories()

        objects = [
            gigs_objects,
            sellers_objects,
            categories_objects
        ]

        chain(
            step_2_objects[0], # agg_prepare_targets('2.1')
            [
                gigs_objects[1], # agg_gigs_insert('10.1.2')
                sellers_objects[1] # agg_sellers_insert('10.2.2')
            ]
        )

        chain(
            step_2_objects[0], # agg_prepare_targets('2.1')
            categories_objects # step_10_categories()
        )

        chain_linear(
            [
                step_9_objects[0], # agg_metrics_insert('9.1')
                step_9_objects[1] # agg_metrics_ratings_stats('9.2.1')
            ],
            [
                gigs_objects[0], # agg_gigs_attributes('10.2.1')
                sellers_objects[0] # agg_sellers_attributes('10.3.1')
            ]
        )

        chain_linear(
            step_9_objects[2], # step_9_categories()
            categories_objects # step_10_categories()
        )

        return objects


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 11 - Populating aggregated target tables for categories links and all_categories target table
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 11.1 - etl_agg.af_agg_all_categories_insert
    @task.short_circuit(
        doc_md = dedent("""Populating aggregated all_categories table with data.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_11/af_agg_all_categories_insert.sql)"""
        )
    )
    def agg_all_categories_insert(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_all_categories_insert",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 11.2 - etl_agg.af_agg_category_x_seller_levels_insert
    @task.short_circuit(
        doc_md = dedent("""Populating aggregated category_x_seller_levels table with data.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_11/af_agg_category_x_seller_levels_insert.sql)"""
        )
    )
    def agg_category_x_seller_levels_insert(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_category_x_seller_levels_insert",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 11.3 - etl_agg.af_agg_subcategory_x_seller_levels_insert
    @task.short_circuit(
        doc_md = dedent("""Populating aggregated subcategory_x_seller_levels table with data.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_11/af_agg_subcategory_x_seller_levels_insert.sql)"""
        )
    )
    def agg_subcategory_x_seller_levels_insert(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_subcategory_x_seller_levels_insert",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 11.4 - etl_agg.af_agg_nested_subcategory_x_seller_levels_insert
    @task.short_circuit(
        doc_md = dedent("""Populating aggregated nested_subcategory_x_seller_levels table with data.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_11/af_agg_nested_subcategory_x_seller_levels_insert.sql)"""
        )
    )
    def agg_nested_subcategory_x_seller_levels_insert(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_nested_subcategory_x_seller_levels_insert",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    @task_group(
        group_id = 'step_11',
        tooltip = "Populating aggregated target tables for categories links and all_categories target table"
    )
    def step_11():

        @task_group(
            group_id = 'step_11_links',
            tooltip = "Populating aggregated target tables for categories links"
        )
        def step_11_links():
            return [
                agg_category_x_seller_levels_insert('11.2'),
                agg_subcategory_x_seller_levels_insert('11.3'),
                agg_nested_subcategory_x_seller_levels_insert('11.4')
            ]

        all_categories_insert_object = agg_all_categories_insert('11.1')
        links_objects = step_11_links()

        objects = [
            all_categories_insert_object,
            links_objects
        ]

        chain(
            step_2_objects[0], # agg_prepare_targets('2.1')
            all_categories_insert_object # agg_all_categories_insert('11.1')
        )

        chain(
            step_10_objects[2], # step_10_categories()
            all_categories_insert_object # agg_all_categories_insert('11.1')
        )

        chain_linear(
            [
                step_2_objects[0], # agg_prepare_targets('2.1')
                step_9_objects[2][0] # agg_seller_counts('9.2.2')
            ],
            links_objects # step_11_links()
        )

        chain(
            step_10_objects[2], # step_10_categories()
            links_objects # step_11_links()
        )

        return objects


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Step 12 - Gathering statistics, cleanup
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Step 12.1.1 - etl.af_mj_fin_etl_statistics
    @task.short_circuit(
        doc_md = dedent("""Appending current ETL run statistics to etl_log.etl_attributes_stats.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_15/af_mj_fin_etl_statistics.sql)"""
        )
    )
    def main_job_etl_statistics(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_schema"],
                func_name = "af_mj_fin_etl_statistics",
                func_args = {
                    "etl_schema": {"value": input_args["etl_agg_schema"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 12.1.2 - etl_agg.af_agg_settings_restore
    @task.short_circuit(
        doc_md = dedent("""Restoring targets' settings.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl_agg/parallel_etl/step_12/af_agg_settings_restore.sql)"""
        )
    )
    def agg_settings_restore(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        return etl_plpgsql_function_call(
                func_schema = input_args["etl_agg_schema"],
                func_name = "af_agg_settings_restore",
                func_args = {
                    "tables_prefix": {"value": input_args["tables_prefix"], "data_type": "text"},
                    "metrics_start_dttm": {"value": input_args["metrics_start_dttm"], "data_type": "timestamptz"},
                    "load_id": {"value": input_args["load_id"], "data_type": "int8"},
                    "debug_mode": {"value": input_args["debug_mode"], "data_type": "bool"},
                    "dag_name": {"value": context["dag"].dag_id, "data_type": "text"},
                    "high_level_step": {"value": high_level_step, "data_type": "text"}
                },
                context = context,
                application_name = input_args["application_name"]
            )


    # Step 12.1.3 - etl.af_mj_fin_log_maintenance
    @task(
        doc_md = dedent("""Closing dblink connection, maintaining log and exiting the DAG with final Slack alert.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/etl/parallel_etl/step_15/af_mj_fin_log_maintenance.sql)"""
        )
    )
    def postprocessing(high_level_step: str):
        context = get_current_context()
        input_args = context["ti"].xcom_pull(task_ids = 'step_0.get_input_args')

        query = sql.SQL("""
            select {etl_schema}.af_mj_fin_log_maintenance(
                load_id := {load_id}::int8,
                dag_name := {dag_name}::text,
                high_level_step := {high_level_step}::text
            );
            """
        ).format(
            etl_schema = sql.Identifier(input_args["etl_schema"]),
            load_id = sql.Literal(input_args["load_id"]),
            dag_name = sql.Literal(context["dag"].dag_id),
            high_level_step = sql.Literal(high_level_step)
        )

        query_status, query_result = ETL_PG.fetch_one(query = query, application_name = input_args["application_name"])

        dag_run = context["dag_run"]
        dag_start = dag_run.get_task_instance('step_0.get_input_args').queued_dttm
        query_result.update(runtime = str((pendulum.now(tz = "UTC") - dag_start).as_timedelta()))

        log_msg = f"""{{"query_status": {query_status}, "query_result": {query_result}}}"""

        if query_status is False or query_result.get("status") != 'Success':
            task_logger.error('%s', log_msg)
            raise RuntimeError(prettify_json(query_result))

        task_logger.info('%s', log_msg)

        status_msg = ':white_check_mark: DAG run has succeeded'
        logs = logs_upload(
            load_id = input_args["load_id"],
            application_name = input_args["application_name"]
        )

        slack_alert(
            context = context,
            status_msg = status_msg,
            input_args = input_args,
            result = query_result,
            logs = logs
        )

        return query_status


    @task_group(
        group_id = 'step_12',
        tooltip = "Gathering statistics, cleanup"
    )
    def step_12():
        objects = [
            main_job_etl_statistics('12.1.1'),
            agg_settings_restore('12.1.2'),
            postprocessing('12.1.3')
        ]

        chain(
            [
                step_9_objects[0], # agg_metrics_insert('9.1')
                step_10_objects[0][1], # agg_gigs_insert('10.1.2')
                step_10_objects[1][1], # agg_sellers_insert('10.2.2')
                step_11_objects[0] # agg_all_categories_insert('12.1')
            ],
            objects[0], # main_job_etl_statistics('12.1.1')
            objects[1], # agg_settings_restore('12.1.2')
            objects[2] # postprocessing('12.1.3')
        )

        chain(
            step_10_objects[2], # step_10_categories()
            objects[0] # main_job_etl_statistics('12.1.1')
        )

        chain(
            step_11_objects[1], # step_11_links()
            objects[0] # main_job_etl_statistics('12.1.1')
        )

        return objects


# ###################################################################################
# Taskflow
# ###################################################################################

    # Listing tasks
    step_0_objects = step_0()
    step_1_objects = step_1()
    step_2_objects = step_2()
    step_3_objects = step_3()
    step_4_objects = step_4()
    step_5_objects = step_5()
    step_6_objects = step_6()
    step_7_objects = step_7()
    step_8_objects = step_8()
    step_9_objects = step_9()
    step_10_objects = step_10()
    step_11_objects = step_11()
    step_12()


# DAG invocation
etl_agg_monthly()
