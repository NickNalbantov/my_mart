"""DAG for killing idle connections in multiple PostgreSQL databases"""

# Imports

import logging
import pendulum
from textwrap import dedent

from classes.pg_driver import PostgreSQL
from classes.db_config import DBConfig
from psycopg import sql

from airflow.decorators import dag, task
from airflow.operators.python import get_current_context
from airflow.models.param import Param
from airflow.exceptions import AirflowFailException


# Setting the logger

task_logger = logging.getLogger("airflow.task")
task_logger.setLevel('DEBUG')


# Connections

CON_ETL_PG = DBConfig(db_type = 'PG', db_name = 'etl')
etl_con = CON_ETL_PG.pg_connections()
ETL_PG = PostgreSQL(
    host = etl_con["proxy_host"],
    port = etl_con["port"],
    db = etl_con["db"],
    user = etl_con["user"],
    password = etl_con["password"],
    row_type = "dict",
    application_name = 'Maintenance idle killer'
)

CON_SCRP_PG = DBConfig(db_type = 'PG', db_name = 'scraping')
scraping_con = CON_SCRP_PG.pg_connections()
SCRP_PG = PostgreSQL(
    host = scraping_con["proxy_host"],
    port = scraping_con["port"],
    db = scraping_con["db"],
    user = scraping_con["user"],
    password = scraping_con["password"],
    row_type = "dict",
    application_name = 'Maintenance idle killer'
)

pg_con_dict = {'etl': ETL_PG, 'scraping': SCRP_PG}


# DAG

@dag(
    dag_id = "maintenance_idle_killer",
    default_args = {"retries": 0},
    description = "DAG for killing idle connections in multiple PostgreSQL databases",
    max_active_runs = 1,
    schedule = '*/30 * * * *', # every 30 minutes
    start_date = pendulum.datetime(2023, 1, 1, 0, 0, 0),
    catchup = False,
    tags = ["RDS", "Maintenance", "Multiple DBs"],
    params = {
        "databases": Param(
                default = ['etl'], #, 'scraping'],
                type = 'array',
                title = "databases",
                description = "List of databases aliases where public.maintenance_idle_killer function must be executed",
                examples = ['etl', 'scraping'],
                uniqueItems = True,
                unevaluatedItems = False,
                items = {"type": "string"}
            )
        },
    doc_md = "DAG for killing idle connections in multiple PostgreSQL databases"
)
def maintenance_idle_killer():
    "Upper-level callable for DAG"

    ###################################################################################
    # Tasks
    ###################################################################################

    # Getting list of databases for exec_maintenance_idle_killer tasks iterations
    @task(
        doc = "Getting list of databases for exec_maintenance_idle_killer tasks iterations"
    )
    def make_dbs_list() -> list:
        # Printing and saving list of databases for exec_maintenance_idle_killer tasks iterations
        # AF can iterate only over either hardcoded list / dicts or list / dicts from XCom (this case)
        context = get_current_context()

        dbs_list = context["params"]["databases"]
        task_logger.info("DAG will open connections to these databases: %s", ', '.join(dbs_list))

        return dbs_list


    # Calling public.maintenance_idle_killer function
    @task(
        doc_md = dedent("""Connecting to database and killing idle connections.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/presets/maintenance/maintenance_idle_killer.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def exec_maintenance_idle_killer(db: str):
        context = get_current_context()
        context["db_map_index"] = f"Database: {db}"

        cur_conn = pg_con_dict.get(db, None)

        try:

            query = sql.SQL("""
                select
                    rolename,
                    cnt_idle
                from public.maintenance_idle_killer()"""
        )

            query_status, query_result = cur_conn.fetch_all(query)

            return {"database": db, "status": query_status, "result": query_result}

        except Exception as error:
            return {"database": db, "status": False, "result": error}


    # Checking if all tasks have been successfully finished
    @task(
        doc = "Marking the DAG failed if any 'exec_maintenance_idle_killer' task was unsuccessful"
    )
    def postprocessing(exec_results: list):
        for task_result in exec_results:
            if task_result["status"] is False:
                raise AirflowFailException(task_result)


    ###################################################################################
    # Taskflow
    ###################################################################################

    dbs_list = make_dbs_list()
    exec_results = exec_maintenance_idle_killer.expand(db = dbs_list)
    postprocessing(exec_results)


# DAG invocation
maintenance_idle_killer()
