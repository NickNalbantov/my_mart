"""Module for synchronous connects to PostgreSQL databases and executing SQL scripts with custom logging and error handling"""

import time
import logging
import re

import pendulum
import psycopg
from psycopg import Error, sql, rows

# Setting the logger
logger = logging.getLogger("db_connect")
logger.setLevel('DEBUG')

# Flattening strings for a log
def str_flatten(string: str) -> str:
    "Custom string flattenning function"
    trans = {'\n': ' ', '\t': '', '    ': ''}
    res = re.sub(f"""({'|'.join(map(re.escape, trans.keys()))})""", lambda m: trans[m.group()], string)
    return res


# Error message processing
def psycopg_err_msg_fmt(error: Exception) -> dict:
    "Error message formatting function"
    err_msg = {
        "SQLSTATE": error.diag.sqlstate,
        "MESSAGE": error.diag.message_primary,
        "CONTEXT": error.diag.context,
        "DETAIL": error.diag.message_detail,
        "HINT": error.diag.message_hint
    }
    return err_msg



class PostgreSQL():
    """
    Class for synchronous connects to PostgreSQL databases and executing SQL scripts.
    Mandatory arguments:
        - host (host address - str),
        - port (port number - int),
        - db (database name at PostgreSQL cluster - str),
        - user (username at PostgreSQL cluster - str),
        - password (user password at PostgreSQL cluster - str)
    Optional arguments:
        - application_name (app name to emit, string)
        - sslmode (mode of SSL authorization, string, default = 'prefer')
        - row_type (type of returned data; options - dict / namedtuple / tuple, str, default = 'tuple')
        - retry_connect (will there be retries if first connection attempt would fail, bool, default = True),
        - autocommit (bool, default = True)
    Supports plain string queries (not secure), and psycopg.sql.Composed type queries (made by psycopg.sql.SQL method, protected from SQL injection)
    """

    def __init__(
            self,
            host: str,
            port: int,
            db: str,
            user: str,
            password: str,
            application_name: str = 'Airflow',
            sslmode: str = 'prefer',
            row_type: str = 'tuple',
            retry_connect: bool = True,
            autocommit: bool = True,
        ) -> None:

        self.host = host
        self.port = str(port)
        self.db = db
        self.user = user
        self.password = password
        self.application_name = application_name
        self.sslmode = sslmode
        self.row_type = row_type
        self.retry_connect = retry_connect
        self.autocommit = autocommit



    def info_output(
        self,
        ts: str = pendulum.now(tz = "UTC").format('YYYY-MM-DD HH:mm:ss.SSSSSS'),
        driver: str = 'psycopg',
        query: str = None,
        msg: str = None,
        runtime: str = None,
        err_code: str = None,
        err: str = None
    ) -> dict:
        "Log formatting function"
        return {
            "Timestamp": ts,
            "Driver": driver,
            "Host": self.host,
            "Port": self.port,
            "DB": self.db,
            "User": self.user,
            "Query": query,
            "Runtime": runtime,
            "Message": msg,
            "Error code": err_code,
            "Error": err,
        }


    # Establishing connection
    def db_connect(self, application_name: str = None) -> psycopg.Connection:
        """Establishing connections with PostgreSQL server.
           If retry_connect = True - there'll be two more attempts to connect after first failed attempt"""

        row_fact = None
        if self.row_type == 'dict':
            row_fact = rows.dict_row
        elif self.row_type == 'namedtuple':
            row_fact = rows.namedtuple_row
        else:
            row_fact = rows.tuple_row

        success = False
        attempts = 1

        while (self.retry_connect is True and success is False and attempts < 4) or (self.retry_connect is False and attempts == 1):
            try:
                conn_start_str = pendulum.now(tz = "UTC").format('YYYY-MM-DD HH:mm:ss.SSSSSS')
                connection = psycopg.connect(
                                                host = self.host,
                                                port = self.port,
                                                dbname = self.db,
                                                user = self.user,
                                                password = self.password,
                                                row_factory = row_fact,
                                                autocommit = self.autocommit,
                                                application_name = application_name if application_name is not None else self.application_name,
                                                sslmode = self.sslmode
                                            )
                msg = f"""{f"Attempt #{attempts}. " if self.retry_connect is True else ''}Succesfully connected to PostgreSQL"""
                logger.info(self.info_output(ts = conn_start_str, msg = msg))
                attempts += 1
                success = True

            except (Exception, Error) as error:
                msg = f"""{f"Attempt #{attempts}. " if self.retry_connect is True else ''}Connection to PostgreSQL server was unsuccessful"""
                logger.info(
                            self.info_output(
                                msg = msg, err = str_flatten(str(error))))
                attempts += 1
                if self.retry_connect is True and success is False and attempts < 4:
                    time.sleep(1)
                    continue
                raise error

        return connection


    # Error-catching execute
    def try_execute(
            self,
            connection: psycopg.Connection,
            query: str | bytes | sql.SQL | sql.Composed,
            params: tuple = None,
            prepare: bool = None,
            binary: bool = None
        ):
        "Tries to execute a query in a specified connection, returns cursor with results"

        if connection is not None:

            try:
                cursor = connection.cursor()

                if isinstance(query, (sql.Composed, sql.SQL)) is True:
                    query_str = query.as_string(cursor)
                elif isinstance(query, (str | bytes)) is True:
                    query_str = str(query)
                else:
                    query_str = str(query)
                    raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                query_start = pendulum.now(tz = "UTC")
                cursor.execute(
                    query =  query,
                    params = params,
                    prepare = prepare,
                    binary = binary
                )
                query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()

                msg = f"Query executed in {query_runtime}"
                logger.info(self.info_output(query = str_flatten(query_str), runtime = str(query_runtime), msg = msg))

                return cursor

            except psycopg.errors.QueryCanceled as error:
                connection.rollback()
                msg = "Transaction have been cancelled due to timeout or user request"
                err_code = error.diag.sqlstate if hasattr(error, 'diag') else None
                err_msg = psycopg_err_msg_fmt(error) if hasattr(error, 'diag') else error
                logger.info(
                        self.info_output(
                            query = str_flatten(query_str), msg = msg, err_code = err_code, err = str_flatten(str(err_msg))
                    )
                )
                return False, err_msg

            except (Exception, Error) as error:
                connection.rollback()
                msg = "Other error"
                err_code = error.diag.sqlstate if hasattr(error, 'diag') else None
                err_msg = psycopg_err_msg_fmt(error) if hasattr(error, 'diag') else error
                logger.info(
                        self.info_output(
                            query = str_flatten(query_str), msg = msg, err_code = err_code, err = str_flatten(str(err_msg))
                    )
                )
                return False, err_msg

            finally:
                connection.close()
                msg = "Connection has been closed"
                logger.info(self.info_output(msg = msg))

        else:
            return False, None



    #  Fetch methods

    def fetch_headers(
            self,
            query: str | bytes | sql.SQL | sql.Composed,
            params: tuple = None,
            prepare: bool = None,
            binary: bool = None,
            application_name: str = None
        ):
        """Returns columns' headers"""

        with self.db_connect(application_name) as connection:
            cursor = self.try_execute(
                connection = connection,
                query = query,
                params = params,
                prepare = prepare,
                binary = binary
            )

            if isinstance(cursor, psycopg.Cursor):
                col_str = str(cursor.description)
                cursor.close()
                connection.close()
                msg = "Connection has been closed"
                logger.info(self.info_output(msg = msg))
                return True, re.findall("""'(.*?)'""", col_str)
            return cursor


    def fetch_one(
            self,
            query: str | bytes | sql.SQL | sql.Composed,
            params: tuple = None,
            prepare: bool = None,
            binary: bool = None,
            application_name: str = None
        ):
        """Returns first cell from SELECT query result"""

        with self.db_connect(application_name) as connection:
            cursor = self.try_execute(
                connection = connection,
                query = query,
                params = params,
                prepare = prepare,
                binary = binary
            )

            if isinstance(cursor, psycopg.Cursor):
                result = cursor.fetchone()
                cursor.close()
                connection.close()
                msg = "Connection has been closed"
                logger.info(self.info_output(msg = msg))

                if result is None:
                    return True, None

                if self.row_type == 'dict':
                    return True, result[next(iter(result))] # value of the first (and only) key in the dictionary

                return True, result[0]
            return cursor


    def fetch_first_row(
            self,
            query: str | bytes | sql.SQL | sql.Composed,
            params: tuple = None,
            prepare: bool = None,
            binary: bool = None,
            application_name: str = None
        ):
        """Returns first row from SELECT query result"""

        with self.db_connect(application_name) as connection:
            cursor = self.try_execute(
                connection = connection,
                query = query,
                params = params,
                prepare = prepare,
                binary = binary
            )

            if isinstance(cursor, psycopg.Cursor):
                result = cursor.fetchone()
                cursor.close()
                connection.close()
                msg = "Connection has been closed"
                logger.info(self.info_output(msg = msg))
                return True, result
            return cursor


    def fetch_many(
            self,
            query: str | bytes | sql.SQL | sql.Composed,
            row_cnt: int,
            params: tuple = None,
            prepare: bool = None,
            binary: bool = None,
            application_name: str = None
        ):
        """Returns first row_cnt rows from SELECT query result"""

        with self.db_connect(application_name) as connection:
            cursor = self.try_execute(
                connection = connection,
                query = query,
                params = params,
                prepare = prepare,
                binary = binary
            )

            if isinstance(cursor, psycopg.Cursor):
                result = cursor.fetchmany(row_cnt)
                cursor.close()
                connection.close()
                msg = "Connection has been closed"
                logger.info(self.info_output(msg = msg))
                return True, result
            return cursor


    def fetch_all(
            self,
            query: str | bytes | sql.SQL | sql.Composed,
            params: tuple = None,
            prepare: bool = None,
            binary: bool = None,
            application_name: str = None
        ):
        """Returns all rows from SELECT query result"""

        with self.db_connect(application_name) as connection:
            cursor = self.try_execute(
                connection = connection,
                query = query,
                params = params,
                prepare = prepare,
                binary = binary
            )

            if isinstance(cursor, psycopg.Cursor):
                result = cursor.fetchall()
                cursor.close()
                connection.close()
                msg = "Connection has been closed"
                logger.info(self.info_output(msg = msg))
                return True, result
            return cursor



    # Commands without fetch methods

    def copy_expert(
            self,
            to_file: bool,
            query: str | bytes | sql.SQL | sql.Composed,
            file,
            size: int = 8192,
            params: tuple = None,
            application_name: str = None
        ):
        """Method for executing COPY command

           Arguments:
           - to_file - if True then COPY ... TO {file} command will be executed; if False - COPY ... FROM {file}
           - query - string or psycopg.sql.Composed object with a SQL COPY command to be executed
           - file - path to a source / target file, must be eligible for open() function
           - size - size of a data batch in bytes
           - application_name - optional string for backend's "application_name" in pg_catalog.pg_stat_activity
        """

        with self.db_connect(application_name) as connection:
            if connection is not None:
                try:

                    with connection.cursor() as cursor:

                        if isinstance(query, (sql.Composed, sql.SQL)) is True:
                            query_str = query.as_string(cursor)
                        elif isinstance(query, (str | bytes)) is True:
                            query_str = str(query)
                        else:
                            query_str = str(query)
                            raise ValueError(f"""Invalid query.\nQuery text:\n{query}""")

                        query_start = pendulum.now(tz = "UTC")

                        if to_file is False: # COPY ... FROM STDIN
                            with open(file = file, mode = "rb") as f:
                                with cursor.copy(statement = query, params = params) as copy:
                                    while data := f.read(size):
                                        copy.write(data)
                        elif to_file is True: # COPY ... TO STDOUT
                            with cursor.copy(statement = query, params = params) as copy:
                                with open(file = file, mode = "wb") as f:
                                    while data := copy.read():
                                        f.write(data)
                        else:
                            raise ValueError("""Flag to_file must be set to True or False""")

                        query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()

                        msg = f"Query executed in {query_runtime}"
                        logger.info(self.info_output(query = query_str, runtime = str(query_runtime), msg = msg))

                        return True, 'Success'

                except psycopg.errors.QueryCanceled as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    err_code = error.diag.sqlstate if hasattr(error, 'diag') else None
                    err_msg = psycopg_err_msg_fmt(error) if hasattr(error, 'diag') else error
                    logger.info(
                            self.info_output(
                                query = str_flatten(query_str), msg = msg, err_code = err_code, err = str_flatten(str(err_msg))
                        )
                    )
                    return False, err_msg

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other error"
                    err_code = error.diag.sqlstate if hasattr(error, 'diag') else None
                    err_msg = psycopg_err_msg_fmt(error) if hasattr(error, 'diag') else error
                    logger.info(
                            self.info_output(
                                query = str_flatten(query_str), msg = msg, err_code = err_code, err = str_flatten(str(err_msg))
                        )
                    )
                    return False, err_msg

                finally:
                    connection.close()
                    msg = "Connection has been closed"
                    logger.info(self.info_output(msg = msg))

            else:
                return False, None


    def execute(
            self,
            query: str | bytes | sql.SQL | sql.Composed,
            params: tuple = None,
            prepare: bool = None,
            binary: bool = None,
            application_name: str = None
        ):
        """Executes one query"""

        with self.db_connect(application_name) as connection:
            cursor = self.try_execute(
                connection = connection,
                query = query,
                params = params,
                prepare = prepare,
                binary = binary
            )

            if isinstance(cursor, psycopg.Cursor):
                result = cursor.statusmessage
                cursor.close()
                connection.close()
                msg = "Connection has been closed"
                logger.info(self.info_output(msg = msg))
                return True, result
            return cursor


    def execute_multiple(
            self,
            queries: list,
            params: tuple = None,
            prepare: bool = None,
            binary: bool = None,
            application_name: str = None
        ):
        """Executes multiple queries (from the input list) in single transaction"""

        with self.db_connect(application_name) as connection:
            if connection is not None:
                query_number = 0

                with connection.cursor() as cursor:

                    # Looping through query list
                    for query in queries:
                        try:

                            if isinstance(query, (sql.Composed, sql.SQL)) is True:
                                query_str = query.as_string(cursor)
                            elif isinstance(query, (str | bytes)) is True:
                                query_str = str(query)
                            else:
                                query_str
                                raise ValueError(f"""Invalid query #{queries.index(query) + 1}.\nQuery text:\n{query}""")

                            query_start = pendulum.now(tz = "UTC")
                            cursor.execute(
                                query = query,
                                params = params,
                                prepare = prepare,
                                binary = binary
                            )
                            query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()

                            msg = f"""Query #{queries.index(query) + 1} succesfully executed in {query_runtime}"""
                            logger.info(
                                self.info_output(
                                    query = str_flatten(query_str), runtime = str(query_runtime), msg = msg
                                )
                            )
                            query_number += 1
                            continue

                        except psycopg.errors.QueryCanceled as error:
                            connection.rollback()
                            msg = f"Transaction #{queries.index(query) + 1} have been cancelled due to timeout or user request"
                            err_code = error.diag.sqlstate if hasattr(error, 'diag') else None
                            err_msg = psycopg_err_msg_fmt(error) if hasattr(error, 'diag') else error
                            logger.error(
                                self.info_output(
                                    query = str_flatten(query_str), msg = msg, err_code = err_code, err = str_flatten(str(err_msg))
                                    )
                                )
                            return False, err_msg

                        except (Exception, Error) as error:
                            connection.rollback()
                            msg = f"Query #{queries.index(query) + 1} - Other error"
                            err_code = error.diag.sqlstate if hasattr(error, 'diag') else None
                            err_msg = psycopg_err_msg_fmt(error) if hasattr(error, 'diag') else error
                            logger.error(
                                self.info_output(
                                    query = str_flatten(query_str), msg = msg, err_code = err_code, err = str_flatten(str(err_msg))
                                )
                            )
                            return False, err_msg

                    if query_number == len(queries):
                        msg = "All queries has been executed"
                        logger.info(self.info_output(msg = msg))

                    connection.close()
                    msg = "Connection has been closed"
                    logger.info(self.info_output(msg = msg))
                    return True, 'Success'

            else:
                return False, None



    def insert_batch(
            self,
            table: str,
            columns_list: list = None,
            data_tuples_list: list = None,
            schema: str = None,
            drop_temp_table: bool = True,
            params: tuple = None,
            prepare: bool = None,
            binary: bool = None,
            application_name: str = None
        ):
        """Method for batch insert via COPY command - use it to prevent numerous inserts from list-like objects.
           Like a COPY, works only with SQL literals, not SQL functions.
           For simple small inserts use execute() function instead.

           Arguments:
           - table - target table name (without schema)
           - columns_list - list of columns in target table
           - data_tuples_list - list of m tuples, each with n elements (values for insertion), where m - number of rows, n - number of columns
           - schema - schema name for target table (optional)
           - drop_temp_table - boolean; if False then intermediate unlogged table won't be dropped after insert to target table
           - params, prepare, binary - arguments of psycopg.cursor.execute method
           - application_name - optional string for backend's "application_name" in pg_catalog.pg_stat_activity
        """

        # Validating inputs
        if (
                columns_list is None
                or data_tuples_list is None
                or columns_list == []
                or data_tuples_list == []
        ):
            raise ValueError("""No values specified or no data to insert""")

        column_cnt = len(columns_list)

        for i, data_tuple in enumerate(data_tuples_list):
            if column_cnt != len(data_tuple):
                raise ValueError(f"""Amount of columns in data tuples list at index {i} is not equal to amount of updated columns""")

        with self.db_connect(application_name) as connection:
            if connection is not None:
                try:
                    with connection.cursor() as cursor:

                        # Preparing queries
                        temp_drop_query = sql.SQL("""drop table if exists {}""").format(
                                sql.Identifier(schema, f'{table}_temp_insert') if schema is not None else sql.Identifier(f'{table}_temp_insert')
                            )

                        temp_query = sql.SQL("""create unlogged table {temp} as select {col} from {t} limit 0"""
                            ).format(
                                        temp = sql.Identifier(schema, f'{table}_temp_insert') if schema is not None else sql.Identifier(f'{table}_temp_insert'),
                                        col = sql.SQL(', ').join(sql.Identifier(n) for n in columns_list),
                                        t = sql.Identifier(schema, table) if schema is not None else sql.Identifier(table)
                                    )

                        insert_query = sql.SQL("""insert into {t} ({col}) select * from {temp}"""
                            ).format(
                                    t = sql.Identifier(schema, table) if schema is not None else sql.Identifier(table),
                                    col = sql.SQL(', ').join(sql.Identifier(n) for n in columns_list),
                                    temp = sql.Identifier(schema, f'{table}_temp_insert') if schema is not None else sql.Identifier(f'{table}_temp_insert')
                                )

                        copy_query = sql.SQL("""copy {} from stdin""").format(
                                sql.Identifier(schema, f'{table}_temp_insert') if schema is not None else sql.Identifier(f'{table}_temp_insert')
                            )

                        # Creating intermediate unlogged table
                        query_start = pendulum.now(tz = "UTC")
                        cursor.execute(query = temp_drop_query, params = params, prepare = prepare, binary = binary)
                        cursor.execute(query = temp_query, params = params, prepare = prepare, binary = binary)

                        # Unloading data into intermediate table
                        with cursor.copy(copy_query) as copy:
                            for row in data_tuples_list:
                                copy.write_row(row)

                        # Inserting into target table
                        cursor.execute(query = insert_query, params = params, prepare = prepare, binary = binary)
                        result = cursor.statusmessage

                        # Dropping intermediate table
                        if drop_temp_table is True:
                            cursor.execute(query = temp_drop_query, params = params, prepare = prepare, binary = binary)

                        query_runtime = (pendulum.now(tz = "UTC") - query_start).as_timedelta()
                        msg = f"Query executed in {query_runtime}"
                        logger.info(
                            self.info_output(
                                query = str_flatten(insert_query.as_string(cursor)), runtime = str(query_runtime), msg = msg
                            )
                        )

                        return True, result

                except psycopg.errors.QueryCanceled as error:
                    connection.rollback()
                    msg = "Transaction have been cancelled due to timeout or user request"
                    err_code = error.diag.sqlstate if hasattr(error, 'diag') else None
                    err_msg = psycopg_err_msg_fmt(error) if hasattr(error, 'diag') else error
                    logger.error(self.info_output(msg = msg, err_code = err_code, err = str_flatten(str(err_msg))))
                    return False, err_msg

                except (Exception, Error) as error:
                    connection.rollback()
                    msg = "Other error"
                    err_code = error.diag.sqlstate if hasattr(error, 'diag') else None
                    err_msg = psycopg_err_msg_fmt(error) if hasattr(error, 'diag') else error
                    logger.error(self.info_output(msg = msg, err_code = err_code, err = str_flatten(str(err_msg))))
                    return False, err_msg

                finally:
                    connection.close()
                    msg = "Connection has been closed"
                    logger.info(self.info_output(msg = msg))

            else:
                return False, None
