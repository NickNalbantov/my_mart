"""Module for storing links to Airflow variables with connection parameters to various databases"""

from airflow.models import Variable


class DBConfig():
    """Class with methods for storing links to Airflow variables with connection parameters to various databases"""

    def __init__(self, db_type: str = "PG", db_name: str = "postgres") -> None:
        self.db_type = db_type
        self.db_name = db_name

    def pg_connections(self) -> dict:
        "Connection parameters for PostgreSQL databases"

        etl_db = {
            "host": Variable.get("ETL_DB_HOST"),
            "proxy_host": Variable.get("ETL_DB_PROXY_HOST"),
            "port": Variable.get("ETL_DB_PORT"),
            "db": Variable.get("ETL_DB_NAME"),
            "user": Variable.get("ETL_DB_USER"),
            "password": Variable.get("ETL_DB_PASSWORD")
        }

        scraping_db = {
            "host": Variable.get("SCRAPING_DB_HOST"),
            "proxy_host": Variable.get("SCRAPING_DB_PROXY_HOST"),
            "port": Variable.get("SCRAPING_DB_PORT"),
            "db": Variable.get("SCRAPING_DB_NAME"),
            "user": Variable.get("SCRAPING_DB_USER"),
            "password": Variable.get("SCRAPING_DB_PASSWORD")
        }

        airflow_meta_db = {
            "host": Variable.get("AF_META_DB_HOST"),
            "proxy_host": Variable.get("AF_META_DB_PROXY_HOST"),
            "port": Variable.get("AF_META_DB_PORT"),
            "db": Variable.get("AF_META_DB_NAME"),
            "user": Variable.get("AF_META_DB_USER"),
            "password": Variable.get("AF_META_DB_PASSWORD")
        }

        pg_con = {
            "etl": etl_db,
            "scraping": scraping_db,
            "airflow_meta": airflow_meta_db
        }

        if self.db_name in pg_con and self.db_type == 'PG':
            return [pg_con[i] for i in [self.db_name]][0]
        else:
            raise ValueError("Unknown database alias")
