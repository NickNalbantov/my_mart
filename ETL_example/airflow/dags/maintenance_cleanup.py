"""DAG for database cleanup - launching CLUSTER and ANALYZE on specified databases, schemas and relations in a database"""

# Imports

import os
import errno
import json
import csv
import logging
import pendulum
from textwrap import dedent
from ast import literal_eval

from classes.pg_driver import PostgreSQL
from classes.db_config import DBConfig
from psycopg import sql

from airflow.cli.commands.db_command import all_tables
from airflow.decorators import dag, task
from airflow.operators.python import get_current_context
from airflow.utils.context import Context
from airflow.exceptions import AirflowFailException
from airflow.models import Variable
from airflow.models.param import Param
from airflow.hooks.base import BaseHook
from airflow.providers.slack.operators.slack_webhook import SlackWebhookOperator
from airflow.models.baseoperator import chain


# Setting the logger

task_logger = logging.getLogger("airflow.task")
task_logger.setLevel('DEBUG')

fail_msg = ':x: DAG run failed'

# Connections

CON_ETL_PG = DBConfig(db_type = 'PG', db_name = 'etl')
etl_con = CON_ETL_PG.pg_connections()
ETL_PG = PostgreSQL(
    host = etl_con["proxy_host"],
    port = etl_con["port"],
    db = etl_con["db"],
    user = etl_con["user"],
    password = etl_con["password"],
    row_type = "dict",
    application_name = 'Maintenance cleanup'
)

CON_SCRP_PG = DBConfig(db_type = 'PG', db_name = 'scraping')
scraping_con = CON_SCRP_PG.pg_connections()
SCRP_PG = PostgreSQL(
    host = scraping_con["proxy_host"],
    port = scraping_con["port"],
    db = scraping_con["db"],
    user = scraping_con["user"],
    password = scraping_con["password"],
    row_type = "dict",
    application_name = 'Maintenance cleanup'
)

CON_AF_META_PG = DBConfig(db_type = 'PG', db_name = 'airflow_meta')
af_meta_con = CON_AF_META_PG.pg_connections()
AF_META_PG = PostgreSQL(
    host = af_meta_con["proxy_host"],
    port = af_meta_con["port"],
    db = af_meta_con["db"],
    user = af_meta_con["user"],
    password = af_meta_con["password"],
    row_type = "dict",
    application_name = 'Maintenance cleanup'
)

pg_con_dict = {'etl': ETL_PG, 'scraping': SCRP_PG, 'airflow_meta': AF_META_PG}

slack_conn_id = Variable.get('slack_conn')


# Other variables

errors_file = f"""/opt/af_repo_logs_mirror/airflow/dags/logs/{os.path.basename(__file__).replace('.py', '')}_errors.csv"""


# Docs

bloat_cleanup_objects_default_params = [
    {
        "database": "etl",
        "schemas":
            [
                {
                    "schema": "public",
                    "include": ["*"],
                    "exclude":
                        [
                            "gigs_pages",
                            "gigs_revisions",
                            "sellers_revisions",
                            "gig_reviews"
                        ]
                },
                {
                    "schema": "etl",
                    "include":
                        [
                            "gigs_revisions_pre_metrics",
                            "gigs_pages_pre_metrics",
                            "sellers_revisions_pre_metrics",
                            "gig_status_history",
                            "seller_status_history",
                            "gig_reviews_agg",
                            "gig_reviews_prices"
                        ],
                    "exclude": []
                },
                {
                    "schema": "auth",
                    "include": ["*"],
                    "exclude": []
                },
                {
                    "schema": "payment",
                    "include": ["*"],
                    "exclude": []
                },
                {
                    "schema": "utils",
                    "include": ["*"],
                    "exclude": []
                }
            ]
    },
    {
        "database": "scraping",
        "schemas":
            [
                {
                    "schema": "public",
                    "include": ["*"],
                    "exclude": []
                }
            ]
    },
    {
        "database": "airflow_meta",
        "schemas":
            [
                {
                    "schema": "public",
                    "include": ["*"],
                    "exclude": []
                }
            ]
    }
]

params_desc_md = """
- *clean_before_timestamp* - DAG will delete DB records older than this timestamp in ETL log tables and Airflow meta DB tables
- *airflow_cleanup_objects* - List of objects where old rows will be deleted in Airflow meta DB
- *bloat_cleanup_objects* - JSON structure with database -> schema -> relations to be included / excluded hierarchy (each level is a list with unique items)
"""

bloat_cleanup_objects_desc_md = """
*bloat_cleanup_objects* parameter description:

All whitelisted ("include" list - "exclude" list) relations will be maintained (CLUSTER and ANALYZE) in order of lists' indices and hierarchy.

```["*"]``` means all local tables and materialized views in corresponding schema (```pg_catalog.pg_class.relkind in ('r', 'p', 'm')```)""" \
""" in alphabetical order by name

If any relation list (included or excluded) contains ```"*"``` then the whole list will be reduced to ```["*"]```

"exclude" list is always overriding the corresponding "include" list. If "exclude" list contains ```"*"``` then all relations in the schema will be blacklisted

For database "etl" schemas *etl_agg* and *etl_agg_dev* are not included in the list because all objects there are already clusterd by ETL

Same thing is applicable to the marts in schemas *etl* and *etl_dev*, but all objects (```"*"```) from them are included in the list""" \
""" - it has been done solely for shortening default argument value so it will fit into the Slack message 3000 symbols limit

It's safe because *public.maintenance_bloat_cleanup* function is clustering only those objects that need clustering""" \
""" (have dead tuples or cluster index keys correlation coefficient < 1)"""

# Docs

dag_doc_md = f"""
DAG for database cleanup - launching CLUSTER and ANALYZE on specified schemas and relations in a database

**READMEs**:
[Airflow repo README](https://gitlab.com/fiverr-dev/data-engineering/airflow/-/blob/main/README.md),
[ETL repo README](https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/README.md)

**Schedule**: 5 6 * * SUN

**Retries**: 0

**DAG params**:
{params_desc_md}
---
{bloat_cleanup_objects_desc_md}

**Default values**:
```python
{{
    "clean_before_timestamp": pendulum.now(tz = "UTC").start_of("day").subtract(months = 1).isoformat(),
    "airflow_cleanup_objects": airflow.cli.commands.db_command.all_tables,
    "bloat_cleanup_objects":
        {json.dumps({"bloat_cleanup_objects": bloat_cleanup_objects_default_params}, sort_keys = True, indent = 4)}
}}
```
"""


###################################################################################
# Supporting functions
###################################################################################

# Removing file if exists
def file_remove(filename):
    "File removal function"
    try:
        os.remove(filename)
    except OSError as err:
        if err.errno != errno.ENOENT:
            task_logger.error("Exception: %s", err)
            raise

# JSON pretty print
def prettify_json(json_input, sort = True, indents = 4) -> str:
    "Returns properly indented JSON string"
    try:
        if isinstance(json_input, str) is True:
            return json.dumps(json.loads(json_input), sort_keys = sort, indent = indents)
        return json.dumps(json_input, sort_keys = sort, indent = indents)
    except ValueError:
        task_logger.error("Invalid JSON input: %s", json_input)
        return json_input


# Slack alert function
def slack_alert(context, status_msg: str, input_args: dict, result):
    "Function for sending messages to Slack channel"

    channel = BaseHook.get_connection(slack_conn_id).login

    # Slack mrkdwn pretty string versions of input_args and status
    input_args_str = prettify_json(input_args)
    
    if isinstance(result, dict) is True and 'status' in result:
        status_str = prettify_json(result['status'])
    elif isinstance(result, dict) is True and 'status' not in result:
        status_str = prettify_json(result)
    else:
        status_str = prettify_json(fr"{str(result)}")


    dag_run = context["dag_run"]
    dag_start = dag_run.get_task_instance('make_dbs_list').queued_dttm
    runtime = str((pendulum.now(tz = "UTC") - dag_start).as_timedelta())

    # Message body
        # Slack message length is limited to 3001 characters
    payload = f"""*DAG*: {context["dag"].dag_id}\n\n""" \
        f"""*Task*: {context.get('task_instance').task_id}\n\n""" \
        f"""*Input parameters*:\n```{input_args_str[0:1000]}```\n\n""" \
        f"""*Runtime*: {runtime}\n\n""" \
        f"""*Status*:\n```{status_str[0:1000]}```"""

    slack_msg = [ # should be a list of dictionaries
            {
                "type": "divider"
            },
            {
                "type": "header",
                "text": {
                    "type": "plain_text",
                    "text": status_msg
                }
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": payload
                }
            }
        ]

    # full Blocks message for the Block Kit Builder https://app.slack.com/block-kit-builder/T05PNU32MQE
    task_logger.info('{"blocks": %s}', slack_msg)

    slack_alert = SlackWebhookOperator(
        task_id='slack_alert',
        message = status_msg,
        blocks = slack_msg,
        channel = channel,
        username = 'airflow',
        slack_webhook_conn_id = slack_conn_id
    )

    return slack_alert.execute(context = context)


###################################################################################
# Callbacks
###################################################################################

def task_failure_callback(context):
    "Task level failure callback function"

    task_logger.info("Task failure callback context: %s", context)

    failed_task_name = context.get('task_instance').task_id
    failed_task_error = context.get("exception")

    try:
        if (
            isinstance(failed_task_error, str) is True
            and failed_task_error.startswith('{')
        ):
            failed_task_error = literal_eval(failed_task_error)

        with open(file = errors_file, mode = "a+") as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow([failed_task_name, failed_task_error])
    except Exception:
        task_logger.error("Invalid dict input: %s", failed_task_error)


def dag_failure_callback(context):
    "DAG level failure callback function"

    task_logger.info("DAG failure callback context: %s", context)
    input_args = context["params"]

    try:
        with open(file = errors_file, mode = 'r+') as csv_file:
            reader = csv.reader(csv_file)
            err_result = {
                rows[0]:
                    literal_eval(prettify_json(fr"{rows[1]}")) if fr"{rows[1]}".startswith('{')
                    else prettify_json(fr"{rows[1]}")
                for rows in reader
            }

        file_remove(errors_file)
    except Exception as err:
        task_logger.error("Exception: %s", err)
        err_result = f"""Check $AIRFLOW_HOME/logs/scheduler/latest/{context["dag"].dag_id}.py.log for callback log"""

    task_logger.error('err_result type: %s, err_result value: %s', type(err_result), err_result)

    slack_alert(
        context = context,
        status_msg = fail_msg,
        input_args = input_args,
        result = err_result
    )


###################################################################################
# Task-formatting functions
###################################################################################

def etl_plpgsql_function_call(
    func_schema: str,
    func_name: str,
    func_args: dict,
    context: Context
) -> bool:
    """
    Function for generating task code for a simple calls of ETL PL/pgSQL-functions.
    "func_args" argument must be a dictionary with this structure:
        {
            "arg_1_name":
                {
                    "value": arg_value,
                    "data_type": PostgreSQL type name identifier (value from pg_catalog.pg_type.typname)
                },
            ...
        }

    "data_type" value must be exactly the same as type name from pg_catalog.pg_type.typname.
    Any aliases (like "bigint" instead of "int8") are deliberately not supported
    Most common types for this DAG: timestamptz, int8, int4, int2, bool, numeric, text, jsonb
    """

    func_list = []

    for arg_name, arg_meta in func_args.items():

        func_list.append(
            sql.SQL('::').join(
                [
                    sql.SQL(' := ').join(
                        [
                            sql.Identifier(arg_name),
                            sql.Literal(
                                arg_meta["value"] if arg_meta["data_type"] not in ['json', 'jsonb']
                                else json.dumps(arg_meta["value"])
                            )
                        ]
                    ),
                    sql.Identifier(arg_meta["data_type"])
                ]
            )
        )

    query = sql.SQL("""select {function}({arguments});""").format(
        function = sql.Identifier(func_schema, func_name),
        arguments = sql.SQL(', ').join(func_list)
    )

    query_status, query_result = ETL_PG.fetch_one(query = query)

    log_msg = f"""{{"query_status": {query_status}, "query_result": {query_result}}}"""
    task_logger.info('%s', log_msg)

    if query_status is False or query_result.get("status") != 'Success':
        dag_run = context["dag_run"]
        dag_start = dag_run.get_task_instance('make_dbs_list').queued_dttm
        query_result.update(runtime = str((pendulum.now(tz = "UTC") - dag_start).as_timedelta()))
        task_logger.error('%s', log_msg)
        raise RuntimeError(prettify_json(query_result))

    return query_status


###################################################################################
# DAG definition
###################################################################################

@dag(
    dag_id = "maintenance_cleanup",
    default_args = {
        "retries": 0,
        "wait_for_downstream": True,
        "on_failure_callback": task_failure_callback
    },
    description = "DAG for maintaining objects in multiple PostgreSQL databases",
    max_active_runs = 1,
    schedule = '5 6 * * SUN', # every Sunday at 06:05 in UTC timezone
    start_date = pendulum.datetime(2023, 1, 1, 0, 0, 0),
    catchup = False,
    tags = ["RDS", "Maintenance", "Multiple DBs"],
    on_failure_callback = dag_failure_callback,
    params = {
        "clean_before_timestamp": Param(
            default = pendulum.now(tz = "UTC").start_of("day").subtract(months = 1).isoformat(),
            type = "string",
            format = "date-time",
            description = "Delete records older than this timestamp in ETL log tables and Airflow meta DB tables",
        ),
        "airflow_cleanup_objects": Param(
            default = all_tables,
            type = ["array"],
            description = "List of objects where old rows will be deleted in Airflow meta DB",
        ),
        "bloat_cleanup_objects": Param(
            default = bloat_cleanup_objects_default_params,
            type = 'array',
            title = "bloat_cleanup_objects",
            description_md = dedent(bloat_cleanup_objects_desc_md),
            examples = bloat_cleanup_objects_default_params,
            minItems = 1,
            uniqueItems = True,
            unevaluatedItems = False,
            items =
                {
                    "type": "object",
                    "minProperties": 2,
                    "maxProperties": 2,
                    "properties":
                        {
                            "database":
                                {
                                    "enum":
                                        [
                                            "etl",
                                            "scraping",
                                            "airflow_meta"
                                        ]
                                },
                            "schemas":
                                {
                                    "type": "array",
                                    "minItems": 1,
                                    "uniqueItems": True,
                                    "unevaluatedItems": False,
                                    "items":
                                        {
                                            "type": "object",
                                            "minProperties": 3,
                                            "maxProperties": 3,
                                            "properties":
                                                {
                                                    "schema":
                                                        {
                                                            "type": "string"
                                                        },
                                                    "include":
                                                        {
                                                            "type": "array",
                                                            "uniqueItems": True,
                                                            "unevaluatedItems": False,
                                                            "items":
                                                                {
                                                                    "type": "string"
                                                                }
                                                        },
                                                    "exclude":
                                                        {
                                                            "type": "array",
                                                            "uniqueItems": True,
                                                            "unevaluatedItems": False,
                                                            "items":
                                                                {
                                                                    "type": "string"
                                                                }
                                                        }
                                                },
                                            "required":
                                                [
                                                    "schema",
                                                    "include",
                                                    "exclude"
                                                ]
                                        }
                                }
                        },
                    "required":
                        [
                            "database",
                            "schemas"
                        ]
                }
            )
        },
    doc_md = dedent(dag_doc_md)
)
def maintenance_cleanup():
    "Upper-level callable for DAG"

    ###################################################################################
    # Tasks
    ###################################################################################

    # Getting list of databases for exec_maintenance_bloat_cleanup tasks iterations
    @task(
        doc = "Getting list of databases for exec_maintenance_bloat_cleanup tasks iterations"
    )
    def make_dbs_list() -> list:
        # Printing and saving list of databases for exec_maintenance_bloat_cleanup tasks iterations
        # AF can iterate only over either hardcoded list / dicts or list / dicts from XCom (this case)
        context = get_current_context()

        dbs_list = [n["database"] for n in context["params"]["bloat_cleanup_objects"]]
        task_logger.info("DAG will open connections to these databases: %s", ', '.join(dbs_list))

        return dbs_list


    # Cleaning up old rows from Airflow meta DB with embedded Airflow CLI command
    @task.bash(
        doc = "Running 'airflow db clean --verbose --yes' Bash command"
    )
    def airflow_db_cleanup():
        context = get_current_context()
        input_args = context["params"]
        bash_command = f"""\
            airflow db clean \
                --clean-before-timestamp '{input_args["clean_before_timestamp"]}' \
                --skip-archive \
                --tables {",".join(f"'{t}'" for t in input_args["airflow_cleanup_objects"])} \
                --verbose \
                --yes \
        """
        return bash_command


    # Cleaning up ETl log
    @task.short_circuit(
        doc_md = dedent("""Deleting old rows from ETL log table in ETL DB with public.maintenance_etl_log_cleanup function
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/presets/maintenance/maintenance_etl_log_cleanup.sql)"""
        )
    )
    def etl_log_cleanup():
        context = get_current_context()
        input_args = context["params"]

        return etl_plpgsql_function_call(
                func_schema = "public",
                func_name = "maintenance_etl_log_cleanup",
                func_args = {
                    "clean_before_timestamp": {"value": input_args["clean_before_timestamp"], "data_type": "timestamptz"}
                },
                context = context
            )


    # Cleaning up ETl statistics
    @task.short_circuit(
        doc_md = dedent("""Deleting old rows from ETL statistics table in ETL DB with public.maintenance_etl_attributes_stats_cleanup function
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/presets/maintenance/maintenance_etl_attributes_stats_cleanup.sql)"""
        )
    )
    def etl_stats_cleanup():
        context = get_current_context()
        input_args = context["params"]

        return etl_plpgsql_function_call(
                func_schema = "public",
                func_name = "maintenance_etl_attributes_stats_cleanup",
                func_args = {
                    "clean_before_timestamp": {"value": input_args["clean_before_timestamp"], "data_type": "timestamptz"}
                },
                context = context
            )


    # Preparing list of relations to be maintained and executing CLUSTER and ANALYZE
    @task(
        doc_md = dedent("""Connecting to database, preparing a list of relations to be maintained""" \
            """ and executing CLUSTER and ANALYZE commands for chosen relations.
            [Code of PL/pgSQL-function on Gitlab]""" \
            """(https://gitlab.com/fiverr-dev/data-engineering/etl/-/blob/main/presets/maintenance/maintenance_bloat_cleanup.sql)"""
        ),
        map_index_template = "{{ db_map_index }}"
    )
    def exec_maintenance_bloat_cleanup(db: str):
        context = get_current_context()
        input_args = context["params"]

        context["db_map_index"] = f"Database: {db}"

        # Checking list of objects and connection for a chosen database
        sch_list = []
        for db_dict in input_args["bloat_cleanup_objects"]:
            if db_dict['database'] == db:
                sch_list = db_dict["schemas"]
            else:
                continue

        cur_conn = pg_con_dict.get(db, None)

        # Calling public.maintenance_bloat_cleanup function:
            # it parses input arguments into a temporary table, execute maintenance_bloat_cleanup function over the resulting list and returns that temporary table
        if sch_list != [] and cur_conn is not None:
            try:
                objs = json.dumps(sch_list)

                query = sql.SQL("""
                        select
                            nspname,
                            relname,
                            cluster_index_name
                        from public.maintenance_bloat_cleanup
                            (
                                objects := {obj}::jsonb,
                                dry_run := false
                            );
                    """
                ).format(obj = sql.Literal(objs))

                query_status, query_result = cur_conn.fetch_all(query)

                if query_status is False:
                    not_clustered = []
                    status_msg = f':x: Cleanup task for database {db} failed'
                    slack_alert(context, status_msg, input_args, query_result)
                else:
                    not_clustered = [f'{row["nspname"]}.{row["relname"]}' for row in query_result if row["cluster_index_name"] is None]

                return {"database": db, "status": query_status, "result": query_result, "not_clustered": not_clustered}

            except Exception as error:
                status_msg = f':x: Cleanup task for database {db} failed'
                slack_alert(context, status_msg, input_args, error)
                return {"database": db, "status": False, "result": error, "not_clustered": []}


    # Creating Slack alerts
    @task(
        doc = "Generating final alert to Slack"
    )
    def postprocessing(bloat_cleanup: list):
        context = get_current_context()
        input_args = context["params"]

        task_logger.info(bloat_cleanup)

        failed_tasks_list = [ft["database"] for ft in bloat_cleanup if ft["status"] is False]

        not_clustered_list = [
            {
                "database": nc["database"],
                "objects": nc["not_clustered"]
            }
            for nc in bloat_cleanup if nc["status"] is True and nc["not_clustered"] != []
        ]

        clustered_list = [
            {
                "database": cl["database"],
                "objects": [f'{obj["nspname"]}.{obj["relname"]}' for obj in cl["result"]]
            }
            for cl in bloat_cleanup if cl["status"] is True
        ]

        exec_tasks_amt = len(bloat_cleanup)
        exec_success_tasks_amt = exec_tasks_amt - len(failed_tasks_list)

        err_msg = warn_msg = info_msg = ''

        status_msg = "Old Airflow meta :white_check_mark:\nOld ETL logs and statistics :white_check_mark:"

        if len(failed_tasks_list) > 0:
            err_msg = f"ERROR: Cleanup tasks for databases {failed_tasks_list} have failed\n" \
                "Check messages above for the details\n\n"

        if len(not_clustered_list) > 0:
            warn_msg = "WARNING: Some objects have been skipped because they can't be clustered:\n\n" \
                f"""{prettify_json(str(not_clustered_list).replace("'", '"'))}\n\n"""

        if len(clustered_list) > 0:
            info_msg = "INFO: Succesfully clustered objects:\n\n" \
                f"""{prettify_json(str(clustered_list).replace("'", '"'))}"""

        # Scenario #1 - all exec_maintenance_bloat_cleanup tasks have failed
        if exec_success_tasks_amt == 0:
            status_msg = f"""{status_msg}\nBloat :x:"""
            slack_alert(context, status_msg, input_args, err_msg)
            raise AirflowFailException(err_msg)

        # Scenario #2-3 - some exec_maintenance_bloat_cleanup tasks have failed, some have finished successfully
        if 0 < exec_success_tasks_amt < exec_tasks_amt:

            # Scenario 2 - not all objects in succesfull tasks have been clustered
            if len(not_clustered_list) > 0:
                status_msg = f"""{status_msg}\nBloat :bangbang:"""
                slack_alert(context, status_msg, input_args, f'{err_msg}{warn_msg}{info_msg}')

            # Scenario 3 - all objects in succesfull tasks have been clustered
            else:
                status_msg = f"""{status_msg}\nBloat :exclamation:"""
                slack_alert(context, status_msg, input_args, f'{err_msg}{info_msg}')

        # Scenario #4-5 - all exec_maintenance_bloat_cleanup tasks have finished successfully
        if exec_tasks_amt == exec_success_tasks_amt:

            # Scenario 4 - not all objects in succesfull tasks have been clustered
            if len(not_clustered_list) > 0:
                status_msg = f"""{status_msg}\nBloat :ballot_box_with_check:"""
                slack_alert(context, status_msg, input_args, f'{warn_msg}{info_msg}')

            # Scenario 5 - all objects in succesfull tasks have been clustered
            else:
                status_msg = f"""{status_msg}\nBloat :white_check_mark:"""
                slack_alert(context, status_msg, input_args, info_msg)

        return 'Cleanup done'


    ###################################################################################
    # Taskflow
    ###################################################################################

    dbs_list = make_dbs_list()
    bloat_cleanup = exec_maintenance_bloat_cleanup.partial().expand(db = dbs_list)

    chain(
            [
                airflow_db_cleanup(),
                etl_log_cleanup(),
                etl_stats_cleanup()
            ],
            bloat_cleanup # exec_maintenance_bloat_cleanup.partial().expand(make_dbs_list())
        )

    postprocessing(bloat_cleanup)


# DAG invocation
maintenance_cleanup()
