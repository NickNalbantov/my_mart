#!/bin/sh

if [ `lsof -t -i:8080 | wc -c` != 0 ]; then kill -9 `lsof -t -i:8080`; fi
if [ `lsof -t -i:8793 | wc -c` != 0 ]; then kill -9 `lsof -t -i:8793`; fi
pkill -f 'airflow webserver'
/usr/local/bin/airflow db init
/usr/local/bin/airflow webserver --pid $AIRFLOW_HOME/airflow-webserver.pid