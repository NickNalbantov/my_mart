#!/bin/sh
airflow users create --role Admin --username admin --email admin --firstname admin --lastname admin --password {admin_password}

airflow variables delete ETL_DB_HOST
airflow variables delete ETL_DB_PORT
airflow variables delete ETL_DB_NAME
airflow variables delete ETL_DB_USER
airflow variables delete ETL_DB_PASSWORD

airflow variables delete ETL_DB_PROXY_HOST

airflow variables delete SCRAPING_DB_HOST
airflow variables delete SCRAPING_DB_PORT
airflow variables delete SCRAPING_DB_NAME
airflow variables delete SCRAPING_DB_USER
airflow variables delete SCRAPING_DB_PASSWORD

airflow variables delete SCRAPING_DB_PROXY_HOST

airflow variables delete AF_META_DB_HOST
airflow variables delete AF_META_DB_PORT
airflow variables delete AF_META_DB_NAME
airflow variables delete AF_META_DB_USER
airflow variables delete AF_META_DB_PASSWORD

airflow variables delete AF_META_DB_PROXY_HOST

airflow connections delete slack_connection
airflow variables delete slack_conn


airflow variables set --description "Host of the ETL DB" ETL_DB_HOST {variable_value}
airflow variables set --description "Port of the ETL DB" ETL_DB_PORT {variable_value}
airflow variables set --description "DB name of the ETL DB" ETL_DB_NAME {variable_value}
airflow variables set --description "Username for connection to the ETL DB" ETL_DB_USER {variable_value}
airflow variables set --description "Password for connection to the ETL DB" ETL_DB_PASSWORD {variable_value}

airflow variables set --description "Host of the ETL DB RDS proxy" ETL_DB_PROXY_HOST {variable_value}

airflow variables set --description "Host of the scraping DB" SCRAPING_DB_HOST {variable_value}
airflow variables set --description "Port of the scraping DB" SCRAPING_DB_PORT {variable_value}
airflow variables set --description "DB name of the scraping DB" SCRAPING_DB_NAME {variable_value}
airflow variables set --description "Username for connection to the scraping DB" SCRAPING_DB_USER {variable_value}
airflow variables set --description "Password for connection to the scraping DB" SCRAPING_DB_PASSWORD {variable_value}

airflow variables set --description "Host of the scraping DB RDS proxy" SCRAPING_DB_PROXY_HOST {variable_value}

airflow variables set --description "Host of the Airflow meta DB" AF_META_DB_HOST {variable_value}
airflow variables set --description "Port of the Airflow meta DB" AF_META_DB_PORT {variable_value}
airflow variables set --description "DB name of the Airflow meta DB" AF_META_DB_NAME {variable_value}
airflow variables set --description "Username for connection to the Airflow meta DB" AF_META_DB_USER {variable_value}
airflow variables set --description "Password for connection to the Airflow meta DB" AF_META_DB_PASSWORD {variable_value}

airflow variables set --description "Host of the Airflow meta DB RDS proxy" AF_META_DB_PROXY_HOST {variable_value}

airflow connections add --conn-type http --conn-description "Slack connection" --conn-host https://hooks.slack.com/services/ --conn-password {password_value} --conn-login airflow_notifications slack_connection
airflow variables set --description "Slack connection" slack_conn slack_connection