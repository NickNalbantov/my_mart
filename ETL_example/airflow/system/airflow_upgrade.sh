#!/bin/sh
# PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install "apache-airflow[celery]==2.10.5" --constraint "https://raw.githubusercontent.com/apache/airflow/constraints-2.10.5/constraints-3.12.txt" &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade apache-airflow &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade apache-airflow-providers-celery &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade apache-airflow-providers-common-compat &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade apache-airflow-providers-common-io &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade apache-airflow-providers-common-sql &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade apache-airflow-providers-fab &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade apache-airflow-providers-ftp &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade apache-airflow-providers-http &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade apache-airflow-providers-imap &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade apache-airflow-providers-smtp &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade apache-airflow-providers-slack &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade apache-airflow-providers-sqlite &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade psycopg &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade psycopg[binary] &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade pendulum &&
PIP_BREAK_SYSTEM_PACKAGES=1 pip --disable-pip-version-check --no-cache-dir --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org install --upgrade psycopg2 &&
airflow db migrate &&
sudo systemctl restart airflow-webserver &&
sudo systemctl restart airflow-scheduler