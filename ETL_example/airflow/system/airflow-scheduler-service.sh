#!/bin/sh

pkill -f 'airflow scheduler'
/usr/local/bin/airflow scheduler --pid $AIRFLOW_HOME/airflow-scheduler.pid